/*
 * Copyright (c) Open Source Strategies, Inc.
 *
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.fio.dataimport;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.fio.dataimport.util.DataUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;
import org.w3c.dom.Element;


/**
 * maps DataImportCustomer into a set of opentaps entities that describes the Customer
 * TODO: break this up big method into several logical steps, each implemented in class methods.
 * This will allow for much easier re-use for custom imports.
 */
public class ContactDecoder implements ImportDecoder {
    private static final String MODULE = CustomerDecoder.class.getName();
    protected String initialResponsiblePartyId;
    protected String initialResponsibleRoleTypeId;
    protected String organizationPartyId;
    protected String arGlAccountId;
    protected String offsettingGlAccountId;
    protected GenericValue userLogin;

    /**
     * Creates a customer decoder from an input context.
     * This will automatically extract the class variables it needs and
     * validate for the existence of GL accounts and CRMSFA roles.
     * If there is a problem, a GeneralException is thrown.
     */
    public ContactDecoder(Map<String, ?> context) throws GeneralException {
        this.initialResponsiblePartyId = (String) context.get("initialResponsiblePartyId");
        this.initialResponsibleRoleTypeId = (String) context.get("initialResponsibleRoleTypeId");
        this.organizationPartyId = (String) context.get("organizationPartyId");
        this.arGlAccountId = (String) context.get("arGlAccountId");
        this.offsettingGlAccountId = (String) context.get("offsettingGlAccountId");
        this.userLogin = (GenericValue) context.get("userLogin");
        
        Delegator delegator = userLogin.getDelegator();
        
        this.initialResponsibleRoleTypeId = getFirstValidRoleTypeId(initialResponsiblePartyId,TEAM_MEMBER_ROLES, delegator);
    }

    public static List<String> TEAM_MEMBER_ROLES = UtilMisc.toList("EMPLOYEE", "ACCOUNT_REP", "CUST_SERVICE_REP");
    
    public List<GenericValue> decode(GenericValue entry, Timestamp importTimestamp, Delegator delegator, LocalDispatcher dispatcher, Object... args) throws Exception {
        
    	String isExternalIdAsPartyId = UtilProperties.getPropertyValue("config.properties", "is.contact.externalId.asPartyId");
    	
    	List<GenericValue> toBeStored = FastList.newInstance();

        GenericValue person = null;
        String partyId = null;
        
        String source = "EXT_PARTY_ID";
        
        GenericValue partyAttribute = EntityUtil.getFirst(delegator.findByAnd("PartyIdentification",UtilMisc.toMap("partyIdentificationTypeId",source,"idValue",entry.getString("rpIdentityno")),null,true));
		if(UtilValidate.isNotEmpty(partyAttribute)){
			partyId = partyAttribute.getString("partyId");
		}
		
		if (UtilValidate.isNotEmpty(partyId)) {
			
			person = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);
			
		}
		
		String firstName = null;
		String lastName = null;
		
		if (UtilValidate.isNotEmpty(entry.getString("rpFullname"))) {
			String[] tokens = entry.getString("rpFullname").split(" ", 2);
			if (tokens.length > 1) {
				firstName = tokens[0];
				lastName = tokens[1];
			} else {
				firstName = tokens[0];
				lastName = tokens[0];
			}
		}
		
        /***********************/
        /** Update Party data **/
        /***********************/
        if(UtilValidate.isNotEmpty(partyId)){
        	
        	//update basic details
        	
            if (UtilValidate.isNotEmpty(person)) {
            	person.put("firstName", firstName);
            	person.put("lastName", lastName);
            	toBeStored.add(person);
            }
            
            if (!UtilValidate.isEmpty(source)) {
            	GenericValue checkSource = delegator.findOne("PartyIdentification", UtilMisc.toMap("partyId", partyId,"partyIdentificationTypeId",source), true);
            	if(UtilValidate.isEmpty(checkSource)) {
                    toBeStored.add(delegator.makeValue("PartyIdentification", UtilMisc.toMap( "partyId", partyId,"partyIdentificationTypeId",source,"idValue",entry.getString("rpIdentityno"))));
            	}
            }
         	
        	toBeStored.add(userLogin);
        	return toBeStored;
        	
        }
        
        /***********************/
        /** Import Party data **/
        /***********************/

        // create the Person and Party with the roles for each depending on whether companyName or lastName is present
        String personPartyId = null;
        if (UtilValidate.isNotEmpty(lastName)) {
        	
        	if (UtilValidate.isNotEmpty(isExternalIdAsPartyId) && isExternalIdAsPartyId.equals("Y")) {
        		personPartyId = entry.getString("rpIdentityno");
            } else {
            	personPartyId = delegator.getNextSeqId("Party");
            }
        	
        	//personPartyId = delegator.getNextSeqId("Party");
            
        	toBeStored.addAll(UtilImport.makePartyWithRolesExt(personPartyId, "PERSON", entry.getString("rpIdentityno"),  UtilMisc.toList("CUSTOMER", "BILL_TO_CUSTOMER"), delegator));
            person = delegator.makeValue("Person", UtilMisc.toMap("partyId", personPartyId, "firstName", firstName, "lastName", lastName));
            toBeStored.add(person);
            
        	toBeStored.addAll(UtilImport.makePartyWithRolesExt(personPartyId, "PERSON", entry.getString("rpIdentityno"),  UtilMisc.toList("CONTACT"), delegator));
        	Map<String, Object> partyRelationship = UtilMisc.toMap("partyIdTo", initialResponsiblePartyId, "roleTypeIdTo", initialResponsibleRoleTypeId, "partyIdFrom", personPartyId, "roleTypeIdFrom", "CONTACT", "partyRelationshipTypeId", "RESPONSIBLE_FOR", "fromDate", importTimestamp);
            partyRelationship.put("securityGroupId", "CONTACT_OWNER");
            toBeStored.add(delegator.makeValue("PartyRelationship", partyRelationship));
            	
            Debug.logInfo("Creating Person [" + personPartyId + "] for Customer [" + entry.getString("rpIdentityno") + "].", MODULE);
        }
        
        if (!UtilValidate.isEmpty(source)) {
            // make the party note
            if (personPartyId != null) {
                toBeStored.add(delegator.makeValue("PartyIdentification", UtilMisc.toMap( "partyId", personPartyId,"partyIdentificationTypeId",source,"idValue",entry.getString("rpIdentityno"))));
            }
           
        }
        
        toBeStored.add(entry);

        return toBeStored;
    }
    
    public static String getFirstValidRoleTypeId(String partyId, List<String> possibleRoleTypeIds, Delegator delegator) throws GenericEntityException {

        List<GenericValue> partyRoles = delegator.findByAnd("PartyRole", UtilMisc.toMap("partyId", partyId),null,false);

        // iterate across all possible roleTypeIds from the parameter
        for (String possibleRoleTypeId : possibleRoleTypeIds) {
            // try to look for each one in the list of PartyRoles
            for (GenericValue partyRole : partyRoles) {
                if (possibleRoleTypeId.equals(partyRole.getString("roleTypeId")))  {
                    return possibleRoleTypeId;
                }
            }
        }
        return null;
    }
}
