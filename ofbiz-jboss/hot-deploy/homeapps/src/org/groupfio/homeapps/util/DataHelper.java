/**
 * 
 */
package org.groupfio.homeapps.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;

/**
 * @author Sharif
 *
 */
public class DataHelper {
	
	private static String MODULE = DataHelper.class.getName();

	public static Map<String, Object> getLockboxStores(Delegator delegator) {
		
		Map<String, Object> lockboxStores = new LinkedHashMap<String, Object>();
		try {
			String defaultSalesChannelEnumId = "SPY_SALES_CHANNEL";
			List<GenericValue> asList = delegator.findAll("ProductStore", false);
			for(GenericValue productStore : asList){
				String storeName = "";
				if(UtilValidate.isNotEmpty(productStore)){
					//storeName = productStore.getString("storeName")+" ["+as.getString("productStoreId")+"]";
					storeName = productStore.getString("storeName");
				}
				
				lockboxStores.put(productStore.getString("productStoreId"), storeName);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Debug.logError(e, MODULE);
		}
		
		return lockboxStores;
	}
	
	public static Map getDropDownOptions(List<GenericValue> entityList, String keyField, String desField) {
		Map<String, Object> options = new LinkedHashMap<String, Object>();
		for (GenericValue entity : entityList) {
			options.put(entity.getString(keyField), entity.getString(desField));
		}
		return options;
	}
	
	public static Map getDropDownOptions(List<GenericValue> entityList, String keyField, String desField1, String desField2) {
		Map<String, Object> options = new LinkedHashMap<String, Object>();
		for (GenericValue entity : entityList) {
			if (UtilValidate.isNotEmpty(desField1) && UtilValidate.isNotEmpty(desField2)) {
				String desc = entity.getString(desField2);
				if (desc.length() > 10) {
					desc = desc.substring(0, 10) + "..";
				}
				options.put(entity.getString(keyField), "("+entity.getString(desField1)+") "+desc);
			} else {
				options.put(entity.getString(keyField), entity.getString(desField1));
			}
		}
		return options;
	}
	
	public static Map getLovDropDownOptions(Delegator delegator, String entityName, String primaryKey, String primaryKeyValue, String keyField, String desField){
		Map<String, Object> options = new LinkedHashMap<String, Object>();
		try {
			List<GenericValue> entityList = delegator.findByAnd(entityName, UtilMisc.toMap(primaryKey, primaryKeyValue), UtilMisc.toList("sequence"), false);
			options = org.groupfio.homeapps.util.DataHelper.getDropDownOptions(entityList, keyField, desField);
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return options;
	}
	
	public static Map getLovDropDownOptions(Delegator delegator, String entityName, String primaryKey, String primaryKeyValue, String keyField, String desField1, String desField2){
		Map<String, Object> options = new LinkedHashMap<String, Object>();
		try {
			List<GenericValue> entityList = delegator.findByAnd(entityName, UtilMisc.toMap(primaryKey, primaryKeyValue), UtilMisc.toList("sequence"), false);
			options = org.groupfio.homeapps.util.DataHelper.getDropDownOptions(entityList, keyField, desField1, desField2);
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return options;
	}
	
	public static Map getDayDropDownOptions() {
		Map<String, Object> options = new LinkedHashMap<String, Object>();
		for (int i = 1; i<=31; i++) {
			options.put(""+i, ""+i);
		}
		return options;
	}
	
	public static Map getSupplierList(Delegator delegator){
		
		Map<String, Object> suppliers = new LinkedHashMap<String, Object>();
		try {
			List<GenericValue> supplierList = delegator.findByAnd("PartyRole", UtilMisc.toMap("roleTypeId", "SUPPLIER"), null, false);
			for (GenericValue supplier : supplierList) {
				suppliers.put(supplier.getString("partyId"), PartyHelper.getPartyName(supplier) + " [" + supplier.getString("partyId") + "]");
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return suppliers;
		
	}
	
	public static Map getServiceList(Delegator delegator){
		
		Map<String, Object> services = new LinkedHashMap<String, Object>();
		try {
			List<GenericValue> serviceList = delegator.findByAnd("ServiceName", UtilMisc.toMap("componentName", "custom-field"), null, false);
			for (GenericValue service : serviceList) {
				services.put(service.getString("serviceName"), " [ " + service.getString("serviceName") + " ] - " + service.getString("description"));
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return services;
		
	}
	
	public static String getModelName(Delegator delegator, String modelId){
		try {
			
			GenericValue etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelId", modelId), null, false) );
			if (UtilValidate.isNotEmpty(etlModel)) {
				return etlModel.getString("modelName");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getGroupName(Delegator delegator, String groupId){
		try {
			
			GenericValue etlGroup = EntityUtil.getFirst( delegator.findByAnd("EtlGrouping", UtilMisc.toMap("groupId", groupId), null, false) );
			if (UtilValidate.isNotEmpty(etlGroup)) {
				return etlGroup.getString("groupName");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getServiceDescription(Delegator delegator, String serviceName, String componentName){
		try {
			
			if (UtilValidate.isEmpty(serviceName) || UtilValidate.isEmpty(componentName)) {
				return null;
			}
			
			GenericValue service = EntityUtil.getFirst( delegator.findByAnd("ServiceName", UtilMisc.toMap("serviceName", serviceName, "componentName", componentName), null, false) );
			if (UtilValidate.isNotEmpty(service)) {
				return service.getString("description");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<GenericValue> getAccountList(Delegator delegator, GenericValue userLogin){
		
		List<GenericValue> accounts = new ArrayList<GenericValue>();
		
		try {
			
			List<EntityCondition> accountConditions = new ArrayList<EntityCondition>();
    		EntityCondition accountRoleTypeCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"));
    		accountConditions.add(accountRoleTypeCondition);

			EntityCondition accountPartyStatusCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
			EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)), EntityOperator.OR);

			accountConditions.add(accountPartyStatusCondition);
			accountConditions.add(EntityUtil.getFilterByDateExpr());
    		
    		/*EntityCondition securityConditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("partyIdTo", EntityOperator.IN, lowerPositionPartyIds),
					EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
					EntityUtil.getFilterByDateExpr()
					);
    		
    		if (UtilValidate.isNotEmpty(userLogin)) {
				securityConditions = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, userLogin.getString("userLoginId")),
						securityConditions
					), EntityOperator.OR);
			}
    		
    		accountConditions.add(securityConditions);*/
    		
    		EntityCondition mainConditons = EntityCondition.makeCondition(accountConditions, EntityOperator.AND);
    		
    		EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			efo.getDistinct();
			
			Debug.logInfo("count 1 start: "+UtilDateTime.nowTimestamp(), MODULE);
			accounts = delegator.findList("PartyCommonView", mainConditons, UtilMisc.toSet("partyId", "groupName"), UtilMisc.toList("partyId"+ " " + "ASC"), efo, false);
			Debug.logInfo("count 2 start: "+UtilDateTime.nowTimestamp(), MODULE);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return accounts;
	}
	
}
