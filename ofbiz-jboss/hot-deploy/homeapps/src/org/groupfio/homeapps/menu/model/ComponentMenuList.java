package org.groupfio.homeapps.menu.model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.ofbiz.base.location.FlexibleLocation;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilXml;
import org.ofbiz.base.util.cache.UtilCache;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.widget.model.MenuFactory;
import org.ofbiz.widget.model.ModelMenu;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
/**
 * @author Mahendran T
 * @since 26-05-2018
 * */
public class ComponentMenuList {
	public static final String module = MenuFactory.class.getName();
	public static final UtilCache<String, Map<String, ModelMenu>> menuWebappCache = UtilCache.createUtilCache("widget.menu.webappResource", 0, 0, false);
    public static final UtilCache<String, Map<String, ModelMenu>> menuLocationCache = UtilCache.createUtilCache("widget.menu.locationResource", 0, 0, false);
    
    public static Map<String, ModelMenu> readMenuDocument(Document menuFileDoc, String menuLocation) {
        Map<String, ModelMenu> modelMenuMap = new HashMap<String, ModelMenu>();
        if (menuFileDoc != null) {
            // read document and construct ModelMenu for each menu element
            Element rootElement = menuFileDoc.getDocumentElement();
            Map<Integer, String> sortMap = new HashMap<Integer,String>();
            for (Element menuElement: UtilXml.childElementList(rootElement, "menu")){
                ModelMenu modelMenu = new ModelMenu(menuElement, menuLocation);
                String id = UtilValidate.isNotEmpty(modelMenu.getId())?modelMenu.getId() : "";
                
                if(UtilValidate.isNotEmpty(id)) {
                	int index = Integer.parseInt(id);
                    sortMap.put(index, modelMenu.getName());
                }

                modelMenuMap.put(modelMenu.getName(), modelMenu);
            }
            
            Map<String, ModelMenu> finalMenuMap = new LinkedHashMap<String, ModelMenu>();
            if(!sortMap.isEmpty()) {
            	sortMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEachOrdered(e->{
                		finalMenuMap.put(e.getValue(), modelMenuMap.get(e.getValue()));
                	
                });
            }
            
            if(UtilValidate.isNotEmpty(finalMenuMap)) {
            	return finalMenuMap;
            } 
         }
        return modelMenuMap;
    }
	 public static List<ModelMenu> getComponentMenuList(String resourceName) throws IOException, SAXException, ParserConfigurationException {
	        Map<String, ModelMenu> modelMenuMap = menuLocationCache.get(resourceName);
	        if (modelMenuMap == null) {
	            URL menuFileUrl = FlexibleLocation.resolveLocation(resourceName);
	            Document menuFileDoc = UtilXml.readXmlDocument(menuFileUrl, true, true);
	            modelMenuMap = readMenuDocument(menuFileDoc, resourceName);
	            menuLocationCache.putIfAbsent(resourceName, modelMenuMap);
	            modelMenuMap = menuLocationCache.get(resourceName);
	            //Debug.logInfo("Menus---->"+modelMenuMap, module);
	        }
	        List<ModelMenu> menuList = new ArrayList<ModelMenu>();
	        if (UtilValidate.isNotEmpty(modelMenuMap)) {
	        	/*modelMenuMap.entrySet().stream().forEach(e -> {
	        		menuList.add(e.getValue());
	        	});*/
	        	modelMenuMap.forEach((k,v)->{menuList.add(v);});
	        }
	        if (menuList.isEmpty()) {
	            throw new IllegalArgumentException("Could not find menu in location [" + resourceName + "]");
	        }
	        /*ModelMenu modelMenu = modelMenuMap.get(menuName);
	        if (modelMenu == null) {
	            throw new IllegalArgumentException("Could not find menu with name [" + menuName + "] in location [" + resourceName + "]");
	        }*/
	        //Debug.logInfo("Menu List from"+resourceName+"--->"+menuList, module);
	        return menuList;
	    }

     public static Map < String, Object> getComponentMenuList(DispatchContext dctx, Map < String, Object> context) {
         Map < String, Object> result = new HashMap < String, Object> ();
         Delegator delegator = (Delegator) dctx.getDelegator();
         GenericValue userLogin = (GenericValue) context.get("userLogin");
         String activeApp = (String) context.get("activeApp");
         List<Map < String, Object>> menuList = new LinkedList<Map < String, Object>> ();
         if (UtilValidate.isNotEmpty(activeApp) && userLogin != null && userLogin.size()> 0) {
             String userLoginId = (String) userLogin.getString("userLoginId");
             String partyId = (String) userLogin.getString("partyId");
             if (UtilValidate.isNotEmpty(userLoginId) && UtilValidate.isNotEmpty(partyId)) {
                 try {
                     List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup")
                         .where(EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId),
                        		 EntityCondition.makeCondition(EntityOperator.OR,
                        				 EntityCondition.makeCondition("groupId", EntityOperator.LIKE, "DBS_%"),
                        				 EntityCondition.makeCondition("groupId", EntityOperator.LIKE, "VND_%")
                     					)
                             )
                         .filterByDate().queryList();
                     if (userLoginSecurityGroup != null && userLoginSecurityGroup.size()> 0) {
                         List<GenericValue> securityGroupPermission = EntityQuery.use(delegator).from("SecurityGroupPermission")
                             .where(EntityCondition.makeCondition("groupId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(userLoginSecurityGroup, "groupId", true)))
                             .queryList();
                         
                         if (UtilValidate.isNotEmpty(securityGroupPermission)) {
                             List<String> permissionIdList = EntityUtil.getFieldListFromEntityList(securityGroupPermission, "permissionId", true);
                             if (permissionIdList != null && permissionIdList.size()> 0) {

                                 GenericValue ofbizComponentAccessQuery = EntityQuery.use(delegator).select("componentId").from("OfbizComponentAccess")
                                     .where(EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionIdList),
                                         EntityCondition.makeCondition("componentName", EntityOperator.EQUALS, activeApp))
                                     .orderBy("seqId").queryFirst();
                                 if (UtilValidate.isNotEmpty(ofbizComponentAccessQuery)) {
                                     String componentId = ofbizComponentAccessQuery.getString("componentId");
                                     if (UtilValidate.isNotEmpty(componentId)) {
                                         EntityQuery ofbizPageSecurityQuery = EntityQuery.use(delegator).select("tabId").from("OfbizPageSecurity")
                                             .where(EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionIdList),
                                                 EntityCondition.makeCondition("pageType", EntityOperator.EQUALS, "TAB"),
                                                 EntityCondition.makeCondition("componentId", EntityOperator.EQUALS, componentId)).orderBy("seqId");
                                         List<String> tabIdList = EntityUtil.getFieldListFromEntityList(ofbizPageSecurityQuery.queryList(), "tabId", true);
                                         if (UtilValidate.isNotEmpty(tabIdList)) {
                                             for (String tabId: tabIdList) {
                                                 Map < String, Object> menuMap = new HashMap < String, Object> ();
                                                 GenericValue ofbizPageSecurityGV = EntityQuery.use(delegator).select("tabId", "permissionId", "uiLabels", "pageId").from("OfbizPageSecurity")
                                                     .where(EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionIdList),
                                                         EntityCondition.makeCondition("pageType", EntityOperator.EQUALS, "TAB"),
                                                         EntityCondition.makeCondition("tabId", EntityOperator.EQUALS, tabId),
                                                         EntityCondition.makeCondition("componentId", EntityOperator.EQUALS, componentId))
                                                     .queryFirst();
                                                 if (UtilValidate.isNotEmpty(ofbizPageSecurityGV)) {
                                                     List<GenericValue> ofbizTabSecurityShortcutGV = EntityQuery.use(delegator).select("shortcutId", "permissionId", "uiLabels", "requestUri", "pageId", "parentShortcutId").from("OfbizTabSecurityShortcut")
                                                         .where(EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionIdList),
                                                             EntityCondition.makeCondition("pageType", EntityOperator.EQUALS, "SHORTCUT"),
                                                             EntityCondition.makeCondition("tabId", EntityOperator.EQUALS, tabId),
                                                             EntityCondition.makeCondition("componentId", EntityOperator.EQUALS, componentId))
                                                         .orderBy("seqId").queryList();
                                                     if (UtilValidate.isNotEmpty(ofbizTabSecurityShortcutGV)) {
                                                    	 
                                                         menuMap.put("tab", ofbizPageSecurityGV);
                                                         menuMap.put("shortcut", ofbizTabSecurityShortcutGV);
                                                         
                                                         for (GenericValue shortcut : ofbizTabSecurityShortcutGV) {
                                                        	 List<GenericValue> shortCutList = EntityQuery.use(delegator).select("shortcutId", "permissionId", "uiLabels", "requestUri", "pageId", "parentShortcutId").from("OfbizTabSecurityShortcut")
                                                                     .where(EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionIdList),
                                                                         EntityCondition.makeCondition("pageType", EntityOperator.EQUALS, "SHORTCUT"),
                                                                         EntityCondition.makeCondition("tabId", EntityOperator.EQUALS, tabId),
                                                                         EntityCondition.makeCondition("componentId", EntityOperator.EQUALS, componentId),
                                                                         EntityCondition.makeCondition("parentShortcutId", EntityOperator.EQUALS, shortcut.getString("shortcutId"))
                                                                         )
                                                                     .orderBy("seqId").queryList();
                                                        	 if (UtilValidate.isNotEmpty(shortCutList)) {
                                                        		 menuMap.put("shortcut#"+shortcut.getString("shortcutId"), shortCutList);
                                                        	 }
                                                         }
                                                         
                                                         menuList.add(menuMap);
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }

                 } catch (Exception e) {
                     Debug.logError("Exception in get menus " + e.getMessage(), module);
                 }
             }
         }
         result.put("menuList", menuList);
         return result;
     }
}
