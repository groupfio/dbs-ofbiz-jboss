<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<script>
   function updateData(count)
   {
                var hdrId = $("#hdrId_"+count).text();              
                $("#hdrId").val(hdrId);
                var hdrFileType = $("#hdrFileType_"+count).text();
                $("#hdrFileType").val(hdrFileType);
                var hdrChangeDt = $("#hdrChangeDt_"+count).text();
                $("#hdrChangeDt").val(hdrChangeDt);
                var hdrLastChangeDt = $("#hdrLastChangeDt_"+count).text();
                $("#hdrLastChangeDt").val(hdrLastChangeDt);
                var hdrLastId = $("#hdrLastId_"+count).text();
                $("#hdrLastId").val(hdrLastId);
                var isActive = $("#isActive_"+count).text();
                $("#isActive").val(isActive);
                var hdrName = $("#hdrName_"+count).text();
                $("#hdrName").val(hdrName);
                var hdrUiLabel = $("#hdrUiLabel_"+count).text();
                $("#hdrUiLabel").val(hdrUiLabel);
                var hdrRmVisible = $("#hdrRmVisible_"+count).val();
                $("#hdrRmVisible").val(hdrRmVisible);
                var hdrRmSeqNum = $("#hdrRmSeqNum_"+count).val();
                $("#hdrRmSeqNum").val(hdrRmSeqNum);
                var hdrCrmType = $("#hdrCrmType_"+count).val();
                $("#hdrCrmType").val(hdrCrmType);
                var hdrCrmSrcId = $("#hdrCrmSrcId_"+count).val();
                $("#hdrCrmSrcId").val(hdrCrmSrcId);
                var hdrCrmSrcField = $("#hdrCrmSrcField_"+count).val();
                $("#hdrCrmSrcField").val(hdrCrmSrcField);
                var hdrSrcKeyVal = $("#hdrSrcKeyVal_"+count).val();
                $("#hdrSrcKeyVal").val(hdrSrcKeyVal);
                var hdrInputSeq = $("#hdrInputSeq_"+count).val();
                $("#hdrInputSeq").val(hdrInputSeq);
                var hdrNewField = $("#hdrNewField_"+count).val();
                $("#hdrNewField").val(hdrNewField);
                var hdrKeyIndicator = $("#hdrKeyIndicator_"+count).val();
                $("#hdrKeyIndicator").val(hdrKeyIndicator);
                
               
                //jQuerry Ajax Request
                jQuery.ajax({
                    url: "updateDataMapping",
                    type: 'POST',
                    data: $('#updateHdrMapping').serialize(),
                    error: function(msg) {
                        alert("An error occurred loading content! : " + msg);
                    },
                    success: function(msg) {
                       $.notify({
                        message : '<p>Data updated successfully!</p>',
                      });
                    }
                });
               
               
                
   }
   
   function updateAll(){
        //jQuerry Ajax Request
    jQuery.ajax({
        url: "bulkUpdate",
        type: 'POST',
        data: $('#bulkUpdate').serialize(),
        error: function(msg) {
            showAlert ("error", msg);
        },
        success: function(msg) {
            showAlert ("success", "Updated successfully");
        }
    });
   }
   function executeDataImport(){
       var executeTypeId = $("#executeTypeId").val();
       if(executeTypeId != null && executeTypeId != "") {
    jQuery.ajax({
        url: "executeHadoopDataImport",
        type: 'POST',
        data:  {"executeType": executeTypeId},
        error: function(msg) {
            showAlert ("error", msg);
        },
        success: function(msg) {
            showAlert ("success", "Successfully execute service");
        }
    });
    } else {
       showAlert ("error", "Please select type");
    }
   }
</script>
<div class="page-header border-b">
   <h1 class="float-left" >Hadoop Key Structure Management</h1>
</div>
<div class="card-header mt-2 mb-3">
   <form method="post" class="form-horizontal" data-toggle="validator">
      <input type="hidden" name="performFindAction" value="Y">  
      <div class="row">
         <div class="col-md-2 col-sm-2">
            <@simpleDropdownInput 
            id="filterHdrFileType"
            options=hadoopHdrFiles
            required=false
            value=headerFilter.hdrFileType
            allowEmpty=true
            emptyText = "Hdr File Type"
            dataLiveSearch=true
            />
         </div>
         <div class="col-md-2 col-sm-2">
            <@simpleDropdownInput 
            id="filterHdrCrmType"
            options=findHdrCrmTypes
            required=false
            value=headerFilter.hdrCrmType
            allowEmpty=true
            emptyText = "Hdr Crm Type"
            dataLiveSearch=true
            />
         </div>
         <div class="col-md-2 col-sm-2">
            <@simpleInput 
            id="filterHdrName"
            placeholder="Hdr Name"
            value=headerFilter.hdrName
            required=false
            maxlength=255
            />
         </div>
         <div class="col-md-2 col-sm-2">
            <@simpleDropdownInput 
            id="filterHdrRmVisible"
            options=yesNoOptions
            required=false
            value=headerFilter.hdrRmVisible
            allowEmpty=true
            emptyText = "Hdr Rm Visible"
            dataLiveSearch=true
            />
         </div>
         <@fromSimpleAction id="" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
<div class="page-header">
   <div class="row">
      <div class="col-md-6">
         <h2>List Header Mapping </h2>
      </div>
      <div class="col-md-6">
         <h2 class="float-right">
            <select id="executeTypeId" name="executeTypeId" class="ui dropdown search input-sm">
               <option value> Select Job Type</option>
               <#--
               <option value="ALL">All</option>
               -->
               <option value="CUSTOM_FIELD">Attribute</option>
               <option value="SEGMENTATION">Segmentation</option>
               <option value="ECONOMIC_METRIC">Economic Metric</option>
            </select>
            <button value="submit" class="btn btn-primary btn-xs" onclick="executeDataImport();">Execute</button>
            <#if resp?has_content>
            <button value="submit" class="btn btn-primary btn-xs" onclick="updateAll();">Update all</button>
            </#if>
         </h2>
      </div>
   </div>
</div>
<div class="row-fluid table-responsive">
   <form name="bulkUpdate" id="bulkUpdate" action="bulkUpdate">
      <table id="heading-mapping-list"  class="table table-striped">
         <thead>
            <tr>
               <th>Hdr File Type</th>
               <th>Hdr Id</th>
               <th style="display:none;">Hdr File Type</th>
               <#-- 
               <th>Hdr Change Date</th>
               -->
               <th>Hdr Last Change Date</th>
               <#-- 
               <th>Hdr Last Id</th>
               -->
               <th>is Active</th>
               <th>Hdr Name</th>
               <th>Hdr Friendly Name</th>
               <th>Hdr Rm Visible</th>
               <th>Hdr Rm Sequence Number</th>
               <th>Hdr Crm Type</th>
               <th>Hdr Crm Src Id</th>
               <th>Hdr Crm Src Field</th>
               <th>Hdr Src Key Value</th>
               <th>Hdr Input Sequence</th>
               <#-- 
               <th>Hdr New Field</th>
               -->
               <th>Hdr Key Indicator</th>
               <th class="text-center">Action</th>
            </tr>
         </thead>
         <tbody>
            <#if resp?has_content>
            <#assign count = 0>
            <#list resp as res>
            <#assign hdrCrmSrcId = "">
            <#assign hdrCrmSrcField = "">
            <#assign hdrSrcKeyVal = "">
            <#if res.hdrCrmType?has_content && (res.hdrCrmType == "SEGMENTATION" || res.hdrCrmType == "ECONOMIC_METRIC")>
            <#assign hdrCrmSrcId = res.groupingCodeName!>
            <#assign hdrCrmSrcField = res.groupName!>
            <#assign hdrSrcKeyVal = res.customFieldName!>
            <#elseif res.hdrCrmType?has_content && (res.hdrCrmType == "CUSTOM_FIELD")>
            <#assign hdrCrmSrcId = res.groupName!>
            <#assign hdrCrmSrcField = res.customFieldName!>
            </#if>
            <#assign count = count+1>
            <tr class=" <#if res.hdrKeyIndicator?has_content && res.hdrKeyIndicator == 'Y'>key-indicator</#if> ">
               <td>${res.hdrFileType!}</td>
               <td id="hdrId_${count!}"><input type="hidden" name="hdrId_${count!}" value="${res.hdrId!}">${res.hdrId!}</td>
               <td  style="display:none;" id="hdrFileType_${count!}"><input type="hidden" name="hdrFileType_${count!}" value="${res.hdrFileType!}">${res.hdrFileType!}</td>
               <#-- 
               <td id="hdrFileType_${count!}"><input type="hidden" name="hdrFileType_${count!}" value="${res.hdrChangeDt!}">${res.hdrChangeDt!}</td>
               -->
               <td id="hdrLastChangeDt_${count!}"><input type="hidden" name="hdrLastChangeDt_${count!}" value="${res.hdrLastChangeDt!}">${res.hdrLastChangeDt!}</td>
               <#-- 
               <td id="hdrLastId_${count!}"><input type="hidden" name="hdrLastId_${count!}" value="${res.hdrLastId!}">${res.hdrLastId!}</td>
               -->
               <td id="isActive_${count!}"><input type="hidden" name="isActive_${count!}" value="${res.isActive!}">${res.isActive!}</td>
               <td id="hdrName_${count!}"><input type="hidden" name="hdrName_${count!}" value="${res.hdrName!}">${res.hdrName!}</td>
               <td id="hdrUiLabel_${count!}">
                  <input type="text" name="hdrUiLabel_${count!}" value="${res.hdrUiLabel!}" class="form-group form-control input-sm">
               </td>
               <td>
                  <@simpleDropdownInput 
                  id="hdrRmVisible_${count!}"
                  options=yesNoOptions
                  required=false
                  value=res.hdrRmVisible
                  allowEmpty=true
                  emptyText = "Rm Visible"
                  dataLiveSearch=true
                  class=""
                  />
               </td>
               <td><input type="text" id="hdrRmSeqNum_${count!}"  name="hdrRmSeqNum_${count!}" value="${res.hdrRmSeqNum!}" class="form-group form-control input-sm"></td>
               <td>
                  <span id="hdrCrmType_${count!}">
                  <#if res.hdrCrmType?has_content && res.hdrCrmType == "STANDARD">
                  ${uiLabelMap.standard!}
                  <#elseif res.hdrCrmType?has_content && res.hdrCrmType == "CUSTOM_FIELD">
                  ${uiLabelMap.attribute!}
                  <#elseif res.hdrCrmType?has_content && res.hdrCrmType == "SEGMENTATION">
                  ${uiLabelMap.segmentation!}
                  <#elseif res.hdrCrmType?has_content && res.hdrCrmType == "ECONOMIC_METRIC">
                  ${uiLabelMap.economicMetric!}
                  </#if>
                  </span>
                  <#-- <@simpleDropdownInput 
                  id="hdrCrmType_${count!}"
                  options=hdrCrmTypes
                  required=false
                  value=res.hdrCrmType
                  allowEmpty=true
                  emptyText = "Type"
                  dataLiveSearch=false
                  class="hdrCrmType"
                  dataAttribute="data-count='${count!}'"
                  /> -->
               </td>
               <td>
                  <span id="hdrCrmSrcId_${count!}">${hdrCrmSrcId!}</span>
                  <#-- <@simpleDropdownInput 
                  id="hdrCrmSrcId_${count!}"
                  required=false
                  value=res.hdrCrmSrcId
                  allowEmpty=true
                  emptyText = "Src Id"
                  dataLiveSearch=false
                  class=""
                  />-->
               </td>
               <td>
                  <span id="hdrCrmSrcField_${count!}">${hdrCrmSrcField!}</span>
                  <#-- <@simpleDropdownInput 
                  id="hdrCrmSrcField_${count!}"
                  required=false
                  value=res.hdrCrmSrcField
                  allowEmpty=true
                  emptyText = "Src Field"
                  dataLiveSearch=false
                  class=""
                  /> -->
                  <#--  <input type="text" id="hdrCrmSrcField_${count!}" name="hdrCrmSrcField_${count!}" value="${res.hdrCrmSrcField!}"  class="form-group  form-control input-sm">-->
               </td>
               <td>
                  <span id="hdrSrcKeyVal_${count!}">${hdrSrcKeyVal!}</span>
                  <#-- <@simpleDropdownInput 
                  id="hdrSrcKeyVal_${count!}"
                  required=false
                  value=res.hdrSrcKeyVal
                  allowEmpty=true
                  emptyText = "Src Value"
                  dataLiveSearch=false
                  class=""
                  /> -->
                  <#-- <input type="text"  id="hdrSrcKeyVal_${count!}" name="hdrSrcKeyVal_${count!}" value="${res.hdrSrcKeyVal!}"  class="form-group  form-control input-sm"> -->
               </td>
               <td id="hdrInputSeq_${count!}" name="hdrInputSeq_${count!}">${res.hdrInputSeq!}
                  <input type="hidden" name="hdrInputSeq_${count!}" value="${res.hdrInputSeq!}">
               </td>
               <#-- 
               <td><input type="text" id="hdrNewField_${count!}" name="hdrNewField_${count!}" value="${res.hdrNewField!}" class="form-group  form-control input-sm"></td>
               -->
               <td>
                  ${res.hdrKeyIndicator!}
                  <#-- 
                  <@simpleDropdownInput 
                  id="hdrKeyIndicator_${count!}"
                  options=hdrKeyIndicator
                  required=false
                  value=res.hdrKeyIndicator
                  allowEmpty=true
                  emptyText = "Hdr key indicator"
                  dataLiveSearch=false
                  class=""
                  />
                  -->               
               </td>
               <#-- 
               <td><button value="submit" class="btn btn-primary btn-xs" onclick="updateData('${count!}')">Update</button></td>
               -->
               <td class="text-center">
                  <div class="">
                     <#if !res.hdrKeyIndicator?has_content || res.hdrKeyIndicator != 'Y'>
                     <a id="configure-header-${count!}" href="#" class="btn btn-xs btn-info tooltips configure-header" 
                        data-index="${count!}" data-hdrName="${res.hdrName!}" data-hdrId="${res.hdrId!}" data-hdrFileType="${res.hdrFileType!}" data-hdrInputSeq="${res.hdrInputSeq!}"
                        data-hdrCrmType="${res.hdrCrmType!}" data-hdrCrmSrcId="${res.hdrCrmSrcId!}" data-hdrCrmSrcField="${res.hdrCrmSrcField!}" data-hdrSrcKeyVal="${res.hdrSrcKeyVal!}" 
                        data-original-title="Configuration"><i class="fa fa-cog info"></i></a>
                     </#if>
                  </div>
               </td>
            </tr>
            </#list>
            </#if>
         </tbody>
      </table>
      <input type="hidden" name="cntInformation" id="cntInformation" value="${count!}" />
   </form>
</div>
<form id="updateHdrMapping">
   <input type="hidden" id="hdrId"  name="hdrId">
   <input type="hidden" id="hdrFileType" name="hdrFileType">
   <input type="hidden" id="hdrChangeDt" name="hdrChangeDt">
   <input type="hidden" id="hdrLastChangeDt" name="hdrLastChangeDt">
   <input type="hidden" id="hdrLastId" name="hdrLastId">
   <input type="hidden" id="isActive" name="isActive">
   <input type="hidden" id="hdrName" name="hdrName">
   <input type="hidden" id="hdrUiLabel" name="hdrUiLabel">
   <input type="hidden" id="hdrRmVisible" name="hdrRmVisible">
   <input type="hidden" id="hdrRmSeqNum" name="hdrRmSeqNum">
   <#-- 
   <input type="hidden" id="hdrCrmType" name="hdrCrmType">
   <input type="hidden" id="hdrCrmSrcId" name="hdrCrmSrcId">
   <input type="hidden" id="hdrCrmSrcField" name="hdrCrmSrcField">
   <input type="hidden" id="hdrSrcKeyVal" name="hdrSrcKeyVal">
   -->
   <input type="hidden" id="hdrInputSeq" name="hdrInputSeq">
   <input type="hidden" id="hdrNewField" name="hdrNewField">
   <input type="hidden" id="hdrKeyIndicator" name="hdrKeyIndicator">
</form>
<div id="modalHeaderConfigView" class="modal fade" >
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h2 class="modal-title">Configuration for [ <span id="configure-value-title"></span> ]</h2>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="clearfix"></div>
            <#-- 
            <div class="page-header">
               <h2 class="float-left"></h2>
               <div class="float-right">
               </div>
            </div>
            -->
            <div class="row padding-r">
               <div class="col-md-6 col-sm-6">
                  <div class="portlet-body form">
                     <form id="headerConfigForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
                        <input type="hidden" name="hdrId" value="">
                        <input type="hidden" name="hdrFileType" value="">
                        <input type="hidden" name="hdrInputSeq" value="">   
                        <div class="form-body">
                           <@dropdownInput 
                           id="hdrCrmType"
                           label="Hdr Crm Type"
                           options=hdrCrmTypes
                           required=true
                           allowEmpty=true
                           dataLiveSearch=true
                           />       
                           <@dropdownInput 
                           id="hdrCrmSrcId"
                           label="Hdr Src Id"
                           required=true
                           allowEmpty=true
                           dataLiveSearch=true
                           />
                           <@dropdownInput 
                           id="hdrCrmSrcField"
                           label="Hdr Src Field"
                           required=true
                           allowEmpty=true
                           dataLiveSearch=true
                           />   
                           <@dropdownInput 
                           id="hdrSrcKeyVal"
                           label="Hdr Src Value"
                           required=false
                           allowEmpty=true
                           dataLiveSearch=true
                           />                                       
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-secondary resetBtn">Clear</button>
            <button id="updateHeaderConfigBtn" type="button" class="btn btn-sm btn-primary navbar-dark">Update</button>
         </div>
      </div>
   </div>
</div>
</div>
<script>
   var hdrCrmType = "";
   var hdrCrmSrcId = "";
   var hdrCrmSrcField = "";
   var hdrSrcKeyVal = "";   
   var justOpenConfigWindow = false;
   
   var index = 0;
   
   $(document).ready(function() {
   
   $('#heading-mapping-list').DataTable({
    "order": [],
    "fnDrawCallback": function( oSettings ) {
            resetDefaultEvents();
            resetHeaderConfigEvents();
    }
   });
    
   resetHeaderConfigEvents();
   
   $('#modalHeaderConfigView').on('shown.bs.modal', function (e) {
   
    if (hdrCrmType) {
            $('#hdrCrmType').val( hdrCrmType );
        } else {
            $('#hdrCrmType').dropdown("clear");
        }
        
        $('#hdrCrmSrcId').dropdown("clear");
        $('#hdrCrmSrcField').dropdown("clear");
        $('#hdrSrcKeyVal').dropdown("clear");
        
        justOpenConfigWindow = true;
   
        $('.ui.dropdown').dropdown('refresh');
        $( "#hdrCrmType" ).trigger( "change" );
   });
   
   $('#updateHeaderConfigBtn').on('click', function(){
    
    var hdrCrmType = $("#hdrCrmType").val();
    
    if ($("#hdrCrmType").val() && (!$("#hdrCrmSrcId").val() || !$("#hdrCrmSrcField").val())) {
        return false;
    }
    
    if ($("#hdrCrmType").val() && $("#hdrCrmType").val() == "ECONOMIC_METRIC" && !$("#hdrSrcKeyVal").val()) {
        showAlert ("error", "Hdr Src Value required for Economic Metric");
        return false;
    }
    
    $.post('updateHeaderConfig', $('#headerConfigForm').serialize(), function(returnedData) {
    
        if (returnedData.code == 200) {
            
            showAlert ("success", returnedData.message)
            
            $("#hdrCrmType_"+index).html( $("#hdrCrmType").val() ? $("#hdrCrmType option:selected").text() : "" );
            $("#hdrCrmSrcId_"+index).html( $("#hdrCrmSrcId").val() ? $("#hdrCrmSrcId option:selected").text() : "" );
            $("#hdrCrmSrcField_"+index).html( $("#hdrCrmSrcField").val() ? $("#hdrCrmSrcField option:selected").text() : "" );
            
            $("#hdrSrcKeyVal_"+index).html( $("#hdrSrcKeyVal").val() ? $("#hdrSrcKeyVal option:selected").text() : "" );
            
            $('#headerConfigForm')[0].reset();
            $('#modalHeaderConfigView').modal("hide");
            $('.selectpicker').selectpicker('refresh');
            
        } else {
            showAlert ("error", returnedData.message)
        }
            
    });
    
    $('#configure-header-'+index).attr("data-hdrCrmType", $("#hdrCrmType").val());
    $('#configure-header-'+index).attr("data-hdrCrmSrcId", $("#hdrCrmSrcId").val());
    $('#configure-header-'+index).attr("data-hdrCrmSrcField", $("#hdrCrmSrcField").val());
    $('#configure-header-'+index).attr("data-hdrSrcKeyVal", $("#hdrSrcKeyVal").val());
                                                                                                    
   });
   
   $('.resetBtn').on('click', function(){
   
    $('#headerConfigForm')[0].reset();
    
    $('#hdrCrmType').dropdown("clear");
        $('#hdrCrmSrcId').dropdown("clear");
        $('#hdrCrmSrcField').dropdown("clear");
        $('#hdrSrcKeyVal').dropdown("clear");
   
   });
       
   });
   
   function resetHeaderConfigEvents() {
   
    $('.configure-header').unbind( "click" );
    $('.configure-header').bind( "click", function( event ) {
   
        event.preventDefault(); 
        
        $('#modalHeaderConfigView').modal("show");
        
        index = $(this).attr("data-index");
        
        hdrName = $(this).attr("data-hdrName");
        hdrId = $(this).attr("data-hdrId");
        hdrFileType = $(this).attr("data-hdrFileType");
        hdrInputSeq = $(this).attr("data-hdrInputSeq");
        
        $('#configure-value-title').html( hdrName );        
        
        $('#headerConfigForm input[name="hdrId"]').val( hdrId );
        $('#headerConfigForm input[name="hdrFileType"]').val( hdrFileType );
        $('#headerConfigForm input[name="hdrInputSeq"]').val( hdrInputSeq );    
        
        hdrCrmType = $(this).attr("data-hdrCrmType");
        hdrCrmSrcId = $(this).attr("data-hdrCrmSrcId");
        hdrCrmSrcField = $(this).attr("data-hdrCrmSrcField");
        hdrSrcKeyVal = $(this).attr("data-hdrSrcKeyVal");   
                                                                                                                                                                                                        
    });
    
    $(".key-indicator :input").attr("disabled", true);
       
       $('.hdrCrmType').unbind( "change" );
    $('.hdrCrmType').bind( "change", function( event ) {
        event.preventDefault(); 
        //var index = $(this).attr('data-count');
        var hdrCrmType = $("#hdrCrmType").val();
        
        if (!justOpenConfigWindow) {
            hdrCrmSrcId = "";
            hdrCrmSrcField = "";
            hdrSrcKeyVal = "";  
            
            $('#hdrCrmSrcId').dropdown("clear");
            $('#hdrCrmSrcField').dropdown("clear");
            $('#hdrSrcKeyVal').dropdown("clear");
        } else {
            justOpenConfigWindow = false;
        }
        
        if (hdrCrmType && (hdrCrmType == "SEGMENTATION" || hdrCrmType == "ECONOMIC_METRIC")){
            loadGroupingCodeList("#hdrCrmSrcId", hdrCrmType, hdrCrmSrcId);
            $(".hdrSrcKeyVal").show();
        }
        
        if (hdrCrmType && (hdrCrmType == "CUSTOM_FIELD")){
            var params = {"groupType": hdrCrmType};
            loadCustomFieldGroupList("#hdrCrmSrcId", params, hdrCrmSrcId);
            $(".hdrSrcKeyVal").hide();
        }
        
    });
    
    $('.hdrCrmSrcId').unbind( "change" );
    $('.hdrCrmSrcId').bind( "change", function( event ) {
        event.preventDefault(); 
        //var index = $(this).attr('data-count');
        var hdrCrmType = $("#hdrCrmType").val();
        
        if (hdrCrmType && (hdrCrmType == "SEGMENTATION" || hdrCrmType == "ECONOMIC_METRIC")){
            var params = {"customFieldGroupingCodeId": $("#hdrCrmSrcId").val(), "isActive": "Y"};
            loadCustomFieldGroupList("#hdrCrmSrcField", params, hdrCrmSrcField);
        }
        
        if (hdrCrmType && (hdrCrmType == "CUSTOM_FIELD")){
            var params = {"groupId": $("#hdrCrmSrcId").val()};
            loadCustomFieldList("#hdrCrmSrcField", params, hdrCrmSrcField);
        }
        
    });
    
    $('.hdrCrmSrcField').unbind( "change" );
    $('.hdrCrmSrcField').bind( "change", function( event ) {
        event.preventDefault(); 
        //var index = $(this).attr('data-count');
        var hdrCrmType = $("#hdrCrmType").val();
        
        if (hdrCrmType && (hdrCrmType == "SEGMENTATION" || hdrCrmType == "ECONOMIC_METRIC")){
            var params = {"groupId": $("#hdrCrmSrcField").val()};
            loadCustomFieldList("#hdrSrcKeyVal", params, hdrSrcKeyVal);
        }
        
    });
   }
   
   function loadGroupingCodeList(targetInputId, hdrCrmType, selectedValue) {
    var options = '<option value="">Please Select</option>';        
        
    $.ajax({
              
        type: "POST",
            url: "getCustomFieldGroupingCodes",
           data:  {"groupType": hdrCrmType},
           async: false,
           success: function (data) {   
               
               if (data.code == 200) {
               
                for (var i = 0; i < data.groupingCodes.length; i++) {
                    var groupingCode = data.groupingCodes[i];
                    options += '<option value="'+groupingCode.customFieldGroupingCodeId+'">'+groupingCode.groupingCode+'</option>';
                }
                
               }
                    
           }
           
    });    
        
    $(targetInputId).html( options );
    $(targetInputId).val( selectedValue );
    if ( selectedValue ) {
        $( targetInputId ).trigger( "change" );
    }
    
    $(targetInputId).dropdown('refresh');
   }
   
   function loadCustomFieldGroupList(targetInputId, params, selectedValue) {
    var options = '<option value="">Please Select</option>';        
        
    $.ajax({
              
        type: "POST",
            url: "getCustomFieldGroups",
           data: params,
           async: false,
           success: function (data) {   
               
               if (data.code == 200) {
               
                for (var i = 0; i < data.groups.length; i++) {
                    var group = data.groups[i];
                    options += '<option value="'+group.groupId+'">'+group.groupName+'</option>';
                }
                
               }
                    
           }
           
    });    
        
    $(targetInputId).html( options );
    $(targetInputId).val( selectedValue );
    if ( selectedValue ) {
        $( targetInputId ).trigger( "change" );
    }
    
    $(targetInputId).dropdown('refresh');
   }
   
   function loadCustomFieldList(targetInputId, params, selectedValue) {
    var options = '<option value="">Please Select</option>';        
        
    $.ajax({
              
        type: "POST",
            url: "getCustomFields",
           data: params,
           async: false,
           success: function (data) {   
               
               if (data.code == 200) {
               
                for (var i = 0; i < data.fields.length; i++) {
                    var field = data.fields[i];
                    options += '<option value="'+field.customFieldId+'">'+field.customFieldName+'</option>';
                }
                
               }
                    
           }
           
    });    
        
    $(targetInputId).html( options );
    
    $(targetInputId).val( selectedValue );
    
    $(targetInputId).dropdown('refresh');
   }
   
</script>