<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h2>${uiLabelMap.employeeHierarchyErrorLogs}</h2>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findLogForm" class="form-horizontal" name="findLogForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	<div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="fromDate"
				tooltip="From Date"
				dateFormat="DD-MM-YYYY"
				/>
         </div>
         <div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="thruDate"
				tooltip="Thru Date"
				dateFormat="DD-MM-YYYY"
				/>
         </div>
         
         <@fromSimpleAction id="find-log-button" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
<#-- 
<input type="hidden" id="customFieldId" value="${customFieldId!}"/>
<input type="hidden" id="groupId" value="${groupId!}"/>
 -->
<div class="clearfix"> </div>
<div class="page-header mt-2 mb-2 nav-tabs">
	<h2 class="float-left ml-1">Log List </h2>
	<div class="float-right">
		<#-- <input class="btn btn-xs btn-primary mt-2 mr-1" id="add-selected-customer-button" value="Add Selected Customers" type="button"> -->
	</div>
</div>

<div class="table-responsive">
	<table id="position-log-list" class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
                <th>Status</th>
                <th>Message</th>
                <th>Processed Time</th>
                <#-- <th><div class="ml-1"><input id="add-select-all-customer" type="checkbox"></div></th> -->
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

jQuery(document).ready(function() {   

$(".form_datetime").datetimepicker({
    //autoclose: true,
    //isRTL: BootStrapInit.isRTL(),
    //format: "dd MM yyyy - hh:ii",
    //pickerPosition: (BootStrapInit.isRTL() ? "bottom-right" : "bottom-left")
    useCurrent: false
});

$('#find-log-button').on('click', function(){
	findPositionImportLogs();
});

});

findPositionImportLogs();
function findPositionImportLogs() {
	
	var fromDate = $('#findLogForm input[name="fromDate"]').val();
	var thruDate = $('#findLogForm input[name="thruDate"]').val();
   	
   	var url = "searchEmplPositionImportLogs?fromDate="+fromDate+"&thruDate="+thruDate;
   
	$('#position-log-list').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 10,
	        "stateSave": false,
	              
	        "columns": [
					        	
	            { "data": "seqId" },
	            { "data": "status" },
	            { "data": "logMsg1" },
	            
	            { "data": "timeStamp" },
	            
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}

</script>
