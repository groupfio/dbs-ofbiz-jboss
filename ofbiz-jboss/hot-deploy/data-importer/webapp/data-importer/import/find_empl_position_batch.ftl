<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h1 class="float-left">${uiLabelMap.FindEmplPositionBatch!}</h1>
	<div class="float-right">
		
	</div>
</div>

<div class="card-header mt-2 mb-3">
   <form id="findEmplPositionBatchForm" method="post" class="form-horizontal" data-toggle="validator">
   		
   		<input type="hidden" name="activeTab" value="economicsMetrics" />	
   		
      <div class="row">
      
      	<div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="dataImportEmplPositionId"
				placeholder=uiLabelMap.sequenceId
				value=filterEmplPositionBatch.dataImportEmplPositionId
				required=false
				maxlength=20
				/>
         </div>
      	<div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="importStatusId"
				options=importStatusList
				required=false
				value=filterEmplPositionBatch.importStatusId
				allowEmpty=true
				tooltip = uiLabelMap.status
				emptyText = uiLabelMap.status
				dataLiveSearch=true
				/>
         </div>
      	<div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="fromDate"
				tooltip="From Date"
				/>
         </div>
         <div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="thruDate"
				tooltip="Thru Date"
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="batchId"
				placeholder=uiLabelMap.batchId
				value=filterEmplPositionBatch.batchId
				required=false
				maxlength=20
				/>
         </div>
		<div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="uploadedByUserLoginId"
				placeholder=uiLabelMap.uploadedBy
				value=filterEmplPositionBatch.uploadedBy
				required=false
				maxlength=100
				/>
         </div>  
         
         <@fromSimpleAction id="find-emplPosition-button" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<div class="clearfix"> </div>
<div class="page-header mt-2 mb-2 nav-tabs">
	<h2 class="float-left ml-1">Batch List </h2>
	<div class="float-right">
		
	</div>
</div>

<div class="table-responsive">
	<table class="table table-hover" id="list-emplPosition-batch-processor">
	<thead>
	<tr>
		<th></th>
		<th>${uiLabelMap.batchId!}</th>
		<th>${uiLabelMap.uploadedDate!}</th>
		<th>${uiLabelMap.uploadedBy!}</th>
		<th>${uiLabelMap.importedRecords!}</th>
		<th>${uiLabelMap.errorRecords!}</th>
	</tr>
	</thead>
	<tbody>
		
	</tbody>
	</table>
</div>

<script type="text/javascript">

var emplPositionBatchProcessorGrid;

jQuery(document).ready(function() {	

$('#find-emplPosition-button').on('click', function(){
	findBatchEmplPositionProcessors();
});

$(".form_datetime").datetimepicker({
    //autoclose: true,
    //isRTL: BootStrapInit.isRTL(),
    //format: "dd MM yyyy - hh:ii",
    //pickerPosition: (BootStrapInit.isRTL() ? "bottom-right" : "bottom-left")
});

});

function resetBatchEmplPositionProcessorEvents() {
	
	$('#list-emplPosition-batch-processor td.details-control').unbind( "click" );
	$('#list-emplPosition-batch-processor td.details-control').bind( "click", function( event ) {
		
        var tr = $(this).closest('tr');
        var row = emplPositionBatchProcessorGrid.row( tr );
     
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            var batchId = row.data()['batchId'];
            var subtable_id = "subtable-"+batchId;
            row.child(prepareBatchEmplPositionGrid(subtable_id)).show(); /* HERE I format the new table */
            tr.addClass('shown');
            findBatchEmplPositions(batchId, subtable_id); /*HERE I was expecting to load data*/
        }
    });
    
}

function prepareBatchEmplPositionGrid ( table_id ) {
    // `d` is the original data object for the row
    return '<div class="page-header ml-4 mr-4"><h2 class="float-left display-4">Employee Position List</h2></div>' + 
    '<table id="'+table_id+'" class="table table-striped">'+
    '<thead>'+
    '<th>${uiLabelMap.sequenceId!}</th>'+
    '<th>${uiLabelMap.reportingPositionType!}</th>'+
    '<th>${uiLabelMap.managedByPositionType!}</th>'+
    '<th>${uiLabelMap.reportingMemberName!}</th>'+
    '<th>${uiLabelMap.mangedMemberName!}</th>'+
    '<th>${uiLabelMap.reporting1bankid!}</th>'+
    '<th>${uiLabelMap.managed1bankid!}</th>'+
    '<th>${uiLabelMap.reportingEmail!}</th>'+
    '<th>${uiLabelMap.mangedEmail!}</th>'+
    '<th>${uiLabelMap.teamId!}</th>'+
    '<th>${uiLabelMap.errorCode!}</th>'+
    '<th>${uiLabelMap.status!}</th>'+
    '</thead>'+
    '</table>';
}

function findBatchEmplPositions(batchId, subTableId) {
	
	var importStatusId = $("#importStatusId").val();
	var dataImportEmplPositionId = $('#dataImportEmplPositionId').val();
	//var batchId = $("#batchId").val();
	var uploadedByUserLoginId = $("#uploadedByUserLoginId").val();
	
	var fromDate = $('#findEmplPositionBatchForm input[name="fromDate"]').val();
	var thruDate = $('#findEmplPositionBatchForm input[name="thruDate"]').val();
   	
   	var url = "searchBatchEmplPositions?importStatusId="+importStatusId+"&fromDate="+fromDate+"&thruDate="+thruDate+"&dataImportEmplPositionId="+dataImportEmplPositionId+"&batchId="+batchId+"&uploadedByUserLoginId="+uploadedByUserLoginId;
   
   	var actionColumnIndex = 4;
   	
	$('#'+subTableId).DataTable({
	    "processing": true,
	    "serverSide": true,
	    "destroy": true,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "pageLength": 10,
        "stateSave": false,
        
        "columnDefs": [ 
        	{
				"targets": 11,
				"orderable": false
			} 
		],
				      
        "columns": [
			
			{ "data": "dataImportEmplPositionId" },
			{ "data": "reportingPositionTypeName" },
			{ "data": "managedByPositionTypeName" },
			{ "data": "reportingMemberName" },
			{ "data": "mangedMemberName" },
			{ "data": "reporting1bankid" },
			{ "data": "managed1bankid" },
			{ "data": "reportingEmail" },
			{ "data": "mangedEmail" },
			{ "data": "teamName" },
			{ "data": "dataImportEmplPositionId",
	          "render": function(data, type, row, meta){
	          	var data = '';
	            if(type === 'display'){
	            	for (var key in row.codeList) {
	            		data += '<span class="tooltips" data-html="true" data-original-title="'+row.codeList[key]+'">['+key+']</span> ';
	            	}		
	            }
	            return data;
	          }
	         },  
            { "data": "importStatusName" },	       
            
        ],
        "fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	
}

findBatchEmplPositionProcessors();
function findBatchEmplPositionProcessors() {
	
	var importStatusId = $("#importStatusId").val();
	var dataImportEmplPositionId = $('#dataImportEmplPositionId').val();
	var batchId = $("#batchId").val();
	var uploadedByUserLoginId = $("#uploadedByUserLoginId").val();
	
	var fromDate = $('#findEmplPositionBatchForm input[name="fromDate"]').val();
	var thruDate = $('#findEmplPositionBatchForm input[name="thruDate"]').val();
   	
   	var url = "searchBatchEmplPositionProcessors?importStatusId="+importStatusId+"&fromDate="+fromDate+"&thruDate="+thruDate+"&dataImportEmplPositionId="+dataImportEmplPositionId+"&batchId="+batchId+"&uploadedByUserLoginId="+uploadedByUserLoginId;
   
   	var actionColumnIndex = 8;
   	
	emplPositionBatchProcessorGrid = $('#list-emplPosition-batch-processor').DataTable({
	    "processing": true,
	    "serverSide": true,
	    "destroy": true,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "pageLength": 20,
        "stateSave": false,
        
        "columnDefs": [ 
        	{
				"targets": 4,
				"orderable": false
			},
        	{
				"targets": 5,
				"orderable": false
			}, 
		],
		"order": [[ 2, "desc" ]],
		    
        "columns": [
			
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "batchId" },
            { "data": "createdStamp" },
	        { "data": "batchId",
	          "render": function(data, type, row, meta){
	          	var data = '';
	            if(type === 'display'){
	                data = '<div class="text-left ml-1" ><a href="javascript: findEmplPositionByUploadedBy(\''+row.createdBy+'\')" class="btn btn-xs btn-primary m5 tooltips"><strong>'+row.createdBy+'</strong></a></div>';
	            }
	            return data;
	          }
	         },
	         { "data": "importedCount" },
	         { "data": "errorCount" },
            
        ],
        "fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
      		resetBatchEmplPositionProcessorEvents();
    	}
	});
	
}

function findEmplPositionByBatchId(batchId) {
	$('#batchId').val( batchId );
	findBatchEmplPositionProcessors();
}

function findEmplPositionByUploadedBy(uploadedByUserLoginId) {
	$('#uploadedByUserLoginId').val( uploadedByUserLoginId );
	findBatchEmplPositionProcessors();
}

</script>

