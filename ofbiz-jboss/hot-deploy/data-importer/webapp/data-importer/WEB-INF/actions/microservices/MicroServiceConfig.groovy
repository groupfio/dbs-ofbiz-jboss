import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.groupfio.custom.field.util.DataHelper;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

//Birthday Service
birthDateServiceCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "birthDate.service.campaignId"))
    .queryFirst();
if (birthDateServiceCampaignIdGV != null && birthDateServiceCampaignIdGV.size() > 0) {
    birthDateServiceCampaignId = birthDateServiceCampaignIdGV.getString("propertyValue");
    context.put("birthDateServiceCampaignId", birthDateServiceCampaignId);
}

birthDateServiceCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "birthDate.service.customField"))
    .queryFirst();
if (birthDateServiceCustomFieldGV != null && birthDateServiceCustomFieldGV.size() > 0) {
    birthDateServiceCustomField = birthDateServiceCustomFieldGV.getString("propertyValue");
    context.put("birthDateServiceCustomField", birthDateServiceCustomField);
}

//Inactive Account Update

inactiveAccountDataUpdateCcvAvgAmtLimitGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.ccvAvgAmtLimit"))
    .queryFirst();
if (inactiveAccountDataUpdateCcvAvgAmtLimitGV != null && inactiveAccountDataUpdateCcvAvgAmtLimitGV.size() > 0) {
    inactiveAccountDataUpdateCcvAvgAmtLimit = inactiveAccountDataUpdateCcvAvgAmtLimitGV.getString("propertyValue");
    context.put("inactiveAccountDataUpdateCcvAvgAmtLimit", inactiveAccountDataUpdateCcvAvgAmtLimit);
}

inactiveAccountDataUpdateOpDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.opDtNoOfMonths"))
    .queryFirst();
if (inactiveAccountDataUpdateOpDtNoOfMonthsGV != null && inactiveAccountDataUpdateOpDtNoOfMonthsGV.size() > 0) {
    inactiveAccountDataUpdateOpDtNoOfMonths = inactiveAccountDataUpdateOpDtNoOfMonthsGV.getString("propertyValue");
    context.put("inactiveAccountDataUpdateOpDtNoOfMonths", inactiveAccountDataUpdateOpDtNoOfMonths);
}

inactiveAccountDataUpdateSchTypeGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.schType"))
    .queryFirst();
if (inactiveAccountDataUpdateSchTypeGV != null && inactiveAccountDataUpdateSchTypeGV.size() > 0) {
    inactiveAccountDataUpdateSchType = inactiveAccountDataUpdateSchTypeGV.getString("propertyValue");
    context.put("inactiveAccountDataUpdateSchType", inactiveAccountDataUpdateSchType);
}

//Inactive Account Conversion Service

inactiveAccountConversionAvarageBalanceGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.conversion.avarageBalance"))
    .queryFirst();
if (inactiveAccountConversionAvarageBalanceGV != null && inactiveAccountConversionAvarageBalanceGV.size() > 0) {
    inactiveAccountConversionAvarageBalance = inactiveAccountConversionAvarageBalanceGV.getString("propertyValue");
    context.put("inactiveAccountConversionAvarageBalance", inactiveAccountConversionAvarageBalance);
}

inactiveAccountConversionCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.conversion.campaignId"))
    .queryFirst();
if (inactiveAccountConversionCampaignIdGV != null && inactiveAccountConversionCampaignIdGV.size() > 0) {
    inactiveAccountConversionCampaignId = inactiveAccountConversionCampaignIdGV.getString("propertyValue");
    context.put("inactiveAccountConversionCampaignId", inactiveAccountConversionCampaignId);
}

//Large OTT FCCA

largeOttFccaExtractionAvgAmtOfOtt12mnthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.avgAmtOfOtt12mnths"))
    .queryFirst();
if (largeOttFccaExtractionAvgAmtOfOtt12mnthsGV != null && largeOttFccaExtractionAvgAmtOfOtt12mnthsGV.size() > 0) {
    largeOttFccaExtractionAvgAmtOfOtt12mnths = largeOttFccaExtractionAvgAmtOfOtt12mnthsGV.getString("propertyValue");
    context.put("largeOttFccaExtractionAvgAmtOfOtt12mnths", largeOttFccaExtractionAvgAmtOfOtt12mnths);
}

largeOttFccaExtractionDbsIndustryCodeGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.dbsIndustryCode"))
    .queryFirst();
if (largeOttFccaExtractionDbsIndustryCodeGV != null && largeOttFccaExtractionDbsIndustryCodeGV.size() > 0) {
    largeOttFccaExtractionDbsIndustryCode = largeOttFccaExtractionDbsIndustryCodeGV.getString("propertyValue");
    context.put("largeOttFccaExtractionDbsIndustryCode", largeOttFccaExtractionDbsIndustryCode);
}

largeOttFccaExtractionLcinStartGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.lcin.start"))
    .queryFirst();
if (largeOttFccaExtractionLcinStartGV != null && largeOttFccaExtractionLcinStartGV.size() > 0) {
    largeOttFccaExtractionLcinStart = largeOttFccaExtractionLcinStartGV.getString("propertyValue");
    context.put("largeOttFccaExtractionLcinStart", largeOttFccaExtractionLcinStart);
}


largeOttFccaExtractionLiabilityFccasaEopbalanceGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.liabilityFccasaEopbalance"))
    .queryFirst();
if (largeOttFccaExtractionLiabilityFccasaEopbalanceGV != null && largeOttFccaExtractionLiabilityFccasaEopbalanceGV.size() > 0) {
    largeOttFccaExtractionLiabilityFccasaEopbalance = largeOttFccaExtractionLiabilityFccasaEopbalanceGV.getString("propertyValue");
    context.put("largeOttFccaExtractionLiabilityFccasaEopbalance", largeOttFccaExtractionLiabilityFccasaEopbalance);
}

largeOttFccaExtractionTotalTandmPceLimitGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.totalTandmPceLimit"))
    .queryFirst();
if (largeOttFccaExtractionTotalTandmPceLimitGV != null && largeOttFccaExtractionTotalTandmPceLimitGV.size() > 0) {
    largeOttFccaExtractionTotalTandmPceLimit = largeOttFccaExtractionTotalTandmPceLimitGV.getString("propertyValue");
    context.put("largeOttFccaExtractionTotalTandmPceLimit", largeOttFccaExtractionTotalTandmPceLimit);
}

//Large Top conversion
loanTopConversionLoanCountGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "loan.topConversion.loanCount"))
    .queryFirst();
if (loanTopConversionLoanCountGV != null && loanTopConversionLoanCountGV.size() > 0) {
    loanTopConversionLoanCount = loanTopConversionLoanCountGV.getString("propertyValue");
    context.put("loanTopConversionLoanCount", loanTopConversionLoanCount);
}

loanTopConversionSchmDescGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "loan.topConversion.schmDesc"))
    .queryFirst();
if (loanTopConversionSchmDescGV != null && loanTopConversionSchmDescGV.size() > 0) {
    loanTopConversionSchmDesc = loanTopConversionSchmDescGV.getString("propertyValue");
    context.put("loanTopConversionSchmDesc", loanTopConversionSchmDesc);
}

//Sales Trigger Btml Conversion

salesTriggerBtmlConversionCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.campaignId"))
    .queryFirst();
if (salesTriggerBtmlConversionCampaignIdGV != null && salesTriggerBtmlConversionCampaignIdGV.size() > 0) {
    salesTriggerBtmlConversionCampaignId = salesTriggerBtmlConversionCampaignIdGV.getString("propertyValue");
    context.put("salesTriggerBtmlConversionCampaignId", salesTriggerBtmlConversionCampaignId);
}

salesTriggerBtmlConversionMatDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.matDtNoOfMonths"))
    .queryFirst();
if (salesTriggerBtmlConversionMatDtNoOfMonthsGV != null && salesTriggerBtmlConversionMatDtNoOfMonthsGV.size() > 0) {
    salesTriggerBtmlConversionMatDtNoOfMonths = salesTriggerBtmlConversionMatDtNoOfMonthsGV.getString("propertyValue");
    context.put("salesTriggerBtmlConversionMatDtNoOfMonths", salesTriggerBtmlConversionMatDtNoOfMonths);
}

salesTriggerBtmlConversionSchmCdeGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.schmCde"))
    .queryFirst();
if (salesTriggerBtmlConversionSchmCdeGV != null && salesTriggerBtmlConversionSchmCdeGV.size() > 0) {
    salesTriggerBtmlConversionSchmCde = salesTriggerBtmlConversionSchmCdeGV.getString("propertyValue");
    context.put("salesTriggerBtmlConversionSchmCde", salesTriggerBtmlConversionSchmCde);
}

//Sales Trigger Btml Extraction

salesTriggerBtmlExtractionMatDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.extraction.matDtNoOfMonths"))
    .queryFirst();
if (salesTriggerBtmlExtractionMatDtNoOfMonthsGV != null && salesTriggerBtmlExtractionMatDtNoOfMonthsGV.size() > 0) {
    salesTriggerBtmlExtractionMatDtNoOfMonths = salesTriggerBtmlExtractionMatDtNoOfMonthsGV.getString("propertyValue");
    context.put("salesTriggerBtmlExtractionMatDtNoOfMonths", salesTriggerBtmlExtractionMatDtNoOfMonths);
}

salesTriggerBtmlExtractionSchmCdeGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.extraction.schmCde"))
    .queryFirst();
if (salesTriggerBtmlExtractionSchmCdeGV != null && salesTriggerBtmlExtractionSchmCdeGV.size() > 0) {
    salesTriggerBtmlExtractionSchmCde = salesTriggerBtmlExtractionSchmCdeGV.getString("propertyValue");
    context.put("salesTriggerBtmlExtractionSchmCde", salesTriggerBtmlExtractionSchmCde);
}

//Fixed Expiry

fixedDepositFDAmountGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.fdamount"))
    .queryFirst();
if (fixedDepositFDAmountGV != null && fixedDepositFDAmountGV.size() > 0) {
    fixedDepositFDAmount = fixedDepositFDAmountGV.getString("propertyValue");
    context.put("fixedDepositFDAmount", fixedDepositFDAmount);
}

fixedDepositRPDesignationGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpDesignation"))
    .queryFirst();
if (fixedDepositRPDesignationGV != null && fixedDepositRPDesignationGV.size() > 0) {
    fixedDepositRPDesignation = fixedDepositRPDesignationGV.getString("propertyValue");
    context.put("fixedDepositRPDesignation", fixedDepositRPDesignation);
}

fixedDepositRPIdentitynoGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpIdentityno"))
    .queryFirst();
if (fixedDepositRPIdentitynoGV != null && fixedDepositRPIdentitynoGV.size() > 0) {
    fixedDepositRPIdentityno = fixedDepositRPIdentitynoGV.getString("propertyValue");
    context.put("fixedDepositRPIdentityno", fixedDepositRPIdentityno);
}

fixedDepositRPTypeGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpType"))
    .queryFirst();
if (fixedDepositRPTypeGV != null && fixedDepositRPTypeGV.size() > 0) {
    fixedDepositRPType = fixedDepositRPTypeGV.getString("propertyValue");
    context.put("fixedDepositRPType", fixedDepositRPType);
}

fixedDepositConstitutionGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.constitution"))
    .queryFirst();
if (fixedDepositConstitutionGV != null && fixedDepositConstitutionGV.size() > 0) {
    fixedDepositConstitution = fixedDepositConstitutionGV.getString("propertyValue");
    context.put("fixedDepositConstitution", fixedDepositConstitution);
}

fixedDepositIbgCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.ibgCustomField"))
    .queryFirst();
if (fixedDepositIbgCustomFieldGV != null && fixedDepositIbgCustomFieldGV.size() > 0) {
    fixedDepositIbgCustomField = fixedDepositIbgCustomFieldGV.getString("propertyValue");
    context.put("fixedDepositIbgCustomField", fixedDepositIbgCustomField);
}

fixedDepositCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
    .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.customField"))
    .queryFirst();
if (fixedDepositCustomFieldGV != null && fixedDepositCustomFieldGV.size() > 0) {
    fixedDepositCustomField = fixedDepositCustomFieldGV.getString("propertyValue");
    context.put("fixedDepositCustomField", fixedDepositCustomField);
}

 