import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.groupfio.custom.field.util.DataHelper;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("DataImporterUiLabels", locale);
/*
groupId = request.getParameter("groupId");
context.put("groupId", groupId);
*/
condition = UtilMisc.toMap();
/*
if (UtilValidate.isNotEmpty(groupId)) {
	condition.put("groupId", groupId);
}*/

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

cond = EntityCondition.makeCondition(condition);
microServiceList = delegator.findList("MicroServiceHeader", cond, null, ["sequenceNumber"], efo, false);
context.put("microServiceList", microServiceList);
