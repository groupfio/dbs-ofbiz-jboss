import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.groupfio.custom.field.util.DataHelper;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

microServiceId = request.getParameter("microServiceId");
context.put("microServiceId",microServiceId);
microServiceName="";
extractProg="";
extractCondId="";
conversionCondId="";
conversionProg="";

    	microservice = EntityQuery.use(delegator).from("MicroServiceHeader")
    			.where(EntityCondition.makeCondition("microServiceId", EntityOperator.EQUALS, microServiceId))
    			.queryOne();

    	if (microservice != null) {
    		microServiceName = microservice.getString("microServiceName");
    		extractProg = microservice.getString("extractProg");
    		conversionProg = microservice.getString("conversionProg");
    		extractCondId = microservice.getString("extractConditionId");
    		conversionCondId = microservice.getString("conversionConditionId");
    	}
    	context.put("microServiceName", microServiceName);
    	context.put("extractProg", extractProg);
    	context.put("conversionProg", conversionProg);
    	if (extractCondId != null) {
    		List < EntityCondition > inputCondition = new ArrayList < EntityCondition > ();

    		inputCondition.add(EntityCondition.makeCondition("conditionId", EntityOperator.EQUALS, extractCondId));
    		inputCondition.add(EntityCondition.makeCondition("conditionType", EntityOperator.EQUALS, "INPUT"));
    		EntityCondition inputConditionMain = EntityCondition.makeCondition(inputCondition, EntityOperator.AND);

    		condionParams = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
    				.where(inputConditionMain)
    				.queryList();

    		context.put("condionParams", condionParams);


    		List < EntityCondition > outputExtractCondition = new ArrayList < EntityCondition > ();
    		outputExtractCondition.add(EntityCondition.makeCondition("conditionId", EntityOperator.EQUALS, extractCondId));
    		outputExtractCondition.add(EntityCondition.makeCondition("conditionType", EntityOperator.EQUALS, "OUTPUT"));

    		EntityCondition outputExtractConditionMain = EntityCondition.makeCondition(outputExtractCondition, EntityOperator.AND);

    		condionExtOutParams = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
    				.where(outputExtractCondition)
    				.queryList();
    		context.put("condionExtOutParams", condionExtOutParams);

    	}
    	if (conversionCondId != null) {

    		List < EntityCondition > inputCovCondition = new ArrayList < EntityCondition > ();
    		inputCovCondition.add(EntityCondition.makeCondition("conditionId", EntityOperator.EQUALS, conversionCondId));
    		inputCovCondition.add(EntityCondition.makeCondition("conditionType", EntityOperator.EQUALS, "INPUT"));

    		EntityCondition inputCovConditionMain = EntityCondition.makeCondition(inputCovCondition, EntityOperator.AND);
    		conversionCondIdCondion = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
    				.where(inputCovConditionMain)
    				.queryList();

    		context.put("conversionCondIdCondions", conversionCondIdCondion);


    		List < EntityCondition > outputConvCondition = new ArrayList < EntityCondition > ();
    		outputConvCondition.add(EntityCondition.makeCondition("conditionId", EntityOperator.EQUALS, conversionCondId));
    		outputConvCondition.add(EntityCondition.makeCondition("conditionType", EntityOperator.EQUALS, "OUTPUT"));

    		EntityCondition outputConvConditionMain = EntityCondition.makeCondition(outputConvCondition, EntityOperator.AND);

    		condionConvOutParams = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
    				.where(outputConvConditionMain)
    				.queryList();
    		context.put("condionConvOutParams", condionConvOutParams);


    	}