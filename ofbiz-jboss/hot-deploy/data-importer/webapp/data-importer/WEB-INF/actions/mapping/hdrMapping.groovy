/**
 * @author vijayakumar, sharif
 * @since July 6, 2018
 *
 */
import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.entity.GenericValue
import org.ofbiz.entity.condition.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.groupfio.data.importer.util.UtilMiscCustom;
import java.util.Date;
import java.util.HashMap
import java.util.List
import java.util.Map
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.party.party.PartyHelper;
import org.ofbiz.entity.util.EntityUtil;
import org.groupfio.custom.field.util.DataHelper;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.groupfio.custom.field.util.QueryUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.UtilMisc;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("DataImporterUiLabels", locale);

hadoopHdrFile = delegator.findAll("HadoopHdrFile", false);
context.hadoopHdrFiles = DataHelper.getDropDownOptions(hadoopHdrFile, "hdrFileType", "hdrFileType");

headerFilter = new HashMap();
List resp = FastList.newInstance();

hdrFileType = UtilValidate.isNotEmpty(request.getParameter("filterHdrFileType")) ? request.getParameter("filterHdrFileType") : context.get("filterHdrFileType");
println("hdrFileType>> "+ hdrFileType)

hdrName = request.getParameter("filterHdrName");
hdrCrmType = request.getParameter("filterHdrCrmType");
hdrRmVisible = request.getParameter("filterHdrRmVisible");

performFindAction = UtilValidate.isNotEmpty(request.getParameter("performFindAction")) ? request.getParameter("performFindAction") : context.get("filterHdrFileType");

headerFilter.hdrFileType = hdrFileType;
headerFilter.hdrName = hdrName;
headerFilter.hdrCrmType = hdrCrmType;
headerFilter.hdrRmVisible = hdrRmVisible;

context.headerFilter = headerFilter;

Debug.logInfo("--------------"+hdrFileType, "");

// START FROM HERE
hdrCrmSrcId = UtilMiscCustom.toMap("GROUP_ID", uiLabelMap.get("GroupId"), "GROUP_NAME", uiLabelMap.get("GroupName"),"FIELD_ID",uiLabelMap.get("FieldId"),"FIELD_NAME",uiLabelMap.get("FieldName"));
context.hdrCrmSrcId = hdrCrmSrcId;

//hdrCrmTypes = UtilMiscCustom.toMap("STANDARD", uiLabelMap.get("standard"), "CUSTOM_FIELD", uiLabelMap.get("attribute"), "SEGMENTATION", uiLabelMap.get("segmentation"), "ECONOMIC_METRIC", uiLabelMap.get("economicMetric"));
hdrCrmTypes = UtilMiscCustom.toMap("SEGMENTATION", uiLabelMap.get("segmentation"), "CUSTOM_FIELD", uiLabelMap.get("attribute"), "ECONOMIC_METRIC", uiLabelMap.get("economicMetric"));
context.hdrCrmTypes = hdrCrmTypes;

findHdrCrmTypes = UtilMiscCustom.toMap("SEGMENTATION", uiLabelMap.get("segmentation"), "CUSTOM_FIELD", uiLabelMap.get("attribute"), "ECONOMIC_METRIC", uiLabelMap.get("economicMetric"), "NOT_CONFIGURED", uiLabelMap.get("notConfigured"));
context.findHdrCrmTypes = findHdrCrmTypes;

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

if(UtilValidate.isNotEmpty(performFindAction)) {

	condition = UtilMiscCustom.toMap();
	
	if (UtilValidate.isNotEmpty(hdrFileType)) {
		condition.put("hdrFileType", hdrFileType);
	}
	
	if (UtilValidate.isNotEmpty(hdrCrmType) && !hdrCrmType.equals("NOT_CONFIGURED")) {
		condition.put("hdrCrmType", hdrCrmType);
	}
	
	if (UtilValidate.isNotEmpty(hdrCrmType) && hdrCrmType.equals("NOT_CONFIGURED")) {
		condition.put("hdrCrmType", null);
	}
	
	if (UtilValidate.isNotEmpty(hdrRmVisible)) {
		condition.put("hdrRmVisible", hdrRmVisible);
	}
	
	cond = EntityCondition.makeCondition(condition);
	
	if (UtilValidate.isNotEmpty(hdrName)) {
		EntityCondition nameCondition = EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("hdrName")), EntityOperator.LIKE, "%"+hdrName.toUpperCase()+"%");
		cond = EntityCondition.makeCondition([cond,
			nameCondition
		], EntityOperator.AND);
	}
	
	hadoopHdrMasterList = delegator.findList("HadoopHdrMaster", cond, null, ["hdrInputSeq"], null, false);

	JSONArray results = new JSONArray();
	hadoopHdrMasterList.each{field ->
		JSONObject result = new JSONObject();
		result.putAll(field);
		if (UtilValidate.isNotEmpty(field.getString("hdrCrmType")) && (field.getString("hdrCrmType").equals("SEGMENTATION") || field.getString("hdrCrmType").equals("ECONOMIC_METRIC"))) {
			if (UtilValidate.isNotEmpty(field.hdrCrmSrcId)) {
				code = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroupingCode",UtilMisc.toMap("customFieldGroupingCodeId", field.hdrCrmSrcId), null, false) );
				if (UtilValidate.isNotEmpty(code)) {
					result.put("groupingCodeName", code.getString("groupingCode"));
				} 
			}
			
			if (UtilValidate.isNotEmpty(field.hdrCrmSrcField)) {
				group = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup",UtilMisc.toMap("groupId", field.hdrCrmSrcField), null, false) );
				if (UtilValidate.isNotEmpty(group)) {
					result.put("groupName", group.getString("groupName"));
				} 
			}
			
			if (UtilValidate.isNotEmpty(field.hdrSrcKeyVal)) {
				customField = EntityUtil.getFirst( delegator.findByAnd("CustomField",UtilMisc.toMap("customFieldId", field.hdrSrcKeyVal), null, false) );
				if (UtilValidate.isNotEmpty(customField)) {
					result.put("customFieldName", customField.getString("customFieldName"));
				} 
			}
		}
		
		if (UtilValidate.isNotEmpty(field.getString("hdrCrmType")) && (field.getString("hdrCrmType").equals("CUSTOM_FIELD"))) {
			if (UtilValidate.isNotEmpty(field.hdrCrmSrcId)) {
				group = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup",UtilMisc.toMap("groupId", field.hdrCrmSrcId), null, false) );
				if (UtilValidate.isNotEmpty(group)) {
					result.put("groupName", group.getString("groupName"));
				} 
			}
			
			if (UtilValidate.isNotEmpty(field.hdrCrmSrcField)) {
				customField = EntityUtil.getFirst( delegator.findByAnd("CustomField",UtilMisc.toMap("customFieldId", field.hdrCrmSrcField), null, false) );
				if (UtilValidate.isNotEmpty(customField)) {
					result.put("customFieldName", customField.getString("customFieldName"));
				} 
			}
		}
		
		results.add(result);
	}
	
	context.resp = results;
}


