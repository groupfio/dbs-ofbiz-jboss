/**
 * @author Sharif Ul Islam
 *
 */
import java.sql.BatchUpdateException;

import javolution.util.FastList

import org.ofbiz.entity.condition.*
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.groupfio.data.importer.util.DataHelper;
import org.ofbiz.base.util.UtilProperties;
import java.util.LinkedHashMap;

String defaultModelId = UtilProperties.getPropertyValue("Etl-Process", "position.import.default.modelId");
context.put("defaultModelId", defaultModelId);

if (UtilValidate.isNotEmpty(defaultModelId)) {
	etlModel = EntityUtil.getFirst(delegator.findByAnd("EtlModel", UtilMisc.toMap("modelId", defaultModelId), null, false));
	if (UtilValidate.isNotEmpty(etlModel)) {
		context.put("defaultModelName", etlModel.getString("modelName"));
	}
}

importFileOptions = new LinkedHashMap<String, Object>();

importFileOptions.put("CSV", "csv");
//importFileOptions.put("EXCEL", "excel");
//importFileOptions.put("TEXT", "text");
//importFileOptions.put("XML", "xml");
//importFileOptions.put("JSON", "json");

context.put("importFileOptions", importFileOptions);

//GET IMPORTED LEADS
 
leadConditions = FastList.newInstance();
leadConditions.add(new EntityExpr("importStatusId", EntityOperator.EQUALS, "DATAIMP_IMPORTED"));
 
conditions = new EntityConditionList(leadConditions, EntityOperator.AND);
 
importedPositions = delegator.findCountByCondition("DataImportEmplPosition", conditions, null, null);
 
//GET ERROR LEADS
 
leadConditions = FastList.newInstance();
statusCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_ERROR"),
		EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED")
	], EntityOperator.OR);
leadConditions.add(statusCondition);
  
conditions = new EntityConditionList(leadConditions, EntityOperator.AND);
println("conditions>>>> "+conditions);  
errorPositions = delegator.findCountByCondition("DataImportEmplPosition", conditions, null, null);

//GET NOT PROCCEED LEADS
 
leadConditions = FastList.newInstance();
statusCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null)
	], EntityOperator.OR);
leadConditions.add(statusCondition);
  
conditions = new EntityConditionList(leadConditions, EntityOperator.AND);
println("conditions>>>> "+conditions);  
notProceedPositions = delegator.findCountByCondition("DataImportEmplPosition", conditions, null, null);
	 
context.put("importedPositions", importedPositions);
context.put("errorPositions", errorPositions);
context.put("notProceedPositions", notProceedPositions);

// Upload section [start]

modelTypes = UtilMisc.toMap("DataImportEmplPosition", "EmplPosition Model");
context.put("modelTypes", modelTypes);

modelList = new HashMap();
uploadFilter = new HashMap();

modelType = request.getParameter("modelType");
if (UtilValidate.isNotEmpty(modelType)) {
    modelList = DataHelper.getLeadModelList(delegator, modelType);
}
uploadFilter.put("modelType", modelType);

modelId = request.getParameter("modelId");
if (UtilValidate.isNotEmpty(modelId)) {
}
uploadFilter.put("modelId", modelId);

context.put("modelList", modelList);
context.put("uploadFilter", uploadFilter);

// Upload section [end]


