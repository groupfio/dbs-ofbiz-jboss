/**
 * @author Sharif Ul Islam
 *
 */
import java.sql.BatchUpdateException;

import javolution.util.FastList

import org.ofbiz.entity.condition.*
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.groupfio.data.importer.util.DataHelper;
import org.ofbiz.base.util.UtilProperties;
import java.util.LinkedHashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("DataImporterUiLabels", locale);

importStatusList = new LinkedHashMap<String, Object>();

importStatusList.put("DATAIMP_IMPORTED", uiLabelMap.get("dataimpImported"));
importStatusList.put("DATAIMP_ERROR", uiLabelMap.get("dataimpError"));

context.put("importStatusList", importStatusList);

importStatusId = request.getParameter("importStatusId");
dataImportEmplPositionId = request.getParameter("dataImportEmplPositionId");

filterEmplPositionBatch = new HashMap();

filterEmplPositionBatch.put("importStatusId", UtilValidate.isNotEmpty(importStatusId) ? importStatusId : null);
filterEmplPositionBatch.put("dataImportEmplPositionId", dataImportEmplPositionId);

context.put("filterEmplPositionBatch", filterEmplPositionBatch);

errorCodeList = new LinkedHashMap<String, Object>();
errorCodes = delegator.findByAnd("ErrorCode", UtilMisc.toMap("ErrorCodeType", "EMPL_POSITION_IMPORT"), UtilMisc.toList("errorCodeId"), false);
errorCodes.each{ errorCode ->
	errorCodeList.put(errorCode.getString("errorCodeId"), "("+errorCode.getString("errorCodeId")+") "+errorCode.getString("codeDescription"));
}
context.put("errorCodeList", errorCodeList);



