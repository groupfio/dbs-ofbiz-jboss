import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.data.importer.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

uiLabelMap = UtilProperties.getResourceBundleMap("DataImporterUiLabels", locale);

importStatusId = parameters.get("importStatusId");
dataImportEmplPositionId = parameters.get("dataImportEmplPositionId");
batchId = parameters.get("batchId");
uploadedByUserLoginId = parameters.get("uploadedByUserLoginId");
errorCodeId = parameters.get("errorCodeId");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "createdStamp";
}

delegator = request.getAttribute("delegator");

boolean isApprover = false;
/*
security = request.getAttribute("security");
if (security.hasPermission("ETL-IMPDAT-APPROVER", userLogin)) {
    isApprover = true;
}
*/
conditionsList = FastList.newInstance();

if (UtilValidate.isNotEmpty(importStatusId)) {
	if (importStatusId.equals("DATAIMP_ERROR")) {
		EntityCondition statusCondition = EntityCondition.makeCondition([
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_ERROR"),
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED")
		], EntityOperator.OR);
		conditionsList.add(statusCondition);		
	} else {
		EntityCondition statusCondition = EntityCondition.makeCondition([
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, importStatusId)
		], EntityOperator.AND);
		conditionsList.add(statusCondition);	
	}
} else {
	/*EntityCondition statusCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_NOT_APPROVED")
	], EntityOperator.AND);
	conditionsList.add(statusCondition);
	*/
}

fromDate = parameters.get("fromDate");
thruDate = parameters.get("thruDate");
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
if (UtilValidate.isNotEmpty(fromDate)) {
	fromdateVal=fromDate+" "+"00:00:00.0";
	Date parsedDateFrom = dateFormat.parse(fromdateVal);
	Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
}
if (UtilValidate.isNotEmpty(thruDate)) { 
	thruDateVal=thruDate+" "+"23:23:59.0";
	Date parsedDateThru = dateFormat.parse(thruDateVal);
	Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
}

if (UtilValidate.isNotEmpty(dataImportEmplPositionId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("dataImportEmplPositionId")), EntityOperator.LIKE, "%"+dataImportEmplPositionId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(batchId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("batchId")), EntityOperator.LIKE, "%"+batchId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(errorCodeId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("errorCodes")), EntityOperator.LIKE, "%"+errorCodeId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(uploadedByUserLoginId)) {
	conditionsList.add(EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, uploadedByUserLoginId));
}

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);
println("isApprover>>> "+isApprover);
println("mainConditons>>> "+mainConditons);

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("DataImportEmplPositionSummary", mainConditons, null, UtilMisc.toSet("dataImportEmplPositionId"), efoNum);

/*
emplPositionList = delegator.findList("DataImportEmplPositionSummary", mainConditons, UtilMisc.toSet("dataImportEmplPositionId"), null, null, false);
int count = emplPositionList.size();
*/

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

println("startInx: "+startInx+", endInx:"+endInx);

if (UtilValidate.isNotEmpty(orderField)) {
	if (orderField.equals("reportingPositionTypeName")) {
		orderField = "reportingPositionType";
	} else if (orderField.equals("managedByPositionTypeName")) {
		orderField = "managedByPositionType";
	} else if (orderField.equals("teamName")) {
		orderField = "teamId";
	}
}

emplPositionList = delegator.findList("DataImportEmplPositionSummary", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);

long recordsFiltered = count;
long recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
emplPositionList.each{emplPosition ->
	JSONObject result = new JSONObject();
	result.putAll(emplPosition);
	
	String importStatusName = "";
	if (UtilValidate.isNotEmpty(emplPosition.get("importStatusId"))) {
		if (emplPosition.get("importStatusId").equals("DATAIMP_APPROVED")) {
			importStatusName = uiLabelMap.get("dataimpApproved");
		} else if (emplPosition.get("importStatusId").equals("DATAIMP_NOT_APPROVED")) {
			importStatusName = uiLabelMap.get("dataimpNotApproved");
		} else if (emplPosition.get("importStatusId").equals("DATAIMP_REJECTED")) {
			importStatusName = uiLabelMap.get("dataimpRejected");
		} else if (emplPosition.get("importStatusId").equals("DATAIMP_IMPORTED")) {
			importStatusName = uiLabelMap.get("dataimpImported");
		} else if (emplPosition.get("importStatusId").equals("DATAIMP_ERROR")) {
			importStatusName = uiLabelMap.get("dataimpError");
		} else if (emplPosition.get("importStatusId").equals("DATAIMP_FAILED")) {
			importStatusName = uiLabelMap.get("dataimpError");
		}
	}
	result.put("importStatusName", importStatusName);
	
	result.put("reportingPositionTypeName", emplPosition.getString("reportingPositionType"));
	result.put("managedByPositionTypeName", emplPosition.getString("managedByPositionType"));
	result.put("teamName", emplPosition.getString("teamId"));
	
	if (UtilValidate.isNotEmpty(emplPosition.get("reportingPositionType"))) {
		emplPositionType = EntityUtil.getFirst( delegator.findByAnd("EmplPositionType", UtilMisc.toMap("emplPositionTypeId", emplPosition.get("reportingPositionType")), null, false) );
		if (UtilValidate.isNotEmpty(emplPositionType)) {
			result.put("reportingPositionTypeName", emplPositionType.getString("description"));
		}
	}
	if (UtilValidate.isNotEmpty(emplPosition.get("managedByPositionType"))) {
		emplPositionType = EntityUtil.getFirst( delegator.findByAnd("EmplPositionType", UtilMisc.toMap("emplPositionTypeId", emplPosition.get("managedByPositionType")), null, false) );
		if (UtilValidate.isNotEmpty(emplPositionType)) {
			result.put("managedByPositionTypeName", emplPositionType.getString("description"));
		}
	}
	if (UtilValidate.isNotEmpty(emplPosition.get("teamId"))) {
		emplTeam = EntityUtil.getFirst( delegator.findByAnd("EmplTeam", UtilMisc.toMap("emplTeamId", emplPosition.get("teamId")), null, false) );
		if (UtilValidate.isNotEmpty(emplTeam)) {
			result.put("teamName", emplTeam.getString("teamName"));
		}
	}
	 
	result.put("lastUpdatedStamp", UtilValidate.isNotEmpty(emplPosition.get("lastUpdatedStamp")) ? UtilDateTime.timeStampToString(emplPosition.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm", TimeZone.getDefault(), null) : "");
	result.put("createdStamp", UtilValidate.isNotEmpty(emplPosition.get("createdStamp")) ? UtilDateTime.timeStampToString(emplPosition.getTimestamp("createdStamp"), "dd/MM/yyyy HH:mm", TimeZone.getDefault(), null) : "");
	
	codeList = new LinkedHashMap<String, Object>();
	if (UtilValidate.isNotEmpty(emplPosition.get("errorCodes"))) {
		
		errorCodeList = StringUtil.split(emplPosition.getString("errorCodes"), ",");
		errorCodeList.each{ errorCode ->
			
			code = EntityUtil.getFirst( delegator.findByAnd("ErrorCode", UtilMisc.toMap("errorCodeId", errorCode, "ErrorCodeType", "EMPL_POSITION_IMPORT"), null, false) );
			if (UtilValidate.isNotEmpty(code)) {
				
				toolTip = code.get("codeDescription");
				
				codeList.put(errorCode, toolTip);
			}
		}
		
	}
	result.put("codeList", codeList);
		
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);