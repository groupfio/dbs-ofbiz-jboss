import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.data.importer.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

uiLabelMap = UtilProperties.getResourceBundleMap("DataImporterUiLabels", locale);

String defaultModelId = UtilProperties.getPropertyValue("Etl-Process", "position.import.default.modelId");

importStatusId = parameters.get("importStatusId");
dataImportEmplPositionId = parameters.get("dataImportEmplPositionId");
batchId = parameters.get("batchId");
uploadedByUserLoginId = parameters.get("uploadedByUserLoginId");
errorCodeId = parameters.get("errorCodeId");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "createdStamp";
}

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

if(UtilValidate.isNotEmpty(defaultModelId)) {
	conditionsList.add(EntityCondition.makeCondition("modelId", EntityOperator.EQUALS, defaultModelId));
}

if (UtilValidate.isNotEmpty(importStatusId)) {
	if (importStatusId.equals("DATAIMP_ERROR")) {
		EntityCondition statusCondition = EntityCondition.makeCondition([
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_ERROR"),
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED")
		], EntityOperator.OR);
		conditionsList.add(statusCondition);		
	} else {
		EntityCondition statusCondition = EntityCondition.makeCondition([
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, importStatusId)
		], EntityOperator.AND);
		conditionsList.add(statusCondition);	
	}
} else {
	/*EntityCondition statusCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_NOT_APPROVED")
	], EntityOperator.AND);
	conditionsList.add(statusCondition);
	*/
}

fromDate = parameters.get("fromDate");
thruDate = parameters.get("thruDate");
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
if (UtilValidate.isNotEmpty(fromDate)) {
	fromdateVal=fromDate+" "+"00:00:00.0";
	Date parsedDateFrom = dateFormat.parse(fromdateVal);
	Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
}
if (UtilValidate.isNotEmpty(thruDate)) { 
	thruDateVal=thruDate+" "+"23:23:59.0";
	Date parsedDateThru = dateFormat.parse(thruDateVal);
	Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
}

if (UtilValidate.isNotEmpty(dataImportEmplPositionId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("dataImportEmplPositionId")), EntityOperator.LIKE, "%"+dataImportEmplPositionId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(batchId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("batchId")), EntityOperator.LIKE, "%"+batchId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(errorCodeId)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("errorCodes")), EntityOperator.LIKE, "%"+errorCodeId.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(uploadedByUserLoginId)) {
	conditionsList.add(EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, uploadedByUserLoginId));
}

conditionsList.add(EntityCondition.makeCondition("dataImportBatchId", EntityOperator.NOT_EQUAL, null));

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);
println("mainConditons>>> "+mainConditons);

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("EtlProcessorEmplPositionSummary", mainConditons, null, UtilMisc.toSet("batchId"), efoNum);

/*
emplPositionList = delegator.findList("EtlProcessorEmplPositionSummary", mainConditons, UtilMisc.toSet("batchId"), UtilMisc.toList("-createdStamp"), null, false);
int count = emplPositionList.size();
*/

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

println("startInx: "+startInx+", endInx:"+endInx);
println("orderField: "+orderField+", sortDir:"+sortDir);
println("count: "+count);

emplPositionList = delegator.findList("EtlProcessorEmplPositionSummary", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);

long recordsFiltered = count;
long recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
emplPositionList.each{emplPosition ->
	JSONObject result = new JSONObject();
	result.putAll(emplPosition);
	
	result.put("createdStamp", UtilValidate.isNotEmpty(emplPosition.get("createdStamp")) ? UtilDateTime.timeStampToString(emplPosition.getTimestamp("createdStamp"), "dd/MM/yyyy HH:mm", TimeZone.getDefault(), null) : "");
	
	condition = EntityCondition.makeCondition(EntityOperator.AND,
			EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, emplPosition.get("batchId")),
			EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_IMPORTED")
			);
	
	long importedCount = delegator.findCountByCondition("DataImportEmplPosition", condition, null, null);
	result.put("importedCount", importedCount);			
	
	condition = EntityCondition.makeCondition(EntityOperator.AND,
			EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, emplPosition.get("batchId")),
			EntityCondition.makeCondition([
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_ERROR"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED")
				], EntityOperator.OR)
			);
	
	long errorCount = delegator.findCountByCondition("DataImportEmplPosition", condition, null, null);
	result.put("errorCount", errorCount);	
	/*
	condition = EntityCondition.makeCondition(EntityOperator.AND,
			EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, emplPosition.get("batchId")),
			EntityCondition.makeCondition([
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "")
				], EntityOperator.OR)
			);
	
	long notProcessedCount = delegator.findCountByCondition("DataImportLead", condition, null, null);
	result.put("notProcessedCount", notProcessedCount);	
	*/
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);