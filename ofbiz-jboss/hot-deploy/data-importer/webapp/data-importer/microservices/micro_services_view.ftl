<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header">
   <h2 class="float-left">${microServiceName!}</h2>
   <div class="float-right">
		  <a href="/data-importer/control/microserviceConfig" class="btn btn-xs btn-primary" role="button">Back</a>
</div>
</div>

<br>
<div>
   <div class="page-header">
      <h2 class="float-left">Extraction Conditions</h2>
      <div class="float-right">
         <div class="form-group row">
            <div class="col-sm-4">
               <label class="font-weight-bold">${extractProg!} </label>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clearfix"></div>
<div>
<br>
<form name="updateConditionParam" id="updateConditionParam" action="updateConditionParam" method="post">
   <input type="hidden" name="microServiceId" id="microServiceId" value="${microServiceId!}">
   <div class="row padding-r">
      <div class="col-md-4 col-sm-4">
         <div class="form-group row mr">
            <label class="font-weight-bold">Param Name</label>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <label class="font-weight-bold">Value Capture</label>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <label class="font-weight-bold">Condtion</label>
         </div>
      </div>
      <div class="col-md-1 col-sm-1">
         <div class="form-group">
            <label class="font-weight-bold">Sequence</label>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <label class="font-weight-bold">Param Value</label>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <#list condionParams as condionParam>
   <div class="row padding-r">
      <div class="col-md-4 col-sm-4">
         <div class="form-group row mr">
            <label>${condionParam.conditionParam!}</label>
         </div>
      </div>
      <#if "SINGLE"==condionParam.valueCapture>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${condionParam.valueCapture!}" placeholder="Value Capture" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="opr" name="opr" value="EQUALS" placeholder="operation" disabled>
         </div>
      </div>
      <div class="col-md-1 col-sm-1">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.valueSeqNum!}" name="${condionParam.valueSeqNum!}" value="${condionParam.valueSeqNum!}" placeholder="${condionParam.valueSeqNum!}" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.conditionParam!}" name="${condionParam.conditionParam!}" value="${condionParam.valueData!}" placeholder="${condionParam.description!}" >
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
         </div>
      </div>
      <#elseif "MULTIPLE"==condionParam.valueCapture>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${condionParam.valueCapture!}" placeholder="Value Capture" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="EQUALS" placeholder="operation" disabled>
         </div>
      </div>
      <div class="col-md-1 col-sm-1">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.valueSeqNum!}" name="${condionParam.valueSeqNum!}" value="${condionParam.valueSeqNum!}" placeholder="${condionParam.valueSeqNum!}" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.conditionParam!}" name="${condionParam.conditionParam!}" value="${condionParam.valueData!}" placeholder="${condionParam.description!}" >
         </div>
      </div>
      <#elseif "RANGE"==condionParam.valueCapture>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${condionParam.valueCapture!}" placeholder="Value Capture" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="BETWEEN" placeholder="operation" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.conditionParam!}" name="${condionParam.conditionParam!}" value="${condionParam.valueMin!}" placeholder="${condionParam.description!}" >
         </div>
      </div>
      <div class="col-md-1 col-sm-1">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.valueSeqNum!}" name="${condionParam.valueSeqNum!}" value="${condionParam.valueSeqNum!}" placeholder="${condionParam.valueSeqNum!}" disabled>
         </div>
      </div>
      <div class="col-md-2 col-sm-2">
         <div class="form-group">
            <input type="text" class="form-control input-sm" id="${condionParam.conditionParam!}" name="${condionParam.conditionParam!}_MAX" value="${condionParam.valueMax!}" placeholder="Operator">
         </div>
      </div>
      </#if>
   </div>
   </#list>
   <br>
   <div class="page-header">
      <h2>Out Parameters </h2>
   </div>
   <div>
      <br>
      <div class="row padding-r">
         <div class="col-md-4 col-sm-4">
            <div class="form-group row mr">
               <label class="font-weight-bold">Param Name</label>
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group">
               <label class="font-weight-bold">Table Name</label>
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group">
               <label class="font-weight-bold">Column Name</label>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <#list condionExtOutParams as condionExtOutParam>
      <div class="row padding-r">
         <div class="col-md-4 col-sm-4">
            <div class="form-group">
               <label>${condionExtOutParam.conditionParam!}</label>
            </div>
         </div>
         <div class="col-md-2">
            <div class="form-group">
               <input type="text" class="form-control input-sm" id="${condionExtOutParam.outTable!}" name="${condionExtOutParam.outTable!}" value="${condionExtOutParam.outTable!}" placeholder="operation" disabled>
            </div>
         </div>
         <div class="col-md-2">
            <div class="form-group">
               <input type="text" class="form-control input-sm" id="${condionExtOutParam.outColumn!}" name="${condionExtOutParam.outColumn!}" value="${condionExtOutParam.outColumn!}" placeholder="${condionExtOutParam.outColumn!}" disabled>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      </#list>
      <div>
         <div class="page-header">
            <h2>Conversion Conditions</h2>
         </div>
         <div class="float-right">
            <div class="form-group row">
               <div class="col-sm-4">
                  <label class="font-weight-bold">${conversionProg!}</label>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div>
         <br>
         <div class="row padding-r">
            <div class="col-md-4 col-sm-4">
               <div class="form-group row mr">
                  <label class="font-weight-bold">Param Name</label>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group">
                  <label class="font-weight-bold">Value Capture</label>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group">
                  <label class="font-weight-bold">Condtion</label>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group">
                  <label class="font-weight-bold">Param Value</label>
               </div>
            </div>
         </div>
         <#if conversionCondIdCondions?has_content>
         <#list conversionCondIdCondions as conversionCondIdCondion>
         <div class="row padding-r">
            <div class="col-md-4 col-sm-4">
               <div class="form-group row mr">
                  <label>${conversionCondIdCondion.conditionParam!}</label>
               </div>
            </div>
            <#if "SINGLE"==conversionCondIdCondion.valueCapture>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${conversionCondIdCondion.valueCapture!}" placeholder="Value Capture" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="EQUALS" placeholder="operation" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="${conversionCondIdCondion.conditionParam!}" name="${conversionCondIdCondion.conditionParam!}" value="${conversionCondIdCondion.valueData!}" placeholder="${conversionCondIdCondion.description!}" >
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
               </div>
            </div>
            <#elseif "MULTIPLE"==conversionCondIdCondion.valueCapture>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${conversionCondIdCondion.valueCapture!}" placeholder="Value Capture" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="EQUALS" placeholder="operation" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="${conversionCondIdCondion.conditionParam!}" name="${conversionCondIdCondion.conditionParam!}" value="${conversionCondIdCondion.valueData!}" placeholder="${conversionCondIdCondion.description!}" >
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
               </div>
            </div>
            <#elseif "RANGE"==conversionCondIdCondion.valueCapture>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="valueCapture" name="valueCapture" value="${conversionCondIdCondion.valueCapture!}" placeholder="Value Capture" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="BETWEEN" placeholder="operation" disabled>
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="${conversionCondIdCondion.conditionParam!}" name="${conversionCondIdCondion.conditionParam!}" value="${conversionCondIdCondion.valueMin!}" placeholder="${conversionCondIdCondion.description!}" >
               </div>
            </div>
            <div class="col-md-2 col-sm-2">
               <div class="form-group row mr">
                  <input type="text" class="form-control input-sm" id="${conversionCondIdCondion.conditionParam!}" name="${conversionCondIdCondion.conditionParam!}_MAX" value="${conversionCondIdCondion.valueMax!}" placeholder="Operator">
               </div>
            </div>
            </#if>
         </div>
      </div>
      </#list>
      </#if>
      <button type="submit" class="btn btn-sm btn-primary" onClick="">${uiLabelMap.submit} </button>
   </div>
</form>