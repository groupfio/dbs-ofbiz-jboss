<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
   <h1 class="float-left">${uiLabelMap.microServiceConfig!}</h1>
</div>


<form role="form" class="form-horizontal" action="<@ofbizUrl>updateConfigConditionParam</@ofbizUrl>" method="post" data-toggle="validator">
  <input type="hidden" name="microServiceId" id="microServiceId" value="${microServiceId!}">
  <div class="page-header">
    <h2 class="float-left">Birthday Service</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="birthDateServiceCampaignId"
    label="birthDate.service.campaignId"
    value=birthDateServiceCampaignId
    />
    
  <@generalInputForMicroServiceConfig 
    id="birthDateServiceCustomField"
    label="birthDate.service.customField"
    value=birthDateServiceCustomField
    />
    
  <div class="page-header">
    <h2 class="float-left">Inactive Account Update</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="inactiveAccountDataUpdateCcvAvgAmtLimit"
    label="inactive.account.dataUpdate.ccvAvgAmtLimit"
    value=inactiveAccountDataUpdateCcvAvgAmtLimit
    />
    
  <@generalInputForMicroServiceConfig 
    id="inactiveAccountDataUpdateOpDtNoOfMonths"
    label="inactive.account.dataUpdate.opDtNoOfMonths"
    value=inactiveAccountDataUpdateOpDtNoOfMonths
    />
    
  <@generalInputForMicroServiceConfig 
    id="inactiveAccountDataUpdateSchType"
    label="inactive.account.dataUpdate.schType"
    value=inactiveAccountDataUpdateSchType
    />

  <div class="page-header">
    <h2 class="float-left">Inactive Account Conversion Service</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="inactiveAccountConversionAvarageBalance"
    label="inactive.account.conversion.avarageBalance"
    value=inactiveAccountConversionAvarageBalance
    />
    
  <@generalInputForMicroServiceConfig 
    id="inactiveAccountConversionCampaignId"
    label="inactive.account.conversion.campaignId"
    value=inactiveAccountConversionCampaignId
    />
    
    
  <div class="page-header">
    <h2 class="float-left">Large OTT FCCA</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="largeOttFccaExtractionAvgAmtOfOtt12mnths"
    label="large.ott.fcca.extraction.avgAmtOfOtt12mnths"
    value=largeOttFccaExtractionAvgAmtOfOtt12mnths
    />
    
  <@generalInputForMicroServiceConfig 
    id="largeOttFccaExtractionDbsIndustryCode"
    label="large.ott.fcca.extraction.dbsIndustryCode"
    value=largeOttFccaExtractionDbsIndustryCode
    />
    
  <@generalInputForMicroServiceConfig 
    id="largeOttFccaExtractionLcinStart"
    label="large.ott.fcca.extraction.lcin.start"
    value=largeOttFccaExtractionLcinStart
    />
    
  <@generalInputForMicroServiceConfig 
    id="largeOttFccaExtractionLiabilityFccasaEopbalance"
    label="large.ott.fcca.extraction.liabilityFccasaEopbalance"
    value=largeOttFccaExtractionLiabilityFccasaEopbalance
    />
    
  <@generalInputForMicroServiceConfig 
    id="largeOttFccaExtractionTotalTandmPceLimit"
    label="large.ott.fcca.extraction.totalTandmPceLimit"
    value=largeOttFccaExtractionTotalTandmPceLimit
    />
    
  <div class="page-header">
    <h2 class="float-left">Large Top conversion</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="loanTopConversionLoanCount"
    label="loan.topConversion.loanCount"
    value=loanTopConversionLoanCount
    />
    
  <@generalInputForMicroServiceConfig 
    id="loanTopConversionSchmDesc"
    label="loan.topConversion.schmDesc"
    value=loanTopConversionSchmDesc
    />
    
  <div class="page-header">
    <h2 class="float-left">Sales Trigger Btml Conversion</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="salesTriggerBtmlConversionCampaignId"
    label="salesTrigger.btml.conversion.campaignId"
    value=salesTriggerBtmlConversionCampaignId
    />
    
  <@generalInputForMicroServiceConfig 
    id="salesTriggerBtmlConversionMatDtNoOfMonths"
    label="salesTrigger.btml.conversion.matDtNoOfMonths"
    value=salesTriggerBtmlConversionMatDtNoOfMonths
    />
    
  <@generalInputForMicroServiceConfig 
    id="salesTriggerBtmlConversionSchmCde"
    label="salesTrigger.btml.conversion.schmCde"
    value=salesTriggerBtmlConversionSchmCde
    />
    
  <div class="page-header">
    <h2 class="float-left">Sales Trigger Btml Extraction</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="salesTriggerBtmlExtractionMatDtNoOfMonths"
    label="salesTrigger.btml.extraction.matDtNoOfMonths"
    value=salesTriggerBtmlExtractionMatDtNoOfMonths
    />
    
  <@generalInputForMicroServiceConfig 
    id="salesTriggerBtmlExtractionSchmCde"
    label="salesTrigger.btml.extraction.schmCde"
    value=salesTriggerBtmlExtractionSchmCde
    />
    
  <div class="page-header">
    <h2 class="float-left">Fixed Expiry</h2>
  </div>
  
  <@generalInputForMicroServiceConfig 
    id="fixedDepositFDAmount"
    label="fixed.deposit.expiry.extraction.fdamount"
    value=fixedDepositFDAmount
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositRPDesignation"
    label="fixed.deposit.expiry.extraction.rpDesignation"
    value=fixedDepositRPDesignation
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositRPIdentityno"
    label="fixed.deposit.expiry.extraction.rpIdentityno"
    value=fixedDepositRPIdentityno
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositRPType"
    label="fixed.deposit.expiry.extraction.rpType"
    value=fixedDepositRPType
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositConstitution"
    label="fixed.deposit.expiry.extraction.constitution"
    value=fixedDepositConstitution
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositIbgCustomField"
    label="fixed.deposit.expiry.extraction.ibgCustomField"
    value=fixedDepositIbgCustomField
    />
    
  <@generalInputForMicroServiceConfig 
    id="fixedDepositCustomField"
    label="fixed.deposit.expiry.extraction.customField"
    value=fixedDepositCustomField
    />
    
    <button type="submit" class="btn btn-sm btn-primary" onclick="">Submit </button>
</form>



