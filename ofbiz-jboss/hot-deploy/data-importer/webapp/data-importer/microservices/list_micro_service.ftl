<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.List} ${uiLabelMap.MicroService}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list-custom-field-group">
	<thead>
	<tr>
		<th>${uiLabelMap.sequenceNumber!}</th>
		<th>${uiLabelMap.microServiceName!}</th>
		<th>${uiLabelMap.description!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	<#if microServiceList?has_content>
		
	<#list microServiceList as ec>
	<tr>
		<td>${ec.sequenceNumber!}</td>
		<td>${ec.microServiceName!}</td>
		<td>${ec.description!}</td>
		<td class="text-center">
			<div class="">
				<button class="btn btn-xs btn-info tooltips execute-service" data-original-title="Execute Service"
					data-microServiceId="${ec.microServiceId!}"
					>
					<i class="fa fa-flash fa-2x info"></i>
				</button>
			</div>
		</td>	
	</tr>
	
	</#list>
		
	</#if>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		

$('#list-custom-field-group').DataTable({
	"order": [],
	"fnDrawCallback": function( oSettings ) {
  		resetDefaultEvents();
  		resetMicroServiceEvents();
	}
});


	
});	

function resetMicroServiceEvents() {

	$('.execute-service').unbind( "click" );
	$('.execute-service').bind( "click", function( event ) {

		event.preventDefault(); 
		
		microServiceId = $(this).attr("data-microServiceId");
		
		$.ajax({
		      
			type: "POST",
	     	url: "executeMicroService",
	        data:  {"microServiceId": microServiceId},
	        async: true,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            	showAlert ("success", data.message);
	            } else {
	            	showAlert ("error", data.message);
	            }
				    	
	        }
	        
		});    
																																																				
	});

}
	
</script>