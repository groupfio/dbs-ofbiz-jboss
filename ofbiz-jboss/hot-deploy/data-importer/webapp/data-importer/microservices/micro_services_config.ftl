<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
   <h1 class="float-left">${uiLabelMap.microServiceConfig!}</h1>
   <div class="float-right">
   </div>
</div>
<div class="table-responsive">
   <table id="microServiceList" class="table table-hover">
      <thead>
         <tr>
            <th>${uiLabelMap.Name}</th>
            <th>${uiLabelMap.extractionConditions}</th>
            <th>${uiLabelMap.programToInvoke}</th>
            <th>${uiLabelMap.conversionConditions}</th>
            <th>${uiLabelMap.programToInvoke}</th>
         </tr>
      </thead>
   </table>
</div>



<script>
    $(document).ready(function() {
    
        var microServiceListUrl = "microServiceList";
        loadMicroServiceList(microServiceListUrl);
    });
    function loadMicroServiceList(microServiceListUrl) {
        oTable = $('#microServiceList').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "ajax": {
                "url": microServiceListUrl,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },

            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [
                {
                    "data": "microServiceId",
                    "render": function(data, type, row, meta) {
                        var microServiceId = row.microServiceId;
                        if (microServiceId != null && microServiceId != "" && microServiceId != undefined) {
                            data = '<a href="/data-importer/control/microserviceView?microServiceId=' + row.microServiceId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.microServiceName + '(' + microServiceId + ')</a>';
                        }
                        return data;
                    }
                },
                {
                    "data": "extractConditionId"
                },
                {
                    "data": "extractProg",
                    "render": function(data, type, row, meta) {
                        var extractProg = row.extractProg;
                        if (extractProg != null && extractProg != "" && extractProg != undefined) {
                            data = "Y";
                        }
                        return data;
                    }
                },
                {
                    "data": "conversionConditionId"
                },
                {
                    "data": "conversionProg",
                    "render": function(data, type, row, meta) {
                        var conversionProg = row.conversionProg;
                        if (conversionProg != null && conversionProg != "" && conversionProg != undefined) {
                            data = "Y";
                        }
                        return data;
                    }
                }
            ]
        });
    }
</script>

