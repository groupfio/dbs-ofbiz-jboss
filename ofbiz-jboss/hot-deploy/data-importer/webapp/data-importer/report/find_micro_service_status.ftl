<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.Find} ${uiLabelMap.microServiceStatus!}</h1>
	<div class="float-right">
		
	</div>
</div>

<div class="card-header mt-2 mb-3">
   <form id="findJobExecutionForm" method="post" class="form-horizontal" data-toggle="validator">
      <div class="row">
      	
      	<div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="fromDate"
				/>
         </div>
         <div class="col-md-2 col-sm-2">
      		<@simpleDateInput 
				name="thruDate"
				/>
         </div>
          
         <@fromSimpleAction id="find-jobExecution-button" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<script>

jQuery(document).ready(function() {

$('#find-jobExecution-button').on('click', function(){
	findBatchJobs();
});

$(".form_datetime").datetimepicker({
    //autoclose: true,
    //isRTL: BootStrapInit.isRTL(),
    //format: "dd MM yyyy - hh:ii",
    //pickerPosition: (BootStrapInit.isRTL() ? "bottom-right" : "bottom-left")
});

});

</script>
