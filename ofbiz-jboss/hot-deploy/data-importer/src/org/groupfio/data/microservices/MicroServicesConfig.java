package org.groupfio.data.microservices;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;

public class MicroServicesConfig {

	public static String updateConditionParam(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String schType	=request.getParameter("inactive.account.dataUpdate.schType");
		String ccvAvgAmtLimit	=request.getParameter("inactive.account.dataUpdate.ccvAvgAmtLimit");
		String opDtNoOfMonths=request.getParameter("inactive.account.dataUpdate.opDtNoOfMonths");
		String campaignId=request.getParameter("inactive.account.conversion.campaignId");
		String avarageBalance=request.getParameter("inactive.account.conversion.avarageBalance");
		String schmDesc=request.getParameter("loan.topConversion.schmDesc");
		String loanCount=request.getParameter("loan.topConversion.loanCount");

		String microServiceId=request.getParameter("microServiceId");
		Long accountNum=new Long(0);
		Long accountNumMax=new Long(0);
		try {
			System.out.println(ccvAvgAmtLimit+"inactiveAcctCount");
			System.out.println(opDtNoOfMonths+"loanAmt");
			System.out.println(microServiceId+"microServiceId");
			request.setAttribute("microServiceId", microServiceId);
			if(UtilValidate.isNotEmpty(schType)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"inactive.account.dataUpdate.schType"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", schType);
					conditionParamUpdate.set("valueData", schType);
					conditionParamUpdate.store();

				}
			}
			if(UtilValidate.isNotEmpty(ccvAvgAmtLimit)){

				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"inactive.account.dataUpdate.ccvAvgAmtLimit"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					/*accountNum=Long.parseLong(ccvAvgAmtLimit);
					if(UtilValidate.isNotEmpty(inactiveAcctCountMax)){
						accountNumMax=Long.parseLong(inactiveAcctCountMax);
					}*/
					conditionParamUpdate.set("conditionValue", ccvAvgAmtLimit);
					conditionParamUpdate.set("valueData",ccvAvgAmtLimit);
					conditionParamUpdate.store();

				}


			}
			if(UtilValidate.isNotEmpty(opDtNoOfMonths)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"inactive.account.dataUpdate.opDtNoOfMonths"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", opDtNoOfMonths);
					conditionParamUpdate.set("valueData", opDtNoOfMonths);
					conditionParamUpdate.store();

				}

			}
			if(UtilValidate.isNotEmpty(campaignId)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"inactive.account.conversion.campaignId"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", campaignId);
					conditionParamUpdate.set("valueData", campaignId);
					conditionParamUpdate.store();

				}

			}
			if(UtilValidate.isNotEmpty(avarageBalance)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"inactive.account.conversion.avarageBalance"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", avarageBalance);
					conditionParamUpdate.set("valueData", avarageBalance);
					conditionParamUpdate.store();

				}

			}
			if(UtilValidate.isNotEmpty(schmDesc)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"loan.topConversion.schmDesc"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", schmDesc);
					conditionParamUpdate.set("valueData", schmDesc);
					conditionParamUpdate.store();

				}

			}
			if(UtilValidate.isNotEmpty(loanCount)){
				List<GenericValue> configuredParamVal = EntityQuery.use(delegator).from("MicroServiceConditionDetail")
						.where(EntityCondition.makeCondition("conditionParam", EntityOperator.EQUALS,"loan.topConversion.loanCount"))
						.queryList();
				if(configuredParamVal!=null && configuredParamVal.size()>0){
					GenericValue conditionParamUpdate=configuredParamVal.get(0);
					conditionParamUpdate.set("conditionValue", loanCount);
					conditionParamUpdate.set("valueData", loanCount);
					conditionParamUpdate.store();

				}

			}
		}catch(Exception e){
			Debug.log("error while Creating  campaign"+e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		request.setAttribute("_EVENT_MESSAGE_","Config Value Updated successfully.");
		return "success";
	}

    public static String updateConfigConditionParam(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");

        String birthDateServiceCampaignId = request.getParameter("birthDateServiceCampaignId");
        String birthDateServiceCustomField = request.getParameter("birthDateServiceCustomField");

        String inactiveAccountDataUpdateCcvAvgAmtLimit = request.getParameter("inactiveAccountDataUpdateCcvAvgAmtLimit");
        String inactiveAccountDataUpdateOpDtNoOfMonths = request.getParameter("inactiveAccountDataUpdateOpDtNoOfMonths");
        String inactiveAccountDataUpdateSchType = request.getParameter("inactiveAccountDataUpdateSchType");

        String inactiveAccountConversionAvarageBalance = request.getParameter("inactiveAccountConversionAvarageBalance");
        String inactiveAccountConversionCampaignId = request.getParameter("inactiveAccountConversionCampaignId");

        //Large OTT FCCA
        String largeOttFccaExtractionAvgAmtOfOtt12mnths = request.getParameter("largeOttFccaExtractionAvgAmtOfOtt12mnths");
        String largeOttFccaExtractionDbsIndustryCode = request.getParameter("largeOttFccaExtractionDbsIndustryCode");
        String largeOttFccaExtractionLcinStart = request.getParameter("largeOttFccaExtractionLcinStart");
        String largeOttFccaExtractionLiabilityFccasaEopbalance = request.getParameter("largeOttFccaExtractionLiabilityFccasaEopbalance");
        String largeOttFccaExtractionTotalTandmPceLimit = request.getParameter("largeOttFccaExtractionTotalTandmPceLimit");

        //Large Top conversion
        String loanTopConversionLoanCount = request.getParameter("loanTopConversionLoanCount");
        String loanTopConversionSchmDesc = request.getParameter("loanTopConversionSchmDesc");

        String salesTriggerBtmlConversionCampaignId = request.getParameter("salesTriggerBtmlConversionCampaignId");
        String salesTriggerBtmlConversionMatDtNoOfMonths = request.getParameter("salesTriggerBtmlConversionMatDtNoOfMonths");
        String salesTriggerBtmlConversionSchmCde = request.getParameter("salesTriggerBtmlConversionSchmCde");


        String salesTriggerBtmlExtractionMatDtNoOfMonths = request.getParameter("salesTriggerBtmlExtractionMatDtNoOfMonths");
        String salesTriggerBtmlExtractionSchmCde = request.getParameter("salesTriggerBtmlExtractionSchmCde");

        //Fixed Expiry
        String fixedDepositFDAmount = request.getParameter("fixedDepositFDAmount");
        String fixedDepositRPDesignation = request.getParameter("fixedDepositRPDesignation");
        String fixedDepositRPIdentityno = request.getParameter("fixedDepositRPIdentityno");
        String fixedDepositRPType = request.getParameter("fixedDepositRPType");
        String fixedDepositConstitution = request.getParameter("fixedDepositConstitution");
        String fixedDepositIbgCustomField = request.getParameter("fixedDepositIbgCustomField");
        String fixedDepositCustomField = request.getParameter("fixedDepositCustomField");

        try {
            //Birthday Service
            GenericValue birthDateServiceCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "birthDate.service.campaignId"))
                .queryFirst();
            if (birthDateServiceCampaignIdGV != null && birthDateServiceCampaignIdGV.size() > 0) {
                birthDateServiceCampaignIdGV.set("propertyValue", birthDateServiceCampaignId);
                birthDateServiceCampaignIdGV.store();
            }

            GenericValue birthDateServiceCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "birthDate.service.customField"))
                .queryFirst();
            if (birthDateServiceCustomFieldGV != null && birthDateServiceCustomFieldGV.size() > 0) {
                birthDateServiceCustomFieldGV.set("propertyValue", birthDateServiceCustomField);
                birthDateServiceCustomFieldGV.store();
            }


            //Inactive Account Update
            GenericValue inactiveAccountDataUpdateCcvAvgAmtLimitGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.ccvAvgAmtLimit"))
                .queryFirst();
            if (inactiveAccountDataUpdateCcvAvgAmtLimitGV != null && inactiveAccountDataUpdateCcvAvgAmtLimitGV.size() > 0) {
                inactiveAccountDataUpdateCcvAvgAmtLimitGV.set("propertyValue", inactiveAccountDataUpdateCcvAvgAmtLimit);
                inactiveAccountDataUpdateCcvAvgAmtLimitGV.store();
            }

            GenericValue inactiveAccountDataUpdateOpDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.opDtNoOfMonths"))
                .queryFirst();
            if (inactiveAccountDataUpdateOpDtNoOfMonthsGV != null && inactiveAccountDataUpdateOpDtNoOfMonthsGV.size() > 0) {
                inactiveAccountDataUpdateOpDtNoOfMonthsGV.set("propertyValue", inactiveAccountDataUpdateOpDtNoOfMonths);
                inactiveAccountDataUpdateOpDtNoOfMonthsGV.store();
            }

            GenericValue inactiveAccountDataUpdateSchTypeGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.dataUpdate.schType"))
                .queryFirst();
            if (inactiveAccountDataUpdateSchTypeGV != null && inactiveAccountDataUpdateSchTypeGV.size() > 0) {
                inactiveAccountDataUpdateSchTypeGV.set("propertyValue", inactiveAccountDataUpdateSchType);
                inactiveAccountDataUpdateSchTypeGV.store();
            }

            //Inactive Account Conversion Service
            GenericValue inactiveAccountConversionAvarageBalanceGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.conversion.avarageBalance"))
                .queryFirst();
            if (inactiveAccountConversionAvarageBalanceGV != null && inactiveAccountConversionAvarageBalanceGV.size() > 0) {
                inactiveAccountConversionAvarageBalanceGV.set("propertyValue", inactiveAccountConversionAvarageBalance);
                inactiveAccountConversionAvarageBalanceGV.store();
            }

            GenericValue inactiveAccountConversionCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "inactive.account.conversion.campaignId"))
                .queryFirst();
            if (inactiveAccountConversionCampaignIdGV != null && inactiveAccountConversionCampaignIdGV.size() > 0) {
                inactiveAccountConversionCampaignIdGV.set("propertyValue", inactiveAccountConversionCampaignId);
                inactiveAccountConversionCampaignIdGV.store();
            }

            //Large OTT FCCA
            GenericValue largeOttFccaExtractionAvgAmtOfOtt12mnthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.avgAmtOfOtt12mnths"))
                .queryFirst();
            if (largeOttFccaExtractionAvgAmtOfOtt12mnthsGV != null && largeOttFccaExtractionAvgAmtOfOtt12mnthsGV.size() > 0) {
                largeOttFccaExtractionAvgAmtOfOtt12mnthsGV.set("propertyValue", largeOttFccaExtractionAvgAmtOfOtt12mnths);
                largeOttFccaExtractionAvgAmtOfOtt12mnthsGV.store();
            }

            GenericValue largeOttFccaExtractionDbsIndustryCodeGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.dbsIndustryCode"))
                .queryFirst();
            if (largeOttFccaExtractionDbsIndustryCodeGV != null && largeOttFccaExtractionDbsIndustryCodeGV.size() > 0) {
                largeOttFccaExtractionDbsIndustryCodeGV.set("propertyValue", largeOttFccaExtractionDbsIndustryCode);
                largeOttFccaExtractionDbsIndustryCodeGV.store();
            }

            GenericValue largeOttFccaExtractionLcinStartGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.lcin.start"))
                .queryFirst();
            if (largeOttFccaExtractionLcinStartGV != null && largeOttFccaExtractionLcinStartGV.size() > 0) {
                largeOttFccaExtractionLcinStartGV.set("propertyValue", largeOttFccaExtractionLcinStart);
                largeOttFccaExtractionLcinStartGV.store();
            }

            GenericValue largeOttFccaExtractionLiabilityFccasaEopbalanceGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.liabilityFccasaEopbalance"))
                .queryFirst();
            if (largeOttFccaExtractionLiabilityFccasaEopbalanceGV != null && largeOttFccaExtractionLiabilityFccasaEopbalanceGV.size() > 0) {
                largeOttFccaExtractionLiabilityFccasaEopbalanceGV.set("propertyValue", largeOttFccaExtractionLiabilityFccasaEopbalance);
                largeOttFccaExtractionLiabilityFccasaEopbalanceGV.store();
            }

            GenericValue largeOttFccaExtractionTotalTandmPceLimitGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "large.ott.fcca.extraction.totalTandmPceLimit"))
                .queryFirst();
            if (largeOttFccaExtractionTotalTandmPceLimitGV != null && largeOttFccaExtractionTotalTandmPceLimitGV.size() > 0) {
                largeOttFccaExtractionTotalTandmPceLimitGV.set("propertyValue", largeOttFccaExtractionTotalTandmPceLimit);
                largeOttFccaExtractionTotalTandmPceLimitGV.store();
            }

            //Large Top conversion
            GenericValue loanTopConversionLoanCountGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "loan.topConversion.loanCount"))
                .queryFirst();
            if (loanTopConversionLoanCountGV != null && loanTopConversionLoanCountGV.size() > 0) {
                loanTopConversionLoanCountGV.set("propertyValue", loanTopConversionLoanCount);
                loanTopConversionLoanCountGV.store();
            }

            GenericValue loanTopConversionSchmDescGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "loan.topConversion.schmDesc"))
                .queryFirst();
            if (loanTopConversionSchmDescGV != null && loanTopConversionSchmDescGV.size() > 0) {
                loanTopConversionSchmDescGV.set("propertyValue", loanTopConversionSchmDesc);
                loanTopConversionSchmDescGV.store();
            }

            //Sales Trigger Btml
            GenericValue salesTriggerBtmlConversionCampaignIdGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.campaignId"))
                .queryFirst();
            if (salesTriggerBtmlConversionCampaignIdGV != null && salesTriggerBtmlConversionCampaignIdGV.size() > 0) {
                salesTriggerBtmlConversionCampaignIdGV.set("propertyValue", salesTriggerBtmlConversionCampaignId);
                salesTriggerBtmlConversionCampaignIdGV.store();
            }

            GenericValue salesTriggerBtmlConversionMatDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.matDtNoOfMonths"))
                .queryFirst();
            if (salesTriggerBtmlConversionMatDtNoOfMonthsGV != null && salesTriggerBtmlConversionMatDtNoOfMonthsGV.size() > 0) {
                salesTriggerBtmlConversionMatDtNoOfMonthsGV.set("propertyValue", salesTriggerBtmlConversionMatDtNoOfMonths);
                salesTriggerBtmlConversionMatDtNoOfMonthsGV.store();
            }

            GenericValue salesTriggerBtmlConversionSchmCdeGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.conversion.schmCde"))
                .queryFirst();
            if (salesTriggerBtmlConversionSchmCdeGV != null && salesTriggerBtmlConversionSchmCdeGV.size() > 0) {
                salesTriggerBtmlConversionSchmCdeGV.set("propertyValue", salesTriggerBtmlConversionSchmCde);
                salesTriggerBtmlConversionSchmCdeGV.store();
            }

            //Sales Trigger Conversion Btml
            GenericValue salesTriggerBtmlExtractionMatDtNoOfMonthsGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.extraction.matDtNoOfMonths"))
                .queryFirst();
            if (salesTriggerBtmlExtractionMatDtNoOfMonthsGV != null && salesTriggerBtmlExtractionMatDtNoOfMonthsGV.size() > 0) {
                salesTriggerBtmlExtractionMatDtNoOfMonthsGV.set("propertyValue", salesTriggerBtmlExtractionMatDtNoOfMonths);
                salesTriggerBtmlExtractionMatDtNoOfMonthsGV.store();
            }

            GenericValue salesTriggerBtmlExtractionSchmCdeGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "salesTrigger.btml.extraction.schmCde"))
                .queryFirst();
            if (salesTriggerBtmlExtractionSchmCdeGV != null && salesTriggerBtmlExtractionSchmCdeGV.size() > 0) {
                salesTriggerBtmlExtractionSchmCdeGV.set("propertyValue", salesTriggerBtmlExtractionSchmCde);
                salesTriggerBtmlExtractionSchmCdeGV.store();
            }

            //Fixed Expiry
            GenericValue fixedDepositFDAmountGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.fdamount"))
                .queryFirst();
            if (fixedDepositFDAmountGV != null && fixedDepositFDAmountGV.size() > 0) {
                fixedDepositFDAmountGV.set("propertyValue", fixedDepositFDAmount);
                fixedDepositFDAmountGV.store();
            }

            GenericValue fixedDepositRPDesignationGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpDesignation"))
                .queryFirst();
            if (fixedDepositRPDesignationGV != null && fixedDepositRPDesignationGV.size() > 0) {
                fixedDepositRPDesignationGV.set("propertyValue", fixedDepositRPDesignation);
                fixedDepositRPDesignationGV.store();
            }

            GenericValue fixedDepositRPIdentitynoGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpIdentityno"))
                .queryFirst();
            if (fixedDepositRPIdentitynoGV != null && fixedDepositRPIdentitynoGV.size() > 0) {
                fixedDepositRPIdentitynoGV.set("propertyValue", fixedDepositRPIdentityno);
                fixedDepositRPIdentitynoGV.store();
            }

            GenericValue fixedDepositRPTypeGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.rpType"))
                .queryFirst();
            if (fixedDepositRPTypeGV != null && fixedDepositRPTypeGV.size() > 0) {
                fixedDepositRPTypeGV.set("propertyValue", fixedDepositRPType);
                fixedDepositRPTypeGV.store();
            }

            GenericValue fixedDepositConstitutionGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.constitution"))
                .queryFirst();
            if (fixedDepositConstitutionGV != null && fixedDepositConstitutionGV.size() > 0) {
                fixedDepositConstitutionGV.set("propertyValue", fixedDepositConstitution);
                fixedDepositConstitutionGV.store();
            }

            GenericValue fixedDepositIbgCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.ibgCustomField"))
                .queryFirst();
            if (fixedDepositIbgCustomFieldGV != null && fixedDepositIbgCustomFieldGV.size() > 0) {
                fixedDepositIbgCustomFieldGV.set("propertyValue", fixedDepositIbgCustomField);
                fixedDepositIbgCustomFieldGV.store();
            }

            GenericValue fixedDepositCustomFieldGV = EntityQuery.use(delegator).from("HdpConfiguration")
                .where(EntityCondition.makeCondition("propertyName", EntityOperator.EQUALS, "fixed.deposit.expiry.extraction.customField"))
                .queryFirst();
            if (fixedDepositCustomFieldGV != null && fixedDepositCustomFieldGV.size() > 0) {
                fixedDepositCustomFieldGV.set("propertyValue", fixedDepositCustomField);
                fixedDepositCustomFieldGV.store();
            }

        } catch (Exception e) {
            Debug.log("Error while updating configuration" + e.getMessage());
            request.setAttribute("_ERROR_MESSAGE_", "Error while updating configuration "+ e.getMessage());
            return "error";
        }
        request.setAttribute("_EVENT_MESSAGE_", "Configuration value updated successfully.");
        return "success";
    }

}
