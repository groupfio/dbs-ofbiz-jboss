/**
 * 
 */
package org.groupfio.data.importer.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.groupfio.data.importer.constants.DataImporterConstants.ProcessorType;
import org.groupfio.data.importer.processor.Processor;
import org.groupfio.data.importer.processor.ProcessorFactory;
import org.groupfio.data.importer.util.DataUtil;
import org.groupfio.data.importer.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.StringUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class HadoopContactImportService {

	private static final String MODULE = HadoopContactImportService.class.getName();
    
    public static Map importContacts(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String contactIds = (String) context.get("contactIds");
    	String accountIds = (String) context.get("accountIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
			EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_NOT_PROC"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_FAILED"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));

			EntityCondition mainCond = null;
			mainCond = EntityCondition.makeCondition(EntityOperator.AND,
					//EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, batchId),
					statusCond);
			
			if (UtilValidate.isNotEmpty(accountIds)) {
				List<String> accountIdList = Arrays.asList(accountIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("lcin", EntityOperator.IN, accountIdList),
						mainCond);
			}
			
			if (UtilValidate.isNotEmpty(contactIds)) {
				List<String> contactIdList = Arrays.asList(contactIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("rpIdentityno", EntityOperator.IN, contactIdList),
						mainCond);
			}
			
			List<GenericValue> importDataList = delegator.findList("HdpContact", mainCond, null, null,
					null, false);
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				Map<String, Object> importContext = new HashMap<String, Object>();
	    		
	    		importContext.put("userLogin", userLogin);
	    		importContext.put("initialResponsiblePartyId", "admin");
	    		importContext.put("organizationPartyId", "Company");
	    		//importContext.put("isTrackImportStatus", false);
				
				importContext.put("importDatas", importDataList);
				Map<String, Object> importResult = dispatcher.runSync("importContacts", importContext);
				
				if (!ServiceUtil.isError(importResult)) {
					
					importDataList = (List<GenericValue>) importResult.get("importedDataList");
					
					if (UtilValidate.isNotEmpty(importDataList)) {
					
						GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
						SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);
					
						Map<String, Object> segmentationHeaderConfigList = new LinkedHashMap<String, Object>();
						segmentationHeaderConfigList.put("contact", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_contact", "SEGMENTATION"));
						
						Map<String, Object> attributeHeaderConfigList = new LinkedHashMap<String, Object>();
						attributeHeaderConfigList.put("contact", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_contact", "CUSTOM_FIELD"));
						
						Map<String, Object> economicMetricHeaderConfigList = new LinkedHashMap<String, Object>();
						economicMetricHeaderConfigList.put("contact", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_contact", "ECONOMIC_METRIC"));
						
						Map<String, Object> partyRelFilterContext = new HashMap<String, Object>();
						partyRelFilterContext.put("roleTypeIdFrom", "CONTACT");
						partyRelFilterContext.put("roleTypeIdTo", "ACCOUNT");
						partyRelFilterContext.put("partyRelationshipTypeId", "CONTACT_REL_INV");
						
						for (GenericValue data : importDataList) {
							
							String accountId = data.getString("lcin");
							String contactId = data.getString("rpIdentityno");
							
							GenericValue accountParty = delegator.findOne("Party", UtilMisc.toMap("partyId", accountId), false);
							GenericValue contactParty = delegator.findOne("Party", UtilMisc.toMap("partyId", contactId), false);
							if (UtilValidate.isEmpty(accountParty) || UtilValidate.isEmpty(contactParty)) {
								continue;
							}
							
							//GenericValue dataImportCustomer = EntityUtil.getFirst( delegator.findByAnd("DataImportCustomer",UtilMisc.toMap("customerId", contactId), null, false) );
							
							List<GenericValue> toBeStored = FastList.newInstance();
							
							partyRelFilterContext.put("partyIdFrom", contactId);
							partyRelFilterContext.put("partyIdTo", accountId);
							
							Map<String, Object> associationContext = new HashMap<String, Object>();
							associationContext.put("accountPartyId", accountId);
							associationContext.put("contactPartyId", contactId);
							associationContext.put("userLogin", userLogin);
							
							Map<String, Object> associationResult = dispatcher.runSync("crmsfa.assignContactToAccount", associationContext);
							
							String partyRelAssocId = DataUtil.getPartyRelAssocId(delegator, partyRelFilterContext);
							
							if (!ServiceUtil.isError(associationResult)) {
								Debug.logInfo("Successfully Account and Contact associated, accountPartyId="+accountId+", contactPartyId="+contactId, MODULE);
							}
							
							GenericValue person = delegator.findOne("Person", UtilMisc.toMap("partyId", contactId), false);
							if (UtilValidate.isNotEmpty(person)) {
								
								if (UtilValidate.isNotEmpty(data.get("rpBday"))) {
									try {
										person.put("birthDate", new java.sql.Date( UtilDateTime.stringToTimeStamp(data.getString("rpBday"), "yyyy-MM-dd", TimeZone.getDefault(), Locale.getDefault()).getTime() ));
									} catch (Exception e) {
										//person.put("birthDate", new java.sql.Date( UtilDateTime.stringToTimeStamp(data.getString("rpBday"), "dd/MM/yyyy", TimeZone.getDefault(), Locale.getDefault()).getTime() ));
									}
								}
								
								toBeStored.add(person);
							}
							
							// import contact designation [start]
							
							if (UtilValidate.isNotEmpty(data.getString("rpDesignation"))) {
								
								EntityCondition nameCondition = EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("description")), EntityOperator.EQUALS, data.getString("rpDesignation").toUpperCase());
								mainCond = EntityCondition.makeCondition(EntityOperator.AND,
										//EntityCondition.makeCondition("contactId", EntityOperator.EQUALS, data.getString("rpIdentityno")),
										nameCondition);
								
								GenericValue enumCode = EntityUtil.getFirst( delegator.findList("Enumeration", mainCond, null, null, null, true) );
								if (UtilValidate.isNotEmpty(enumCode)) {
									GenericValue designationAssoc = EntityUtil.getFirst(delegator.findByAnd("HdpContactDesignationAssoc", UtilMisc.toMap("accountId", accountId, "contactId", contactId, "designationEnumId", enumCode.getString("enumId"), "partyRelAssocId", partyRelAssocId), null, false));
									if (UtilValidate.isEmpty(designationAssoc)) {
										
										designationAssoc = delegator.makeValue("HdpContactDesignationAssoc");
										
										designationAssoc.put("hdpContactDesignationAssocId", delegator.getNextSeqId("HdpContactDesignationAssoc"));
										
										designationAssoc.put("accountId", accountId);
										designationAssoc.put("contactId", contactId);
										
										designationAssoc.put("designationEnumId", enumCode.getString("enumId"));
										designationAssoc.put("designationName", data.getString("rpDesignation"));
										designationAssoc.put("sequenceNumber", new Long(1));
										designationAssoc.put("partyRelAssocId", partyRelAssocId);
										
										toBeStored.add(designationAssoc);
										
									}
									if (UtilValidate.isNotEmpty(partyRelAssocId)) {
				                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "DESIGNATION", "assocId", designationAssoc.getString("hdpContactDesignationAssocId")), null, false));
				                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
				                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
				                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
				                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
				                    		partyRelAssoc.put("assocTypeId", "DESIGNATION");
				                    		partyRelAssoc.put("assocId", designationAssoc.getString("hdpContactDesignationAssocId"));
				                    		partyRelAssoc.put("solicitationStatus", "Y");
				                    		toBeStored.add(partyRelAssoc);
				                    	}
						         	}
									
									// mark contact as default [start]
									
									if (enumCode.getString("enumId").equals("012")) {
										associationContext = new HashMap<String, Object>();
										associationContext.put("partyIdFrom", contactId);
										associationContext.put("partyId", accountId);
										associationContext.put("roleTypeIdFrom", "CONTACT");
										associationContext.put("roleTypeIdTo", "ACCOUNT");
										associationContext.put("statusId", "PARTY_DEFAULT");
										associationContext.put("isMarketable", "Y");
										associationContext.put("userLogin", userLogin);
										
										associationResult = dispatcher.runSync("crmsfa.updateDefaultContact", associationContext);
										
										if (!ServiceUtil.isError(associationResult)) {
											Debug.logInfo("Successfully Contact marked as default and isMarketable: Y, accountPartyId="+accountId+", contactPartyId="+contactId, MODULE);
										}
										
									} else {
										associationContext = new HashMap<String, Object>();
										associationContext.put("partyIdFrom", contactId);
										associationContext.put("partyId", accountId);
										associationContext.put("roleTypeIdFrom", "CONTACT");
										associationContext.put("roleTypeIdTo", "ACCOUNT");
										associationContext.put("statusId", null);
										associationContext.put("isMarketable", "Y");
										associationContext.put("userLogin", userLogin);
										
										associationResult = dispatcher.runSync("crmsfa.updateDefaultContact", associationContext);
										
										if (!ServiceUtil.isError(associationResult)) {
											Debug.logInfo("Successfully Contact marked as not default and isMarketable: Y, accountPartyId="+accountId+", contactPartyId="+contactId, MODULE);
										}
									}
									
									// mark contact as default [end]
									
								}
								
							}
							
							// import contact designation [end]
							
							// update PRIMARY email [start]
							
							String primaryEmailAddress = UtilValidate.isNotEmpty(data.getString("rpIdealemailaddress")) ? data.getString("rpIdealemailaddress") : data.getString("rpAosEmailaddress");
							
							String partyRelAssocContactMechId = "";
				         	GenericValue emailPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "PRIMARY_EMAIL", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(emailPurpose)){
				         		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", emailPurpose.getString("contactMechId")));
				             	
				         		if (DataUtil.isContactEmailChange(delegator, contactId, partyRelAssocId, data.getString("rpIdealemailaddress"), data.getString("rpAosEmailaddress"))) {
				         			emailContactMech.put("infoString", primaryEmailAddress);
					             	toBeStored.add(emailContactMech);
				         		}
				         		
				             	partyRelAssocContactMechId = emailPurpose.getString("contactMechId");
				         	} else if (!UtilValidate.isEmpty(primaryEmailAddress)) {
				                // make the email address
				                GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", primaryEmailAddress));
				                toBeStored.add(emailContactMech);
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y", "emailValidInd", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = emailContactMech.getString("contactMechId");
				         	}
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "EMAIL", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "EMAIL");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update PRIMARY email [end]
							
							// update IDEAL email [start]
							
							partyRelAssocContactMechId = "";
				         	emailPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "IDEAL_EMAIL_ADDRESS", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(emailPurpose)){
				         		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", emailPurpose.getString("contactMechId")));
				             	emailContactMech.put("infoString", data.getString("rpIdealemailaddress"));
				             	toBeStored.add(emailContactMech);
				             	partyRelAssocContactMechId = emailPurpose.getString("contactMechId");
				         	} else if (!UtilValidate.isEmpty(data.getString("rpIdealemailaddress"))) {
				                // make the email address
				                GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", data.getString("rpIdealemailaddress")));
				                toBeStored.add(emailContactMech);
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y", "emailValidInd", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("IDEAL_EMAIL_ADDRESS", emailContactMech, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = emailContactMech.getString("contactMechId");
				         	}
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "EMAIL", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "EMAIL");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update IDEAL email [end]
				         	
				         	// update PRIMARY mobile [start]
				         	
				         	String primaryPhoneNumber = UtilValidate.isNotEmpty(data.getString("rpIdealMobileno")) ? data.getString("rpIdealMobileno") : data.getString("rpAosMobileno");
				         	
				         	partyRelAssocContactMechId = "";
				         	GenericValue mobilePurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "PRIMARY_PHONE", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(mobilePurpose)){
				         		//GenericValue mobileContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", mobilePurpose.getString("contactMechId")));
				         		String contactMechId = mobilePurpose.getString("contactMechId");
				        		
				        		GenericValue phoneContactMech = EntityUtil.getFirst(delegator.findByAnd("TelecomNumber",  UtilMisc.toMap("contactMechId", contactMechId), null, false));
				        		if (UtilValidate.isNotEmpty(phoneContactMech) && DataUtil.isContactPhoneChange(delegator, contactId, partyRelAssocId, data.getString("rpIdealMobileno"), data.getString("rpAosMobileno"))) {
				        			//phoneContactMech.put("contactMechId", contactMechId);
					        		//phoneContactMech.put("countryCode", entry.getString("primaryPhoneCountryCode"));
					        		//phoneContactMech.put("areaCode", entry.getString("primaryPhoneAreaCode"));
					        		phoneContactMech.put("contactNumber", primaryPhoneNumber);
					        		toBeStored.add(phoneContactMech);
				        		}
				        		partyRelAssocContactMechId = contactMechId;
				         	} else if (!UtilValidate.isEmpty(primaryPhoneNumber)) {
				                // make the mobile no
				                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
				                
				                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, null, null, primaryPhoneNumber, delegator);
				                primaryNumber.put("phoneValidInd", "Y");
				                
				                toBeStored.add(contactMech);
				                toBeStored.add(primaryNumber);
				                
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_PHONE", primaryNumber, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = contactMech.getString("contactMechId");
				            }
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "PHONE", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "PHONE");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update PRIMARY mobile [end]
				         	
				         	// update IDEAL mobile [start]
				         	
				         	partyRelAssocContactMechId = "";
				         	mobilePurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "IDEAL_MOBILE_PHONE", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(mobilePurpose)){
				         		//GenericValue mobileContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", mobilePurpose.getString("contactMechId")));
				         		String contactMechId = mobilePurpose.getString("contactMechId");
				        		
				        		GenericValue phoneContactMech = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", contactMechId), false);
				        		if (UtilValidate.isNotEmpty(phoneContactMech)) {
				        			//phoneContactMech.put("contactMechId", contactMechId);
					        		//phoneContactMech.put("countryCode", entry.getString("primaryPhoneCountryCode"));
					        		//phoneContactMech.put("areaCode", entry.getString("primaryPhoneAreaCode"));
					        		phoneContactMech.put("contactNumber", data.getString("rpIdealMobileno"));
					        		toBeStored.add(phoneContactMech);
				        		}
				        		partyRelAssocContactMechId = contactMechId;
				         	} else if (!UtilValidate.isEmpty(data.getString("rpIdealMobileno"))) {
				                // make the mobile no
				                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
				                
				                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, null, null, data.getString("rpIdealMobileno"), delegator);
				                primaryNumber.put("phoneValidInd", "Y");
				                
				                toBeStored.add(contactMech);
				                toBeStored.add(primaryNumber);
				                
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("IDEAL_MOBILE_PHONE", primaryNumber, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = contactMech.getString("contactMechId");
				            }
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "PHONE", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "PHONE");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update IDEAL mobile [end]
				         	
				         	// update AOS email [start]
				         	
				         	partyRelAssocContactMechId = "";
				         	emailPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "AOS_EMAIL_ADDRESS", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(emailPurpose)){
				         		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", emailPurpose.getString("contactMechId")));
				             	emailContactMech.put("infoString", data.getString("rpAosEmailaddress"));
				             	emailContactMech.store();
				             	partyRelAssocContactMechId = emailPurpose.getString("contactMechId");
				         	} else if (!UtilValidate.isEmpty(data.getString("rpAosEmailaddress"))) {
				                // make the email address
				                GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", data.getString("rpAosEmailaddress")));
				                toBeStored.add(emailContactMech);
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y", "emailValidInd", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("AOS_EMAIL_ADDRESS", emailContactMech, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = emailContactMech.getString("contactMechId");
				            }
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "EMAIL", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "EMAIL");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update AOS email [end]
				         	
				         	// update AOS mobile [start]
				         	
				         	partyRelAssocContactMechId = "";
				         	mobilePurpose = DataUtil.getActivePartyContactMechPurpose(delegator, contactId, "AOS_MOBILE_PHONE", partyRelAssocId);
				         	if(UtilValidate.isNotEmpty(mobilePurpose)){
				         		//GenericValue mobileContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", mobilePurpose.getString("contactMechId")));
				         		String contactMechId = mobilePurpose.getString("contactMechId");
				        		
				        		GenericValue phoneContactMech = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", contactMechId), false);
				        		if (UtilValidate.isNotEmpty(phoneContactMech)) {
				        			//phoneContactMech.put("contactMechId", contactMechId);
					        		//phoneContactMech.put("countryCode", entry.getString("primaryPhoneCountryCode"));
					        		//phoneContactMech.put("areaCode", entry.getString("primaryPhoneAreaCode"));
					        		phoneContactMech.put("contactNumber", data.getString("rpAosMobileno"));
					        		toBeStored.add(phoneContactMech);
				        		}
				        		partyRelAssocContactMechId = contactMechId;
				         	} else if (!UtilValidate.isEmpty(data.getString("rpAosMobileno"))) {
				                // make the mobile no
				                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
				                
				                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, null, null, data.getString("rpAosMobileno"), delegator);
				                primaryNumber.put("phoneValidInd", "Y");
				                
				                toBeStored.add(contactMech);
				                toBeStored.add(primaryNumber);
				                
			                    toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", contactId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y")));
			                    toBeStored.add(UtilImport.makeContactMechPurpose("AOS_MOBILE_PHONE", primaryNumber, contactId, UtilDateTime.nowTimestamp(), delegator, partyRelAssocId));
			                    partyRelAssocContactMechId = contactMech.getString("contactMechId");
				            }
				         	if (UtilValidate.isNotEmpty(partyRelAssocId)) {
		                    	GenericValue partyRelAssoc = EntityUtil.getFirst(delegator.findByAnd("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId", partyRelAssocId, "assocTypeId", "PHONE", "assocId", partyRelAssocContactMechId), null, false));
		                    	if (UtilValidate.isEmpty(partyRelAssoc)) {
		                    		partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc");
		                    		partyRelAssoc.put("partyRelAssocId", partyRelAssocId);
		                    		partyRelAssoc.put("assocSeqId", delegator.getNextSeqIdLong("PartyRelationshipAssoc"));
		                    		partyRelAssoc.put("assocTypeId", "PHONE");
		                    		partyRelAssoc.put("assocId", partyRelAssocContactMechId);
		                    		partyRelAssoc.put("solicitationStatus", "Y");
		                    		toBeStored.add(partyRelAssoc);
		                    	}
				         	}
				         	
				         	// update AOS mobile [end]
				         	
				         	importContext.put("importStatusId", data.get("importStatusId"));
							importContext.put("importError", data.get("importError"));
							importContext.put("processedTimestamp", data.get("processedTimestamp"));
							
							Map<String, Object> processorContext = new HashMap<String, Object>();
							
							processorContext.put("dispatcher", dispatcher);
							processorContext.put("delegator", delegator);
							processorContext.put("userLogin", userLogin);
							processorContext.put("data", data);
							processorContext.put("primaryKey", "rpIdentityno");
							processorContext.put("sqlProcessor", sqlProcessor);
							
							importContext.put("headerConfigList", segmentationHeaderConfigList);
							processorContext.put("importContext", importContext);
							Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
							Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
							
							importContext.put("headerConfigList", attributeHeaderConfigList);
							processorContext.put("importContext", importContext);
							Processor processorAttribute = ProcessorFactory.getProcessor(ProcessorType.ATTRIBUTE);
							Map<String, Object> processorAttributeResult = processorAttribute.process(processorContext);
							
							importContext.put("headerConfigList", economicMetricHeaderConfigList);
							processorContext.put("importContext", importContext);
							processorContext.put("markDataAsImported", "true");
							Processor processorEconomicMetirc = ProcessorFactory.getProcessor(ProcessorType.ECONOMIC_METRIC);
							Map<String, Object> processorEconomicMetircResult = processorEconomicMetirc.process(processorContext);
							/*
							data.set("importStatusId", "DATAIMP_IMPORTED");
							data.set("processedTimestamp", UtilDateTime.nowTimestamp());
							data.set("importError", null);
							
							toBeStored.add(data);
				         	*/
							delegator.storeAll(toBeStored);
							
						}
					}
				}
				
			}
				
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account.."));
    	
    	return result;
    	
    }
    
}
