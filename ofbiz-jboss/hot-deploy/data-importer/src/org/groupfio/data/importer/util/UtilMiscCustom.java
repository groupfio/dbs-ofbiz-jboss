/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package org.groupfio.data.importer.util;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilGenerics;

/**
 * UtilMiscCustom - Misc Utility Functions
 */
public class UtilMiscCustom {

	public static final String module = UtilMiscCustom.class.getName();

	public static final BigDecimal ZERO_BD = BigDecimal.ZERO;

	private UtilMiscCustom () {} public static <K, V> Map<String, V> toMap(Object... data) {
		if (data.length == 1 && data[0] instanceof Map) {
			// Logging a warning here because a lot of code misuses this method and that code needs to be fixed.
			//Debug.logWarning("UtilMisc.toMap called with a Map. Use UtilGenerics.checkMap instead.", module);
			return UtilGenerics.<String, V>checkMap(data[0]);
		}
		if (data.length % 2 == 1) {
			IllegalArgumentException e = new IllegalArgumentException("You must pass an even sized array to the toMap method (size = " + data.length + ")");
			Debug.logInfo(e, module);
			throw e;
		}
		Map<String, V> map = new LinkedHashMap<String, V>();
		for (int i = 0; i < data.length;) {
			map.put((String) data[i++], (V) data[i++]);
		}
		return map;
	}

}
