/**
 * 
 */
package org.groupfio.data.importer.service;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.custom.field.service.SegmentService;
import org.groupfio.custom.field.util.DataUtil;
import org.groupfio.data.importer.constants.DataImporterConstants.ProcessorType;
import org.groupfio.data.importer.processor.Processor;
import org.groupfio.data.importer.processor.ProcessorFactory;
import org.groupfio.data.importer.util.DataHelper;
import org.groupfio.data.importer.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.contact.ContactHelper;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class GeneralImportService {

	private static final String MODULE = GeneralImportService.class.getName();
	
    public static Map importLeadAssocs(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String leadIds = (String) context.get("leadIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		//delegator.clearAllCaches();
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("leadIds", leadIds);
    		importContext.put("userLogin", userLogin);
			
			Map<String, Object> importResult = dispatcher.runSync("dataimporter.importLeadSegmentations", importContext);
			
			importResult = dispatcher.runSync("dataimporter.importLeadAttributes", importContext);
			
			importResult = dispatcher.runSync("dataimporter.importLeadEconomicMetrics", importContext);
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported lead assocs.."));
    	
    	return result;
    	
    }
    
    public static Map importContactAssocs(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String contactIds = (String) context.get("contactIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		//delegator.clearAllCaches();
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("contactIds", contactIds);
    		importContext.put("userLogin", userLogin);
			
			Map<String, Object> importResult = dispatcher.runSync("dataimporter.importContactSegmentations", importContext);
			
			importResult = dispatcher.runSync("dataimporter.importContactAttributes", importContext);
			
			importContext.put("markDataAsImported", "true");
			importResult = dispatcher.runSync("dataimporter.importContactEconomicMetrics", importContext);
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported contact assocs.."));
    	
    	return result;
    	
    }
    
}
