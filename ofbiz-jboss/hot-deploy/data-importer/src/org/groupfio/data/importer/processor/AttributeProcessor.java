/**
 * 
 */
package org.groupfio.data.importer.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.data.importer.ResponseCodes;
import org.groupfio.data.importer.constants.DataImporterConstants;
import org.groupfio.data.importer.util.DataHelper;
import org.groupfio.data.importer.util.ParamUtil;
import org.groupfio.data.importer.util.QueryUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

/**
 * @author Sharif
 *
 */
public class AttributeProcessor extends Processor {
	
	private static String MODULE = AttributeProcessor.class.getName();
	
	private static AttributeProcessor instance;
	
	public static synchronized AttributeProcessor getInstance(){
        if(instance == null) {
            instance = new AttributeProcessor();
        }
        return instance;
    }

	@Override
	protected Map<String, Object> doProcess(Map<String, Object> context) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			
			LocalDispatcher dispatcher = (LocalDispatcher) context.get("dispatcher"); 
			Delegator delegator = (Delegator) context.get("delegator");  
			GenericValue userLogin = (GenericValue) context.get("userLogin"); 
			GenericValue data = (GenericValue) context.get("data");  
			Map<String, Object> importContext = (Map<String, Object>) context.get("importContext");
			SQLProcessor sqlProcessor = (SQLProcessor) context.get("sqlProcessor");  
			
			boolean markDataAsImported = ParamUtil.getBoolean(context, "markDataAsImported");
			boolean isIgnoreStatusCondition = ParamUtil.getBoolean(context, "isIgnoreStatusCondition");
			String primaryKey = ParamUtil.getString(context, "primaryKey");
			
			try {
	    		
				String accountId = data.getString(primaryKey);
				String updateQuery = null;
				String primaryKeySqlCondition = null;
				Debug.logInfo("Start process attribute for accountId: "+ accountId+", time: "+UtilDateTime.nowTimestamp(), MODULE);
				if (UtilValidate.isNotEmpty( importContext.get("headerConfigList") )) {
					
					Map<String, List<GenericValue>> headerConfigList = (Map<String, List<GenericValue>>) importContext.get("headerConfigList");
					
					for (Map.Entry<String, List<GenericValue>> entry : headerConfigList.entrySet()) {
						
						String key = entry.getKey();
						List<GenericValue> headerConfigs = entry.getValue();
						
						String entityName = DataImporterConstants.ENTITY_NAME_BY_FILE_TYPE.get(key);
						
						if (UtilValidate.isEmpty(entityName)) {
							continue;
						}
						
						EntityCondition mainCond = null;
						
						if (!isIgnoreStatusCondition) {
							EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
											"DATAIMP_NOT_PROC"),
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
											"DATAIMP_FAILED"),
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));

							mainCond = EntityCondition.makeCondition(EntityOperator.AND,
									org.groupfio.data.importer.util.DataUtil.createRecordIdentifier(entityName, data),
									statusCond
									);
						} else {
							mainCond = EntityCondition.makeCondition(EntityOperator.AND,
									org.groupfio.data.importer.util.DataUtil.createRecordIdentifier(entityName, data)
									);
						}
						
						List<GenericValue> importDataList = delegator.findList(entityName, mainCond, null, null,
								null, false);
						
						for (GenericValue importData : importDataList) {
							
							if (markDataAsImported) {
								primaryKeySqlCondition = QueryUtil.buildPrimaryKeySqlCondition(importData);
								updateQuery = "update "+DataHelper.javaPropToSqlProp(importData.getEntityName())+" set import_status_id='DATAIMP_PROCESSING', processed_timestamp=NOW() where "+primaryKeySqlCondition;
								sqlProcessor.prepareStatement(updateQuery);
								sqlProcessor.executeUpdate(updateQuery);
							}
							
							for (GenericValue headerConfig : headerConfigs) {
								
								String fieldValue = null;
								try {
									fieldValue = importData.getString(DataHelper.sqlPropToJavaProp(headerConfig.getString("hdrName")));
									if (UtilValidate.isEmpty(fieldValue)) {
										continue;
									}
								} catch (Exception e) {
									Debug.logError("error durigng get fieldValue: "+e.getMessage(), MODULE);
									continue;
								}
								
								if (UtilValidate.isNotEmpty(headerConfig.getString("hdrCrmType")) && headerConfig.getString("hdrCrmType").equals("CUSTOM_FIELD")) {
									String hdrCrmSrcField = headerConfig.getString("hdrCrmSrcField");
									
									if (UtilValidate.isNotEmpty(hdrCrmSrcField)) {
										GenericValue attributeField = delegator.findOne("CustomField", UtilMisc.toMap("customFieldId", hdrCrmSrcField), false);
										if (UtilValidate.isNotEmpty(attributeField)) {
											//GenericValue identification = EntityUtil.getFirst( delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyIdentificationTypeId", "EXT_PARTY_ID", "idValue", accountId), null, false) );
											//if (UtilValidate.isNotEmpty(identification)) {
												//String partyId = identification.getString("partyId");
												String partyId = accountId;
												GenericValue associatedEntity = delegator.findOne("CustomFieldValue", UtilMisc.toMap("customFieldId", attributeField.getString("customFieldId"), "partyId", partyId), false);
												
												if (UtilValidate.isEmpty(associatedEntity)) {
													
													associatedEntity = delegator.makeValue("CustomFieldValue");
													
													associatedEntity.put("customFieldId", attributeField.getString("customFieldId"));
													associatedEntity.put("partyId", partyId);
													
													associatedEntity.create();
													
												}
												
												if (attributeField.getString("customFieldType").equals("MULTIPLE")) {
													List<GenericValue> multiValues = delegator.findByAnd("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", attributeField.getString("customFieldId")), null, false);
													for (GenericValue multiValue : multiValues) {
														if (UtilValidate.isNotEmpty(multiValue.getString("fieldValue")) && multiValue.getString("fieldValue").equals(fieldValue)) {
															fieldValue = multiValue.getString("fieldValue");
															break;
														}
													}
												}
												
												associatedEntity.put("fieldValue", fieldValue);
												
												associatedEntity.store();
											//}
										}
									}
								}
							}
							
							if (markDataAsImported) {
								
								/*importData.put("importStatusId", "DATAIMP_IMPORTED");
								//importData.put("importError", null);
								importData.put("processedTimestamp", UtilDateTime.nowTimestamp());*/
								
								updateQuery = "update "+DataHelper.javaPropToSqlProp(importData.getEntityName())+" set import_status_id='DATAIMP_IMPORTED', processed_timestamp=NOW() where "+primaryKeySqlCondition;
								sqlProcessor.prepareStatement(updateQuery);
								sqlProcessor.executeUpdate(updateQuery);
							}
							
						}
						
						//delegator.storeAll(importDataList);
						
					}
					
				}
				Debug.logInfo("End process attribute for accountId: "+ accountId+", time: "+UtilDateTime.nowTimestamp(), MODULE);
			} catch (Exception e) {
				e.printStackTrace();
				Debug.logError("Error process attribute: "+e.getMessage(), MODULE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(DataImporterConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(DataImporterConstants.RESPONSE_MESSAGE, e.getMessage());
			
			return response;
			
		}
		
		response.put(DataImporterConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		
		return response;

	}

}
