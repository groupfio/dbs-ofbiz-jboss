/**
 * 
 */
package org.groupfio.data.importer.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sharif Ul Islam
 * @since June 16, 2015
 *
 */
public class DataImporterConstants {
	
	// Resource bundles	
    public static final String configResource = "responsive-template";
    public static final String uiLabelMap = "ResponsiveTemplateUiLabels";
    
    public static final int DEFAULT_BUFFER_SIZE = 102400;
    public static final int LOCKBOX_ITEM_SEQUENCE_ID_DIGITS = 5;
    
    public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	
	public static final class AppStatus {
        private AppStatus() { }
        public static final String ACTIVATED = "ACTIVATED";
        public static final String DEACTIVATED = "DEACTIVATED";
    }
	
	public static final class SourceInvoked {
        private SourceInvoked() { }
        public static final String API = "API";
        public static final String PORTAL = "PORTAL";
        public static final String UNKNOWN = "UNKNOWN";
    }
	
	public enum ProcessorType {
		ATTRIBUTE("ATTRIBUTE"),
		SEGMENTATION("SEGMENTATION"),
		ECONOMIC_METRIC("ECONOMIC_METRIC"),
		COMBINED("COMBINED"),
		;

		public String value;

		private ProcessorType(String value) {
			this.value = value;
		}
		
	}
	
	public static final Map<String, String> ENTITY_NAME_BY_FILE_TYPE = 
	   		 Collections.unmodifiableMap(new HashMap<String, String>() {{ 
	    	        put("facilities", "HdpFacilities");
	    	        put("caAccount", "HdpCaAccount");
	    	        put("customerSme", "HdpCustomerSme");
	    	        put("fdAccount", "HdpFdAccount");
	    	        put("gcinPpinLin", "HdpGcinGpinLinkage");
	    	        put("loanAccount", "HdpLoanAccount");
	    	        put("tradeAccount", "HdpTradeAccount");
	    	        put("contact", "HdpContact");
	    	        put("lead", "DataImportLead");
	   		 }});
	
	public static final Map<String, String> TABLE_NAME_BY_ENTITY_NAME = 
	   		 Collections.unmodifiableMap(new HashMap<String, String>() {{ 
	    	        put("HdpCustomerSme", "hdp_customer_sme");
	    	        put("HdpCaAccount", "hdp_ca_account");
	    	        put("HdpFacilities", "hdp_facilities");
	    	        put("HdpFdAccount", "hdp_fd_account");
	    	        put("HdpLoanAccount", "hdp_loan_account");
	    	        put("HdpTradeAccount", "hdp_trade_account");
	    	        put("HdpContact", "hdp_contact");
	    	        put("HdpGcinGpinLinkage", "hdp_gcin_gpin_linkage");
	   		 }});
	
}
