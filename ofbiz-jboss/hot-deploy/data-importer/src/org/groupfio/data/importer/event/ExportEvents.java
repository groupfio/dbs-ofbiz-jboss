package org.groupfio.data.importer.event;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fio.crm.export.ExportUtil;
import org.fio.crm.export.ExporterFacade;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.service.LocalDispatcher;

import javolution.util.FastList;

public class ExportEvents {
		
	private static final String module = ExportEvents.class.getName();
	
	@SuppressWarnings("unchecked")
	public static String exportBatchError(HttpServletRequest request,HttpServletResponse response) throws GenericEntityException {

		HttpSession session = request.getSession();
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		String exportType = request.getParameter("exportType");
		
		String executionId = request.getParameter("executionId");
		String exitType = request.getParameter("exitType");
		
		try {
			
			String sortDir = "DESC";
			String orderField = "createdStamp";
			
			List<EntityCondition> conditionsList = new ArrayList<EntityCondition>();
			
			if (UtilValidate.isNotEmpty(executionId) && UtilValidate.isNotEmpty(exitType)) {
				if (exitType.equals("job")) {
					conditionsList.add(EntityCondition.makeCondition("jobExecutionId", EntityOperator.EQUALS, new Long(executionId)));
				} else if (exitType.equals("step")) {
					conditionsList.add(EntityCondition.makeCondition("stepExecutionId", EntityOperator.EQUALS, new Long(executionId)));
				}
			}
			
			EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
			
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			
			List<GenericValue> errorList = delegator.findList("BatchStepErrorLog", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);

			if (UtilValidate.isNotEmpty(errorList)) {
				
				List<Map<String, Object>> resultList = FastList.newInstance();
				
				for (GenericValue error : errorList) {
					
					Map<String, Object> result = new LinkedHashMap<String,Object>();
					
					result.put("Id", error.getString("batchStepErrorLogId"));
					
					result.put("Create Time", UtilValidate.isNotEmpty(error.get("createdStamp")) ? UtilDateTime.timeStampToString(error.getTimestamp("createdStamp"), "dd/MM/yyyy HH:mm", TimeZone.getDefault(), null) : "");
					
					result.put("Error Message", error.getString("errorMessage"));
					
					resultList.add(result);
					
				}
				
				String fileName = "BatchErrorExport_"+exitType+"_"+executionId+"_"+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+ExportUtil.getFileExtension(exportType);
				String location = ComponentConfig.getRootLocation("data-importer")+"/webapp/di-resource/template";
				String delimiter = ",";
				
				Map<String, Object> exportContext = new HashMap<String, Object>();
				
				exportContext.put("delegator", delegator);
				exportContext.put("rows", resultList);
				exportContext.put("headers", null);
				exportContext.put("fileName", fileName);
				exportContext.put("location", location);
				exportContext.put("delimiter", delimiter);
				exportContext.put("isHeaderRequird", true);
				
				exportContext.put("exportType", exportType);
				
				ExporterFacade.exportReport(exportContext);
				
				Thread.sleep(1000);
				
				String filePath = location+File.separatorChar+fileName;
				
				ExporterFacade.downloadReport(request, response, filePath, exportType);
				
				boolean isdelete = true;
				if(isdelete) {
					File file = new File(filePath);
					file.delete();
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), module);
			return "error";
		}

		return "success";
	}
}
