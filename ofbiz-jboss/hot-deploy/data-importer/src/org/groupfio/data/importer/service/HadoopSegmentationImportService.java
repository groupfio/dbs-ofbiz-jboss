/**
 * 
 */
package org.groupfio.data.importer.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.groupfio.data.importer.constants.DataImporterConstants.ProcessorType;
import org.groupfio.data.importer.processor.Processor;
import org.groupfio.data.importer.processor.ProcessorFactory;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.StringUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
public class HadoopSegmentationImportService {

	private static final String MODULE = HadoopSegmentationImportService.class.getName();
	
    public static Map importSegmentations(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	String gcins = (String) context.get("gcins");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		if (UtilValidate.isNotEmpty(gcins)) {
    			
    			EntityCondition mainCond = null;
    			List<String> gcinList = Arrays.asList(gcins.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("gcin", EntityOperator.IN, gcinList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("lcin");
				List<GenericValue> importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null, null, false);
				List<String> lcinList = EntityUtil.getFieldListFromEntityList(importDataList, "lcin", true);
				accountIds = StringUtil.join(lcinList, ",");
    		}
    		
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("organizationPartyId", "Company");
    		importContext.put("userLogin", userLogin);
    		
    		List<GenericValue> importDataList = null;
			EntityCondition mainCond = null;
			
			if (UtilValidate.isNotEmpty(accountIds)) {
				List<String> accountIdList = Arrays.asList(accountIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("lcin", EntityOperator.IN, accountIdList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("lcin", "importStatusId", "importError", "processedTimestamp");
				importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null,
						null, false);
			}
			
			// import other hdr crm type data [start]
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
				SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);
				
				Map<String, Object> headerConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "SEGMENTATION");
				
				importContext.put("headerConfigList", headerConfigList);
				
				for (GenericValue data : importDataList) {
					
					String accountId = data.getString("lcin");
					
					//HadoopImportService.importSegmentation(dispatcher, delegator, userLogin, data);
					
					importContext.put("importStatusId", data.get("importStatusId"));
					importContext.put("importError", data.get("importError"));
					importContext.put("processedTimestamp", data.get("processedTimestamp"));
					
					Map<String, Object> processorContext = new HashMap<String, Object>();
					
					processorContext.put("dispatcher", dispatcher);
					processorContext.put("delegator", delegator);
					processorContext.put("userLogin", userLogin);
					processorContext.put("data", data);
					processorContext.put("importContext", importContext);
					processorContext.put("markDataAsImported", context.get("markDataAsImported"));
					processorContext.put("primaryKey", "lcin");
					processorContext.put("sqlProcessor", sqlProcessor);
					
					Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
					Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
					
				}
				
				sqlProcessor.close();
				
			}
			
			// import other hdr crm type data [end]
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported segmentations.."));
    	
    	return result;
    	
    }
    
    public static Map importLeadSegmentations(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String leadIds = (String) context.get("leadIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("organizationPartyId", "Company");
    		importContext.put("userLogin", userLogin);
    		
    		List<GenericValue> importDataList = null;
			EntityCondition mainCond = null;
			
			if (UtilValidate.isNotEmpty(leadIds)) {
				List<String> leadIdList = Arrays.asList(leadIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("leadId", EntityOperator.IN, leadIdList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("leadId", "primaryPartyId", "importStatusId", "importError", "processedTimestamp");
				importDataList = delegator.findList("DataImportLead", mainCond, fieldsToSelect, null,
						null, false);
			}
			
			// import other hdr crm type data [start]
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
				SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);
				
				Map<String, Object> segmentationHeaderConfigList = new LinkedHashMap<String, Object>();
				segmentationHeaderConfigList.put("lead", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_lead", "SEGMENTATION"));
				
				for (GenericValue data : importDataList) {
					
					String leadId = data.getString("leadId");
					
					//HadoopImportService.importSegmentation(dispatcher, delegator, userLogin, data);
					
					importContext.put("importStatusId", data.get("importStatusId"));
					importContext.put("importError", data.get("importError"));
					importContext.put("processedTimestamp", data.get("processedTimestamp"));
					
					Map<String, Object> processorContext = new HashMap<String, Object>();
					
					processorContext.put("dispatcher", dispatcher);
					processorContext.put("delegator", delegator);
					processorContext.put("userLogin", userLogin);
					processorContext.put("data", data);
					processorContext.put("importContext", importContext);
					processorContext.put("markDataAsImported", context.get("markDataAsImported"));
					processorContext.put("primaryKey", "primaryPartyId");
					processorContext.put("isIgnoreStatusCondition", true);
					processorContext.put("sqlProcessor", sqlProcessor);
					
					importContext.put("headerConfigList", segmentationHeaderConfigList);
					processorContext.put("importContext", importContext);
					Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
					Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
					
				}
				
				sqlProcessor.close();
				
			}
			
			// import other hdr crm type data [end]
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported Lead segmentations.."));
    	
    	return result;
    	
    }
    
    public static Map importContactSegmentations(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String contactIds = (String) context.get("contactIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("organizationPartyId", "Company");
    		importContext.put("userLogin", userLogin);
    		
    		List<GenericValue> importDataList = null;
			EntityCondition mainCond = null;
			
			if (UtilValidate.isNotEmpty(contactIds)) {
				List<String> contactIdList = Arrays.asList(contactIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("rpIdentityno", EntityOperator.IN, contactIdList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("lcin", "rpIdentityno", "rpDesignation", "importStatusId", "importError", "processedTimestamp");
				importDataList = delegator.findList("HdpContact", mainCond, fieldsToSelect, null,
						null, false);
			}
			
			// import other hdr crm type data [start]
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
				SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);
				
				Map<String, Object> segmentationHeaderConfigList = new LinkedHashMap<String, Object>();
				segmentationHeaderConfigList.put("contact", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_contact", "SEGMENTATION"));
				
				for (GenericValue data : importDataList) {
					
					String accountId = data.getString("lcin");
					String contactId = data.getString("rpIdentityno");
					
					//HadoopImportService.importSegmentation(dispatcher, delegator, userLogin, data);
					
					importContext.put("importStatusId", data.get("importStatusId"));
					importContext.put("importError", data.get("importError"));
					importContext.put("processedTimestamp", data.get("processedTimestamp"));
					
					Map<String, Object> processorContext = new HashMap<String, Object>();
					
					processorContext.put("dispatcher", dispatcher);
					processorContext.put("delegator", delegator);
					processorContext.put("userLogin", userLogin);
					processorContext.put("data", data);
					processorContext.put("importContext", importContext);
					processorContext.put("markDataAsImported", context.get("markDataAsImported"));
					processorContext.put("primaryKey", "rpIdentityno");
					processorContext.put("isIgnoreStatusCondition", true);
					processorContext.put("sqlProcessor", sqlProcessor);
					
					importContext.put("headerConfigList", segmentationHeaderConfigList);
					processorContext.put("importContext", importContext);
					Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
					Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
					
				}
				
				sqlProcessor.close();
			}
			
			// import other hdr crm type data [end]
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported Customer segmentations.."));
    	
    	return result;
    	
    }
    
}
