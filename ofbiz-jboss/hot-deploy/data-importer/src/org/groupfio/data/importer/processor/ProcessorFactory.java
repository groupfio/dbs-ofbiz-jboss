/**
 * 
 */
package org.groupfio.data.importer.processor;

import org.groupfio.data.importer.constants.DataImporterConstants.ProcessorType;

/**
 * @author Sharif
 *
 */
public class ProcessorFactory {

	public static Processor getProcessor(ProcessorType type) {
		
		Processor processor = null;
		
		switch (type) {
			case SEGMENTATION:
				processor = new SegmentationProcessor();
				break;
			case ATTRIBUTE:
				processor = new AttributeProcessor();
				break;
			case ECONOMIC_METRIC:
				processor = new EconomicMetricProcessor();
				break;
			case COMBINED:
				processor = new CombinedProcessor();
				break;	
			default:
				break;
		}
		
		return processor;
	}
	
}
