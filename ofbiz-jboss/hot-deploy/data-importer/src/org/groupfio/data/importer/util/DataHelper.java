/**
 * 
 */
package org.groupfio.data.importer.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.data.importer.util.DataHelper;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;

/**
 * @author Sharif
 *
 */
public class DataHelper {
	
	private static String MODULE = DataHelper.class.getName();
	
	public static Map getDropDownOptions(List<GenericValue> entityList, String keyField, String desField){
		Map<String, Object> options = new HashMap<String, Object>();
		for (GenericValue entity : entityList) {
			options.put(entity.getString(keyField), entity.getString(desField));
		}
		return options;
	}
	
	public static Map getSupplierList(Delegator delegator){
		
		Map<String, Object> suppliers = new HashMap<String, Object>();
		try {
			List<GenericValue> supplierList = delegator.findByAnd("PartyRole", UtilMisc.toMap("roleTypeId", "SUPPLIER"), null, false);
			for (GenericValue supplier : supplierList) {
				suppliers.put(supplier.getString("partyId"), PartyHelper.getPartyName(supplier) + " [" + supplier.getString("partyId") + "]");
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return suppliers;
		
	}
	
	public static Map getServiceList(Delegator delegator){
		
		Map<String, Object> services = new HashMap<String, Object>();
		try {
			List<GenericValue> serviceList = delegator.findByAnd("ServiceName", UtilMisc.toMap("componentName", "custom-field"), null, false);
			for (GenericValue service : serviceList) {
				services.put(service.getString("serviceName"), " [ " + service.getString("serviceName") + " ] - " + service.getString("description"));
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return services;
		
	}
	
	public static String getServiceDescription(Delegator delegator, String serviceName, String componentName){
		try {
			
			if (UtilValidate.isEmpty(serviceName) || UtilValidate.isEmpty(componentName)) {
				return null;
			}
			
			GenericValue service = EntityUtil.getFirst( delegator.findByAnd("ServiceName", UtilMisc.toMap("serviceName", serviceName, "componentName", componentName), null, false) );
			if (UtilValidate.isNotEmpty(service)) {
				return service.getString("description");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String sqlPropToJavaProp(String prop) {
		if (UtilValidate.isNotEmpty(prop)) {
			//String prop = "hp_due_wthin_1_yr_amt";
			prop = prop.toLowerCase();
			prop = prop.replace("_1_", "1");
			String convertedString = "";
			for (int i = 0; i < prop.length(); i++) {
				if (prop.charAt(i) == '_') {
					convertedString += (""+prop.charAt(++i)).toUpperCase();
				} else {
					convertedString += prop.charAt(i);
				}
			}
			return convertedString;
		}
		return prop;
	}
	
	public static String javaPropToSqlProp(String prop) {
		if (UtilValidate.isNotEmpty(prop)) {
			for (int i = 0; i < prop.length(); i++) {
				if (Character.isUpperCase(prop.charAt(i))) {
					String additionalChar = (i == 0) ? "" : "_";
					prop = prop.replace("" + prop.charAt(i), additionalChar + ("" + prop.charAt(i)).toLowerCase());
				}
	
			}
		}
		return prop;
	}
	
	public static GenericValue findPartyByEmail(Delegator delegator, String emailAddress) {
		
		try {
			
			GenericValue contactMech = EntityUtil.getFirst(delegator.findByAnd("ContactMech", UtilMisc.toMap("contactMechTypeId", "EMAIL_ADDRESS", "infoString", emailAddress), null, true));
			if (UtilValidate.isNotEmpty(contactMech)) {
				
				EntityCondition mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("contactMechId", EntityOperator.EQUALS, contactMech.getString("contactMechId")),
						EntityUtil.getFilterByDateExpr()
						);
				
				GenericValue partyContactMech = EntityUtil.getFirst( delegator.findList("PartyContactMech", mainCond, null, null, null, true) );
				if (UtilValidate.isNotEmpty(partyContactMech)) {
					GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyContactMech.getString("partyId")), true);
					return party;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Map getPositionModelList(Delegator delegator, String modelType){
		
		Map<String, Object> modelList = new HashMap<String, Object>();
		try {
			
			EntityCondition conditions = null;
			
			if (UtilValidate.isNotEmpty(modelType)) {
				conditions = EntityCondition.makeCondition("tableName", EntityOperator.EQUALS, modelType);
			} else {
				conditions = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("tableName", EntityOperator.EQUALS, "DataImportEmplPosition")
				), EntityOperator.OR);
			}
			
			List<GenericValue> etlSourceTable = delegator.findList("EtlSourceTable", conditions, null, null, null, false);
			if(UtilValidate.isNotEmpty(etlSourceTable)){
				List<String> listNameList = EntityUtil.getFieldListFromEntityList(etlSourceTable, "listName", true);
				
				if (UtilValidate.isNotEmpty(listNameList)) {
					for (String modelName : listNameList) {
						GenericValue etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelName", modelName), null, false) );
						if (UtilValidate.isNotEmpty(etlModel) && UtilValidate.isEmpty(etlModel.getString("isExport"))) {
							modelList.put(etlModel.getString("modelId"), modelName);
						}
					}
				}
				
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return modelList;
		
	}
	
}
