/**
 * 
 */
package org.groupfio.data.importer.processor;

import java.util.HashMap;
import java.util.Map;

import org.ofbiz.base.util.Debug;

/**
 * @author Sharif
 *
 */
public abstract class Processor {

	private static String MODULE = Processor.class.getName();
	
	private Map<String, Object> context;

	protected abstract Map<String, Object> doProcess(Map<String, Object> context) throws Exception;

	public Map<String, Object> process(Map<String, Object> context){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = doProcess(context);
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		return result;
	}
	
}
