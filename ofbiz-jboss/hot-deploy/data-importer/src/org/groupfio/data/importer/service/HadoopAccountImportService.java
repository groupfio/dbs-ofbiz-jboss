/**
 * 
 */
package org.groupfio.data.importer.service;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.groupfio.data.importer.constants.DataImporterConstants.ProcessorType;
import org.groupfio.data.importer.processor.Processor;
import org.groupfio.data.importer.processor.ProcessorFactory;
import org.groupfio.data.importer.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.StringUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class HadoopAccountImportService {

	private static final String MODULE = HadoopAccountImportService.class.getName();
	
	public static Map testService(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	//String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		System.out.println("context>>>> "+context);
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account.."));
    	
    	return result;
    	
    }
	
	public static Map importAccounts(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
			EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_NOT_PROC"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_FAILED"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));

			EntityCondition mainCond = null;
			mainCond = EntityCondition.makeCondition(EntityOperator.AND,
					//EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, batchId),
					statusCond);
			
			if (UtilValidate.isNotEmpty(accountIds)) {
				List<String> accountIdList = Arrays.asList(accountIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("accountId", EntityOperator.IN, accountIdList),
						mainCond);
			}
			
			List<GenericValue> importDataList = delegator.findList("DataImportAccount", mainCond, null, null,
					null, false);
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				Map<String, Object> importContext = new HashMap<String, Object>();
	    		importContext.put("organizationPartyId", "Company");
	    		//importContext.put("isTrackImportStatus", false);
	    		importContext.put("userLogin", userLogin);
	    		
	    		importContext.put("importDatas", importDataList);
				
	    		Debug.log("start importAccounts process time: "+UtilDateTime.nowTimestamp(), MODULE);
				Map<String, Object> importResult = dispatcher.runSync("importAccounts", importContext);
				Debug.log("end   importAccounts process time: "+UtilDateTime.nowTimestamp(), MODULE);
				
				if (!ServiceUtil.isError(importResult)) {
					
					importDataList = (List<GenericValue>) importResult.get("importedDataList");
					
					if (UtilValidate.isNotEmpty(importDataList)) {
						Map<String, Object> processorContext = new HashMap<String, Object>();
						
						processorContext.put("dispatcher", dispatcher);
						processorContext.put("delegator", delegator);
						processorContext.put("userLogin", userLogin);
						
						for (GenericValue data : importDataList) {
							
							mainCond = EntityCondition.makeCondition("lcin", EntityOperator.EQUALS, data.getString("accountId"));
							
							Set<String> fieldsToSelect = UtilMisc.toSet(Arrays.asList("lcin", "dbsIndustryCode", "constitutionCode", "importStatusId", "rm1bankid", "rm", "cntryCde", "importError", "processedTimestamp"));
							GenericValue importData = EntityUtil.getFirst( delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null,
									null, false));
							
							if (UtilValidate.isNotEmpty(importData)) {
								processorContext.put("data", importData);
								importAccountAdditional(processorContext);
							}
							
						}
					}
					
				}
				
			}
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account.."));
    	
    	return result;
    	
    }
    
    /*public static Map importAccounts(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("organizationPartyId", "Company");
    		//importContext.put("isTrackImportStatus", false);
    		importContext.put("userLogin", userLogin);
			
    		Debug.log("start importAccounts process time: "+UtilDateTime.nowTimestamp(), MODULE);
			Map<String, Object> importResult = dispatcher.runSync("importAccounts", importContext);
			Debug.log("end   importAccounts process time: "+UtilDateTime.nowTimestamp(), MODULE);
			
			if (!ServiceUtil.isError(importResult)) {
				
				EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
								"DATAIMP_NOT_PROC"),
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
								"DATAIMP_FAILED"),
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));

				EntityCondition mainCond = null;
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						//EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, batchId),
						statusCond);
				
				if (UtilValidate.isNotEmpty(accountIds)) {
					List<String> accountIdList = Arrays.asList(accountIds.split(","));
					mainCond = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("lcin", EntityOperator.IN, accountIdList),
							mainCond);
				}
				
				List<GenericValue> importDataList = delegator.findList("HdpCustomerSme", mainCond, null, null,
						null, false);
				
				// import other hdr crm type data [start]
				
				if (UtilValidate.isNotEmpty(importDataList)) {
					
					Debug.log("start HdpCustomerSme process time: "+UtilDateTime.nowTimestamp(), MODULE);
//					
//					
//					Map<String, Object> segmentationHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "SEGMENTATION");
//					Map<String, Object> attributeHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "CUSTOM_FIELD");
//					Map<String, Object> economicMetricHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "ECONOMIC_METRIC");
//					
					for (GenericValue data : importDataList) {
						
						String accountId = data.getString("lcin");
						
						Timestamp importTimestamp = UtilDateTime.nowTimestamp();
//						GenericValue dataImportAccount = EntityUtil.getFirst( delegator.findByAnd("DataImportAccount",UtilMisc.toMap("accountId", accountId), null, true) );
//						if (UtilValidate.isNotEmpty(dataImportAccount) && UtilValidate.isNotEmpty(dataImportAccount.get("processedTimestamp"))) {
//							importTimestamp = dataImportAccount.getTimestamp("processedTimestamp");
//						}
						
						GenericValue accountParty = delegator.findOne("Party", UtilMisc.toMap("partyId", accountId), true);
						if (UtilValidate.isEmpty(accountParty)) {
							continue;
						}
						
						List<GenericValue> toBeStored = FastList.newInstance();
						
						// store industry code [start]
						
						GenericValue partySuppData = delegator.findOne("PartySupplementalData", UtilMisc.toMap("partyId", accountId), false);
						if (UtilValidate.isNotEmpty(partySuppData)) {
							if (UtilValidate.isNotEmpty(data.getString("dbsIndustryCode"))) {
								GenericValue enumCode = EntityUtil.getFirst( delegator.findByAnd("Enumeration", UtilMisc.toMap("enumCode", data.getString("dbsIndustryCode")), null, true) );
								if (UtilValidate.isNotEmpty(enumCode)) {
									partySuppData.put("industryEnumId", enumCode.getString("enumId"));
								}
							}
							
							if (UtilValidate.isNotEmpty(data.getString("constitutionCode"))) {
								GenericValue enumCode = EntityUtil.getFirst( delegator.findByAnd("Enumeration", UtilMisc.toMap("enumCode", data.getString("constitutionCode")), null, true) );
								if (UtilValidate.isNotEmpty(enumCode)) {
									partySuppData.put("ownershipEnumId", enumCode.getString("enumId"));
								}
							}
							
							toBeStored.add(partySuppData);
						}
						
						// store industry code [end]
						
						// RM party import [start]
						
						if (UtilValidate.isNotEmpty(data.getString("rm"))) {
							
							String accountRmEmailId = UtilImport.getAccountRmEmailId(data);
							//String accountRmEmailId = data.getString("rmEmail");
							
							GenericValue rmParty = DataHelper.findPartyByEmail(delegator, accountRmEmailId);
							String rmPartyId = null;
							if (UtilValidate.isEmpty(rmParty)) {
								
								rmPartyId = delegator.getNextSeqId("Party");
								String firstName = "";
								String lastName = "";
								String rmFullName = data.getString("rm").split("/")[0];
								
								if (UtilValidate.isNotEmpty(rmFullName)) {
						        	String[] tokens = rmFullName.split(" ", 2);
						        	if (tokens.length > 1) {
						        		firstName = tokens[0];
						        		lastName = tokens[1];
						        	} else {
						        		lastName = tokens[0];
						        	}
						        }
								
								toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("CUSTOMER", "BILL_TO_CUSTOMER"), delegator));
					            GenericValue person = delegator.makeValue("Person", UtilMisc.toMap("partyId", rmPartyId, "firstName", firstName, "lastName", lastName));
					            toBeStored.add(person);
					            
					            toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("ACCOUNT_MANAGER", "EMPLOYEE"), delegator));
					            Map<String, Object> partyRelationship = UtilMisc.toMap("partyIdTo", accountId, "roleTypeIdTo", "ACCOUNT", "partyIdFrom", rmPartyId, "roleTypeIdFrom", "ACCOUNT_MANAGER", "partyRelationshipTypeId", "RESPONSIBLE_FOR", "fromDate", importTimestamp);
				                partyRelationship.put("securityGroupId", "ACCOUNT_OWNER");
				                toBeStored.add(delegator.makeValue("PartyRelationship", partyRelationship));
					            
					            GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", accountRmEmailId));
					            toBeStored.add(emailContactMech);
					            if (rmPartyId != null) {
					                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
					                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, rmPartyId, importTimestamp, delegator));
					                //toBeStored.add(UtilImport.makePartySupplementalData(partySupplementalData, personPartyId, "primaryEmailId", emailContactMech, delegator));
					            }
					            
					            GenericValue partyRelationshipcreate = delegator.makeValue("PartyRelationship");
			                    partyRelationshipcreate.set("partyIdFrom", accountId);
			                    partyRelationshipcreate.set("partyIdTo", rmPartyId);
			                    partyRelationshipcreate.set("roleTypeIdFrom", "ACCOUNT");
			                    partyRelationshipcreate.set("roleTypeIdTo", "ACCOUNT_MANAGER");
			                    partyRelationshipcreate.set("securityGroupId", "ACCOUNT_OWNER");
			                    partyRelationshipcreate.set("fromDate", UtilDateTime.nowTimestamp());
			                    partyRelationshipcreate.set("partyRelationshipTypeId", "RESPONSIBLE_FOR");
			                    
			                    toBeStored.add(partyRelationshipcreate);
			                    
							} else {
								rmPartyId = rmParty.getString("partyId");
								
								Map<String, Object> associationContext = new HashMap<String, Object>();
								associationContext.put("partyId", accountId);
								associationContext.put("roleTypeIdFrom", "ACCOUNT");
								associationContext.put("accountPartyId", rmPartyId);
								associationContext.put("userLogin", userLogin);
								
								Map<String, Object> associationResult = dispatcher.runSync("crmsfa.updatePersonResponsibleFor", associationContext);
								
								if (!ServiceUtil.isError(associationResult)) {
									Debug.logInfo("Successfully Changed Account Responsible For, accountPartyId="+accountId+", contactPartyId="+rmPartyId, MODULE);
								}
							}
							
							// create userLogin for RM party [start]
		                    
		                    GenericValue rmUserLogin = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", accountRmEmailId), true);
		                    if (UtilValidate.isEmpty(rmUserLogin)) {
		                    	
		                    	rmUserLogin = delegator.makeValue("UserLogin");
		                    	
		                    	EntityCondition condition = EntityCondition.makeCondition(EntityOperator.OR,
		                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoId")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
		                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoName")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
		                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoCode")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
		                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("abbreviation")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase())
		                       			);                       	
		        				
		        				GenericValue geoEntity = EntityUtil.getFirst( delegator.findList("Geo", condition, null, UtilMisc.toList("-createdStamp"), null, true) );
		                       	if (UtilValidate.isNotEmpty(geoEntity)) {
		                       		rmUserLogin.put("countryGeoId", geoEntity.getString("geoId"));
		                       	}
		                    	
		                    	rmUserLogin.put("userLoginId", accountRmEmailId);
		                    	rmUserLogin.put("partyId", rmPartyId);
		                    	rmUserLogin.put("currentPassword", "{SHA}47b56994cbc2b6d10aa1be30f70165adb305a41a");
		                    	rmUserLogin.put("enabled", "Y");
		                    	toBeStored.add(rmUserLogin);
		                    }
		                    
		                    GenericValue makerChecker = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "security", "systemPropertyId", rmUserLogin.getString("userLoginId")), true);
		                    if (UtilValidate.isEmpty(makerChecker)) {
		                    	makerChecker = delegator.makeValue("SystemProperty");
		                    	makerChecker.put("systemResourceId", "security");
		                    	makerChecker.put("systemPropertyId", rmUserLogin.getString("userLoginId"));
		                    	makerChecker.put("systemPropertyValue", "SG-I3BE-MAKERCHECKER");
		                    	makerChecker.put("description", "MAKERCHECKER");
		                    	toBeStored.add(makerChecker);
		                    }
		                    
		                    // create userLogin for RM party [end]
							
						}
						
						// RM party import [end]
						
						importContext.put("importStatusId", data.get("importStatusId"));
						importContext.put("importError", data.get("importError"));
						importContext.put("processedTimestamp", data.get("processedTimestamp"));
						
						Map<String, Object> processorContext = new HashMap<String, Object>();
						
						processorContext.put("dispatcher", dispatcher);
						processorContext.put("delegator", delegator);
						processorContext.put("userLogin", userLogin);
						processorContext.put("data", data);
						processorContext.put("primaryKey", "lcin");
						
						importContext.put("headerConfigList", segmentationHeaderConfigList);
						processorContext.put("importContext", importContext);
						Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
						Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
						
						importContext.put("headerConfigList", attributeHeaderConfigList);
						processorContext.put("importContext", importContext);
						Processor processorAttribute = ProcessorFactory.getProcessor(ProcessorType.ATTRIBUTE);
						Map<String, Object> processorAttributeResult = processorAttribute.process(processorContext);
						
						importContext.put("headerConfigList", economicMetricHeaderConfigList);
						processorContext.put("importContext", importContext);
						processorContext.put("markDataAsImported", "true");
						Processor processorEconomicMetirc = ProcessorFactory.getProcessor(ProcessorType.ECONOMIC_METRIC);
						Map<String, Object> processorEconomicMetircResult = processorEconomicMetirc.process(processorContext);
						
						
						data.set("importStatusId", "DATAIMP_IMPORTED");
						data.set("processedTimestamp", UtilDateTime.nowTimestamp());
						data.set("importError", null);
						
						toBeStored.add(data);
						
						delegator.storeAll(toBeStored);
						
					}
					
					Debug.log("end HdpCustomerSme process time: "+UtilDateTime.nowTimestamp(), MODULE);
					
				}
				
				// import other hdr crm type data [end]
				
			}
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account.."));
    	
    	return result;
    	
    }*/
	
    /*public static Map importAccountAssocs(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	String gcins = (String) context.get("gcins");
    	
    	String entityName = (String) context.get("entityName");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {

    		if (UtilValidate.isNotEmpty(gcins)) {
    			
    			EntityCondition mainCond = null;
    			List<String> gcinList = Arrays.asList(gcins.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("gcin", EntityOperator.IN, gcinList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("lcin");
				List<GenericValue> importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null, null, false);
				List<String> lcinList = EntityUtil.getFieldListFromEntityList(importDataList, "lcin", true);
				accountIds = StringUtil.join(lcinList, ",");
    		}
    		
    		if (UtilValidate.isNotEmpty(accountIds)) {
    			Map<String, Object> importContext = new HashMap<String, Object>();
        		importContext.put("accountIds", accountIds);
        		importContext.put("userLogin", userLogin);
        		
        		if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpCustomerSme")) {
        			dispatcher.runSync("dataimporter.importAccountAdditionals", importContext);
        		}
    			
    			Map<String, Object> importResult = dispatcher.runSync("dataimporter.importSegmentations", importContext);
    			
    			importResult = dispatcher.runSync("dataimporter.importAttributes", importContext);
    			
    			importContext.put("markDataAsImported", "true");
    			importResult = dispatcher.runSync("dataimporter.importEconomicMetrics", importContext);
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account assocs.."));
    	
    	return result;
    	
    }*/
	
	public static Map importAccountAssocs(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	String gcins = (String) context.get("gcins");
    	
    	String entityName = (String) context.get("entityName");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {

    		if (UtilValidate.isNotEmpty(gcins)) {
    			
    			EntityCondition mainCond = null;
    			List<String> gcinList = Arrays.asList(gcins.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("gcin", EntityOperator.IN, gcinList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet("lcin");
				List<GenericValue> importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null, null, false);
				List<String> lcinList = EntityUtil.getFieldListFromEntityList(importDataList, "lcin", true);
				accountIds = StringUtil.join(lcinList, ",");
    		}
    		
    		if (UtilValidate.isNotEmpty(accountIds)) {
    			
        		Map<String, Object> importContext = new HashMap<String, Object>();
        		importContext.put("organizationPartyId", "Company");
        		importContext.put("userLogin", userLogin);
        		
        		List<GenericValue> importDataList = null;
    			EntityCondition mainCond = null;
    			
				List<String> accountIdList = Arrays.asList(accountIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("lcin", EntityOperator.IN, accountIdList)
						);
				Set<String> fieldsToSelect = UtilMisc.toSet(Arrays.asList("lcin", "dbsIndustryCode", "constitutionCode", "importStatusId", "rm1bankid", "rm", "cntryCde", "importError", "processedTimestamp"));
				importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null,
						null, false);
    			
    			if (UtilValidate.isNotEmpty(importDataList)) {
    				
    				GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
    				SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);
    				
    				Map<String, Object> headerConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "COMBINED");
    				
    				importContext.put("headerConfigList", headerConfigList);
    				
    				Map<String, Object> processorContext = new HashMap<String, Object>();
					
					processorContext.put("dispatcher", dispatcher);
					processorContext.put("delegator", delegator);
					processorContext.put("userLogin", userLogin);
    				
    				for (GenericValue data : importDataList) {
    					
    					String accountId = data.getString("lcin");
    					
    					processorContext.put("data", data);
    					
    					/*if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpCustomerSme")) {
    						importAccountAdditional(processorContext);
    	        		}*/
    					
    					importContext.put("importStatusId", data.get("importStatusId"));
    					importContext.put("importError", data.get("importError"));
    					importContext.put("processedTimestamp", data.get("processedTimestamp"));
    					
    					processorContext.put("importContext", importContext);
    					processorContext.put("markDataAsImported", true);
    					processorContext.put("primaryKey", "lcin");
    					processorContext.put("sqlProcessor", sqlProcessor);
    					
    					Processor processorCombined = ProcessorFactory.getProcessor(ProcessorType.COMBINED);
    					Map<String, Object> processorCombinedResult = processorCombined.process(processorContext);
    					
    				}
    				
    				sqlProcessor.close();
    				
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account assocs.."));
    	
    	return result;
    	
    }
    
    public static Map importAccountAdditionals(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String accountIds = (String) context.get("accountIds");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		Map<String, Object> importContext = new HashMap<String, Object>();
    		importContext.put("organizationPartyId", "Company");
    		//importContext.put("isTrackImportStatus", false);
    		importContext.put("userLogin", userLogin);
			
			Map<String, Object> importResult = new HashMap<String, Object>();
			
			EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_NOT_PROC"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
							"DATAIMP_FAILED"),
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));

			EntityCondition mainCond = null;
			mainCond = EntityCondition.makeCondition(EntityOperator.AND,
					//EntityCondition.makeCondition("batchId", EntityOperator.EQUALS, batchId),
					statusCond);
			
			if (UtilValidate.isNotEmpty(accountIds)) {
				List<String> accountIdList = Arrays.asList(accountIds.split(","));
				mainCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("lcin", EntityOperator.IN, accountIdList),
						mainCond);
			}
			
			Set<String> fieldsToSelect = UtilMisc.toSet(Arrays.asList("lcin", "dbsIndustryCode", "constitutionCode", "importStatusId", "rm1bankid", "rm", "cntryCde", "importError", "processedTimestamp"));
			List<GenericValue> importDataList = delegator.findList("HdpCustomerSme", mainCond, fieldsToSelect, null,
					null, false);
			
			if (UtilValidate.isNotEmpty(importDataList)) {
				
				Debug.log("start HdpCustomerSme additional process time: "+UtilDateTime.nowTimestamp(), MODULE);
				
				/*
				Map<String, Object> segmentationHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "SEGMENTATION");
				Map<String, Object> attributeHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "CUSTOM_FIELD");
				Map<String, Object> economicMetricHeaderConfigList = org.groupfio.data.importer.util.DataUtil.loadHeaderConfigList(delegator, "ECONOMIC_METRIC");
				*/
				for (GenericValue data : importDataList) {
					
					String accountId = data.getString("lcin");
					
					Map<String, Object> processorContext = new HashMap<String, Object>();
					
					processorContext.put("dispatcher", dispatcher);
					processorContext.put("delegator", delegator);
					processorContext.put("userLogin", userLogin);
					processorContext.put("data", data);
					
					importAccountAdditional(processorContext);
					
				}
				
				Debug.log("end HdpCustomerSme additional process time: "+UtilDateTime.nowTimestamp(), MODULE);
				
			}
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully imported account additionals.."));
    	
    	return result;
    	
    }
    
    public static void importAccountAdditional(Map<String, Object> context) {
    	
    	try {
			LocalDispatcher dispatcher = (LocalDispatcher) context.get("dispatcher"); 
			Delegator delegator = (Delegator) context.get("delegator");  
			GenericValue userLogin = (GenericValue) context.get("userLogin"); 
			GenericValue data = (GenericValue) context.get("data");  
			
			if (UtilValidate.isNotEmpty(data)) {
				
				String accountId = data.getString("lcin");
				
				Timestamp importTimestamp = UtilDateTime.nowTimestamp();
				/*GenericValue dataImportAccount = EntityUtil.getFirst( delegator.findByAnd("DataImportAccount",UtilMisc.toMap("accountId", accountId), null, true) );
				if (UtilValidate.isNotEmpty(dataImportAccount) && UtilValidate.isNotEmpty(dataImportAccount.get("processedTimestamp"))) {
					importTimestamp = dataImportAccount.getTimestamp("processedTimestamp");
				}*/
				
				GenericValue accountParty = delegator.findOne("Party", UtilMisc.toMap("partyId", accountId), true);
				if (UtilValidate.isEmpty(accountParty)) {
					return;
				}
				
				List<GenericValue> toBeStored = FastList.newInstance();
				
				// store industry code [start]
				
				GenericValue partySuppData = delegator.findOne("PartySupplementalData", UtilMisc.toMap("partyId", accountId), false);
				if (UtilValidate.isNotEmpty(partySuppData)) {
					if (UtilValidate.isNotEmpty(data.getString("dbsIndustryCode"))) {
						GenericValue enumCode = EntityUtil.getFirst( delegator.findByAnd("Enumeration", UtilMisc.toMap("enumCode", data.getString("dbsIndustryCode")), null, true) );
						if (UtilValidate.isNotEmpty(enumCode)) {
							partySuppData.put("industryEnumId", enumCode.getString("enumId"));
						}
					}
					
					if (UtilValidate.isNotEmpty(data.getString("constitutionCode"))) {
						GenericValue enumCode = EntityUtil.getFirst( delegator.findByAnd("Enumeration", UtilMisc.toMap("enumCode", data.getString("constitutionCode")), null, true) );
						if (UtilValidate.isNotEmpty(enumCode)) {
							partySuppData.put("ownershipEnumId", enumCode.getString("enumId"));
						}
					}
					
					toBeStored.add(partySuppData);
				}
				
				// store industry code [end]
				
				// RM party import [start]
				
				if (UtilValidate.isNotEmpty(data.getString("rm1bankid"))) {
					
					//String accountRmEmailId = UtilImport.getAccountRmEmailId(data);
					//String accountRmEmailId = data.getString("rmEmail");
					
					GenericValue rmUserLogin = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", data.getString("rm1bankid")), true);
					
					//GenericValue rmParty = DataHelper.findPartyByEmail(delegator, accountRmEmailId);
					String rmPartyId = null;
					
					if (UtilValidate.isNotEmpty(rmUserLogin)) {
						rmPartyId = rmUserLogin.getString("partyId");
						
						Map<String, Object> associationContext = new HashMap<String, Object>();
						associationContext.put("partyId", accountId);
						associationContext.put("roleTypeIdFrom", "ACCOUNT");
						associationContext.put("accountPartyId", rmPartyId);
						associationContext.put("userLogin", userLogin);
						
						Map<String, Object> associationResult = dispatcher.runSync("crmsfa.updatePersonResponsibleFor", associationContext);
						
						if (!ServiceUtil.isError(associationResult)) {
							Debug.logInfo("Successfully Changed Account Responsible For, accountPartyId="+accountId+", contactPartyId="+rmPartyId, MODULE);
						}
					} else {
						EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(
	                    		EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, accountId),
	                    		//EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, rmPartyId),
	                    		EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"),
	                    		EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
	                    		EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
	                    		EntityUtil.getFilterByDateExpr()
	                		), 
	                		EntityOperator.AND);

	                    List<GenericValue> responsibleForList = delegator.findList("PartyRelationship", conditionPR, null, null, null, true);
	                    if (UtilValidate.isNotEmpty(responsibleForList)) {
	                    	for (GenericValue responsibleFor : responsibleForList) {
	                    		responsibleFor.set("thruDate", UtilDateTime.nowTimestamp());
	                    		toBeStored.add(responsibleFor);
	                    	}
	                    }
					}
					
					/*if (UtilValidate.isEmpty(rmUserLogin)) {
						
						rmPartyId = delegator.getNextSeqId("Party");
						String firstName = "";
						String lastName = "";
						
						if (UtilValidate.isNotEmpty(data.getString("rm"))) {
							String rmFullName = data.getString("rm").split("/")[0];
				        	String[] tokens = rmFullName.split(" ", 2);
				        	if (tokens.length > 1) {
				        		firstName = tokens[0];
				        		lastName = tokens[1];
				        	} else {
				        		lastName = tokens[0];
				        	}
				        }
						
						toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("CUSTOMER", "BILL_TO_CUSTOMER"), delegator));
			            GenericValue person = delegator.makeValue("Person", UtilMisc.toMap("partyId", rmPartyId, "firstName", firstName, "lastName", lastName));
			            toBeStored.add(person);
			            
			            toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("ACCOUNT_MANAGER", "EMPLOYEE"), delegator));
			            Map<String, Object> partyRelationship = UtilMisc.toMap("partyIdTo", accountId, "roleTypeIdTo", "ACCOUNT", "partyIdFrom", rmPartyId, "roleTypeIdFrom", "ACCOUNT_MANAGER", "partyRelationshipTypeId", "RESPONSIBLE_FOR", "fromDate", importTimestamp);
			            partyRelationship.put("securityGroupId", "ACCOUNT_OWNER");
			            toBeStored.add(delegator.makeValue("PartyRelationship", partyRelationship));
			            
			            GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", accountRmEmailId));
			            toBeStored.add(emailContactMech);
			            if (rmPartyId != null) {
			                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
			                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, rmPartyId, importTimestamp, delegator));
			                //toBeStored.add(UtilImport.makePartySupplementalData(partySupplementalData, personPartyId, "primaryEmailId", emailContactMech, delegator));
			            }
			            
			            GenericValue partyRelationshipcreate = delegator.makeValue("PartyRelationship");
			            partyRelationshipcreate.set("partyIdFrom", accountId);
			            partyRelationshipcreate.set("partyIdTo", rmPartyId);
			            partyRelationshipcreate.set("roleTypeIdFrom", "ACCOUNT");
			            partyRelationshipcreate.set("roleTypeIdTo", "ACCOUNT_MANAGER");
			            partyRelationshipcreate.set("securityGroupId", "ACCOUNT_OWNER");
			            partyRelationshipcreate.set("fromDate", UtilDateTime.nowTimestamp());
			            partyRelationshipcreate.set("partyRelationshipTypeId", "RESPONSIBLE_FOR");
			            
			            toBeStored.add(partyRelationshipcreate);
			            
					} else {
						rmPartyId = rmUserLogin.getString("partyId");
						
						Map<String, Object> associationContext = new HashMap<String, Object>();
						associationContext.put("partyId", accountId);
						associationContext.put("roleTypeIdFrom", "ACCOUNT");
						associationContext.put("accountPartyId", rmPartyId);
						associationContext.put("userLogin", userLogin);
						
						Map<String, Object> associationResult = dispatcher.runSync("crmsfa.updatePersonResponsibleFor", associationContext);
						
						if (!ServiceUtil.isError(associationResult)) {
							Debug.logInfo("Successfully Changed Account Responsible For, accountPartyId="+accountId+", contactPartyId="+rmPartyId, MODULE);
						}
					}*/
					
					// create userLogin for RM party [start]
			        
			        /*if (UtilValidate.isEmpty(rmUserLogin)) {
			        	
			        	rmUserLogin = delegator.makeValue("UserLogin");
			        	
			        	EntityCondition condition = EntityCondition.makeCondition(EntityOperator.OR,
			           			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoId")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
			           			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoName")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
			           			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoCode")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase()),
			           			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("abbreviation")), EntityOperator.EQUALS, data.getString("cntryCde").toUpperCase())
			           			);                       	
						
						GenericValue geoEntity = EntityUtil.getFirst( delegator.findList("Geo", condition, null, UtilMisc.toList("-createdStamp"), null, true) );
			           	if (UtilValidate.isNotEmpty(geoEntity)) {
			           		rmUserLogin.put("countryGeoId", geoEntity.getString("geoId"));
			           	}
			        	
			        	rmUserLogin.put("userLoginId", data.getString("rm1bankid"));
			        	rmUserLogin.put("partyId", rmPartyId);
			        	rmUserLogin.put("currentPassword", "{SHA}47b56994cbc2b6d10aa1be30f70165adb305a41a");
			        	rmUserLogin.put("enabled", "N");
			        	toBeStored.add(rmUserLogin);
			        	
			        	GenericValue makerChecker = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "security", "systemPropertyId", rmUserLogin.getString("userLoginId")), true);
			            if (UtilValidate.isEmpty(makerChecker)) {
			            	makerChecker = delegator.makeValue("SystemProperty");
			            	makerChecker.put("systemResourceId", "security");
			            	makerChecker.put("systemPropertyId", rmUserLogin.getString("userLoginId"));
			            	makerChecker.put("systemPropertyValue", "SG-I3BE-MAKERCHECKER");
			            	makerChecker.put("description", "MAKERCHECKER");
			            	toBeStored.add(makerChecker);
			            }
			            
			        }*/
			        
			        // create userLogin for RM party [end]
					
				} else {
					
					EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(
                    		EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, accountId),
                    		//EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, rmPartyId),
                    		EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"),
                    		EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
                    		EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
                    		EntityUtil.getFilterByDateExpr()
                		), 
                		EntityOperator.AND);

                    List<GenericValue> responsibleForList = delegator.findList("PartyRelationship", conditionPR, null, null, null, true);
                    if (UtilValidate.isNotEmpty(responsibleForList)) {
                    	for (GenericValue responsibleFor : responsibleForList) {
                    		responsibleFor.set("thruDate", UtilDateTime.nowTimestamp());
                    		toBeStored.add(responsibleFor);
                    	}
                    }
					
				}
				
				// RM party import [end]
				
				/*importContext.put("importStatusId", data.get("importStatusId"));
				importContext.put("importError", data.get("importError"));
				importContext.put("processedTimestamp", data.get("processedTimestamp"));*/
				
				/*
				Map<String, Object> processorContext = new HashMap<String, Object>();
				
				processorContext.put("dispatcher", dispatcher);
				processorContext.put("delegator", delegator);
				processorContext.put("userLogin", userLogin);
				processorContext.put("data", data);
				processorContext.put("primaryKey", "lcin");
				
				importContext.put("headerConfigList", segmentationHeaderConfigList);
				processorContext.put("importContext", importContext);
				Processor processorSegmentation = ProcessorFactory.getProcessor(ProcessorType.SEGMENTATION);
				Map<String, Object> processorSegmentationResult = processorSegmentation.process(processorContext);
				
				importContext.put("headerConfigList", attributeHeaderConfigList);
				processorContext.put("importContext", importContext);
				Processor processorAttribute = ProcessorFactory.getProcessor(ProcessorType.ATTRIBUTE);
				Map<String, Object> processorAttributeResult = processorAttribute.process(processorContext);
				
				importContext.put("headerConfigList", economicMetricHeaderConfigList);
				processorContext.put("importContext", importContext);
				processorContext.put("markDataAsImported", "true");
				Processor processorEconomicMetirc = ProcessorFactory.getProcessor(ProcessorType.ECONOMIC_METRIC);
				Map<String, Object> processorEconomicMetircResult = processorEconomicMetirc.process(processorContext);
				*/
				
				/*if (UtilValidate.isNotEmpty(dataImportAccount)) {
					data.put("importStatusId", dataImportAccount.get("importStatusId"));
					data.put("importError", dataImportAccount.get("importError"));
					data.put("processedTimestamp", dataImportAccount.get("processedTimestamp"));
					toBeStored.add(data);
				}*/
				
				/*data.set("importStatusId", "DATAIMP_IMPORTED");
				data.set("processedTimestamp", UtilDateTime.nowTimestamp());
				data.set("importError", null);
				toBeStored.add(data);*/
				
				delegator.storeAll(toBeStored);
				
			}
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
    	
    }
    
}
