/*
 * Copyright (c) Open Source Strategies, Inc.
 *
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.groupfio.data.importer.event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.groupfio.custom.field.constants.CustomFieldConstants;
import org.groupfio.data.importer.ResponseCodes;
import org.groupfio.data.importer.event.AjaxEvents;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

import javolution.util.FastList;
import javolution.util.FastMap;

/**
 * Utility class for making Ajax JSON responses.
 * @author Sharif Ul Islam
 */
public final class AjaxEvents {

    private AjaxEvents() { }

    private static final String MODULE = AjaxEvents.class.getName();

    public static String doJSONResponse(HttpServletResponse response, JSONObject jsonObject) {
        return doJSONResponse(response, jsonObject.toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Collection<?> collection) {
        return doJSONResponse(response, JSONArray.fromObject(collection).toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Map map) {
        return doJSONResponse(response, JSONObject.fromObject(map));
    }

    public static String doJSONResponse(HttpServletResponse response, String jsonString) {
        String result = "success";

        response.setContentType("application/x-json");
        try {
            response.setContentLength(jsonString.getBytes("UTF-8").length);
        } catch (UnsupportedEncodingException e) {
            Debug.logWarning("Could not get the UTF-8 json string due to UnsupportedEncodingException: " + e.getMessage(), MODULE);
            response.setContentLength(jsonString.length());
        }

        Writer out;
        try {
            out = response.getWriter();
            out.write(jsonString);
            out.flush();
        } catch (IOException e) {
            Debug.logError(e, "Failed to get response writer", MODULE);
            result = "error";
        }
        return result;
    }
    
    public static GenericValue getUserLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (GenericValue) session.getAttribute("userLogin");
    }

    /*************************************************************************/
    /**                                                                     **/
    /**                      Common JSON Requests                           **/
    /**                                                                     **/
    /*************************************************************************/
    
    /*@SuppressWarnings("unchecked")
    public static String getOperatingWallets(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        
        String masterBillingAccountId = request.getParameter("masterWalletNumbr");
        
        Map<String, Object> operatorList = new HashMap<String, Object>();
        Map<String, Object> resp = new HashMap<String, Object>();
        
        try {
        	
        	if (UtilValidate.isNotEmpty(masterBillingAccountId)) {
        		
        		operatorList = DataHelper.getActiveOperatorWalletAccounts(delegator, masterBillingAccountId);
        		
    			resp.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
    			resp.put("operatorList", operatorList);
        		
        	} else {
        		
        		resp.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
        		resp.put(EMConstants.RESPONSE_MESSAGE, "masterWalletNumbr cant be empty!!");
        	}
        	
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(EMConstants.RESPONSE_MESSAGE, e.getMessage());
        }
        
        return doJSONResponse(response, resp);
    }*/
    
    public static String getHdrMappingInformation(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        List<GenericValue> hadoopHdrMaster = null;
        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        String hdrFileType = request.getParameter("hdrFileType");
        List resp = FastList.newInstance();
        try {
        	hadoopHdrMaster = delegator.findByAnd("HadoopHdrMaster", UtilMisc.toMap("hdrFileType",hdrFileType), null, false);
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
    	return doJSONResponse(response, UtilMisc.toMap("data",hadoopHdrMaster));
    }
        
    public static String updateDataMapping(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

    	Locale locale = UtilHttp.getLocale(request);
    	HttpSession session = request.getSession(true);


    	Map<String,String> responser = UtilMisc.toMap("isSuccess","N");
    	
    	String hdrId = request.getParameter("hdrId");//primary key
		String hdrFileType = request.getParameter("hdrFileType"); //primary key
		String hdrChangeDt = request.getParameter("hdrChangeDt");
		String hdrLastChangeDt = request.getParameter("hdrLastChangeDt");
		String hdrLastId = request.getParameter("hdrLastId");
		String isActive = request.getParameter("isActive");
		String hdrName = request.getParameter("hdrName");
		String hdrUiLabel = request.getParameter("hdrUiLabel");
		String hdrRmVisible = request.getParameter("hdrRmVisible");
		String hdrRmSeqNum = request.getParameter("hdrRmSeqNum");
		String hdrCrmType = request.getParameter("hdrCrmType");
		String hdrCrmSrcId = request.getParameter("hdrCrmSrcId");
		String hdrCrmSrcField = request.getParameter("hdrCrmSrcField");
		String hdrSrcKeyVal = request.getParameter("hdrSrcKeyVal");
		String hdrInputSeq = request.getParameter("hdrInputSeq"); // primarykey
		String hdrNewField = request.getParameter("hdrNewField");
		String hdrKeyIndicator = request.getParameter("hdrKeyIndicator");
    	//getting primary key only
    	GenericValue hadoopHdrMaster = delegator.findOne("HadoopHdrMaster", false, UtilMisc.toMap("hdrId",hdrId,"hdrFileType",hdrFileType,"hdrInputSeq",hdrInputSeq));
    	if(UtilValidate.isNotEmpty(hadoopHdrMaster))
    	{
    		/*hadoopHdrMaster.setString("hdrChangeDt",hdrChangeDt);
    		hadoopHdrMaster.setString("hdrLastChangeDt",hdrLastChangeDt);*/
    		hadoopHdrMaster.setString("hdrLastId",hdrLastId);
    		hadoopHdrMaster.setString("isActive",isActive);
    		hadoopHdrMaster.setString("hdrName",hdrName);
    		hadoopHdrMaster.setString("hdrUiLabel",hdrUiLabel);
    		hadoopHdrMaster.setString("hdrRmVisible",hdrRmVisible);
    		hadoopHdrMaster.setString("hdrRmSeqNum",hdrRmSeqNum);
    		hadoopHdrMaster.setString("hdrCrmType",hdrCrmType);
    		hadoopHdrMaster.setString("hdrCrmSrcId",hdrCrmSrcId);
    		hadoopHdrMaster.setString("hdrCrmSrcField",hdrCrmSrcField);
    		hadoopHdrMaster.setString("hdrSrcKeyVal",hdrSrcKeyVal);
    		hadoopHdrMaster.setString("hdrNewField",hdrNewField);
    		hadoopHdrMaster.setString("hdrKeyIndicator",hdrKeyIndicator);
    		hadoopHdrMaster.store();
    		responser.put("isSuccess", "Y");

    	}

    	return doJSONResponse(response, UtilMisc.toMap("data",responser));
    }
    
    
    public static String bulkUpdate(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	String cntInformation = (String)request.getParameter("cntInformation");
    	for(int i=1;i<=Integer.parseInt(cntInformation);i++)
    	{
    		String hdrId = request.getParameter("hdrId_"+i);//primary key
    		String hdrFileType = request.getParameter("hdrFileType_"+i); //primary key
    		String hdrChangeDt = request.getParameter("hdrChangeDt");
    		String hdrLastChangeDt = request.getParameter("hdrLastChangeDt_"+i);
    		String hdrLastId = request.getParameter("hdrLastId_"+i);
    		String isActive = request.getParameter("isActive_"+i);
    		String hdrName = request.getParameter("hdrName_"+i);
    		String hdrUiLabel = request.getParameter("hdrUiLabel_"+i);
    		String hdrRmVisible = request.getParameter("hdrRmVisible_"+i);
    		String hdrRmSeqNum = request.getParameter("hdrRmSeqNum_"+i);
    		
    		/*String hdrCrmType = request.getParameter("hdrCrmType_"+i);
    		String hdrCrmSrcId = request.getParameter("hdrCrmSrcId_"+i);
    		String hdrCrmSrcField = request.getParameter("hdrCrmSrcField_"+i);
    		String hdrSrcKeyVal = request.getParameter("hdrSrcKeyVal_"+i);*/
    		
    		String hdrInputSeq = request.getParameter("hdrInputSeq_"+i); // primarykey
    		String hdrNewField = request.getParameter("hdrNewField_"+i);
    		
    		//String hdrKeyIndicator = request.getParameter("hdrKeyIndicator_"+i);
    		
    		GenericValue hadoopHdrMaster = delegator.findOne("HadoopHdrMaster", false, UtilMisc.toMap("hdrId",hdrId,"hdrFileType",hdrFileType,"hdrInputSeq",hdrInputSeq));
        	if(UtilValidate.isNotEmpty(hadoopHdrMaster))
        	{
        		/*hadoopHdrMaster.setString("hdrChangeDt",hdrChangeDt);
        		hadoopHdrMaster.setString("hdrLastChangeDt",hdrLastChangeDt);*/
        		hadoopHdrMaster.setString("hdrLastId",hdrLastId);
        		hadoopHdrMaster.setString("isActive",isActive);
        		hadoopHdrMaster.setString("hdrName",hdrName);
        		hadoopHdrMaster.setString("hdrUiLabel",hdrUiLabel);
        		hadoopHdrMaster.setString("hdrRmVisible",hdrRmVisible);
        		hadoopHdrMaster.setString("hdrRmSeqNum",hdrRmSeqNum);
        		
        		/*hadoopHdrMaster.setString("hdrCrmType",hdrCrmType);
        		hadoopHdrMaster.setString("hdrCrmSrcId",hdrCrmSrcId);
        		hadoopHdrMaster.setString("hdrCrmSrcField",hdrCrmSrcField);
        		hadoopHdrMaster.setString("hdrSrcKeyVal",hdrSrcKeyVal);*/
        		
        		hadoopHdrMaster.setString("hdrNewField",hdrNewField);
        		//hadoopHdrMaster.setString("hdrKeyIndicator",hdrKeyIndicator);
        		hadoopHdrMaster.store();

        	}
    	}
    	
    	return doJSONResponse(response, UtilMisc.toMap("data",null));
    }
    
    @SuppressWarnings("unchecked")
	public static String updateHeaderConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String isCompleteReset = request.getParameter("isCompleteReset");

		String hdrId = request.getParameter("hdrId");
		String hdrFileType = request.getParameter("hdrFileType");
		String hdrInputSeq = request.getParameter("hdrInputSeq");
		
		String hdrCrmType = request.getParameter("hdrCrmType");
		String hdrCrmSrcId = request.getParameter("hdrCrmSrcId");
		String hdrCrmSrcField = request.getParameter("hdrCrmSrcField");
		String hdrSrcKeyVal = request.getParameter("hdrSrcKeyVal");
		
		Map<String, Object> resp = new HashMap<String, Object>();

		try {
			
			if (UtilValidate.isNotEmpty(hdrId) && UtilValidate.isNotEmpty(hdrId) && UtilValidate.isNotEmpty(hdrId)) {
				
				GenericValue headerConfig = EntityUtil.getFirst( delegator.findByAnd("HadoopHdrMaster", UtilMisc.toMap("hdrId", hdrId, "hdrFileType", hdrFileType, "hdrInputSeq", hdrInputSeq), null, false) );
				
				if (UtilValidate.isNotEmpty(headerConfig)) {
					
					headerConfig.put("hdrCrmType", hdrCrmType);
					headerConfig.put("hdrCrmSrcId", hdrCrmSrcId);
					headerConfig.put("hdrCrmSrcField", hdrCrmSrcField);
					headerConfig.put("hdrSrcKeyVal", hdrSrcKeyVal);
					
					headerConfig.store();
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully update header configuration..");
					
				}
			} else {
				resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
	            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Required parameters missing!!");
			} 
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String executeMicroService(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String microServiceId = request.getParameter("microServiceId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {
			
			if (UtilValidate.isNotEmpty(microServiceId)) {
				
				GenericValue microService = EntityUtil.getFirst( delegator.findByAnd("MicroServiceHeader", UtilMisc.toMap("microServiceId", microServiceId), null, false) );
				
				if (UtilValidate.isNotEmpty(microService)) {
					
					/*Runtime r = Runtime.getRuntime();
					Process p = null;
					p = r.exec(new String[] { "cmd", "/c", "start C:/temp/Test.bat" });*/
					
					/*ProcessBuilder builder = new ProcessBuilder("java -jar "+microService.getString("executableJarLocation"));		    
					Process process = builder.start();*/
					
					List<String> command = new ArrayList<String>();
				    
				    command.add("java");
				    command.add("-jar");
				    command.add(microService.getString("executableJarLocation"));
				    
				    ProcessBuilder builder = new ProcessBuilder(command);		    
				    Process process = builder.start();
					
					/*ProcessBuilder pb = new ProcessBuilder(microService.getString("executableJarLocation"), "-jar", "your.jar");
					pb.directory(new File("preferred/working/directory"));
					Process p = pb.start();*/
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully execute service: "+microService.getString("microServiceName"));
					
				}
			} else {
				resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
	            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Required parameters missing!!");
			} 
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
			resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
		}
		
		return doJSONResponse(response, resp);
	}
    
    public static String microServiceList(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	String draw = request.getParameter("draw");
    	String start = request.getParameter("start");
    	String length = request.getParameter("length");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	List<GenericValue> microServiceHeader = null;
    	Locale locale = UtilHttp.getLocale(request);
    	HttpSession session = request.getSession(true);
    	List < Object > microServList = FastList.newInstance();
    	Map < String, Object > returnMap = FastMap.newInstance();
    	int recordsTotal=0;
    	try {

    		microServiceHeader = delegator.findAll("MicroServiceHeader",true);
    		if(microServiceHeader!=null && microServiceHeader.size()>0){
    		 	int count = microServiceHeader.size();
    			recordsTotal=count;
    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);
    			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
    			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
    			efo.setOffset(startInx);
    			efo.setLimit(endInx);
    			List < GenericValue > microServiceHeaderList = delegator.findList("MicroServiceHeader",null, null, null, efo, false);

    			for(GenericValue microService:microServiceHeaderList){
    				HashMap<String, String> micromap=new HashMap<String, String>();
    				micromap.put("microServiceId", microService.getString("microServiceId"));
    				micromap.put("microServiceName", microService.getString("microServiceName"));
    				micromap.put("extractConditionId", microService.getString("extractConditionId"));
    				micromap.put("extractProg", microService.getString("extractProg"));
    				micromap.put("conversionConditionId", microService.getString("conversionConditionId"));
    				micromap.put("conversionProg", microService.getString("conversionProg"));
    				microServList.add(micromap);
    			}
    			returnMap.put("data", microServList);
    			returnMap.put("draw", draw);
    			returnMap.put("recordsTotal", recordsTotal);
    			returnMap.put("recordsFiltered", recordsTotal);
    		}else{
    			returnMap.put("data", microServList);
    			returnMap.put("draw", draw);
    			returnMap.put("recordsTotal", 0);
    			returnMap.put("recordsFiltered", 0);
    			return AjaxEvents.doJSONResponse(response, returnMap);
    		}
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return AjaxEvents.doJSONResponse(response, returnMap);
    }
    /*public static String executeHadoopDataImport(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        Map < String, Object > resp = new HashMap < String, Object > ();
        String executeType = request.getParameter("executeType");
        try {
            if (UtilValidate.isNotEmpty(executeType)) {
                String executableJarLocation = UtilProperties.getPropertyValue("data-importer", "hadoopDataImporterJarLocation");
                String executableJarName = "";
                if ("ALL".equalsIgnoreCase(executeType)) {
                    executableJarName = UtilProperties.getPropertyValue("data-importer", "batchProcessing");
                } else if ("CUSTOM_FIELD".equalsIgnoreCase(executeType)) {
                    executableJarName = UtilProperties.getPropertyValue("data-importer", "batchProcessingAttribute");
                } else if ("SEGMENTATION".equalsIgnoreCase(executeType)) {
                    executableJarName = UtilProperties.getPropertyValue("data-importer", "batchProcessingSegmentation");
                } else if ("ECONOMIC_METRIC".equalsIgnoreCase(executeType)) {
                    executableJarName = UtilProperties.getPropertyValue("data-importer", "batchProcessingEconomicMetric");
                }
                if (UtilValidate.isNotEmpty(executableJarLocation) && UtilValidate.isNotEmpty(executableJarName)) {
                    List < String > command = new ArrayList < String > ();
                    command.add("java");
                    command.add("-jar");
                    command.add(executableJarLocation + executableJarName);

                    ProcessBuilder builder = new ProcessBuilder(command);
                    Process process = builder.start();

                    resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
                    resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully execute service");
                } else {
                    resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
                    resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Required parameters missing!!");
                }
            } else {
                resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
                resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Required parameters missing!!");
            }

        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: " + e.getMessage());
        }
        return doJSONResponse(response, resp);
    }*/
    
    public static String executeHadoopDataImport(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        Map < String, Object > resp = new HashMap < String, Object > ();
        String executeType = request.getParameter("executeType");
        try {
            if (UtilValidate.isNotEmpty(executeType)) {
                String shellScriptPath = null;
                if ("ALL".equalsIgnoreCase(executeType)) {
                } else if ("CUSTOM_FIELD".equalsIgnoreCase(executeType)) {
                	GenericValue systemProperty = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "data-importer","systemPropertyId","hadoop.addl.attribute"), false);
        			if(systemProperty != null && systemProperty.size() > 0) {
        				shellScriptPath = systemProperty.getString("systemPropertyValue");
        			}
                } else if ("SEGMENTATION".equalsIgnoreCase(executeType)) {
                	GenericValue systemProperty = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "data-importer","systemPropertyId","hadoop.addl.segmentation"), false);
        			if(systemProperty != null && systemProperty.size() > 0) {
        				shellScriptPath = systemProperty.getString("systemPropertyValue");
        			}
                } else if ("ECONOMIC_METRIC".equalsIgnoreCase(executeType)) {
                	GenericValue systemProperty = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "data-importer","systemPropertyId","hadoop.addl.economic.metric"), false);
        			if(systemProperty != null && systemProperty.size() > 0) {
        				shellScriptPath = systemProperty.getString("systemPropertyValue");
        			}
                }
                if (UtilValidate.isNotEmpty(shellScriptPath) && shellScriptPath.endsWith(".sh")) {
                	ProcessBuilder builder = new ProcessBuilder(shellScriptPath);
            		Process p = null;
                	try {
                		p = builder.start();
                		BufferedReader output = new BufferedReader(new InputStreamReader(p.getInputStream()));
                		StringBuilder strBuilder = new StringBuilder();
                		String line = null;
                		while ( (line = output.readLine()) != null) {
                			strBuilder.append(line);
                		}
                		String result = strBuilder.toString();
                		Debug.logInfo(" Shell script "+shellScriptPath+" output : " + result,MODULE);
                		output.close();

                		BufferedReader output1 = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                		StringBuilder errorBuilder = new StringBuilder();
                		String errorLine = null;
                		while ( (errorLine = output1.readLine()) != null) {
                			errorBuilder.append(errorLine);
                		}
                		String errorResult = errorBuilder.toString();
                		Debug.logError("Shell script "+shellScriptPath+"  error output : " + errorResult,MODULE);
                		output1.close();
                		//p.waitFor();
                	} catch (Exception  e) {
                		Debug.logInfo("Error : "+e.getMessage(),MODULE);
                	}finally{
                		p.destroy();
                	}

                	resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
                	resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully execute service");
                } else {
                    resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
                    resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Script not found!!");
                }
            } else {
                resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
                resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Required parameters missing!!");
            }

        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: " + e.getMessage());
        }
        return doJSONResponse(response, resp);
    }
}
