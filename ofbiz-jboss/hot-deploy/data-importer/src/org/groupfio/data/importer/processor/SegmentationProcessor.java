/**
 * 
 */
package org.groupfio.data.importer.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.groupfio.custom.field.constants.CustomFieldConstants.SegmentationValueCapture;
import org.groupfio.custom.field.util.DataUtil;
import org.groupfio.data.importer.ResponseCodes;
import org.groupfio.data.importer.constants.DataImporterConstants;
import org.groupfio.data.importer.util.DataHelper;
import org.groupfio.data.importer.util.ParamUtil;
import org.groupfio.data.importer.util.QueryUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilObject;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.model.ModelField;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

/**
 * @author Sharif
 *
 */
public class SegmentationProcessor extends Processor {
	
	private static String MODULE = SegmentationProcessor.class.getName();
	
	private static SegmentationProcessor instance;
	
	public static synchronized SegmentationProcessor getInstance(){
        if(instance == null) {
            instance = new SegmentationProcessor();
        }
        return instance;
    }

	@Override
	protected Map<String, Object> doProcess(Map<String, Object> context) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			
			LocalDispatcher dispatcher = (LocalDispatcher) context.get("dispatcher"); 
			Delegator delegator = (Delegator) context.get("delegator");  
			GenericValue userLogin = (GenericValue) context.get("userLogin"); 
			GenericValue data = (GenericValue) context.get("data");  
			Map<String, Object> importContext = (Map<String, Object>) context.get("importContext"); 
			SQLProcessor sqlProcessor = (SQLProcessor) context.get("sqlProcessor");  
			
			boolean markDataAsImported = ParamUtil.getBoolean(context, "markDataAsImported");
			boolean isIgnoreStatusCondition = ParamUtil.getBoolean(context, "isIgnoreStatusCondition");
			String primaryKey = ParamUtil.getString(context, "primaryKey");
			
			try {
	    		
				String accountId = data.getString(primaryKey);
				String updateQuery = null;
				String primaryKeySqlCondition = null;
				Debug.logInfo("Start process segmentation for accountId: "+ accountId+", time: "+UtilDateTime.nowTimestamp(), MODULE);
				if (UtilValidate.isNotEmpty( importContext.get("headerConfigList") )) {
					
					Map<String, List<GenericValue>> headerConfigList = (Map<String, List<GenericValue>>) importContext.get("headerConfigList");
					
					for (Map.Entry<String, List<GenericValue>> entry : headerConfigList.entrySet()) {
						
						String key = entry.getKey();
						List<GenericValue> headerConfigs = entry.getValue();
						
						String entityName = DataImporterConstants.ENTITY_NAME_BY_FILE_TYPE.get(key);
						
						if (UtilValidate.isEmpty(entityName)) {
							continue;
						}
						
						EntityCondition mainCond = null;
						
						if (!isIgnoreStatusCondition) {
							EntityCondition statusCond = EntityCondition.makeCondition(EntityOperator.OR,
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
											"DATAIMP_NOT_PROC"),
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS,
											"DATAIMP_FAILED"),
									EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, null));
							
							mainCond = EntityCondition.makeCondition(EntityOperator.AND,
									org.groupfio.data.importer.util.DataUtil.createRecordIdentifier(entityName, data),
									statusCond
									);
						} else {
							mainCond = EntityCondition.makeCondition(EntityOperator.AND,
									org.groupfio.data.importer.util.DataUtil.createRecordIdentifier(entityName, data)
									);
						}
						
						List<GenericValue> importDataList = delegator.findList(entityName, mainCond, null, null,
								null, false);
						
						for (GenericValue importData : importDataList) {
							
							if (markDataAsImported) {
								primaryKeySqlCondition = QueryUtil.buildPrimaryKeySqlCondition(importData);
								updateQuery = "update "+DataHelper.javaPropToSqlProp(importData.getEntityName())+" set import_status_id='DATAIMP_PROCESSING', processed_timestamp=NOW() where "+primaryKeySqlCondition;
								sqlProcessor.prepareStatement(updateQuery);
								sqlProcessor.executeUpdate(updateQuery);
							}
							
							for (GenericValue headerConfig : headerConfigs) {
								
								String fieldValue = null;
								try {
									fieldValue = importData.getString(DataHelper.sqlPropToJavaProp(headerConfig.getString("hdrName")));
									if (UtilValidate.isEmpty(fieldValue)) {
										continue;
									}
								} catch (Exception e) {
									Debug.logError("error durigng get fieldValue: "+e.getMessage(), MODULE);
									continue;
								}
								
								
								if (UtilValidate.isNotEmpty(headerConfig.getString("hdrCrmType")) && headerConfig.getString("hdrCrmType").equals("SEGMENTATION")) {
									String hdrCrmSrcField = headerConfig.getString("hdrCrmSrcField");
									String hdrSrcKeyVal = headerConfig.getString("hdrSrcKeyVal");
										
									if (UtilValidate.isNotEmpty(hdrCrmSrcField)) {
										
										GenericValue segmentCode = delegator.findOne("CustomFieldGroup", UtilMisc.toMap("groupId", hdrCrmSrcField), false);
										if (UtilValidate.isNotEmpty(segmentCode)) {
											String groupId = segmentCode.getString("groupId");
											//String segmentationValueAssociatedEntityName = DataUtil.getSegmentationValueAssociatedEntityName(delegator, groupId);
											String segmentationValueAssociatedEntityName = "CustomFieldPartyClassification";
											
											Debug.logInfo("segmentCode valueCapture: "+ segmentCode.getString("valueCapture"), MODULE);
											
											// actual segment value resolve [start]
											GenericValue segmentValue = null;
											if (UtilValidate.isNotEmpty(segmentCode.getString("valueCapture")) && segmentCode.getString("valueCapture").equals(SegmentationValueCapture.SINGLE)) {
												GenericValue segmentValueConfig = EntityUtil.getFirst( delegator.findByAnd("CustomFieldValueConfig", UtilMisc.toMap("groupId", segmentCode.getString("groupId"), "valueData", fieldValue), null, false) );
												if (UtilValidate.isNotEmpty(segmentValueConfig)) {
													segmentValue = delegator.findOne("CustomField", UtilMisc.toMap("customFieldId", segmentValueConfig.getString("customFieldId")), false);
													
													String removeQuery = "SELECT SQL_CACHE GROUP_ID, CUSTOM_FIELD_ID, PARTY_ID from custom_field_party_classification WHERE GROUP_ID = '"+groupId+"' AND PARTY_ID = '"+accountId+"'";
													
													List<Map<String, Object>> associatedEntityList = QueryUtil.runSqlQuery(delegator, removeQuery);
													if (UtilValidate.isNotEmpty(associatedEntityList)) {
														for (Map<String, Object> associatedEntity : associatedEntityList) {
															updateQuery = "update custom_field_party_classification set is_removed='Y' where GROUP_ID = '"+associatedEntity.get("GROUP_ID")+"' AND PARTY_ID = '"+associatedEntity.get("PARTY_ID")+"'"+" AND CUSTOM_FIELD_ID = '"+associatedEntity.get("CUSTOM_FIELD_ID")+"'";
															sqlProcessor.prepareStatement(updateQuery);
															sqlProcessor.executeUpdate(updateQuery);
														}
														
													}
														
												}
											} else if (UtilValidate.isNotEmpty(segmentCode.getString("valueCapture")) && segmentCode.getString("valueCapture").equals(SegmentationValueCapture.MULTIPLE)) {
												GenericValue segmentValueConfig = EntityUtil.getFirst( delegator.findByAnd("CustomFieldValueConfig", UtilMisc.toMap("groupId", segmentCode.getString("groupId"), "valueData", fieldValue), null, false) );
												if (UtilValidate.isNotEmpty(segmentValueConfig)) {
													segmentValue = delegator.findOne("CustomField", UtilMisc.toMap("customFieldId", segmentValueConfig.getString("customFieldId")), false);
												}
											} else if (UtilValidate.isNotEmpty(segmentCode.getString("valueCapture")) && segmentCode.getString("valueCapture").equals(SegmentationValueCapture.RANGE)) {
												List<GenericValue> segmentValueConfigs = delegator.findByAnd("CustomFieldValueConfig", UtilMisc.toMap("groupId", segmentCode.getString("groupId")), null, false);
												if (UtilValidate.isNotEmpty(segmentValueConfigs)) {
													for (GenericValue config : segmentValueConfigs) {
														if (UtilValidate.isNotEmpty(config.getString("valueMin")) && UtilValidate.isNotEmpty(config.getString("valueMax"))) {
															double valueMin = Double.parseDouble(config.getString("valueMin"));
															double valueMax = Double.parseDouble(config.getString("valueMax"));
															
															double actualValue = Double.parseDouble(fieldValue);
															
															if (valueMin >= actualValue && actualValue < valueMax) {
																segmentValue = delegator.findOne("CustomField", UtilMisc.toMap("customFieldId", config.getString("customFieldId")), false);
																break;
															}
														}
													}
												}
												
												if (UtilValidate.isNotEmpty(segmentValue)) {
													
													String removeQuery = "SELECT SQL_CACHE GROUP_ID, CUSTOM_FIELD_ID, PARTY_ID from custom_field_party_classification WHERE GROUP_ID = '"+groupId+"' AND PARTY_ID = '"+accountId+"'";
													
													List<Map<String, Object>> associatedEntityList = QueryUtil.runSqlQuery(delegator, removeQuery);
													if (UtilValidate.isNotEmpty(associatedEntityList)) {
														for (Map<String, Object> associatedEntity : associatedEntityList) {
															updateQuery = "update custom_field_party_classification set is_removed='Y' where GROUP_ID = '"+associatedEntity.get("GROUP_ID")+"' AND PARTY_ID = '"+associatedEntity.get("PARTY_ID")+"'"+" AND CUSTOM_FIELD_ID = '"+associatedEntity.get("CUSTOM_FIELD_ID")+"'";
															sqlProcessor.prepareStatement(updateQuery);
															sqlProcessor.executeUpdate(updateQuery);
														}
														
													}
													
												}
												
											}
											// actual segment value resolve [end]
											
											if (UtilValidate.isNotEmpty(segmentValue)) {
												String customFieldId = segmentValue.getString("customFieldId");
												
												String partyId = accountId;
												GenericValue associatedEntity = delegator.findOne(segmentationValueAssociatedEntityName, UtilMisc.toMap("groupId", groupId, "customFieldId", customFieldId, "partyId", partyId), false);
												
												if (UtilValidate.isEmpty(associatedEntity)) {
													
													associatedEntity = delegator.makeValue(segmentationValueAssociatedEntityName);
													
													associatedEntity.put("groupId", groupId);
													associatedEntity.put("customFieldId", customFieldId);
													associatedEntity.put("partyId", partyId);
													associatedEntity.put("inceptionDate", UtilDateTime.nowTimestamp());
													
													associatedEntity.create();
													
												}
												
												associatedEntity.put("groupActualValue", fieldValue);
												associatedEntity.put("isRemoved", null);
												
												associatedEntity.store();
											}
											
										}
										
									}
								
								}
								
							}
							
							if (markDataAsImported) {
								
								/*importData.put("importStatusId", "DATAIMP_IMPORTED");
								//importData.put("importError", null);
								importData.put("processedTimestamp", UtilDateTime.nowTimestamp());*/
								
								updateQuery = "update "+DataHelper.javaPropToSqlProp(importData.getEntityName())+" set import_status_id='DATAIMP_IMPORTED', processed_timestamp=NOW() where "+primaryKeySqlCondition;
								sqlProcessor.prepareStatement(updateQuery);
								sqlProcessor.executeUpdate(updateQuery);
								
							}
							
						}
						
						//delegator.storeAll(importDataList);
						
					}
					
				}
				
				Debug.logInfo("End process segmentation for accountId: "+ accountId+", time: "+UtilDateTime.nowTimestamp(), MODULE);
				
			} catch (Exception e) {
				e.printStackTrace();
				Debug.logError("Error process segmentation: "+e.getMessage(), MODULE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(DataImporterConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(DataImporterConstants.RESPONSE_MESSAGE, e.getMessage());
			
			return response;
			
		}
		
		response.put(DataImporterConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		
		return response;

	}

}
