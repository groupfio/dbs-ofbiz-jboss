/**
 * 
 */
package org.groupfio.data.importer.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.data.importer.constants.DataImporterConstants.SourceInvoked;
import org.groupfio.data.importer.util.DataUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class DataUtil {
	
	private static String MODULE = DataUtil.class.getName();

	public static void prepareAppStatusData(Map<String, Object> data) {
		if (UtilValidate.isEmpty(data.get("sourceInvoked"))) {
			data.put("sourceInvoked", SourceInvoked.UNKNOWN);
		}
	}
	
	public static String getStatusId (Delegator delegator, String statusCode) {
		try {
			GenericValue statusItem = EntityUtil.getFirst( delegator.findByAnd("StatusItem", UtilMisc.toMap("statusTypeId", "CUSTOM_FIELD_STATUS", "statusCode", statusCode), null, false) );
			if (UtilValidate.isNotEmpty(statusItem)) {
				return statusItem.getString("statusId");
			}
		} catch (Exception e) {
			Debug.logError(e, MODULE);
		}
		return null;
	}
	
	public static List<GenericValue> getLatestVersionHeaderConfigs (Delegator delegator, String hdrFileType) {
		return getLatestVersionHeaderConfigs(delegator, hdrFileType, null);
	}
	
	public static List<GenericValue> getLatestVersionHeaderConfigs (Delegator delegator, String hdrFileType, String hdrCrmType) {
		
		List<GenericValue> headerConfigs = new ArrayList<GenericValue>();
		
		try {
			
			List conditionsList = FastList.newInstance();
			
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			
			if (UtilValidate.isNotEmpty(hdrFileType)) {
				conditionsList.add(EntityCondition.makeCondition("hdrFileType", EntityOperator.EQUALS, hdrFileType));
			}
			
			if (UtilValidate.isNotEmpty(hdrCrmType) && hdrCrmType.equals("COMBINED")) {
				
				EntityCondition typeCondition = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("hdrCrmType", EntityOperator.EQUALS, "SEGMENTATION"),
						EntityCondition.makeCondition("hdrCrmType", EntityOperator.EQUALS, "CUSTOM_FIELD"),
						EntityCondition.makeCondition("hdrCrmType", EntityOperator.EQUALS, "ECONOMIC_METRIC")
						);
				conditionsList.add(typeCondition);
			} else if (UtilValidate.isNotEmpty(hdrCrmType)) {
				conditionsList.add(EntityCondition.makeCondition("hdrCrmType", EntityOperator.EQUALS, hdrCrmType));
			}
			
			EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
			
			List<GenericValue> hdrIdList = delegator.findList("HadoopHdrMaster", mainConditons, UtilMisc.toSet("hdrId"), UtilMisc.toList("hdrRmSeqNum"), efo, false);
			
			if (UtilValidate.isNotEmpty(hdrIdList)) {
				String hdrId = hdrIdList.get(0).getString("hdrId");
				
				headerConfigs = delegator.findByAnd("HadoopHdrMaster", UtilMisc.toMap("hdrId", hdrId, "hdrFileType", hdrFileType), null, false);
				
			}
			
		} catch (Exception e) {
			Debug.logError(e, MODULE);
		}
		
		return headerConfigs;
		
	}
	
	public static Map<String, Object> loadHeaderConfigList(Delegator delegator, String hdrCrmType) {
		Map<String, Object> headerConfigList = new LinkedHashMap<String, Object>();
		
		headerConfigList.put("facilities", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_facilities", hdrCrmType));
		headerConfigList.put("caAccount", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_ca_account", hdrCrmType));
		headerConfigList.put("customerSme", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_customer_sme", hdrCrmType));
		headerConfigList.put("fdAccount", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_fd_account", hdrCrmType));
		headerConfigList.put("gcinPpinLin", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_gcin_gpin_lin", hdrCrmType));
		headerConfigList.put("loanAccount", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_loan_account", hdrCrmType));
		headerConfigList.put("tradeAccount", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_trade_account", hdrCrmType));
		//headerConfigList.put("contact", org.groupfio.data.importer.util.DataUtil.getLatestVersionHeaderConfigs (delegator, "o_uc65_contact", hdrCrmType));
		
		return headerConfigList;
	}
	
	public static EntityCondition createRecordIdentifier(String entityName, GenericValue data) {
		if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpGcinGpinLinkage")) {
			return EntityCondition.makeCondition("gcin", EntityOperator.EQUALS, data.getString("gcin"));
		} else if (UtilValidate.isNotEmpty(entityName) && entityName.equals("DataImportLead")) {
			return EntityCondition.makeCondition("leadId", EntityOperator.EQUALS, data.getString("leadId"));
		} else if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpContact") && data.getEntityName().equals("HdpContact")) {
			EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("lcin", EntityOperator.EQUALS, data.getString("lcin")),
					EntityCondition.makeCondition("rpIdentityno", EntityOperator.EQUALS, data.getString("rpIdentityno")),
					EntityCondition.makeCondition("rpDesignation", EntityOperator.EQUALS, data.getString("rpDesignation"))
					);
			return searchConditions;
		}
		return EntityCondition.makeCondition("lcin", EntityOperator.EQUALS, data.getString("lcin"));
	}
	
	public static String getPartyRelAssocId(Delegator delegator, Map<String, Object> context) {
		
		String partyIdFrom = (String) context.get("partyIdFrom");
		String partyIdTo = (String) context.get("partyIdTo");
		String roleTypeIdFrom = (String) context.get("roleTypeIdFrom");
		String roleTypeIdTo = (String) context.get("roleTypeIdTo");
		String partyRelationshipTypeId = (String) context.get("partyRelationshipTypeId");
		
		try {
			if (UtilValidate.isNotEmpty(partyIdFrom) && UtilValidate.isNotEmpty(partyIdTo) && UtilValidate.isNotEmpty(roleTypeIdFrom) && UtilValidate.isNotEmpty(roleTypeIdTo) && UtilValidate.isNotEmpty(partyRelationshipTypeId)) {
				EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyIdFrom),
						EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyIdTo),
						EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeIdFrom),
						EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, roleTypeIdTo),
						EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, partyRelationshipTypeId),
						EntityUtil.getFilterByDateExpr());
				
				GenericValue existingRelationship = EntityUtil.getFirst( delegator.findList("PartyRelationship", searchConditions,null, null, null, true) );
				if (UtilValidate.isNotEmpty(existingRelationship)) {
					return existingRelationship.getString("partyRelAssocId");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static boolean isContactEmailChange(Delegator delegator, String contactId, String partyRelAssocId, String rpIdealemailaddress, String rpAosEmailaddress) {
		
		String emailAddress = UtilValidate.isNotEmpty(rpIdealemailaddress) ? rpIdealemailaddress : rpAosEmailaddress;
		
		try {
			if (UtilValidate.isNotEmpty(emailAddress)) {
				GenericValue emailPurpose = EntityUtil.getFirst(delegator.findByAnd("PartyContactMechPurpose", UtilMisc.toMap("partyId", contactId, "contactMechPurposeTypeId", "IDEAL_EMAIL_ADDRESS", "partyRelAssocId", partyRelAssocId), null, false));
			 	if(UtilValidate.isNotEmpty(emailPurpose)){
			 		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", emailPurpose.getString("contactMechId")));
			     	
			 		if (UtilValidate.isNotEmpty(emailContactMech.getString("infoString")) && !emailAddress.equals(emailContactMech.getString("infoString"))) {
			 			return true;
			 		}
			 		
			 	}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean isContactPhoneChange(Delegator delegator, String contactId, String partyRelAssocId, String rpIdealMobileno, String rpAosMobileno) {
		
		String phoneNumber = UtilValidate.isNotEmpty(rpIdealMobileno) ? rpIdealMobileno : rpAosMobileno;
		
		try {
			if (UtilValidate.isNotEmpty(phoneNumber)) {
				GenericValue mobilePurpose = EntityUtil.getFirst(delegator.findByAnd("PartyContactMechPurpose", UtilMisc.toMap("partyId", contactId, "contactMechPurposeTypeId", "IDEAL_MOBILE_PHONE", "partyRelAssocId", partyRelAssocId), null, false));
	         	if(UtilValidate.isNotEmpty(mobilePurpose)){
	         		String contactMechId = mobilePurpose.getString("contactMechId");
	        		
	        		GenericValue phoneContactMech = EntityUtil.getFirst(delegator.findByAnd("TelecomNumber",  UtilMisc.toMap("contactMechId", contactMechId), null, false));
	        		if (UtilValidate.isNotEmpty(phoneContactMech.getString("contactNumber")) && !phoneNumber.equals(phoneContactMech.getString("contactNumber"))) {
	        			return true;
	        		}
	         	}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static GenericValue getActivePartyContactMechPurpose(Delegator delegator, String partyId, String contactMechPurposeTypeId, String partyRelAssocId) {
		GenericValue partyContactMechPurpose = null;
		try {
			if (UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(contactMechPurposeTypeId)) {
				EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
						EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, contactMechPurposeTypeId),
						EntityUtil.getFilterByDateExpr());
				
				if (UtilValidate.isNotEmpty(partyRelAssocId)) {
					searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("partyRelAssocId", EntityOperator.EQUALS, partyRelAssocId),
							searchConditions);
				}
				
				partyContactMechPurpose = EntityUtil.getFirst( delegator.findList("PartyContactMechPurpose", searchConditions,null, null, null, false) );
				return partyContactMechPurpose;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return partyContactMechPurpose;
	}
	
}
