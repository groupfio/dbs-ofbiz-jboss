<div id="reassignRMModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Find RM</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="card-header">
               <form method="post" action="#" id="FindTeamMembers" class="form-horizontal" name="FindTeamMembers" novalidate="novalidate" data-toggle="validator">
                  <input type="hidden" name="rmAMPartyId" id="rmAMPartyId"/>
                  <div class="row">
                     <div class="col-md-2 col-sm-2">
                        <div class="form-group row mr">
                           <input type="text" class="form-control input-sm" id="firstName" name="firstName" value="" placeholder="First Name">
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-2">
                        <div class="form-group row mr">
                           <input type="text" class="form-control input-sm" id="lastName" name="lastName" placeholder="Last Name">
                        </div>
                     </div>
                     <div class="col-md-1 col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary navbar-dark m5" onclick="javascript:getAccountManagerPRF();">Find RM</button>
                     </div>
                  </div>
               </form>
               <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">RM Members</h2>
            </div>
            <div class="table-responsive">
               <table id="ajaxFindTeamMembersdatatablePRF" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Name</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="sbmit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<form method="post" action="rmReassignForm" name="rmReassignForm" id="rmReassignForm">
   <#assign requestURI = "approveList"/>
   <#if enableCampaignApprove?exists && enableCampaignApprove?has_content && enableCampaignApprove=="Y">
   <#assign requestURI = "viewMarketingCampaign"/>
   <input type="hidden" name="activeTab" value="campList" />
   </#if>
   <input type="hidden" name="contactListId" id="contactListId" value="${contactListIdAL?if_exists}"/>
   <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
   <input type="hidden" name="approvePage" id="approvePage" value="${requestURI?if_exists}"/>
   <input type="hidden" name="campaignIdAL" id="campaignIdAL" value="${campaignIdAL?if_exists}"/>
   <input type="hidden" name="statusIdAL" id="statusIdAL" value="${statusIdAL?if_exists}"/>
   <input type="hidden" name="rmUserLoginIdAL" id="rmUserLoginIdAL" value="${rmUserLoginIdAL?if_exists}"/>
   <input type="hidden" name="rmPartyId" value="" id="rmPartyId"/>
   <input type="hidden" name="roleTypeIdFrom" value="" id="roleTypeIdFrom"/>
   <input type="hidden" name="partyId" value="" id="partyId"/>
   <input type="hidden" name="accountPartyId" value="" id="accountPartyId"/>
</form>

<script>
    function reassignCSR(partyId, roleTypeIdFrom, rmAMPartyId, accountPartyId) {
        if (rmAMPartyId != null && rmAMPartyId != "") {
            $("#rmAMPartyId").val(rmAMPartyId);
        } else {
            $("#rmAMPartyId").val("");
        }
        if (partyId != null && partyId != "" && roleTypeIdFrom != null && roleTypeIdFrom != "") {
            document.rmReassignForm.roleTypeIdFrom.value = roleTypeIdFrom;
            document.rmReassignForm.partyId.value = partyId;
            document.rmReassignForm.accountPartyId.value = accountPartyId;
            getAccountManagerPRF();
        }
    }

    function reassignParty(value) {
        if (value != null && value != "") {
            document.rmReassignForm.rmPartyId.value = value;
            $.ajax({
               url: "rmReassignFormAjax",
               type: 'POST',
               data: $('#rmReassignForm').serialize(),
               success: function(data) {
                  if (data.code == 200) {
                     showAlert ("success", data.message);
                     loadApproveList();
                     $('#reassignRMModal').modal('hide');
                  } else {
                     showAlert ("error", data.message);
                  }
               }
            });
        }
    }

    function getAccountManagerPRF() {
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var rmAMPartyId = $("#rmAMPartyId").val();
        var url = "getTeamMembers?firstName=" + firstName + "&lastName=" + lastName + "&rmPartyId=" + rmAMPartyId;
        $('#ajaxFindTeamMembersdatatablePRF').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": url,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": true,
            "columns": [{
                    "data": "partyId",
                    "render": function(data, type, row, meta) {
                        data = '<a href="#" onclick=reassignParty("' + data + '")>' + row.name + '(' + data + ')</a>';
                        return data;
                    }
                },

            ]
        });
    }
</script>