<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#assign enableCampaignApproveList = "N"/>
<#if enableCampaignApprove?exists && enableCampaignApprove?has_content && enableCampaignApprove=="Y">
   <#assign enableCampaignApproveList = "Y"/>
</#if>
<#if enableCampaignApproveList=="Y">
<div class="page-header">
   <h2 class="float-left" >${uiLabelMap.campaignList}</h2>
</div>
<#else>
<div class="page-header border-b">
   <h1 class="float-left" >${uiLabelMap.approveList}</h1>
      <#if marketingCampaignId?exists && marketingCampaignId?has_content>
        <div class="float-right">
          <form method="post" action="viewMarketingCampaign?marketingCampaignId=${marketingCampaignId?if_exists}">
             <input type="hidden" name="activeTab" value="campList" />
             <input type="submit" value="${uiLabelMap.Back}" class="btn btn-xs btn-primary"/>
          </form>
        </div>
      </#if>
</div>
</#if>
<div class="card-header">
   <#if marketingCampaignId?exists && marketingCampaignId?has_content>
      <#if enableCampaignApproveList=="Y">
         <form method="post" action="viewMarketingCampaign?marketingCampaignId=${marketingCampaignId?if_exists}" id="approveListForm" class="form-horizontal" name="approveListForm" novalidate="true" data-toggle="validator">
      <#else>
         <form method="post" action="approveList?marketingCampaignId=${marketingCampaignId?if_exists}" id="approveListForm" class="form-horizontal" name="approveListForm" novalidate="true" data-toggle="validator">
      </#if>
   <#else>
      <form method="post" action="approveList" id="approveListForm" class="form-horizontal" name="approveListForm" novalidate="true" data-toggle="validator">
   </#if>
      <input type="hidden" name="activeTab" value="campList" />
      <div class="row">
         <#assign disabled = false/>
         <#assign required = true/>
         <#assign campaignId = "${campaignIdAL?if_exists}"/>
         <#if marketingCampaignId?exists && marketingCampaignId?has_content>
            <#assign disabled = true/>
            <#assign required = false/>
            <#assign campaignId = "${marketingCampaignId}"/>
         </#if>
         
         <@dropdownInputForRow 
         id="campaignIdAL"
         options=marketingCampaignListAL
         required=required
         value="${campaignId?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.campaign
         label=uiLabelMap.campaign
         dataLiveSearch=true
         disabled=disabled
         />
         
         <@dropdownInputForRow 
         id="rmUserLoginIdAL"
         name="rmUserLoginIdAL"
         options=responsibleForAL
         value="${rmUserLoginIdAL?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.RM
         label=uiLabelMap.RM
         dataLiveSearch=true
         />
         <@dropdownInputForRow 
         id="statusIdAL"
         name="statusIdAL"
         options=optionMap
         value="${statusIdAL?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.status
         label=uiLabelMap.status
         dataLiveSearch=true
         />
         <@dropdownInputForRow 
         id="contactTypeId"
         name="contactTypeId"
         options=contactType
         value="${contactTypeId?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.listType
         label=uiLabelMap.listType
         dataLiveSearch=true
         />
         <div class="col-md-2 col-sm-2">
            <input type="submit" value="${uiLabelMap.find}" class="btn btn-sm btn-primary tooltips" style="margin-top:28px;"/>
         </div>
         <#if enableCampaignApproveList != "Y" && activeApp?exists && activeApp?has_content && activeApp == "campaign">
         <div class="col-md-2 col-sm-2">
         <div class="float-right">
         <a href="<@ofbizUrl>importApproveList?marketingCampaignId=${marketingCampaignId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary ">Import</a> 
         </div>
         </div>
         </#if>
        </div>
   </form>
</div>
<div class="clearfix"> </div>
<div class="table-responsive">
   <div class="float-right">
      <#if enableCampaignApproveList=="N">
         <button type="reset" class="btn btn-xs btn-primary" id="approveList">${uiLabelMap.approve}</button>
         <button type="reset" class="btn btn-xs btn-primary" id="notApproveList">${uiLabelMap.notApproved}</button>
      <#else>
      	 <a href="<@ofbizUrl>exportCampaignList?marketingCampaignId=${marketingCampaignId?if_exists}&contactListId=${contactListIdAL?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" style="margin-right: 10px;" id="exportCampaignList">${uiLabelMap.export}</a>
      </#if>
   </div>
   <form method="post" action="approveListUpdate" name="approveListUpdate" id="approveListUpdate">
      <input type="hidden" name="contactListId" id="contactListId" value="${contactListIdAL?if_exists}"/>
      <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
      <input type="hidden" name="approveListStatus" id="approveListStatus" value=""/>
      <input type="hidden" name="approvePage" id="approvePage" value="approveList"/>
      <input type="hidden" name="campaignIdAL" id="campaignIdAL" value="${campaignIdAL?if_exists}"/>
      <input type="hidden" name="statusIdAL" id="statusIdAL" value="${statusIdAL?if_exists}"/>
      <input type="hidden" name="rmUserLoginIdAL" id="rmUserLoginIdAL" value="${rmUserLoginIdAL?if_exists}"/>
      <table id="approveListTable" class="table table-striped">
         <thead>
            <tr>
               <th>${uiLabelMap.RM}</th>
               <th>${uiLabelMap.designation}</th>
               <th>${uiLabelMap.accountName}</th>
               <th>${uiLabelMap.contactName}</th>
               
               <#if phoneCampaignAL?exists && phoneCampaignAL?has_content && phoneCampaignAL == "Y">
               <th>${uiLabelMap.opened}</th>
               <th>${uiLabelMap.converted}</th>
               <#else>
               <th>${uiLabelMap.opened}</th>
               <th>${uiLabelMap.clicked}</th>
               <th>${uiLabelMap.converted}</th>
               </#if>
               
               <#if emailCampaignAL?exists && emailCampaignAL?has_content && emailCampaignAL == "Y">
               <th>${uiLabelMap.email}</th>
               <#else>
               <th>${uiLabelMap.phoneNumber}</th>
               <th>${uiLabelMap.email}</th>
               </#if>
               <th>${uiLabelMap.sentDate}</th>
               <th>${uiLabelMap.status}</th>
               <th>${uiLabelMap.edit}</th>
               <th>Add Contact</th>
               <th>
                   <#if enableCampaignApproveList=="N">
                     <input type="checkbox" id="selectAll" name="selectAll" value="1" />
                  </#if>
               </th>
            </tr>
         </thead>
      </table>
   </form>
</div>

<form method="post" action="updateApproveCustomerDetails" id="updateApproveCustomerDetails" class="form-horizontal" name="updateApproveCustomerDetails" novalidate="novalidate" data-toggle="validator">
    <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
    <input type="hidden" name="partyId" id="partyIdAppend" value=""/>
    <input type="hidden" name="infoString" id="infoStringAppend" value=""/>
    <input type="hidden" name="phoneNumber" id="phoneNumberAppend" value=""/>
    <input type="hidden" name="country" id="country" value=""/>
</form>

<script>

    var oTable = null;
    $(document).ready(function() {
        loadApproveList();
    });
    
    function loadApproveList() {
        var marketingCampaignIdAjaxCall = "${marketingCampaignIdAjaxCall?if_exists}";
        var contactListIdAL = "${contactListIdAL?if_exists}";
        var rmUserLoginIdAL = "${rmUserLoginIdAL?if_exists}";
        var statusIdAL = "${statusIdAL?if_exists}"; 
        var contactTypeId =  "${contactTypeId?if_exists}";
        oTable = $('#approveListTable').dataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "ajax": {
                "url": "getApproveListData",
                "type": "POST",
                data: {
                   "marketingCampaignId": marketingCampaignIdAjaxCall,
                   "contactListIdAL": contactListIdAL,
                   "statusIdAL": statusIdAL,
                   "rmUserLoginIdAL": rmUserLoginIdAL,
                   "contactTypeId":contactTypeId
                }
            },
            "Paginate": true,
            "order": [[ 2, "ASC" ]],
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                    "data": "csrFirstName",
                    "render": function(data, type, row, meta) {
                        data = '<a onclick=reassignCSR("' + row.partyId + '","' + row.partyStatusIdAL + '","' + row.csrPartyId + '","' + row.accountId +'")>' + row.rmUserLogin + '<span class="fa fa-user-o btn btn-xs btn-primary" data-toggle="modal" href="#reassignRMModal" alt="${uiLabelMap.reassign}" title="${uiLabelMap.reassign}"></span></a>';
                        return data;
                    }
                },
                {
                    "data": "designation", 
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                         data = '<div class="longtext-180" title="'+ row.designation +'">'+ row.designation +'</div>';
                         return data;
                    }
                },
                {
                    "data": "groupName",
                    "render": function(data, type, row, meta) {
                        var accountId = row.accountId;
                        if (accountId != null && accountId != "" && accountId != undefined) {
                            data = '<a href="/crm/control/viewAccount?partyId=' + row.accountId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.accountName + '(' + accountId + ')</a>';
                        } else {
                            data = 'N/A';
                        }
                        return data;
                    }
                },
                {
                    "data": "firstName",
                    "render": function(data, type, row, meta) {
                        var partyStatusId = row.partyStatusIdAL;
                        if (partyStatusId != null && partyStatusId != "" && partyStatusId == "CONTACT") {
                            data = '<a href="/crm/control/viewContact?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.contactName + ' (' + row.partyId + ')</a>';
                        } else if (partyStatusId != null && partyStatusId != "" && partyStatusId == "LEAD") {
                            data = '<a href="/crm/control/viewLead?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.contactName + ' (' + row.partyId + ')</a>';
                        } else {
                            data = row.contactName + ' (' + row.partyId + ')';
                        }
                        return data;
                    }
                },
                <#if phoneCampaignAL?exists && phoneCampaignAL?has_content && phoneCampaignAL == "Y">
                {
                    "data": "opened",
                    "orderable": false
                },
                {
                    "data": "converted",
                    "orderable": false
                },
                <#else>
                {
                    "data": "opened",
                    "orderable": false
                },
                {
                    "data": "clicked",
                    "orderable": false
                },
                {
                    "data": "converted",
                    "orderable": false
                },
                </#if>
                <#if emailCampaignAL?exists && emailCampaignAL?has_content && emailCampaignAL == "Y">
                {
                    "data": "infoString",
                    "createdCell":  function (td, cellData, rowData, row, col) {
                         $(td).attr('id', 'infoStringTD'); 
                     },
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        data = data + '<input type="hidden" class="emailContactMechIdHidden" id="emailContactMechIdHidden" name="emailContactMechIdHidden" value="'+row.emailContactMechId+'"/>';
                        return data;
                    }
                },
                <#else>
                {
                    "data": "phoneNumber",
                    "createdCell":  function (td, cellData, rowData, row, col) {
                         $(td).attr('id', 'phoneNumberTD'); 
                     },
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        data = data + '<input type="hidden" class="phoneContactMechIdHidden" id="phoneContactMechIdHidden" name="phoneContactMechIdHidden" value="'+row.phoneContactMechId+'"/>';
                        return data;
                    }
                },
                {
                    "data": "infoString",
                    "createdCell":  function (td, cellData, rowData, row, col) {
                         $(td).attr('id', 'infoStringTD'); 
                     },
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        data = data + '<input type="hidden" class="emailContactMechIdHidden" id="emailContactMechIdHidden" name="emailContactMechIdHidden" value="'+row.emailContactMechId+'"/>';
                        return data;
                    }
                },
                </#if>
                {
                    "data": "sentDate",
                    "render": function(data, type, row, meta) {
                        return row.sentDate;
                    }
                },
                {
                    "data": "isApproved",
                    "render": function(data, type, row, meta) {
                        return row.status;
                    }
                },
                {
                    "data": "rmUserLogin",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        data = '<i id="updateCustomerEditTD" class="fa fa-edit btn btn-xs btn-primary" aria-hidden="true"></i>';
                        return data;
                    }
                },
                {
                    "data": "account",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        var accountId = row.accountId;
                        var account= '';
                        if (accountId != null && accountId != "") {
                            account = '<a onclick=addAccountContact("' + row.accountId + '","${contactListIdAL?if_exists}","' + row.contactPurpose + '")><span class="fa fa-user-o btn btn-xs btn-primary" data-toggle="modal" href="#addContactModal"></span></a>';
                        }
                        return account;
                    }
                },
                {
                    "data": "checkBoxSelect",
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        var checkBoxSelect = '<input type="hidden" class="partyIdHidden" id="partyIdHidden" name="partyIdHidden" value="'+row.partyId+'"/>';
                        	checkBoxSelect = checkBoxSelect+'<input type="hidden" id="contactAccountId" name="contactAccountId" value="'+row.accountId+'"/>';
                        <#if enableCampaignApproveList=="N">
                           checkBoxSelect = checkBoxSelect+'<input type="checkbox" class="partyIdCheckBox" name="partyIdCheckBox" id="partyIdCheckBox" value="'+row.partyId+'&accountId='+row.accountId+'&accountId='+row.contactPurpose+'"/>';
                        </#if>
                        return checkBoxSelect;
                    }
                }
            ]
        });
    }
   $(document).ready(function () { 
       $("#selectAll").change(function(){  
          var status = this.checked;
          if (status) {
             $('input[name="partyIdCheckBox"]').each(function(){ 
                this.checked = status; 
             });
          } else {
             $('input[name="partyIdCheckBox"]').each(function(){ 
                this.checked = false;
             });
          }
       });
       
       $("#approveList").click(function () {
          var form = document.getElementById("approveListUpdate");
          if($('#approveListUpdate input[type="checkbox"]').is(':checked')){
             $("input:checked", oTable.fnGetNodes()).each(function(){
             $(form).append(
                $('<input>').attr('type', 'hidden').attr('name', 'partyId').val($(this).val()));
             });
             $("#approveListStatus").val("Y");
             form.submit();
           } else {
              $.notify({
                message : '<p>Please select atleast one record in the list</p>',
              });
           }
       });
       
       $("#notApproveList").click(function () {
          var form = document.getElementById("approveListUpdate");
          
          if($('#approveListUpdate input[type="checkbox"]').is(':checked')){
             $("input:checked", oTable.fnGetNodes()).each(function(){
             $(form).append(
                $('<input>').attr('type', 'hidden').attr('name', 'partyId').val($(this).val()));
             });
            $("#approveListStatus").val("X");
             form.submit();
           } else {
              $.notify({
                message : '<p>Please select atleast one record in the list</p>',
              });
           }
       });
       
       $("#approveListTable").on('click', "#updateCustomerEditTD", function(e) {
           var row = $(this).closest("tr");
           var phoneNumber = row.find("#phoneNumberTD").text();
           var infoString = row.find("#infoStringTD").text();
           //if((phoneNumber != null && phoneNumber != "") || (infoString != null && infoString != "")) {
              $(this).removeClass().addClass("fa fa-check-square-o btn btn-xs btn-success");
              $(this).attr("id", "updateCustomerUpdateTD");
           //}
           //if(phoneNumber != null && phoneNumber != "" && phoneNumber != undefined) {
              var phoneContactMechIdHidden = row.find("#phoneContactMechIdHidden").val();
              var phone = '<input type="number" id="phoneNumberTD" value="'+phoneNumber+'">'
              phone = phone + '<input type="hidden" class="phoneContactMechIdHidden" id="phoneContactMechIdHidden" name="phoneContactMechIdHidden" value="'+phoneContactMechIdHidden+'"/>';
              row.find("#phoneNumberTD").html("").append(phone);
           //} 
           //if(infoString != null && infoString != "" && infoString != undefined) {
              var emailContactMechIdHidden = row.find("#emailContactMechIdHidden").val();
              var email = '<input type="text" id="infoStringTD" value="'+infoString+ '"/>';
              email = email + '<input type="hidden" class="emailContactMechIdHidden" id="emailContactMechIdHidden" name="emailContactMechIdHidden" value="'+emailContactMechIdHidden+'"/>';
              row.find("#infoStringTD").html("").append(email);
           //}
       });

       $("#approveListTable").on('click', "#updateCustomerUpdateTD", function(e) {
           var row = $(this).closest("tr");
           var partyId = row.find("td").find('#partyIdHidden').val();
           var phoneNumber = row.find("td").find('#phoneNumberTD').val();
           var infoString = row.find("td").find('#infoStringTD').val();
           var contactAccountId = row.find("td").find('#contactAccountId').val();
           var phoneContactMechIdHidden = row.find("td").find('#phoneContactMechIdHidden').val();
           var emailContactMechIdHidden = row.find("td").find('#emailContactMechIdHidden').val();
           if(infoString != null && infoString != "" && infoString != undefined) {
              var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
              if (reg.test(infoString) == false) {
                 $.notify({
                       message : '<p>Invalid Email Address</p>',
                  });
                  return false;
              }
           }
           if((infoString == null || infoString == "" || infoString == undefined) && (phoneNumber == null || phoneNumber == "" || phoneNumber == undefined)) {
               $.notify({
                       message : '<p>Please enter valid email or phone number</p>',
               });
               return false;
           }
           if(partyId != null && partyId != "") {
              var urlString = "updateApproveCustomerDetails?partyId="+partyId+"&accountId="+contactAccountId+"&phoneNumber="+phoneNumber+"&infoString="+infoString+"&phoneContactMechId="+phoneContactMechIdHidden+"&emailContactMechId="+emailContactMechIdHidden;
              $.ajax({
                 url: urlString,
                 async: false,
                 type: 'POST',
                 success: function(data) {
                    $.notify({
                       message : '<p>'+data.status+'</p>',
                    });
                 }
              });
              if(phoneNumber != null && phoneNumber != "") {
                 row.find("#phoneNumberTD").html(phoneNumber);
              }
              if(infoString != null && infoString != "") {
                 row.find("#infoStringTD").html(infoString);
              }
           }
           $(this).removeClass().addClass("fa fa-edit btn btn-xs btn-primary");
           $(this).attr("id", "updateCustomerEditTD");
       });
      
   });
   
</script>