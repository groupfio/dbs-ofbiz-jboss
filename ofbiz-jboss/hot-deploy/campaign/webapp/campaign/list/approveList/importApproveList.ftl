<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="clearfix"> </div>


<div class="page-header">
   <h2 class="float-left">Import Approval List</h2>
   <div class="float-right">
         <form method="post" action="approveList?marketingCampaignId=${marketingCampaignId?if_exists}">
             <input type="hidden" name="activeTab" value="campList" />
             <input type="submit" value="${uiLabelMap.Back}" class="btn btn-xs btn-primary"/>
         </form>
   </div>
</div>
<form name="uploadApproveListImport" id="uploadApproveListImport" action="uploadApproveListImport" enctype="multipart/form-data" method="post">
<input type="hidden" id="marketingCampaignId" name="marketingCampaignId" value="${marketingCampaignId!}" />
<div class="row padding-r">
   <div class="col-md-6 col-sm-6 form-horizontal">
      <div class="form-group row has-error">
         <label class="col-sm-4 col-form-label">Import File</label>
         <div class="col-sm-7">
            <input id="uploadedFile" name="uploadedFile" type="file" size="30" maxlength="" class="form-control" required>
            <label class="col-form-label fw" id="errorFileId"></label>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-sm-4 col-form-label">File Format</label>
         <div class="col-sm-7">
            <select class="form-control input-sm" disabled="">
               <option value="CSV">CSV</option>
            </select>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-sm-4 col-form-label">CSV Format Template</label>
         <div class="col-sm-7">
            <a id="file-download-btn" href="/campaign-resource/template/Import Approve Temp.csv" class="btn btn-xs btn-primary ml-0" title="" download="">
                        ${uiLabelMap.download!}
            </a>
         </div>
      </div>
   </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12">
   <div class="form-group row">
      <div class="offset-sm-2 col-sm-9">
         <input type="submit" class="btn btn-xs btn-primary ml-0" value="Upload"/>
      </div>
   </div>
</div>
<form>

<div class="clearfix"> </div>
<div class="page-header">
   <h2 class="float-left">Results</h2>
</div>
<div class="table-responsive">
   <table id="approveListImportDetails" class="table table-striped">
      <thead>
         <tr>
            <th>Process Id</th>
            <th class="pl-5">Campaign Id</th>
            <th class="pl-5">${uiLabelMap.totalCount!}</th>
            <th>${uiLabelMap.successCount!}</th>
            <th>${uiLabelMap.errorCount!}</th>
            <th>${uiLabelMap.createdDate!}</th>
            <th></th>
         </tr>
      </thead>
      <tbody>
         <#assign approveListFileImport = delegator.findByAnd("ApproveListImportProcess", {"campaignId" : "${marketingCampaignId!}"}, Static["org.ofbiz.base.util.UtilMisc"].toList("createdStamp DESC"), false)>
         <#if approveListFileImport?exists && approveListFileImport?has_content>
         <#list approveListFileImport as approveListFileImportItr>
         <tr>
            <td>${approveListFileImportItr.processId!}</td>
            <#assign mktCamp = delegator.findOne("MarketingCampaign", {"marketingCampaignId" : "${marketingCampaignId!}"}, false)>
            <td>
            <#if mktCamp?exists && mktCamp?has_content>
            ${mktCamp.campaignName!} (${mktCamp.marketingCampaignId!})
            <#else>
            ${approveListFileImportItr.campaignId!}
            </#if>
            </td>
            <td class="text-right"><div class="pr-4">${approveListFileImportItr.totalCount!}</td>
            <td class="text-right"><div class="pr-4">${approveListFileImportItr.successCount!}</td>
            <td class="text-right"><div class="pr-4">${approveListFileImportItr.errorCount!}</td>
            <td>${approveListFileImportItr.createdStamp?if_exists?string["dd-MM-YYYY"]}</td>
            <td>
            <a onclick="viewApproveErrorLogs('${approveListFileImportItr.processId}')">
              <span class="btn btn-xs btn-primary ml-4" data-toggle="modal" href="#approveListErrorLogsModal">${uiLabelMap.view}</span>
            </a>
            </td>
         </tr>
         </#list>
         </#if>
      </tbody>
   </table>
</div>


<div id="approveListErrorLogsModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">Approve List Error Log</h2>
            </div>
            <div class="table-responsive">
               <table id="ajaxApproveListErrorLogsdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.processId!}</th>
                        <th>${uiLabelMap.logId!}</th>
                        <th>${uiLabelMap.campaignId!}</th>
                        <th>${uiLabelMap.errorDescription!}</th>
                        <th>${uiLabelMap.lcinId!}</th>
                        <th>${uiLabelMap.contactId}</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <a href="#" class="btn btn-sm btn-primary" id="downloadApproveLogs">${uiLabelMap.download!}</a>
            <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>

<script>
$("#uploadApproveListImport").submit(function(e){
   var fileName=$("#uploadedFile").val();
   var match = fileName.match(/(.+)\.(.+)/);
   var fileExt = match[2];
    $("#errorFileId").empty();
    if(fileExt != null && fileExt != "" && fileExt != "csv") {
        $("#errorFileId").append("Please choose CSV file");
        e.preventDefault();
    }
});
 $('#approveListImportDetails').DataTable({
    "order":[]
 });
 
 function viewApproveErrorLogs(processId) {
        $("#downloadApproveLogs").attr('href','<@ofbizUrl>downloadApproveListErrorLogs</@ofbizUrl>?processId='+ processId);
        $('#ajaxApproveListErrorLogsdatatable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "approveImportErrorLogs",
                "type": "POST",
                data: {"processId" : processId},
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                    "data": "processId"
                },
                {
                   "data": "logId"
                },
                {
                   "data": "campaignId"
                },
                {
                   "data": "errorDescription"
                },
                {
                   "data": "acctId"
                },
                {
                   "data": "partyId"
                }
            ]
        });
    }
    
</script>
