<div id="addContactModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Contact</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table id="ajaxGetAccountRelatedContacts" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.title}</th>
                        <th>${uiLabelMap.contactName}</th>
                        <th>${uiLabelMap.email}</th>
                        <th>${uiLabelMap.phoneNumber}</th>
                        <th>${uiLabelMap.emailSoliciation}</th>
                        <th>${uiLabelMap.phoneSoliciation}</th>
                        <th><button type="reset" class="btn btn-xs btn-primary" id="addCampaignList">${uiLabelMap.addToCampaignList}</button></th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="sbmit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<form method="post" action="addContactToListForm" name="addContactToListForm" id="addContactToListForm">
<#assign requestURI = "approveList"/>
<#if enableCampaignApprove?exists && enableCampaignApprove?has_content && enableCampaignApprove=="Y">
<#assign requestURI = "viewMarketingCampaign"/>
   <input type="hidden" name="activeTab" value="campList" />
</#if>
   <input type="hidden" name="contactListId" id="contactListId" value="${contactListIdAL?if_exists}"/>
   <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
   <input type="hidden" name="approvePage" id="approvePage" value="${requestURI?if_exists}"/>
   <input type="hidden" name="campaignIdAL" id="campaignIdAL" value="${campaignIdAL?if_exists}"/>
   <input type="hidden" name="statusIdAL" id="statusIdAL" value="${statusIdAL?if_exists}"/>
   <input type="hidden" name="rmUserLoginIdAL" id="rmUserLoginIdAL" value="${rmUserLoginIdAL?if_exists}"/>
   <input type="hidden" name="acctPartyId" id="acctPartyId" value=""/>
   <input type="hidden" name="contactPurposeType" id="contactPurposeType" value=""/>
</form>
<script>
    function addAccountContact(accountPartyId, contactListId, contactPurpose) {
        if (accountPartyId != null && accountPartyId != "" && contactListId != null && contactListId != "") {
            document.addContactToListForm.acctPartyId.value = accountPartyId;
            document.addContactToListForm.contactPurposeType.value = contactPurpose;
            var url = "getAccountRelatedContacts?accountPartyId=" + accountPartyId + "&contactListId=" + contactListId;
            $('#ajaxGetAccountRelatedContacts').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "ajax": {
                    "url": url,
                    "type": "POST"
                },
                "Paginate": true,
                "language": {
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "Show _MENU_ entries",
                    "zeroRecords": "No matching records found",
                    "oPaginate": {
                        "sNext": "Next",
                        "sPrevious": "Previous"
                    }
                },
                
                "pageLength": 10,
                "bAutoWidth": false,
                "stateSave": false,
                "columns": [{
                        "data": "title"
                    },
                    {
                        "data": "partyId",
                        "render": function(data, type, row, meta) {
                            data = '<a href="/crm/control/viewContact?partyId='+row.partyId+'" target="_blank">'+row.name+'('+data+')</a>';
                            return data;
                        }
                    },
                    {
                        "data": "emailAddress"
                    },
                    {
                        "data": "phoneNumber"
                    },
                    {
                        "data": "emailSolicitation"
                    },
                    {
                        "data": "phoneSolicitation"
                    },
                    {
                        "data": "checkBoxSelect",
                        "render": function(data, type, row, meta) {
                            data = '<input type="checkbox" class="addContactInListCheckBox" name="addContactInListCheckBox" value="'+row.partyId+'"/>';
                            
                            return data;
                        }
                    }
                ]
            });
        }
    }
    $("#addCampaignList").click(function () {
          var form = document.getElementById("addContactToListForm");
          if($('.addContactInListCheckBox').is(':checked')){
             $('.addContactInListCheckBox:checked').each(function() {
               $(form).append($('<input>').attr('type', 'hidden').attr('name', 'partyId').val($(this).val()));
             });
             form.submit();
           } else {
              $.notify({
                message : '<p>Please select atleast one record in the list</p>',
              });
           }
    });
</script>