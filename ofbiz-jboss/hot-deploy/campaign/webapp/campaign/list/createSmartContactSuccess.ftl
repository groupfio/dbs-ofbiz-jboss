
<#assign requestURI = request.getRequestURI()/>
<#if requestParameters.contactListId?exists && requestParameters.contactListId?has_content>
	<#assign contactListId = requestParameters.contactListId?if_exists>
</#if>
<#if requestParameters.updateFlag?exists && requestParameters.updateFlag?has_content>
	<#assign updateFlag = requestParameters.updateFlag?if_exists>
</#if>
<script type="text/javascript" src="/cm_js/1.7.2/jquery.min.js"></script>
<script>
	
	<#-- var url="<@ofbizUrl>viewContactList</@ofbizUrl>?contactListId=${contactListId?if_exists}";-->
	<#if updateFlag?has_content>
	<#if updateFlag!="Y">
	var url="<@ofbizUrl>findContactList</@ofbizUrl>?updateFlag=N";
	<#else>
	var url="<@ofbizUrl>findContactList</@ofbizUrl>?updateFlag=${updateFlag?if_exists}";
	</#if>
	<#else>
	var url="<@ofbizUrl>findContactList</@ofbizUrl>?updateFlag=N";
	</#if>
	
	
	parent.window.location.href=url;	
</script>