<!-- modified by Manjesh-->
<link href="/campaign-resource/js/smartList/query-builder.default.min.css" rel="stylesheet">
<script src="/campaign-resource/js/smartList/query-builder.standalone.min.js"></script>
<script src="/campaign-resource/js/smartList/sql-parser.js"></script>
<style>

#getsql {
    border:1px solid #555;
    ;
    padding: 6px 18px;
}
#queryBuilderGoesHere {
    border-bottom: 1px solid navy;
}
.ltblue {
    background: lightblue;
    color:white;
    font-weight:bold;
}

#builder_group_0 {
width: 100%;
}
#builder2_group_0 {
width: 100%;
}
#builder3_group_0 {
width: 100%;
}
.rule-operator-container span{
   display:none;
}
.rules-group-header label.btn.active {
    background: #32cd32 !important;
    border-color: #32cd32 !important;
}
.rules-group-header label.btn {
    background: #a9a9a9 !important;
    border-color: #a9a9a9 !important;
}
</style>
<#assign roleType ="
<select class='form-control' name='roleTypeId' id='roleTypeId' onchange='filterRoles(this.value);'>
   <option value=''>--Select--</option>
   <option value='PROSPECT'>Lead</option>
   <option value='ACCOUNT'>Account</option>
   <option value='CUSTOMER'>Customer</option>
   <option value='CONTACT'>Contact</option>
</select>
" >  
      <div class="page-header border-b">
         <h1 class="float-left">Smart List</h1>
         <div class="float-right">
            ${roleType?if_exists}
         </div>
      </div>
      <div style="margin-top: 10px;">
         <div class="page-header">
            <h2 class="float-left">Segmentation </h2>
         </div>
         <div id="builder"></div>
      </div>
<script>
<#if criteria?has_content>
	var criteria = ${StringUtil.wrapString(criteria?if_exists)};
</#if>
function filterRoles(element){
  var url="";
 	if(element==""){
 	     url="<@ofbizUrl>smartList</@ofbizUrl>";
 		 window.location = url;
 	}else{
 	     url="<@ofbizUrl>smartList</@ofbizUrl>?id="+element;
 	     window.location = url;	
 	}
 
 }
                var element='';
				<#if requestParameters.id?has_content>
				element = "${requestParameters.id?if_exists}";
				<#elseif contactList?has_content >
				 element = "${contactList.roleTypeId?if_exists}";
					$('#roleTypeId option[value=element]');
				</#if>
window.onload = function() {
         $('[name=roleTypeId]').val(element);
         $.getJSON("getSmartListJsonData?groupType=SEGMENTATION&roleTypeId="+element,function(result){
    				data = result;
    				
    				$('#builder').queryBuilder({
					  sortable: true,
					  filters:result
					});
					
					<#if criteria?has_content>
					
					var ruless = parseJsonSegmentCode(criteria,'CUSTOM_FIELD');
					//alert(JSON.stringify(ruless));
					  if(jQuery.isEmptyObject(ruless)){
					  
					  }else{
					  
					    $('#builder').queryBuilder('setRules',ruless);
					
						
						}
						
					</#if>
					$(".rule-operator-container > span:contains('equal')").hide();
  				});
	
        $.getJSON("getSmartListJsonData?groupType=CUSTOM_FIELD&roleTypeId="+element,function(result){
				//data = result;
				
				$('#builder2').queryBuilder({
				  sortable: true,
				  filters:result
				});
				
				
				   <#if criteria?has_content>
					   
						var rulesss = parseJsonCustomField(criteria,'CUSTOM_FIELD');
						
					  if(jQuery.isEmptyObject(rulesss)){
					  
					  }else{
					
					    $('#builder2').queryBuilder('setRules',rulesss);
						
						
						}
						
					</#if>
			});
			
	  $.getJSON("getSmartListJsonData?groupType=ECONOMIC_METRIC&roleTypeId="+element,function(result){
				$('#builder3').queryBuilder({
				   sortable: true,
				  filters:result
				});
				   <#if criteria?has_content>
					   
						var rulesss7 = parseJsonEconomicData(criteria,'ECONOMIC_METRIC');
						
					  if(jQuery.isEmptyObject(rulesss7)){
					  
					  }else{
					
					    $('#builder3').queryBuilder('setRules',rulesss7);
						
						
						}
						
					</#if>
			});
  				  				
  				var timeOut = 1500;
				var block ="N"
				setTimeout(function(){
    
				if(block == "N")
				    {
				    block = "Y";
				    
			    	}else{
			    	
			    	}	
			    },timeOut);
				
			
	$( "div[data-add='group']" ).hide();
}	
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true && JSON.stringify(obj) === JSON.stringify({});
}
</script>	
<script>
function parseQuerySmart(contactListId) {
var role = "${requestParameters.id?if_exists}";
<#if contactList?has_content >
role = "${contactList.roleTypeId?if_exists}";
</#if>

  var segment = $('#builder').queryBuilder('getRules');
  var customFields = $('#builder2').queryBuilder('getRules');
  var economic = $('#builder3').queryBuilder('getRules');
  if(segment==null && customFields==null && economic==null)
  {
    $("#myModal").modal('hide');
    return false;
  }else{
      $("#myModal").modal('show');
  }
  var myobj = {"segment":segment,"custom":customFields,"economic":economic};
  var finalJsonObject = JSON.stringify(myobj);
  	finalJsonObject = finalJsonObject.replace(/\\/g, "");
    finalJsonObject = finalJsonObject.replace(/"/g, "quot;");
	if(contactListId !=0)
	{
		document.getElementById('hiddenjson').value = finalJsonObject;
		document.getElementById('criteriaForm').submit();
	}
	else{
		/*var urlLink="createSmartContactList?jsonString="+encodeURIComponent(finalJsonObject)+"&role="+role;
		document.getElementById('serializedSubmit').href=urlLink;	
		document.getElementById('serializedSubmit').click(); */
		document.getElementById('jsonString').value = finalJsonObject;
		
		
		$("#loaderGif").css("display","none");
	}
}
</script>
<div style="margin-top: 10px;">
         <div class="page-header">
            <h2 class="float-left">Attributes</h2>
         </div>
         <div id="builder2"></div>
      </div>
      <div style="margin-top: 10px;">
         <div class="page-header">
            <h2 class="float-left">Economic Metric</h2>
         </div>
         <div id="builder3"></div>
      </div>
      <div style="float:left;width:100%;">
         <#if requestParameters.contactListId?has_content>
         <#if requestParameters.action?has_content && requestParameters.action="clone"> 
         <div style="width:50%;float:left;"><button type="button" id="createCloneBtn" onClick="javascript:submitCreateSmartListForm();" class="btn btn-primary" ><i class=""></i> Create Clone List</button></div>
         <#else> 
         <div style="width:50%;float:left;">
         <button type="button" id="updateBtn" onClick="javascript:parseQuerySmart(${requestParameters.contactListId?if_exists})" class="btn btn-sm btn-primary" ><i class=""></i> Update</button>
         <button class="btn btn-sm btn-secondary" title="Create" data-toggle="modal" >Cancel</button>
         </div>
         </#if>
         <#else>
         <div>
            <button onClick="javascript:parseQuerySmart(0)" class="btn btn-sm btn-primary" title="Create" data-toggle="modal" >Submit</button>
            <button class="btn btn-sm btn-secondary" title="Create" data-toggle="modal" >Clear</button>
         </div>
         </#if>
         <input type="hidden" name="roleTypeParameter" value="${requestParameters.id?if_exists}">
         <div style="width:50%;float: left;text-align: right;display:none;" id="processButton">
            <form action="createSmartContactListValues" name="criteriaForm" id="criteriaForm" method="post">
               <input type="hidden" name="marketingCampaignId" value="${requestParameters.marketingCampaignId?if_exists}"/>
               <input type="hidden" name="contactListId" value="${requestParameters.contactListId?if_exists}"/>
               <input name="jsonString" value="" type="hidden" id="hiddenjson" />
            </form>
         </div>
      </div>
      <span id="loaderImage" style="position: absolute;display: none; ">
      <img id="loaderGif"  style="padding-left: 300px;height:40px" src="/images/ajax-loader-big.gif">
      </span>
      <br>
      <br>
      <br>
      <table id="resultGrid"></table>
      <a id="serializedSubmit" class="createList"></a>
   </div>
   <div id="ajaxLoader" name="ajaxLoader" style="display:none;">
      <img src="/rms_images/progress.gif" id="image" style="width: 10%; margin-left: 45%;"/>
   </div>
   <div class="clearfix"> </div>

<!-- /.container -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Create Smart List</h4>
         </div>
         <div class="modal-body">
            <form method="post" action="createContactListAndAssignParties" id="createSmartContactListForm" class="form-horizontal" data-toggle="validator" name="createSmartContactListForm" novalidate="true">
               <input type="hidden" name="contactListTypeId" value="MARKETING">
               <input type="hidden" name="totalPartyIds" value="">
               <input type="hidden" name="jsonString" id="jsonString" value="${parameters.jsonString?if_exists}">
               <input type="hidden" name="roleTypeParameter" value="${requestParameters.id?if_exists}">
               <div class="row padding-r">
                  <div class="col-md-12 col-sm-12 ">
                     <div class="form-group row has-error">
                        <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.name!}</label>
                        <div class="col-sm-7">
                           <input type="text" class="form-control input-sm" value="${parameters.contactListName?if_exists}" name="contactListName" id="contactListName" required>
                           <div class="help-block with-errors"></div>
                        </div>
                     </div>
                     <div class="form-group row  has-error">
                        <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.contactType!}</label>
                        <div class="col-sm-7">
                           <select name="contactMechTypeId"  value="${parameters.contactMechTypeId?if_exists}"  id="createSmartContactListForm_contactMechTypeId" class="ui dropdown search form-control input-sm" required>
                              <option value="">---Select Type---</option>
                              <option value="EMAIL_ADDRESS" <#if contactList?has_content && contactList.contactMechTypeId?if_exists=="EMAIL_ADDRESS">selected</#if>>Email Address</option>
                              <option value="POSTAL_ADDRESS" <#if contactList?has_content && contactList.contactMechTypeId=="POSTAL_ADDRESS">selected</#if>>Postal Address</option>
                              <option value="TELECOM_NUMBER" <#if contactList?has_content && contactList.contactMechTypeId=="TELECOM_NUMBER">selected</#if>>Phone Number</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group row  has-error">
                        <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.contactPurposeType!}</label>
                        <div class="col-sm-7">
                           <select name="contactPurposeTypeId"  value="${parameters.contactPurposeTypeId?if_exists}"  id="createSmartContactListForm_contactPurposeTypeId" class="ui dropdown search form-control input-sm" required>
                              <option value="">---Select Type---</option>
                              <option value="TEST" <#if contactList?has_content && contactList.contactPurposeTypeId?if_exists=="TEST">selected</#if>>Test List</option>
                              <option value="LIVE" <#if contactList?has_content && contactList.contactPurposeTypeId=="LIVE">selected</#if>>Production List</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group row has-error">
                        <label  class="col-sm-4 col-form-label">${uiLabelMap.description!}</label>
                        <div class="col-sm-7">
                           <textarea class="form-control" rows="3" name="description" id="createSmartContactListForm_description" placeholder="Enter Description" required></textarea>
                           <div class="help-block with-errors"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-sm-12">
                  <div class="form-group row">
                     <div class="offset-sm-2 col-sm-9">
                        <input type="submit" class="btn btn-sm btn-primary mt" value="Submit" />
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<script>
 
 function parseJsonCustomField(criteria,method){
    
	 var viewData = { 
		condition : criteria.custom.condition,
		rules : []
	};
	
	var i=0;
	
	 criteria.custom.rules.forEach(function(column) 
    {	
		viewData.rules.push({id : column.id,
		field : column.field,
		type : column.type,
		input : column.input,
		operator : column.operator,
		value : column.value
		});
		
		i = i+1;

    });
	
	if(i==0)viewData = {};
	
	return viewData;
	
 }
 
  function parseJsonEconomicData(criteria,method){
    
	var viewData = { 
		condition : criteria.economic.condition,
		rules : []
	};
	
	var i=0;
	
	 criteria.economic.rules.forEach(function(column) 
    {
		viewData.rules.push({id : column.id,
		field : column.field,
		type : column.type,
		input : column.input,
		operator : column.operator,
		value : column.value
		});
		
		i = i+1;

    });
	
	if(i==0)viewData = {};
	
	return viewData;
	
 }
 
 
 
 function parseJsonSegmentCode(criteria,method){
    //alert("1");
	 var viewData = { 
		condition : criteria.segment.condition,
		rules : []
	};
	
	var i=0;
	
	 criteria.segment.rules.forEach(function(column) 
    {
		if(method == column.id || "PARTY_ADDITIONAL_DATA" == column.id ){
		  return;
		}else{
		  
		}
		
		viewData.rules.push({id : column.id,
		field : column.field,
		type : column.type,
		input : column.input,
		operator : column.operator,
		value : column.value
		});
		
		i = i+1;

    });
	
	if(i==0)viewData = {};
	
	return viewData;
	
 }
 function executePrepareSmartList(contactListId){
   if(contactListId != null && contactListId != "") {
     $("#hideScreen").hide();
     $("#ajaxLoader").show();
     $("#executePrepareSmartList").prop("disabled",true);
     $.getJSON("prepareSmartList",{"contactListId":contactListId},function(data) {
       if(data.prepareSmartListStatus != "" && data.prepareSmartListStatus != null && data.prepareSmartListStatus == "SUCCESS") {
          //alert("List generation process is completed for smart list");
          var timer = setTimeout(function() {
          window.location='<@ofbizUrl>smartList</@ofbizUrl>?contactListId='+contactListId
          }, 5000);
       } else {
          $("#ajaxLoader").hide();
          $("#hideScreen").show();
          alert("Error in execute smart list"+data.prepareSmartListStatus);
       }
     });
     $("#executePrepareSmartList").prop("disabled",false);
   }
 }
</script>
<script>
$(function(){

        $('[name="builder_rule_0_filter"]').change(function(){
    });
    
   
});
</script>