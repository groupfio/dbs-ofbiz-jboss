<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

    <#if isUpdate?if_exists="true">
        <div class="page-header border-b">
            <h1>${uiLabelMap.updateContactList!}</h1>
        </div>
        <form method="post" action="<@ofbizUrl>updateRmsContactList</@ofbizUrl>" id="createContactListForm" class="form-horizontal" name="createContactListForm" data-toggle="validator" onsubmit="return onSubmitValidate(this);">
     <#else>
        <div class="page-header border-b">
            <h1>${uiLabelMap.createContactList!}</h1>
        </div>
        <form method="post" action="<@ofbizUrl>createContactLists</@ofbizUrl>" id="createContactListForm" class="form-horizontal" name="createContactListForm"  data-toggle="validator" onsubmit="return onSubmitValidate(this);">
     </#if>
            <input type="hidden" name="contactListId" value="<#if contactList?exists>${contactList.contactListId?if_exists}</#if>">
            <input type="hidden" name="isValidate" id="isValidate" value="Y"/> 
            <div class="row padding-r">
                <div class="col-md-6 col-sm-6 ">
                     <@generalInput 
			         id="createContactListForm_contactListName"
			         name="contactListName"
			         label=uiLabelMap.name
			         placeholder="Enter List Name"
			         value=contactList?if_exists.contactListName?if_exists
			         required=true
			         maxlength="100"
			         />
			         <@dropDown
			         id="contactMechTypeId"
                     label=uiLabelMap.contactType
                     options=contactTypeList
                     value=contactList?if_exists.contactMechTypeId?if_exists
                     required=true
                     allowEmpty=true
                     dataLiveSearch = true
                     />
                     <@dropDown 
                     id="contactPurposeTypeId"
                     label=uiLabelMap.contactPurposeType
                     options=contactPurposeTypeList
                     value=contactList?if_exists.contactPurposeTypeId?if_exists
                     required=true
                     allowEmpty=true
                     dataLiveSearch = true
                     />
                     
                     <#assign emailType = "" />
                     <#if contactList?exists && contactList?has_content>
                     	<#assign emailType = contactList.emailType?if_exists />
                     </#if>
                     <div class="form-group row">
			            <label class="col-sm-4 col-form-label">${uiLabelMap.emailType}</label>
			            <div class="col-sm-7">
			               <div class="form-check-inline">
			                  <label class="form-check-label"> 
			                  <input class="form-check-input" id="emailType" name="emailType" value="SENSITIVE" type="radio" <#if emailType?exists && emailType == "SENSITIVE">checked</#if> <#if numberOfMembers?has_content && numberOfMembers &gt; 0>disabled</#if>>Sensitive
			                  </label>
			               </div>
			               <div class="form-check-inline">
			                  <label class="form-check-label"> 
			                  <input class="form-check-input" id="emailType" name="emailType" value="NONSENSITIVE" type="radio" <#if emailType?exists && (emailType == "NONSENSITIVE" || emailType == "") >checked</#if> <#if numberOfMembers?has_content && numberOfMembers &gt; 0>disabled</#if>>Non Sensitive
			                  </label>
			               </div>
			            </div>
			         </div>
                    
                    <#-- <#if isUpdate?if_exists="true">
                     <@dropDown 
				      id = "isValidate"
				      label = "Validation Required"
				      options = flagMap
				      value = ""
				      allowEmpty=false
				      required=true
				      disabled=true
				      dataLiveSearch = true
				     />
				     <#else>
				     <@dropDown 
				      id = "isValidate"
				      label = "Validation Required"
				      options = flagMap
				      value = ""
				      allowEmpty=false
				      required=true
				      dataLiveSearch = true
				     />
				     </#if> -->
	                <@textareaInput
			 		id="description"
			 		label=uiLabelMap.description
			 		rows="3"
			 		placeholder = "Description"
					value = contactList?if_exists.description?if_exists
					maxlength="255"
					required = true
					/>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-9">
                    <#if isUpdate?if_exists="true">
                        <button type="submit" class="btn btn-sm btn-primary" >${uiLabelMap.update!}</button>
                    <#else>
                        <button type="submit" class="btn btn-sm btn-primary mt-2" >Submit</button>
                        <button type="reset" class="btn btn-sm btn-secondary mt-2 reset">Clear</button>
                    </#if>    
                    </div>
                </div>
            </div>
        </form>
        <div class="clearfix"> </div>

<script>
$(document).ready(function() {
  $("#contactMechTypeId").change(function() {
     $("#contactMechTypeId_error").empty();
     if($(this).val() == null || $(this).val() == "") {
        $("#contactMechTypeId_error").css('display','block');
        $("#contactMechTypeId_error").append('<ul class="list-unstyled"><li id="error1">Please select an item in the list.</li></ul>');
     } else {
        $("#contactMechTypeId_error").css('display','none');
     }
   });   
     
     $("#contactPurposeTypeId").change(function() {
     $("#contactPurposeTypeId_error").empty();
     if($(this).val() == null || $(this).val() == "") {
        $("#contactPurposeTypeId_error").css('display','block');
        $("#contactPurposeTypeId_error").append('<ul class="list-unstyled"><li id="error2">Please select an item in the list.</li></ul>');
     } else {
        $("#contactPurposeTypeId_error").css('display','none');
     }
  });
  
  $('.reset').click(function(){
   $('#createContactListForm').trigger("reset");
   
   $("#contactMechTypeId_error").empty();
   $("#error1").empty();   
   $("#contactMechTypeId option[value='${contactMechTypeId?if_exists}']").removeAttr('selected');
      $("#contactMechTypeId option[value='${contactMechTypeId?if_exists}']").removeAttr('selected');
   $('.text').empty();
    $('.text').removeClass('.text').addClass('default text');
   //$('.text').text("Please Select");
    $('.default.text').text("Please Select");
  $('.dropdown.icon.clear').removeClass('.dropdown icon clear').addClass('dropdown icon');
   $("#contactPurposeTypeId_error").empty();
   $("#error2").empty();
   $("#contactPurposeTypeId option[value='${contactPurposeTypeId?if_exists}']").removeAttr('selected');
  });
});
  function onSubmitValidate() {
    var contactMechTypeId = $("#contactMechTypeId").val();
     var contactPurposeTypeId = $("#contactPurposeTypeId").val();
    $("#contactMechTypeId_error").empty();
    $("#error1").empty();
          $("#contactPurposeTypeId_error").empty();
      $("#error2").empty();
    if(contactMechTypeId == '' || contactMechTypeId == null || contactPurposeTypeId == '' || contactPurposeTypeId == null ) {
         if(contactMechTypeId == '' || contactMechTypeId == null){
         $("#contactMechTypeId_error").css('display','block');
         $("#contactMechTypeId_error").append('<ul class="list-unstyled"><li id="error1">Please select an item in the list.</li></ul>');
         }
         if(contactPurposeTypeId == '' || contactPurposeTypeId == null){
         $("#contactPurposeTypeId_error").css('display','block');
         $("#contactPurposeTypeId_error").append('<ul class="list-unstyled"><li id="error2">Please select an item in the list.</li></ul>');
         }
         return false;
     } else {
        $("#contactMechTypeId_error").css('display','none');
         $("#contactPurposeTypeId_error").css('display','none');
     }
  }
</script>

