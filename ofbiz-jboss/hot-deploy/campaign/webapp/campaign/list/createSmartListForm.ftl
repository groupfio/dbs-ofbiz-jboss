
<script type="text/javascript" src="/bootstrap/js/js/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/js/jquery.validate.min.js"></script>
<script>
$(document).ready(function(){	

	$("#createSmartContactListForm").validate({
	
		rules:{			
			contactListName:"required",
			description:"required",
			contactPurposeTypeId:"required",
			contactMechTypeId:"required"
			
		},
		messages:{			
			contactListName:"<a font style='color:red'>&nbsp;Please enter contact list name</a>",
			description:"<a font style='color:red'>&nbsp;Please enter the description</a>",
			contactPurposeTypeId:"<a font style='color:red'>&nbsp;Please enter the contact purpose type</a>",
			contactMechTypeId:"<a font style='color:red'>&nbsp;Please enter the contact type</a>"
		}	
	});

});


$(document).ready(function(){	

	$("#updateContactListForm").validate({
	
		rules:{			
			contactListName:"required",
			description:"required",
			contactMechTypeId:"required",
			contactPurposeTypeId:"required"
			
		},
		messages:{			
			contactListName:"<a font style='color:red'>&nbsp;Please enter contact list name</a>",
			description:"<a font style='color:red'>&nbsp;Please enter the description</a>",
			contactMechTypeId:"<a font style='color:red'>&nbsp;Please enter the contact type</a>",
			contactPurposeTypeId:"<a font style='color:red'>&nbsp;Please enter the contact purpose type</a>"
		}	
	});

});

function closePopup(){
	document.createSmartContactSuccess.submit();	
}
 </script>
 
 
<#if requestAttributes.errorMessageList?has_content><#assign errorMessageList=requestAttributes.errorMessageList></#if>
<#if requestAttributes.eventMessageList?has_content><#assign eventMessageList=requestAttributes.eventMessageList></#if>

<#-- display the error messages -->
<#if errorMessageList?has_content>
<div class="messages">
<div class="errorMessageHeader">${uiLabelMap.CommonFollowingErrorsOccurred}:</div>
<ul class="errorList">
 
  <#list errorMessageList?if_exists as errorMsg>
    <li class="errorMessage">${StringUtil.wrapString(errorMsg)}</li>
  </#list>
</ul>
</div>
</#if>
<#if eventMessageList?has_content>
<div class="messages">
<div class="eventMessageHeader">${uiLabelMap.CommonFollowingOccurred}:</div>
<ul class="eventList">
  <#list eventMessageList as eventMsg>
    <li class="eventMessage">${StringUtil.wrapString(eventMsg)}</li>
  </#list>
</ul>
</div>

</#if>
 
 
   <div class="frameSectionHeader" style="">
    	<div class="x-panel-tl">
      		<div class="x-panel-tr">
        		<div class="x-panel-tc">
          			<div class="x-panel-header" id="">Create Smart List</div>
        		</div>
      		</div>
    	</div>
  	</div>

<form method="post" action="createContactListAndAssignParties" id="createSmartContactListForm" class="basic-form" onsubmit="javascript:submitFormDisableSubmits(this)" name="createSmartContactListForm" novalidate="novalidate">
<input type="hidden" name="contactListTypeId" value="MARKETING">
<input type="hidden" name="totalPartyIds" value="">
<#--<input type="hidden" name="contactMechTypeId" value="EMAIL_ADDRESS">-->
<input type="hidden" name="jsonString" value="${parameters.jsonString?if_exists}">
<input type="hidden" name="roleTypeParameter" value="${parameters.role?if_exists}">
<div class="fieldgroup"><div class="fieldgroup-title-bar"><table><tbody><tr><td class="collapse"></td><td></td></tr></tbody></table></div><div id="_G27__body" class="fieldgroup-body"> <table cellspacing="0">
  <tbody><tr>
   <td class="label"><span class="requiredField">Name</span></td>
   <td><input type="text" class="inputBox" value="${parameters.contactListName?if_exists}" name="contactListName" size="25" id="createSmartContactListForm_contactListName">
</td>
  </tr>
  <tr>
   <td class="label"><span class="requiredField">Contact Purpose Type</span></td>
   <td><select class="inputBox" name="contactPurposeTypeId"  value="${parameters.contactPurposeTypeId?if_exists}"  id="createSmartContactListForm_contactPurposeTypeId" size="1">
   <option value="">&nbsp;</option>
   <option value="TEST" <#if parameters.contactPurposeTypeId?has_content && parameters.contactPurposeTypeId == "TEST"></#if>>Test List</option>
   <option value="LIVE">Production List</option></select></td>
  </tr>
  <tr>
   <td class="label"><span class="requiredField">Contact Type</span></td>
   <td>
   		<select class="inputBox" name="contactMechTypeId"  value="${parameters.contactMechTypeId?if_exists}"  id="createSmartContactListForm_contactMechTypeId" size="1">
		   <option value="">&nbsp;</option>
		   <option value="EMAIL_ADDRESS" <#if parameters.contactMechTypeId?has_content && parameters.contactMechTypeId == "EMAIL_ADDRESS"></#if>>Email Address</option>
		   <option value="POSTAL_ADDRESS"  <#if parameters.contactMechTypeId?has_content && parameters.contactMechTypeId == "POSTAL_ADDRESS"></#if>>Postal Address</option>
	   	   <option value="TELECOM_NUMBER" <#if parameters.contactMechTypeId?has_content && parameters.contactMechTypeId == "TELECOM_NUMBER"></#if>>Phone Number</option>	   	   
	   	</select>
   </td>
  </tr>
  <tr>
   <td class="label"><span class="requiredField">Description</span></td>
   <td><textarea class="inputBox" name="description" cols="60" rows="3" id="createSmartContactListForm_description">${parameters.description?if_exists}</textarea></td>
  </tr>
  <tr>
   <td class="label">&nbsp;</td>
   <td colspan="4"><input type="submit" class="smallSubmit" name="submitButton" value="Create Smart List"></td>
  </tr>
 </tbody></table>
</div></div></form>

