<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header border-b">
   <h1 class="float-left">${uiLabelMap.viewContactList!}
    - <#if contactList?has_content && contactList?exists>${contactList.contactListName?if_exists}</#if></h1>
   <p class="float-right">
      <a href="<@ofbizUrl>updateContactListForm?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Edit</a>
      <a href="<@ofbizUrl>findContactListMember?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">${uiLabelMap.findContactMembers!}</a>
      <#--<button type="reset" class="btn btn-xs btn-primary">${uiLabelMap.updateTagsInfo}</button>-->
   </p>
</div>
<div class="row padding-r">
   <div class="col-md-6 col-sm-6 form-horizontal">
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.name!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactList.contactListName?if_exists}</#if></label>
         </div>
      </div>
      <#if contactList?has_content>
      <#if contactList?if_exists.contactMechTypeId?if_exists == "TELECOM_NUMBER">		
      <#assign contactDescription = "Phone Number">
      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "EMAIL_ADDRESS">
      <#assign contactDescription = "Email Address">
      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "POSTAL_ADDRESS">
      <#assign contactDescription = "Postal Address">
      </#if>
      </#if>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.contactType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactDescription?if_exists}</#if></label>
         </div>
      </div>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.description!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactList.description?if_exists}</#if></label>
         </div>
      </div>
      
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.contactPurposeType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">
            <#if contactList?has_content && contactList?exists>
	            <#if contactList.contactPurposeTypeId?if_exists == "TEST">
	            ${uiLabelMap.testList!}
	            <#elseif contactList.contactPurposeTypeId?if_exists == "LIVE">
	            ${uiLabelMap.productionList!}
	            </#if>
            </#if>
            </label>
         </div>
      </div>
      	
      <#if contactList?has_content>
	      <#if contactList.emailType?if_exists == "SENSITIVE">		
	      	<#assign emailType = "Sensitive">
	      <#elseif contactList.emailType?if_exists == "NONSENSITIVE">
	      	<#assign emailType = "Non Sensitive">
	      </#if>
      </#if>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.emailType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if emailType?exists>${emailType?if_exists}</#if></label>
         </div>
      </div>
      
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.numberOfAccount!}</label>
         <div class="col-sm-7">
          <#if roleTypeIdVal?has_content && roleTypeIdVal="LEAD">
            <label class="col-form-label input-sm fw">${numberOfMembers?if_exists}</label>
          <#else>
            <label class="col-form-label input-sm fw">${accountInListCount?if_exists}</label>
          </#if>
         </div>
      </div>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.numberOfContacts!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">${numberOfMembers?if_exists}</label>
         </div>
      </div>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">Created Date</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">${contactList.createdStamp?string["dd-MM-yyyy"]?if_exists}</label>
         </div>
      </div>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.importedStatus!}</label>
         <div class="col-sm-8">
           <#if roleTypeIdVal?exists &&  roleTypeIdVal=="ACCOUNT">
            <label class="col-form-label input-sm fw"> TOTAL:${totalNoOfParties?if_exists};  
               Success:<#if totalNoOfSucess?has_content && totalNoOfSucess?exists>${totalNoOfSucess?if_exists}<#else>0</#if>;  No Party Found:<#if totalNoPartyFound?has_content && totalNoPartyFound?exists>${totalNoPartyFound?if_exists}<#else>0</#if>;  Dedup:<#if totaldedupCount?has_content && totaldedupCount?exists>${totaldedupCount?if_exists}<#else>0</#if>;  Disabled Party: <#if totalDisableParty?has_content && totalDisableParty?exists>${totalDisableParty?if_exists}<#else>0</#if>;
               Role Not Found: <#if roleNotFoundCount?has_content && roleNotFoundCount?exists>${roleNotFoundCount?if_exists}<#else>0</#if>;
               No Default Contact Found: <#if totalNoDefaultCount?has_content && totalNoDefaultCount?exists>${totalNoDefaultCount?if_exists}<#else>0</#if>;
               No Active Flag Found: <#if noFlagCount?has_content && noFlagCount?exists>${noFlagCount?if_exists}<#else>0</#if>;
               <!--No Unique Default Contact: <#if noUniqueContactCount?has_content && noUniqueContactCount?exists>${noUniqueContactCount?if_exists}<#else>0</#if>-->
            </label>
              <#elseif roleTypeIdVal?exists &&  roleTypeIdVal=="CONTACT">
              <label class="col-form-label input-sm fw"> TOTAL:${totalNoOfParties?if_exists};
              Success:<#if totalNoOfSucess?has_content && totalNoOfSucess?exists>${totalNoOfSucess?if_exists}<#else>0</#if>;  No Party Found:<#if totalNoPartyFound?has_content && totalNoPartyFound?exists>${totalNoPartyFound?if_exists}<#else>0</#if>;  Dedup:<#if totaldedupCount?has_content && totaldedupCount?exists>${totaldedupCount?if_exists}<#else>0</#if>;  Disabled Party: <#if totalDisableParty?has_content && totalDisableParty?exists>${totalDisableParty?if_exists}<#else>0</#if>;
              Role Not Found: <#if roleNotFoundCount?has_content && roleNotFoundCount?exists>${roleNotFoundCount?if_exists}<#else>0</#if>;
              <!--No Account Associated: <#if noAccountCount?has_content && noAccountCount?exists>${noAccountCount?if_exists}<#else>0</#if>-->
              </label>
          <#else>
          <label class="col-form-label input-sm fw"> TOTAL:${totalNoOfParties?if_exists};  
             Success:<#if totalNoOfSucess?has_content && totalNoOfSucess?exists>${totalNoOfSucess?if_exists}<#else>0</#if>;  No Party Found:<#if totalNoPartyFound?has_content && totalNoPartyFound?exists>${totalNoPartyFound?if_exists}<#else>0</#if>;  Dedup:<#if totaldedupCount?has_content && totaldedupCount?exists>${totaldedupCount?if_exists}<#else>0</#if>;  Disabled Party: <#if totalDisableParty?has_content && totalDisableParty?exists>${totalDisableParty?if_exists}<#else>0</#if>;
             Role Not Found: <#if roleNotFoundCount?has_content && roleNotFoundCount?exists>${roleNotFoundCount?if_exists}<#else>0</#if>
           </label>
          </#if>
         </div>
      </div>
   </div>
</div>
<div class="clearfix"> </div>
<div class="page-header">
  <#if isAssociated?exists && !isAssociated>
   <h2 class="float-left">Upload Section</h2>
   <p class="float-right">
      <a href="<@ofbizUrl>addNewMember?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary m5" role="button">Add New Contacts</a>
   </p>
   </#if>
</div>
<ul class="nav nav-tabs">
<#if isAssociated?exists && !isAssociated>
	<#assign classtab1 = "tab-pane fade show active"/>
	<#assign classtab2 = "tab-pane fade show"/>
         <li class="nav-item"><a data-toggle="tab" href="#tab1" class="nav-link active">${uiLabelMap.upload!}</a></li>
         <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link">${uiLabelMap.results!}</a></li>
   <#else>
   <#assign classtab1 = "tab-pane fade show"/>
   <#assign classtab2 = "tab-pane fade show active"/>
         <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link active">${uiLabelMap.results!}</a></li>
         <li class="nav-item"><a data-toggle="tab" href="#tab3" class="nav-link ">${uiLabelMap.campaignList!}</a></li>
</#if>
</ul>
<div class="tab-content">
<#if isAssociated?exists && !isAssociated>
   <div id="tab1" class="${classtab1}">
      <form action="uploadPartiesFile?contactListId=${requestParameters.contactListId?if_exists}" method="Post" id="uploadPartiesFile" name="uploadTxtFile" enctype="multipart/form-data">
         <#if contactList?has_content>
         <#if contactList.contactMechTypeId == "TELECOM_NUMBER">		
         <#assign contactMechType = "PRIMARY_PHONE">
         <#elseif contactList.contactMechTypeId == "EMAIL_ADDRESS">
         <#assign contactMechType = "PRIMARY_EMAIL">
         </#if>
         </#if>
         <input type="hidden" name="contactListId" value=${requestParameters.contactListId?if_exists}/>
         <input type='hidden' name='contactMechType' id='contactMechType' value='${contactMechType?if_exists}'/>
         <input type="hidden" name="fileFormat" value="TEXT"/>
         <input type='hidden' name="roleType" id="roleType" value="${roleTypeIdVal?if_exists}"/>
         <div class="row padding-r mt-2">
            <div class="col-md-6 col-sm-6">
            <#if isValidate?has_content && isValidate=="Y">
              <#if !roleTypeIdVal?exists>
              <div class="form-group row has-error">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.roleType!}</label>
                  <div class="col-sm-7">
                   <@simpleDropdownInputZone 
				    id="roleTypeId"
				    options=roleList?if_exists
				    required=true
				    value=roleType?if_exists
				    allowEmpty=true
				    emptyText = uiLabelMap.roleType
				    />	
                  </div>
               </div>
               <#else>
               <div class="form-group row has-error">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.roleType!}</label>
                  <div class="col-sm-7">
                     <select class="form-control input-sm" name="roleTypeId" id="roleTypeId" disabled>
		             <option value="LEAD" <#if roleTypeIdVal?has_content && roleTypeIdVal=="LEAD">selected</#if>>Lead</option>
		             <option value="ACCOUNT" <#if roleTypeIdVal?has_content && roleTypeIdVal=="ACCOUNT">selected</#if>> Account</option>
		             <!--<option value="CONTACT" <#if roleTypeIdVal?has_content && roleTypeIdVal=="CONTACT">selected</#if>>Contact</option>-->
	               </select>
                  </div>
               </div>
               </#if>
               </#if>
               <div class="form-group row has-error">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.fileToImport!}</label>
                  <div class="col-sm-7">
                     <input id="uploadedFile" name="uploadedFile" data-error="Please select Text file to Upload" type="file" size="30" maxlength="" class="form-control" onchange="" required>
                  </div>
                  
               </div>
               <div class="form-group row has-error">
                  <label  class="col-sm-4 col-form-label"></label>
                  <div class="col-sm-7" >
                     <div class="help-block with-errors" id="errorFileId"></div>
                  </div>
               </div>
               <div class="form-group row">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.formatTemplate!}</label>
                  <div class="col-sm-7">
                     <select class="form-control input-sm disabled" name="extension" id="extension">
                        <option value="txt">Text</option>
                        <option value="csv">CSV</option>
                        <!--<option value="xlsx">Excel</option>-->
                     </select>
                  </div>
               </div>
               <div class="form-group row">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.textFormatTemplate!}</label>
                  <div class="col-sm-7">
                     <#--<a href="downloadFile?resourceName=campaign-resource&componentName=campaign&fileName=Classification_Parties_Import_File_Format.txt" target="_blank" id="downloadFile" class="btn btn-xs btn-primary ml-0">${uiLabelMap.download!}</a>-->
                     <a id="file-download-btn" href="/campaign-resource/template/Classification_Parties_Import_File_Format.txt" class="btn btn-xs btn-primary ml-0" title="" download="">
                        ${uiLabelMap.download!}
                     </a>
                     <#-- <a href="/campaign-resource/template/Classification_Parties_Import_File_Format.txt" target="_blank" id="downloadFile" class="btn btn-xs btn-primary ml-0" >${uiLabelMap.download!}</a>-->
                     <#if isCreated?exists && isCreated>
                     <a href="processSmartListFile?contactListId=${contactList.contactListId?if_exists}&contactMechType=${contactMechType?if_exists}&roleTypeId=${roleTypeIdVal?if_exists}" class="btn btn-xs btn-primary ml-2">Process</a>
                     </#if>
                  </div>
               </div>
               <div class="form-group row">
                  <label  class="col-sm-4 col-form-label"></label>
                  <div class="col-sm-7">
                  <#if isCreated?exists && !isCreated>
                     <button type="submit" id="upload" name="upload" class="btn btn-xs btn-primary ml-0">${uiLabelMap.upload!}</button>
                  </#if>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"> </div>
      </form>
   </div></#if>
   <div id="tab2" class="${classtab2}">
      <div class="row padding-r">
         <div class="col-md-12 col-sm-12 form-horizontal">
            <div class="form-group row">
               <label  class="col-sm-2 col-form-label">${uiLabelMap.totalCount!}</label>
               <div class="col-sm-4">
                  <label class="col-form-label input-sm fw">${totalNoOfParties?if_exists}</label>
               </div>
            </div>
            <div class="form-group row">
               <label  class="col-sm-2 col-form-label">${uiLabelMap.successCount!}</label>
               <div class="col-sm-2">
                  <label class="col-form-label input-sm fw">Account :<#if roleTypeIdVal?has_content && roleTypeIdVal="LEAD"> ${numberOfMembers?if_exists} <#else> ${accountInListCount?if_exists}</#if></label>
               </div>
               <div class="col-sm-2">
                  <label class="col-form-label input-sm fw">Contact : ${numberOfMembers?if_exists}</label>
               </div>
               </div>
            <div class="form-group row">
               <label  class="col-sm-2 col-form-label">${uiLabelMap.errorCount!}</label>
               <div class="col-sm-4">
                  <label class="col-form-label input-sm fw">${totalNoOfError?if_exists}</label>
                 <span class="btn btn-xs btn-primary ml-4" data-toggle="modal" href="#ErrorLog">View</span>
               </div>
            </div>
            <div class="form-group row">
               <label  class="col-sm-2 col-form-label">Dedup</label>
               <div class="col-sm-4">
                  <label class="col-form-label input-sm fw"><#if totaldedupCount?has_content && totaldedupCount?exists>${totaldedupCount?if_exists}<#else>0</#if></label>
                 <span class="btn btn-xs btn-primary ml-4" data-toggle="modal" href="#dedup">View</span>
               </div>
            </div>
         </div>
      </div>
   </div>
 <div id="tab3" class="tab-pane fade show ">
    <#assign campaignList = delegator.findByAnd("MarketingCampaignContactList", {"contactListId" : requestParameters.contactListId?if_exists},[],false)?if_exists/>
  <div class="page-header">
     <h2 class="float-left">Campaign List  </h2>
  </div>
      <div class="table-responsive">
     <table  id="dtableGrid" class="table table-striped">
        <thead>
           <tr>
              <th>Campaign Name</th>
              <th>Contact Purpose Type</th>
        </thead>
        <tbody>
           <#if campaignList?has_content>
            <#list campaignList as list>
           <#assign campaignListN =delegator.findOne("MarketingCampaign", {"marketingCampaignId" : list?if_exists.marketingCampaignId?if_exists}, false)?if_exists/>
           <tr>
            <td>${campaignListN?if_exists.campaignName?if_exists}</td>
			<td>
			<#if list?if_exists.contactPurposeType?has_content && list?if_exists.contactPurposeType?exists>
            <#if list?if_exists.contactPurposeType?if_exists == "TEST">
            ${uiLabelMap.testList!}
            <#elseif list?if_exists.contactPurposeType?if_exists == "LIVE">
            ${uiLabelMap.productionList!}
            </#if>
            </#if>
			   </td>
              </tr>
              </#list>
             </#if>
        </tbody>
     </table>
</div>
</div>
<div class="clearfix"> </div>


<!-- Error list Module-->
<div id="ErrorLog" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header">
            <h4 class="modal-title">Contact List Error Log</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button> 
         </div>
         <div class="modal-body">
		<div class="card-header">
		<form method="post" action="findErrorLog" id="findContact" class="form-horizontal" name="findContact" novalidate="novalidate" data-toggle="validator">
          <div class="row"> 
          <#-- <div class="col-md-2 col-sm-2"> 
			 <div class="form-group row mr">
			 <input type="text" class="form-control input-sm" name="partyId" id="partyId" placeholder="CustomerId">
          </div>
          </div>
		  <div class="col-md-2 col-sm-2">
		  <input type="submit" class="btn btn-sm btn-primary " value="Find"/>
          <#--<button type="submit" class="btn btn-sm btn-primary m5">Find Contacts </button>-->
          <#--</div> -->
          </div>
	  </form>
	  <div class="clearfix"> </div>
	  </div>
          <div class="table-responsive">
              <table id="dtable" class="table table-striped tbq">
                <thead>
                  <tr>
                    <th>Log Id</th>
                    <th>${uiLabelMap.contactListName!}</th>
                    <th>${uiLabelMap.customerId!}</th>
                    <th>${uiLabelMap.RM!}</th>
                    <th>${uiLabelMap.errorCode!}</th>
                    <th>${uiLabelMap.errorDescription!}</th>
                  </tr>
                </thead>
                <tbody>
               <#if errorLog?exists>
                <#list errorLog as list>
               <#assign errorCodeList =delegator.findOne("ErrorCode", {"errorCodeId" : list.errorLog?if_exists}, false)?if_exists/>
                <#if errorCodeList?has_content>
                   <#if errorCodeList.errorCodeId?if_exists != 'E140'>
                  <tr>
                   <td>${list.ErrorLogId?if_exists}</td>
                   <#assign contactListN =delegator.findOne("ContactList", {"contactListId" : list.contactListId?if_exists}, false)?if_exists/>
                   <td><#if contactListN?has_content> ${contactListN.contactListName}</#if></td>
				   <td>${list.partyId?if_exists}</td>
				   <td>${list.csrPartyId?if_exists}</td>
				   <#assign errorCodeList =delegator.findOne("ErrorCode", {"errorCodeId" : list.errorLog?if_exists}, false)?if_exists/>
				   <td><#if errorCodeList?has_content>${errorCodeList?if_exists.errorCodeId?if_exists}</#if></td>
				   <td><#if errorCodeList?has_content>${errorCodeList?if_exists.codeDescription?if_exists}</#if></td>
                  </tr> 
                       </#if>
                    </#if>
                  </#list>
                  </#if>
                </tbody>
              </table>
            </div>
         </div>
         <div class="modal-footer">
      	 <a href="<@ofbizUrl>errotCountExport?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-sm btn-primary navbar-dark" id="errotCountExport">${uiLabelMap.download}</a>

           <#-- <button type="button" id="export" class="btn btn-sm btn-primary navbar-dark" >Download</button> -->
         </div>
      </div>
   </div>
</div>


<div id="dedup" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header">
            <h4 class="modal-title">Deduped Contact List Log</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button> 
         </div>
         <div class="modal-body">
		<div class="card-header">
		<form method="post" action="findErrorLog" id="findContact" class="form-horizontal" name="findContact" novalidate="novalidate" data-toggle="validator">
          <div class="row"> 
          <#-- <div class="col-md-2 col-sm-2"> 
			 <div class="form-group row mr">
			 <input type="text" class="form-control input-sm" name="partyId" id="partyId" placeholder="CustomerId">
          </div>
          </div>
		  <div class="col-md-2 col-sm-2">
		  <input type="submit" class="btn btn-sm btn-primary " value="Find"/>
          <#--<button type="submit" class="btn btn-sm btn-primary m5">Find Contacts </button>-->
          <#--</div> -->
          </div>
	  </form>
	  <div class="clearfix"> </div>
	  </div>
          <div class="table-responsive">
              <table id="dtableError" class="table table-striped tbd">
                <thead>
                  <tr>
                    <th>Log Id</th>
                    <th>${uiLabelMap.contactListName!}</th>
                    <th>${uiLabelMap.customerId!}</th>
                    <th>${uiLabelMap.errorCode!}</th>
                    <th>${uiLabelMap.errorDescription!}</th>
                  </tr>
                </thead>
                <tbody>
               <#if errorLog?exists>
                <#list errorLog as list>
                <#assign errorCodeList =delegator.findOne("ErrorCode", {"errorCodeId" : list.errorLog?if_exists}, false)?if_exists/>
                <#if errorCodeList?has_content>
                   <#if errorCodeList.errorCodeId?if_exists == 'E140'>
                <tr>
                   <td>${list.ErrorLogId?if_exists}</td>
                   <#assign contactListN =delegator.findOne("ContactList", {"contactListId" : list.contactListId?if_exists}, false)?if_exists/>
                   <td><#if contactListN?has_content> ${contactListN.contactListName}</#if></td>
				   <td>${list.partyId?if_exists}</td>
				   
				   <td><#if errorCodeList?has_content>${errorCodeList?if_exists.errorCodeId?if_exists}</#if></td>
				   <td><#if errorCodeList?has_content>${errorCodeList?if_exists.codeDescription?if_exists}</#if></td>
                  </tr>
                      </#if>
                    </#if>
                  </#list>
                  </#if>
                </tbody>
              </table>
            </div>
         </div>
         <div class="modal-footer">
            <a href="<@ofbizUrl>dedupCountExport?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-sm btn-primary navbar-dark" id="errotCountExport">${uiLabelMap.download}</a>
             
             <#--<button type="button" id="exportd" class="btn btn-sm btn-primary navbar-dark" >Download</button>-->
         </div>
      </div>
   </div>
</div>
<script>
$("#dtableGrid").DataTable();
</script>
<script src="<@ofbizContentUrl>/campaign-resource/template/csvExport.js</@ofbizContentUrl>" type="text/javascript"></script>	
<script>
//$( "#export" ).click(function() {
 // $('.tbq').csvExport();
//});
</script>
<script>
//$( "#exportd" ).click(function() {
 // $('.tbd').csvExport();
//});
</script>

<script>
$("#roleTypeId").change(function() {
    var role=$(this).val();
 	$("#roleType").val(role);
 });
</script>
<script>
   $(function(){
       $(document).ready(function(){
           $("#dtableError").DataTable({
               "order": [[ 0, "desc" ]]
           });
       });
   });
</script>
<script>
$("#extension").change(function(){
   var ext=$(this).val();
   if("txt"==ext){
      //$("#downloadFile").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.txt");
      //$("#downloadFile").attr("href", "downloadFile?resourceName=campaign-resource&componentName=campaign&fileNameClassification_Parties_Import_File_Format.txt");
      $("#file-download-btn").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.txt");
   }else if("csv"==ext){
      //$("#downloadFile").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.csv");
      //$("#downloadFile").attr("href", "downloadFile?resourceName=campaign-resource&componentName=campaign&fileName=Classification_Parties_Import_File_Format.csv");
      $("#file-download-btn").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.csv");
   }else if("xlsx"==ext){
      //$("#downloadFile").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.xlsx");
      $("#file-download-btn").attr("href", "/campaign-resource/template/Classification_Parties_Import_File_Format.xlsx");
   }
});
</script>
<script>
$("#uploadPartiesFile").submit(function(e){
   var ext=$("#extension").val();
   var fileName=$("#uploadedFile").val();
   var match = fileName.match(/(.+)\.(.+)/);
   var fileExt = match[2];
    $("#errorFileId").empty();
    if("txt"==ext && ext!=fileExt){
        $("#errorFileId").append("Please choose TEXT file");
        e.preventDefault();
    }else if("csv"==ext && ext!=fileExt){
        $("#errorFileId").append("Please choose CSV file");
        e.preventDefault();
    }else if("xlsx"==ext && ext!=fileExt){
        $("#errorFileId").append("Please choose xlsx file");
        e.preventDefault();
    }
   
   
});
 
</script>