<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header border-b">
   <h1 class="float-left">${uiLabelMap.viewContactList!}
    - <#if contactList?has_content && contactList?exists>${contactList.contactListName?if_exists}</#if></h1>
   <p class="float-right">
      <#-- <a href="<@ofbizUrl>updateContactListForm?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Edit</a> -->
      <a href="<@ofbizUrl>findContactList</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Back</a>
      <a href="<@ofbizUrl>findContactListMember?contactListId=${requestParameters.contactListId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">${uiLabelMap.findContactMembers!}</a>
   </p>
</div>
<div class="row padding-r">
   <div class="col-md-6 col-sm-6 form-horizontal">
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.name!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactList.contactListName?if_exists}</#if></label>
         </div>
      </div>
      <#if contactList?has_content>
      <#if contactList?if_exists.contactMechTypeId?if_exists == "TELECOM_NUMBER">		
      <#assign contactDescription = "Phone Number">
      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "EMAIL_ADDRESS">
      <#assign contactDescription = "Email Address">
      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "POSTAL_ADDRESS">
      <#assign contactDescription = "Postal Address">
      </#if>
      </#if>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.contactType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactDescription?if_exists}</#if></label>
         </div>
      </div>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.description!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if contactList?has_content && contactList?exists>${contactList.description?if_exists}</#if></label>
         </div>
      </div>
      
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.contactPurposeType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">
            <#if contactList?has_content && contactList?exists>
	            <#if contactList.contactPurposeTypeId?if_exists == "TEST">
	            ${uiLabelMap.testList!}
	            <#elseif contactList.contactPurposeTypeId?if_exists == "LIVE">
	            ${uiLabelMap.productionList!}
	            </#if>
            </#if>
            </label>
         </div>
      </div>
      	
      <#if contactList?has_content>
	      <#if contactList.emailType?if_exists == "SENSITIVE">		
	      	<#assign emailType = "Sensitive">
	      <#elseif contactList.emailType?if_exists == "NONSENSITIVE">
	      	<#assign emailType = "Non Sensitive">
	      </#if>
      </#if>
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.emailType!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw"><#if emailType?exists>${emailType?if_exists}</#if></label>
         </div>
      </div>
      
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">Created Date</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">${contactList.createdStamp?string["dd-MM-yyyy"]?if_exists}</label>
         </div>
      </div>
      
      <div class="form-group row">
         <label  class="col-sm-4 col-form-label">${uiLabelMap.importedStatus!}</label>
         <div class="col-sm-7">
            <label class="col-form-label input-sm fw">This list created by smart list template</label>
         </div>
      </div>
      
   </div>
</div>

   


<script>
$("#roleTypeId").change(function() {
    var role=$(this).val();
 	$("#roleType").val(role);
 });
</script>
<script>
   $(function(){
       $(document).ready(function(){
           $("#dtableError").DataTable({
               "order": [[ 0, "desc" ]]
           });
       });
   });
</script>