        <div class="page-header border-b">
          <h1 class="float-left">Find List Member - ${contactListName} (${contactId?if_exists})</h1>
		  <div class="float-right">
		  <#if isAssociated?exists && !isAssociated>
		  <a href="<@ofbizUrl>addNewMember?contactListId=${contactId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Add New Contacts</a>
		  </#if>
		  <a href="<@ofbizUrl>viewContactList?contactListId=${contactId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Back</a>
		  </div>
		  </div>
		<div class="card-header">
          <form method="post" action="<@ofbizUrl>findContactListMember?contactListId=${contactId?if_exists}</@ofbizUrl>" id="" class="form-horizontal" name="createLeadForm" novalidate="novalidate" data-toggle="validator">
            <input type="hidden" name="contactListId" value="${contactId?if_exists}">
			<div class="row padding-r">
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="customerId" id="customerId" placeholder="Customer ID" value="${customerId?if_exists}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="firstName" id="firstName" placeholder="First Name" value="${firstName?if_exists}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="lastName" id="lastName" placeholder="Last Name" value="${lastName?if_exists}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
            <button type="submit" class="btn btn-sm btn-primary mr-4" >Find</button>
            </div>
			</div>
          </form>
          <div class="clearfix"> </div>
        </div>        
        <div class="clearfix"> </div>
            <div class="page-header">
              <h2 class="float-left">Members List </h2>
            </div>
            <div class="table-responsive">
              <table id="dtable" class="table table-striped">
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Email Solicitation</th>
                    <th>Phone Solicitation</th>
                  </tr>
                </thead>
                <tbody>
                <#if contactListMemberData?has_content>
                <#list contactListMemberData as list>
                  <tr>
                    <td>${list?if_exists.firstName?if_exists} ${list?if_exists.lastName?if_exists} (${list?if_exists.partyId?if_exists})</td>
                    <td>${list?if_exists.infoString?if_exists}</td>
                    <td>${list?if_exists.contactNumber?if_exists}</td>
                    <td>${list?if_exists.emailSolicitation?if_exists}</td>
                    <td>${list?if_exists.phoneSolicitation?if_exists}</td>
                    
                  </tr>
                  </#list>
                  </#if>
                </tbody>
              </table>
            </div>
      </div>
    </div>
<script>
$('#dtable').DataTable( {
	"processing": true,
	"destroy": true
});
</script>