<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="float-right">
   <button class="btn btn-xs btn-primary" title="Create" data-toggle="modal" data-target="#addAMRMToCampaign">Add AM/RM To Campaign</button>
</div>
<div class="page-header">
   <h2 class="float-left" >Assign AM/RM To Campaign</h2>
</div>
<div id="addAMRMToCampaign" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Find RM</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="card-header">
               <form method="post" class="form-horizontal" data-toggle="validator">
                  <input type="hidden" name="partyId" id="rmAMPartyId"/>
                  <input type="hidden" name="activeTab" value="assignRM" />
                  <div class="row">
                     <div class="col-md-2 col-sm-2">
                        <@simpleInput 
                        id="firstNameRMAM"
                        name="firstName"
                        placeholder=uiLabelMap.firstName
                        required=false
                        />
                     </div>
                     <div class="col-md-2 col-sm-2">
                        <@simpleInput 
                        id="lastNameRMAM"
                        name="lastName"
                        placeholder=uiLabelMap.lastName
                        required=false
                        />
                     </div>
                     <div class="col-md-2 col-sm-2">
                        <@simpleDropdownInput 
                        id="roleTypeId"
                        options=rmMap
                        required=false
                        value=""
                        allowEmpty=true
                        emptyText = "Role Type"
                        dataLiveSearch=true
                        />
                     </div>
                     <div class="col-md-1 col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary navbar-dark m5" onclick="javascript:getAccountManagerOrRM();">Find</button>
                     </div>
                  </div>
               </form>
               <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">RM Members</h2>
            </div>
            <div class="table-responsive">
               <div class="float-right">
                  <button type="reset" class="btn btn-xs btn-primary" id="assignAMOrRMButton">Add To Campaign</button>
               </div>
               <table id="ajaxFindAssignRMdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>RM Id</th>
                        <th>RM Name</th>
                        <th><input type="checkbox" id="assignAMOrRM" name="assignAMOrRM"/></th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="sbmit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<form method="post" action="addCampaignCsrParties" name="addCampaignCsrParties" id="addCampaignCsrParties">
   <input type="hidden" name="activeTab" value="assignRM" />
   <input type="hidden" name="approvePage" id="approvePage" value="viewMarketingCampaign"/>
   <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
</form>
<form name="removeCsrCampagin" id="removeCsrCampagin" action="removeCsrCampagin">
    <input type="hidden" name="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
    <input type="hidden" name="partyId" id="partyId"/>
</form>
<div class="table-responsive">
   <table id="listOfCampaignCsrAssoc" class="table table-striped">
      <thead>
         <tr>
            <th>AM/RM Name</th>
            <th>Remove</th>
         </tr>
      </thead>
      <tbody>
         <#if campaignCsrAssocListRMToCamp?exists && campaignCsrAssocListRMToCamp?has_content>
         <#list campaignCsrAssocListRMToCamp as campaignCsrAssoc>
         <tr>
           <td>
           <#assign person = delegator.findOne("Person", {"partyId" : campaignCsrAssoc.csrPartyId}, false)?if_exists/>
           <#if person?exists && person?has_content>${person.firstName?if_exists} ${person.lastName?if_exists} (${person.partyId?if_exists})</#if>
           </td>
           <td>
           <a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="javascript:validateRemoveCSR('${campaignCsrAssoc.csrPartyId?if_exists}')" data-original-title="Delete"><i class="fa fa-times red"></i></a>
           </td>
         </tr>
          </#list>
          </#if>
      </tbody>
   </table>
</div>
<script>
    $(document).ready(function () { 
       getAccountManagerOrRM();
       $('#listOfCampaignCsrAssoc').dataTable();
    });
    function validateRemoveCSR(partyId){
        if(partyId != null && partyId != "") {
           document.removeCsrCampagin.partyId.value = partyId;
           document.removeCsrCampagin.submit();
        }
    }
    $("#assignAMOrRM").change(function(){  
          var status = this.checked;
          if (status) {
             $('input[name="assignAMOrRMCheckbox"]').each(function(){ 
                this.checked = status; 
             });
          } else {
             $('input[name="assignAMOrRMCheckbox"]').each(function(){ 
                this.checked = false; 
             });
          }
    });
    
    $("#assignAMOrRMButton").click(function () {
          var form = document.getElementById("addCampaignCsrParties");
          if($('.assignAMOrRMCheckbox').is(':checked')){
             $('.assignAMOrRMCheckbox:checked').each(function() {
               $(form).append($('<input>').attr('type', 'hidden').attr('name', 'partyId').val($(this).val()));
             });
             form.submit();
           } else {
              $.notify({
                message : '<p>Please select atleast one record in the list</p>',
              });
           }
    });
    function getAccountManagerOrRM() {
        var firstName = $("#firstNameRMAM").val();
        var lastName = $("#lastNameRMAM").val();
        var roleTypeId = $("#roleTypeId").val();
        var url = "getTeamMembers?firstName=" + firstName + "&lastName=" + lastName + "&roleTypeId=" + roleTypeId;
        $('#ajaxFindAssignRMdatatable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": url,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [
               {"data": "partyId"},
               {"data": "name"},
               {
                    "data": "partyId",
                    "render": function(data, type, row, meta) {
                        data ='<input type="checkbox" class="assignAMOrRMCheckbox" id="assignAMOrRMCheckbox" name="assignAMOrRMCheckbox" value="'+row.partyId+'"/>';
                        return data;
                    }
                }
            ]
        });
    }
</script>