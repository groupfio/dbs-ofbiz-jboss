<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#if availableCampaign && generatePhoneCallList && contactListIdAL?exists && contactListIdAL?has_content>
<#assign campaignContactListParty = delegator.findByAnd("CampaignContactListParty", {"contactListId" : contactListIdAL}, [], false)>
<#if campaignContactListParty?exists && campaignContactListParty?has_content>
<div class="float-right">
<#assign callRecordMaster = delegator.findByAnd("CallRecordMaster", {"marketingCampaignId" : marketingCampaignId?if_exists}, [], false)>
<a href="<@ofbizUrl>loadCsr?marketingCampaignId=${marketingCampaignId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary  <#if callRecordMaster?exists && callRecordMaster?has_content>disabled</#if>" role="button">Generate Call List</a>
</div>
</#if>
</#if>
<#assign enableCampaignApproveList = "N"/>
<#if enableCampaignApprove?exists && enableCampaignApprove?has_content && enableCampaignApprove=="Y">
<#assign enableCampaignApproveList = "Y"/>
</#if>
<#if enableCampaignApproveList=="Y">
<div class="page-header">
   <h2 class="float-left" >Outbound Call List</h2>
</div>
<#else>
<div class="page-header border-b">
   <h1 class="float-left" >Outbound Call List</h1>
</div>
</#if>
<div class="card-header">
   <form method="post" action="createOrUpdateOutboundCallList" id="outboundCallListForm" class="form-horizontal" name="outboundCallListForm" novalidate="true" data-toggle="validator">
      <input type="hidden" name="activeTab" value="callList" />
      <input type="hidden" name="approvePage" id="approvePage" value="viewMarketingCampaign"/>
      <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
      <div class="row">
         <#assign disabled = false/>
         <#assign campaignId = "${campaignIdOCL?if_exists}"/>
         <#if marketingCampaignId?exists && marketingCampaignId?has_content>
         <#assign disabled = true/>
         <#assign campaignId = "${marketingCampaignId}"/>
         </#if>
         <@dropdownInputForRow 
         id="campaignIdOCL"
         name="campaignId"
         options=marketingCampaignListOCL
         value="${campaignId?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.campaign
         label=uiLabelMap.campaign
         dataLiveSearch=true
         disabled=disabled
         />
         <@dropdownInputForRow 
         id="rmUserLoginIdOCL"
         name="rmUserLoginId"
         options=responsibleForOCL
         value="${rmUserLoginIdOCL?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.RM
         label=uiLabelMap.RM
         dataLiveSearch=true
         />
         <@dropdownInputForRow 
         id="callStatusOCL"
         name="callStatus"
         options=callStatusListOCL
         value="${callStatusOCL?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.status
         label="Call Status"
         dataLiveSearch=true
         />
         <@dropdownInputForRow 
         id="noOfDaysSinceLastCallOCL"
         name="noOfDaysSinceLastCall"
         options=numberCountOCL
         value="${noOfDaysSinceLastCallOCL?if_exists}"
         allowEmpty=true
         tooltip = uiLabelMap.status
         label="# Days Since Last Call"
         dataLiveSearch=true
         />
         <div class="col-md-2 col-sm-2">
            <div class="form-group">
               <label class="col-form-label">Call Back Date</label>
               <div class="input-group date" id="datetimepicker11">
                  <input type='text' class="form-control input-sm" name="callBackDate" id="callBackDateOCL" data-date-format="DD-MM-YYYY" value="${callBackDateOCL?if_exists}" placeholder="DD-MM-YYYY" />
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
               </div>
            </div>
         </div>
      <div class="col-md-2 col-sm-2">
         <input type="submit" value="Clear" onclick="clearRecords();" class="btn btn-sm btn-primary tooltips" style="margin-top:28px;"/>
         <input type="submit" value="Save" class="btn btn-sm btn-primary tooltips" style="margin-top:28px;"/>
      </div>
      </div>
   </form>
</div>
<div class="clearfix"> </div>
<div class="table-responsive">
   <div class="float-right">
      <a name="rmOrAMReassignHref" href="#" id="rmOrAMReassignHref">
      <button type="reset" class="btn btn-xs btn-primary"  id="rmOrAMReassign" data-target="#updateRMOrAM">AM/RM Reassign</button>
      </a>
   </div>
   <table id="outboundCallListTable" class="table table-striped">
      <thead>
         <tr>
            <th>${uiLabelMap.campaign}</th>
            <th>${uiLabelMap.Start}</th>
            <th>${uiLabelMap.End}</th>
            <th>${uiLabelMap.contactName}</th>
            <th>${uiLabelMap.accountName}</th>
            <th>${uiLabelMap.phone}</th>
            <th>${uiLabelMap.city}</th>
            <th>${uiLabelMap.State}</th>
            <th>${uiLabelMap.RM}</th>
            <th>${uiLabelMap.status}</th>
            <th># Days Since Last Call</th>
            <th>Call Back Date</th>
            <th>Imp Note</th>
            <#if csrValidation?exists && csrValidation?has_content && csrValidation == "Y">
            <th>AM/RM Reassign <input type="checkbox" id="rmSelectAll" name="rmSelectAll" value="1" /></th>
            </#if>
         </tr>
      </thead>
   </table>
</div>
<div id="impNoteAppend" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Important Note</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>Note Info</th>
                        <th>Note Date</th>
                        <th>Created By</th>
                     </tr>
                  </thead>
                  <tbody id="importantNoteTbody">
                  </tbody>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="sbmit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<div id="updateRMOrAM" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">RM/Account Manager Reassign</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form name="rmRessignUpdate" action="<@ofbizUrl>rmPartyReassign</@ofbizUrl>"  method="post">
               <input type="hidden" id="partyId" name="partyId" value=""/>
               <input type="hidden" id="campaignListIds" name="campaignId" value=""/>
               <input type="hidden" name="partyLists" id="partyLists" />
               <input type="hidden" name="approvePage" id="approvePage" value="viewMarketingCampaign"/>
               <input type="hidden" name="activeTab" value="callList" />
               <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${marketingCampaignId?if_exists}"/>
               <@dropdownInput 
               id="toRM"
               name="toRM"
               label="RM/Account Manager"
               options=rmMap
               required=true
               allowEmpty=true
               dataLiveSearch=true
               />
               <div class="col-md-12 col-sm-12">
                  <div class="form-group row">
                     <div class="offset-sm-4 col-sm-9">
                        <input type="submit" class="btn btn-sm btn-primary navbar-dark mt" value="${uiLabelMap.submit}"/>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="sbmit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>
<script>
    var oTable = null;
    $(document).ready(function() {
        var urlString = "getOutBoundCallListUrl";
        var campaignId = $("#campaignIdOCL").val();
        var csrPartyId = $("#rmUserLoginIdOCL").val();
        var csrPartyList = "${csrListPartyId?if_exists}";
        var callStatus = $("#callStatusOCL").val();
        var noOfDaysSinceLastCall = $("#noOfDaysSinceLastCallOCL").val();
        var callBackDate = $("#callBackDateOCL").val();
        var outboundCallListUrl = "getOutBoundCallList?campaignId=" + campaignId + "&csrPartyId=" + csrPartyId + "&callStatus=" + callStatus + "&noOfDaysSinceLastCall=" + noOfDaysSinceLastCall + "&callBackDate=" + callBackDate + "&csrPartyList=" + csrPartyList;
        loadOutboundCallList(outboundCallListUrl);
    });

    $("#campaignIdOCL, #rmUserLoginIdOCL, #callStatusOCL, #noOfDaysSinceLastCallOCL, #callBackDateOCL").change(function() {
        var campaignId = $("#campaignIdOCL").val();
        var csrPartyId = $("#rmUserLoginIdOCL").val();
        var callStatus = $("#callStatusOCL").val();
        var noOfDaysSinceLastCall = $("#noOfDaysSinceLastCallOCL").val();
        var callBackDate = $("#callBackDateOCL").val();
        var csrPartyList = "${csrListPartyId?if_exists}";
        var outboundCallListUrl = "getOutBoundCallList?campaignId=" + campaignId + "&csrPartyId=" + csrPartyId + "&callStatus=" + callStatus + "&noOfDaysSinceLastCall=" + noOfDaysSinceLastCall + "&callBackDate=" + callBackDate + "&csrPartyList=" + csrPartyList;
        loadOutboundCallList(outboundCallListUrl);
    });

    $("#callBackDateOCL").focus(function() {}).blur(function() {
        $('#callBackDateOCL').change();
    });

    function clearRecords() {
        $("#campaignIdOCL").val("");
        $("#rmUserLoginIdOCL").val("");
        $("#callStatusOCL").val("");
        $("#noOfDaysSinceLastCallOCL").val("");
        $("#callBackDateOCL").val("");
    }

    function loadOutboundCallList(outboundCallListUrl) {
        oTable = $('#outboundCallListTable').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "ajax": {
                "url": outboundCallListUrl,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },

            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                    "data": "marketingCampaignId",
                    "render": function(data, type, row, meta) {
                        data = row.campaignName + ' (' + data + ')';
                        return data;
                    }
                },
                {
                    "data": "startDate"
                },
                {
                    "data": "endDate"
                },
                {
                    "data": "partyId",
                    "render": function(data, type, row, meta) {
                        var partyStatusId = row.partyStatusId;
                        if (partyStatusId != null && partyStatusId != "" && partyStatusId == "CONTACT") {
                            data = '<a href="/crm/control/viewContact?partyId=' + row.partyId + '&campaignListId=' + row.campaignListId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.contactName + ' (' + data + ')</a>';
                        } else if (partyStatusId != null && partyStatusId != "" && partyStatusId == "LEAD") {
                            data = '<a href="/crm/control/viewLead?partyId=' + row.partyId + '&campaignListId=' + row.campaignListId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.contactName + ' (' + data + ')</a>';
                        } else {
                            data = row.contactName + ' (' + data + ')';
                        }
                        return data;
                    }
                },
                {
                    "data": "accountId",
                    "render": function(data, type, row, meta) {
                        var accountId = row.accountId;
                        if (accountId != null && accountId != "" && accountId != undefined) {
                            data = '<a href="/crm/control/viewAccount?partyId=' + row.accountId + '&externalLoginKey=${requestAttributes.externalLoginKey!}" target="_blank">' + row.accountName + '(' + data + ')</a>';
                        }
                        return data;
                    }
                },
                {
                    "data": "contactNumber"
                },
                {
                    "data": "city"
                },
                {
                    "data": "state"
                },
                {
                    "data": "rmId",
                    "render": function(data, type, row, meta) {
                        data = row.rmName + ' (' + data + ')';
                        return data;
                    }
                },
                {
                    "data": "callStatus"
                },
                {
                    "data": "noOfDateSinceLastCall",
                    "render": function(data, type, row, meta) {
                        var difDays = row.diffDays;
                        var noOfDateSinceLastCall = "";
                        if (difDays != null && difDays != "") {
                            noOfDateSinceLastCall = row.diffDays + " - " + row.noOfDateSinceLastCall;
                        }
                        return noOfDateSinceLastCall;
                    }
                },
                {
                    "data": "callBackDate"
                },
                {
                    "data": "notes",
                    "render": function(data, type, row, meta) {
                        var noteValidation = row.noteValidation;
                        var notes = '';
                        if (noteValidation != null && noteValidation != "" && noteValidation == "Y") {
                            notes = '<a onclick=getNotes("' + row.partyId + '"); ><span class="fa fa-sticky-note btn btn-xs btn-danger tooltips" data-toggle="modal" data-target="#impNoteAppend" data-original-title="${uiLabelMap.impNote}"></span></a>';
                        }
                        return notes;
                    }
                },
                {
                    "data": "csr",
                    "render": function(data, type, row, meta) {
                        var csrValid = "${csrValidation?if_exists}";
                        var csr = '';
                        if (csrValid == "Y") {
                            csr = '<input type="checkbox" class="rmPartyIdCheckBox" id="rmPartyIdCheckBox" name="rmPartyIdCheckBox" value="' + row.partyId + '_' + row.marketingCampaignId + '"/>'
                        }
                        return csr;
                    }
                }
            ]
        });
    }

    function getNotes(partyId) {
        $('#importantNoteTbody').html('');
        var jsondata = '<tr>';
        var urlString = "getPartyNotes?partyId=" + partyId;
        $.ajax({
            type: 'POST',
            async: false,
            url: urlString,
            success: function(data) {
                for (var i = 0; i < data.partyNotes.length; i++) {
                    var noteInfo = "" + data.partyNotes[i].noteInfo + "";
                    var partyName = "" + data.partyNotes[i].partyName + "";
                    var noteDateTime = "" + data.partyNotes[i].noteDateTime + "";
                    jsondata = jsondata + '<td>' + noteInfo + '</td>';
                    jsondata = jsondata + '<td>' + noteDateTime + '</td>';
                    jsondata = jsondata + '<td>' + partyName + '</td>';
                }
            }
        });
        jsondata = jsondata + '</tr>';
        $(jsondata).appendTo("#importantNoteTbody");
    }

    $("#rmSelectAll").change(function() {
        var status = this.checked;
        if (status) {
            $('input[name="rmPartyIdCheckBox"]').each(function() {
                this.checked = status;
            });
        } else {
            $('input[name="rmPartyIdCheckBox"]').each(function() {
                this.checked = false;
            });
        }
    });

    $("#rmOrAMReassign").click(function() {
        var favorite = [];
        if ($('#outboundCallListTable input[type="checkbox"]').is(':checked')) {
            $.each($("input[name='rmPartyIdCheckBox']:checked"), function() {
                favorite.push($(this).val());
            });
            $('#partyLists').val(favorite.toString());
            $('#updateRMOrAM').modal('show');
        } else {
            $.notify({
                message: '<p>Please select atleast one customer to reassign RM/AM</p>',
            });
            $('#updateRMOrAM').modal('hide');
        }
    });
</script>