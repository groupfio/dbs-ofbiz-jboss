
<#if contactList?exists && contactList?has_content>
	<#assign isSmartList = contactList.isSmartList?if_exists />
	<#if isSmartList == "Y">
		<#include "component://campaign/webapp/campaign/list/viewSmartContactList.ftl"/>
	<#else>
		<#include "component://campaign/webapp/campaign/list/viewContactList.ftl"/>
	</#if>
	
</#if>
