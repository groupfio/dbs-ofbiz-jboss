<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#assign extra>
	<a href="<@ofbizUrl>smartListTemplateBuilder</@ofbizUrl>" class="btn btn-xs btn-primary">Create</a>
</#assign>
<@sectionHeader title="${uiLabelMap.findSmartListTemplate!}" extra=extra />
   <div class="card-header">
   	<form method="post" action="" id="findSmartListTemplate" class="form-horizontal" name="findSmartListTemplate" novalidate="novalidate" data-toggle="validator">
      <div class="row padding-r">
         <div class="col-md-2 col-sm-2">
            <#--<@simpleDropdownInput 
				id="smartListId"
				options=smartListTemplateList
				required=false
				value="${requestParameters.smartListId?if_exists}"
				allowEmpty=true
				emptyText = uiLabelMap.name
				filter=true
				dataLiveSearch = true
				/>-->
                <@simpleInput 
                id="smartListId"
                placeholder=uiLabelMap.name
                value=smartListId?if_exists
                required=false
                />
         </div>
         <@fromSimpleAction id="smartListTemplateList" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
      </div>
     </form>
   </div>


<div class="clearfix"> </div>
<div class="clearfix"> </div>
<@sectionTitle title="${uiLabelMap.smartListTemplateList!}" />
<div class="table-responsive">
   <table class="table table-striped" id="ajaxDataTable">
      <thead>
         <tr>
         	<th>Name</th>
            <th>Type</th>
            <th>Campaign Id</th>
            <th>isProcessed</th>
            <th class="text-right">Action</th>
         </tr>
      </thead>
   </table>
</div>
<!-- Loader -->
<div class="loader text-center" id="loader" sytle="display:none;">
  <span></span>
  <span></span>
  <span></span>
</div>
<script type="text/javascript">
$(function() {
	findSmartListTemplate();
});

/*$('#smartListId').change(function(){
	findSmartListTemplate();
});*/
$('#smartListTemplateList').click(function(){
	findSmartListTemplate();
});
function findSmartListTemplate() {
    var name = $("#smartListId").val();
    $("#loader").show();
    var url = "<@ofbizUrl>getSmartListTemplateBuilderList</@ofbizUrl>";
    $('#ajaxDataTable').DataTable({
        //"processing": true,
        "serverSide": true,
        "destroy": true,
        "filter": false,
        "fnDrawCallback": function(oSettings) {
            resetDefaultEvents();
        },
        "ajax": {
            "url": url,
            "type": "POST",
            'data': {
                "smartListName": name
            },
        },
        "Paginate": true,
        "language": {
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "zeroRecords": "No matching records found",
            "oPaginate": {
                "sNext": "Next",
                "sPrevious": "Previous"
            }
        },
        "ordering": false,
        "pageLength": 10,
        "bAutoWidth": false,
        "stateSave": true,
        "columns": [{
                "data": "name",
                "render": function(data, type, row, meta) {
                    if (type === 'display') {
                        var smartListId = row.smartListId;
                        var type = row.type;
                        var campaignId = row.marketingCampaignId;
                        if("CAMPAIGN_TYPE" == type){
                        	data = '<div class=""><a href="viewMarketingCampaign?smartListId='+ row.smartListId +'&marketingCampaignId='+row.marketingCampaignId+'&activeTab=smartList" id="edit_' + row.smartListId + '" >' + row.name + '(' + row.smartListId + ')</a></div>';
                        }else{
                        	data = '<div class=""><a href="smartListTemplateBuilder?smartListId=' + row.smartListId + '" id="edit_' + row.smartListId + '" >' + row.name + '(' + row.smartListId + ')</a></div>';
                        }
                    }
                    return data;
                }
            },
            {
                "data": "type",
                "render": function(data, type, row, meta) {
                    if (type === 'display') {
                        if ("TEMPLATE_TYPE" == data) {
                            data = "Template Type";
                        } else if ("CAMPAIGN_TYPE" == data) {
                            data = "Campaign Type";
                        }
                    }
                    return data;
                }
            },
            {
                "data": "marketingCampaignId"
            },
            {
                "data": "isProcessed",
                "render": function(data, type, row, meta) {
                    if (type === 'display') {
                        if ("N" == data || null == data || "" == data) {
                            data = "No";
                        } else if ("Y" == data) {
                            data = "Yes";
                        }
                    }
                    return data;
                }
            },
            {
                "data": null,
                "render": function(data, type, row, meta) {
                    if (type === 'display') {
                        var smartListId = row.smartListId;
                        data = '<div class="text-right"><a class="btn btn-xs btn-secondary btn-danger" id="remove_' + row.smartListId + '" data-toggle="confirmation" href="javascript:removeSmartListTemplate(\'' + smartListId + '\');" alt="Remove" title="Are you sure? Do you want to Remove"><i class="fa fa-times red"></i></a></div>';    
                    }
                    return data;
                }
            }
        ]
    });
    $("#loader").hide();
}

function removeSmartListTemplate(smartListId){
	$('#deleteSmartListTemplate input[name=smartListId]').val(smartListId);
	$("#deleteSmartListTemplate").submit();
}
</script>
<form id="deleteSmartListTemplate" name="deleteSmartListTemplate" action="<@ofbizUrl>deleteSmartListTemplate</@ofbizUrl>" method="post">
    <input type="hidden" id="smartListId" name="smartListId" />
</form>






