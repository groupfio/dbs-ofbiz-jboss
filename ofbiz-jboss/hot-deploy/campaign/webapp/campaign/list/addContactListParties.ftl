       <div class="page-header border-b">
          <h1 class="float-left">Add List Member - ${contactListName?if_exists} (${contactId?if_exists})</h1>
		  <div class="float-right">
		  <a href="<@ofbizUrl>viewContactList?contactListId=${contactId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Back</a>
		  </div>
		  </div>
           <form method="post" action="<@ofbizUrl>addNewMember?contactListId=${contactId?if_exists}</@ofbizUrl>" id="addMemberList " name="addMemberList ">
		   <div class="card-header">
            <input type="hidden" name="duplicatingPartyId">
	        <input type="hidden" name="contactListId" value="${contactId?if_exists}" />
            <input type="hidden" name="">
			<div class="row padding-r">
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="firstName" id="firstName" placeholder="First Name">
              </div>
            </div>
			<div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="lastName" id="lastName" placeholder="Last Name">
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="infoString" id="infoString" placeholder="Email">
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                <input type="text" class="form-control input-sm" name="groupName" id="groupName" placeholder="Company Name">
              </div>
            </div>
			<div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
               <select name="roleType" id="roleType" class="ui dropdown search form-control input-sm"  <#if contactListRoleType?exists && contactListRoleType?has_content>disabled</#if>>
                 <option value="" >Please Select</option>
                 <option value="ACCOUNT" <#if selectedRoleType?has_content && selectedRoleType?if_exists=="ACCOUNT">selected</#if>>Account</option>
                 <option value="CONTACT" <#if selectedRoleType?has_content && selectedRoleType?if_exists=="CONTACT">selected</#if>>Contact</option>
                 <option value="LEAD" <#if selectedRoleType?has_content && selectedRoleType?if_exists=="LEAD">selected</#if>>Lead</option>
			   </select>
              </div>
            </div>
            <div class="col-md-2 col-sm-2">
              <button type="submit" class="btn btn-sm btn-primary">Search </button>
            </div>
			</div>
          <div class="clearfix"> </div>
        </div>
        </form> 
        <#if contactListParties?exists && contactListParties?has_content>
      <form method="post" type="multi" use-row-submit="true" action="<@ofbizUrl>addContactListsParties?contactListId=${contactId?if_exists}</@ofbizUrl>" id="ListContactListParties" name="ListContactListParties">
	   <input type="hidden" name="_useRowSubmit" value="Y" />
        <div class="clearfix"> </div>
            <div class="page-header">
              <h2 class="float-left">Available Contact Members to Add </h2>
			  <div class="float-right">
			   <button type="button" class="btn btn-xs btn-primary mr-4" onclick="javascript:validateCheck();"  data-original-title="Please select atleast one Member">Add</button>
		  </div>
            </div>
            <div class="table-responsive">
              <table id="example" class="table table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                  </tr>
                </thead>
                <tbody>
                <#list contactListParties as list>
                  <tr>
                   <td>${list?if_exists.firstName?if_exists} ${list?if_exists.lastName?if_exists}(${list?if_exists.partyId?if_exists}) </td>
				   <td>${list?if_exists.contactNumber?if_exists}${list?if_exists.infoString?if_exists}</td>
				   <td>
				   <input type="hidden" name="partyId_o_${list_index?if_exists}" value="${list.partyId?if_exists}" />
				   <input type="hidden" name="statusId_o_${list_index?if_exists}" value="CLPT_ACCEPTED" />
				   <input type="hidden" name="preferredContactMechId_o_${list_index?if_exists}" value="${list.contactMechId?if_exists}" />
				   <input type="hidden" name="infoString_o_${list_index?if_exists}" value="${list.infoString?if_exists}" />
                   <input type="checkbox" name="_rowSubmit_o_${list_index?if_exists}" id="_rowSubmit_o_${list_index?if_exists}" value="Y" />
				   </td>
                  </tr>
                  </#list>
                </tbody>
              </table>
            </div>
      </div>
    </div>
    </form>
    </#if>
<script>
$(document).ready(function (){
   var table = $('#example').DataTable({
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         "targets": [2]
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#example tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
});

function validateCheck(){
		var checkedAtLeastOne=false;
		/*$('#ListContactListParties input[type="checkbox"]').each(function() {
	    if ($(this).is(":checked")) {*/
	    
	   	if($('#ListContactListParties input[type="checkbox"]').is(':checked')){
	        document.getElementById("ListContactListParties").submit();
	    }
	    else
	    {
	    showAlert ("error", "Select one contact from the list to add");
          //$('[data-toggle="confirmation"]').confirmation();
	    	return false;
	    }
}
</script>

