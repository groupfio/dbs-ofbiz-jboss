        <div class="page-header border-b">
            <h1 class="float-left">Find Contact Lists</h1>
            <p class="float-right">
                <a  href="<@ofbizUrl>createContactListForm</@ofbizUrl>" class="btn btn-xs btn-primary">Create New</a>           
            </p>
        </div>
        <div class="card-header">
        <form method="post" action="findContactList" id="findContactForm" class="form-horizontal" name="findContactForm" novalidate="novalidate" data-toggle="validator">
            <input type="hidden" name="duplicatingPartyId">
            <input type="hidden" name="">
            <input type="hidden" name="">
          <div class="row padding-r">
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
             <input type="text" class="form-control input-sm" name="contactListId" id="contactListId" placeholder="Enter Contact List ID" value="${contactListId?if_exists}">            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
             <input type="text" class="form-control input-sm" name="contactListName" id="contactListName" placeholder="Enter Contact List Name" value="${contactListName?if_exists}">            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <select name="contactType" id="contactType" class="ui dropdown search form-control input-sm" >
               <option value="" ><div>Please Select</div></option>
               <option value="EMAIL_ADDRESS" <#if contactType?exists && contactType?has_content && contactType == "EMAIL_ADDRESS">selected</#if>>Email Address</option>
               <option value="POSTAL_ADDRESS" <#if contactType?exists && contactType?has_content && contactType == "POSTAL_ADDRESS">selected</#if>>Postal Address</option>
               <option value="TELECOM_NUMBER" <#if contactType?exists && contactType?has_content && contactType == "TELECOM_NUMBER">selected</#if>>Phone Number</option>
               </select>
            </div>
          </div>
          <div class="col-md-2 col-sm-2 pl-0">
            <button type="submit" class="btn btn-sm btn-primary">${uiLabelMap.find} </button>
          </div>
        </div>
            
        </form>
        </div>
        <div class="clearfix"> </div>
        <div class="page-header">
            <h2 class="float-left">${uiLabelMap.contactListView!} </h2>
        </div>
        <div class="table-responsive">
            <table id="contactListTable" class="table table-striped">
                <thead>
                    <tr>
                        <th>${uiLabelMap.contactListName!}</th>
                        <th>${uiLabelMap.type}</th>
                        <th>${uiLabelMap.description!}</th>
                        <th>${uiLabelMap.createdDate!}</th>
                    </tr>
                </thead>
                <tbody>
                <#if contactListData?has_content>
                    <#list contactListData as contactList>
                    <tr>
                        <#if contactList.isSmartList?if_exists="Y">
                             <td><a href="<@ofbizUrl>viewSmartContactList?contactListId=${contactList?if_exists.contactListId?if_exists}</@ofbizUrl>">${contactList?if_exists.contactListName?if_exists}(${contactList?if_exists.contactListId?if_exists})</a></td>
                        <#else>
                             <td><a href="<@ofbizUrl>viewContactList?contactListId=${contactList?if_exists.contactListId?if_exists}</@ofbizUrl>">${contactList?if_exists.contactListName?if_exists}(${contactList?if_exists.contactListId?if_exists})</a></td>
                        </#if>
                        <td>
                         <#if contactList?has_content>
					      <#if contactList?if_exists.contactMechTypeId?if_exists == "TELECOM_NUMBER">
					      Phone Number
					      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "EMAIL_ADDRESS">
					      Email Address
					      <#elseif contactList?if_exists.contactMechTypeId?if_exists == "POSTAL_ADDRESS">
					      Postal Address
					      </#if>
					      </#if>
                        </td>
                        <td><div  title ="${contactList?if_exists.description?if_exists}" class="longtext">${contactList?if_exists.description?if_exists}</div></td>
                        <td><#if contactList.createdStamp?has_content>${contactList.createdStamp?string["yyyy-MM-dd"]}</#if></td>
                    </tr>
                    </#list>
                </#if>
                </tbody>
            </table>
        </div>
 
<!-- /.container -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {	
    $('#contactListTable').DataTable({
        "order": [[ 3, 'desc' ]],
    });
});
</script>