<script>
$(document).ready(function(){
$("#campaign_data_table").DataTable();
});
</script>


<div class="page-header border-b" style="">
   <h1 class="float-left">Campaign detail</h1>
</div>
<div class="col-md-12 col-sm-12">
		<div class="small-box border rounded">
        <div class="inner">
 	    <div class="page-header">
        <h2 class="float-left">${marketingCampaign.campaignName!}</h2>  
        <h2 class="float-right">
        <a href="main#campDet" class="btn btn-primary btn-xs">Back</a>
        <#if isExpandAll?has_content>	
        <#if isExpandAll=="Y">
        	<a href="getCampaignDetails?campaignId=${marketingCampaignId!}&csrPartyId=${csrPartyId!}&expand=N" class="btn btn-primary btn-xs">Collapse all</a>
        </#if>
        <#if isExpandAll=="N">
        	<a href="getCampaignDetails?campaignId=${marketingCampaignId!}&csrPartyId=${csrPartyId!}&expand=Y" class="btn btn-primary btn-xs">Expand all</a>
        </#if>		
        </#if>
        
        <#if !(isExpandAll?has_content)>
        	<a href="getCampaignDetails?campaignId=${marketingCampaignId!}&csrPartyId=${csrPartyId!}&expand=Y" class="btn btn-primary btn-xs">Expand all</a>
        </#if>
        	
        </h2>             
        </div>
		<div class="table-responsive">				   
			<table class="table table-striped" id="campaign_data_table">
            <thead>
              <tr>
                <th>RM</th>
                <th>Company Name</th>
                <th>Contact Name</th>
                <th>Contact Email</th>
                <th>Contact Mobile</th>
                <th>Opened</th>
                <th>Not Opened</th>
                <th>Clicked</th>
                <th>Converted</th>
                <th>UnSubscribed</th>
                <th>Boun</th>
                <th>Black</th>
              </tr>
            </thead>
            <tbody>
            <#if campaignDetails?has_content>
            <#list campaignDetails as camp>
              <tr>
                <td>${camp.rmPartyName!} </td>
                <td>${camp.accntPartyName!}</td>
                <td>${camp.contactPartyName!}</td>
                <td>${camp.contactPartyEmail!} </td>
                <td>${camp.phoneNumber!}</td>
                <td>${camp.open!}</td>
                <td>${camp.not_open!}</td>
                <td>${camp.clicked!} </td>
                <td>${camp.converted!}</td>
                <td>${camp.unsubscribed!}</td>
                <td>${camp.boun!}</td>
                <td>${camp.black!}</td>
              </tr>   
			</#list>   
			</#if>
            </tbody>
          </table>
         </div>          	
        </div>
        </div>
        </div>