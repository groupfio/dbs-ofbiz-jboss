<script>
   $(document).ready(function(){
   $("#note_data_table").DataTable();
   $("#salestrigger_data_table").DataTable({
   "order": [[ 7, 'desc' ]]
   });
   $("#campaign_data_table").DataTable();
   });
</script>
<script>includeHTML();</script>
<div class="page-header border-b">
   <h1 class="float-left">RM Dashboard</h1> 
</div>
<div class="row"> 
   <div class="col-md-12 col-sm-12">
      <div class="small-box border rounded">
         <div class="inner">
            <div class="page-header">
               <h2 class="float-left">Sales Triggers</h2>
            </div>
            <div class="table-responsive">
               <table class="table table-striped" id="salestrigger_data_table">
                  <thead>
                     <tr>
                        <th>Trigger Name</th>
                        <!--<th>Company Name</th>-->
                        <th>Customer Name</th>
                        <!-- <th>Contact Email</th>
                           <th>Contact Mobile</th>
                           <th>Opened</th>
                           <th>Not Opened</th>
                           <th>Clicked</th>
                           <th>Converted</th>
                           <th>Unsub</th>
                           <th>Boun</th>
                           <th>Black</th>-->
                        <th>CIN</th>
                        <th>Product</th>
                        <th>O/s bal</th>
                        <th>Recommended Action</th>
                        <th>Report date</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     <#if campaignDetails?has_content>
                     <#list campaignDetails as camp>
                     <tr>
                        <td>${camp.campaignName!} </td>
                        <!-- <td>${camp.accntPartyName!}</td>-->
                        <td>${camp.contactPartyName!}</td>
                        <!-- <td>${camp.contactPartyEmail!} </td>
                           <td>${camp.phoneNumber!}</td>
                           <td>${camp.open!}</td>
                           <td>${camp.not_open!}</td>
                           <td>${camp.clicked!} </td>
                           <td>${camp.converted!}</td>
                           <td>${camp.unsubscribed!}</td>
                           <td>${camp.boun!}</td>
                           <td>${camp.converted!}</td> -->  
                        <td>${camp.cin!}</td>
                        <td>${camp.product!}</td>
                        <td>${camp.osBalance!}</td>
                        <td>${camp.recommendedAction!}</td>
                        <td>${camp.reportDate?string["dd MMMM yyyy"]}</td>
                        <td>Pending</td>
                     </tr>
                     </#list>   
                     </#if>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12 col-sm-12" id="campDet">
      <div class="small-box border rounded">
         <div class="inner">
            <div class="page-header">
               <h2 class="float-left">Campaigns</h2>
            </div>
            <div class="table-responsive">
               <table class="table table-striped" id="campaign_data_table">
                  <thead>
                     <tr>
                        <!-- <th>Campaign Name</th>
                           <th>Emails</th>
                           <th>Customers</th>
                           <th>Opened</th>
                           <th>Not Opened</th>
                           <th>Clicked</th>
                           <th>Converted</th>
                           <th>Unsub</th>
                           <th>Boun</th>
                           <th>Black</th>-->
                        <th>Campaign Name</th>
                        <th>Campaign Date</th>
                        <th>Product</th>
                        <th># of Customers</th>
                        <th># Opened</th>
                        <th># Registered</th>
                        <th># Bounced</th>
                        <th># Unsub</th>
                     </tr>
                  </thead>
                  <tbody>
                     <#if nonSalesTriggerSqlQurey?has_content>
                     <#list nonSalesTriggerSqlQurey as sales>
                     <tr>
                        <td>${sales.campaign_name!} </td>
                        <td>${sales.startDate!} </td>
                        <td>${sales.product!} </td>
                        <td>${sales.customers!}</td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.open!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.subscribe!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.boun!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.unsubscribed!}</a></td>
                        <#-- 
                        <td>${sales.campaign_name!} </td>
                        <td>${sales.email!}</td>
                        <td>${sales.customers!}</td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.open!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.not_open!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.clicked!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.converted!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.unsubscribed!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.boun!}</a></td>
                        <td><a href="getCampaignDetails?campaignId=${sales.marketingCampaignId!}&csrPartyId=${sales.csr_party_id!}" target="_blank">${sales.black!}</a></td>
                        -->
                     </tr>
                     </#list>  
                     </#if>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12 col-sm-12">
      <div class="small-box border rounded">
         <div class="inner">
            <div class="page-header">
               <h2 class="float-left">Follow-Ups Calls</h2>
            </div>
            <div class="table-responsive">
               <table class="table table-striped" id="note_data_table">
                  <thead>
                     <tr>
                        <th>Note type </td>
                        <th>Sub Note </td>
                        <th>Call back date </td>
                        <th>Company name </td>
                        <th>Remarks </td>
                     </tr>
                  </thead>
                  <tbody>
                     <#if noteContainer?has_content>
                     <#list noteContainer as note>
                     <tr>
                        <td>${note.noteId!} </td>
                        <td>${note.subNtType!} </td>
                        <td>${note.callBackDate?string["dd MMMM"]} </td>
                        <td>${note.campaignName!} </td>
                        <td>${note.noteInfo!}</td>
                     </tr>
                     </#list>  
                     </#if>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>