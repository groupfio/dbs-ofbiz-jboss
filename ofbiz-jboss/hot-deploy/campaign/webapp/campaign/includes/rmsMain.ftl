<style>
	#tabExample1{
	background-color:#092936;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover{
	background: #70b0e0;

}
.btn.btn-primary, .btn.btn-info{
	background-color:#678fcc;
	border-radius:4px ;
	border:none;
	
}
.box {
	-webkit-border-radius:0px;
	-moz-border-radius:0px;
	border-radius:0px;
	border:0px solid #e9e9e9;
	background-color:#eff0f2;
	margin-bottom:20px;
	color:#5472A4;
	position:relative;
	-webkit-box-shadow:rgba(0, 0, 0, 0.1) 0 1px 0px;
	-moz-box-shadow:rgba(0, 0, 0, 0.1) 0 1px 0px;
	box-shadow:rgba(0, 0, 0, 0.1) 0 1px 0px
}

.tabs-left>.nav-tabs>li>a:hover {
	background-color:#70B0E0;
	}
.icons-sprite, .fc-header-left .fc-button-prev .fc-button-inner, .fc-header-left .fc-button-next .fc-button-inner, .dropdown_arrow.accordion-toggle:after, .button a.accordion-toggle:after, #main .top_right ul.nav>li a.accordion-toggle:after, button a.accordion-toggle:after, #sidebar_menu li a.accordion-toggle:after, .dropdown_arrow.dropdown-toggle:after, .button a.dropdown-toggle:after, #main .top_right ul.nav>li a.dropdown-toggle:after, button a.dropdown-toggle:after, #sidebar_menu li a.dropdown-toggle:after, div.checker span, div.checker span.checked, div.radio span, div.radio span.checked, .ui-datepicker .ui-datepicker-prev .ui-icon, .ui-datepicker .ui-datepicker-next .ui-icon, ul.messages_layout li .message_wrap .arrow, .search form .input-append button.btn_search, .menu_states #menu_state_1, .menu_states #menu_state_2, .menu_states #menu_state_3, #header .breadcrumbs ul li .arrow {
	background:url('images/icons-s195a86524b.png') no-repeat
}
.fc {
	direction:ltr;
	text-align:left;
	position:relative
}
.fc table {
	border-collapse:collapse;
	border-spacing:0
}
html .fc, .fc table {
	font-size:1em
}
.fc td, .fc th {
	padding:0;
	vertical-align:top
}
.external-event {
	margin:10px 0;
	padding:3px 9px;
	background:url("shadows/b15.png");
	color:#333;
	font-size:15px;
	line-height:1.8;
	cursor:pointer;
	border-bottom:1px solid #ddd;
}
.fc-header td {
	white-space:nowrap;
	background:none
}
.fc-header-left {
	width:100%;
	text-align:left;
	position:absolute;
	left:0;
	top:6px
}
.fc-header-left .fc-button {
	margin:0;
	position:relative
}
.fc-header-left .fc-button-prev, .fc-header-left .fc-button-next {
	float:left;
	border:none;
	padding:5px 10px;
	/* opacity:0.5; */
}
/* .fc-header-left .fc-button-prev .fc-button-inner, .fc-header-left .fc-button-next .fc-button-inner {
	border:none
}
.fc-header-left .fc-button-prev .fc-button-inner .fc-button-content, .fc-header-left .fc-button-next .fc-button-inner .fc-button-content {
	display:none
}
.fc-header-left .fc-button-prev.fc-state-hover, .fc-header-left .fc-button-next.fc-state-hover {
	opacity:1
}
.fc-header-left .fc-button-prev.fc-state-down, .fc-header-left .fc-button-next.fc-state-down {
	background:none !important;
	margin-top:-1px
} */
/* .fc-header-left .fc-button-prev .fc-button-inner {
	height:16px;
	width:11px
} */
.fc-header-left .fc-button-next {
	float:right
}
/* .fc-header-left .fc-button-next .fc-button-inner {
	height:16px;
	width:11px
} */
.fc-header-left .fc-button-today, .fc-header-left .fc-button-today .fc-button-inner {
	font-size:15px;
	background:url("shadows/b15.png");
	margin-top:0px
}
.fc-header-left .fc-button-today .fc-button-content {
	height:25px;
	color:#333;
	line-height:25px;
	font-size:15px;
	background-color:#fff !important;
}
.fc-header-left .fc-button-next .fc-button-content {
	height:25px;
	/* color:#333; */
	color:#2d65b2 !important;
	line-height:25px;
	font-size:15px;
	 -webkit-user-select: none; /* webkit (safari, chrome) browsers */
    -moz-user-select: none; /* mozilla browsers */
    -khtml-user-select: none; /* webkit (konqueror) browsers */
    -ms-user-select: none; /* IE10+ */
}
.fc-header-left .fc-button-prev .fc-button-content {
	height:25px;
	color:#2d65b2 !important;
	line-height:25px;
	font-size:15px;
	 -webkit-user-select: none; /* webkit (safari, chrome) browsers */
    -moz-user-select: none; /* mozilla browsers */
    -khtml-user-select: none; /* webkit (konqueror) browsers */
    -ms-user-select: none; /* IE10+ */
}
.fc-header-left .fc-button-prev.fc-state-hover, .fc-header-left .fc-button-next.fc-state-hover {
	background:none !important;
}
.fc-header-center {
	text-align:center
}
.fc-header-right {
	text-align:right;
	position:absolute;
	top:-55px;
	right:0px
}
.fc-header-title {
	display:inline-block;
	vertical-align:top;
}
.fc-header-title h2 {
	margin-top:0;
	white-space:nowrap;
	font-size:2.1rem;
	color:#2d65b2 !important;
	line-height:33px
}
.fc .fc-header-space {
	padding-left:10px
}
.fc-header .fc-button {
	margin-bottom:1em;
	vertical-align:top
}
.fc-header .fc-button {
	margin-right:-1px
}
.fc-header .fc-corner-right {
	margin-right:1px
}
.fc-header .ui-corner-right {
	margin-right:0
}
.fc-header .fc-state-hover, .fc-header .ui-state-hover {
	z-index:2
}
.fc-header .fc-state-down {
	z-index:3
}
.fc-header .fc-state-active, .fc-header .ui-state-active {
	z-index:4
}
.fc-content {
	clear:both;
	background:url("shadows/b150png")
}
.fc-view {
	width:100%;
	overflow:hidden
}
.fc-view thead {
	background:url("shadows/b15.png");
	line-height:35px
}
.fc-widget-header, .fc-widget-content {
	border:1px solid rgba(0, 0, 0, 0.2)
}
.fc-state-highlight {
	background:url("shadows/b15.png")
}
.fc-cell-overlay {
	background:#9cf;
	opacity:.2;
	filter:alpha(opacity=20)
}
.fc-button {
	position:relative;
	display:inline-block;
	cursor:pointer
}
.fc-state-default {
	border-style:solid;
	border-width:0
}
.fc-button-inner {
	position:relative;
	float:left;
	overflow:hidden
}
.fc-state-default .fc-button-inner {
	border-style:solid;
	border-width:0
}
.fc-button-content {
	position:relative;
	float:left;
	height:45px;
	line-height:26px;
	padding:0 .6em;
	white-space:nowrap;
	text-transform:uppercase;
	font-weight:300;
	font-size:14px;
	font-family:"Open Sans",Arial,sans-serif
}
.fc-button-content .fc-icon-wrap {
	position:relative;
	float:left;
	top:50%
}
.fc-button-content .ui-icon {
	position:relative;
	float:left;
	margin-top:-50%;
*margin-top:0;
*top:-50%
}
.fc-state-default .fc-button-effect {
	position:absolute;
	top:0;
	left:0
}
.fc-state-default .fc-button-effect span {
	position:absolute;
	top:-100px;
	left:0;
	width:500px;
	height:100px;
	border-width:100px 0 0 1px;
	border-style:solid;
	border-color:#fff;
	background:#444;
	opacity:.09;
	filter:alpha(opacity=9)
}
.fc-state-default, .fc-state-default .fc-button-inner {
	color:#fff
}
.fc-state-hover, .fc-state-hover .fc-button-inner {
	background:url(shadows/b15.png)
}
.fc-state-active, .fc-state-active .fc-button-inner {
	background:url(shadows/b15.png);
	color:#fff;
}
.fc-state-disabled, .fc-state-disabled .fc-button-inner {
	color:#999;
	border-color:#ddd
}
.fc-state-disabled {
	cursor:default
}
.fc-state-disabled .fc-button-effect {
	display:none
}
.fc-event {
	border-style:solid;
	border-width:0;
	font-size:.85em;
	cursor:default
}
a.fc-event, .fc-event-draggable {
	cursor:pointer
}
a.fc-event {
	text-decoration:none
}
.fc-rtl .fc-event {
	text-align:right
}
.fc-event-skin {
	border-color:rgba(0, 0, 0, 0.4);
	background:url(shadows/b15.png);
	color:#000
}
.fc-event-inner {
	position:relative;
	width:100%;
	height:100%;
	border-style:solid;
	border-width:0;
	overflow:hidden
}
.fc-event-time, .fc-event-title {
	padding:0 1px
}
.fc .ui-resizable-handle {
	display:block;
	position:absolute;
	z-index:99999;
	overflow:hidden;
	font-size:300%;
	line-height:50%
}
.fc-event-hori {
	border-width:0px 0;
	margin-bottom:1px
}
.fc-event-hori .ui-resizable-e {
	top:0 !important;
	right:-3px !important;
	width:7px !important;
	height:100% !important;
	cursor:e-resize
}
.fc-event-hori .ui-resizable-w {
	top:0 !important;
	left:-3px !important;
	width:7px !important;
	height:100% !important;
	cursor:w-resize
}
.fc-event-hori .ui-resizable-handle {
	_padding-bottom:14px
}
.fc-corner-left {
	margin-left:1px
}
.fc-corner-left .fc-button-inner, .fc-corner-left .fc-event-inner {
	margin-left:-1px
}
.fc-corner-right {
	margin-right:1px
}
.fc-corner-right .fc-button-inner, .fc-corner-right .fc-event-inner {
	margin-right:-1px
}
.fc-corner-top {
	margin-top:1px
}
.fc-corner-top .fc-event-inner {
	margin-top:-1px
}
.fc-corner-bottom {
	margin-bottom:1px
}
.fc-corner-bottom .fc-event-inner {
	margin-bottom:-1px
}
.fc-corner-left .fc-event-inner {
	border-left-width:0px
}
.fc-corner-right .fc-event-inner {
	border-right-width:0px;
	padding-left:3px
}
.fc-corner-top .fc-event-inner {
	border-top-width:0px
}
.fc-corner-bottom .fc-event-inner {
	border-bottom-width:0px
}
table.fc-border-separate {
	border-collapse:separate
}
.fc-border-separate th, .fc-border-separate td {
	border-width:1px 0 0 1px
}
.fc-border-separate th.fc-last, .fc-border-separate td.fc-last {
	border-right-width:1px
}
.fc-border-separate tr.fc-last th, .fc-border-separate tr.fc-last td {
	border-bottom-width:0px
}
/*.fc-first {
	border-left-width:0 !important
}
.fc-last {
	border-right-width:0 !important
}*/
.fc-grid th {
	text-align:center
}
.fc-grid .fc-day-number {
	float:right;
	padding:0 2px
}
.fc-grid .fc-other-month .fc-day-number {
	opacity:0.5;
	filter:alpha(opacity=0.5)
}
.fc-grid .fc-day-content {
	clear:both;
	padding:2px 2px 1px
}
.fc-grid .fc-event-time {
	font-weight:bold;
	font-size:11px;
	color:#fff;
}
.fc-rtl .fc-grid .fc-day-number {
	float:left
}
.fc-rtl .fc-grid .fc-event-time {
	float:right
}
.fc-agenda table {
	border-collapse:separate
}
.fc-agenda-days th {
	text-align:center
}
.fc-agenda .fc-agenda-axis {
	width:60px !important;
	padding:0 4px;
	vertical-align:middle;
	text-align:right;
	white-space:nowrap;
	font-weight:normal
}
.fc-agenda .fc-day-content {
	padding:2px 2px 1px
}
.fc-agenda-days .fc-agenda-axis {
	border-right-width:1px
}
.fc-agenda-days .fc-col0 {
	border-left-width:0
}
.fc-agenda-allday th {
	border-width:0 1px
}
.fc-agenda-allday .fc-day-content {
	min-height:34px;
	_height:34px
}
.fc-agenda-divider-inner {
	height:2px;
	overflow:hidden
}
.fc-widget-header .fc-agenda-divider-inner {
	background:#eee
}
.fc-agenda-slots th {
	border-width:1px 1px 0
}
.fc-agenda-slots td {
	border-width:1px 0 0;
	background:none
}
.fc-agenda-slots td div {
	height:20px
}
.fc-agenda-slots tr.fc-slot0 th, .fc-agenda-slots tr.fc-slot0 td {
	border-top-width:0
}
.fc-agenda-slots tr.fc-minor th, .fc-agenda-slots tr.fc-minor td {
	border-top-style:dotted
}
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
*border-top-style:solid
}
.fc-event-vert {
	border-width:1px;
	border-color:rbba(0, 0, 0, 0.15)
}
.fc-event-vert .fc-event-head, .fc-event-vert .fc-event-content {
	position:relative;
	z-index:2;
	width:100%;
	overflow:hidden
}
.fc-event-vert .fc-event-time {
	white-space:nowrap;
	font-size:10px
}
.fc-event-vert .fc-event-bg {
	position:absolute;
	z-index:1;
	top:0;
	left:0;
	width:100%;
	height:100%;
	background:#fff;
	opacity:.3;
	filter:alpha(opacity=30)
}
.fc .ui-draggable-dragging .fc-event-bg, .fc-select-helper .fc-event-bg {
	display:none\9
}
.fc-event-vert .ui-resizable-s {
	bottom:0 !important;
	width:100% !important;
	height:8px !important;
	overflow:hidden !important;
	line-height:8px !important;
	font-size:11px !important;
	font-family:monospace;
	text-align:center;
	cursor:s-resize
}
.fc-agenda .ui-resizable-resizing {
	_overflow:hidden
}
.box .title {
	position: relative;
	color: #fff;
	background-color: #205081;
	height: 30x;
}
.box .title h1, .box .title h2, .box .title h3, .box .title h4, .box .title h5 {
    padding: 5px 15px;
    text-transform: capitalize;
    font-size: 15px;
}
	</style>
  
  </head>
<body>
      <div class="page-header border-b">
        <h1>Dashboard</h1>
      </div>
      <div class="row">
       
</div>
<div class="clearfix"></div>
<p></p>
<div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="box color_13 paint_hover" id="tour_calendar">
                  <div class="title">
                    <h4 id="ext-gen104"> <span>Marketing Calendar</span> </h4>
                  </div>
                  <div class="content top " id="ext-gen120">
                    <div id="calendar" class="fc">
                      <table class="fc-header" style="width:100%">
                        <tbody>
                          <tr>
                            <td class="fc-header-left" id="ext-gen130"><span class="fc-button fc-button-prev fc-state-default fc-corner-left"><span class="fc-button-inner"><span class="fc-button-content">&nbsp;◄&nbsp;</span><span class="fc-button-effect"><span></span></span></span></span><span class="fc-button fc-button-next fc-state-default fc-corner-right" id="ext-gen121"><span class="fc-button-inner"><span class="fc-button-content">&nbsp;►&nbsp;</span><span class="fc-button-effect"><span></span></span></span></span></td>
                            <td class="fc-header-center" id="ext-gen122">
                              <span class="fc-header-title">
                                <h2>May 2018</h2>
                              </span>
                            </td>
                            <td class="fc-header-right"><span class="fc-button fc-button-month fc-state-default fc-corner-left fc-state-active"><span class="fc-button-inner"><span class="fc-button-content">month</span><span class="fc-button-effect"><span></span></span></span></span><span class="fc-button fc-button-agendaWeek fc-state-default"><span class="fc-button-inner"><span class="fc-button-content">week</span><span class="fc-button-effect"><span></span></span></span></span><span class="fc-button fc-button-agendaDay fc-state-default fc-corner-right"><span class="fc-button-inner"><span class="fc-button-content">day</span><span class="fc-button-effect"><span></span></span></span></span></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="fc-content" style="position: relative; min-height: 1px;">
                        <div class="fc-view fc-view-month fc-grid" style="position: relative;" unselectable="on">
                          <table class="fc-border-separate" style="width:100%" cellspacing="0">
                            <thead>
                              <tr class="fc-first fc-last">
                                <th class="fc-sun fc-widget-header fc-first" style="width: 118px;">Sun</th>
                                <th class="fc-mon fc-widget-header" style="width: 118px;">Mon</th>
                                <th class="fc-tue fc-widget-header" style="width: 118px;" id="ext-gen131">Tue</th>
                                <th class="fc-wed fc-widget-header" style="width: 118px;" id="ext-gen132">Wed</th>
                                <th class="fc-thu fc-widget-header" style="width: 118px;">Thu</th>
                                <th class="fc-fri fc-widget-header" style="width: 118px;" id="ext-gen135">Fri</th>
                                <th class="fc-sat fc-widget-header fc-last" style="width: 118px;">Sat</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="fc-week0 fc-first">
                                <td class="fc-sun fc-widget-content fc-day0 fc-first fc-other-month">
                                  <div style="min-height: 90px;">
                                    <div class="fc-day-number">29</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 20px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day1 fc-other-month" id="ext-gen129">
                                  <div>
                                    <div class="fc-day-number">30</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day2" id="ext-gen128">
                                  <div>
                                    <div class="fc-day-number">1</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day3" id="ext-gen127">
                                  <div>
                                    <div class="fc-day-number">2</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day4" id="ext-gen126">
                                  <div id="ext-gen133">
                                    <div class="fc-day-number">3</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative" id="ext-gen134">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day5">
                                  <div id="ext-gen136">
                                    <div class="fc-day-number">4</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative" id="ext-gen125">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day6 fc-last">
                                  <div>
                                    <div class="fc-day-number">5</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative" id="ext-gen124">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr class="fc-week1">
                                <td class="fc-sun fc-widget-content fc-day7 fc-first">
                                  <div style="min-height: 90px;">
                                    <div class="fc-day-number">6</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 20px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day8">
                                  <div>
                                    <div class="fc-day-number">7</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day9">
                                  <div>
                                    <div class="fc-day-number">8</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day10">
                                  <div>
                                    <div class="fc-day-number">9</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day11">
                                  <div>
                                    <div class="fc-day-number">10</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day12">
                                  <div>
                                    <div class="fc-day-number">11</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day13 fc-last">
                                  <div>
                                    <div class="fc-day-number">12</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr class="fc-week2">
                                <td class="fc-sun fc-widget-content fc-day14 fc-first">
                                  <div style="min-height: 90px;">
                                    <div class="fc-day-number">13</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 20px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day15">
                                  <div>
                                    <div class="fc-day-number">14</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day16">
                                  <div>
                                    <div class="fc-day-number">15</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day17">
                                  <div>
                                    <div class="fc-day-number">16</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day18">
                                  <div>
                                    <div class="fc-day-number">17</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day19">
                                  <div>
                                    <div class="fc-day-number">18</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day20 fc-last">
                                  <div>
                                    <div class="fc-day-number">19</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr class="fc-week3">
                                <td class="fc-sun fc-widget-content fc-day21 fc-first">
                                  <div style="min-height: 90px;">
                                    <div class="fc-day-number">20</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 19px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day22">
                                  <div>
                                    <div class="fc-day-number">21</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day23">
                                  <div>
                                    <div class="fc-day-number">22</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day24">
                                  <div>
                                    <div class="fc-day-number">23</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day25">
                                  <div>
                                    <div class="fc-day-number">24</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day26">
                                  <div>
                                    <div class="fc-day-number">25</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day27 fc-last">
                                  <div>
                                    <div class="fc-day-number">26</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr class="fc-week4">
                                <td class="fc-sun fc-widget-content fc-day28 fc-first">
                                  <div style="min-height: 90px;">
                                    <div class="fc-day-number">27</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 19px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day29">
                                  <div>
                                    <div class="fc-day-number">28</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day30">
                                  <div>
                                    <div class="fc-day-number">29</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day31">
                                  <div>
                                    <div class="fc-day-number">30</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day32 fc-state-highlight fc-today">
                                  <div>
                                    <div class="fc-day-number">31</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day33 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">1</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day34 fc-last fc-other-month">
                                  <div>
                                    <div class="fc-day-number">2</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr class="fc-week5 fc-last">
                                <td class="fc-sun fc-widget-content fc-day35 fc-first fc-other-month">
                                  <div style="min-height: 94px;">
                                    <div class="fc-day-number">3</div>
                                    <div class="fc-day-content">
                                      <div style="position: relative; height: 0px;">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-mon fc-widget-content fc-day36 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">4</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-tue fc-widget-content fc-day37 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">5</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-wed fc-widget-content fc-day38 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">6</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-thu fc-widget-content fc-day39 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">7</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-fri fc-widget-content fc-day40 fc-other-month">
                                  <div>
                                    <div class="fc-day-number">8</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                                <td class="fc-sat fc-widget-content fc-day41 fc-last fc-other-month">
                                  <div>
                                    <div class="fc-day-number">9</div>
                                    <div class="fc-day-content">
                                      <div style="position:relative">&nbsp;</div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <div style="position:absolute;z-index:8;top:0;left:0">
                            <div class="fc-event fc-event-skin fc-event-hori fc-event-draggable fc-corner-left fc-corner-right" style="position: absolute; z-index: 8; left: 20px; background-color: rgb(0, 153, 0); width: 101px; top: 59px;" data-hasqtip="0">
                              <div class="fc-event-inner fc-event-skin" style="background-color:#205081">
                                <span class="fc-event-time">6:32    AM</span>
                                <span class="fc-event-title">
                                  <div style="float: right;display: inline-block;width: 14px; margin-top: 1px;"></div>
                                </span>
                              </div>
                              <div class="ui-resizable-handle ui-resizable-e">&nbsp;&nbsp;&nbsp;</div>
                            </div>
                            <div class="fc-event fc-event-skin fc-event-hori fc-event-draggable fc-corner-left fc-corner-right" style="position: absolute; z-index: 8; left: 646px; background-color: rgb(0, 153, 0); width: 146px; top: 160px;" data-hasqtip="1">
                              <div class="fc-event-inner fc-event-skin" style="background-color:#205081">
                                <span class="fc-event-time">7:58    AM</span>
                                <span class="fc-event-title">
                                  <div style="float: right;display: inline-block;width: 14px;margin-top: 1px;"></div>
                                </span>
                              </div>
                              <div class="ui-resizable-handle ui-resizable-e">&nbsp;&nbsp;&nbsp;</div>
                            </div>
                            <div class="fc-event fc-event-skin fc-event-hori fc-event-draggable fc-corner-left fc-corner-right" style="position: absolute; z-index: 8; left: 646px; background-color: rgb(0, 153, 0); width: 101px; top: 261px;" data-hasqtip="2">
                              <div class="fc-event-inner fc-event-skin" style="background-color:#205081">
                                <span class="fc-event-time">3:50    AM</span>
                                <span class="fc-event-title">
                                  <div style="float: right;display: inline-block;width: 14px;margin-top: 1px;"></div>
                                </span>
                              </div>
                              <div class="ui-resizable-handle ui-resizable-e">&nbsp;&nbsp;&nbsp;</div>
                            </div>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
</div>
</div>
</div>
<div class="clearfix"></div>
<p></p>     
  