<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<@sectionHeader title="${uiLabelMap.findTagConfiguration!}" />
<form method="post" action="#" id="findTemplateTag" class="form-horizontal" name="findTemplateTag" novalidate="novalidate" data-toggle="validator">
   <input type="hidden" name="groupId" id="groupId" />
   <input type="hidden" name="groupingCode" id="groupingCode" />
   <div class="card-header">
   <div class="row padding-r">
   
      <#-- <div class="col-md-2 col-sm-2">
         <@simpleDropdownInput 
				id="templateTagTypeId"
				options=templateTagTypeList
				required=false
				value=""
				allowEmpty=true
				tooltip = uiLabelMap.tagType
				emptyText = uiLabelMap.tagType
				filter=true
				/> 
      </div>
       <div class="col-md-2 col-sm-2" id="CUSTOM_FIELD" style="display:none;">
            <@simpleDropdownInput 
				id="attributeGroup"
				options=""
				required=false
				value=""
				allowEmpty=true
				tooltip = uiLabelMap.attributeGroup
				emptyText = uiLabelMap.attributeGroup
				filter=true
				/>
       </div> 
       <div class="col-md-4 col-sm-4" id="SEGMENTATION" style="display:none;">
           <div class="row padding-r">
	           <div class="col-md-6 col-sm-6">
	            <@simpleDropdownInput 
					id = "segmentGroupingCode"
					options=""
					required=false
					allowEmpty=true
					tooltip = uiLabelMap.segmentGroupingCode
					emptyText = uiLabelMap.segmentGroupingCode
					filter=true
					/>
				</div>
				<div class="col-md-6 col-sm-6">
				<@simpleDropdownInput 
					id="segmentCode"
					options=""
					required=false
					allowEmpty=true
					tooltip = uiLabelMap.segmentCode
					emptyText = uiLabelMap.segmentCode
					filter=true
					/>
				</div>
			</div>
        </div>
        
        <div class="col-md-4 col-sm-4" id="ECONOMIC_METRIC" style="display:none;">
        	<div class="row padding-r">
	           <div class="col-md-6 col-sm-6">
	            <@simpleDropdownInput 
					id = "economicGroupingCode"
					options=""
					required=false
					allowEmpty=true
					tooltip = uiLabelMap.economicGroupingCode
					emptyText = uiLabelMap.economicGroupingCode
					filter=true
					/>
			   </div>
			   <div class="col-md-6 col-sm-6">
	            <@simpleDropdownInput 
					id="economicMetric"
					options=""
					required=false
					allowEmpty=true
					tooltip = uiLabelMap.economicMetric
					emptyText = uiLabelMap.economicMetric
					filter=true
					/>
			   </div>	
			</div>
        </div>-->
         <div class="col-md-2 col-sm-2">
             <@simpleInput 
             id="customFieldGroupName"
             placeholder="Group"
             required=false
             />
         </div>
         <div class="col-md-2 col-sm-2">
             <@simpleInput 
             id="customFieldName"
             placeholder="Field"
             required=false
             />
         </div>
         <div class="col-md-2 col-sm-2">
             <@simpleInput 
             id="tagId"
             placeholder="Tag Id"
             required=false
             />
         </div>
         <div class="col-md-2 col-sm-2">
             <@simpleInput 
             id="tagName"
             placeholder="Tag Name"
             required=false
             />
         </div>
         <@fromSimpleAction id="findTemplateTags" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
   </div>
   </div>
</form>
<div class="clearfix"> </div>
<div class="clearfix"> </div>
<@sectionTitle title="${uiLabelMap.tagsList!}" />
<div class="table-responsive">
   <table class="table table-striped" id="tagdatatable">
      <thead>
         <tr>
         	<th>Group Type</th>
            <th>Group</th>
            <th>Field/Value</th>
            <th>Tag ID</th>
            <th>Tag Name</th>
            <th>From Date</th>
            <th>Thru Date</th>
            <#--<th>Display</th>-->
            <th>Use For Contact</th>
            <th class="text-right">Action</th>
         </tr>
      </thead>
   </table>
</div>
<!-- Loader -->
<div class="loader text-center" id="loader" sytle="display:none;">
  <span></span>
  <span></span>
  <span></span>
</div>
<form id="deleteTagConfig" name="deleteTagConfig" action="<@ofbizUrl>deleteTemplateTagConfiguration</@ofbizUrl>" method="post">
    <input type="hidden" id="tagId" name="tagId">
</form>
<script type="text/javascript">
   $(function() {
      getTemplateTags();
   });
   
   $('#templateTagTypeId').change(function(){
   		$('#groupId').val('');
   		$('#groupingCode').val('');
  		var selectedItem = $('#templateTagTypeId').val();
		$('#templateTagTypeId').find('option').each(function() {
    		var option = $(this).val();
    		if(option != "" && option != null && option !='undefined'){
	    		if(selectedItem == option){
	    			$('#' + option).show();
	    		} else{
	    			$('#' + option).hide();
	    		}
	    	}
    	});
    	
    	if(selectedItem == "SEGMENTATION" || selectedItem == "ECONOMIC_METRIC"){
    		$('#segmentCode').val('').trigger('change');
    		$('#economicMetric').val('').trigger('change');
	    	// get the segment and economic grouping list
		  	$.post("getGroupingCode",{"templateTagTypeId":selectedItem},function(data){
	  			var nonSelectClass = "nonselect";
				var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
				if(data !=null && data !=""){
				   for(var i=0;i<data.length;i++){
				   		options += '<option value="'+data[i].customFieldGroupingCodeId+'" data-id="'+data[i].sequenceNo+'">'+data[i].groupingCode+'</option>';
				   }
				}
			    if(selectedItem == "SEGMENTATION"){
			        $("#segmentGroupingCode").empty();
			   		$("#segmentGroupingCode").append(options);
			   		$("#segmentGroupingCode").dropdown("refresh");
			    } else if(selectedItem == "ECONOMIC_METRIC"){
			   		$("#economicGroupingCode").empty();
			   		$("#economicGroupingCode").append(options);
			   		$("#economicGroupingCode").dropdown("refresh");
			    }
			});
    	} else if(selectedItem == "CUSTOM_FIELD"){
			getCodeOrGroupOrMetric(selectedItem,'');
		}
    	
   	    getTemplateTags();
   });
   
   function getCodeOrGroupOrMetric(tagType,groupingCode){
   	  
	  $.post("getCodeOrGroupOrMetric",{"templateTagTypeId":tagType,"groupingCode":groupingCode},function(data){
			var nonSelectClass = "nonselect";
			var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
			if(data !=null && data !=""){
			   for(var i=0;i<data.length;i++){
			   		options += '<option value="'+data[i].groupId+'" data-id="'+data[i].sequenceNo+'">'+data[i].groupName+'</option>';
			   }
			}
		    if(tagType == "SEGMENTATION"){
		        $("#segmentCode").empty();
		   		$("#segmentCode").append(options);
		   		$("#segmentCode").dropdown("refresh");
		    }
		    if(tagType == "CUSTOM_FIELD"){
		   		$("#attributeGroup").empty();
		   		$("#attributeGroup").append(options);
		   		$("#attributeGroup").dropdown("refresh");
		    }
		    if(tagType == "ECONOMIC_METRIC"){
		   		$("#economicMetric").empty();
		   		$("#economicMetric").append(options);
		   		$("#economicMetric").dropdown("refresh");
		    }
		});
		getTemplateTags();
	}
		
   $('#segmentGroupingCode').change(function(){
   	  $('#groupId').val('');
      var tagType = $('#templateTagTypeId').val();
      var groupingCode = $(this).val();
      $('#groupingCode').val(groupingCode);
      getCodeOrGroupOrMetric(tagType,groupingCode);
   });
  
   $('#economicGroupingCode').change(function(){
      $('#groupId').val('');
      var tagType = $('#templateTagTypeId').val();
      var groupingCode = $(this).val();
      $('#groupingCode').val(groupingCode);
      getCodeOrGroupOrMetric(tagType,groupingCode);
      $('#economicMetric').val('').trigger('change');
   });
   	
   $('#attributeGroup').change(function(){
   	    var selectedGroup = $(this).val();
   	    $('#groupId').val(selectedGroup);
   	    getTemplateTags();
   });
   $('#segmentCode').change(function(){
   	    var selectedGroup = $(this).val();
   	    $('#groupId').val(selectedGroup);
   	    getTemplateTags();
   });
   $('#economicMetric').change(function(){
   	    var selectedGroup = $(this).val();
   	    $('#groupId').val(selectedGroup);
   	    getTemplateTags();
   });
   
   $('#findTemplateTags').on('click',function(){
	  getTemplateTags();
   });
   $(function(){
      $('#customFieldGroupName, #customFieldName, #tagId, #tagName').keypress(function(e){
        if(e.which == 13) {
            getTemplateTags();
        }
      });
   });
   var searchKeyword ="";
   function getTemplateTags() {
   /*var templateTagTypeId = $('#templateTagTypeId').val();
   var groupingCode = $('#groupingCode').val();
   var groupId = $('#groupId').val();*/
   var customFieldGroupName = $('#customFieldGroupName').val();
   var customFieldName = $('#customFieldName').val();
   var tagId = $('#tagId').val();
   var tagName = $('#tagName').val();
   $("#loader").show();
   var url = "<@ofbizUrl>getTemplateTagConfigList</@ofbizUrl>";
   $('#tagdatatable').DataTable( {
   	    //"processing": true,
   	    "serverSide": true,
   	    "destroy": true,
   	    "filter" : true,
   	    "fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	},
   	    "ajax": {
               "url": url,
               "type": "POST",
               /*'data': {
		           templateTagTypeId: templateTagTypeId,
		           groupId: groupId,
		           groupingCode: groupingCode,
		           keyword : function(){
		           console.log($('.dataTables_filter input').val());
		           	return $('.dataTables_filter input').val();
		           }
		        },*/
		        "data": {
		            "customFieldGroupName":customFieldGroupName,
		            "customFieldName":customFieldName,
		            "tagId":tagId,
		            "tagName":tagName
		        }
           },
           "Paginate": true,
	   		"language": {
	   			"emptyTable": "No data available in table",
	   			"info": "Showing _START_ to _END_ of _TOTAL_ entries",
	   			"infoEmpty": "No entries found",
	   			"infoFiltered": "(filtered1 from _MAX_ total entries)",
	   			"lengthMenu": "Show _MENU_ entries",
	   			"zeroRecords": "No matching records found",
	   			"oPaginate": {
	   				"sNext": "Next",
	   				"sPrevious": "Previous"
	   			}
	   		},
	   		"ordering": false,
            "pageLength": 10,
            "bAutoWidth":false,
            "stateSave": false,
            "columns": [
            	{ "data": "groupType"},
            	{ "data": "customFieldGroupId",
            		"render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if(""==data || data == null){
	   							data = "N/A";
	   						}
	   					}
	   					return data;
	   				 }
            	},
	   	        { "data": "customFieldId", 
	   	        	"render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if(""==data || data == null){
	   							data = "N/A";
	   						}
	   					}
	   					return data;
	   				 }
	   	        },
                { "data": "tagId"},
	   	        { "data": "tagName" },
	   	        { "data": "fromDate" },
	   	        { "data": "thruDate" },
	   	        /*{ "data": "isEnabled",
	   	      	    "render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("N"==data){
	   							data = "No";
	   						} else if("Y"==data){
	   							data = "Yes";
	   						}
	   					}
	   					return data;
	   				 }
	   			},*/
	   			{ "data": "isContact",
	   	      	    "render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("N"==data){
	   							data = "No";
	   						} else if("Y"==data){
	   							data = "Yes";
	   						}
	   					}
	   					return data;
	   				 }
	   			},
	   			{ "data":  null,
	   				"render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("Standard" == row.groupType){
	   							//data = '<div class="text-right"><a href="#" class="btn btn-xs btn-primary" data-original-title="Edit" ><i class="fa fa-edit info"></i></a><a class="btn btn-xs btn-secondary btn-danger" href="#" data-original-title="Remove"><i class="fa fa-times red"></i></a></div>';
	   							data = '<div class="text-right"></div>';
	   						} else{
	   							var tagId = row.tagId;
	   							data = '<div class="text-right"><a href="editTemplateTagConfig?tagId='+row.tagId+'&groupId='+row.customFieldGupId+'" id="edit_'+row.tagId+'" class="btn btn-xs btn-primary tooltips" data-original-title="Edit" ><i class="fa fa-edit info"></i></a><a class="btn btn-xs btn-secondary btn-danger" id="remove_'+row.tagId+'" data-toggle="confirmation" href="javascript:removeTagConfig(\''+tagId+'\');" alt="Remove tag configuration" title="Are you sure? Do you want to Remove"><i class="fa fa-times red"></i></a></div>';
	   						}
	   					}
	   					return data;
	   				 }
	   			 }
	   	        
	   	      ]
   	   });
   	   $("#loader").hide();
   	}
   	

</script>
<!-- Delete Modal HTML -->
<#-- 
<div id="deleteModal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <form id="deleteTagConfig" name="deleteTagConfig" action="<@ofbizUrl>deleteTemplateTagConfiguration</@ofbizUrl>" method="post">
            <input type="hidden" id="tagId" name="tagId">
            <div class="modal-header">
               <h4 class="modal-title">Are you sure you want to delete?</h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div>
            </div>
            <div class="modal-footer">
               <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
               <input type="submit" class="btn btn-danger" value="Delete">
            </div>
         </form>
      </div>
   </div>
</div> -->
<script>
   function removeTagConfig(tagId){
      $('#tagId').val(tagId);
       $("#deleteTagConfig").submit();
   }
   /*
   $("#deleteModal").on("show.bs.modal", function(e) {
     var tagId = $(e.relatedTarget).data('target-id');
     $("#deleteTagConfig input[name=tagId]").val(tagId)
   }); */
</script>