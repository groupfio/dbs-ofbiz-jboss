<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
   <h1 class="float-left">
      <#if financialMetricConfigGV?exists && financialMetricConfigGV.financialConfigId?has_content>
         ${uiLabelMap.updateFinancialMetricConfiguration!} -  ${financialMetricConfigGV?if_exists.financialConfigName?if_exists} (${financialMetricConfigGV?if_exists.financialConfigId?if_exists} )
      <#else>
         ${uiLabelMap.createFinancialMetricConfiguration!}
      </#if>
   </h1>
   <#-- <div class="float-right">
      <a class="btn btn-xs btn-primary" href="<@ofbizUrl>financialMetricConfig</@ofbizUrl>">${uiLabelMap.createNew}</a>
   </div>-->
</div>
<#if financialMetricConfigGV?exists && financialMetricConfigGV.financialConfigId?has_content>
   <form method="post" action="<@ofbizUrl>updateFinancialMetricConfig</@ofbizUrl>" id="createOrUpdateFinancialMetricConfig" class="form-horizontal" name="createOrUpdateFinancialMetricConfig" novalidate="novalidate" data-toggle="validator" onsubmit="return configValidation();">
   <input type="hidden" name="financialConfigId" value="${financialMetricConfigGV?if_exists.financialConfigId?if_exists}"/>
<#else>
   <form method="post" action="<@ofbizUrl>createFinancialMetricConfig</@ofbizUrl>" id="createOrUpdateFinancialMetricConfig" class="form-horizontal" name="createOrUpdateFinancialMetricConfig" novalidate="novalidate" data-toggle="validator" onsubmit="return configValidation();">
</#if>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <@generalInput 
         id="financialConfigName"
         name="financialConfigName"
         label=uiLabelMap.name
         placeholder=uiLabelMap.name
         value="${financialMetricConfigGV?if_exists.financialConfigName?if_exists}"
         required=true
         />
         <#assign startDate = ""/>
         <#assign endDate = ""/>
         <#if financialMetricConfigGV?exists && financialMetricConfigGV.financialConfigId?has_content>
            <#assign startDate = financialMetricConfigGV.financialConfigFromDate?string["dd-MM-yyyy"]/>
            <#assign endDate = financialMetricConfigGV.financialConfigThruDate?string["dd-MM-yyyy"]/>
         </#if>
         <@inputDate 
         id="startDate"
         label=uiLabelMap.startDate
         value="${startDate?if_exists}"
         placeholder="DD-MM-YYYY"
         required = true
         default=true
         />
         
         <@inputDate 
         id="endDate"
         label=uiLabelMap.endDate
         value="${endDate?if_exists}"
         placeholder="DD-MM-YYYY"
         required = true
         default=true
         />

         <#assign disabled = false/>
         <#if financialMetricConfigGV?exists && financialMetricConfigGV.financialConfigId?has_content>
            <#assign disabled = true/>
         </#if>
         <@dropdownInput 
            id="financialConfigType"
            name="financialConfigType"
            label=uiLabelMap.type
            options=campaignTypeList
            required=true
            value="${financialMetricConfigGV?if_exists.financialConfigType?if_exists}"
            allowEmpty=true
            dataLiveSearch=true
            disabled=disabled
            />
         <div class="form-group row">
           <label class="col-sm-4 col-form-label text-danger">${uiLabelMap.cost}</label>
             <div class="col-sm-7">
                 <input type="number" class="form-control input-sm" placeholder="$" name="costPerValue" id="costPerValue" value="${financialMetricConfigGV?if_exists.costPerValue?if_exists}" required step="0.01">
                 <div class="help-block with-errors"></div>
             </div>
         </div>
      </div>
   </div>
   <div class="col-md-12 col-sm-12">
   <div class="form-group row">
   <div class="offset-sm-2 col-sm-2">
     <#if financialMetricConfigGV?exists && financialMetricConfigGV.financialConfigId?has_content>
       <input type="submit" class="btn btn-sm btn-primary mt" value="${uiLabelMap.update}"/>
       <a class="btn btn-sm btn-secondary mt" href="/campaign/control/financialMetricConfig">${uiLabelMap.cancel}</a>
     <#else>
       <input type="submit" class="btn btn-sm btn-primary mt" value="${uiLabelMap.submit}"/>
       <button type="reset" class="btn btn-sm btn-secondary mt-2">Clear</button>
     </#if>
   </div>
   </div>
   </div>
</form>
<div class="table-responsive">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>${uiLabelMap.configuration}</th>
            <th>${uiLabelMap.type}</th>
            <th>${uiLabelMap.cost}</th>
            <th>${uiLabelMap.startDate}</th>
            <th>${uiLabelMap.endDate}</th>
            <th>${uiLabelMap.expired}</th>
            <th>${uiLabelMap.action}</th>
         </tr>
      </thead>
      <tbody>
        <#if financialMetricConfigList?exists && financialMetricConfigList?has_content>
         <#list financialMetricConfigList as financialMetricConfig>
         <tr>
            <td>${financialMetricConfig.financialConfigName?if_exists} (${financialMetricConfig.financialConfigId?if_exists})</td>
            <td>
            <#assign costPerCampaign = delegator.findOne("CampaignType", {"campaignTypeId",financialMetricConfig?if_exists.financialConfigType?if_exists?string}, false)>
            ${costPerCampaign.description?if_exists}
            </td>
            <td>${financialMetricConfig.costPerValue?if_exists}</td>
            <td>${financialMetricConfig?if_exists.financialConfigFromDate?if_exists?string["dd-MM-yyyy"]}</td>
            <td>${financialMetricConfig?if_exists.financialConfigThruDate?if_exists?string["dd-MM-yyyy"]}</td>
            <td>
            <#if financialMetricConfig?exists && financialMetricConfig.isExpired?has_content && financialMetricConfig.isExpired=="Y">
            Yes
            <#else>
            No
            </#if>
            </td>
            <td>
            <div class="">
               <a href="financialMetricConfig?financialConfigId=${financialMetricConfig.financialConfigId?if_exists}" class="btn btn-xs btn-primary tooltips <#if financialMetricConfig?if_exists.isExpired?if_exists=="Y">disabled</#if>" data-original-title="${uiLabelMap.edit}"><i class="fa fa-pencil info"></i></a>
               <a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="removeFinancialMetricsConfig?financialConfigId=${financialMetricConfig.financialConfigId?if_exists}" data-original-title="${uiLabelMap.delete}"><i class="fa fa-times red"></i></a>
            </div>
            </td>
         </tr>
         </#list>
       </#if>
      </tbody>
   </table>
</div>
<script>
   $(document).ready(function () {
     $('#example').dataTable({
           "order": [[ 4, "desc" ]]
     });
   });
   
   $('.decimalValidation').keypress(function (e) {
    var character = String.fromCharCode(e.keyCode)
    var newValue = this.value + character;
    if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
        e.preventDefault();
        return false;
    }
   });

   function hasDecimalPlace(value, x) {
     var pointIndex = value.indexOf('.');
     return  pointIndex >= 0 && pointIndex < value.length - x;
   }
   
   $("#startDate").focus(function() {}).blur(function() {
     return startDateValidation();
   });

  $("#endDate").focus(function() {}).blur(function() {
    return endDateValidation();
  });
  
  function startDateValidation() {
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    if (startDate != null && startDate != "" && endDate != null && endDate != "") {
        if(process(endDate) <= process(startDate)){
            $("#startDate_error").empty();
            $("#startDate_error").append('<ul class="list-unstyled"><li>Start date cannot be after end date</li></ul>');
            return false;
        } else {
            $("#startDate_error").empty();
            $("#endDate_error").empty();
            return true;
        }
    }
    return true;
  }
  
  function endDateValidation() {
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var endDateLen = $("#endDate").val().trim().length;
    if ((startDate == null || startDate == "") && (endDateLen > 0)) {
        $("#startDate_error").empty();
        $("#startDate_error").append('<ul class="list-unstyled"><li>Please select start date</li></ul>');
        return false;
    } else if ((startDate != null || startDate != "") && (endDate != null || endDate != "")) {
        $("#endDate_error").empty();
        if(process(endDate) <= process(startDate)){
            $("#endDate_error").empty();
            $("#endDate_error").append('<ul class="list-unstyled"><li>End date cannot be before start date</li></ul>');
            return false;
        } else {
            $("#startDate_error").empty();
            $("#endDate_error").empty();
            return true;
        }
    }
    return true;
  }
  
  function process(date) {
    var parts = date.split("-");
    return new Date(parts[2], parts[1] - 1, parts[0]);
  }
  
  function configValidation() {
    var startDateValidate = startDateValidation();
    if (!startDateValidate) {
        return false;
    }
    var endDateValidate = endDateValidation();
    if (!endDateValidate) {
        return false;
    }
    return true;
  }
</script>
