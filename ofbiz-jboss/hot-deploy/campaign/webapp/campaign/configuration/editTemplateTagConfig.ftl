<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#-- <#assign extra>
	<a href="<@ofbizUrl>findTemplateTagConfig</@ofbizUrl>" class="btn btn-xs btn-primary">Back</a>
</#assign> -->
<@sectionHeader title="${uiLabelMap.editPersonalisationFieldTagging!}" extra=extra />
<#assign tagId = requestParameters.tagId?if_exists />

<#assign tagConfigurationData = delegator.findOne("DataTagConfiguration", {"tagId" : tagId}, true)?if_exists>
<#if tagConfigurationData?has_content>
<form method="post" action="<@ofbizUrl>updateTemplateTagConfiguration</@ofbizUrl>" id="updateTemplateTag" name="updateTemplateTag" class="form-horizontal" novalidate="novalidate" data-toggle="validator">
	<input type="hidden" name="tagId" id="tagId" value="${tagId?if_exists}"/>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <#assign customFieldGroupingCode = delegator.findOne("CustomFieldGroupingCode", {"customFieldGroupingCodeId" : tagConfigurationData.groupingCode?if_exists}, false)?if_exists />
         <#assign customFieldGroup = delegator.findOne("CustomFieldGroup", {"groupId" : tagConfigurationData.customFieldGroupId?if_exists}, false)?if_exists />
         <#assign customField = delegator.findOne("CustomField", {"customFieldId" : tagConfigurationData.customFieldId?if_exists}, false)?if_exists />
         <#if tagConfigurationData?exists && tagConfigurationData.templateTagTypeId?has_content>
     		<#assign templateTagType = delegator.findOne("TemplateTagType",{"templateTagTypeId":tagConfigurationData.templateTagTypeId},false)?if_exists />
     		<#if templateTagType?has_content>
     			<#assign groupTypeDes = templateTagType.description?if_exists />
     		</#if>
         </#if>
         
         <@displayRow 
            label=uiLabelMap.groupType
            value=groupTypeDes
            />
         
         <#if tagConfigurationData.templateTagTypeId == "CUSTOM_FIELD">
            <@displayRow 
	            label=uiLabelMap.attributeGroup
	            value=customFieldGroup.groupName
	            />
	            
	        <@displayRow 
	            label=uiLabelMap.attributeField
	            value=customField.customFieldName
	            />
         </#if>
         
         <#if tagConfigurationData.templateTagTypeId == "SEGMENTATION">
            <@displayRow 
	            label=uiLabelMap.segmentGroupingCode
	            value=customFieldGroupingCode.groupingCode
	            />
            <@displayRow 
	            label=uiLabelMap.segmentGroup
	            value=customFieldGroup.groupName
	            />
	        <#--         
	        <@displayRow 
	            label=uiLabelMap.segmentValue
	            value=customField.customFieldName
	            /> -->
         </#if>
         
         
         <#if tagConfigurationData.templateTagTypeId == "ECONOMIC_METRIC">
            <@displayRow 
	            label=uiLabelMap.economicGroupingCode
	            value=customFieldGroupingCode.groupingCode
	            />
            <@displayRow 
	            label=uiLabelMap.economicMetric
	            value=customFieldGroup.groupName
	            />
	        <@displayRow 
	            label=uiLabelMap.economicValue
	            value=customField.customFieldName
	            />
         </#if>
         
         
		 
		 <@displayRow 
	            label=uiLabelMap.tagId
	            value=tagConfigurationData.tagId
	            />
	            
         <@generalInput 
				id = "tagName"
				label = uiLabelMap.tagName
				placeholder = uiLabelMap.tagName
				value = tagConfigurationData.tagName
				dataError="Please enter tag name"
				required = true
				/>
				
		<@generalInput 
				id = "groupSequenceNo"
				label = "Group Sequence No"
				placeholder = "Group Sequence No"
				value = tagConfigurationData.groupSequenceNo
				inputType = "number"
				min=0
				/>
				
		<@generalInput 
				id = "fieldSequenceNo"
				label = "Value/Field Sequence No"
				placeholder = "Value/Field Sequence No"
				value = tagConfigurationData.fieldSequenceNo
				inputType = "number"
				min=0
				/>
				
		<#if tagConfigurationData.fromDate?exists>
           <#assign fromDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(tagConfigurationData.fromDate?if_exists, "dd-MM-yyyy")?if_exists/>
          </#if>
		<@inputDate 
               id="fromDate"
               label=uiLabelMap.startDate
               value=fromDate
               required = false
               default=true
               />
        <#if tagConfigurationData.thruDate?exists>
        	<#assign thruDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(tagConfigurationData.thruDate?if_exists, "dd-MM-yyyy")?if_exists/>
        </#if>
        <@inputDate 
               id="thruDate"
               label=uiLabelMap.endDate
               value=thruDate
               required = false
               default=true
               />
		<#--<@dropDown 
				id = "isEnabled"
				label = uiLabelMap.display
				options = flagMap
				value = tagConfigurationData.isEnabled
				allowEmpty=false
				dataLiveSearch = true
				/>-->
		<@dropDown 
				id = "isContact"
				label = "Use For Contact"
				options = flagMap
				value = tagConfigurationData.isContact?if_exists
				allowEmpty=false
				dataLiveSearch = true
				/>		
      </div>
      </div>
      <div class="col-md-12 col-sm-12">
	     <@fromActions showCancelBtn=true cancelUrl="findTemplateTagConfig" />
	  </div>
   
   
</form>

<div class="clearfix"> </div>
<div class="clearfix"> </div>
<@sectionTitle title="${uiLabelMap.tagsList!}" />
<div class="table-responsive">
   <table class="table table-striped" id="tagdatatable">
      <thead>
         <tr>
         	<th>Group Type</th>
            <th>Group</th>
            <th>Attribute Field/ Segment Code</th>
            <th>Tag ID</th>
            <th>Tag Name</th>
            <th>From Date</th>
            <th>Thru Date</th>
            <#--<th>Display</th>-->
            <th>Use For Contact</th>
            <th class="text-right">Action</th>
         </tr>
      </thead>
   </table>
</div>
</#if>
<#assign groupId = requestParameters.groupId?if_exists />
<script type="text/javascript">

$(function() {
   var templateTagTypeId = '';
   var groupId = "${groupId?if_exists}";
   $("#loader").show();
   //var url = "<@ofbizUrl>getTemplateTagConfigList?templateTagTypeId="+templateTagTypeId+"&groupId="+groupId+"</@ofbizUrl>";
   var url = "<@ofbizUrl>getTemplateTagConfigList</@ofbizUrl>";
   $('#tagdatatable').DataTable( {
   	    "processing": true,
   	    "serverSide": true,
   	    "destroy": true,
   	    "fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	},
   	    //"filter" : false,
   	    "ajax": {
               "url": url,
               "type": "POST",
               'data': {
		           templateTagTypeId: templateTagTypeId,
		           groupId: groupId
		        },
           },
           "Paginate": true,
	   		"language": {
	   			"emptyTable": "No data available in table",
	   			"info": "Showing _START_ to _END_ of _TOTAL_ entries",
	   			"infoEmpty": "No entries found",
	   			"infoFiltered": "(filtered1 from _MAX_ total entries)",
	   			"lengthMenu": "Show _MENU_ entries",
	   			"zeroRecords": "No matching records found",
	   			"oPaginate": {
	   				"sNext": "Next",
	   				"sPrevious": "Previous"
	   			}
	   		},
	   		"order": [],
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4,5,6]}],
            "pageLength": 10,
            "bAutoWidth":false,
            "stateSave": true,
            "columns": [
            	{ "data": "groupType"},
            	{ "data": "customFieldGroupId" },
	   	        { "data": "customFieldId" },
                { "data": "tagId"},
	   	        { "data": "tagName" },
	   	        { "data": "fromDate" },
	   	        { "data": "thruDate" },
	   	        /*{ "data": "isEnabled",
	   	      	    "render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("N"==data){
	   							data = "No";
	   						} else if("Y"==data){
	   							data = "Yes";
	   						}
	   					}
	   					return data;
	   				 }
	   			},*/
	   			{ "data": "isContact",
	   	      	    "render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("N"==data){
	   							data = "No";
	   						} else if("Y"==data){
	   							data = "Yes";
	   						}
	   					}
	   					return data;
	   				 }
	   			},
	   			{ "data":  null,
	   				"render": function(data, type, row, meta){
	   					if(type === 'display'){
	   						if("Standard" == row.groupType){
	   							data = '<div class="text-right"><a href="#" class="btn btn-xs btn-primary" data-original-title="Edit" ><i class="fa fa-edit info"></i></a><a class="btn btn-xs btn-secondary btn-danger" href="#" data-original-title="Remove"><i class="fa fa-times red"></i></a></div>';
	   						} else{
	   							var tagId = row.tagId;
	   							data = '<div class="text-right"><a href="editTemplateTagConfig?tagId='+row.tagId+'&groupId='+row.customFieldGupId+'" id="edit_'+row.tagId+'" class="btn btn-xs btn-primary tooltips" data-original-title="Edit" ><i class="fa fa-edit info"></i></a><a class="btn btn-xs btn-secondary btn-danger" id="remove_'+row.tagId+'" data-toggle="confirmation" href="javascript:removeTagConfig(\''+tagId+'\');" alt="Remove tag configuration" title="Are you sure? Do you want to Remove"><i class="fa fa-times red"></i></a></div>';
	   							//data = '<a class="btn btn-xs btn-secondary btn-danger tooltips" data-toggle="confirm-m-box" id="remove_'+row.tagId+'" href="javascript:removeTagConfig(\''+tagId+'\');" alt="Remove tag configuration" title="Are you sure?Do you want to Remove">Remove</a>';
	   						}
	   					}
	   					return data;
	   				 }
	   			 }
	   	        
	   	      ]
   	   });
   $("#loader").hide();
});

   
$(':reset').on('click', function(evt) {
    evt.preventDefault()
    $form = $(evt.target).closest('form')
    $form[0].reset()
    
    //$form.find('select').selectpicker('render')
    $form.find('select').dropdown({
		clearable: true
	});
    
    $('#templateTagTypeId').val('').trigger('change');
});
</script>
<!-- Delete Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="deleteTagConfig" name="deleteTagConfig" action="<@ofbizUrl>deleteTemplateTagConfiguration</@ofbizUrl>" method="post">
				<input type="hidden" id="tagId" name="tagId">
				<div class="modal-header">						
					<h4 class="modal-title">Are you sure you want to delete?</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-danger" value="Delete">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$("#deleteModal").on("show.bs.modal", function(e) {
	var tagId = $(e.relatedTarget).data('target-id');
	$("#deleteTagConfig input[name=tagId]").val(tagId)
});
</script>

