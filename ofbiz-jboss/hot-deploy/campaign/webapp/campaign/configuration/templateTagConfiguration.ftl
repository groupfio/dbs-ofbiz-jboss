<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<@sectionHeader title="${uiLabelMap.createPersonalisationFieldTagging!}" />
<#assign tagId = requestParameters.tagId?if_exists />
<#assign tagConfigurationData = delegator.findOne("DataTagConfiguration", {"tagId" : tagId}, true)?if_exists>
<form method="post" action="<@ofbizUrl>createTemplateTagConfiguration</@ofbizUrl>" id="createTemplateTag" class="form-horizontal" name="createTemplateTag" novalidate="novalidate" data-toggle="validator">
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         
         <@dropDown 
				id = "templateTagTypeId"
				label = uiLabelMap.tagType
				options = templateTagTypes
				value = ""
				allowEmpty=true
				dataError="Please select tag type"
				dataLiveSearch = true
				required = true
				/>
				
		<div id="CUSTOM_FIELD" style="display:none;">
			
		<@dropDown 
				id = "attributeGroup"
				label = uiLabelMap.attributeGroup
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select attribute group"
				dataLiveSearch = true
				/>
				
		<@dropDown 
				id = "attributeField"
				label = uiLabelMap.attributeField
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select attribute field"
				dataLiveSearch = true
				/>
		</div>
		
		<div id="SEGMENTATION" style="display:none;"> 
         <@dropDown 
				id = "segmentGroupingCode"
				label = uiLabelMap.segmentGroupingCode
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select segment grouping code"
				dataLiveSearch = true
				/>   	
         <@dropDown 
				id = "segmentCode"
				label = uiLabelMap.segmentCode
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select segment code"
				dataLiveSearch = true
				/>
				
		<#-- <@dropDown 
				id = "segmentValue"
				label = uiLabelMap.segmentValue
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select segment value"
				dataLiveSearch = true
				/> -->
		</div>
		
		<div id="ECONOMIC_METRIC" style="display:none;">
		<@dropDown 
				id = "economicGroupingCode"
				label = uiLabelMap.economicGroupingCode
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select economic grouping code"
				dataLiveSearch = true
				/>   	
				
		<@dropDown 
				id = "economicMetric"
				label = uiLabelMap.economicMetric
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select economic metric"
				dataLiveSearch = true
				/>
				
		<@dropDown 
				id = "economicValue"
				label = uiLabelMap.economicValue
				labelClass = "text-danger"
				value = ""
				allowEmpty=true
				dataError="Please select economic value"
				dataLiveSearch = true
				/>
		</div>
		
         <@generalInput 
				id = "tagId"
				label = uiLabelMap.tagId
				placeholder = uiLabelMap.tagId
				value = ""
				pattern="^[a-zA-Z0-9._]*$"
				dataError="You can use only letters,numbers and underscores ('_')"
				required = true
				/>
				
         <@generalInput 
				id = "tagName"
				label = uiLabelMap.tagName
				placeholder = uiLabelMap.tagName
				value = ""
				required = true
				dataError="Please enter tag name"
				/>
				
		<@generalInput 
				id = "groupSequenceNo"
				label = uiLabelMap.groupSequenceNo
				placeholder = uiLabelMap.groupSequenceNo
				value = ""
				inputType = "number"
				min=0
				/>
				
		<@generalInput 
				id = "fieldSequenceNo"
				label = uiLabelMap.fieldSequenceNo
				placeholder = uiLabelMap.fieldSequenceNo
				value = ""
				inputType = "number"
				min=0
				/>
				
		
		
		<@inputDate 
               id="fromDate"
               label=uiLabelMap.startDate
               value=""
               required = false
               default=true
               />
               
        <@inputDate 
               id="thruDate"
               label=uiLabelMap.endDate
               value=""
               required = false
               default=true
               />
		<#--<@dropDown 
				id = "isEnabled"
				label = uiLabelMap.display
				options = flagMap
				value = ""
				allowEmpty=false
				dataLiveSearch = true
				/>-->
		<@dropDown 
				id = "isContact"
				label = "Use For Contact"
				options = flagMap
				value = ""
				allowEmpty=false
				dataLiveSearch = true
				/>
				
		
      </div>
   </div>
   
   <div class="clearfix"></div>
   
   <div class="col-md-12 col-sm-12">
     <@fromActions showClearBtn=true />
   </div>
</form>

   <div class="clearfix"> </div>
   <div class="clearfix"> </div>
   
   <#-- 
   <div class="page-header">
      <h2 class="float-left">Tags List</h2>
   </div>
   <div class="table-responsive">
      <table class="table table-striped" id="tagdatatable">
         <thead>
            <tr>
               <th>Tag ID</th>
               <th>Tag Name	</th>
               <th>Group	</th>
               <th>Attribute Field/ Segment Code</th>
               <th>Display</th>
               <th class="text-right">Action</th>
            </tr>
         </thead>
      </table>
   </div> -->

<script type="text/javascript">

$(function() {
  $('#templateTagTypeId').change(function(){
  	var selectedItem = $(this).val();
  	$("#groupSequenceNo").val('');
  	$('#fieldSequenceNo').val('');
    document.createTemplateTag.tagId.value = "";
    
  	// get the segment and economic grouping list
  	$.post("getGroupingCode",{"templateTagTypeId":selectedItem},function(data){
  			var nonSelectClass = "nonselect";
			var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
			if(data !=null && data !=""){
			   for(var i=0;i<data.length;i++){
			   		options += '<option value="'+data[i].customFieldGroupingCodeId+'" data-id="'+data[i].sequenceNo+'">'+data[i].groupingCode+'</option>';
			   }
			}
		   if(selectedItem == "SEGMENTATION"){
		        $("#segmentGroupingCode").empty();
		   		$("#segmentGroupingCode").append(options);
		   		$("#segmentGroupingCode").dropdown("refresh");
		   		//getCodeOrGroupOrMetric(selectedItem,'');
		   } else if(selectedItem == "ECONOMIC_METRIC"){
		   		$("#economicGroupingCode").empty();
		   		$("#economicGroupingCode").append(options);
		   		$("#economicGroupingCode").dropdown("refresh");
		   		//getCodeOrGroupOrMetric(selectedItem,'');
		   } else if(selectedItem == "CUSTOM_FIELD"){
			   getCodeOrGroupOrMetric(selectedItem,'');
		   }
	});
  	
  	$('#templateTagTypeId').find('option').each(function() {
    	var option = $(this).val();
    	if(option != "" && option != null && option !='undefined'){
    		if(selectedItem == option){
    			if(option == "SEGMENTATION"){
    				$('#segmentGroupingCode').attr("required",true);
    				$('#segmentCode').attr("required",true);
    				//$('#segmentValue').attr("required",true);
    			} 
    			if(option == "CUSTOM_FIELD"){
    				$('#attributeGroup').attr("required",true);
    				$('#attributeField').attr("required",true);
    			}
    			if(option == "ECONOMIC_METRIC"){
    				$('#economicMetric').attr("required",true);
    				$('#economicValue').attr("required",true);
    			}
    			$('#' + option).show();
    		} else{
    			$('#' + option).hide();
    			if(option == "SEGMENTATION"){
    				$('#segmentGroupingCode').attr("required",false);
    				$('#segmentCode').attr("required",false);
    				//$('#segmentValue').attr("required",false);
    			} 
    			if(option == "CUSTOM_FIELD"){
    				$('#attributeGroup').attr("required",false);
    				$('#attributeField').attr("required",false);
    			}
    			if(option == "ECONOMIC_METRIC"){
    				$('#economicMetric').attr("required",false);
    				$('#economicValue').attr("required",false);
    			}
    			
    		}
    	} 
	});
  });
  
  $('#segmentGroupingCode').change(function(){
     var tagType = $('#templateTagTypeId').val();
     var groupingCode = $(this).val();
     getCodeOrGroupOrMetric(tagType,groupingCode);
     document.createTemplateTag.tagId.value = "";
  });
  
  $('#economicGroupingCode').change(function(){
     var tagType = $('#templateTagTypeId').val();
     var groupingCode = $(this).val();
     getCodeOrGroupOrMetric(tagType,groupingCode);
     $('#economicMetric').val('').trigger('change');
     document.createTemplateTag.tagId.value = "";
     
  });
  
  function getCodeOrGroupOrMetric(tagType,groupingCode){
     $.post("getCodeOrGroupOrMetric",{"templateTagTypeId":tagType,"groupingCode":groupingCode},function(data){
		var nonSelectClass = "nonselect";
		var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
		if(data !=null && data !=""){
		   for(var i=0;i<data.length;i++){
		   		options += '<option value="'+data[i].groupId+'" data-id="'+data[i].sequenceNo+'">'+data[i].groupName+'</option>';
		   }
		}
	    if(tagType == "SEGMENTATION"){
	        $("#segmentCode").empty();
	   		$("#segmentCode").append(options);
	   		$("#segmentCode").dropdown("refresh");
	    }
	    if(tagType == "CUSTOM_FIELD"){
	   		$("#attributeGroup").empty();
	   		$("#attributeGroup").append(options);
	   		$("#attributeGroup").dropdown("refresh");
	    }
	    if(tagType == "ECONOMIC_METRIC"){
	   		$("#economicMetric").empty();
	   		$("#economicMetric").append(options);
	   		$("#economicMetric").dropdown("refresh");
	    }
	});
  }
 
  /*
   $('#segmentCode').change(function(){
   		$('#fieldSequenceNo').val('');
   		var tagType = $('#templateTagTypeId').val();
  		var selectedItem = $(this).val();
  		var data_id = $(this).find(':selected').data('id');
  		$("#groupSequenceNo").val(data_id);
  		
		  	$.post("getValuesOrFields",{"group":selectedItem,"groupType":tagType},function(data){
		  		var nonSelectClass = "nonselect";
		  		var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
		  		if(data !=null && data !=""){  
				   for(var i=0;i<data.length;i++){
				   		options += '<option value="'+data[i].customFieldId+'" data-id="'+data[i].sequenceNo+'">'+data[i].customFieldName+'</option>';
				   }
				}
				$("#segmentValue").empty();
				$("#segmentValue").append(options);
				$("#segmentValue").dropdown("refresh");
			});
	}); */
	
	$('#attributeGroup').change(function(){
		var tagType = $('#templateTagTypeId').val();
		$('#fieldSequenceNo').val('');
		document.createTemplateTag.tagId.value = "";
        
  		var selectedItem = $(this).val();
  		var data_id = $(this).find(':selected').data('id');
  		$("#groupSequenceNo").val(data_id);
	  	$.post("getValuesOrFields",{"group":selectedItem,"groupType":tagType},function(data){
	  	    var nonSelectClass = "nonselect";
			var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
	  		if(data !=null && data !=""){
			   for(var i=0;i<data.length;i++){
			   		options += '<option value="'+data[i].customFieldId+'" data-id="'+data[i].sequenceNo+'">'+data[i].customFieldName+'</option>';
			   }
			} 
			$("#attributeField").empty();
			$("#attributeField").append(options);
			$("#attributeField").dropdown("refresh");
		});
	});
	
	$('#economicMetric').change(function(){
		var tagType = $('#templateTagTypeId').val();
		$('#fieldSequenceNo').val('');
  		var selectedItem = $(this).val();
  		document.createTemplateTag.tagId.value = "";
        
  		var data_id = $(this).find(':selected').data('id');
  		$("#groupSequenceNo").val(data_id);
	  	$.post("getValuesOrFields",{"group":selectedItem,"groupType":tagType},function(data){
            var nonSelectClass = "nonselect";
			var options = '<option value="" data-content="<span class='+nonSelectClass+'>Please Select</span>" selected>Please Select</option>';
	  		if(data !=null && data !=""){
			   for(var i=0;i<data.length;i++){
			   		options += '<option value="'+data[i].customFieldId+'" data-id="'+data[i].sequenceNo+'">'+data[i].customFieldName+'</option>';
			   }
			} 
			$("#economicValue").empty();
			$("#economicValue").append(options);
			$("#economicValue").dropdown("refresh");
		});
	});
	
	$('#segmentValue').change(function(){
		var data_id= $(this).find(':selected').data('id');
		$('#fieldSequenceNo').val(data_id);
	});
	$('#attributeField').change(function(){
		var data_id= $(this).find(':selected').data('id');
		$('#fieldSequenceNo').val(data_id);
	});
	
});

/*
$("#loader").show();
var url = "<@ofbizUrl>getTemplateTagConfigList</@ofbizUrl>";
$('#tagdatatable').DataTable( {
	    "processing": true,
	    "serverSide": true,
	    "destroy": true,
	    //"filter" : false,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "Paginate": true,
		"language": {
			"emptyTable": "No data available in table",
			"info": "Showing _START_ to _END_ of _TOTAL_ entries",
			"infoEmpty": "No entries found",
			"infoFiltered": "(filtered1 from _MAX_ total entries)",
			"lengthMenu": "Show _MENU_ entries",
			"zeroRecords": "No matching records found",
			"oPaginate": {
				"sNext": "Next",
				"sPrevious": "Previous"
			}
		},
         "pageLength": 10,
         "bAutoWidth":false,
         "stateSave": true,
         "columns": [
            { "data": "tagId"},
	        { "data": "tagName" },
	        { "data": "customFieldGroupId" },
	        { "data": "customFieldId" },
	        { "data": "isEnabled",
	      	    "render": function(data, type, row, meta){
					if(type === 'display'){
						if("N"==data){
							data = "No";
						} else if("Y"==data){
							data = "Yes";
						}
					}
					return data;
				 }
			},
			{ "data":  null,
				"render": function(data, type, row, meta){
					if(type === 'display'){
						//data = '<div class="text-right"><a href="#" class="btn btn-xs btn-primary tooltips" data-toggle="modal" data-target="#editModal" data-original-title="Edit" ><i class="fa fa-edit info"></i></a><a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="#" data-toggle="modal" data-target="#deleteModal" data-original-title="Remove"><i class="fa fa-times red"></i></a></div>';
						data = '<div class="text-right"><a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="#" data-toggle="modal" data-target-id="'+row.tagId+'" data-target="#deleteModal" data-original-title="Remove"><i class="fa fa-times red"></i></a></div>';
						
					}
					return data;
				 }
			 }
	        
	      ]
	});
$("#loader").hide();
 */
   
    $(':reset').on('click', function(evt) {
        evt.preventDefault()
        $form = $(evt.target).closest('form')
        $form[0].reset()
        
        //$form.find('select').dropdown('render');
        $form.find('select').dropdown({
			clearable: true
		});
        
        $('#templateTagTypeId').val('').trigger('change');
    });
</script>

<!-- Edit Modal HTML -->
<div id="editModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form>
				<div class="modal-header">						
					<h4 class="modal-title">Edit Configuration</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Address</label>
						<textarea class="form-control" required></textarea>
					</div>
					<div class="form-group">
						<label>Phone</label>
						<input type="text" class="form-control" required>
					</div>					
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-info" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Delete Modal HTML -->
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="deleteTagConfig" name="deleteTagConfig" action="<@ofbizUrl>deleteTemplateTagConfiguration</@ofbizUrl>" method="post">
				<input type="hidden" id="tagId" name="tagId">
				<div class="modal-header">						
					<h4 class="modal-title">Are you sure you want to delete?</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-danger" value="Delete">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$("#deleteModal").on("show.bs.modal", function(e) {
    	var tagId = $(e.relatedTarget).data('target-id');
    	$("#deleteTagConfig input[name=tagId]").val(tagId)
 	});
 	
 	$('#attributeField, #segmentCode, #economicValue').change(function(){
     var idValue = $(this).val();
     if(idValue != null && idValue != "") {
        idValue = idValue.replace(/[^a-zA-Z0-9._]/g, "");
        document.createTemplateTag.tagId.value = idValue;
     }
  });
</script>