<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<script>
   var searchRequest = null;
   
   $(function () {
       var minlength = 3;
   
       $("#mailingListId").keyup(function () {
           var that = this,
           value = $(this).val();
           if (value.length >= minlength ) {
               if (searchRequest != null) 
                   searchRequest.abort();
                    searchRequest = $.ajax({
                   type: "GET",
                   url: '<@ofbizUrl>getAutoCompleteProdMailingLists</@ofbizUrl>',
                   data: {
                       'mailingListId' : value
                   },
                   dataType: "text",
                   success: function(json){
                           $.each(JSON.parse(json), function(key, value) { 
                           alert(value);
                              $('#mySelect')
                              .append($("<option></option>")
                              .attr("value",key)
                              .text(value)); 
                           });
                   }
               });
           }
       });
   });
</script>
<#if isCreate>
<form method="post" action="<@ofbizUrl>createRMSCampaign</@ofbizUrl>" id="createMarketingCampaignForm" class="form-horizontal" novalidate="novalidate" data-toggle="validator"  name="createMarketingCampaignForm">
   <input type="hidden" name="statusId" value="MKTG_CAMP_CREATED">
   <input type="hidden" name="templateImageUrl" id="Template_Image_Url">
   <input type="hidden" name="campaignTemplateName" id="campaignTemplateName">
   <input type="hidden" name="isApproved" value="NO">
   <input type="hidden" name="classifyId" id="classifyId" value="${classifyId!}">
   <#if selectedCampaignType?has_content && selectedCampaignType?exists && selectedCampaignType  == "PHONE_CALL">
   <input type="hidden" name="preference" id="preference" value="ONLY_PHONE">
   <#elseif selectedCampaignType?has_content && selectedCampaignType?exists && selectedCampaignType  == "EMAIL">
   <input type="hidden" name="preference" id="preference" value="ONLY_EMAIL">
   </#if>
   <div class="page-header border-b">
      <h1 class="float-left">Create Campaign </h1>
      <div class="float-right">
         <a href="<@ofbizUrl>newCampaigns</@ofbizUrl>" class="btn btn-xs btn-primary m5"> Back</a>
      </div>
   </div>
   <#else>
   <#if marketingCampaign?exists>			
<form method="post" action="<@ofbizUrl>updateRMSCampaign</@ofbizUrl>" id="updateMarketingCampaignForm" class="form-horizontal" novalidate="novalidate" data-toggle="validator"  name="updateMarketingCampaignForm">
   <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}">
   <input type="hidden" name="isApproved" value="${marketingCampaign.isApproved?if_exists}">
   <input type="hidden" name="campaignTemplateName" id="campaignTemplateName">
   <input type="hidden" name="classifyId" id="classifyId" value="${marketingCampaign.classificationType?if_exists}">
   </#if>
   <div class="page-header border-b">
      <h1 class="float-left">${marketingCampaign.campaignName}[${marketingCampaign.marketingCampaignId?if_exists}]</h1>
   </div>
   <div class="page-header mt-1 mb-2 btn-primary border rounded  pl-2 pr-1">
      <h2 class="display-4">
      <span class="mr-2"> # of Contacts: <span class="badge badge-light">${campCountMember?default(0)}</span></span> 
      <span class="mr-2"> RM Approved: <span class="badge badge-light">${rmApproved?default(0)}</span></span> 
      <span class="mr-2">Sent: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.sentMailCount?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Not Opened: <span class="badge badge-light"><#if notOpen?exists> ${notOpen?default(0)}</#if></span></span> 
      <span class="mr-2">Opened: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.openedMailCount?default(0)}<#else>0</#if></span></span>  
      <span class="mr-2">Clicked: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.clickedMailCount?default(0)}<#else>0</#if></span></span>  
      <!--<span class="mr-2">Converted: <span class="badge badge-light">${convertedCount?default(0)}</span></span>--> 
      <span class="mr-2">Unsubscribed: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.unsubscribeCount?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Hard Bounced: <span class="badge badge-light">0</span></span> 
      <span class="mr-2">Soft Bounced: <span class="badge badge-light">0</span></span>   
      <span class="mr-2">Revenue: <span class="badge badge-light">$ 0.00</span></span>
      <span class="mr-2">Blacklisted: <span class="badge badge-light">0</span></span> 
      </h2>
   </div>
   </#if>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6 form-horizontal">
         <#assign typeVal="">
         <#if marketingCampaign?exists>
         <#assign typeVal=marketingCampaign.campaignTypeId?if_exists/>
         <#else>
         <#assign typeVal="${requestParameters.campaignTypeId?if_exists}"/>
         </#if>
         <@generalInput 
         id="campaignName"
         label=uiLabelMap.campaignName
         placeholder=uiLabelMap.campaignName
         value=marketingCampaign?if_exists.campaignName?if_exists
         maxlength=100
         required=true
         />
         <#-- <@textareaInput
         id="product"
         label=uiLabelMap.product
         rows="3"
         placeholder = uiLabelMap.product
         value = marketingCampaign?if_exists.product?if_exists
         maxlength=255
         required = false
         />-->
         <@dropdownInput 
         id="product"
         label=uiLabelMap.product
         options=enumerationProductsList
         required=false
         value=product?if_exists
         allowEmpty=true
         />
         <@dropdownInput 
         id="subProduct"
         label=uiLabelMap.subProduct
         required=false
         value=subProduct?if_exists
         allowEmpty=true
         />
         <#assign continousDisabled = false/>
         <#if continousValidation>
             <#assign continousDisabled = true/>
         </#if>
         <@dropdownInput 
         id="campaignTypeId"
         label=uiLabelMap.deliveryMethod
         options=campaignTypes
         required=true
         value=typeVal?if_exists
         allowEmpty=true
         disabled = continousDisabled
         />
         <div class="form-group row">
         	<#assign contactListExists = "N" />
         	<#if testIdList?exists && testIdList?has_content>
         		<#assign contactListExists ="Y" >
         	<#elseif prodIdList?exists && prodIdList?has_content >
         		<#assign contactListExists ="Y" >
         	</#if>
            <label class="col-sm-4 col-form-label">${uiLabelMap.campaignEmailType}</label>
            <div class="col-sm-7">
               <div class="form-check-inline">
                  <label class="form-check-label"> 
                  	 <input class="form-check-input" name="campaignEmailType" value="SENSITIVE" type="radio" <#if marketingCampaign?if_exists.campaignEmailType?has_content && marketingCampaign?if_exists.campaignEmailType == "SENSITIVE">checked</#if> <#if contactListExists == "Y">disabled</#if> >Sensitive
                  </label>
               </div>
               <div class="form-check-inline">
                  <label class="form-check-label"> 
	                  <input class="form-check-input" name="campaignEmailType" value="NONSENSITIVE" type="radio" 
	                  <#if marketingCampaign?if_exists.campaignEmailType?has_content && marketingCampaign?if_exists.campaignEmailType == "NONSENSITIVE">checked
	                  <#elseif !marketingCampaign?if_exists.campaignEmailType?has_content>checked</#if> <#if contactListExists == "Y">disabled</#if>>Non Sensitive
                  </label>
               </div>
            </div>
         </div>
         <div class="form-group row">
            <label  class="col-sm-4 control-label">${uiLabelMap.roleType}</label>
            <div class="col-sm-7">
               <@simpleDropdownInputZone 
               id="roleTypeId"
               options=roleList
               required=false
               value=marketingCampaign?if_exists.roleTypeId?if_exists
               allowEmpty=true
               emptyText = uiLabelMap.roleType
               />	
            </div>
         </div>
         <div class="form-group row">
            <label  class="col-sm-4 control-label">${uiLabelMap.salesTriggerType}</label>
            <div class="col-sm-7">
               <#if salesTriggerType?exists && "SALES_TRIGGER"=="${salesTriggerType}">
               <@simpleDropdownInputZone 
               id="salesTriggerType"
               options=enumerationSalesTriggerList
               required=false
               value=marketingCampaign?if_exists.salesTriggerType?if_exists
               allowEmpty=true
               disabled=true
               emptyText = uiLabelMap.salesTriggerType
               />
               <#else>
                <@simpleDropdownInputZone 
               id="salesTriggerType"
               options=enumerationSalesTriggerList
               required=false
               value=marketingCampaign?if_exists.salesTriggerType?if_exists
               allowEmpty=true
               emptyText = uiLabelMap.salesTriggerType
               />
               </#if>	
            </div>
         </div>
         <#-- 
         <div class="form-group row">
            <label class="col-sm-4 col-form-label">${uiLabelMap.costPerAction}</label>
            <div class="col-sm-7">
               <div class="input-group">
                  <input type="hidden" class="form-control input-sm" name="costPerAction" value="${campFinancialValues?if_exists.financialConfigId?if_exists}" id="costPerAction">
                  <input type="text" class="form-control input-sm" name="costPerActionValue" value="" id="costPerActionValue" placeholder="Select Cost Configuration">
                  <span class="input-group-addon ">
                  <span class="glyphicon glyphicon-list-alt"
                     data-toggle="modal" data-target="#reassign1" id="findConfig">
                  </span>                  
                  </span>
               </div>
            </div>
         </div>
         -->
         <div class="form-group row">
            <label class="col-sm-4 col-form-label">${uiLabelMap.parentCampaign}</label>
            <div class="col-sm-7">
               <div class="input-group">
                  <input type="text" class="form-control input-sm" name="parentCampaignId" value="${marketingCampaignParent?if_exists.campaignName?if_exists}<#if marketingCampaignParent?if_exists.marketingCampaignId?exists> (${marketingCampaignParent?if_exists.marketingCampaignId?if_exists})</#if>" id="parentCampaignId" placeholder="${uiLabelMap.parentCampaign}">
                  <span class="input-group-addon ">
                  <span class="glyphicon glyphicon-list-alt"
                     data-toggle="modal" data-target="#parentMktCampaign" id="findConfig4">
                  </span>                  
                  </span>
               </div>
            </div>
         </div>
         <div class="form-group row">
            <label class="col-sm-4 col-form-label">${uiLabelMap.masterCampaign}</label>
            <div class="col-sm-7">
               <div class="input-group">
                  <input type="text" class="form-control input-sm" name="masterCampaignId" value="${marketingCampaignMaster?if_exists.campaignName?if_exists}<#if marketingCampaignMaster?if_exists.marketingCampaignId?exists> (${marketingCampaignMaster?if_exists.marketingCampaignId?if_exists})</#if>" id="masterCampaignId" placeholder="${uiLabelMap.masterCampaign}">
                  <span class="input-group-addon ">
                  <span class="glyphicon glyphicon-list-alt"
                     data-toggle="modal" data-target="#masterMktCamp" id="findConfig5">
                  </span>                  
                  </span>
               </div>
            </div>
         </div>
         <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">${uiLabelMap.campaignTemplate}</label>
            <div class="col-sm-7">
               <div class="input-group">
                  <input type="text" class="form-control input-sm" value="${marketingCampaign?if_exists.campaignTemplateName?if_exists}<#if marketingCampaign?if_exists.campaignTemplateId?exists> (${marketingCampaign?if_exists.campaignTemplateId?if_exists})</#if>" id="mergeFormId"  name="mergeFormId" placeholder="Select Campaign Template">
                  <span class="input-group-addon ">
                  <span class="glyphicon glyphicon-list-alt"
                     data-toggle="modal" data-target="#findTemplates" id="findTemp">
                  </span>                  
                  </span>
               </div>
            </div>
         </div>
		<#include "component://campaign/webapp/campaign/campaigns/multipleTemplate.ftl"/>
       <div class="form-group row">
         <label class="col-sm-4 col-form-label">${uiLabelMap.RmBcc}</label>
           <div class="col-sm-7">
             <div class="input-group">
              <input type="radio" name="rmBccStatus" id="rmBccStatus1" value="Y"  
              <#if marketingCampaign?if_exists.rmBccStatus?has_content && marketingCampaign?if_exists.rmBccStatus == "Y">checked</#if>><label class="col-sm-2 col-form-label">Yes</label>
              <input type="radio" name="rmBccStatus" id="rmBccStatus2" value="N"  
              <#if marketingCampaign?if_exists.rmBccStatus?has_content && marketingCampaign?if_exists.rmBccStatus == "N">checked</#if>><label class="col-sm-2 col-form-label">No</label>
             </div>
           </div>
       </div>
         <#-- 
         <div class="form-group row">
            <label  class="col-sm-4 control-label">${uiLabelMap.timeZone}</label>
            <div class="col-sm-7">
               <@simpleDropdownInputZone 
               id="timeZoneCamp"
               options=timeZoneList
               required=false
               value=marketingCampaign?if_exists.timeZoneId?if_exists
               allowEmpty=true
               emptyText = uiLabelMap.timeZone
               />	
            </div>
         </div>
         -->
         <#assign curTime = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(currentTimestamp, "dd-MM-yyyy")/>
         <#if marketingCampaign?exists>
         <#if marketingCampaign.startDate?exists>
         <#assign startTime = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(marketingCampaign.startDate?if_exists, "dd-MM-yyyy")?if_exists/>
         </#if>
         <#if marketingCampaign.endDate?exists>
         <#assign endTime = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(marketingCampaign.endDate?if_exists, "dd-MM-yyyy")?if_exists/>
         </#if>
         </#if>
         <#--
         <div class="form-group row">
            <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.startDate}</label>
            <div class="col-sm-7">
               <div class="input-group date" id="datetimepicker7">
                  <input type='text' class="form-control input-sm" name="fromDate" id="fromDate" data-date-format="DD-MM-YYYY" value="<#if marketingCampaign?exists>${startTime?if_exists}<#else>${curTime?if_exists}</#if>"/>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
               </div>
               <div class="help-block with-errors" id="fromDate_error"></div>
            </div>
         </div>
         -->
         <div class="form-group row">
            <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.startDate}</label>
            <div class="col-sm-7">
               <div class="input-group date" id="datetimepicker7">
                  <input type='text' class="form-control input-sm" name="fromDate" id="fromDate" data-date-format="DD-MM-YYYY" value="<#if marketingCampaign?exists>${startTime?if_exists}<#else>${curTime?if_exists}</#if>" />
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
               </div>
               <div class="help-block with-errors" id="fromDate_error"></div>
            </div>
         </div>
         <div class="form-group row">
            <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.endDate}</label>
            <div class="col-sm-7">
               <div class="input-group date" id="datetimepicker8">
                  <input type='text' class="form-control input-sm" name="thruDate" id="thruDate" data-date-format="DD-MM-YYYY" value="<#if marketingCampaign?exists>${endTime?if_exists}<#else>${curTime?if_exists}</#if>" />
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
               </div>
               <div class="help-block with-errors" id="endDate_error"></div>
            </div>
         </div>
         <@textareaInput
         id="campaignSummary"
         label=uiLabelMap.description
         rows="3"
         placeholder = uiLabelMap.description
         value = marketingCampaign?if_exists.campaignSummary?if_exists
         required = false
         />
         <@textareaInput
         id="objectives"
         label=uiLabelMap.objectives
         rows="3"
         placeholder = uiLabelMap.objectives
         value = marketingCampaign?if_exists.objectives?if_exists
         maxlength=255
         required = false
         />
      </div>
      <div class="col-md-6 col-sm-6 form-horizontal">
         <div class="row padding-r">
            <div class="col-md-6 col-sm-6 form-horizontal">
               <#-- <@generalInputTwoRow 
               id="budgetSpend"
               label=uiLabelMap.budgetSpend 
               placeholder="0.00"
               value=campFinancialValues?if_exists.budgetSpendValue?if_exists
               required=false
               inputType="number"
               class="no-spin"
               min=1
               />-->
               <@generalInputTwoRow 
               id="budgetRevenue"
               label=uiLabelMap.targetRevenue
               placeholder="0.00"
               value=campFinancialValues?if_exists.budgetRevenueValue?if_exists
               required=false
               inputType="number"
               dataError=""
               min=0
               class="no-spin"
               />
               <@generalInputTwoRow 
               id="budgetMargin"
               label=uiLabelMap.targetMargin
               placeholder="0.00"
               value=campFinancialValues?if_exists.budgetMarginValue?if_exists
               required=false
               inputType="number"
               min=1
               class="no-spin"
               />
               <div id="emailShow">
                  <@generalInputTwoRow 
                  id="openTarget"
                  label=uiLabelMap.openTarget
                  placeholder="0"
                  value=campFinancialValues?if_exists.openTargetPerc?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  step="0.01"
                  />
                  <@generalInputTwoRow 
                  id="clickTarget"
                  label=uiLabelMap.clickTarget
                  placeholder="0"
                  value=campFinancialValues?if_exists.clickTargetPerc?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  step="0.01"
                  />
               </div>
               <div id="phoneShow">
                  <@generalInputTwoRow 
                  id="contactsMade"
                  label=uiLabelMap.contactsMade
                  placeholder="0"
                  value=campFinancialValues?if_exists.contactsMade?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  />
               </div>
               <@generalInputTwoRow 
               id="conversionTarget"
               label=uiLabelMap.conversionTarget
               placeholder="0"
               value=campFinancialValues?if_exists.conversionTargetPerc?if_exists
               required=false
               inputType="number"
               min=1
               class="no-spin"
               step="0.01"
               />
               <@generalInputTwoRow 
               id="offerCodeCount"
               label=uiLabelMap.offerCodeCount 
               placeholder="0"
               value=marketingCampaign?if_exists.offerCodeCount?if_exists
               required=false
               inputType="number"
               class="no-spin"
               min=1
               />
            </div>
            <#if !isCreate>
            <div class="col-md-6 col-sm-6 form-horizontal">
               <@generalInputTwoRow 
               id="actualSpendValue"
               label=uiLabelMap.actualSpendValue
               placeholder="0.00"
               value=campFinancialValues?if_exists.actualSpendValue?if_exists
               required=false
               inputType="number"
               class="no-spin"
               min=1
               />
               <@generalInputTwoRow 
               id="actualBudgetRevenue"
               label=uiLabelMap.actualBudgetRevenue
               placeholder="0.00"
               value=campFinancialValues?if_exists.actualBudgetRevenue?if_exists
               required=false
               inputType="number"
               min=1
               class="no-spin"
               />
               <@generalInputTwoRow 
               id="actualBudgetMargin"
               label=uiLabelMap.actualBudgetMargin
               placeholder="0.00"
               value=campFinancialValues?if_exists.actualBudgetMargin?if_exists
               required=false
               inputType="number"
               min=1
               class="no-spin"
               />
               <div id="emailActualShow">	
                  <@generalInputTwoRow 
                  id="actualOpenTarget"
                  label=uiLabelMap.actualOpenTarget
                  placeholder="0"
                  value=campFinancialValues?if_exists.actualOpenTarget?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  step="0.01"
                  />
                  <@generalInputTwoRow 
                  id="actualClickTarget"
                  label=uiLabelMap.actualClickTarget
                  placeholder="0"
                  value=campFinancialValues?if_exists.actualClickTarget?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  step="0.01"
                  />
               </div>
               <div id="phoneActualShow">
                  <@generalInputTwoRow 
                  id="actualContactsMade"
                  label=uiLabelMap.actualContactsMade
                  placeholder="0"
                  value=campFinancialValues?if_exists.actualContactsMade?if_exists
                  required=false
                  inputType="number"
                  min=1
                  class="no-spin"
                  />
               </div>
               <@generalInputTwoRow 
               id="actualConversionTarget"
               label=uiLabelMap.actualConversionTarget
               placeholder="0"
               value=campFinancialValues?if_exists.actualConversionTarget?if_exists
               required=false
               inputType="number"
               min=1
               class="no-spin"
               step="0.01"
               />
            </div>
            </#if>
            <div class="clearfix"> </div>
            <div class="col-md-12 col-sm-12 mt-12">   
               <#--@textareaTwoInput
               id="product"
               label=uiLabelMap.product
               rows="3"
               placeholder = uiLabelMap.product
               value = marketingCampaign?if_exists.product?if_exists
               maxlength=255
               required = false
               />-->
               <@textareaTwoInput
               id="measurementOfSuccess"
               label=uiLabelMap.measurementOfSuccess
               rows="3"
               placeholder = uiLabelMap.measurementOfSuccess
               value = marketingCampaign?if_exists.measurementOfSuccess?if_exists
               maxlength=255
               required = false
               />
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"> </div>
   <div class="col-md-12 col-sm-12">
      <div class="form-group row">
         <div class="offset-sm-2 col-sm-9">
            <#if isCreate>
            <button type="submit" onClick="return ValidCaptcha();" class="btn btn-sm btn-primary mt">${uiLabelMap.submit}</button>
            <button type="reset" class="btn btn-sm btn-secondary mt">Clear</button>
            <#else>
            <button type="submit" onClick="return ValidCaptcha();" class="btn btn-sm btn-primary mt">${uiLabelMap.update}</button>
            </#if>  
         </div>
      </div>
   </div>
</form>
<div id="reassign1" class="modal fade" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.financialMetrics}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table id="dtable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.configurationName!}</th>
                     </tr>
                  </thead>
                  <tbody>
                     <#if FinancialMetricConfigList?has_content>
                     <#list FinancialMetricConfigList as FinancialMetricConfig>
                     <tr>
                        <td><a href="#" class="parentSet" onClick="javaScript:getValue(${FinancialMetricConfig?if_exists.costPerValue?if_exists});"><span id="finMetri"  name="finMetri" value ="${FinancialMetricConfig?if_exists.financialConfigName?if_exists} (${FinancialMetricConfig?if_exists.financialConfigId?if_exists})"></span>${FinancialMetricConfig?if_exists.financialConfigName?if_exists}(${FinancialMetricConfig?if_exists.financialConfigId?if_exists})</a></td>
                     </tr>
                     </#list>  
                     </#if>  
                  </tbody>
               </table>
            </div>
            <div class="modal-footer">
               <button type="reset" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--parent campaign modal-->
<div id="reassign3" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.campaigns}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <#--<#include "component://campaign/webapp/campaign/campaigns/findMarketingCampaigns.ftl"/>-->
            <div class="modal-footer">
               <button type="reset" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="reassign5" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.campaignListView}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table id="example" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.campaignNameId!}</th>
                        <th>${uiLabelMap.startDate}</th>
                        <th>${uiLabelMap.endDate}</th>
                     </tr>
                  </thead>
               </table>
            </div>
            <div class="modal-footer">
               <button type="reset" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="reassign4" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.campaignListView}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table id="exampleMaster" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.campaignNameId!}</th>
                        <th>${uiLabelMap.startDate}</th>
                        <th>${uiLabelMap.endDate}</th>
                     </tr>
                  </thead>
                  <tbody>
                     <#if marketingCampaigns?has_content>
                     <#list marketingCampaigns as marketingCampaignsLook>
                     <tr>
                        <td><a href="#" class="campMaster"><span id="marketingMaster" name="marketingMaster" value ="${marketingCampaignsLook?if_exists.campaignName?if_exists} (${marketingCampaignsLook?if_exists.marketingCampaignId?if_exists})"></span>${marketingCampaignsLook?if_exists.campaignName?if_exists}(${marketingCampaignsLook?if_exists.marketingCampaignId?if_exists})</a></td>
                        <td>${marketingCampaignsLook.startDate?if_exists}</td>
                        <td>${marketingCampaignsLook.endDate?if_exists}</td>
                     </tr>
                     </#list>  
                     </#if>  
                  </tbody>
               </table>
            </div>
            <div class="modal-footer">
               <button type="reset" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="findTemplates" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content" id="searchTemplate">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.findTemplates}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row padding-r">
               <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <#-- <div class="col-md-2 col-sm-2 ">
                        <button class="btn btn-sm btn-primary  active" onclick="findTemplates('all')"> Show all</button>
                     </div>
                     <div class="col-md-5 col-sm-5 mr-auto ">
                        <@simpleDropdownInput 
                        id="templateCategory"
                        options=templateCategoryList
                        required=false
                        value=""
                        onchange="javascript:findTemplates();"
                        allowEmpty=true
                        emptyText = uiLabelMap.category
                        dataLiveSearch=true
                        />
                     </div>
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search Templates" id="tempalateName" name="tempalateName">
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Templates" href="javascript:findTemplates('');"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div> -->
                     <div class="col-md-3 col-sm-2">
                         <@simpleInput 
                           id="tempalateName"
                           value=tempName!
                           placeholder="Templates Name"
                           required=false
                         />
                     </div>
                     <@fromSimpleAction id="findTempButton" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table id="tempAjaxdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Template name</th>
                        <th>Subject</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>View Template</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   <#-- $(function(){
     $('#tempalateName').keypress(function(e){
       if(e.which == 13) {
         findTemplates('');
       }
     });
   });
   -->
   $('#findTempButton').on('click',function(){
	  findTemplates();
   });
   $(function(){
     $('#tempalateName').keypress(function(e){
       if(e.which == 13) {
       	findTemplates();
       }
     });
   });
</script>
<div id="parentMktCampaign" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content"  id="searchParentCamp">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.campaignListView}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row padding-r">
               <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search Campaigns" id="campaignNameToFind" name="campaignNameToFind" >
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Campaigns" href="javascript:findCampaignLists();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table id="campAjaxdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Campaign Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(function(){
     $('#searchParentCamp').keypress(function(e){
       if(e.which == 13) {
         findCampaignLists();
       }
     });
   });
</script>
<div id="masterMktCamp" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content" id="searchMasterCamp">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.campaignListView}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row padding-r">
               <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search Campaigns" id="campaignMasterToFind" name="campaignMasterToFind" >
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Campaigns" href="javascript:findCampaignMasterLists();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table id="campMasterAjaxdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Campaign Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(function(){
     $('#searchMasterCamp').keypress(function(e){
       if(e.which == 13) {
         findCampaignMasterLists();
       }
     });
   });
</script>
<div id="segmentSearch" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content" id="segmentSearchId">
         <div class="modal-header">
            <h4 class="modal-title">Find Segment</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row padding-r">
               <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search Campaigns" id="segmentName" name="segmentName" >
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Campaigns" href="javascript:findSegment();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table id="segmentDataTable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Segment Code</th>
                        <th>Segment Value</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(function(){
     $('#segmentSearchId').keypress(function(e){
       if(e.which == 13) {
         findSegment();
       }
     });
   });
</script>


<div id="findMultiTemplates" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content" id="templateSearchId">
         <div class="modal-header">
            <h4 class="modal-title">Find Template</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row padding-r">
               <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search Template" id="searchTempalateName" name="searchTempalateName" >
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Template" href="javascript:findMultiTemplates();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table id="multiTempAjaxtable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Template name</th>
			            <th>Subject</th>
			            <th>Start Date</th>
			            <th>End Date</th>
			            <th>View Template</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(function(){
     $('#templateSearchId').keypress(function(e){
       if(e.which == 13) {
         findMultiTemplates();
       }
     });
   });
</script>
<script>
   function getValue(s){
       $('#costPerActionValue').val(s);
   }
   $('#reassign1').on('click', '.parentSet', function() {
       var value = $(this).children("span").attr("value");
       $('#costPerAction').val(value);
       $('#reassign1').modal('hide');
   });
   
   $('#parentMktCampaign').on('click', '.campParent', function() {
       var value = $(this).children("span").attr("value");
       $('#parentCampaignId').val(value);
       $('#parentMktCampaign').modal('hide');
   });
   $('#masterMktCamp').on('click', '.campMaster', function() {
       var value = $(this).children("span").attr("value");
       $('#masterCampaignId').val(value);
       $('#masterMktCamp').modal('hide');
   });
   $('#findMultiTemplates').on('click', '.tempSet', function() {
    var value = $(this).children("span").attr("value");    
    $("#spec-date-template-display-"+specDateTemplateSelected).val( value );
    $("#spec-date-template-"+specDateTemplateSelected).val( $(this).children('input[name="templateMultiId"]').val() );
    $('#findMultiTemplates').modal('hide');
   });
   $('#segmentSearch').on('click', '.segSet', function() {
   var value = $(this).children("span").attr("value");
   $("#spec-segment-display-"+specSegmentSelected).val( value );
   $("#spec-segment-"+specSegmentSelected).val( $(this).children('input[name="segmentGroupId"]').val() );
   $("#spec-segment-custFieldId"+specSegmentSelected).val( $(this).children('input[name="customFieldId"]').val() );
   $('#segmentSearch').modal('hide');
   });
   
   
   $(document).ready(function(){
   	//findTemplates('');
   	localStorage.clear();
   	findTemplates();
   });
   
   //function findTemplates(isAll){
   function findTemplates(){
   
   	var templateCategories = '';
   	var tempalateName = '';
   		/*if(isAll == "all"){
   		$("#templateCategory").val('default');
   		$("#templateCategory").dropdown("refresh");
   		$("#tempalateName").val('');
   		
   	} else{ */
   		templateCategories = "";//$("#templateCategory").val();
   		tempalateName = $("#tempalateName").val();
   //	}
   	
   	$("#loader").show();
   	/*$.post("findTemplatesAjax",{"templateCategories":,"tempalateName":tempalateName},function(data){
   		$("#loadTemplate").empty();
   			if(data !=null && data !=""){
   			var templatedata = '';
   			   for(var i=0;i<data.length;i++){
   			   		templatedata = templatedata+'<div class="col-md-2 col-sm-2 btn">';
   				    templatedata = templatedata+'<a href="<@ofbizUrl>viewTemplate?templateId='+data[i].templateId+'</@ofbizUrl>">';
   				    templatedata = templatedata+'<div class="content border rounded">';
   				    templatedata = templatedata+'<img src="'+data[i].uploadThumbnail+'" alt="'+data[i].templateName+'" style="width:100%">';
   				    templatedata = templatedata+'<p class="pt-2 font-weight-bold">'+data[i].templateName+'('+data[i].templateId+')</p>';
   				    templatedata = templatedata+'</div></a></div>';
   			   }
   			   $("#loadTemplate").append(templatedata);
   			}
   	}); */
   	
   	var url = "findTemplatesAjax?templateCategories="+templateCategories+"&tempalateName="+tempalateName;
   	$('#tempAjaxdatatable').DataTable( {
   		    "processing": true,
   		    "serverSide": true,
   		    "destroy": true,
   		    "filter" : false,
   		    "ajax": {
   	            "url": url,
   	            "type": "POST"
   	        },
   	        "Paginate": true,
   			"language": {
   				"emptyTable": "No data available in table",
   				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
   				"infoEmpty": "No entries found",
   				"infoFiltered": "(filtered1 from _MAX_ total entries)",
   				"lengthMenu": "Show _MENU_ entries",
   				"zeroRecords": "No matching records found",
   				"oPaginate": {
   					"sNext": "Next",
   					"sPrevious": "Previous"
   				}
   			},
   	         "pageLength": 10,
   	         "bAutoWidth":false,
   	         "stateSave": true,
   	         "columns": [
   	            { "data": "templateName",
   		          "render": function(data, type, row, meta){
   		            if(type === 'display'){
   		                data = '<a class="mergeSet" data-toggle="tooltip" href="#" ">'+row.templateName+' ('+row.templateId+')</a>';
   		            }
   		            return data;
   		         } 
   		      	},
   	            { "data": "subject" },
   	            { "data":  "fromDate"},
   				{ "data":  "thruDate"},
   	            { "data":  null,
   	                orderable: false,
   					"render": function(data, type, row, meta){
   						if(type === 'display'){
   							data = '<a href="<@ofbizUrl>getTemplate?templateId='+row.templateId+'</@ofbizUrl>" class="fa fa-eye btn btn-xs btn-primary" title="View Template" target="_blank"></a>';
   						}
   						return data;
   					 }
   				 }
   				 
   	          ]
   		});
   	$("#loader").hide();
   }
   
   
   function templateSearch(element, id) {
       var value = $(element).val();
       $("#" + id + " div").each(function() {
           if ($(this).text().search(new RegExp(value, "i")) > -1) {
               $(this).css("display","block");
           } else {
               $(this).css("display","none");
           }
       });
   }
   	
   $("#viewTemplate").on("show.bs.modal", function(e) {
       var templateId = $(e.relatedTarget).data('target-id');
       $.get( "getTemplateContent?templateId=" + templateId, function( data ) {
           $("#modal-body").html(data["htmlContent"]);
       });
   
    });      	 
   
   
   $('#findTemplates').on('click', '.mergeSet', function() {
       var value = $(this).text();
       $('#mergeFormId').val(value);
       $('#findTemplates').modal('hide');
   });
   
   
   $("#budgetRevenue").focus(function() {}).blur(function() {
       return budgetCal();
   });
   
   function budgetCal() {
       var spentValue = $("#budgetSpend").val();
       var revenueValue = $("#budgetRevenue").val();
       if (spentValue != null && spentValue != "" && revenueValue != null && revenueValue != "") {
       var spentValueInt = parseInt(spentValue);
       var revenueValueInt = parseInt(revenueValue);
       if(revenueValueInt == 0) {
          $("#budgetRevenue_error").empty();
          return true;
       }
          if (revenueValueInt < spentValueInt) {
             $("#budgetRevenue_error").empty();
             $("#budgetRevenue_error").append('<ul class="list-unstyled"><li>Revenue should be higher than the spend</li></ul>');
             return false;
          } else {
             $("#budgetRevenue_error").empty();
             return true;
          }
       }
       return true;
   }
   
   $("#clickTarget").focus(function() {}).blur(function() {
       return clickTargetCal();
   });
   
   function clickTargetCal() {
       var openTarget = $("#openTarget").val();
       var clickTarget = $("#clickTarget").val();
       if(openTarget != null && openTarget != "" && clickTarget != null && clickTarget != "") {
          if (parseFloat(openTarget) < parseFloat(clickTarget)) {
              $("#clickTarget_error").empty();
              $("#clickTarget_error").append('<ul class="list-unstyled"><li>Click Target cannot be higher than the open target</li></ul>');
              return false;
          } else {
              $("#clickTarget_error").empty();
              return true;
          }
       }
        return true;
   }
   
   
   $("#fromDate").focus(function() {}).blur(function() {
       var fromDate = $("#fromDate").val();
       if (fromDate != null || fromDate != "") {
           $("#fromDate_error").empty();
       }
   });
   
   $("#thruDate").focus(function() {}).blur(function() {
       var fromDate = $("#fromDate").val();
       var thruDate = $("#thruDate").val();
       var thruDateLen = $("#thruDate").val().trim().length;
       if ((fromDate == null || fromDate == "") && (thruDateLen > 0)) {
           $("#fromDate_error").empty();
           $("#fromDate_error").append('<ul class="list-unstyled"><li>Please enter from date</li></ul>');
       } else if ((fromDate != null || fromDate != "") && (thruDate != null || thruDate != "")) {
           $("#endDate_error").empty();
           if (process(thruDate) < process(fromDate)) {
               $("#endDate_error").empty();
               $("#endDate_error").append('<ul class="list-unstyled"><li>End date cannot be before start date</li></ul>');
           }
       }
   });
   
   function ValidCaptcha() {
       var fromDate = $("#fromDate").val();
       var thruDate = $("#thruDate").val();
       var thruDateLen = $("#thruDate").val().trim().length;
       if ((fromDate == null || fromDate == "") && (thruDateLen > 0)) {
           $("#fromDate_error").empty();
           $("#fromDate_error").append('<ul class="list-unstyled"><li>Please enter from date</li></ul>');
           return false;
       } else if ((fromDate != null || fromDate != "") && (thruDate != null || thruDate != "")) {
           $("#endDate_error").empty();
           if (process(thruDate) < process(fromDate)) {
               $("#fromDate_error").empty();
               $("#endDate_error").empty();
               $("#endDate_error").append('<ul class="list-unstyled"><li>End date cannot be before start date</li></ul>');
               return false;
           }
       } else {
           $("#fromDate_error").empty();
           $("#endDate_error").empty();
       }
       var returnedRevenue = budgetCal();
       if (!returnedRevenue) {
           return false;
       }
       var clickTarget = clickTargetCal();
       if (!clickTarget) {
           return false;
       }
   }
   
   $(document).ready(function() {
       var typeId = $("#campaignTypeId").val();
       if ("PHONE_CALL" == typeId) {
           $("#emailShow").hide();
           $("#emailActualShow").hide();
   
       } else if ("EMAIL" == typeId) {
           $("#phoneShow").hide();
           $("#phoneActualShow").hide();
       } else if ("SMS" == typeId) {
           $("#emailShow").hide();
           $("#emailActualShow").hide();
       }
       var costValue = callGetCost(typeId);
       $("#campaignTypeId").change(function() {
           var typeVal = this.value;
           var typeId = typeVal;
           if ("PHONE_CALL" == typeVal) {
               $("#emailShow").hide();
               $("#emailActualShow").hide();
               $("#phoneShow").show();
               $("#phoneActualShow").show();
               var costValue = callGetCost(typeId);
           } else if ("EMAIL" == typeVal) {
               $("#phoneShow").hide();
               $("#phoneActualShow").hide();
               $("#emailShow").show();
               $("#emailActualShow").show();
               var costValue = callGetCost(typeId);
           } else if ("SMS" == typeVal) {
               $("#emailShow").hide();
               $("#emailActualShow").hide();
               var costValue = callGetCost(typeId);
           }
       });
   });
   
   function callGetCost(typeId) {
       var costData = $.ajax({
           type: 'POST',
           async: false,
           url: "<@ofbizUrl>getCostPerAction</@ofbizUrl>",
           data: {
               'typeId': typeId
           },
           dataType: "json",
           success: function(resultData) {
               var costToAssign = resultData.configId;
               var configName = resultData.configName;
               var costValueaj = resultData.costValue;
               if(costToAssign!=null && configName!=null){ 
               $('#costPerAction').val(configName + " " + "(" + costToAssign + ")");
               $('#costPerActionValue').val(parseFloat(costValueaj));
               }else{
                $('#costPerAction').val("");
                $('#costPerActionValue').val("");
               }
               return resultData;
           }
       });
   }
   
   function process(date) {
       var parts = date.split("-");
       return new Date(parts[2], parts[1] - 1, parts[0]);
   }
   $("#example").DataTable();
   $("#exampleMaster").DataTable();
       	 
   
   $(document).ready(function(){
   	localStorage.clear();
   	findCampaignLists();
   });
   
   function findCampaignLists(){
   
   	var campaignNameToFind = $("#campaignNameToFind").val();
   	$("#loader").show();
   	var url = "findCampaignListsAjax?campaignNameToFind="+campaignNameToFind;
   	$('#campAjaxdatatable').DataTable( {
   		    "processing": true,
   		    "serverSide": true,
   		    "destroy": true,
   		    "filter" : false,
   		    "ajax": {
   	            "url": url,
   	            "type": "POST"
   	        },
   	        "Paginate": true,
   			"language": {
   				"emptyTable": "No data available in table",
   				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
   				"infoEmpty": "No entries found",
   				"infoFiltered": "(filtered1 from _MAX_ total entries)",
   				"lengthMenu": "Show _MENU_ entries",
   				"zeroRecords": "No matching records found",
   				"oPaginate": {
   					"sNext": "Next",
   					"sPrevious": "Previous"
   				}
   			},
   	         "pageLength": 10,
   	         "bAutoWidth":false,
   	         "stateSave": true,
   	         "columns": [
   	           
   	            { "data":  null,
   					"render": function(data, type, row, meta){
   						if(type === 'display'){
                           data = '<a href="#" class="campParent"><span id="marketingParent" name="marketingParent" value ="'+row.campaignName+' ('+row.campaignId+')"></span>'+row.campaignName+' ('+row.campaignId+')</a>';						}
   						return data;
   					 }
   				 },
   	            { "data":  "startDate"},
   	            { "data":  "endDate"},
   	            
   				 
   	          ]
   		});
   	$("#loader").hide();
   }
   
   
   $(document).ready(function(){
   	localStorage.clear();
   	findCampaignMasterLists();
   });
   
   function findCampaignMasterLists(){
   
   	var campaignMasterToFind = $("#campaignMasterToFind").val();
   	$("#loader").show();
   	var url = "findCampaignListsAjax?campaignNameToFind="+campaignMasterToFind;
   	$('#campMasterAjaxdatatable').DataTable( {
   		    "processing": true,
   		    "serverSide": true,
   		    "destroy": true,
   		    "filter" : false,
   		    "ajax": {
   	            "url": url,
   	            "type": "POST"
   	        },
   	        "Paginate": true,
   			"language": {
   				"emptyTable": "No data available in table",
   				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
   				"infoEmpty": "No entries found",
   				"infoFiltered": "(filtered1 from _MAX_ total entries)",
   				"lengthMenu": "Show _MENU_ entries",
   				"zeroRecords": "No matching records found",
   				"oPaginate": {
   					"sNext": "Next",
   					"sPrevious": "Previous"
   				}
   			},
   	         "pageLength": 10,
   	         "bAutoWidth":false,
   	         "stateSave": true,
   	         "columns": [
   	           
   	            { "data":  null,
   					"render": function(data, type, row, meta){
   						if(type === 'display'){
                           data = '<a href="#" class="campMaster"><span id="marketingParent" name="marketingParent" value ="'+row.campaignName+' ('+row.campaignId+')"></span>'+row.campaignName+' ('+row.campaignId+')</a>';						}
   						return data;
   					 }
   				 },
   	            { "data":  "startDate"},
   	            { "data":  "endDate"},
   	            
   				 
   	          ]
   		});
   	$("#loader").hide();
   }
   
   $(document).ready(function() {
       getSubProducts("${product?if_exists}", "${subProduct?if_exists}");
       $("#product").change(function() {
           getSubProducts($(this).val(), "${subProduct?if_exists}");
       });
   
       function getSubProducts(product, subProduct) {
           var productId = product;
           $("#subProduct").empty();
           var list = $("#subProduct");
           list.append("<option value='' class='nonselect'>Please Select</option>");
           if (productId != null && productId != "") {
               $("#subProduct").attr("required",true);
               $.ajax({
                   type: 'POST',
                   async: false,
                   url: "getSubProductsDataJSON",
                   data: {
                       "productId": productId
                   },
                   success: function(subProducts) {
                       if (subProducts.length == 0) {
                           list.append("<option value = ''>N/A</option>");
                       } else {
                           for (var i = 0; i < subProducts.length; i++) {
                               if (subProduct != null && subProduct != "" && subProducts[i].enumId == subProduct) {
                                   list.append("<option  value =" + subProducts[i].enumId + " selected>" + subProducts[i].description + " </option>");
                               } else {
                                   list.append("<option  value =" + subProducts[i].enumId + ">" + subProducts[i].description + "</option>");
                               }
   
                           }
                       }
                   }
               });
           } else {
              $("#subProduct").attr("required",false);
           }
           $('#subProduct').append(list);
           $('#subProduct').dropdown('refresh');
       }
   });
</script>