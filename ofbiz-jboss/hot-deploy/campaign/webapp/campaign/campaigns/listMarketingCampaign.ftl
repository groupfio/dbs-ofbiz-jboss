<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.campaignListView}</h2>
</div>
        <input type="hidden" name="parentCampaignId" id="parentCampaignId" value="${context.get('parentCampaignId')!}">
        <input type="hidden" name="masterCampaignId" id="masterCampaignId" value="${context.get('masterCampaignId')!}">
        <input type="hidden" name="isDripCampaign" id="isDripCampaign" value="${context.get('isDripCampaign')!}">
<#assign listCampaignId = .now?long>

<div class="table-responsive">
	<table id="findCampains" class="table table-hover list-campaign-${listCampaignId}">
	<thead>
	<tr>
		<th>${uiLabelMap.Name}</th>
		<th>${uiLabelMap.masterCampaign}</th>
		<th>${uiLabelMap.parentCampaign}</th>
		<th>${uiLabelMap.notOpen}</th>
		<th>${uiLabelMap.opened}</th>
		<th>${uiLabelMap.clicked}</th>
		<th>${uiLabelMap.bounced}</th>
		<th>Un-Sub</th>
		<th>${uiLabelMap.converted}</th>
        <th>${uiLabelMap.deliveryMethod}</th>
        <th>${uiLabelMap.status}</th>
        <th>${uiLabelMap.startDate}</th>
        <th>${uiLabelMap.endDate}</th>
        <th>Created By</th>
        <th>Created Date</th>
       <#--  
        <#if isDripCampaign?has_content>
        <th># of Contacts</th>
        <th>#Opened</th>
        <th>#Clicked</th>
        <th>#Converted</th>
        </#if>
        -->
	</tr>
	</thead>
	</table>
</div>

<script>
    $(document).ready(function() {
  
  		
  			
        var campaignId = $("#campaignId").val();
        if(undefined==campaignId){
           campaignId="";
        }
        var campaignName = $("#campaignName").val();
        if(undefined==campaignName){
           campaignName="";
        }
        var statusId = $("#statusId").val();
        if(undefined==statusId){
             statusId="";
        }
        var campaignType = $("#campaignType").val();
        if(undefined==campaignType){
             campaignType="";
        }
        var fromDate = $("#fromDate").val();
        if(undefined==fromDate){
             fromDate="";
        }
        var thruDate = $("#thruDate").val();
        if(undefined==thruDate){
             thruDate="";
        }
        var parentCampaignId = $("#parentCampaignId").val();
        if(undefined==parentCampaignId){
             parentCampaignId="";
        }
        var masterCampaignId = $("#masterCampaignId").val();
         if(undefined==masterCampaignId){
             masterCampaignId="";
        }
        var isDripCampaign = $("#isDripCampaign").val();
         if(undefined==isDripCampaign){
             isDripCampaign="";
        }
        
        var createdBy =$("#createdBy").val();
         if(undefined==createdBy){
             createdBy="";
        }
        
        
   		var createdDate =$("#createdDate").val();
        if(undefined==createdDate){
         createdDate="";
        }
        
        var campaignHierarchyType =$("#campaignHierarchyType").val();
        if(campaignHierarchyType == null || campaignHierarchyType == undefined){
         campaignHierarchyType="";
        }
        
        var masterParentCampaignId =$("#masterParentCampaignId").val();
        if(masterParentCampaignId == null || masterParentCampaignId == undefined){
         masterParentCampaignId="";
        }
        
        var findCampaignsUrl = "findCampaignsDetails?campaignId=" + campaignId + "&campaignName=" + campaignName + "&statusId=" + statusId + "&campaignType=" + campaignType+ "&fromDate=" + fromDate + "&thruDate=" + thruDate + "&parentCampaignId=" + parentCampaignId + "&masterCampaignId=" + masterCampaignId + "&isDripCampaign=" + isDripCampaign + "&createdBy=" + createdBy + "&createdDate=" + createdDate+ "&campaignHierarchyType=" + campaignHierarchyType + "&masterParentCampaignId=" + masterParentCampaignId ;
        loadFindCampaigns(findCampaignsUrl);
    });
    function loadFindCampaigns(findCampaignsUrl) {
        oTable = $('#findCampains').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "ajax": {
                "url": findCampaignsUrl,
                "type": "POST"
            },
            "Paginate": true,
            "order": [[ 11, "DESC" ]],
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },

            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [
                {
                    "data": "campaignName",
                    "render": function(data, type, row, meta) {
                        var campaignId = row.marketingCampaignId;
                        if (campaignId != null && campaignId != "" && campaignId != undefined) {
                            data = '<a href="/campaign/control/viewMarketingCampaign?marketingCampaignId=' + row.marketingCampaignId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.campaignName + '(' + campaignId + ')</a>';
                        }
                        return data;
                    }
                },
                
                {
                    "data": "masterCampDesc",
                    "render": function(data, type, row, meta) {
                        var masterCampaignId = row.masterCampaignId;
                        if (masterCampaignId != null && masterCampaignId != "" && masterCampaignId != undefined) {
                            data = '<a href="/campaign/control/viewMarketingCampaign?marketingCampaignId=' + row.masterCampaignId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.masterCampDesc + '(' + masterCampaignId + ')</a>';
                        }
                        return data;
                    }
                },
                
                {
                    "data": "parentCampDesc",
                    "render": function(data, type, row, meta) {
                        var parentCampaignId = row.parentCampaignId;
                        if (parentCampaignId != null && parentCampaignId != "" && parentCampaignId != undefined) {
                            data = '<a href="/campaign/control/viewMarketingCampaign?marketingCampaignId=' + row.parentCampaignId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.parentCampDesc + '(' + parentCampaignId + ')</a>';
                        }
                        return data;
                    }
                },
                
                {
                    "data": "notOpen",
                   "orderable": false
                },
                {
                    "data": "opened",
                   "orderable": false
                },
                {
                    "data": "clicked",
                   "orderable": false
                },
                {
                    "data": "bounced",
                   "orderable": false
                },
                {
                    "data": "unSub",
                   "orderable": false
                },
                {
                    "data": "converted",
                   "orderable": false
                },
                {
                    "data": "campaignTypeId"
                },
                {
                    "data": "statusId"
                },
                {
                    "data": "startDate"
                },
                {
                    "data": "endDate"
                },
                {
                    "data": "createdByUserLogin"
                },
                {
                    "data": "createdStamp"
                }
            ]
        });
    }
</script>