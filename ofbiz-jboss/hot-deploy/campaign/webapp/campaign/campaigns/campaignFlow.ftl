
<#assign marketingCampaignId = requestParameters.marketingCampaignId?if_exists />

<script src="/campaign-resource/js/highcharts-gantt.js"></script>
<script type="text/javascript">

$.getJSON('getMasterParentCampaignHierarchy', { marketingCampaignId: "${marketingCampaignId?if_exists}" }, function(data) {
	var chartData = data.chartData;
	if(chartData.length > 0){
		Highcharts.ganttChart('container', {
	        title: {
	            text: 'Campaign Graphical Flow'
	        },
	        credits:{
				enabled:false
			},
	        xAxis: {
	            //currentDateIndicator: true,
	            min: data.min,
	            max: data.max
	        },
	        series: data.chartData
	    });
	} else{
		$("#container").html("<span>No Child Campaigns</span>");
	}
    
});
</script>
<div class="clearfix"> </div>

<div id="container" class="d-flex justify-content-center pt-3"></div>

