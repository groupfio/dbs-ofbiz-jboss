<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<link href="/campaign-resource/css/querybuilder/query-builder.default.min.css" rel="stylesheet">
<script src="/campaign-resource/js/querybuilder/query-builder.standalone.min.js"></script>
<script src="/campaign-resource/js/querybuilder/jQuery.extendext.min.js"></script>
<style>
#builder_group_0 {
width: 100%;
}
#builder2_group_0 {
width: 100%;
}
#builder3_group_0 {
width: 100%;
}
.rules-group-header label.btn.active {
    background: #32cd32 !important;
    border-color: #32cd32 !important;
}
.rules-group-header label.btn {
    background: #a9a9a9 !important;
    border-color: #a9a9a9 !important;
}
</style>
<#--  <div class="page-header border-b">
   <h1 class="float-left">Smart List Template</h1>
</div> -->
<div style="margin-top: 10px;">
   <div class="page-header">
      <h2 class="float-left">Segmentation </h2>
   </div>
   <div id="builder"></div>
</div>

<div style="margin-top: 10px;">
   <div class="page-header">
      <h2 class="float-left">Attributes</h2>
   </div>
   <div id="builder2"></div>
</div>

<div style="margin-top: 10px;">
   <div class="page-header">
      <h2 class="float-left">Economic Metric</h2>
   </div>
   <div id="builder3"></div>
</div>

<script>
   <#if campaignCriteria?has_content>
   	var criteria = ${StringUtil.wrapString(campaignCriteria?if_exists)};
   </#if>
   window.onload = function() {
       $.getJSON("getSmartlistSegmentationJsonData?groupType=SEGMENTATION",function(result){
			data = result;
			
			$('#builder').queryBuilder({
			  sortable: true,
			  allow_groups:false,
			  filters:result
			});
			
			<#if campaignCriteria?has_content>
			  var segmentRules = parseJsonSegmentCode(criteria,'SEGMENTATION');
			  if(jQuery.isEmptyObject(segmentRules)){
			  }else{
			    $('#builder').queryBuilder('setRules',segmentRules);
			  }
			</#if>
 	    });

       $.getJSON("getSmartlistAttrAndEconomicJsonData?groupType=CUSTOM_FIELD",function(result){
			//data = result;
			
			$('#builder2').queryBuilder({
			  sortable: true,
			  allow_groups:false,
			  filters:result
			});
		   <#if campaignCriteria?has_content> 
			  var customRules = parseJsonCustomField(criteria,'CUSTOM_FIELD');
			  if(jQuery.isEmptyObject(customRules)){
			  }else{
			    $('#builder2').queryBuilder('setRules',customRules);
			  }
		   </#if>
		});
		
 	   $.getJSON("getSmartlistAttrAndEconomicJsonData?groupType=ECONOMIC_METRIC",function(result){
			$('#builder3').queryBuilder({
			   sortable: true,
			   allow_groups:false,
			   filters:result
			});
		   <#if campaignCriteria?has_content>
			  var economicRules = parseJsonEconomicData(criteria,'ECONOMIC_METRIC');
			  if(jQuery.isEmptyObject(economicRules)){
			  }else{
			    $('#builder3').queryBuilder('setRules',economicRules);
			  }
			</#if>
		});
 				  				
 		var timeOut = 1500;
		var block ="N"
		setTimeout(function(){
			if(block == "N"){
			    block = "Y";
		    }else{	
		    	}	
		},timeOut);
   }	
   
   function isEmpty(obj) {
       for(var prop in obj) {
           if(obj.hasOwnProperty(prop))
               return false;
       }
       return true && JSON.stringify(obj) === JSON.stringify({});
   }

   function parseQuerySmart(id) {
    var segment = $('#builder').queryBuilder('getRules',{allow_invalid:true});
     var isSegmentValid = Boolean(segment.valid);
     
     var segJsonData = {};
     if(!isSegmentValid){
     	var rulesArray = segment.rules;
     	if(rulesArray.length > 0){
     		
     		var ruleResults =[];
     		for(var i = 0; i<rulesArray.length;i++){
     			var id = rulesArray[i].id;
     			var value = rulesArray[i].value;
     			var operator = rulesArray[i].operator;
     			if((id == null || id == "undefinded" || value == null || value == "undefined") && !(operator == "is_not_null" || operator == "is_null")){}
     			else{
     				ruleResults.push({"id":rulesArray[i].id,"field":rulesArray[i].field,"input":rulesArray[i].input,"operator":rulesArray[i].operator,"type":rulesArray[i].type,"value":rulesArray[i].value});
     			}	
     		}
     		if(ruleResults.length > 0){
     			segJsonData.condition = segment.condition;
     			segJsonData.rules = ruleResults;
     			segJsonData.valid = true;
     		}
     	}
     	//segJsonData = JSON.stringify(segJsonData);
     } else{
     	segJsonData = segment;
     }
     
     console.log("segJsonData-->"+segJsonData);
     
     var customFields = $('#builder2').queryBuilder('getRules',{allow_invalid:true});
     var iscustomFieldsValid = Boolean(customFields.valid);
     
     var custFieldJsonData = {};
     if(!iscustomFieldsValid){
     	var rulesArray = customFields.rules;
     	if(rulesArray.length > 0){
     		
     		var ruleResults =[];
     		for(var i = 0; i<rulesArray.length;i++){
     			var id = rulesArray[i].id;
     			var value = rulesArray[i].value;
     			var operator = rulesArray[i].operator;
     			if((id == null || id == "undefinded" || value == null || value == "undefined") && !(operator == "is_not_null" || operator == "is_null")){}
     			else{
     				ruleResults.push({"id":rulesArray[i].id,"field":rulesArray[i].field,"input":rulesArray[i].input,"operator":rulesArray[i].operator,"type":rulesArray[i].type,"value":rulesArray[i].value});
     			}	
     		}
     		if(ruleResults.length > 0){
     			custFieldJsonData.condition = customFields.condition;
     			custFieldJsonData.rules = ruleResults;
     			custFieldJsonData.valid = true;
     		}
     	}
     	//custFieldJsonData = JSON.stringify(custFieldJsonData);
     } else{
     	custFieldJsonData = customFields;
     }
     
     var economic = $('#builder3').queryBuilder('getRules',{allow_invalid:true});
     
     var isEconomicValid = Boolean(economic.valid);
     
     var economicJsonData = {};
     if(!isEconomicValid){
     	var rulesArray = economic.rules;
     	if(rulesArray.length > 0){
     		
     		var ruleResults =[];
     		for(var i = 0; i<rulesArray.length;i++){
     			var id = rulesArray[i].id;
     			var value = rulesArray[i].value;
     			var operator = rulesArray[i].operator;
     			if((id == null || id == "undefinded" || value == null || value == "undefined") && !(operator == "is_not_null" || operator == "is_null")){}
     			else{
     				ruleResults.push({"id":rulesArray[i].id,"field":rulesArray[i].field,"input":rulesArray[i].input,"operator":rulesArray[i].operator,"type":rulesArray[i].type,"value":rulesArray[i].value});
     			}	
     		}
     		if(ruleResults.length > 0){
     			economicJsonData.condition = economic.condition;
     			economicJsonData.rules = ruleResults;
     			economicJsonData.valid = true;
     		}
     	}
     	//economicJsonData = JSON.stringify(economicJsonData);
     } else{
     	economicJsonData = economic;
     }
     if(isEmpty(segJsonData) && isEmpty(custFieldJsonData) && isEmpty(economicJsonData)) {
		return false;
    }
     
    var myobj = {"segment":segJsonData,"custom":custFieldJsonData,"economic":economicJsonData};
    var finalJsonObject = JSON.stringify(myobj);
    finalJsonObject = finalJsonObject.replace(/\\/g, "");
    finalJsonObject = finalJsonObject.replace(/"/g, "quot;");
   	
   	if(id !=0)
	{
		document.getElementById('jsonString').value = finalJsonObject;
   		document.getElementById('smartlistTemplateForm').submit();
	}	
   }
</script>
<div style="">
   <#if campaignTypeSmTemplate?exists && campaignTypeSmTemplate?has_content>
		<div style="">
			<button type="button" id="update" onClick="javascript:parseQuerySmart(${campaignTypeSmTemplate.smartListId?if_exists})" class="btn btn-sm btn-primary mt-2" >Update</button>
			<a class="btn btn-sm btn-primary mt-2" href="javascript:submitProcessForm();">Process</a>
			
			<form name="processSmartListForm" id ="processSmartListForm" action="<@ofbizUrl>executeSmartList</@ofbizUrl>" method="post" data-toggle="validator" novalidate="true">
				<input type="hidden" id="processName" name="processName" value="prepareSmartListData" />
				<input type="hidden" id="marketingCampaignId" name="marketingCampaignId"  value="${requestParameters.marketingCampaignId?if_exists}" />
				<input type="hidden" name="smartListId" value="${campaignTypeSmTemplate.smartListId?if_exists}"/>
				<input type="hidden" name="activeTab" value="smartList" />
			</form>
		</div>
		
		<form action="updateSmartlistTemplate" name="smartlistTemplateForm" id="smartlistTemplateForm" method="post" data-toggle="validator" novalidate="true">
		<input type="hidden" name="smartListId" value="${campaignTypeSmTemplate.smartListId?if_exists}"/>
		<input type="hidden" name="marketingCampaignId" value="${requestParameters.marketingCampaignId?if_exists}"/>
		<input type="hidden" name="jsonString" value="" id="jsonString" />
		<input type="hidden" name="type" value="CAMPAIGN_TYPE" id="type" />
		<input type="hidden" name="name" value="${campaignTypeSmTemplate.name?if_exists}" id="name" />
		   <#--<div class="row padding-r">
		      <div class="col-md-4 col-sm-4 form-horizontal">
		        <@generalInput 
					id = "name"
					label = uiLabelMap.name
					placeholder = uiLabelMap.name
					value = ""
					dataError="Please enter smart list template name"
					required = true
					/>
		      </div>
		   		
		      <div class="col-md-12 col-sm-12">
		         <@fromActions showClearBtn=true offsetSize="4"/>
		      </div>
		   </div> -->
		</form>
   </#if>
   
</div>



<script>
    function parseJsonCustomField(criteria, method) {
        var viewData = {
            condition: criteria.custom.condition,
            rules: []
        };
        var i = 0;
        criteria.custom.rules.forEach(function(column) {
            viewData.rules.push({
                id: column.id,
                field: column.field,
                type: column.type,
                input: column.input,
                operator: column.operator,
                value: column.value
            });

            i = i + 1;

        });

        if (i == 0) viewData = {};

        return viewData;

    }

    function parseJsonEconomicData(criteria, method) {

        var viewData = {
            condition: criteria.economic.condition,
            rules: []
        };

        var i = 0;

        criteria.economic.rules.forEach(function(column) {
            viewData.rules.push({
                id: column.id,
                field: column.field,
                type: column.type,
                input: column.input,
                operator: column.operator,
                value: column.value
            });

            i = i + 1;

        });

        if (i == 0) viewData = {};

        return viewData;

    }



    function parseJsonSegmentCode(criteria, method) {
        var viewData = {
            condition: criteria.segment.condition,
            rules: []
        };

        var i = 0;

        criteria.segment.rules.forEach(function(column) {
            if (method == column.id) {
                return;
            } else {

            }

            viewData.rules.push({
                id: column.id,
                field: column.field,
                type: column.type,
                input: column.input,
                operator: column.operator,
                value: column.value
            });

            i = i + 1;

        });

        if (i == 0) viewData = {};

        return viewData;

    }

    /* function executePrepareSmartList(contactListId) {
        if (contactListId != null && contactListId != "") {
            $("#hideScreen").hide();
            $("#ajaxLoader").show();
            $("#executePrepareSmartList").prop("disabled", true);
            $.getJSON("prepareSmartList", {
                "contactListId": contactListId
            }, function(data) {
                if (data.prepareSmartListStatus != "" && data.prepareSmartListStatus != null && data.prepareSmartListStatus == "SUCCESS") {
                    //alert("List generation process is completed for smart list");
                    var timer = setTimeout(function() {
                        window.location = '<@ofbizUrl>smartList</@ofbizUrl>?contactListId=' + contactListId
                    }, 5000);
                } else {
                    $("#ajaxLoader").hide();
                    $("#hideScreen").show();
                    alert("Error in execute smart list" + data.prepareSmartListStatus);
                }
            });
        }
    }*/
</script>
<script>
    $(function() {
        $('[name="builder_rule_0_filter"]').change(function() {});
    });
    
    function submitProcessForm(){
    	$('#processSmartListForm').submit();
    }
    
</script>