<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<link href="/bootstrap/css/dualselectlist.css" rel="stylesheet">
<script src="/bootstrap/js/dualselectlist.jquery.js"></script>
<script type="text/javascript" >
  function changeContactListStatus(campaignListId,status){
  $("#campaignListId").val(campaignListId);
  $("#isExpire").val(status);
   document.changeContactListStatus.submit();
  }
  
  function validateRemove(contactListId,campaignId){
  var r = confirm("Are you sure you want to delete");
  if (r == true) {
    
    document.removeContactListCampagin.contactListId.value = contactListId;
    document.removeContactListCampagin.marketingCampaignId.value = campaignId;
    document.removeContactListCampagin.submit();
  }
  
  }
  function add_list_value(value) {
           document.assignSmartListForm.contactSmartListId.value=value;
           document.assignSmartListForm.submit();
         }
</script>
<script type="text/javascript">
		$(document).ready(function(){
			var dsl = $('#dualSelectExample').DualSelectList({
				'candidateItems' : ['Tag Name 01', 'Tag Name 02', 'Tag Name 03', 'Tag Name 04', 'Tag Name 05', 'Tag Name 06', 'Tag Name 07'],
				'selectionItems' : ['Tag Name 08', 'Tag Name 09', 'Tag Name 03']
				
			});

			$('#getSel').click(function(){
				var res = dsl.getSelection();
				var str = '';
				for (var n=0; n<res.length; ++n) str += res[n] + '\n';
				$('#selResult').val(str);
			});

			$('#addSel').click(function(){
				var items = $('#addIterms').val().split('\n');
				var res = dsl.setCandidate(items);
				$('#addIterms').val('');
			});

			$('#setColor').click(function(){
				var clrName = $('#colorSelector').val();
				var clrValue = $('#colorValue').val();
				dsl.setColor(clrName, clrValue);
			});

			$('#resetColor').click(function(){
				var clrName = $('#colorSelector').val();
				dsl.resetColor(clrName);
			});
		});
		
$(document).ready(function(){
var typeId=$("#campaignTypeId").val();
if("PHONE_CALL"==typeId){
          $("#emailShow").hide();
          $("#emailActualShow").hide();
      }else if("EMAIL"==typeId){
         $("#phoneShow").hide();
         $("#phoneActualShow").hide();
      }else if("SMS"==typeId){
         $("#emailShow").hide();
         $("#emailActualShow").hide();
      }
    });
 $(document).ready(function(){  
   $('#campId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#camp";
    window.scrollTo(0,0);
  });
  $('#contactId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#contact";
    window.scrollTo(0,0);
  });
  $('#campListId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#tab3";
    window.scrollTo(0,0);
  });
  $('#dTagsId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#dTags";
    window.scrollTo(0,0);
  });
 $('#dripId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#drip";
    window.scrollTo(0,0);
  });
  $('#campListId').click(function(){
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#campList";
    window.scrollTo(0,0);
  });
  <#if tabId?if_exists=="contact">
    var loc=window.location;
    var newLoc=loc.toString().substring(0,loc.toString().indexOf("#"));
    window.location=newLoc+"#contact";
    window.scrollTo(0,0);
  </#if>
    var loc=window.location;
  var start = loc.toString().indexOf("#")+1;
  var end = loc.toString().length;
  var newLoc=loc.toString().substring(start,end);
  if(newLoc!= null && newLoc != "") {
       $('.nav-tabs a[href="#'+newLoc+'"]').tab('show');
   }
});
</script>

<#assign templateMasterData = delegator.findOne("TemplateMaster", {"templateId" : "${marketingCampaign.campaignTemplateId?if_exists}"}, false)?if_exists>
<#assign extra>
    <#if marketingCampaign.campaignTemplateId?exists && marketingCampaign.campaignTemplateId?has_content>
      <a class="btn btn-xs btn-primary" href="getTemplate?templateId=${marketingCampaign.campaignTemplateId?if_exists}" target="_blank">View Campaign Template</a>
    </#if>
</#assign>
<@sectionHeader title="${marketingCampaign.campaignName}[${marketingCampaign.marketingCampaignId?if_exists}]" extra=extra/>
<div class="clearfix"> </div>
<div class="page-header mt-1 mb-2 btn-primary border rounded  pl-2 pr-1">
	  <h2 class="display-4">
      <span class="mr-2"> # of Contacts: <span class="badge badge-light"><#if responseCount?exists>${responseCount.campCountMember?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2"> RM Approved: <span class="badge badge-light"><#if responseCount?exists>${responseCount.rmApproved?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2">Sent: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.sentMailCount?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Not Opened: <span class="badge badge-light"><#if responseCount.notOpen?exists> ${responseCount.notOpen?default(0)}</#if></span></span> 
      <span class="mr-2">Opened: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.openedMailCount?default(0)}<#else>0</#if></span></span>  
      <span class="mr-2">Clicked: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.clickedMailCount?default(0)}<#else>0</#if></span></span>  
     <!--<span class="mr-2">Converted: <span class="badge badge-light">${responseCount.convertedCount?default(0)}</span></span>-->
      <span class="mr-2">Unsubscribed: <span class="badge badge-light"><#if unSubscribed?exists> ${responseCount.unSubscribed?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Hard Bounced: <span class="badge badge-light"><#if responseCount?exists>${responseCount.hardBouncedMailCount?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2">Soft Bounced: <span class="badge badge-light"><#if responseCount?exists>${responseCount.softBouncedMailCount?default(0)}<#else>0</#if></span></span>   
      <span class="mr-2">Revenue: <span class="badge badge-light">$ 0.00</span></span>
      <span class="mr-2">Blacklisted: <span class="badge badge-light">0</span></span>
      </h2>
</div>
      <#if marketingCampaign.campaignTypeId?has_content && marketingCampaign.campaignTypeId=="PHONE_CALL">
        <#-- Assign RM screen in campaign section -->
        ${screens.render("component://campaign/webapp/widget/campaign/screens/list/listScreens.xml#viewOutboundCallList")}
      <#else>
        <div class="float-right">
          <a href="<@ofbizUrl>approveList?marketingCampaignId=${marketingCampaign.marketingCampaignId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary <#if !availableCampaign>disabled</#if>">Approve List</a>
        </div>
        ${screens.render("component://campaign/webapp/widget/campaign/screens/list/listScreens.xml#viewApproveList")}
      </#if>
<div class="clearfix"> </div>
<div id="myModalList" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Find Contact Lists</h4>
        <button type="reset" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="card-header">
          <form method="post" action="findList" id="findList" class="form-horizontal" name="findList" novalidate="novalidate" data-toggle="validator">
            <input type="hidden" name="duplicatingPartyId">
            <input type="hidden" name=" ">
            <input type="hidden" name="">		  
          </form>
          <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
        <h2 class="mt-3">Contact Lists</h2>
        <hr>
        <div class="table-responsive">
          <table id="dtable" class="table table-striped">
            <thead>
              <tr>
                <th>Contact List Id</th>
                <th>Contact List Name</th>
              </tr>
            </thead>
            <tbody>
              <#if contactListLookUp?has_content>
              <#list contactListLookUp as contact>
              <tr>
                <td><a href="javascript:add_list_value('${contact.contactListName?if_exists}(${contact.contactListId?if_exists})')"">${contact.contactListId}</a></td>
                <td>${contact?if_exists.contactListName?if_exists}</td>
              </tr>
              </#list>
              </#if>			 
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="sbmit" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
<script>
$("#dtableGrid").DataTable();
$("#dtableSmart").DataTable();
</script>

<script>

$(document).ready(function() {
    
    <#if !activeTab?has_content>
    	<#assign activeTab = requestParameters.activeTab!>
    </#if>
    
    <#if activeTab?has_content && activeTab == "camp">
    	$('.nav-tabs a[href="#camp"]').tab('show');
    <#elseif activeTab?has_content && activeTab == "contact">
    	$('.nav-tabs a[href="#contact"]').tab('show');
    <#elseif activeTab?has_content && activeTab == "campList">
    	$('.nav-tabs a[href="#campList"]').tab('show');
    <#elseif activeTab?has_content && activeTab == "drip">
    	$('.nav-tabs a[href="#drip"]').tab('show');
    <#else>
    	$('.nav-tabs a[href="#camp"]').tab('show');	
    </#if>
    
});

</script>