<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<link href="/bootstrap/css/dualselectlist.css" rel="stylesheet">
<script src="/bootstrap/js/dualselectlist.jquery.js"></script>

<script type="text/javascript" >
   $(document).ready(function() {
       
       <#if !activeTab?has_content>
         <#assign activeTab = requestParameters.activeTab!>
       </#if>
       
       <#if activeTab?has_content && activeTab == "camp">
         $('.nav-tabs a[href="#camp"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "contact">
         $('.nav-tabs a[href="#contact"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "campList">
         $('.nav-tabs a[href="#campList"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "drip">
         $('.nav-tabs a[href="#drip"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "assignRM">
         $('.nav-tabs a[href="#assignRM"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "callList">
         $('.nav-tabs a[href="#callList"]').tab('show');
       <#elseif activeTab?has_content && activeTab == "smartList">
    	$('.nav-tabs a[href="#smartList"]').tab('show'); 
       <#elseif activeTab?has_content && activeTab == "campaignFlow">
    	$('.nav-tabs a[href="#campaignFlow"]').tab('show');  
       <#else>
         $('.nav-tabs a[href="#camp"]').tab('show');	
       </#if>
       
   });
   function changeContactListStatus(campaignListId,status){
   $("#campaignListId").val(campaignListId);
   $("#isExpire").val(status);
    document.changeContactListStatus.submit();
   }
   
   function validateRemove(contactListId,campaignId){
   var r = confirm("Are you sure you want to delete");
   if (r == true) {
     
     document.removeContactListCampagin.contactListId.value = contactListId;
     document.removeContactListCampagin.marketingCampaignId.value = campaignId;
     document.removeContactListCampagin.submit();
   }
   
   }
   function add_list_value(value) {
            document.assignSmartListForm.contactSmartListId.value=value;
            document.assignSmartListForm.submit();
          }
  function add_smartList_value(value) {
           document.assignSmartListTemplate.contactSmartListId.value=value;
           document.assignSmartListTemplate.submit();
         }        
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var dsl = $('#dualSelectExample').DualSelectList({
            'candidateItems': ['Tag Name 01', 'Tag Name 02', 'Tag Name 03', 'Tag Name 04', 'Tag Name 05', 'Tag Name 06', 'Tag Name 07'],
            'selectionItems': ['Tag Name 08', 'Tag Name 09', 'Tag Name 03']

        });

        $('#getSel').click(function() {
            var res = dsl.getSelection();
            var str = '';
            for (var n = 0; n < res.length; ++n) str += res[n] + '\n';
            $('#selResult').val(str);
        });

        $('#addSel').click(function() {
            var items = $('#addIterms').val().split('\n');
            var res = dsl.setCandidate(items);
            $('#addIterms').val('');
        });

        $('#setColor').click(function() {
            var clrName = $('#colorSelector').val();
            var clrValue = $('#colorValue').val();
            dsl.setColor(clrName, clrValue);
        });

        $('#resetColor').click(function() {
            var clrName = $('#colorSelector').val();
            dsl.resetColor(clrName);
        });
    });

    $(document).ready(function() {
        var typeId = $("#campaignTypeId").val();
        if ("PHONE_CALL" == typeId) {
            $("#emailShow").hide();
            $("#emailActualShow").hide();
        } else if ("EMAIL" == typeId) {
            $("#phoneShow").hide();
            $("#phoneActualShow").hide();
        } else if ("SMS" == typeId) {
            $("#emailShow").hide();
            $("#emailActualShow").hide();
        }
    });
</script>
<div class="page-header border-b">
   <h1 class="float-left">${marketingCampaign.campaignName}[${marketingCampaign.marketingCampaignId?if_exists}]</h1>
   <p class="float-right">
      <#if marketingCampaign.marketingCampaignId?has_content && marketingCampaign.marketingCampaignId == "10160">
      <a href="viewCampaignFlowChart1?marketingCampaignId=${marketingCampaign.marketingCampaignId}" class="btn btn-xs btn-primary" >Flow Chart</a>
      <#elseif marketingCampaign.marketingCampaignId?has_content && marketingCampaign.marketingCampaignId == "10161">	
      <a href="viewCampaignFlowChart2?marketingCampaignId=${marketingCampaign.marketingCampaignId}" class="btn btn-xs btn-primary" >Flow Chart</a>
      <#elseif marketingCampaign.marketingCampaignId?has_content && marketingCampaign.marketingCampaignId == "10262">	
      <a href="viewCampaignFlowChart3?marketingCampaignId=${marketingCampaign.marketingCampaignId}" class="btn btn-xs btn-primary" >Flow Chart</a>	
      </#if>
      <#if availableCampaign>
      <#--<button type="submit" class="btn btn-xs btn-primary" href="<@ofbizUrl>scheduleCampaign?marketingCampaignId=${marketingCampaign.marketingCampaignId?if_exists}</@ofbizUrl>">Schedule Campaign</button>-->
      <a href="<@ofbizUrl>updateMarketingCampaignForm?marketingCampaignId=${marketingCampaign.marketingCampaignId}&reportType=VIEW&donePage=viewMarketingCampaign</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Edit</a>
       
      <#if publishValidation?exists && publishValidation?has_content>
      <button type="reset" class="btn btn-xs btn-primary" id="publishTestButton">Test Publish</button>
      <#else>      
      <a href="<@ofbizUrl>publishTestMarketingCampaign?marketingCampaignId=${marketingCampaign.marketingCampaignId}&processId=campHeader&reportType=VIEW&donePage=viewMarketingCampaign</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Test Publish</a>
      </#if>
      <#if salesTriggerType?exists && "SALES_TRIGGER"!="${salesTriggerType}">
      <#if publishValidation?exists && publishValidation?has_content>
      <button type="reset" class="btn btn-xs btn-primary" id="publishButton">Production Publish</button>
      <#else>
      <a href="<@ofbizUrl>publishMarketingCampaign?marketingCampaignId=${marketingCampaign.marketingCampaignId}&reportType=VIEW&donePage=viewMarketingCampaign</@ofbizUrl>" class="btn btn-xs btn-primary" role="button">Production Publish</a>
      </#if>
      </#if>      
      <#else>

      </#if>
   </p>
</div>
<div class="clearfix"> </div>
<div class="page-header mt-1 mb-2 btn-primary border rounded pl-2 pr-1">
    <h2 class="display-4">
      <span class="mr-2"> # of Contacts: <span class="badge badge-light"><#if responseCount?exists>${responseCount.campCountMember?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2"> RM Approved: <span class="badge badge-light"><#if responseCount?exists>${responseCount.rmApproved?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2">Sent: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.sentMailCount?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Not Opened: <span class="badge badge-light"><#if responseCount.notOpen?exists> ${responseCount.notOpen?default(0)}</#if></span></span> 
      <span class="mr-2">Opened: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.openedMailCount?default(0)}<#else>0</#if></span></span>  
      <span class="mr-2">Clicked: <span class="badge badge-light"><#if responseCount?exists> ${responseCount.clickedMailCount?default(0)}<#else>0</#if></span></span>  
     <!--<span class="mr-2">Converted: <span class="badge badge-light">${responseCount.convertedCount?default(0)}</span></span>-->
      <span class="mr-2">Unsubscribed: <span class="badge badge-light"><#if unSubscribed?exists> ${responseCount.unSubscribed?default(0)}<#else>0</#if></span></span>
      <span class="mr-2">Hard Bounced: <span class="badge badge-light"><#if responseCount?exists>${responseCount.hardBouncedMailCount?default(0)}<#else>0</#if></span></span> 
      <span class="mr-2">Soft Bounced: <span class="badge badge-light"><#if responseCount?exists>${responseCount.softBouncedMailCount?default(0)}<#else>0</#if></span></span>   
      <span class="mr-2">Revenue: <span class="badge badge-light">$ 0.00</span></span>
      <span class="mr-2">Blacklisted: <span class="badge badge-light">0</span></span>
   </h2>
</div>
<ul class="nav nav-tabs">
   <li class="nav-item"><a data-toggle="tab" href="#camp" id="campId" class="nav-link ">${uiLabelMap.campaign}</a></li>
   <li class="nav-item"><a data-toggle="tab" href="#contact" id="contactId" class="nav-link">${uiLabelMap.contactList}</a></li>
   <li class="nav-item"><a data-toggle="tab" href="#campList" id="campListId" class="nav-link">${uiLabelMap.campaignList}</a></li>
   <!--<li class="nav-item"><a data-toggle="tab" href="#dTags" id="dTagsId" class="nav-link">${uiLabelMap.dynamicDataTags}</a></li>-->
   <#if marketingCampaign.campaignTypeId?has_content && marketingCampaign.campaignTypeId=="PHONE_CALL">
     <li class="nav-item"><a data-toggle="tab" href="#assignRM" id="assignRMId" class="nav-link">Assign AM/RM To Campaign</a></li>
     <li class="nav-item"><a data-toggle="tab" href="#callList" id="callListId" class="nav-link">Call List</a></li>
   </#if>
   <li class="nav-item"><a data-toggle="tab" href="#smartList" id="smartListId" class="nav-link">Assign Smart List</a></li>
   <li class="nav-item"><a data-toggle="tab" href="#drip" id="dripId" class="nav-link">${uiLabelMap.dripCampaigns}</a></li>
   <li class="nav-item"><a data-toggle="tab" href="#campaignFlow" id="campaignFlowId" class="nav-link">${uiLabelMap.campaignFlow}</a></li>
   <#-- 
   <li class="nav-item"><a data-toggle="tab" href="#tab6" class="nav-link">Flow Chart</a></li>
   -->
</ul>
<div class="tab-content">
   <div id="camp" class="tab-pane fade">
      <div class="page-header">
         <h2 class="float-left" >${uiLabelMap.campaign}</h2>
      </div>
      <#--
      <p>&nbsp;</p>
      -->
      <div class="row padding-r">
         <div class="col-md-6 col-sm-6 form-horizontal">
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.Name}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${marketingCampaign.campaignName?if_exists}</label>				  
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.product}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">
                  <#if marketingCampaign.product?exists && marketingCampaign.product?has_content>
                  <#assign enumerationProduct = delegator.findOne("Enumeration", {"enumId" : marketingCampaign.product?if_exists}, false)?if_exists>
                  <#if enumerationProduct?exists && enumerationProduct?has_content>
                      ${enumerationProduct.description?if_exists}
                  </#if>
                  </#if>
                  </label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.subProduct}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">
                  <#if marketingCampaign.subProduct?exists && marketingCampaign.subProduct?has_content>
                  <#assign enumerationSubProduct = delegator.findOne("Enumeration", {"enumId" : marketingCampaign.subProduct?if_exists}, false)?if_exists>
                  <#if enumerationSubProduct?exists && enumerationSubProduct?has_content>
                  ${enumerationSubProduct.description?if_exists}
                  </#if>
                  </#if>
                  </label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">Campaign ${uiLabelMap.status}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${statusName?if_exists}</label>			
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.testCampStat!}</label>
               <div class="col-sm-7">
               <#if  testPublishStatus?if_exists == "Y">
                  <label class="col-form-label input-sm fw">${uiLabelMap.published!}</label>
                  <#else>
                  <label class="col-form-label input-sm fw"></label>
                </#if>			
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.deliveryMethod}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${campaignTypeName?if_exists}</label>
               </div>
               <input type="hidden" name="campaignTypeId" id="campaignTypeId" value="${marketingCampaign?if_exists.campaignTypeId?if_exists}">
            </div>
             <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.campaignEmailType}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${campaignEmailDesc?if_exists}</label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.pushType}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${pushType?if_exists}</label>			
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.roleType}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${roleDesc?if_exists}</label>			
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.salesTriggerType}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${salesTriggerTypeDesc?if_exists}</label>			
               </div>
            </div>
            <#-- <#assign financialMetricName =delegator.findOne("FinancialMetricConfig", {"financialConfigId" : marketingFinancialValues?if_exists.financialConfigId?if_exists}, false)?if_exists/>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.costPerAction}</label>
               <#if financialMetricName?has_content && financialMetricName?exists>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${financialMetricName?if_exists.costPerValue?if_exists}</label>
               </div>
               </#if>
            </div>-->
            <#assign parentCampaignName =delegator.findOne("MarketingCampaign", {"marketingCampaignId" : marketingCampaign.parentCampaignId?if_exists}, false)?if_exists/>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.parentCampaign}</label>
               <#if parentCampaignName?has_content && parentCampaignName?exists>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw"><a target="_blank" href="<@ofbizUrl>viewMarketingCampaign?marketingCampaignId=${marketingCampaign.parentCampaignId?if_exists}</@ofbizUrl>">${parentCampaignName?if_exists.campaignName?if_exists}(${marketingCampaign.parentCampaignId?if_exists})</a></label>
               </div>
               </#if>
            </div>
            <#assign masterCampaignId =delegator.findOne("MarketingCampaign", {"marketingCampaignId" : marketingCampaign.masterCampaignId?if_exists}, false)?if_exists/>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.masterCampaign}</label>
               <#if masterCampaignId?has_content && masterCampaignId?exists>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw"><a target="_blank" href="<@ofbizUrl>viewMarketingCampaign?marketingCampaignId=${marketingCampaign.masterCampaignId?if_exists}</@ofbizUrl>">${masterCampaignId?if_exists.campaignName?if_exists}(${marketingCampaign.masterCampaignId?if_exists})</a></label>
               </div>
               </#if>
            </div>
            <#assign templateMasterData = delegator.findOne("TemplateMaster", {"templateId" : "${marketingCampaign.campaignTemplateId?if_exists}"}, true)?if_exists>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.campaignTemplate}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">
                  <#--<a data-target="#ViewTemp" data-toggle="modal" href="getTemplate?mergeFormId=${marketingCampaign.campaignTemplateId?if_exists}">${marketingCampaign.campaignTemplateName?if_exists} [${marketingCampaign.campaignTemplateId?if_exists}]</a>-->
                  <#if marketingCampaign.campaignTemplateId?exists && marketingCampaign.campaignTemplateId?has_content>
                     <a href="getTemplate?templateId=${marketingCampaign.campaignTemplateId?if_exists}" target="_blank">${marketingCampaign.campaignTemplateName?if_exists} [${marketingCampaign.campaignTemplateId?if_exists}]</a>
                  </#if>
                  </label>
               </div>
            </div>			
			<div class="form-group row mb-0">
			   <label  class="col-sm-4 col-form-label">Multiple Template</label>
			   <div class="col-sm-7">
			      <label class="col-form-label input-sm fw">
			      <#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "Y">
			      Yes
			      <#elseif  marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "N">
			      No
			      <#else>
			      -
			      </#if>
			      </label>
			   </div>
			</div>
		    <div class= "form-group row mb-0">
			   <label  class="col-sm-4 col-form-label">${uiLabelMap.RmBcc}</label>
			   <div class="col-sm-7">
			      <label class="col-form-label input-sm fw">
			      <#if marketingCampaign?if_exists.rmBccStatus?has_content && marketingCampaign?if_exists.rmBccStatus == "Y">
			      Yes
			      <#elseif  marketingCampaign?if_exists.rmBccStatus?has_content && marketingCampaign?if_exists.rmBccStatus == "N">
			      No
			      <#else>
			      -
			      </#if>
			      </label>
			   </div>
			</div>
			<#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "Y">
			<div class="form-group row mb-0" >
			   <label class="col-sm-4 col-form-label"> </label>
			   <div class="col-sm-4">
			      <div class="input-group">
			         <label class="col-sm-4 col-form-label"> Templates</label>
			      </div>
			   </div>
			   <div class="col-sm-4">
			      <div class="input-group">
			         <label class="col-sm-4 col-form-label"> Segments</label>
			      </div>
			   </div>
			</div>
			<#if campMultiTempList?has_content>
			<#list campMultiTempList as list>
			<#assign templateDetails = delegator.findOne("TemplateMaster", {"templateId" : "${list.templateId?if_exists}"}, true)?if_exists>
			<#assign segmentDetails = delegator.findOne("CustomField", {"customFieldId" : "${list.customFieldId?if_exists}"}, true)?if_exists>
			<div class="form-group row mb-0" >
			   <label class="col-sm-4 col-form-label"> </label>
			   <div class="col-sm-4">
			      <div class="input-group">
			         <input  class="form-control input-sm "  type="text" value="${templateDetails.templateName?if_exists}(${templateDetails.templateId?if_exists})"readonly>
			      </div>
			   </div>
			   <div class="col-sm-4">
			      <div class="input-group">
			         <input  class="form-control input-sm "  type="text" value="${segmentDetails.groupName?if_exists}(${segmentDetails.groupId?if_exists})" readonly>
			      </div>
			   </div>
			</div>
			</#list>
			</#if>
			</#if>
            <#-- <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.timeZone}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${marketingCampaign.timeZoneId?if_exists}</label>
               </div>
            </div>-->
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.startDate}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw"><#if marketingCampaign.startDate?exists>${marketingCampaign.startDate?if_exists?string["dd-MM-YYYY"]}</#if></label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.endDate}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw"><#if marketingCampaign.endDate?exists>${marketingCampaign.endDate?if_exists?string["dd-MM-YYYY"]}</#if></label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.description}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${marketingCampaign.campaignSummary?if_exists}</label>
               </div>
            </div>
            <div class="form-group row mb-0">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.objectives}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">${marketingCampaign.objectives?if_exists}</label>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-6 form-horizontal">
            <div class="row padding-r">
               <div class="col-md-6 col-sm-6 form-horizontal">
                  <#-- <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.budgetSpendView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.budgetSpendValue?if_exists}</label>
                     </div>
                  </div>-->
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.targetRevenueView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.budgetRevenueValue?if_exists}</label>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.targetMarginView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.budgetMarginValue?if_exists}</label>
                     </div>
                  </div>
                  <div id="emailShow">
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.openTargetView}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.openTargetPerc?if_exists}%</label>
                        </div>
                     </div>
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.clickTargetView}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.clickTargetPerc?if_exists}%</label>
                        </div>
                     </div>
                  </div>
                  <div id="phoneShow">
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.contactsMadeView}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.contactsMade?if_exists}%</label>
                        </div>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.conversionTargetView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.conversionTargetPerc?if_exists}%</label>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.offerCodeCount}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">${marketingCampaign?if_exists.offerCodeCount?if_exists}</label>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 form-horizontal">
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.actualSpendValueView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.actualSpendValue?if_exists}</label>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.actualBudgetRevenueView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.actualBudgetRevenue?if_exists}</label>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.actualBudgetMarginView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">$${marketingFinancialValues?if_exists.actualBudgetMargin?if_exists}</label>
                     </div>
                  </div>
                  <div id="emailActualShow">
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.actualOpenTargetView}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.actualOpenTarget?if_exists}%</label>
                        </div>
                     </div>
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.actualClickTargetView}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.actualClickTarget?if_exists}%</label>
                        </div>
                     </div>
                  </div>
                  <div id="phoneActualShow">
                     <div class="form-group row mb-0">
                        <label  class="col-sm-7 col-form-label">${uiLabelMap.contactsMadeActual}</label>
                        <div class="col-sm-4">
                           <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.actualContactsMade?if_exists}%</label>
                        </div>
                     </div>
                  </div>
                  <div class="form-group row mb-0">
                     <label  class="col-sm-7 col-form-label">${uiLabelMap.actualConversionTargetView}</label>
                     <div class="col-sm-4">
                        <label class="col-form-label input-sm fw">${marketingFinancialValues?if_exists.actualConversionTarget?if_exists}%</label>
                     </div>
                  </div>
               </div>
               <div class="clearfix"> </div>
               <div class="col-md-12 col-sm-12">
                  <#-- <div class="form-group row mb-0">
                     <label  class="col-sm-4 col-form-label">${uiLabelMap.product}</label>
                     <div class="col-sm-7 pl-0">
                        <label class="col-form-label input-sm fw pl-0">${marketingCampaign?if_exists.product?if_exists}</label>
                     </div>
                  </div>-->
                  <div class="form-group row mb-0">
                     <label  class="col-sm-4 col-form-label">${uiLabelMap.measurementOfSuccess}</label>
                     <div class="col-sm-7 pl-0">
                        <label class="col-form-label input-sm fw pl-0">${marketingCampaign?if_exists.measurementOfSuccess?if_exists}</label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <form name="assignSmartListForm" id="assignSmartListForm" method="POST" action="<@ofbizUrl>addExistingSmartLisToCampagin</@ofbizUrl>" style="display:none;">
      <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
      <input type="hidden" name="contactSmartListId" value="" id="contactSmartListId"/>
      <input type="hidden" name="activeTab" value="contact" />	
   </form>
   <form name="assignSegmentForm" id="assignSegmentForm" method="POST" action="<@ofbizUrl>assignSegmentCodeToList</@ofbizUrl>" style="display:none;">
      <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
      <input type="hidden" name="segmentCodeId" value="" id="segmentCodeId"/>
      <input type="hidden" name="segmentCodeTypeId" value="" id="segmentCodeTypeId"/>
   </form>
   	<form name="assignSmartListTemplate" id="assignSmartListTemplate" method="POST" action="<@ofbizUrl>addExistingSmartLisTemplate</@ofbizUrl>" style="display:none;">
    <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
    <input type="hidden" name="contactSmartListId" value="" id="contactSmartListId"/>
    <input type="hidden" name="activeTab" value="smartList" />	
  </form>
  <form name="getSmartListTemplate" id="getSmartListTemplate" method="POST" action="<@ofbizUrl>getSmartListTemplate</@ofbizUrl>" style="display:none;">
    <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
    <input type="hidden" name="smartListId" value="" id="smartListId"/>
    <input type="hidden" name="activeTab" value="smartList" />	
  </form>
   <form name="changeContactListStatus" id="changeContactListStatus" method="POST" action="<@ofbizUrl>activeAndExpireMarketingContactList</@ofbizUrl>" style="display:none;">
      <input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
      <input type="hidden" name="isExpire" value="" id="isExpire"/>
      <input type="hidden" name="campaignListId" value="" id="campaignListId"/>
   </form>
   <form name="removeContactListCampagin" id="removeContactListCampagin" action="removeContactListCampagin">
      <input type="hidden" value="" name="contactListId"/>
      <input type="hidden" value="" name="marketingCampaignId"/>	
   </form>
   <div id="contact" class="tab-pane fade in ">
      <div class="page-header">
         <h2 class="float-left" >${uiLabelMap.contactList}</h2>
      </div>
      <#if availableCampaign>
      <div class="float-right">
         <#--<a href="<@ofbizUrl>approveList?marketingCampaignId=${marketingCampaign.marketingCampaignId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary <#if !availableCampaign>disabled</#if>">Approve List</a>-->
         <#--<a href="<@ofbizUrl>createSmartList</@ofbizUrl>"><span class="btn btn-xs btn-primary">New Smart List </span></a>-->
         <button class="btn btn-xs btn-primary" title="Create" data-toggle="modal" data-target="#myModalList" >Add List</button>
      </div>
      </#if>
      <div class="table-responsive">
         <table class="table table-striped" id="dtableGrid">
            <thead>
               <tr>
                  <th>${uiLabelMap.list}</th>
                  <th>${uiLabelMap.contactType}</th>
                  <th>${uiLabelMap.listType}</th>
                  <th>${uiLabelMap.numberOfMembers}</th>
                  <th>${uiLabelMap.numberOfAccount}</th>
                  <th>${uiLabelMap.noPartyFound}</th>
                  <th>${uiLabelMap.disabledParty}</th>
                  <th>${uiLabelMap.deduped}</th>
                  <th>${uiLabelMap.dedupedAccrossList}</th>
               </tr>
            </thead>
            <tbody>
               <#if marketingCampaignContactLists?exists && marketingCampaignContactLists?has_content>
               <#list marketingCampaignContactLists as mktCampaignContactList>
               <tr>
                  <td><a target="_blank" style="" href="findContactListMember?contactListId=${mktCampaignContactList.contactListId?if_exists}">${mktCampaignContactList.contactListName?if_exists} (${mktCampaignContactList.contactListId?if_exists})</a></td>
                  <td>
                     <#assign contactMechType=delegator.findOne("ContactMechType", {"contactMechTypeId" : mktCampaignContactList.contactMechTypeId?if_exists}, false)?if_exists/>
                     ${contactMechType.description?if_exists}							
                  </td>
                  <td>
                        <#if mktCampaignContactList.contactPurposeType?has_content> 
                            ${mktCampaignContactList.contactPurposeType?if_exists}
                        </#if>
                  </td>
                  <td class="text-right">
                     <div class="mr-4">
                        <#assign numberOfMembers = (Static["org.fio.campaign.common.MarketingHelper"].countContactListMembers(mktCampaignContactList.contactListId?if_exists,delegator))?if_exists>	
                        ${numberOfMembers?default(0)}
                     </div>
                  </td>
                  <td class="text-right">
                     <div class="mr-4">
                        <#assign numberOfAccount = (Static["org.fio.campaign.common.MarketingHelper"].countAccountListMembers(mktCampaignContactList.contactListId?if_exists,delegator))?if_exists>	
                        ${numberOfAccount?default(0)}
                     </div>
                  </td>
                  <#assign contactErrorList=delegator.findOne("ContactListAttribute", {"contactListId" : mktCampaignContactList.contactListId?if_exists,"attrName":"E145"}, false)?if_exists/>
                  <td class="text-right">
                     <div class="mr-4">
                        <#if contactErrorList?has_content>
                        ${contactErrorList.attrValue?if_exists?default(0)}
                     </div>
                     <#else>0</#if>
                  </td>
                  <#assign contactErrorList2=delegator.findOne("ContactListAttribute", {"contactListId" : mktCampaignContactList.contactListId?if_exists,"attrName":"E146"}, false)?if_exists/>
                  <td class="text-right">
                     <div class="mr-4">
                        <#if contactErrorList2?has_content>
                        ${contactErrorList2.attrValue?if_exists?default(0)}
                     </div>
                     <#else>0</#if>
                  </td>
                  <#assign contactErrorList3=delegator.findOne("ContactListAttribute", {"contactListId" : mktCampaignContactList.contactListId?if_exists,"attrName":"E148"}, false)?if_exists/>
                  <td class="text-right">
                     <div class="mr-4">
                        <#if contactErrorList3?has_content>
                        <!--${contactErrorList3.attrValue?if_exists?default(0)}-->
                        0
                     </div>
                     <#else>0</#if>
                  </td>
                  <#assign contactErrorList4=delegator.findOne("ContactListAttribute", {"contactListId" : mktCampaignContactList.contactListId?if_exists,"attrName":"E147"}, false)?if_exists/>
                  <td class="text-right">
                     <div class="mr-4">
                        <#if contactErrorList4?has_content>
                        ${contactErrorList4.attrValue?if_exists?default(0)}
                     </div>
                     <#else>0</#if>
                  </td>
               </tr>
               </#list>					
               </#if>
            </tbody>
         </table>
      </div>
   </div>
   <div id="campList" class="tab-pane fade in ">
      <#if availableCampaign>
      <div class="float-right">
         <a href="<@ofbizUrl>approveList?marketingCampaignId=${marketingCampaign.marketingCampaignId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary ">Approve List</a>
      </div>
      </#if> 
      <#-- Approve List screen in campaign section -->
      ${screens.render("component://campaign/webapp/widget/campaign/screens/list/listScreens.xml#viewApproveList")}
   </div>
   <#if marketingCampaign.campaignTypeId?has_content && marketingCampaign.campaignTypeId=="PHONE_CALL">
   <div id="assignRM" class="tab-pane fade in">
      <#-- Assign RM screen in campaign section -->
      ${screens.render("component://campaign/webapp/widget/campaign/screens/list/listScreens.xml#assignRM")}
   </div>
   <div id="callList" class="tab-pane fade in">
      <#-- Assign RM screen in campaign section -->
      ${screens.render("component://campaign/webapp/widget/campaign/screens/list/listScreens.xml#viewOutboundCallList")}
   </div>
   </#if>
   
<#-- <#assign campaignTypeSmTemplate = delegator.findOne("SmartlistTemplateBuilder",{"smartListId",requestParameters.smartListId?if_exists},false)?if_exists /> -->
<div id="smartList" class="tab-pane fade in">
  <div class="page-header">
      <h2 class="float-left" >Assigned Smart List 
      <#assign isProcessed ="N" />
 		<#if campaignTypeSmTemplate?exists && campaignTypeSmTemplate?has_content>
 			<#assign isProcessed = campaignTypeSmTemplate.isProcessed?if_exists />
 			&nbsp; [${campaignTypeSmTemplate.name?if_exists}(${campaignTypeSmTemplate.smartListId?if_exists})]
 		</#if>     
      </h2>
      <div class="float-right">
    <#if availableCampaign && isProcessed != "Y">
         <button class="btn btn-xs btn-primary" title="Create" data-toggle="modal" data-target="#myModalSmartList" >
         <#if campaignTypeSmTemplate?exists && campaignTypeSmTemplate?has_content>
        	Re-assign Smart List
         <#else>
         	Assign Smart List
         </#if>
         </button>
    </#if>
    <#if campaignTypeSmTemplate?exists && campaignTypeSmTemplate?has_content>
    	<button class="btn btn-xs btn-primary" title="Scheduler Config" data-toggle="modal" data-target="#schedulerModal">Scheduler Config</button>
    </#if>
      <#if smartListDownload?exists && smartListDownload?has_content>
      <a id="smartListFileDownloadDtn" href="/campaign-resource/template/smartlists/${smartListDownload!}" class="btn btn-xs btn-primary tooltips" title="" data-original-title="Export Smart List" download="">
         <i class="fa fa-download"></i> Download
      </a>
      </#if>
      </div>
  </div> 
  <#--  
   <div class="card-header mt-2 mb-3">
     
 	<div class="row">
	    <div class="col-md-2 col-sm-2">
	    	<label class="form-check-label">Smart List Template</label>
	     </div>
    
   		<div class="col-md-2 col-sm-2">
	     	<@simpleDropdownInput 
				id="selectedSmartListId"
				options=smartTemplate
				required=false
				value=requestParameters.smartListId?if_exists
				allowEmpty=true
				tooltip = "Smart List Template" 
				emptyText = "Smart List Template"
				dataLiveSearch=true
				/>
	     </div>
   	</div>
  </div>  -->
	<#if campaignTypeSmTemplate?exists && campaignTypeSmTemplate?has_content>
	       <#include "component:///campaign/webapp/campaign/campaigns/campaignSmartList.ftl" />
	</#if>
<script>
$("#selectedSmartListId").change(function(){
       var marketingCampaignId = ${marketingCampaign.marketingCampaignId?if_exists};
       var smartListId = document.getElementById("selectedSmartListId").value;
       document.getSmartListTemplate.smartListId.value=smartListId;
       document.getSmartListTemplate.submit();
   
});
$("#publishButton").click(function () {
    showAlert ("error", "${publishValidation?if_exists}");
});
$("#publishTestButton").click(function () {
    showAlert ("error", "${publishValidation?if_exists}");
});
</script>    	  	


</div>    
   <!--<div id="dTags" class="tab-pane fade">
      <div class="panel-group mt-2" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#menu1" aria-expanded="true" aria-controls="menu1">
              Tag Configuration
              </a>
            </h4>
          </div>
          <div id="menu1" class="panel-collapse collapse" data-parent="#accordion" aria-labelledby="heading1">
            <div class="panel-body">
              <div class="row ml-2 mr-2">
                <div class="col-md-8 col-sm-8 form-horizontal">
                  <form method="post" action="" class="form-horizontal" name="" novalidate="novalidate" data-toggle="validator">
                    <div class="row mb-2 mt-2 ">
                      <div class="col-md-4 col-sm-4">
                        <div class="form-group row">
                          <select class="ui dropdown search form-control input-sm" multiple>
                            <option value="">Predefined Tags </option>
                            <option value="10010" >Custom Tags</option>
                            <option value="10010" >Store Tags</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4">
                        <div class="form-group row">
                          <label class="col-form-label input-sm fw"> Sales Reminder Template (2588)</label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2">
                        <button type="reset" class="btn btn-sm btn-primary ">Save</button>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"> </div>
                </div>
              </div>
              <div id="dualSelectExample" style="height:300px;"></div>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#menu2" aria-expanded="false" aria-controls="headingTwo">
              Dynamic Data Tag
              </a>
            </h4>
          </div>
          <div id="menu2" class="panel-collapse collapse" data-parent="#accordion" aria-labelledby="menu2">
            <div class="panel-body">
              <div class="row ml-2 mr-2">
                <div class="col-md-8 col-sm-8 form-horizontal">
                  <form method="post" action="" class="form-horizontal" name="" novalidate="novalidate" data-toggle="validator">
                    <div class="row mb-2 mt-2 ">
                      <div class="col-md-4 col-sm-4">
                        <div class="form-group row">
                          <div class="col-sm-12 row">                  
                            <input type="text" class="form-control input-sm" placeholder="Account">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5 col-sm-5">
                        <div class="form-group row">
                          <select class="ui dropdown search form-control input-sm" multiple>
                            <option value="">Tag 1 </option>
                            <option value="10010" >Tag 2</option>
                            <option value="10010" >Tag 3</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"> </div>
                </div>
              </div>
              <div class="clearfix"> </div>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Account Name (ID)	</th>
                      <th>Tag Name</th>
                      <th>Tag Value</th>
                      <th>Tag Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="6"> No values for this campaign.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>-->
   <div id="drip" class="tab-pane fade in">
      ${screens.render("component://campaign/webapp/widget/campaign/screens/campaign/CampaignScreens.xml#DripCampaign")}
   </div>
   <div id="campaignFlow" class="tab-pane fade in">
      ${screens.render("component://campaign/webapp/widget/campaign/screens/campaign/CampaignScreens.xml#CampaignFlow")}
   </div>
   <#-- 
   <div id="tab6" class="tab-pane fade in">
      ${screens.render("component://chart-generator/webapp/widget/chart-generator/screens/chart/ChartScreens.xml#SampleFlowchartView04")}
   </div>
   -->
</div>

<div class="clearfix"> </div>
<div id="myModalList" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content" id="searchContactList">
         <div class="modal-header">
            <h4 class="modal-title">${uiLabelMap.findContactList}</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <#assign listName= requestParameters.listName?default("") />
         <div class="modal-body">
            <div class="row padding-r">
				<div class="col-md-4 col-sm-4">
					 <@simpleInput 
					 id="listName"
					 value=listName!
					 placeholder="Contact List Name"
					 required=false
					 />
				</div>
				
				<div class="col-md-3 col-sm-2">
                        <div class="form-group row mr">
                          <select class="ui dropdown"  id="contactTpId" >
                            <option value="" data-content="<span class='nonselect'>Please Select</span>" selected>Please Select</option>
                            <option value="TEST" >Test</opttion>
                            <option value="LIVE" >Production</option>
                          </select>
                        </div>
				</div>
                 
				 <@fromSimpleAction id="findContactlist" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
               <#-- <div class="col-md-12 col-sm-12 ">
                  <div id="myBtnContainer" class="row mb-2">
                     <div class="col-md-3 col-sm-3">
                        <div class="form-group autocomplete">
                           <div class="input-group search-bg autocomplete m5">
                              <input type="text" class="form-control" placeholder="Search List" id="listName" name="listName" >
                              <span class="input-group-addon"> <a class="btn btn-sm" title="Search Contact List" href="javascript:findcontactLists();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
            <div class="table-responsive">
               <table id="listAjaxdatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>List Id</th>
                        <th>List Name</th>
                        <th>List Type</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(function(){
  $('#listName').keypress(function(e){
    if(e.which == 13) {
      findcontactLists();
    }
  });
});

$('#findContactlist').on('click',function(){
	findcontactLists();	
});

</script>
<#--Campaign Template Model -->
<div class="clearfix"> </div>
<div id="myModalSmartList" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
		<div class="modal-content" id="searchSmartListTemplate">
			<div class="modal-header">
			   <h4 class="modal-title">${uiLabelMap.findSmartListTemplate}</h4>
			   <button type="reset" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body"> 
			<div class="row padding-r">
				  <div class="col-md-12 col-sm-12 ">
					 <div id="myBtnContainer" class="row mb-2">
						<div class="col-md-3 col-sm-3">
						  <div class="form-group autocomplete">
							 <div class="input-group search-bg autocomplete m5">
								<input type="text" class="form-control" placeholder="Template Name" id="templateName" name="templateName" >
								<span class="input-group-addon"> <a class="btn btn-sm" title="Search Template List" href="javascript:findtemplateLists();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
							 </div>
						  </div>
						</div>
					 </div>
				  </div>
			   </div>
			   
				  
			   <div class="table-responsive">
			   <h2 class="mt-3">SmartList Template List</h2>
				  <table id="listAjaxSmartTable" class="table table-striped">
					 <thead>
						<tr>
						   <th>Template Id</th>
						   <th>Template Name</th>
						</tr>
					 </thead>
				  </table>
			   </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function(){
  $('#searchSmartListTemplate').keypress(function(e){
    if(e.which == 13) {
      findtemplateLists();
    }
  });
});

</script>
<div id="schedulerModal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Smart List Scheduler Configuration</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         </div>
         
         <div class="modal-body">
         <form name="schedulerForm" id="schedulerForm" action="smartListSchedulerConfig" role="form" class="form-horizontal" method="post" data-toggle="validator" onsubmit="javascript:scheduleSmartList('schedulerForm');">
         	<input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}" id="marketingCampaignId"/>
		    <input type="hidden" name="smartListId" value="${campaignTypeSmTemplate?if_exists.smartListId?if_exists}" id="smartListId"/>
		    <input type="hidden" name="activeTab" value="smartList" />	
         	<@dropdownInput 
		         id="executionType"
		         label=uiLabelMap.executionType
		         options=executionTypes
		         required=true
		         value=smartListJobSchedulerJob?if_exists.executionType?if_exists
		         />
		    <div id="isMultiExecution">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label noselect"></label>
	   				<div class="col-sm-7">
						<div class="form-check-inline">
							<label class="form-check-label noselect"> 
							<#assign configType = "NO_OF_DAYS" />
							<#if smartListJobSchedulerJob?exists && smartListJobSchedulerJob.configType?has_content>
								<#assign configType = smartListJobSchedulerJob.configType?if_exists />	
							</#if>
							<input class="form-check-input" name="configType" value="NO_OF_DAYS" type="radio" required <#if configType == "NO_OF_DAYS">checked</#if> > 
								# of days
							</label> &nbsp;&nbsp;
							<label class="form-check-label noselect"> 
							<input class="form-check-input" name="configType" value="SPEC_DATE" type="radio" required <#if configType == "SPEC_DATE">checked</#if> >
								Specific Date
							</label>
						</div>
					</div>
				</div>
				<div id="noOfDays">
				<@generalInput 
					id="duration"
					label=uiLabelMap.Duration
					placeholder=uiLabelMap.Duration
					value = smartListJobSchedulerJob?if_exists.intervalDays?if_exists
					inputType="number"
					required=true
					min=1
					/>
				</div>
				<div id="specDate">
				<@inputDate 
	               id="specificDate"
	               label=uiLabelMap.specificDate
	               placeholder=uiLabelMap.specificDate
	               value=jobSpecificDate?if_exists
	               default=true
	               required =true
	               />
	            </div>
		    	<@inputDate 
	               id="fromDate"
	               label=uiLabelMap.startDate
	               value=jobStartDate?if_exists
	               dateStartFrom = jobMinDate?if_exists
	               dateEndTo = jobMaxDate?if_exists
	               default=true
	               />
	               
	            <@inputDate 
		               id="thruDate"
		               label=uiLabelMap.endDate
		               value=jobEndDate?if_exists
		               dateStartFrom = jobMinDate?if_exists
		               dateEndTo = jobMaxDate?if_exists
		               disablePastDate =true
		               default=true
		               />
		    </div>
		    
            <div id="isSingleExecution">
            	<@inputDate 
	               id="startDate"
	               label=uiLabelMap.startDate
	               dateStartFrom = jobMinDate?if_exists
	               dateEndTo = jobMaxDate?if_exists
	               value=jobStartDate?if_exists
	               default=true
	               />
            </div>
           
            <#assign isEnable = "Y" />
			<#if smartListJobSchedulerJob?exists && smartListJobSchedulerJob?has_content>
				<#assign isEnable = smartListJobSchedulerJob.isEnable!"Y" />
			</#if>
             <div class="form-group row">
			   <label class="col-sm-4 col-form-label noselect" for="isEnable"></label>
			   <div class="col-sm-7">
			      <div class="input-icon ">
			         <label class="checkbox-inline noselect" for="isEnable">
						<input type="checkbox" class="checkbox" name="isEnable" id="isEnable" <#if isEnable?exists && isEnable?has_content && 'Y' == isEnable>checked<#else></#if> />Is Enable
					 </label>
					 <input type="hidden" name="isJobEnable" id="isJobEnable"/>  
			      </div>
			   </div>
			</div>
         </div>
         <div class="modal-footer">
           <button class="btn btn-sm btn-primary navbar-dark">Submit</button>
           <button class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
         </div>
      </form>
      </div>
   </div>
</div>

<script>
	$(function(){
		var executionType = $('#executionType').val();
		if (executionType == "MULTI") {
			$("#isMultiExecution").show();
			$("#isSingleExecution").hide();
		} else if (executionType == "SINGLE") {
			$("#isSingleExecution").show();
			$("#isMultiExecution").hide();
		}
		
		var configType =$("input[name='configType']:checked").val();
		if(configType == "NO_OF_DAYS"){
			$("#noOfDays").show();
			$("#specDate").hide();
		} else if (configType == "SPEC_DATE") {
			$("#specDate").show();
			$("#noOfDays").hide();
		}
	});
	$('#executionType').change(function(){
		if ($(this).val() == "MULTI") {
			$("#isMultiExecution").show();
			$("#isSingleExecution").hide();
		} else if ($(this).val() == "SINGLE") {
			$("#isSingleExecution").show();
			$("#isMultiExecution").hide();
		}
	});
	$('input[name="configType"]').on('click', function(){
		if ($(this).val() == "NO_OF_DAYS") {
			$("#noOfDays").show();
			$("#specDate").hide();
		} else if ($(this).val() == "SPEC_DATE") {
			$("#specDate").show();
			$("#noOfDays").hide();
		}
	});
	function scheduleSmartList(myform){
			var form = document.getElementById(myform);
			var isEnable = form.elements['isEnable'].checked;
			if(isEnable){
				$('#isJobEnable').val("Y");
			} else{
				$('#isJobEnable').val("N");
			}
			var selectedItem = $('#executionType').val();
			var flag = true;
	  		if("MULTI" == selectedItem){
	  			var configType =$("input[name='configType']:checked").val();
	  			if(configType == "NO_OF_DAYS"){
	  				var duration = $('#duration').val();
	  				if(duration == "" || duration == null || duration == "undefined" || duration == "0"){
		  				flag=false;
		  			}
	  			}
	  			else{
	  				var specificDate = $('#specificDate').val();
	  				if(specificDate == "" || specificDate == null || specificDate == "undefined"){
		  				flag=false;
		  			}
	  			}
	  		}
	  		if(flag){
				form.submit();
			}
			
		}		  
</script>

<div id="modalFlowChartView" class="modal fade" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Flow Chart</h4>
        <button type="reset" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="sbmit" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
<#--<div id="ViewTemp" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      Modal content
      <div class="modal-content">
      <#if templateMasterData?exists && templateMasterData?has_content>
         <div class="modal-header">
            <h5 class="modal-title">${templateMasterData.templateName} [${templateMasterData.templateId}]</h5>
            <button type="reset" class="close" data-dismiss="modal">&times;</button> 
         </div>
         <div class="modal-body">
            <div class="card-header">
               <#if templateMasterData.templateFormContent?has_content>
               <div contentEditable="false" style=" pointer-events: none;">
                  <#assign isBase64 = Static["org.apache.commons.codec.binary.Base64"].isBase64(templateMasterData.templateFormContent?if_exists) />
                  <#if isBase64>
                  <#assign template = Static["org.ofbiz.base.util.Base64"].base64Decode(templateMasterData.templateFormContent?if_exists) />
                  <#else>
                  <#assign template = templateMasterData.templateFormContent?if_exists />
                  </#if>
                  <#assign templateEncode = Static["org.ofbiz.base.util.Base64"].base64Encode(templateMasterData.templateFormContent?if_exists) />
                  ${StringUtil.wrapString(template)}
               </div>
               <#else>
               <span class="tableheadtext">${templateMasterData.textContent?if_exists}</span>
               </#if>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
         </div>
      </#if>
      </div>
   </div>
</div>-->
<script>
$(document).ready(function(){
	localStorage.clear();
	findcontactLists();
	findtemplateLists();
});

function findcontactLists(){

	var listName = $("#listName").val();
	var emailType = "${campaignEmailType!}";
    var contactTpId = $("#contactTpId").val();	
	console.log("contactTypeId-->"+contactTpId);
	$("#loader").show();
	var url = "findContactListAjax?listName="+listName+"&emailType="+emailType+"&contactTypeId="+contactTpId;
	$('#listAjaxdatatable').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "filter" : false,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "Paginate": true,
			"language": {
				"emptyTable": "No data available in table",
				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
				"infoEmpty": "No entries found",
				"infoFiltered": "(filtered1 from _MAX_ total entries)",
				"lengthMenu": "Show _MENU_ entries",
				"zeroRecords": "No matching records found",
				"oPaginate": {
					"sNext": "Next",
					"sPrevious": "Previous"
				}
			},
	         "pageLength": 10,
	         "bAutoWidth":false,
	         "stateSave": true,
	         "columns": [
	           
	            { "data":  null,
					"render": function(data, type, row, meta){
						if(type === 'display'){
                        data = '<a href="#" onclick=add_list_value("' + row.contactListId + '")>'+row.contactListId+'</a>';						}
						return data;
					 }
				 },
	            { "data":  "contactListName"},
	            { "data":  "contactListType"}
	            
				 
	          ]
		});
	$("#loader").hide();
}


function findtemplateLists(){
	var templateName = $("#templateName").val();
	$("#loader").show();
	var url = "findtemplateListAjax?templateName="+templateName;
	$('#listAjaxSmartTable').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "filter" : false,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "Paginate": true,
			"language": {
				"emptyTable": "No data available in table",
				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
				"infoEmpty": "No entries found",
				"infoFiltered": "(filtered1 from _MAX_ total entries)",
				"lengthMenu": "Show _MENU_ entries",
				"zeroRecords": "No matching records found",
				"oPaginate": {
					"sNext": "Next",
					"sPrevious": "Previous"
				}
			},
	         "pageLength": 10,
	         "bAutoWidth":false,
	         "stateSave": true,
	         "columns": [
	           
	            { "data":  null,
					"render": function(data, type, row, meta){
						if(type === 'display'){
                        data = '<a href="#" title="Click to Assign Smart List Template to Campaign" onclick=add_smartList_value("' + row.smartListId + '")>'+row.smartListId+'</a>';						}
						return data;
					 }
				 },
	            { "data":  "templateListName"}
	            
				 
	          ]
		});
	$("#loader").hide();
}

</script>

<script>
   $("#dtableGrid").DataTable();
   $("#listAjaxSmartTable").DataTable();
</script>