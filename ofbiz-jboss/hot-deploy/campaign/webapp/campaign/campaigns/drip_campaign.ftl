<div class="page-header">
	<h2 class="float-left">Drip Campaigns</h2>
	<#-- <div class="float-right">
		<div class="form-group row">
			<div class="col-sm-5">
				<input type="text" class="form-control input-sm" placeholder="">
			</div>
			<div class="col-sm-3">
				<button type="reset" class="btn btn-xs btn-primary m5">Search</button>
			</div>
			<div class="col-sm-4">
				<a href="" class="btn btn-xs btn-primary m5" alt="Report"
					title="Report">Clear All Filters</a>
			</div>
	
		</div>
	
	</div> -->
</div>

<br>
${setContextField("activeTab", "drip")}
<div class="panel-group" id="accordionMenu" role="tablist" aria-multiselectable="true">

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordionMenu"
					href="#accordionSegmentCode" aria-expanded="false"
					aria-controls="collapseOne"> Next Actions </a>
			</h4>
		</div>
		<div id="accordionSegmentCode" class="panel-collapse collapse show"
			role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				
				${screens.render("component://custom-field/webapp/widget/custom-field/screens/segment/SegmentScreens.xml#ListDripSegmentValue")}
				
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordionMenu"
					href="#accordionChildCampaign" aria-expanded="false"
					aria-controls="collapseOne"> Child Campaigns </a>
			</h4>
		</div>
		<div id="accordionChildCampaign" class="panel-collapse collapse show"
			role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				
				${setContextField("parentCampaignId", marketingCampaignId)}
				${setContextField("masterCampaignId", "")}
				${setContextField("isDripCampaign", "true")}
				${screens.render("component://campaign/webapp/widget/campaign/screens/campaign/CampaignScreens.xml#ListMarketingCampaign")}
				
			</div>
		</div>
	</div>
	<#-- 
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordionMenu"
					href="#accordionAsMaster" aria-expanded="false"
					aria-controls="collapseOne"> Campaigns as Master </a>
			</h4>
		</div>
		<div id="accordionAsMaster" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				
				${setContextField("parentCampaignId", "")}
				${setContextField("masterCampaignId", marketingCampaignId)}
				${setContextField("isDripCampaign", "true")}
				${screens.render("component://campaign/webapp/widget/campaign/screens/campaign/CampaignScreens.xml#ListMarketingCampaign")}
				
			</div>
		</div>
	</div>
	 --> 
</div>
${setContextField("activeTab", "")}