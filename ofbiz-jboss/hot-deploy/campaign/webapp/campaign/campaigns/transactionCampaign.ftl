 <script type="text/javascript" src="/crmsfa_js/jquery.validate.js"></script>
<head>
<style>
  #Cancel
  {
   margin-left:5px;
  }
  .control-label
  {
   font-size: 13px;
  }
 .box .title h4 span, .box .title h5 span {
    font-size: 16px;
   }
   
@media only screen and (min-width : 940px) and (max-width : 1024px) {
  .row-fluid:not(.fluid) .span6 {
    width: 75%;
  }

}

@media only screen and (min-width : 750px) and (max-width : 930px) {
     #formflow
     {
      margin-left:20%;
     }
     .row-fluid:not(.fluid) .span6 {
    width: 50%;
  }
  .icon-edit
	{
	 display:none;
	}
    .box .title h1 span, .box .title h2 span, .box .title h3 span, .box .title h4 span, .box .title h5 span {
    margin-left: 2%;
   }
}

@media only screen and (min-width : 320px) and (max-width : 450px) {
.box .title h1, .box .title h2, .box .title h3, .box .title h4, .box .title h5 {

    padding-left: 4px;
    }
	.icon-edit
	{
	 display:none;
	}
	.box .title h1 span, .box .title h2 span, .box .title h3 span, .box .title h4 span, .box .title h5 span {
       margin-bottom: 0;
      margin-left: 1%;
    }
}	
@media only screen and (min-width : 480px) and (max-width : 600px) {
.box .title h1 span, .box .title h2 span, .box .title h3 span, .box .title h4 span, .box .title h5 span {
    margin-left: 1%;
 }
  .icon-edit
	{
	 display:none;
	}
}
</style>
<style>
input.error{
border:1px solid #f00;
}

</style>
</head>



<@import location="component://opentaps-common/webapp/common/includes/lib/opentapsFormMacros.ftl"/>
<#assign marketingCampaign=delegator.findByPrimaryKey("MarketingCampaign", Static["org.ofbiz.base.util.UtilMisc"].toMap("marketingCampaignId", "${requestParameters.marketingCampaignId?if_exists}"))?if_exists>
	<div class="frameSectionHeader" style="">
    	<div class="x-panel-tl">
      		<div class="x-panel-tr">
        		<div class="x-panel-tc">
        		
        		<#if marketingCampaign?has_content>
        		<div class="x-panel-header" id="">Edit Transaction Campaign</div>
        		     <div class="frameSectionExtra" id="timeZoneDisplay" style="color:white;">
          				<#if emmServerTimeMap?has_content>Campaign runs at ${emmServerTimeMap.convertedTime?if_exists} ${emmServerTimeMap.timeZone?if_exists}</#if> 
          			</div>
        		
        		<#else>
        		<div class="x-panel-header" id="">Create Transaction Campaign</div>
        		     <div class="frameSectionExtra" id="timeZoneDisplay" style="color:white;">
          				<#if emmServerTimeMap?has_content>Campaign runs at ${emmServerTimeMap.convertedTime?if_exists} ${emmServerTimeMap.timeZone?if_exists}</#if> 
          			</div>
        		
        		</#if>	

        		</div>
      		</div>
    	</div>
  	</div>

<div class="form">
	<#if requestParameters.marketingCampaignId?if_exists=="">
		<form method="post" action="/rms/control/createTransactionCampaign" id="createMarketingCampaignForm" class="basic-form" onsubmit="javascript:submitFormDisableSubmits(this)" name="createMarketingCampaignForm">
			<input type="hidden" name="statusId" value="MKTG_CAMP_CREATED">
			<input type="hidden" name="templateImageUrl" id="Template_Image_Url">
			<input type="hidden" name="campaignTemplateName" id="campaignTemplateName">
			<input type="hidden" name="isApproved" value="NO">
	<#else>
		<form method="post" action="/rms/control/updateTransactionCampaign" id="updateMarketingCampaignForm" class="basic-form" onsubmit="javascript:submitFormDisableSubmits(this)" name="updateMarketingCampaignForm">	
		<#if marketingCampaign?exists>			
			<input type="hidden" name="openEmmCampaignId" value="${marketingCampaign.openEmmCampaignId?if_exists}">
			<input type="hidden" name="marketingCampaignId" value="${marketingCampaign.marketingCampaignId?if_exists}">
		</#if>
	</#if>
	<form method="post" action="/webServiceVersion/control/createCampaign" id="createMarketingCampaignForm" class="basic-form" onsubmit="javascript:submitFormDisableSubmits(this)" name="createMarketingCampaignForm">
			<div class="fieldgroup" id="_G463_"><div class="fieldgroup-title-bar"><table><tbody><tr><td class="collapse"></td><td></td></tr></tbody></table></div><div id="_G463__body" class="fieldgroup-body"> <table cellspacing="0">
				<table><tbody><tr><td class="collapse"></td><td ></td></tr></tbody></table></div><div id="_G71__body" class="fieldgroup-body"> <table cellspacing="0">
				  <tbody><tr>
				   <td class="label"><span class="requiredField">Campaign Name</span></td>
				   <td width="40px;">
				   	<input type="text" class="inputBox" name="campaignName" size="25" id="createMarketingCampaignForm_campaignName" value="<#if marketingCampaign?exists>${marketingCampaign.campaignName?if_exists}</#if>">
				   
				   	<div id="campaignNameError" style="padding-left:5px;color:#f00;"></div>
				  </td>
					<td class="label">
						<span class="requiredField">Campaign Template</span>
					</td>
					<td>
					
						<span class="formInputSpan1">
		<#if requestParameters.marketingCampaignId?if_exists!="">
	<#assign template=delegator.findByPrimaryKey("MergeForm", Static["org.ofbiz.base.util.UtilMisc"].toMap("mergeFormId", "${marketingCampaign.campaignTemplate?if_exists}"))>
	<input type="text" name="mergeFormId" id="mergeFormId" class="inputBox" size="30" " value="<#if marketingCampaign?has_content>${template.mergeFormName?if_exists}(${marketingCampaign.campaignTemplate?if_exists})</#if>" onBlur="getTemplate1()"></input>
      <a title="Template" href="javascript:call_fieldlookup2(document.updateMarketingCampaignForm.mergeFormId, 'LookupTemplates?typeId=TRANSACTON');">
        <img src="<@ofbizContentUrl>/images/fieldlookup.gif</@ofbizContentUrl>" width="16" height="16" border="0" alt="Lookup"></img>
      </a>
 <#--<@inputAutoCompleteTemplate   name="mergeFormId" id="mergeFormId"  default="${template.mergeFormName?if_exists}(${marketingCampaign.campaignTemplate?if_exists})" url="getAutoCompleteTemplates"  index=row_index  />-->
      <#else>
      <input type="text" name="mergeFormId" id="mergeFormId" class="inputBox" size="30"  value="<#if marketingCampaign?has_content>${marketingCampaign.campaignTemplate?if_exists}</#if>" onBlur="getTemplate1()"></input>
      <!--<a title="Template" href="javascript:call_fieldlookup2(document.createMarketingCampaignForm.mergeFormId, 'LookupTemplates');">-->
     <a id="tra" href="javascript:call_fieldlookup2(document.createMarketingCampaignForm.mergeFormId, 'LookupTemplates?typeId=TRANSACTON');">
        <img src="<@ofbizContentUrl>/images/fieldlookup.gif</@ofbizContentUrl>" width="16" height="16" border="0" alt="Lookup"></img>
      </a>
 <#--<@inputAutoCompleteTemplate   name="mergeFormId" id="mergeFormId"  default="" url="getAutoCompleteTemplates"  index=row_index  size=""/>-->
		</#if>

				</span>
				
				<a href="" class="hyperlink" name="submitButton" id="testselect" target="_blank"  style="display:none;" ><b>Edit Template</b></a>
					<div id="templateError" style="color:red;font-size:12px;list-style: none;"></div>
					</td>	
				  </tr>
				  <tr>
				   <td class="label" ><span class="tableheadtext" >Description</span></td>
				   <td >
				   <#if marketingCampaign?has_content>
				   <textarea class="inputBox" name="campaignSummary" cols="40" rows="3" value="" id="createMarketingCampaignForm_campaignSummary">${marketingCampaign.campaignSummary?if_exists}
				   </textarea>
				   <#else>
				   <textarea class="inputBox" name="campaignSummary" cols="40" rows="3" value="" id="createMarketingCampaignForm_campaignSummary"></textarea>
				   </#if>
				   <div id="descError" style="color:red;font-size:12px;list-style: none;"></div>
				   </td>
				  </tr>
				  <tr>
				   <td class="label" >&nbsp;</td>
				   <td colspan="4" ><input type="submit" class="smallSubmit" name="submitButton" value="Save" >
				   </td>
				  </tr>
				 </tbody></table>
			</div>
		</div>		
	</form>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#createMarketingCampaignForm').validate({
		rules: {
			campaignName: "required",
			mergeFormId : "required"

		},
		messages:{
			campaignName: "Please enter the Campaign Name",
			mergeFormId : "Please Choose the Template "
			
		},
		errorPlacement: function(error,element){
			if(element.attr("name")=="campaignName")
				error.appendTo("#campaignNameError");
			else if(element.attr("name")=="mergeFormId")
				error.appendTo("#templateError");
			else
				error.insertAfter(element);	
		}
	});

});
$(document).ready(function(){
	$('#updateMarketingCampaignForm').validate({
		rules: {
			campaignName: "required",
			mergeFormId : "required"

		},
		messages:{
			campaignName: "Please enter the Campaign Name",
			mergeFormId : "Please Choose the Template "
			
		},
		errorPlacement: function(error,element){
			if(element.attr("name")=="campaignName")
				error.appendTo("#campaignNameError");
			else if(element.attr("name")=="mergeFormId")
				error.appendTo("#templateError");
			else
				error.insertAfter(element);	
		}
	});

});

</script>
<script>
function getTemplate(){
var templateId=document.getElementById("mergeFormId").value;
//alert(templateId);
if(templateId.length>0){
var tep=$("#tempId").text(templateId);
$("#testselect").show();
$("#testselect").attr('href','<@ofbizUrl>EditMergeForm</@ofbizUrl>?mergeFormId='+ templateId);
}else{
$("#testselect").hide();

}

}
</script>

<style>
#ComboBox_mergeFormId{
width: 205px;
	}
#widget_ComboBox_mergeFormId{
width: 214px;
	}
</style>







