<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.campaignListView}</h2>
</div>
        <input type="hidden" name="parentCampaignId" id="parentCampaignId" value="${context.get('parentCampaignId')!}">
        <input type="hidden" name="masterCampaignId" id="masterCampaignId" value="${context.get('masterCampaignId')!}">
        <input type="hidden" name="isDripCampaign" id="isDripCampaign" value="${context.get('isDripCampaign')!}">
<#assign listCampaignId = .now?long>

<div class="table-responsive">
	<table id="findCrmCampains" class="table table-hover list-campaign-${listCampaignId}">
	<thead>
	<tr>
		<th>${uiLabelMap.Name}</th>
        <th>${uiLabelMap.deliveryMethod}</th>
        <th>${uiLabelMap.status}</th>
        <th>${uiLabelMap.startDate}</th>
        <th>${uiLabelMap.endDate}</th>
        <th>Created By</th>
        <th>Created Date</th>
        
        <#if isDripCampaign?has_content>
        <th># of Contacts</th>
        <th>#Open</th>
        <th>#Click</th>
        <th>#Converted</th>
        </#if>
        
	</tr>
	</thead>
	</table>
</div>

<script>
    $(document).ready(function() {
  
        var campaignId = $("#campaignId").val();
        if(undefined==campaignId){
           campaignId="";
        }
        var campaignName = $("#campaignName").val();
        if(undefined==campaignName){
           campaignName="";
        }
        var statusId = $("#statusId").val();
        if(undefined==statusId){
             statusId="";
        }
        var campaignType = $("#campaignType").val();
        if(undefined==campaignType){
             campaignType="";
        }
        var fromDate = $("#fromDate").val();
        if(undefined==fromDate){
             fromDate="";
        }
        var thruDate = $("#thruDate").val();
        if(undefined==thruDate){
             thruDate="";
        }
        var parentCampaignId = $("#parentCampaignId").val();
        if(undefined==parentCampaignId){
             parentCampaignId="";
        }
        var masterCampaignId = $("#masterCampaignId").val();
         if(undefined==masterCampaignId){
             masterCampaignId="";
        }
        var isDripCampaign = $("#isDripCampaign").val();
         if(undefined==isDripCampaign){
             isDripCampaign="";
        }
        var findCampaignsUrl = "findCampaignsDetails?campaignId=" + campaignId + "&campaignName=" + campaignName + "&statusId=" + statusId + "&campaignType=" + campaignType+ "&fromDate=" + fromDate + "&thruDate=" + thruDate + "&parentCampaignId=" + parentCampaignId + "&masterCampaignId=" + masterCampaignId + "&isDripCampaign=" + isDripCampaign;
        loadFindCampaigns(findCampaignsUrl);
    });
    function loadFindCampaigns(findCampaignsUrl) {
        oTable = $('#findCrmCampains').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "ajax": {
                "url": findCampaignsUrl,
                "type": "POST"
            },
            "Paginate": true,
            "order": [[ 6, "DESC" ]],
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },

            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [
                {
                    "data": "campaignName",
                    "render": function(data, type, row, meta) {
                        var campaignId = row.marketingCampaignId;
                        if (campaignId != null && campaignId != "" && campaignId != undefined) {
                            data = '<a href="viewMarketingCampaign?marketingCampaignId=' + row.marketingCampaignId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.campaignName + '(' + campaignId + ')</a>';
                        }
                        return data;
                    }
                },
                {
                    "data": "campaignTypeId"
                },
                {
                    "data": "statusId"
                },
                {
                    "data": "startDate"
                },
                {
                    "data": "endDate"
                },
                {
                    "data": "createdByUserLogin"
                },
                {
                    "data": "createdStamp"
                }
            ]
        });
    }
</script>