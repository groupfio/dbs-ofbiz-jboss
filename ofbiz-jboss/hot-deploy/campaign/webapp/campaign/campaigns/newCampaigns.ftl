 <div class="page-header border-b">
      <h1>New Campaign</h1>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="small-box border rounded">
          <div class="inner">
            <h3>Email</h3>
            <p>Campaign</p>
          </div>
          <div class="icon"><i class="fa fa-envelope"></i></div>
          <div class="small-box-footer" >
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=EMAIL&&classifyId=BATCH</@ofbizUrl>" class="btn btn-sm btn-primary">Batch</a>
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=EMAIL&&classifyId=CONTINUOUS</@ofbizUrl>" class="btn btn-sm btn-primary">Continuous</a>  
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=EMAIL&&classifyId=TRIGGER</@ofbizUrl>" class="btn btn-sm btn-primary">Trigger</a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="small-box border rounded">
          <div class="inner">
            <h3>Phone</h3>
            <p>Campaign</p>
          </div>
          <div class="icon">
            <i class="fa fa-phone"></i>
          </div>
          <div class="small-box-footer" >
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=PHONE_CALL&&classifyId=BATCH</@ofbizUrl>" class="btn btn-sm btn-primary">Calls</a>
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=PHONE_CALL&&classifyId=CONTINUOUS</@ofbizUrl>" class="btn btn-sm btn-primary">Continuous</a>  
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=PHONE_CALL&&classifyId=TRIGGER</@ofbizUrl>" class="btn btn-sm btn-primary">Trigger</a> 
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="small-box border rounded">
          <div class="inner">
            <h3>SMS</h3>
            <p>Campaign</p>
          </div>
          <div class="icon">
            <i class="fa fa-mobile"></i>
          </div>
          <div class="small-box-footer" >
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=SMS&&classifyId=BATCH</@ofbizUrl>" class="btn btn-sm btn-primary">SMS</a>
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=SMS&&classifyId=CONTINUOUS</@ofbizUrl>" class="btn btn-sm btn-primary">Continuous</a>  
            <a href="<@ofbizUrl>createMarketingCampaignForm?campaignTypeId=SMS&&classifyId=TRIGGER</@ofbizUrl>" class="btn btn-sm btn-primary">Trigger</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
