<div id="modalContactView" class="modal fade" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">${uiLabelMap.List} ${uiLabelMap.Member} for [ <span id="segment-value-titles"></span> ]</h2>
        <button type="reset" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
        <div class="clearfix"></div>
		<#-- <div class="page-header">
			<h2 class="float-left"></h2>
			<div class="float-right">
		  			
			</div>
		</div> -->
		<div class="table-responsive">
			<table id="view-contactCustomer-list" class="table table-striped">
				<thead>
					<tr>
						<th>Member</th>
		                <#-- <th>City</th>
		                <th>State</th> -->
		                <th>Phone Number</th>
		                <th>Email Address</th>
		                <th>Status</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
        
      </div>
      <div class="modal-footer">
        <button type="sbmit" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
