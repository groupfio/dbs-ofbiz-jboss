

<#assign seqInputId = 1000>
<#assign segmentInputId = 5000>
<div class="form-group row">
   <label class="col-sm-4 col-form-label">Multiple Template</label>
   <div class="col-sm-7">
      <div class="input-group">
         <input type="radio" name="multiTemplate" id="r1" value="Y"  
         <#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "Y">checked</#if>><label class="col-sm-2 col-form-label">Yes</label>
         <input type="radio" name="multiTemplate" id="r2"  value="N"  
         <#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "N">checked</#if>><label class="col-sm-2 col-form-label">No</label>
      </div>
   </div>
</div>
<div id="multi-template-section" style="display: <#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "Y">
   block<#else>none</#if>">
   <#if marketingCampaign?if_exists.multiTemplate?has_content && marketingCampaign?if_exists.multiTemplate == "Y" >
   <#assign counter = 0>
   <#if campMultiTempList?has_content>
   <#list campMultiTempList as list>
   <#assign templateDetails = delegator.findOne("TemplateMaster", {"templateId" : "${list.templateId?if_exists}"}, true)?if_exists>
   <#assign segmentDetails = delegator.findOne("CustomField", {"customFieldId" : "${list.customFieldId?if_exists}"}, true)?if_exists>
   <div class="form-group row campaign-content" >
      <label class="col-sm-4 col-form-label"> </label>
      <div class="col-sm-4">
         <div class="input-group">
            <input id="spec-date-template-display-${seqInputId}" class="form-control input-sm specDateTemplateSelected" placeholder="Select Template" type="text" value="${templateDetails.templateName?if_exists}" readonly>
            <input id="spec-date-template-${seqInputId}" type="hidden" name="specDateTemplateSelected" value="${list.templateId?if_exists}"> 
            <input id="spec-seq-Id-${seqInputId}" type="hidden" name="tempSeqId" value="${list.seqId?if_exists}"> 
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-list-alt template-picker" data-seqInputId="${seqInputId}" onclick="templatePicker(this)"> 
            </span>
            </span>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="input-group">
            <input id="spec-segment-display-${seqInputId}" class="form-control input-sm  specSegmentSelected" placeholder="Select Segment" type="text" value="${segmentDetails.groupName?if_exists}" readonly>
            <input id="spec-segment-${seqInputId}" type="hidden" name="specSegmentSelected" value="${list.groupId}">
            <input id="spec-segment-custFieldId${seqInputId}" type="hidden" name="specCustFieldSelected" value="${list.customFieldId?if_exists}"> 
            <span class="input-group-addon"> 
            <span class="glyphicon glyphicon-list-alt campaign-picker" data-seqInputId="${seqInputId}" onclick="segmentPicker(this)"> 
            </span>
            <#if (counter > 1) >
            <a class="plus-icon01 rd ml-1" onclick="removeCampaignRepeateContent(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
            </#if> 
            <a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
            </span>
         </div>
      </div>
   </div>
   <#assign counter = counter + 1>
   <#assign seqInputId = seqInputId + 1>
   </#list>
   </#if>
   <#else>
   <div class="form-group row campaign-content">
      <label class="col-sm-4 col-form-label"> </label>
      <div class="col-sm-4">
         <div class="input-group">
            <input id="spec-date-template-display-${seqInputId}" class="form-control input-sm specDateTemplateSelected" placeholder="Select Template" type="text" value="" readonly>
            <input id="spec-date-template-${seqInputId}" type="hidden" name="specDateTemplateSelected" value=""> 
            <span class="input-group-addon"> 
            <span class="glyphicon glyphicon-list-alt template-picker" data-seqInputId="${seqInputId}" onclick="templatePicker(this)"> 
            </span>
            </span>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="input-group">
            <input id="spec-segment-display-${seqInputId}" class="form-control input-sm  specSegmentSelected" placeholder="Select Segment" type="text" value="" readonly>
            <input id="spec-segment-${seqInputId}" type="hidden" name="specSegmentSelected" value="">
            <input id="spec-segment-custFieldId${seqInputId}" type="hidden" name="specCustFieldSelected" value=""> 
            <span class="input-group-addon"> 
            <span class="glyphicon glyphicon-list-alt campaign-picker" data-seqInputId="${seqInputId}" onclick="segmentPicker(this)"> 
            </span>
            <a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
            </span>
         </div>
      </div>
   </div>
   </#if>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {

        $('input[name="multiTemplate"]').on('click', function() {

            if ($(this).val() == "Y") {
                $("#multi-template-section").show()
            } else if ($(this).val() == "N") {

                $("#multi-template-section").hide();
            }
        });
    });
</script>
<script>
    var specSegmentSelected;
    var specDateTemplateSelected;

    function templatePicker(actionButton) {
        $('#findMultiTemplates').modal("show");
        specDateTemplateSelected = $(actionButton).attr("data-seqInputId");
    }

    function segmentPicker(actionButton) {

        $('#segmentSearch').modal("show");
        specSegmentSelected = $(actionButton).attr("data-seqInputId");
    }


    function addCampaignRepeateContent(actionButton) {

        var values = Number($(actionButton).closest(".campaign-content").children().find('.campaign-picker').attr("data-seqInputId"));
        var segVal = $("#spec-segment-" + values).val();
        var tmpVal = $("#spec-date-template-" + values).val();
        if (segVal != "" && tmpVal != "") {
            addCampaignRepeate(actionButton);
        } else {
            showAlert("error", "Please select all Multiple Template values");
            return;
        }

    }

    var seqInputId;

    function addCampaignRepeate(actionButton) {

        var cloneHtml = $(actionButton).closest(".campaign-content").clone();

        if ($(actionButton).parent().children().find('[class=\"fa fa-minus-circle\"]').length == 0) {
            cloneHtml.children().find('[class=\"fa fa-plus-circle\"]').parent().before('<a class="plus-icon01 rd ml-1" onclick="removeCampaignRepeateContent(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>');
        }

        if (!seqInputId) {
            seqInputId = Number($(actionButton).closest(".campaign-content").children().find('.campaign-picker').attr("data-seqInputId"));
        }
        seqInputId = seqInputId + 1;
        cloneHtml.children().find('.campaign-picker').attr("data-seqInputId", seqInputId);
        cloneHtml.children().find('.specSegmentSelected').attr("id", "spec-segment-display-" + seqInputId);

        cloneHtml.children().find('[name="specSegmentSelected"]').attr("id", "spec-segment-" + seqInputId);
        cloneHtml.children().find('[name="specCustFieldSelected"]').attr("id", "spec-segment-custFieldId" + seqInputId);

        cloneHtml.children().find('[name="specSegmentSelected"]').val("");
        cloneHtml.children().find('[name="specCustFieldSelected"]').val("");
        cloneHtml.children().find('.form-control').val("");

        cloneHtml.children().find('.template-picker').attr("data-seqInputId", seqInputId);
        cloneHtml.children().find('.specDateTemplateSelected').attr("id", "spec-date-template-display-" + seqInputId);
        cloneHtml.children().find('[name="specDateTemplateSelected"]').attr("id", "spec-date-template-" + seqInputId);

        cloneHtml.children().find('[name="specDateTemplateSelected"]').val("");
        cloneHtml.children().find('.form-control').val("");

        $(actionButton).closest(".campaign-content").after(cloneHtml);
    }

    function removeCampaignRepeateContent(actionButton) {
        $(actionButton).closest(".campaign-content").remove();
    }
</script>
<script>
    $(document).ready(function() {
        localStorage.clear();
        findSegment();
    });

    function findSegment() {
        var segmentName = $("#segmentName").val();
        $("#loader").show();
        var url = "findsegmentAjax?segmentName=" + segmentName;
        $('#segmentDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "filter": false,
            "ajax": {
                "url": url,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": true,
            "columns": [

                {
                    "data": "groupName",
                    "render": function(data, type, row, meta) {
                        if (type === 'display') {
                            data = '<a href="#" class="segSet"><span value ="' + row.groupName + ' (' + row.groupId + ')"></span>' + row.groupName + ' (' + row.groupId + ')<input type="hidden" name="segmentGroupId" value="' + row.groupId + '"><input type="hidden" name="customFieldId" value="' + row.customFieldId + '"></a>';
                        }
                        return data;
                    }
                },
                {
                    "data": "customFieldName"
                }


            ]
        });
        $("#loader").hide();
    }
    $(document).ready(function() {
        findMultiTemplates('');
    });

    function findMultiTemplates() {
        var templateCategories = '';
        var tempalateName = '';
        tempalateName = $("#searchTempalateName").val();        
        $("#loader").show();
        var url = "findTemplatesAjax?templateCategories="+templateCategories+"&tempalateName="+tempalateName;
        $('#multiTempAjaxtable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "filter": false,
            "ajax": {
                "url": url,
                "type": "POST"
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": true,
            "columns": [{
                    "data": "templateName",
                    "render": function(data, type, row, meta) {
                        if (type === 'display') {
                            data = '<a href="#" class="tempSet"><span value ="' + row.templateName + ' (' + row.templateId + ')"></span>' + row.templateName + ' (' + row.templateId + ')<input type="hidden" name="templateMultiId" value="' + row.templateId + '"></a>';
                        }
                        return data;

                    }
                },
                {
                    "data": "subject"
                },
                {
                    "data": "fromDate"
                },
                {
                    "data": "thruDate"
                },
                {
                    "data": null,
                    "orderable": false,
                    "render": function(data, type, row, meta) {
                        if (type === 'display') {
                            data = '<a href="<@ofbizUrl>getTemplate?templateId=' + row.templateId + '</@ofbizUrl>" class="fa fa-eye btn btn-xs btn-primary" title="View Template" target="_blank"></a>';
                        }
                        return data;
                    }
                }

            ]
        });
        $("#loader").hide();
    }
</script>

