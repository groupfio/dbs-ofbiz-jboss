<div class="page-header border-b">
      <h1 class="float-left">Find Campaigns</h1>
      <!--p class="float-right">
        <a type="reset" class="btn btn-xs btn-primary mt">Sync Orders</a>           
              </p!-->
    </div>
    <div class="card-header">
      <form method="post" action="findMarketingCampaigns" id="findMarketingCampaigns" class="form-horizontal" name="findMarketingCampaigns" novalidate="novalidate" data-toggle="validator">
        <input type="hidden" name="duplicatingPartyId">
        <div class="row padding-r">
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <select class="ui dropdown search form-control input-sm" name="campaignHierarchyType" id="campaignHierarchyType" value="">
              	<option value="">All</option>
              	<option value="MASTER_CAMPAIGN" <#if campaignHierarchyType?exists && campaignHierarchyType == "MASTER_CAMPAIGN">selected</#if>>Master</option>
              	<option value="PARENT_CAMPAIGN" <#if campaignHierarchyType?exists && campaignHierarchyType == "PARENT_CAMPAIGN">selected</#if>>Parent</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <input type="text" class="form-control input-sm" id="masterParentCampaignId" name="masterParentCampaignId" value="${masterParentCampaignId?if_exists}" placeholder="Master/Parent Campaign Id">
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <input type="text" class="form-control input-sm" id="campaignId" name="campaignId" value="${campaignId?if_exists}" placeholder="Campaign Id">
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <input type="text" class="form-control input-sm" id="campaignName" name="campaignName" value="${campaignName?if_exists}" placeholder="Campaign Name">
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <select class="ui dropdown search form-control input-sm" name="statusId" id="statusId" value="${statusId?if_exists}">
                <option value="">Select Status</option>
                <option value="MKTG_CAMP_CREATED" <#if statusId?has_content && statusId=="MKTG_CAMP_CREATED">selected</#if>>Created</option>
                <option value="MKTG_CAMP_PUBLISHED" <#if statusId?has_content && statusId=="MKTG_CAMP_PUBLISHED">selected</#if>>Published</option>
                <option value="SCHEDULED" <#if statusId?has_content && statusId=="SCHEDULED">selected</#if>>Scheduled</option>
                <option value="MKTG_CAMP_INPROGRESS" <#if statusId?has_content && statusId=="MKTG_CAMP_INPROGRESS">selected</#if>>Progress</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <select class="ui dropdown search form-control input-sm" name="campaignType" id="campaignType">
                <option value="">Select Delivery Method</option>
                <#if campaignTypes?has_content>
                <#list campaignTypes as campaignType>
                <option value="${campaignType.campaignTypeId}" <#if campaignTypeIdInd?if_exists=campaignType.campaignTypeId>selected</#if>>${campaignType.description}</option>
                </#list>
                </#if> 
              </select>
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <div class="input-group date" id="datetimepicker11">
                <input type='text' class="form-control input-sm" name="fromDate" id="fromDate" data-date-format="YYYY-MM-DD" value="${fromDate?if_exists}" placeholder="Start Date" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <div class="input-group date" id="datetimepicker10">
                <input type='text' class="form-control input-sm" name="thruDate" id="thruDate" value="${thruDate?if_exists}"  data-date-format="YYYY-MM-DD"  placeholder="End Date" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          
          
          <!--created by -->
          
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <input type="text" class="form-control input-sm" id="createdBy" name="createdBy" value="${createdBy?if_exists}" placeholder="Created by">
            </div>
          </div>
          
          <!--created date -->
          <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
              <div class="input-group date" id="datetimepicker12">
                <input type='text' class="form-control input-sm" name="createdDate" id="createdDate" value="${createdDate?if_exists}"  data-date-format="YYYY-MM-DD"  placeholder="Created date	" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          
          
          <div class="col-md-2 col-sm-2 pl-0">
            <button type="submit" class="btn btn-sm btn-primary">${uiLabelMap.findCampaigns} </button>
          </div>
        </div>
      </form>
      <div class="clearfix"> </div>
    </div>
    