
    <div class="jumbotron">
      <div class="container-fluid">
        <div class="page-header border-b">
		<div class="row padding-r">
		<div class="col-md-6 col-sm-6 padding-l">
          <h1 class="float-left">${uiLabelMap.campaignName!}</h1>
		 </div>	 
		  <div class="col-md-6 col-sm-6 mr-auto">
			  <form autocomplete="off" action="#">
             <div class="form-group autocomplete">
                <div class="input-group search-bg autocomplete m5">
                  <input type="text" class="form-control" placeholder="Select Campaign" id="myInput">
                 <span class="input-group-addon"> <a class="btn btn-sm btn-primary" title="Create New Product" href="#"><i class="fa fa-search" aria-hidden="true"></i></a></span><a class="btn btn-sm btn-primary" title="Refresh" href="">Refresh</a>
                </div>
              </div>
			  </form>
			 </div>
		  </div>
        </div>
	<div class="clearfix"> </div>
		<div class="page-header">
          <h2 class="float-left">${uiLabelMap.clickedDetailsReport!}</h2>
		  </div>
       <div class="table-responsive">
          <table  class="table table-striped">
            <thead>
              <tr>
                <th>URL</th>
                <th>Total Clicks</th>
                <th>Unique Clicks</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td> 
               </tr>
			  <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>	
            </tbody>
          </table>
		</div>
        <div class="clearfix"> </div>
      </div>
    </div>
    <!-- /.container -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
			<div class="modal-header"> 
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">
            <p>Some text in the modal.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>