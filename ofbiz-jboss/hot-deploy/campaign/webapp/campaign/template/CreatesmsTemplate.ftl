<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<#assign templateId = requestParameters.templateId?if_exists>
<#assign templateMasterData = delegator.findOne("TemplateMaster", {"templateId" : "${templateId?if_exists}"}, false)?if_exists>

<#if templateMasterData?exists && templateMasterData?has_content>
<@sectionHeader title="${uiLabelMap.editSMSTemplate!}" />
<form method="post" action="<@ofbizUrl>updateSMSTemplate</@ofbizUrl>" id="updateSmsTempalteForm" class="form-horizontal" name="updateSmsTempalteForm" novalidate="novalidate" data-toggle="validator">
   <input type="hidden" name="templateId" value="${templateId?if_exists}"/>
<#else>
<@sectionHeader title="${uiLabelMap.createSMSTemplate!}" />
<form method="post" action="<@ofbizUrl>createSMSTemplate</@ofbizUrl>" id="createSmsTempalteForm" class="form-horizontal" name="createSmsTempalteForm" novalidate="novalidate" data-toggle="validator">
</#if>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6 form-horizontal">
         
         <@generalInput 
               id = "templateName"
               label = uiLabelMap.name
               dataError="Please enter template name"
               placeholder = "Enter Sender Mail"
               value = templateMasterData.templateName
               required = true
               />
         <@generalInput 
               id = "subject"
               label = uiLabelMap.subject
               dataError="Please enter subject"
               placeholder = uiLabelMap.subject
               value = templateMasterData.subject
               required = true
               />
               
         <div class="form-group row  has-error">
            <label  class="col-sm-4 col-form-label has-error">${uiLabelMap.format!}</label>
            <div class="col-sm-7">
               <select name="templateType" id="templateType" class="form-control input-sm" required data-error="Please choose template format">
                   <option value="SMS" data-tokens="Unicode (UTF-8)" <#if templateMasterData.templateType?exists && templateMasterData.templateType == "SMS">selected</#if> >Text</option>
                  <option value="MMS" data-tokens="ISO 8859-15" <#if templateMasterData.templateType?exists && templateMasterData.templateType == "MMS">selected</#if>>MMS</option>
               </select>
            </div>
         </div>
         
         <@textareaInput
               id="textContent"
               label=uiLabelMap.text
               rows="3"
               placeholder = "Text"
               value = templateMasterData.textContent
               dataError="Please enter text"
               required = true
               />
      </div>
      <div class="col-md-12 col-sm-12">
      <#if templateMasterData?exists && templateMasterData?has_content>
	    <@fromActions showCancelBtn=true cancelUrl="findTemplates" />
	  <#else>
	    <@fromActions showClearBtn=true />
	  </#if>
	  </div>
   </div>
   <div class="clearfix"> </div>
</form>

