<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#assign tempName= requestParameters.tempalateName?default("") />
<@sectionHeader title="${uiLabelMap.findTemplates}" />
<div class="card-header">
   <div class="row padding-r">
   		<div class="col-md-2 col-sm-2">
             <@simpleInput 
             id="tempalateName"
             value=tempName!
             placeholder="Templates Name"
             required=false
             />
         </div>
         <@fromSimpleAction id="findTemplates" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
      <#--  
      <div class="col-md-10 col-sm-10 ">
         <div id="myBtnContainer" class="row mb-2">
            <div class="col-md-4 col-sm-4 ">
               <input type="text" class="form-control" placeholder="Search Templates" id="tempalateName" name="tempalateName" value="${requestParameters.tempalateName?if_exists}" >
               <span class="input-group-addon"> <a class="btn btn-sm" title="Search Templates" href="javascript:findTemplates('');"><i class="fa fa-search" aria-hidden="true"></i></a></span>
            </div>
         </div>
      </div>
      
      <div class="col-md-2 col-sm-2 mr-auto ">
         <div class="form-group autocomplete">
            <div class="input-group search-bg autocomplete m5">
               <input type="text" class="form-control" placeholder="Search Templates" id="tempalateName" name="tempalateName" value="${requestParameters.tempalateName?if_exists}" >
               <span class="input-group-addon"> <a class="btn btn-sm" title="Search Templates" href="javascript:findTemplates('');"><i class="fa fa-search" aria-hidden="true"></i></a></span>
            </div>
         </div>
      </div>
      -->
   </div>
</div>
<script>

   $('#findTemplates').on('click',function(){
	  findTemplates();
   });
   $(function(){
     $('#tempalateName').keypress(function(e){
       if(e.which == 13) {
       	findTemplates();
       }
     });
   });
</script>
<!-- Portfolio Gallery Grid -->
<!-- <div class="row" id="loadTemplate"> -->
<#-- <#if templateList?exists && templateList?has_content>
<#list templateList as template>
<div class="col-md-2 col-sm-2 nature btn">
   <a href="<@ofbizUrl>viewTemplate?templateId=${template.templateId!}</@ofbizUrl>">
      <div class="content border rounded">
         <img src="<@ofbizContentUrl>${StringUtil.wrapString(template.uploadThumbnail!)}</@ofbizContentUrl>" alt="${template.templateName!}" style="width:100%">
         <p class="pt-2 font-weight-bold">${template.templateName!}(${template.templateId!})</p>
      </div>
   </a>
</div>
</#list>
</#if> -->
<!-- END GRID -->
<!-- </div> -->
<div class="table-responsive">
   <table id="tempAjaxdatatable" class="table table-striped">
      <thead>
         <tr>
            <th>Template name</th>
            <th>Subject</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>View Template</th>
         </tr>
      </thead>
   </table>
</div>
<!-- Loader -->
<div class="loader text-center" id="loader" sytle="display:none;">
   <span></span>
   <span></span>
   <span></span>
</div>
<!-- END MAIN -->
<div id="viewTemplate" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div contentEditable="false" style=" pointer-events: none;" id="modal-body"/>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function(){
   	localStorage.clear();
   	findTemplates();
   });
   
   //function findTemplates(isAll){
   
   function findTemplates(){
   
   	var templateCategories = '';
   	var tempalateName = '';
   	var tempalateId = '';
   	
   	/*if(isAll == "all"){
   		$("#templateCategory").val('default');
   		$("#templateCategory").dropdown("refresh");
   		$("#tempalateName").val('');
   		
   	} else{ */
   		templateCategories = "";//$("#templateCategory").val();
   		tempalateName = $("#tempalateName").val();
   //	}
   	
   	$("#loader").show();
   	/*$.post("findTemplatesAjax",{"templateCategories":templateCategories,"tempalateName":tempalateName},function(data){
   		$("#loadTemplate").empty();
   			if(data !=null && data !=""){
   			var templatedata = '';
   			   for(var i=0;i<data.length;i++){
   			   		templatedata = templatedata+'<div class="col-md-2 col-sm-2 btn">';
   				    templatedata = templatedata+'<a href="<@ofbizUrl>viewTemplate?templateId='+data[i].templateId+'</@ofbizUrl>">';
   				    templatedata = templatedata+'<div class="content border rounded">';
   				    templatedata = templatedata+'<img src="'+data[i].uploadThumbnail+'" alt="'+data[i].templateName+'" style="width:100%">';
   				    templatedata = templatedata+'<p class="pt-2 font-weight-bold">'+data[i].templateName+'('+data[i].templateId+')</p>';
   				    templatedata = templatedata+'</div></a></div>';
   			   }
   			   $("#loadTemplate").append(templatedata);
   			}
   	}); */
   	
   	var url = "findTemplatesAjax?templateCategories="+templateCategories+"&tempalateName="+tempalateName;
   	$('#tempAjaxdatatable').DataTable( {
   		    "processing": true,
   		    "serverSide": true,
   		    "destroy": true,
   		    "filter" : false,
   		    "ajax": {
   	            "url": url,
   	            "type": "POST"
   	        },
   	        "Paginate": true,
   			"language": {
   				"emptyTable": "No data available in table",
   				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
   				"infoEmpty": "No entries found",
   				"infoFiltered": "(filtered1 from _MAX_ total entries)",
   				"lengthMenu": "Show _MENU_ entries",
   				"zeroRecords": "No matching records found",
   				"oPaginate": {
   					"sNext": "Next",
   					"sPrevious": "Previous"
   				}
   			},
   	         "pageLength": 10,
   	         "bAutoWidth":false,
   	         "stateSave": true,
   	         "columns": [
   	            { "data": "templateName",
   		          "render": function(data, type, row, meta){
   		            if(type === 'display'){
   		                data = '<a href="<@ofbizUrl>viewTemplate?templateId='+row.templateId+'</@ofbizUrl>">'+data+'('+row.templateId+')</a>';
   		            }
   		            return data;
   		         } 
   		      	},
   	            { "data": "subject",
   	            	"render": function(data, type, row, meta){
   						if(type === 'display'){
   							data = '<div class="longtext" title="'+data+'">'+data+'</div>';
   						}
   						return data;
   					 }
   	            },
   	            { "data":  "fromDate"},
   				{ "data":  "thruDate"},
   	            { "data":  null,
                  "orderable": false,
   					"render": function(data, type, row, meta){
   						if(type === 'display'){
   							data = '<a href="<@ofbizUrl>getTemplate?templateId='+row.templateId+'</@ofbizUrl>" class="fa fa-eye btn btn-xs btn-primary" title="View Template" target="_blank"></a>';
   						}
   						return data;
   					 }
   				 }
   		        
   	          ]
   		});
   	$("#loader").hide();
   }
   
   
   function templateSearch(element, id) {
       var value = $(element).val();
       $("#" + id + " div").each(function() {
           if ($(this).text().search(new RegExp(value, "i")) > -1) {
               $(this).css("display","block");
           } else {
               $(this).css("display","none");
           }
       });
   }
   	
   $("#viewTemplate").on("show.bs.modal", function(e) {
       var templateId = $(e.relatedTarget).data('target-id');
       $.get( "getTemplateContent?templateId=" + templateId, function( data ) {
           $("#modal-body").html(data["htmlContent"]);
       });
   
    });      	 
</script>