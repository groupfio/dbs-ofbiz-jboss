<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<script src="/campaign-resource/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<link href="/bootstrap/css/dualselectlist.css" rel="stylesheet">
<script src="/bootstrap/js/dualselectlist.jquery.js"></script>
<script type="text/javascript">
   var externalLoginUrl =  '${externalLoginKey?if_exists}';
</script> 
<#assign currentDate = Static["org.ofbiz.base.util.UtilDateTime"].nowDateString("dd-MM-yyyy")/>
<#assign templateId = requestParameters.templateId?if_exists>
<#assign templateMasterData = delegator.findOne("TemplateMaster", {"templateId" : "${templateId?if_exists}"}, true)?if_exists>
<#if templateMasterData?has_content && templateMasterData?exists>
	
	<#-- <#assign extra>
	<div id="extra">
	   <a class="btn btn-xs btn-primary templateextra" id="tempContent" data-target="Template" href="#">Continue</a>
	   <a class="btn btn-xs btn-primary templateextra d-none" id="tempDetails" data-target="Details" href="#">Back</a>
	</div>
	</#assign> -->
	
	<#assign extra>	
		<a class="btn btn-xs btn-primary" href="<@ofbizUrl>viewTemplate?templateId=${templateId}</@ofbizUrl>">Back</a>
	</#assign>
	<@sectionHeader title="${uiLabelMap.editTemplateForEmailBlast!}" extra=extra/>
	<form method="post" class="form-horizontal" action="<@ofbizUrl>updateEmailTemplate</@ofbizUrl>" id="TemplateForm" name="TemplateForm" novalidate="novalidate" data-toggle="validator" role="form">
	   <input type="hidden" name="templateId" value="${templateMasterData.templateId?if_exists}"/>
	   <input type="hidden" name="templateType" value="${templateMasterData.templateType?if_exists}"/>
<#else>
	<#-- 
   	<#assign extra>
   	<div id="extra">
      <a class="btn btn-xs btn-primary templateextra" id="tempContent" data-target="Template" href="#">Continue</a>
      <a class="btn btn-xs btn-primary templateextra d-none" id="tempDetails" data-target="Details" href="#">Back</a>
  	</div>
   	</#assign> -->
   	<@sectionHeader title="${uiLabelMap.createEmailTemplate!}" extra="" />
	<form method="post" action="<@ofbizUrl>createEmailTemplate</@ofbizUrl>" id="TemplateForm" name="TemplateForm" class="form-horizontal" onsubmit="javascript:updateContent();" novalidate="novalidate" data-toggle="validator" role="form">
	   <input type="hidden" name="templateType" value="EMAIL_BLAST">
</#if>
   <ul class="nav nav-tabs">
      <li class="nav-item"><a data-toggle="tab" href="#Details" class="nav-link">Details</a></li>
      <li class="nav-item"><a data-toggle="tab" href="#Template" class="nav-link">Template</a></li>
      <#if templateId?exists && templateId?has_content>
      <li class="nav-item"><a data-toggle="tab" href="#tagConfiguration" class="nav-link">Tag Configuration</a></li>
      </#if>
   </ul>
   <div class="tab-content">
      <div id="Details" class="tab-pane fade">
         <p></p>
         <div class="row padding-r">
            <div class="col-md-6 col-sm-6 form-horizontal">
               <@generalInput 
               id = "templateName"
               label = uiLabelMap.name
               placeholder = uiLabelMap.name
               value = templateMasterData.templateName
               dataError="Please enter template name"
               required = true
               />
               <@generalInput 
               id = "senderEmail"
               inputType="email"
               label = uiLabelMap.senderMail
               dataError="Please enter valid email"
               placeholder = "Please enter sender mail"
               value = templateMasterData.senderEmail
               required = true
               />	
               <@generalInput 
               id = "senderName"
               label = uiLabelMap.senderFullName
               placeholder = "Please enter sender full name"
               value = templateMasterData.senderName
               required = false
               />
               <@dropDown 
               id="emailCharSet"
               label=uiLabelMap.charSet
               options=charSetList
               value=""
               allowEmpty=false
               dataLiveSearch = true
               />
               <#assign defaultMailFormat="1">
               <#if templateMasterData.mailFormat?exists && templateMasterData.mailFormat?has_content>
               <#assign defaultMailFormat = templateMasterData.mailFormat />
               </#if>
               <@dropDown 
               id="mailFormat"
               label=uiLabelMap.format
               options=mailFormat
               onchange="javascript:mailFormatVal(this.value)"
               value = defaultMailFormat
               allowEmpty=false
               dataLiveSearch = true
               />
               <#assign templateCategory = delegator.findByAnd("TemplateToCategory", {"templateId" : templateId}, [], false)?if_exists />
               <@dropDown 
               id="templateCategoryId"
               label=uiLabelMap.templateCategory
               options=templateCategoryList
               value = ""
               allowEmpty=true
               dataLiveSearch = true
               />
               <@textareaInput
               id="description"
               label=uiLabelMap.description
               rows="3"
               placeholder = "Description"
               value = templateMasterData.description
               dataError="Please enter description"
               required = false
               />
            </div>
            <div class="col-md-6 col-sm-6 form-horizontal">
               <@generalInput 
               id = "subject"
               label = uiLabelMap.subject
               placeholder = uiLabelMap.subject
               value = templateMasterData.subject
               dataError="Please enter subject"
               required = true
               />
               <@generalInput 
               id = "senderEmailReplay"
               label = uiLabelMap.replyToMail
               placeholder = "Please enter reply mail"
               value = templateMasterData.senderEmailReplay
               required = false
               />
               <@generalInput 
               id = "replayToName"
               label = uiLabelMap.replyToFullName
               placeholder = "Please enter reply full name"
               value = templateMasterData.replayToName
               required = false
               />
               <#assign defaultEmailLineFeed="70">
               <#if templateMasterData.emailLineFeed?exists && templateMasterData.emailLineFeed?has_content>
               <#assign defaultEmailLineFeed = templateMasterData.emailLineFeed />
               </#if> 
               <@dropDown 
               id="emailLineFeed"
               label=uiLabelMap.lineFeed
               options=emailLineFeedList
               value= defaultEmailLineFeed
               allowEmpty=false
               dataLiveSearch = true
               />
               <#assign defaultEmailOnePixel="top">
               <#if templateMasterData.emailOnePixel?exists && templateMasterData.emailOnePixel?has_content>
               <#assign defaultEmailOnePixel = templateMasterData.emailOnePixel />
               </#if> 
               <@dropDown 
               id="emailOnePixel"
               label=uiLabelMap.measureOpenRate
               options=measureOpenRateList
               value = defaultEmailOnePixel
               allowEmpty=false
               dataLiveSearch = true
               />
               <@dropDown 
               id="preTemp"
               label=uiLabelMap.preTemplates
               options=preTemplatesList
               value = ""
               allowEmpty=false
               onchange="javascript:loadPreTemplate(this.value);"
               dataLiveSearch = true
               />
               <@generalInput 
               id = "thumbnailUrl"
               label = uiLabelMap.thumbnailUrl
               placeholder = "Please enter image URL"
               value = templateMasterData.thumbnailUrl
               required = false
               />
               <#if templateMasterData.fromDate?exists>
               <#assign fromDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(templateMasterData.fromDate?if_exists, "dd-MM-yyyy")?if_exists/>
               </#if>
               <@inputDate 
               id="fromDate"
               label=uiLabelMap.startDate
               value=fromDate
               required = false
               default=true
               />
               <#if templateMasterData.thruDate?exists>
               <#assign thruDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(templateMasterData.thruDate?if_exists, "dd-MM-yyyy")?if_exists/>
               </#if>
               <@inputDate 
               id="thruDate"
               label=uiLabelMap.endDate
               value=thruDate
               required = false
               default=true
               />
            </div>
            
            <#--<div class="col-md-12 col-sm-12">
	           <@fromActions showClearBtn=true />
	        </div>-->
           
            
         </div>
         <div class="col-md-12 col-sm-12">
	           <@fromActions showClearBtn=true />
	        </div>
         <div class="clearfix"> </div>
      </div>
      <div id="Template" class="tab-pane fade">
         <p></p>
         <div class="row pl-3 pr-3">
            <div class="col-md-3 col-sm-3 padding-l">
               <div class="panel-group" id="templateTagMenu" role="tablist" aria-multiselectable="true">
                 <#if templateTagTypes?exists && templateTagTypes?has_content>
                     <#list templateTagTypes as templateTagType>
                        <#if templateTagType.templateTagTypeId == "STANDARD">
                           <div class="panel panel-default">
		                     <div class="panel-heading" role="tab" id="headingTwo">
		                        <h4 class="panel-title">
		                           <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
		                           ${uiLabelMap.standard!}</a>
		                        </h4>
		                     </div>
		                     <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
		                        <div class="panel-body">
		                           <#if standardTagList?exists && standardTagList?has_content>
		                             <#list standardTagList as standardTag>
		                             	<@displayRow 
				                           label = standardTag.tagName
				                           value = standardTag.tagId
				                           isTag = true
				                           />
		                             </#list>
		                           </#if>
		                        </div>
		                     </div>
		                   </div>
                        <#elseif templateTagType.templateTagTypeId == "SEGMENTATION">
                           <div class="panel panel-default">
		                     <div class="panel-heading" role="tab" id="headingTwo">
		                        <h4 class="panel-title">
		                           <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
		                           ${uiLabelMap.segmentation!}</a>
		                        </h4>
		                     </div>
		                     <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
		                        <div class="panel-body">
		                           <#if segmentationTagList?exists && segmentationTagList?has_content>
		                             <#list segmentationTagList as segmentationTag>
		                             	<@displayRow 
				                           label = segmentationTag.tagName
				                           value = segmentationTag.tagId
				                           isTag = true
				                           />
		                             </#list>
		                           </#if>
		                        </div>
		                     </div>
		                   </div>
                        <#elseif templateTagType.templateTagTypeId == "CUSTOM_FIELD">
                           <div class="panel panel-default">
		                     <div class="panel-heading" role="tab" id="headingTwo">
		                        <h4 class="panel-title">
		                           <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
		                           ${uiLabelMap.attribute!}</a>
		                        </h4>
		                     </div>
		                     <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
		                        <div class="panel-body">
		                           <#if attributeTagList?exists && attributeTagList?has_content>
		                             <#list attributeTagList as attributeTag>
		                             	<@displayRow 
				                           label = attributeTag.tagName
				                           value = attributeTag.tagId
				                           isTag = true
				                           />
		                             </#list>
		                           </#if>
		                        </div>
		                     </div>
		                   </div>
                        <#elseif templateTagType.templateTagTypeId == "ECONOMIC_METRIC">
                           <div class="panel panel-default">
		                     <div class="panel-heading" role="tab" id="headingTwo">
		                        <h4 class="panel-title">
		                           <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
		                           ${uiLabelMap.economicMetrics!}</a>
		                        </h4>
		                     </div>
		                     <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
		                        <div class="panel-body">
		                           <#if economicTagList?exists && economicTagList?has_content>
		                             <#list economicTagList as economicTag>
		                             	<@displayRow 
				                           label = economicTag.tagName
				                           value = economicTag.tagId
				                           isTag = true
				                           />
		                             </#list>
		                           </#if>
		                        </div>
		                     </div>
		                   </div>
                        </#if>
                     </#list>
                 </#if>
                  
                  
               </div>
            </div>
            <div class="col-md-9 col-sm-9 padding-l">
               <@textareaLarge
               id="textContent"
               groupId = "textDisplay"
               label=uiLabelMap.text
               rows="3"
               value = templateMasterData.textContent
               dataError="Please enter text"
               required = false
               />
               <#if templateMasterData.templateFormContent?exists>
	               <#assign isBase64 = Static["org.apache.commons.codec.binary.Base64"].isBase64(templateMasterData.templateFormContent?if_exists) />
			       <#if isBase64>
			         <#assign template = Static["org.ofbiz.base.util.Base64"].base64Decode(templateMasterData.templateFormContent?if_exists) />
			       <#else>
			         <#assign template = templateMasterData.templateFormContent?if_exists />
			       </#if>
		       </#if>
               <div class="clearfix"> </div>
               <@textareaLarge
               id="templateFormContent"
               groupId = "htmlDisplay"
               label=uiLabelMap.html
               rows="3"
               value = template
               required = false
               txareaClass = "ckeditor"
               />
            </div>
         </div>
         <@fromActions showCancelBtn=true offsetSize="3"/>
      </div> <!-- end of template tab-->
      </form>
      
      
      <div id="tagConfiguration" class="tab-pane fade">
       <div class="row ml-2 mr-2">
		   <div class="col-md-8 col-sm-8 form-horizontal">
		         <div class="row mb-2 mt-2 ">
		            <div class="col-md-4 col-sm-4">
					   <@simpleDropdownInput 
						    id="templateTagType"
							options=templateTagTypeList
							required=false
							onchange="javascript:getTemplateTags();"
							value=""
							allowEmpty=true
							dataLiveSearch=true
							emptyText = "Tag Type"
							/>
		            </div>
		            <form method="post" action="addTemplateTags" id="addTemplateTagForm" name="addTemplateTagForm" class="form-horizontal" name="" novalidate="novalidate" data-toggle="validator">
		      			<input type="hidden" id="templateTagJson" name="templateTagJson"/>
		      			<input type="hidden" id="templateId" name="templateId" value="${templateId?if_exists}"/>
		      			<input type="hidden" id="tagMandatory" name="tagMandatory" value="Y"/>
		      			<input type="hidden" id="templateTagTypeId" name="templateTagTypeId"/>
			            <div class="col-md-2 col-sm-2 pl-0">
			               <button type="submit" class="btn btn-sm btn-primary ">Save</button>
			            </div>
		            </form>
		         </div>
		      <div class="clearfix"> </div>
		   </div>
		</div>
		<div id="templateTags" style="height:300px;"></div>
		<div class="clearfix"> </div>
        
      </div><!--end of Tag configuration-->
      
   </div>
   <!-- tab content end-->
   
<script type="text/javascript">
   function mailFormatVal(mailFormat)
   {
	   if(mailFormat == 0){
	    $("#textDisplay").show();
	    $("#htmlDisplay").hide();
	   }else if(mailFormat == 1){
	    $("#textDisplay").show();
	    $("#htmlDisplay").show();
	   }else if(mailFormat == 2){
	    $("#textDisplay").show();
	    $("#htmlDisplay").show();
	   }
   }
   
   $('#TemplateFrom').validator().on('submit', function (e) {
	   if (e.isDefaultPrevented()) {
	     // handle the invalid form...
	     //alert("calling.....");
	   } else {
	     // everything looks good!
	     <#if !templateMasterData.templateFormContent ? has_content >
	        $("#templateFormContent").text("");
	        var htmlContent = CKEDITOR.instances["templateFormContent"].getData();
	        $("#templateFormContent").text(htmlContent); 
	        </#if>
	   }
   });
   
   function loadPreTemplate(templateId){
     var value = $('#preTemp').val();
     var currentReq = window.location.pathname; 
    var url_array = currentReq.split( '/' );
    var request_str = url_array.pop();
    
    if(value!=null && value!=""){
         window.location.href = "<@ofbizUrl>"+request_str+"?templateId="+value+"</@ofbizUrl>";
    }
    else{
      window.location.href = "<@ofbizUrl>"+request_str+"</@ofbizUrl>";
    }
   }
   $(".templateextra").click(function(){
    var selTab = $(this).attr('data-target');
    var selTabId = $(this).attr('id');
    $('.nav-tabs a[href="#' + selTab + '"]').tab('show');
    
    $('#extra').find('a').each(function() {
    		var tabId = $(this).attr("id");
   		if(selTabId == tabId){
   			$('#'+tabId).addClass('d-none');
   		} else{
   			$('#'+tabId).removeClass('d-none');
   		}
    });
   });
   
   $(".nav-item").find("a").click(function(){
   var selectedTabId = $(this).attr("href");
   selectedTabId = selectedTabId.substring(1, selectedTabId.length);
   if("Template" == selectedTabId){
   $('#tempContent').addClass('d-none');
   $('#tempDetails').removeClass('d-none');
   }
   if("Details" == selectedTabId){
   $('#tempDetails').addClass('d-none');
   $('#tempContent').removeClass('d-none');
   }
   });
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    var isExists =false;
    $('a[data-toggle="tab"]').each(function() {
	   	var tabId = $(this).attr("href");
	   	if(tabId == activeTab){
	   	  isExists = true;
	   	}
	});
    if(isExists && activeTab){
        $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
    } else{
    	$('.nav-tabs a[href="#Details"]').tab('show');
    }

	getTemplateTags();
});

var dsl = '';
function getTemplateTags(){
	var templateTagType = $("#templateTagType").val();
	$('#templateTags').empty();
	$.post("getTemplateTagsAjax",{"templateTagTypeId":templateTagType,"templateId":"${templateId?if_exists}"},function(data){
		dsl = $('#templateTags').DualSelectList({
			'candidateItems' : data.candidateItems,
			'selectionItems' : data.selectionItems
		});
	});

}

$('#addTemplateTagForm').on('submit', function (e) {
	   var res = dsl.getSelection();
		var str = '';
		var selectedTags = [];
		var jsonObj = [];
		for (var n=0; n<res.length; ++n){
			var item = {}
	        item ["id"] = res[n];
	        jsonObj.push(item);
		}
		var templateTagType = $("#templateTagType").val();
		$('#templateTagTypeId').val(templateTagType);
		var selectedTagJSON = JSON.stringify(jsonObj);
		selectedTagJSON = selectedTagJSON.replace(/\\/g, "");
    	selectedTagJSON = selectedTagJSON.replace(/"/g, "quot;");
		$('#templateTagJson').val(selectedTagJSON);
});

</script>