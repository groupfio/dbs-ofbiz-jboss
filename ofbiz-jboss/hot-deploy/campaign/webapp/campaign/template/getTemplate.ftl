<#assign templateId = requestParameters.templateId?if_exists>
<#assign templateMasterData =delegator.findOne("TemplateMaster", {"templateId" : templateId}, false)?if_exists>
<#if templateMasterData?exists && templateMasterData?has_content>
	<#if templateMasterData.templateFormContent?has_content>
		<div contentEditable="false" style=" pointer-events: none;"/>
		<#assign isBase64 = Static["org.apache.commons.codec.binary.Base64"].isBase64(templateMasterData.templateFormContent?if_exists) />
		<#if isBase64>
			<#assign template = Static["org.ofbiz.base.util.Base64"].base64Decode(templateMasterData.templateFormContent?if_exists) />
		<#else>
			<#assign template = templateMasterData.templateFormContent?if_exists />
		</#if>

		${StringUtil.wrapString(template)}
		</div>
	<#else>
		<span class="tableheadtext">${templateMasterData.textContent?if_exists}</span>
	</#if>
</#if>