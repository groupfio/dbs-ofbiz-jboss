<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#assign templateId = requestParameters.templateId?if_exists>
<#assign templateMasterData = delegator.findOne("TemplateMaster", {"templateId" : "${templateId?if_exists}"}, false)?if_exists>
<@sectionHeader title="${uiLabelMap.viewTemplate!}" />
<ul class="nav nav-tabs">
   <li class="nav-item"><a data-toggle="tab" href="#templateView" class="nav-link">Template</a></li>
   <li class="nav-item"><a data-toggle="tab" href="#tagTempConfig" class="nav-link">Tag Configuration </a></li>
</ul>
<div class="tab-content">
   <div id="templateView" class="tab-pane fade">
      <@sectionTitle title="${uiLabelMap.template!}"/>
      <#if templateMasterData?exists && templateMasterData?has_content>
      <div class="row padding-r">
         <div class="col-md-6 col-sm-6 form-horizontal">
            <@displayRow 
            label=uiLabelMap.name
            value="${templateMasterData.templateName!} [${templateMasterData.templateId!}]"
            />
            <#if templateMasterData.templateType == "EMAIL_BLAST">
            <@displayRow 
            label=uiLabelMap.senderFullName
            value=templateMasterData.senderName
            />
            <@displayRow 
            label=uiLabelMap.replyToFullName
            value=templateMasterData.replayToName
            />
            <@displayRow 
            label=uiLabelMap.description
            value=templateMasterData.description
            />
            <div class="form-group row">
               <label  class="col-sm-4 col-form-label">${uiLabelMap.template!}</label>
               <div class="col-sm-7">
                  <label class="col-form-label input-sm fw">
                     <#--<a href="#" data-toggle="modal" data-target="#ViewTemp">View Template</a>-->
                     <a href="getTemplate?templateId=${templateMasterData.templateId?if_exists}" target="_blank">View Template</a>
                  </label>
                  <input type="hidden" id="templateId" name="templateId" value="${templateMasterData.templateId?if_exists}"/>
               </div>
            </div>
            <#else>
            <@displayRow 
            label=uiLabelMap.text
            value=templateMasterData.textContent
            />
            </#if>
            <#if templateMasterData.fromDate?exists>
            <#assign fromDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(templateMasterData.fromDate?if_exists, "dd-MM-yyyy")?if_exists/>
            </#if>
            <@displayRow 
            label=uiLabelMap.startDate
            value=fromDate
            />
         </div>
         <div class="col-md-6 col-sm-6 form-horizontal">
            <@displayRow 
            label=uiLabelMap.subject
            value=templateMasterData.subject
            />
            <#if templateMasterData.templateType == "EMAIL_BLAST">
            <@displayRow 
            label=uiLabelMap.senderEmail
            value=templateMasterData.senderEmail
            />
            <@displayRow 
            label=uiLabelMap.replyToMail
            value=templateMasterData.senderEmailReplay
            />
            </#if>
            <#if templateMasterData.templateType == "EMAIL_BLAST">
            <#assign templateType="EMAIL BLAST"/> 
            <#else>
            <#assign templateType = "${templateMasterData.templateType?if_exists}" />
            </#if>
            <@displayRow 
            label=uiLabelMap.templateType
            value=templateType
            />
            <#if templateMasterData.templateType == "EMAIL_BLAST">
            <@displayRow 
            label=uiLabelMap.text
            value=templateMasterData.textContent
            />
            </#if>
            <#if templateMasterData.thruDate?exists>
            <#assign thruDate = Static["org.ofbiz.base.util.UtilDateTime"].toDateString(templateMasterData.thruDate?if_exists, "dd-MM-yyyy")?if_exists/>
            </#if>
            <@displayRow 
            label=uiLabelMap.endDate
            value=thruDate
            />
         </div>
      </div>
      </#if>
      <div class="col-md-12 col-sm-12">
         <div class="form-group row">
            <div class="offset-sm-2 col-sm-9">
               <a type="submit" class="btn btn-sm btn-primary mt" href="<@ofbizUrl>findTemplates</@ofbizUrl>">${uiLabelMap.back!}</a>
               <#--<a href="<#if templateMasterData.templateType == "EMAIL_BLAST"><@ofbizUrl>editTemplate?templateId=${templateId?if_exists}</@ofbizUrl><#elseif templateMasterData.templateType == "SMS" || templateMasterData.templateType == "MMS"><@ofbizUrl>editsmsTemplate?templateId=${templateId?if_exists}</@ofbizUrl></#if>" class="btn btn-sm btn-primary mt">${uiLabelMap.edit!}</a>-->	
            </div>
         </div>
      </div>
   </div>
   <div id="tagTempConfig" class="tab-pane fade">
      <div class="clearfix"> </div>
      <div class="panel-group mt-2" id="templateTagMenu" role="tablist" aria-multiselectable="true">
         <#if templateTagTypes?exists && templateTagTypes?has_content>
         <#list templateTagTypes as templateTagType>
         <#if templateTagType.templateTagTypeId == "STANDARD">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
               <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
                  ${uiLabelMap.standard!}</a>
               </h4>
            </div>
            <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
               <div class="panel-body">
                  <#-- <#if standardTagList?exists && standardTagList?has_content>
                  <#list standardTagList as standardTag>
                  <@displayRow 
                  label = standardTag.tagName
                  value = standardTag.tagId
                  isTag = true
                  />
                  </#list>
                  </#if>-->
                  <table class="table table-striped" id="tagdatatable">
                     <thead>
                        <tr>
                           <th>Tag ID</th>
                           <th>Tag Name</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <#if standardTagList?has_content>
                        <#list standardTagList as standardTag>
                        <tr>
                           <td>${standardTag.tagName!}</td>
                           <td>[${standardTag.tagId!}]</td>
                           <td class="text-center">
                              <div class="">
                                 <a href="<@ofbizUrl>editTemplate?templateId=${templateId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary">Configure</a>
                              </div>
                           </td>
                        </tr>
                        </#list>
                        </#if>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <#elseif templateTagType.templateTagTypeId == "SEGMENTATION">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
               <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
                  ${uiLabelMap.segmentation!}</a>
               </h4>
            </div>
            <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
               <div class="panel-body">
                  <#-- <#if segmentationTagList?exists && segmentationTagList?has_content>
                  <#list segmentationTagList as segmentationTag>
                  <@displayRow 
                  label = segmentationTag.tagName
                  value = segmentationTag.tagId
                  isTag = true
                  />
                  </#list>
                  </#if> -->
                  <table class="table table-striped" id="tagdatatable">
                     <thead>
                        <tr>
                           <th>Tag ID</th>
                           <th>Tag Name</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <#if segmentationTagList?has_content>
                        <#list segmentationTagList as segmentationTag>
                        <tr>
                           <td>${segmentationTag.tagName!}</td>
                           <td>[${segmentationTag.tagId!}]</td>
                           <td class="text-center">
                              <div class="">
                                 <a href="<@ofbizUrl>editTemplate?templateId=${templateId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary">Configure</a>
                              </div>
                           </td>
                        </tr>
                        </#list>
                        </#if>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <#elseif templateTagType.templateTagTypeId == "CUSTOM_FIELD">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
               <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
                  ${uiLabelMap.attribute!}</a>
               </h4>
            </div>
            <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
               <div class="panel-body">
                  <#-- <#if attributeTagList?exists && attributeTagList?has_content>
                  <#list attributeTagList as attributeTag>
                  <@displayRow 
                  label = attributeTag.tagName
                  value = attributeTag.tagId
                  isTag = true
                  />
                  </#list>
                  </#if> -->
                  <table class="table table-striped" id="tagdatatable">
                     <thead>
                        <tr>
                           <th>Tag ID</th>
                           <th>Tag Name</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <#if attributeTagList?has_content>
                        <#list attributeTagList as attributeTag>
                        <tr>
                           <td>${attributeTag.tagName!}</td>
                           <td>[${attributeTag.tagId!}]</td>
                           <td class="text-center">
                              <div class="">
                                 <a href="<@ofbizUrl>editTemplate?templateId=${templateId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary">Configure</a>
                              </div>
                           </td>
                        </tr>
                        </#list>
                        </#if>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <#elseif templateTagType.templateTagTypeId == "ECONOMIC_METRIC">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
               <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#templateTagMenu" href="#${templateTagType.templateTagTypeId?if_exists}" aria-expanded="true" aria-controls="collapseOne">
                  ${uiLabelMap.economicMetrics!}</a>
               </h4>
            </div>
            <div id="${templateTagType.templateTagTypeId?if_exists}" class="panel-collapse collapse" data-parent="#templateTagMenu" aria-labelledby="headingOne">
               <div class="panel-body">
                  <#-- <#if economicTagList?exists && economicTagList?has_content>
                  <#list economicTagList as economicTag>
                  <@displayRow 
                  label = economicTag.tagName
                  value = economicTag.tagId
                  isTag = true
                  />
                  </#list>
                  </#if> -->
                  <table class="table table-striped" id="tagdatatable">
                     <thead>
                        <tr>
                           <th>Tag ID</th>
                           <th>Tag Name</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <#if economicTagList?has_content>
                        <#list economicTagList as economicTag>
                        <tr>
                           <td>${economicTag.tagName!}</td>
                           <td>[${economicTag.tagId!}]</td>
                           <td class="text-center">
                              <div class="">
                                 <a href="<@ofbizUrl>editTemplate?templateId=${templateId?if_exists}</@ofbizUrl>" class="btn btn-xs btn-primary">Configure</a>
                              </div>
                           </td>
                        </tr>
                        </#list>
                        </#if>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         </#if>
         </#list>
         </#if>
      </div>
   </div>
</div>
<!-- /.container -->
<#--<div id="ViewTemp" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
       Modal content
      <div class="modal-content">
      <#if templateMasterData?exists && templateMasterData?has_content>
         <div class="modal-header">
            <h5 class="modal-title">${templateMasterData.templateName} [${templateMasterData.templateId}]</h5>
            <button type="reset" class="close" data-dismiss="modal">&times;</button> 
         </div>
         <div class="modal-body">
            <div class="card-header">
               <#if templateMasterData.templateFormContent?has_content>
               <div contentEditable="false" style=" pointer-events: none;">
                  <#assign isBase64 = Static["org.apache.commons.codec.binary.Base64"].isBase64(templateMasterData.templateFormContent?if_exists) />
                  <#if isBase64>
                  <#assign template = Static["org.ofbiz.base.util.Base64"].base64Decode(templateMasterData.templateFormContent?if_exists) />
                  <#else>
                  <#assign template = templateMasterData.templateFormContent?if_exists />
                  </#if>
                   <#assign templateEncode = Static["org.ofbiz.base.util.Base64"].base64Encode(templateMasterData.templateFormContent?if_exists) />
                  ${StringUtil.wrapString(template)}
               </div>
               <#else>
               <span class="tableheadtext">${templateMasterData.textContent?if_exists}</span>
               </#if>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
         </div>
      </#if>
      </div>
   </div>
</div>-->
<script>
    $(document).ready(function(){
     $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
         localStorage.setItem('activeTab', $(e.target).attr('href'));
     });
     var isExists =false;
     var activeTab = localStorage.getItem('activeTab');
     $('a[data-toggle="tab"]').each(function() {
     	var tabId = $(this).attr("href");
     	if(tabId == activeTab){
     	  isExists = true;
     	}
   });
     if(isExists && activeTab){
         $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
     } else{
     	$('.nav-tabs a[href="#templateView"]').tab('show');
     }
   
   });
</script>