/*
 * Copyright (c) Open Source Strategies, Inc.
 * 
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import javolution.util.FastMap;
import javolution.util.FastList;
import java.sql.Timestamp;
import org.ofbiz.entity.GenericValue;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import org.fio.campaign.util.CampaignUtil;

campaignTypes = delegator.findAll("CampaignType",false);
if(campaignTypes!=null){
	context.put("campaignTypes",campaignTypes);
}
campaignName = parameters.get("campaignName");
statusId = parameters.get("statusId");
campaignTypeId = parameters.get("campaignType");
fromDate = parameters.get("fromDate");
thruDate = parameters.get("thruDate");
campaignId = parameters.get("campaignId");
createdBy = parameters.get("createdBy");

createdDate = parameters.get("createdDate");
masterParentCampaignId = parameters.get("masterParentCampaignId");
campaignHierarchyType = parameters.get("campaignHierarchyType");

SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
andExprs = FastList.newInstance();
if (UtilValidate.isNotEmpty(campaignId)) 
{
	context.put("campaignId",campaignId);
}
if (UtilValidate.isNotEmpty(campaignName)) 
{
	context.put("campaignName",campaignName);
}
if (UtilValidate.isNotEmpty(statusId)) 
{
	context.put("statusId",statusId);
}
if (UtilValidate.isNotEmpty(campaignTypeId)) 
{
	context.put("campaignTypeIdInd",campaignTypeId);
}
if (UtilValidate.isNotEmpty(fromDate)) 
{
	context.put("fromDate",fromDate);
}
if (UtilValidate.isNotEmpty(thruDate)) 
{ 
	context.put("thruDate",thruDate);
}

if (UtilValidate.isNotEmpty(campaignHierarchyType))
{
	context.put("campaignHierarchyType",campaignHierarchyType);
}
if (UtilValidate.isNotEmpty(masterParentCampaignId))
{
	context.put("masterParentCampaignId",masterParentCampaignId);
}



//created by 
if (UtilValidate.isNotEmpty(createdBy)) 
{ 
	context.put("createdBy",createdBy);
}

//created Date
if (UtilValidate.isNotEmpty(createdDate)) 
{ 
	context.put("createdDate",createdDate);
}

isDripCampaign = UtilValidate.isNotEmpty(request.getParameter("isDripCampaign")) ? request.getParameter("isDripCampaign") : context.get("isDripCampaign");

context.put("isDripCampaign", isDripCampaign);

