import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.ofbiz.entity.condition.EntityCondition;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("CustomFieldUiLabels", locale);

context.put("showDripCampaign", true);

marketingCampaignId = request.getParameter("marketingCampaignId");
context.put("marketingCampaignId", marketingCampaignId);

marketingCampaign = context.get("marketingCampaign");
if (UtilValidate.isEmpty(marketingCampaign)) {
	marketingCampaign = delegator.findOne("MarketingCampaign", [marketingCampaignId : marketingCampaignId], false);
}

if (UtilValidate.isNotEmpty(marketingCampaign)) {
	segmentCode = marketingCampaign.getString("campaignTypeId")+"_"+marketingCampaignId;
	
	context.put("segmentCodeId", segmentCode);
	
	//context.put("showChildCampaign", true);
	
	//context.put("parentCampaignId", marketingCampaign.getString("marketingCampaignId"));
}
/*
groupId = request.getParameter("groupId");
context.put("groupId", groupId);

condition = UtilMisc.toMap("groupType", GroupType.SEGMENTATION);
if (UtilValidate.isNotEmpty(groupId)) {
	condition.put("groupId", groupId);
}

cond = EntityCondition.makeCondition(condition);
println("cond>>>> "+cond);
customFieldList = delegator.findList("CustomFieldSummary", cond, null, ["sequenceNumber"], null, false);
context.put("customFieldList", customFieldList);
*/
