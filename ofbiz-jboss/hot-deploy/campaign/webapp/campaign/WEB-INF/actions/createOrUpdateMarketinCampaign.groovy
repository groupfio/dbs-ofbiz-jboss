/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator; 
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import org.groupfio.custom.field.util.DataHelper;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.*;


userLogin = request.getAttribute("userLogin");
typeId=request.getParameter("campaignTypeId");
classifyId=request.getParameter("classifyId");
if(classifyId!=null)
{
    context.put("classifyId",classifyId);
}
marketingCampaign = null;
requestUrl=request.getRequestURL();
context.put("requestUrl",requestUrl);
if(requestUrl.toString().indexOf("updateMarketingCampaignForm")!=-1)
{
	context.put("isCreate",false);
}
else{
	context.put("isCreate",true);
}
List campaignTypes = delegator.findAll("CampaignType",false);
if(campaignTypes!=null){
String typeIdCamp=campaignTypes.campaignTypeId;
String description=campaignTypes.description;

    campaignTypesMap=DataHelper.getDropDownOptions(campaignTypes,"campaignTypeId", "description");
	context.put("campaignTypes",campaignTypesMap);
}

currencyUom = delegator.findByAnd("Uom", [uomTypeId: "CURRENCY_MEASURE"], null, false);
if(currencyUom!=null)
{
	context.put("currencyUom",currencyUom);
}

marketingCampaignId = request.getParameter("marketingCampaignId");
if(marketingCampaignId==null){
	marketingCampaignId = parameters.get("marketingCampaignId");
}
continousValidation = false;
if(marketingCampaignId!=null){
	marketingCampaign = delegator.findOne("MarketingCampaign", [marketingCampaignId : marketingCampaignId], false);
	if(marketingCampaign!=null)
		context.put("marketingCampaign",marketingCampaign);
		parentIdCamp=marketingCampaign.getString("parentCampaignId");
		masterCampId=marketingCampaign.getString("masterCampaignId");
		campaignTypeId=marketingCampaign.getString("campaignTypeId");
		convertedCustomFieldId=campaignTypeId+"_"+marketingCampaignId+"_CONVERTED";
        getConvertedCount= delegator.findCountByCondition("CustomFieldPartyClassification",
						EntityCondition.makeCondition("customFieldId", EntityOperator.EQUALS, convertedCustomFieldId), null,
						null);
        context.put("convertedCount",getConvertedCount);
		if(parentIdCamp!=null){
		    marketingCampaignParent = delegator.findOne("MarketingCampaign", [marketingCampaignId : parentIdCamp], false);
		    if(marketingCampaignParent!=null){
		        context.put("marketingCampaignParent",marketingCampaignParent);
		    }
		}
		salesTriggerType=marketingCampaign.get("salesTriggerType");
        context.put("salesTriggerType",salesTriggerType);
        
		if(masterCampId!=null){
		    marketingCampaignMaster = delegator.findOne("MarketingCampaign", [marketingCampaignId : masterCampId], false);
		    if(marketingCampaignMaster!=null){
		        context.put("marketingCampaignMaster",marketingCampaignMaster);
		    }
		}
		campFinancialValues = delegator.findByAnd("MarketingCampaignFinancialMetrics",UtilMisc.toMap("marketingCampaignId", marketingCampaignId), null, false);
		if(campFinancialValues.size()>0){
		campFinancialValue=campFinancialValues.get(0);
		//campFinancialValues=delegator.findOne("FinancialMetricConfig", [financialConfigId : financialConfigId], false);
		//context.put("financialConfigId",financialConfigId);
		context.put("campFinancialValues",campFinancialValue);
		}
		
		campCountMember=0;
        conditionsCampList = new EntityConditionList( UtilMisc.toList(
            new EntityExpr("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId),
            new EntityExpr("contactListTypeId", EntityOperator.EQUALS, "CAMPAIGN_LIST")
        ), EntityOperator.AND)
       campListCountListId=delegator.findList("ContactList",conditionsCampList, null, null, null, false);
       if(campListCountListId.size()>0){
       campListId=campListCountListId.get(0).getString("contactListId");
         if(campListId!=null){
            campCountMember=delegator.findCountByCondition("CampaignContactListParty", EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS,campListId ), null, null);
   
         }
      }
      context.put("campCountMember", campCountMember)
      Long sendCount = delegator.findCountByCondition("MarketingCampaignSendDetails",
						EntityCondition.makeCondition("campaignId", EntityOperator.EQUALS, marketingCampaignId), null,
						null);
	context.put("sendCount",sendCount);
     if(marketingCampaign != null && marketingCampaign.size() > 0) {
         product=marketingCampaign.getString("product");
         subProduct=marketingCampaign.getString("subProduct");
		 context.put("product", product);
		 context.put("subProduct", subProduct);
     }
     classificationType = marketingCampaign.getString("classificationType");
     if(UtilValidate.isNotEmpty(classificationType) && "CONTINUOUS".equals(classificationType)) {
         continousValidation = true;
     }
}
context.put("continousValidation", continousValidation);
campaigenStatuses = delegator.findByAnd("StatusItem", [statusTypeId: "MKTG_CAMP_STATUS"], UtilMisc.toList("sequenceId"), false);
if(campaigenStatuses!=null){
	context.put("campaigenStatuses",campaigenStatuses);
}

LocalDateTime currentTime = LocalDateTime.now();
LocalDate dateFront = currentTime.toLocalDate();

currentTimestamp = UtilDateTime.nowTimestamp();
context.put("currentTimestamp",currentTimestamp);
SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
LocalDate date1 = currentTime.toLocalDate();
currentEmmTime = sdf.format(currentTimestamp);
context.put("currentEmmTime",currentEmmTime);


selectedCampaignType = parameters.get("campaignTypeId");
if("SMS".equals(selectedCampaignType)){
	selectedCampaignType="PHONE_CALL";
}
context.put("selectedCampaignType",selectedCampaignType);
contactLiveLists = delegator.findList("ContactList",EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, "LIVE"), null, null, null, false);
context.put("contactLiveLists",contactLiveLists);

contactTestLists = delegator.findList("ContactList",EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, "TEST"), null, null, null, false);
context.put("contactTestLists",contactTestLists); 

/*if("EMAIL"==typeId)
{
	typeId="EMAIL_BLAST";
}
if(typeId!=null){   
	templatesList = delegator.findList("TemplateMaster",EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, typeId), null, null, null, false);
}else if (marketingCampaign.campaignTypeId!=null){
	if("EMAIL".equals(marketingCampaign.campaignTypeId)){
		tempTypeDesc="EMAIL_BLAST";
	}else if("PHONE_CALL".equals(marketingCampaign.campaignTypeId)){
		tempTypeDesc="SMS";
	}else{
		tempTypeDesc=marketingCampaign.campaignTypeId;
	}
	templatesList = delegator.findList("TemplateMaster",EntityCondition.makeCondition("templateType",EntityOperator.EQUALS, tempTypeDesc), null, null, null, false);

}
context.put("templatesList",templatesList); 

*/


List campIdListsToView=new ArrayList();
marketingCampaignsList = delegator.findAll("MarketingCampaign",true);
for(GenericValue marketingCampaigns : marketingCampaignsList){
	Map paramValue = FastMap.newInstance();
	campaignName=marketingCampaigns.getString("campaignName");
	paramValue.put("campaignName",campaignName);
	mktCampaignId=marketingCampaigns.getString("marketingCampaignId");
	paramValue.put("marketingCampaignId",mktCampaignId);	
	startDisp="";
	endDisp="";
	startToDisplay=marketingCampaigns.getString("startDate");
	if(UtilValidate.isNotEmpty(startToDisplay)){
	 startDisp=startToDisplay.substring(0, 10);
	}
	paramValue.put("startDate",startDisp);
	
	endToDisplay=marketingCampaigns.getString("endDate");
	if(UtilValidate.isNotEmpty(endToDisplay)){
	 endDisp=endToDisplay.substring(0, 10);
	}
	paramValue.put("endDate",endDisp);

	campIdListsToView.add(paramValue);

}
context.put("marketingCampaigns",campIdListsToView); 




campTestList=delegator.findByAnd("MarketingCampaignContactList",UtilMisc.toMap("marketingCampaignId", marketingCampaignId,"contactPurposeType","TEST"), null, false);
if(campTestList.size()>0)
{
	testIdList=campTestList.get(0).getString("contactListId");
	context.put("testIdList",testIdList); 
}

campProdList=delegator.findByAnd("MarketingCampaignContactList",UtilMisc.toMap("marketingCampaignId", marketingCampaignId,"contactPurposeType","LIVE"), null, false);
if(campProdList.size()>0)
{
	prodIdList=campProdList.get(0).getString("contactListId");
	context.put("prodIdList",prodIdList);
}


FinancialMetricConfigList=delegator.findAll("FinancialMetricConfig",false);
context.put("FinancialMetricConfigList",FinancialMetricConfigList);

Map timeZoneList=new HashMap();
timeZoneList.put("SGT","SGT");
timeZoneList.put("IST","IST");
timeZoneList.put("EST","EST");
context.put("timeZoneList",timeZoneList);

if(UtilValidate.isNotEmpty(marketingCampaignId)) {
    GenericValue campCLSummary = EntityQuery.use(delegator).from("CampaignContactListSummary")
        .where("campaignId", marketingCampaignId, "attrName", "RM_APPROVED").queryFirst();
    if(campCLSummary != null && campCLSummary.size() > 0) {
        context.put("rmApproved", campCLSummary.getString("attrValue"));
    }
    
    
}

Map roleList=new HashMap();
roleList.put("LEAD","Lead");
roleList.put("ACCOUNT","Account");
//roleList.put("CONTACT","Contact");
context.put("roleList",roleList);

enumerationSalesTriggerList = EntityQuery.use(delegator).from("Enumeration").where("enumTypeId","SALES_TRIGGER").orderBy("sequenceId").queryList();
context.put("enumerationSalesTriggerList", DataHelper.getDropDownOptions(enumerationSalesTriggerList, "enumId", "description"));

//Select Product
enumerationProductsList = EntityQuery.use(delegator).from("Enumeration").where("enumTypeId","MAIN_PRODUCT").orderBy("sequenceId").queryList();
context.put("enumerationProductsList", DataHelper.getDropDownOptions(enumerationProductsList, "enumId", "description"));

