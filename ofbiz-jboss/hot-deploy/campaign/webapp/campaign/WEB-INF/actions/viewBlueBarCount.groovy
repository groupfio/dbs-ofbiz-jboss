/*
     * Copyright (c) Open Source Strategies, Inc.
     * 
     * Opentaps is free software: you can redistribute it and/or modify it
     * under the terms of the GNU Affero General Public License as published
     * by the Free Software Foundation, either version 3 of the License, or
     * (at your option) any later version.
     *
     * Opentaps is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * GNU Affero General Public License for more details.
     *
     * You should have received a copy of the GNU Affero General Public License
     * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
     */
    
    import org.ofbiz.entity.condition.*;
    import org.ofbiz.entity.util.EntityUtil;
    import org.ofbiz.base.util.Debug;
    import org.ofbiz.base.util.UtilMisc;
    import org.ofbiz.base.util.UtilValidate;
    import org.ofbiz.base.util.UtilDateTime;
    
    import org.fio.campaign.common.UtilCommon;
    import org.fio.campaign.util.StringUtil;
    
    import java.util.*;
    
    import javolution.util.FastList;
    import javolution.util.FastMap;
    
    import org.ofbiz.entity.jdbc.SQLProcessor;
    import java.sql.ResultSet;
    
    import org.ofbiz.entity.util.EntityFindOptions;
    import org.ofbiz.entity.util.EntityQuery;
    import org.ofbiz.entity.util.EntityUtil;
    import org.ofbiz.entity.GenericValue;
    
    import java.sql.Timestamp;
    import java.text.SimpleDateFormat
    
    import org.fio.campaign.util.CampaignUtil;
    import org.fio.crm.util.DataHelper;
    import org.fio.campaign.util.LoginFilterUtil;
    import org.fio.campaign.util.ResponseUtils;
    
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import javax.servlet.http.HttpSession;
    
    
    marketingCampaignId = request.getParameter("marketingCampaignId");
    if (UtilValidate.isNotEmpty(marketingCampaignId)) {
    
     if (marketingCampaignId != null && marketingCampaignId.contains("?")) {
      marketingCampaignId = marketingCampaignId.substring(0, marketingCampaignId.indexOf("?"));
     }
     marketingCampaign = delegator.findOne("MarketingCampaign", [marketingCampaignId: marketingCampaignId], false);
    
     security = request.getAttribute("security");
     userLogin = request.getAttribute("userLogin");
     HttpSession session = request.getSession();
     userLoginId = userLogin.getAt('partyId');
	 String campaignContactId = null;
     marketingCampCL = EntityQuery.use(delegator).from("ContactList")
      .where("marketingCampaignId", marketingCampaignId, "contactListTypeId", "CAMPAIGN_LIST").queryFirst();
     if (marketingCampCL != null && marketingCampCL.size() > 0) {
      campaignContactId = marketingCampCL.getString("contactListId");
     }
    
     List < String > rmList = new ArrayList < String > ();
     String sqlQureycond = null;
     if (LoginFilterUtil.checkEmployeePosition(delegator, userLoginId)) {
      Map < String, Object > dataSecurityMetaInfo = (Map < String, Object > ) session.getAttribute("dataSecurityMetaInfo");
      if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {
       List < String > lowerPositionPartyIds = (List < String > ) dataSecurityMetaInfo.get("lowerPositionPartyIds");
       rmList = LoginFilterUtil.getRmList(delegator, lowerPositionPartyIds);
    
      }
      criteriaValue = StringUtil.toList(rmList, "");
      sqlQureycond = "SELECT  COUNT(party_id) AS campMemberCount, SUM(IF(cclp.IS_APPROVED='Y',1,0)) AS 'rmApprovedCount',SUM(IF(cclp.sent='Y',1,0)) AS 'sent',SUM(IF(cclp.opened='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open',SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted',SUM(IF(cclp.subscribed='Y',1,0)) AS 'subscribed', SUM(IF(cclp.unsubscribed='Y',1,0)) AS 'unsubscribed', SUM(IF(cclp.bounced='Y',1,0)) AS 'bounced', SUM(IF(cclp.hard_bounce='Y',1,0)) AS 'hardBounce',SUM(IF(cclp.soft_bounce='Y',1,0)) AS 'softBoun',SUM(IF(cclp.blacked='Y',1,0))  AS 'black'   FROM campaign_contact_list_party cclp WHERE cclp.contact_list_id ='" + campaignContactId + "' AND cclp.csr_party_id IN (" + criteriaValue + ")";
    
    
     } else {
      sqlQureycond = "SELECT  COUNT(party_id) AS campMemberCount, SUM(IF(cclp.IS_APPROVED='Y',1,0)) AS 'rmApprovedCount',SUM(IF(cclp.sent='Y',1,0)) AS 'sent',SUM(IF(cclp.opened='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open',SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted', SUM(IF(cclp.subscribed='Y',1,0)) AS 'subscribed', SUM(IF(cclp.unsubscribed='Y',1,0)) AS 'unsubscribed', SUM(IF(cclp.bounced='Y',1,0)) AS 'bounced',SUM(IF(cclp.hard_bounce='Y',1,0)) AS 'hardBounce',SUM(IF(cclp.soft_bounce='Y',1,0)) AS 'softBoun',SUM(IF(cclp.blacked='Y',1,0))  AS 'black'   FROM campaign_contact_list_party cclp WHERE cclp.contact_list_id ='" + campaignContactId + "' ";
    
     }
    
     Debug.logInfo("==================== RM List :" + rmList, "");
    

	 List responseCountList = new ArrayList();
     ResultSet rs = null;
     //String sqlQurey = "SELECT  COUNT(party_id) AS campMemberCount, SUM(IF(cclp.IS_APPROVED='Y',1,0)) AS 'rmApprovedCount',SUM(IF(cclp.opened='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open',SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted', SUM(IF(cclp.unsubscribed='Y',1,0)) AS 'unsubscribed',SUM(IF(cclp.hard_bounce='Y',1,0)) AS 'boun',SUM(IF(cclp.blacked='Y',1,0))  AS 'black'   FROM campaign_contact_list_party cclp WHERE cclp.contact_list_id ='"+campaignContactId+"' AND cclp.csr_party_id IN ("+criteriaValue+")";
    
     SQLProcessor sqlProcessor = new SQLProcessor(delegator, delegator.getGroupHelperInfo("org.ofbiz"));
     String sqlQurey = sqlQureycond;
     rs = sqlProcessor.executeQuery(sqlQurey);
     Debug.logInfo("==================== Generated qurey : " + sqlQurey, "");
	 Map list = new HashMap();
     if (rs != null) {
      while (rs.next()) {
       
       campMemberCount = rs.getString("campMemberCount");
       rmApprovedCount = rs.getString("rmApprovedCount");
       open = rs.getString("open");
       sent = rs.getString("sent");
       not_open = rs.getString("not_open");
       clicked = rs.getString("clicked");
       converted = rs.getString("converted");
       unsubscribed = rs.getString("unsubscribed");
	   subscribed = rs.getString("subscribed");
       hardBounce = rs.getString("hardBounce");
       softBoun = rs.getString("softBoun");
	   bounced = rs.getString("bounced");
       black = rs.getString("black");
       list.put("campCountMember", campMemberCount);
       list.put("rmApproved", rmApprovedCount);
       list.put("openedMailCount", open);
       list.put("sentMailCount", sent);
       list.put("notOpen", not_open);
       list.put("clickedMailCount", clicked);
       list.put("convertedCount", converted);
       list.put("unSubscribed", unsubscribed);
	   list.put("subscribed", subscribed);
       list.put("softBouncedMailCount", softBoun);
       list.put("hardBouncedMailCount", hardBounce);
	   list.put("bouncedMailCount", bounced);
       responseCountList.add(list);
      }
     }
     context.put("responseCount", list);
     Debug.logInfo("==================== Blue Bar result : " + list, "");
    
    }