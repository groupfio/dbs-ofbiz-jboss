/*
 * Copyright (c) Open Source Strategies, Inc.
 * 
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.ofbiz.entity.condition.*;
import org.ofbiz.entity.util.EntityUtil;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilDateTime;
import org.fio.campaign.common.UtilCommon;
import java.util.*;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.GenericValue;
import java.sql.Timestamp;
import java.text.SimpleDateFormat

import org.fio.campaign.util.CampaignUtil;
import org.fio.crm.util.DataHelper;

activeTab = UtilValidate.isNotEmpty(request.getParameter("activeTab")) ? request.getParameter("activeTab") : request.getAttribute("activeTab");
context.put("activeTab", activeTab);

marketingCampaignId = request.getParameter("marketingCampaignId");
/*tabId=request.getParameter("tabId");
if(tabId=="tab2"){
context.put("tabId", "contact");
request.setAttribute("tabId", "");
}else{
 context.put("tabId", "");
}*/
availableCampaign = true;
if(marketingCampaignId !=null && marketingCampaignId.contains("?"))
{
	marketingCampaignId=marketingCampaignId.substring(0,marketingCampaignId.indexOf("?"));
}
marketingCampaign = delegator.findOne("MarketingCampaign", [marketingCampaignId : marketingCampaignId], false);
if (marketingCampaign == null) {
    context.put("validView", false);
    return;
}
testPublishStatus = null;
if(marketingCampaign != null && marketingCampaign.size() > 0) {
	
	testPublishStatus=marketingCampaign.getString("campTestStatus");
		context.put("testPublishStatus", testPublishStatus);
}
if(marketingCampaign != null && marketingCampaign.size() > 0) {
    endDate = marketingCampaign.get("endDate");
	startDate = marketingCampaign.get("startDate");
    statusId=marketingCampaign.getString("statusId");
    classificationTypeId=marketingCampaign.getString("classificationType");
    campaignTemplateId = marketingCampaign.getString("campaignTemplateId");
    salesTriggerType=marketingCampaign.getString("salesTriggerType");
    testPublished=marketingCampaign.getString("campTestStatus");
	String publishValidation = null;
    if(UtilValidate.isEmpty(campaignTemplateId)) {
		publishValidation = "Template not associated";
    } else {
		mktCampContactListValidation = delegator.findByAnd("MarketingCampaignContactList", [marketingCampaignId : marketingCampaignId], null, false);
		if(mktCampContactListValidation != null && mktCampContactListValidation.size() > 0) {
			contactListValidation = EntityQuery.use(delegator).from("ContactList")
			.where(EntityCondition.makeCondition([EntityCondition.makeCondition("contactListId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(mktCampContactListValidation, "contactListId", true)),
				EntityCondition.makeCondition("contactListTypeId", EntityOperator.EQUALS, "CAMPAIGN_LIST")
			])).queryFirst();
			if(contactListValidation == null || contactListValidation.size() < 1) {
				publishValidation = "Campaign List not generated";
			}
		} else {
			publishValidation = "List not associated";
		}
	}
	publishTestValidation=null;
	if(UtilValidate.isNotEmpty(publishValidation)) {
	    publishTestValidation="published already";
	}else if(UtilValidate.isNotEmpty(testPublished)){
	    publishTestValidation="test published already";
	}
	
	context.put("publishTestValidation", publishTestValidation)
	context.put("publishValidation", publishValidation);
    if(UtilValidate.isNotEmpty(endDate)) {
        if(((endDate.before(UtilDateTime.nowTimestamp())) ||  "MKTG_CAMP_PUBLISHED".equals(statusId)) && "BATCH".equals(classificationTypeId)){
            availableCampaign = false;
        }
	}
    pushType=null;
    if("BATCH".equals(classificationTypeId)){
        pushType="Batch";
    }else if("CONTINUOUS".equals(classificationTypeId)){
        pushType="Continuos";
    }else if("TRIGGER".equals(classificationTypeId)){
        pushType="Trigger";
    }
    context.put("pushType",pushType);
    campaignEmailDesc="";
    campaignEmailType=marketingCampaign.get("campaignEmailType");
    if(UtilValidate.isNotEmpty(campaignEmailType)) {
       if("SENSITIVE".equals(campaignEmailType)){
        campaignEmailDesc="Sensitive";
       }else if("NONSENSITIVE".equals(campaignEmailType)){
        campaignEmailDesc="Non Sensitive";
       }
	   context.put("campaignEmailType",campaignEmailType);
    }
   
    context.put("campaignEmailDesc",campaignEmailDesc);
    roleDesc="";
    roleTypeIdVal=marketingCampaign.get("roleTypeId");
    if(roleTypeIdVal=="LEAD"){
       roleDesc="Lead";
    }else if(roleTypeIdVal=="ACCOUNT"){
       roleDesc="Account";
    }else if(roleTypeIdVal=="CONTACT"){
       roleDesc="Contact";
    }
    context.put("roleDesc",roleDesc);	
    context.put("roleTypeIdVal",roleTypeIdVal);
    
    salesTriggerType=marketingCampaign.get("salesTriggerType");
    if(UtilValidate.isNotEmpty(salesTriggerType)) {
        salesTriggerTypeDescList = delegator.findOne("Enumeration", [enumId : salesTriggerType], false);
        salesTriggerTypeDesc=salesTriggerTypeDescList.getString("description");
        context.put("salesTriggerTypeDesc",salesTriggerTypeDesc);
    
    }else{
        salesTriggerType="N"
    }
     context.put("salesTriggerType",salesTriggerType);			
    
}
context.put("availableCampaign", availableCampaign);
context.put("marketingCampaign", marketingCampaign);
context.put("campaignId",marketingCampaignId);
marketingFinancial = delegator.findByAnd("MarketingCampaignFinancialMetrics",UtilMisc.toMap("marketingCampaignId", marketingCampaignId), null, false);
		if(marketingFinancial.size()>0){
		marketingFinancialValues=marketingFinancial.get(0);
		context.put("marketingFinancialValues",marketingFinancialValues);
		}

statusId=marketingCampaign.getString("statusId");
campaignTypeId=marketingCampaign.getString("campaignTypeId");
if(UtilValidate.isNotEmpty(statusId)){
    statusName=delegator.findOne("StatusItem", [statusId : statusId], false).getString("description");
    if(UtilValidate.isNotEmpty(statusName)){
        context.put("statusName",statusName);
    }
}

convertedCustomFieldId=campaignTypeId+"_"+marketingCampaignId+"_CONVERTED";
getConvertedCount= delegator.findCountByCondition("CustomFieldPartyClassification",
						EntityCondition.makeCondition("customFieldId", EntityOperator.EQUALS, convertedCustomFieldId), null,
						null);
context.put("convertedCount",getConvertedCount);
if(UtilValidate.isNotEmpty(campaignTypeId)){
    campaignTypeName=delegator.findOne("CampaignType", [campaignTypeId : campaignTypeId], false).getString("description");
    if(UtilValidate.isNotEmpty(campaignTypeName)){
        context.put("campaignTypeName",campaignTypeName);
    }
}
csrpartyId=parameters.get("csrpartyId");
roleType=parameters.get("roleType");
tabView=parameters.get("tabView");
campaignId=marketingCampaignId;
if(UtilValidate.isNotEmpty(campaignId)){
	conditions = FastList.newInstance();
	if(UtilValidate.isNotEmpty(csrpartyId)){
		conditions.add (new EntityExpr("partyId", EntityOperator.EQUALS,csrpartyId));
		context.put("csrpartyId",csrpartyId);
	}
	if(UtilValidate.isNotEmpty(roleType)){
		conditions.add (new EntityExpr("roleTypeId",EntityOperator.EQUALS,roleType));
	
	}else{
	conditions.add (new EntityExpr("roleTypeId", EntityOperator.IN ,UtilMisc.toList("ACCOUNT_MANAGER","CUST_SERVICE_REP")));
	
	}
	partyCond = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    csrList = delegator.findList("PartyRole", partyCond, null, null, null, false);
    csrSize=csrList.size();
    context.put("csrSize",csrSize);
    context.put("csrList",csrList);
    context.put("roleType",roleType);
    if(UtilValidate.isNotEmpty(tabView)){
        context.put("tabView",tabView);
    }

}
conditionsNameList = FastList.newInstance();
EntityFindOptions findOpts = new EntityFindOptions();
findOpts.setDistinct(true);
conditionsNameList.add (new EntityExpr("roleTypeId", EntityOperator.IN ,UtilMisc.toList("ACCOUNT_MANAGER","CUST_SERVICE_REP")));
partyCondName = EntityCondition.makeCondition(conditionsNameList, EntityOperator.AND);
csrNameList = delegator.findList("PartyRole", partyCondName, UtilMisc.toSet("partyId"),null, findOpts, false);
ArrayList fullNames=new ArrayList();
for(GenericValue csrName:csrNameList){
	HashMap hp=new HashMap();
	partyIds=csrName.getString("partyId");
	fnameLnameGv=delegator.findOne("Person", [partyId : partyIds], false);
	if(UtilValidate.isNotEmpty(fnameLnameGv)){
	fnameDrp=fnameLnameGv.getString("firstName");
	lnameDrp=fnameLnameGv.getString("lastName");
	
	fullName=fnameDrp+" "+lnameDrp;
	hp.put("partyIdd",partyIds);
	hp.put("fullName",fullName);
	fullNames.add(hp);
	}


}
context.put("fullNames",fullNames);

CampaignCsrAssoc = delegator.findByAnd("CampaignCsrAssoc",UtilMisc.toMap("campaignId",campaignId), null, false);
EntityFindOptions findOptsCsr = new EntityFindOptions();
findOptsCsr.setDistinct(true);
CampaignCsrAssoc = delegator.findList("CampaignCsrAssoc",EntityCondition.makeCondition("campaignId",EntityOperator.EQUALS,campaignId), UtilMisc.toSet("csrPartyId"),null, findOptsCsr, false);
ArrayList fullNamesCsr=new ArrayList();
if(UtilValidate.isNotEmpty(CampaignCsrAssoc)){	
	for(GenericValue csrName:CampaignCsrAssoc){
		HashMap hp=new HashMap();
		partyIds=csrName.getString("csrPartyId");
		fnameLnameGv=delegator.findOne("Person", [partyId : partyIds], false);
		if(UtilValidate.isNotEmpty(fnameLnameGv)){
		fnameDrp=fnameLnameGv.getString("firstName");
		lnameDrp=fnameLnameGv.getString("lastName");
		
		fullName=fnameDrp+" "+lnameDrp;
		hp.put("csrPartyId",partyIds);
		hp.put("fullName",fullName);
		fullNamesCsr.add(hp);
		}
      }

	}
context.put("CampaignCsrAssoc",fullNamesCsr);
//put to history
history = UtilCommon.makeHistoryEntry(marketingCampaign.get("campaignName"), "viewMarketingCampaign", UtilMisc.toList("marketingCampaignId"));
context.put("history", history);
security = request.getAttribute("security");
userLogin = request.getAttribute("userLogin");

if (security.hasEntityPermission("CRMSFA_CAMP", "_UPDATE", userLogin)) {
    context.put("hasUpdatePermission", true);
}

// get the associated contact lists
conditions = new EntityConditionList( UtilMisc.toList(
             new EntityExpr("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId),
        ), EntityOperator.AND);
        
 conditionsOR = new EntityConditionList( UtilMisc.toList(
                      new EntityExpr("isProcessed", EntityOperator.NOT_EQUAL, "C"),
                      new EntityExpr("isProcessed", EntityOperator.EQUALS, null)), EntityOperator.OR);
andExprs = [];
andExprs.add(conditions);
andExprs.add(conditionsOR);
conditionsFinal=new EntityConditionList(andExprs, EntityOperator.AND);
        
    marketingCampaignContactLists = delegator.findList("MarketingCampaignContactList",conditionsFinal, null,null, null, false);
	Iterator<String> mktCampaignContactLists = marketingCampaignContactLists.iterator();	
	Timestamp currentTimestamp = UtilDateTime.nowTimestamp();
	ArrayList mkContactLists=new ArrayList();
	if (marketingCampaignContactLists != null) {		
		try {
		       
                while (mktCampaignContactLists.hasNext()) {
                	Map paramValue = FastMap.newInstance();
                	GenericValue contactList = (GenericValue)mktCampaignContactLists.next();
		        	paramValue.putAll(contactList);
					String contactPurposeType = contactList.getString("contactPurposeType");
					if(UtilValidate.isNotEmpty(contactPurposeType)) {
						if("LIVE".equals(contactPurposeType)) {
							paramValue.put("contactPurposeType","Production");
						} else {
							paramValue.put("contactPurposeType","Test");
						}
					}
		        	paramValue.put("isExpired","N");
		        	String contactListId = contactList.getString("contactListId");
		        	Timestamp thruDate = contactList.getTimestamp("thruDate");
		        	if(UtilValidate.isNotEmpty(thruDate) && thruDate.before(currentTimestamp)){
		        		 paramValue.put("isExpired","Y"); 
		        	}
		        	GenericValue contactListInfo = delegator.findOne("ContactList", [contactListId : contactListId], false);
		        	if(contactListInfo !=null){
		        		String isSmartList = contactListInfo.getString("isSmartList");
		        		paramValue.put("isSmartList",isSmartList);
		        		paramValue.put("contactListName",contactListInfo.getString("contactListName"));
		        		paramValue.put("contactMechTypeId",contactListInfo.getString("contactMechTypeId"));
		        	}
		        	
					mkContactLists.add(paramValue);
				}				
		}catch (Exception e) {               
        } 	
	}
context.put("marketingCampaignContactLists", mkContactLists);



//BatchId 
getBatchId = EntityUtil.getFirst(delegator.findByAnd("TagsBatchHeader", UtilMisc.toMap("campaignId",marketingCampaignId),UtilMisc.toList("lastUpdatedStamp DESC"), false));
if(getBatchId!=null){
	context.put("campaignBatchId",getBatchId.getString("batchId"));
}else{
	context.put("campaignBatchId","");
}

context.put("tagStatus",getBatchId);

List dripCampaignList = delegator.findByAnd("DripCampaigns", UtilMisc.toMap("parentCampaignId",marketingCampaignId),UtilMisc.toList("lastUpdatedStamp DESC"), false);

List dripCampaignLists = new ArrayList();

for(GenericValue dripCampaign:dripCampaignList){
   
   Map map = new HashMap();
   map.put("campaignId",dripCampaign.getString("campaignId"));
   map.put("count",dripCampaign.getString("count"));
   
   
   GenericValue mktCampaign = delegator.findOne("MarketingCampaign", [marketingCampaignId : dripCampaign.getString("campaignId")], false);
   if(mktCampaign!=null)
    map.put("campaignName",mktCampaign.getString("campaignName"));
   
   dripCampaignLists.add(map);
}

if(dripCampaignLists!=null && dripCampaignLists.size()>0)
context.put("dripCampaignList",dripCampaignLists);


/*storeConfiguration = delegator.findByAnd("EreceiptStoreConfiguration",UtilMisc.toMap("campaignId",marketingCampaignId), null, false);

storeLists=new ArrayList(); 

 if(storeConfiguration!=null){
	  for(GenericValue store: storeConfiguration){
	  	Map storeValue = FastMap.newInstance();
	   	productStore =  delegator.findOne("ProductStore", [productStoreId : store.getString("storeId")], false);
	 	
		 if(UtilValidate.isNotEmpty(productStore)){
		 	storeValue.put("storeName" , productStore.getString("storeName"));
		  	storeLists.add(storeValue);
		  }
	  } 
  } 
  context.put("storeConfig",storeLists);*/
// Generate Call List
if(UtilValidate.isNotEmpty(marketingCampaign)){
    classificationType = marketingCampaign.getString("classificationType");
}

callRecordMaster = delegator.findByAnd("CallRecordMaster",UtilMisc.toMap("marketingCampaignId",marketingCampaignId), null, false);
if(UtilValidate.isNotEmpty(callRecordMaster)){
  if( !"COUPON_EXPIRY".equals(classificationType)){
      context.put("isGenerated","Y");
  }else{
      context.put("isGenerated","N");
  }
  
}else{
    context.put("isGenerated","N");
}
conditionsListDisp = new EntityConditionList( UtilMisc.toList(
            new EntityExpr("isSmartList", EntityOperator.EQUALS, null),
            new EntityExpr("contactListTypeId", EntityOperator.NOT_EQUAL, "CAMPAIGN_LIST"),
        ), EntityOperator.AND);
contactListLookUp=delegator.findList("ContactList",conditionsListDisp, null, null, null, false);
context.put("contactListLookUp",contactListLookUp);	

contactListSmartLookUp=delegator.findList("SmartlistTemplateBuilder",EntityCondition.makeCondition("type", EntityOperator.EQUALS, "TEMPLATE_TYPE"), null, null, null, false);
context.put("contactListSmartLookUp",contactListSmartLookUp);	

if(UtilValidate.isNotEmpty(marketingCampaignId)) {
marketingCampCL = delegator.findByAnd("MarketingCampaignContactList", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), null, false);
if(marketingCampCL != null && marketingCampCL.size() > 0) {
    List contactListPartyList = EntityQuery.use(delegator).from("ContactListParty")
    .where(EntityCondition.makeCondition("contactListId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(marketingCampCL, "contactListId", true)))
    .queryList();
    if(contactListPartyList != null && contactListPartyList.size() > 0) {
        List<String> partyIdList =  EntityUtil.getFieldListFromEntityList(contactListPartyList, "partyId", true);
        if(partyIdList != null && partyIdList.size() > 0) {
            context.put("partyIdList", partyIdList.size());
        }
    }
}
templateMaster = delegator.findOne("TemplateMaster", UtilMisc.toMap("templateId", marketingCampaign.getString("campaignTemplateId")), false);
if(templateMaster != null && templateMaster.size() > 0) {
    context.put("templateMaster", templateMaster);
}
}

//context.put("campCountMember", CampaignUtil.getCampaignMemberCount(delegator, marketingCampaignId));


//Enable approve list in view campaign screen.
context.put("enableCampaignApprove", "Y");

/*if(UtilValidate.isNotEmpty(marketingCampaignId)) {
GenericValue campCLSummary = EntityQuery.use(delegator).from("CampaignContactListSummary")
    .where("campaignId", marketingCampaignId, "attrName", "RM_APPROVED").queryFirst();
if(campCLSummary != null && campCLSummary.size() > 0) {
    context.put("rmApproved", campCLSummary.getString("attrValue"));
}
}*/

// smart list assoc
condition = UtilMisc.toMap("marketingCampaignId", marketingCampaignId); 
cond = EntityCondition.makeCondition("marketingCampaignId",EntityOperator.EQUALS,marketingCampaignId);
campaignSmListTemplate = delegator.findList("SmartlistTemplateBuilder", cond, null, UtilMisc.toList("smartListId DESC"), null, false);
smartTemplate = new LinkedHashMap();
if( campaignSmListTemplate != null && campaignSmListTemplate.size() > 0 ){
	GenericValue campaignTypeSmTemplate = campaignSmListTemplate.get(0);
	if(campaignTypeSmTemplate != null && campaignTypeSmTemplate.size() >0) {
		context.put("campaignCriteria",campaignTypeSmTemplate.getString("criteria"));
		context.put("campaignTypeSmTemplate",campaignTypeSmTemplate);
	}
	/*for(GenericValue campaignTemplate : campaignTemplateList){	
					name = campaignTemplate.getString("name");
					smartListIds = campaignTemplate.getString("smartListId");
					smartTemplate.put(smartListIds, name);				
		}*/
}
//context.put("smartTemplate", smartTemplate);

//Blue Bar Count
/*responseCountList = delegator.findOne("MarketingCampaignAnalysisSummary", UtilMisc.toMap("campaignId", marketingCampaignId,"domainName",'DBS'), false);
Long notOpenCount = 0;
Long openLong = 0;
Long sentLong = 0;
Long unSubscribed = 0;
Long subscribed = 0;
String contListId = marketingCampaignId+"_PROD_LIST";
if( responseCountList != null && responseCountList.size() > 0 ){
	openedMailCount = responseCountList.getString("openedMailCount");
	if( openedMailCount != null ){
	openLong = Long.valueOf(openedMailCount);
	}
	sentMailCount = responseCountList.getString("sentMailCount");
	if( sentMailCount != null ){
	sentLong = Long.valueOf(sentMailCount);	
	notOpenCount = sentLong - openLong;
	}
}
context.put("responseCount",responseCountList);
Debug.logInfo("==================== Blue Bar result2 : " + responseCountList, "");

mktCampContactListNotOpen = delegator.findByAnd("CampaignContactListParty",UtilMisc.toMap("contactListId", contListId,"contactPurposeTypeId",'LIVE',"notOpen",'Y'), null, false);
if( mktCampContactListNotOpen != null && mktCampContactListNotOpen.size() > 0 ){
	notOpenCount = mktCampContactListNotOpen.size();
}
context.put("notOpen",notOpenCount);

mktCampContactListUnSubscribed = delegator.findByAnd("CampaignContactListParty",UtilMisc.toMap("contactListId", contListId,"contactPurposeTypeId",'LIVE',"unsubscribed",'Y'), null, false);
if( mktCampContactListUnSubscribed != null && mktCampContactListUnSubscribed.size() > 0 ){
	unSubscribed = mktCampContactListUnSubscribed.size();
}
context.put("unSubscribed",unSubscribed);

mktCampContactListSubscribed = delegator.findByAnd("CampaignContactListParty",UtilMisc.toMap("contactListId", contListId,"contactPurposeTypeId",'LIVE',"subscribed",'Y'), null, false);
if( mktCampContactListSubscribed != null && mktCampContactListSubscribed.size() > 0 ){
	subscribed = mktCampContactListSubscribed.size();
}
context.put("subscribed",subscribed);
*/

mktCampContactListDownload = delegator.findByAnd("MarketingCampaignContactList", [marketingCampaignId : marketingCampaignId], null, false);
downloadList = EntityQuery.use(delegator).from("ContactList")
    .where(EntityCondition.makeCondition([EntityCondition.makeCondition("contactListId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(mktCampContactListDownload, "contactListId", true)),
         EntityCondition.makeCondition("contactListTypeId", EntityOperator.EQUALS, "GENERALLIST")
     ])).queryFirst();
if(downloadList != null && downloadList.size() > 0) {
    download = downloadList.getString("download");
    if(UtilValidate.isNotEmpty(download)) {
        context.put("smartListDownload", download);
    }
}

campMultiTempList = delegator.findByAnd("CampaignMultiTemplate", [marketingCampaignId : marketingCampaignId], null, false);

if(campMultiTempList != null && campMultiTempList.size() > 0) {
	for(GenericValue campMultiTemp:campMultiTempList){
	multiTemplateId = campMultiTemp.getString("templateId");
	multigroupId = campMultiTemp.getString("groupId");
	multiCustomFieldId = campMultiTemp.getString("customFieldId");
		if(UtilValidate.isNotEmpty(multiTemplateId)) {
			context.put("multiTemplateId", multiTemplateId);
		}
		if(UtilValidate.isNotEmpty(multigroupId)) {
			context.put("multigroupId", multigroupId);
		}
		if(UtilValidate.isNotEmpty(multiCustomFieldId)) {
			context.put("multiCustomFieldId", multiCustomFieldId);
		}
	}
	context.put("campMultiTempList", campMultiTempList);
	
}


flagMap = new LinkedHashMap();
flagMap.put("Y", "Yes");
flagMap.put("N", "No");
context.put("flagMap", flagMap);

executionTypes = new LinkedHashMap();
executionTypes.put("SINGLE","Single");
executionTypes.put("MULTI","Multiple");

context.put("executionTypes", executionTypes);

smartListJobSchedulerJob = EntityUtil.getFirst(delegator.findByAnd("SmartlistJobScheduler", UtilMisc.toMap("campaignId",marketingCampaignId) , UtilMisc.toList("lastUpdatedTxStamp DESC"), false));
String fromDate = "";
String thruDate = "";

if(smartListJobSchedulerJob != null && smartListJobSchedulerJob.size() > 0) {
	SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
	fromDate = smartListJobSchedulerJob.getString("startDate");
	thruDate = smartListJobSchedulerJob.getString("endDate");
	specificDate = smartListJobSchedulerJob.getString("specificDate");
	if(UtilValidate.isNotEmpty(fromDate)) {
		fromDate = df.format(df1.parse(fromDate));
	}
	if(UtilValidate.isNotEmpty(thruDate)) {
		thruDate = df.format(df1.parse(thruDate));
	}
	if(UtilValidate.isNotEmpty(specificDate)) {
		specificDate = df.format(df1.parse(specificDate));
	}

	context.put("jobStartDate",fromDate);
	context.put("jobEndDate",thruDate);
	context.put("jobSpecificDate",specificDate);
}
context.put("jobMinDate",startDate);
context.put("jobMaxDate",endDate);
context.put("smartListJobSchedulerJob",smartListJobSchedulerJob);



