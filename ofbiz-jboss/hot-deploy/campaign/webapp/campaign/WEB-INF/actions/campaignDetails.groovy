import org.ofbiz.util.*;

import javolution.util.FastList
import javolution.util.FastMap
import org.ofbiz.party.party.PartyHelper;
import org.ofbiz.party.contact.ContactHelper;
import org.ofbiz.entity.*;
import org.ofbiz.entity.condition.*;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.ModelEntity;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate
import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.jdbc.SQLProcessor;
import java.util.ArrayList;
import org.ofbiz.base.util.UtilDateTime;

import java.sql.ResultSet
import java.sql.Timestamp;
import org.ofbiz.entity.util.EntityUtilProperties;

userLogin = session.getAttribute("userLogin");
userLoginId = null;
if(UtilValidate.isNotEmpty(userLogin))
{
	userLoginId = userLogin.userLoginId;
}

List campaignDetails = FastList.newInstance();
marketingCampaignId = request.getParameter("campaignId");
if(marketingCampaignId)
{
	context.marketingCampaignId = marketingCampaignId;
}
Debug.logInfo("#1--CampaignId "+marketingCampaignId,"");

csrPartyId = request.getParameter("csrPartyId");
if(csrPartyId)
{
	context.csrPartyId = csrPartyId;
}
isExpandAll = request.getParameter("expand");
if(isExpandAll)
{
	context.isExpandAll = isExpandAll;
}

Debug.logInfo("#1--csrPartyId "+csrPartyId,"");


ResultSet rs = null;

groupBy = " GROUP BY cclp.csr_party_id,cclp.acct_party_id ";
if("Y".equals(isExpandAll))
{
	groupBy = " GROUP BY cclp.csr_party_id,cclp.acct_party_id,cclp.party_id ";
}
//FIXME it will display all the expand all section......
SQLProcessor sqlProcessor = new SQLProcessor(delegator, delegator.getGroupHelperInfo("org.ofbiz"));



	//sqlQuery = "SELECT SUM(IF(cclp.open='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open', SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted',cclp.csr_party_id as 'csr_party_id',cclp.acct_party_id as 'acct_party_id',cclp.party_id as 'party_id' FROM marketing_campaign AS mc INNER JOIN campaign_contact_list_summary AS ccls ON mc.marketing_campaign_id = ccls.campaign_id INNER JOIN campaign_contact_list_party AS cclp ON ccls.contact_list_id = ccls.contact_list_id WHERE csr_party_id='"+csrPartyId+"' and mc.marketing_campaign_id='"+marketingCampaignId+"' "+groupBy+" ";
	sqlQuery = " SELECT SUM(IF(cclp.opened='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open',SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted', cclp.csr_party_id AS 'csr_party_id',cclp.acct_party_id AS 'acct_party_id',cclp.party_id AS 'party_id', SUM(IF(cclp.unsubscribed='Y',1,0)) AS 'unsubscribed',SUM(IF(cclp.hard_bounce='Y',1,0)) AS 'boun',SUM(IF(cclp.blacked='Y',1,0))  AS 'black'   FROM campaign_contact_list_party AS cclp INNER JOIN campaign_contact_list_summary ccls ON ccls.contact_list_id = cclp.contact_list_id WHERE ccls.campaign_id='"+marketingCampaignId+"' AND cclp.csr_party_id='"+csrPartyId+"'  "+groupBy;
	Debug.logInfo("=======SQL QUERY==="+sqlQuery, "");
	if(UtilValidate.isNotEmpty(marketingCampaignId))
	{

		marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
		if(UtilValidate.isNotEmpty(marketingCampaign))
		{
			context.marketingCampaign = marketingCampaign;



			//getting for contact information
			try {

				rs = sqlProcessor.executeQuery(sqlQuery);

				if (rs != null) {
					while (rs.next()) {
						temp = [:];
						csrPartyId = rs.getString("csr_party_id");
						acctPartyId = rs.getString("acct_party_id");
						partyId = rs.getString("party_id");

						if(UtilValidate.isNotEmpty(csrPartyId))
						{
							temp.rmPartyName = PartyHelper.getPartyName(delegator, csrPartyId, false);
						}

						if(UtilValidate.isNotEmpty(acctPartyId))
						{
							temp.accntPartyName = PartyHelper.getPartyName(delegator, acctPartyId, false);
						}

						if(UtilValidate.isNotEmpty(partyId))
						{
							temp.contactPartyName = PartyHelper.getPartyName(delegator, partyId, false);


							party = delegator.findOne("Party", UtilMisc.toMap("partyId",partyId), false)

							if(UtilValidate.isNotEmpty(party))
							{
								contactMech =  ContactHelper.getContactMechByPurpose(party, "PRIMARY_EMAIL", false)
								if(contactMech)
								{
									contactMechFirst = EntityUtil.getFirst(contactMech);
									if(contactMechFirst)
									{
										temp.contactPartyEmail = contactMechFirst.infoString;
									}


								}

								contactMechFrPhone =  ContactHelper.getContactMechByPurpose(party, "PRIMARY_PHONE", false)
								if(contactMechFrPhone)
								{
									contactMechFirst = EntityUtil.getFirst(contactMechFrPhone);
									if(contactMechFirst)
									{
										GenericValue telecomNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId",contactMechFirst.contactMechId), false);
										if(telecomNumber)
										{
											temp.phoneNumber = (telecomNumber.countryCode?telecomNumber.countryCode:"")+""+(telecomNumber.areaCode?telecomNumber.areaCode:"")+""+(telecomNumber.contactNumber?telecomNumber.contactNumber:"");
										}
									}


								}


							}

							temp.open = rs.getString("open");
							temp.not_open = rs.getString("not_open");
							temp.clicked = rs.getString("clicked");
							temp.converted = rs.getString("converted");
							temp.unsubscribed = rs.getString("unsubscribed");
							temp.boun = rs.getString("boun");
							temp.black = rs.getString("black");
						}


						campaignDetails.add(temp);
					}
				}
				context.campaignDetails = campaignDetails;
			}catch(Exception e)
			{
				e.printStackTrace();
			}finally {
				if(rs!=null)
				{
					rs.close();
				}
			}

		}

	}
