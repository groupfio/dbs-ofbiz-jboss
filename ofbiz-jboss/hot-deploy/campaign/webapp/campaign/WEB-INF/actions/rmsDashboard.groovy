import org.ofbiz.util.*;

import javolution.util.FastList

import org.ofbiz.entity.*;
import org.ofbiz.entity.condition.*;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.ModelEntity;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate
import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.jdbc.SQLProcessor;
import java.util.ArrayList;
import java.util.LinkedList;
import org.ofbiz.base.util.UtilDateTime;

import org.ofbiz.party.party.PartyHelper;
import org.ofbiz.party.contact.ContactHelper;
import java.sql.ResultSet
import java.sql.Timestamp;
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.ofbiz.entity.util.EntityUtilProperties;

int MAX_COUNT = 10;
userLogin = session.getAttribute("userLogin");
userLoginId = null;
if(UtilValidate.isNotEmpty(userLogin))
{
	userLoginId = userLogin.userLoginId;
}
triggerType = "SALES_TRIGGER";
salesTrigger = new LinkedList();
nonSalesTrigger = new LinkedHashSet();
marketingCampaign = delegator.findAll("MarketingCampaign", false);
ResultSet rs = null;
groupBy = " GROUP BY mc.marketing_campaign_id,cp.csr_party_id ";
try
{
	LinkedList campaignDetails = new LinkedList();
	partyRole = delegator.findOne("PartyRole", UtilMisc.toMap("partyId",userLogin.partyId,"roleTypeId","CAMPAIGN_MANAGER"), false);
	if(partyRole)
	{

		groupBy = " GROUP BY mc.marketing_campaign_id ";
	}
	//sales trigger query
	//String sqlQurey = "SELECT cp.csr_party_id as 'csr_party_id',mc.marketing_campaign_id as 'marketing_campaign_id',mc.campaign_name AS 'campaign_name',SUM(IF(cp.sent='Y',1,0)) as 'email',COUNT(acct_party_id) AS customers,SUM(IF(cp.open='Y',1,0)) AS 'open',SUM(IF(cp.not_open='Y',1,0)) AS 'not_open', SUM(IF(cp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cp.converted='Y',1,0)) AS 'converted', SUM(IF(cp.unsubscribed='Y',1,0)) AS 'unsubscribed',SUM(IF(cp.hard_bounce='Y',1,0)) AS 'boun',SUM(IF(cp.blacked='Y',1,0))  AS 'black' FROM Marketing_campaign AS mc INNER JOIN campaign_contact_list_summary AS ccls ON mc.marketing_campaign_id = ccls.campaign_id  INNER JOIN campaign_contact_list_party AS cp ON cp.contact_list_id = ccls.contact_list_id  WHERE mc.sales_trigger_type='SALES_TRIGGER' AND cp.csr_party_id='"+userLoginId+"'  "+groupBy+" ORDER BY mc.start_date DESC LIMIT 10";
	String sqlQurey = " SELECT  ccls.campaign_id AS 'campaign_id',SUM(IF(cclp.opened='Y',1,0)) AS 'open',SUM(IF(cclp.not_open='Y',1,0)) AS 'not_open',SUM(IF(cclp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cclp.converted='Y',1,0))  AS 'converted', cclp.csr_party_id AS 'csr_party_id',cclp.acct_party_id AS 'acct_party_id',cclp.party_id AS 'party_id', SUM(IF(cclp.unsubscribed='Y',1,0)) AS 'unsubscribed',SUM(IF(cclp.hard_bounce='Y',1,0)) AS 'boun',SUM(IF(cclp.blacked='Y',1,0))  AS 'black'   FROM campaign_contact_list_party AS cclp INNER JOIN campaign_contact_list_summary ccls ON ccls.contact_list_id = cclp.contact_list_id INNER JOIN marketing_campaign AS mc ON mc.marketing_campaign_id = ccls.campaign_id  WHERE cclp.csr_party_id='"+userLogin.partyId+"'  GROUP BY cclp.csr_party_id,cclp.acct_party_id,cclp.party_id  ORDER BY  mc.start_date DESC";
	String nonSalesTriggerSqlQurey = "SELECT cp.csr_party_id as 'csr_party_id',mc.marketing_campaign_id as 'marketing_campaign_id',mc.campaign_name AS 'campaign_name',SUM(IF(cp.sent='Y',1,0)) as 'email',COUNT(acct_party_id) AS customers,SUM(IF(cp.opened='Y',1,0)) AS 'open',SUM(IF(cp.subscribed='Y',1,0)) AS 'subscribe',SUM(IF(cp.not_open='Y',1,0)) AS 'not_open', SUM(IF(cp.clicked='Y',1,0)) AS 'clicked',SUM(IF(cp.converted='Y',1,0)) AS 'converted', SUM(IF(cp.unsubscribed='Y',1,0)) AS 'unsubscribed',SUM(IF(cp.hard_bounce='Y',1,0)) AS 'boun',SUM(IF(cp.blacked='Y',1,0))  AS 'black' FROM Marketing_campaign AS mc INNER JOIN campaign_contact_list_summary AS ccls ON mc.marketing_campaign_id = ccls.campaign_id  INNER JOIN campaign_contact_list_party AS cp ON cp.contact_list_id = ccls.contact_list_id  WHERE cp.csr_party_id='"+userLogin.partyId+"'  "+groupBy+" ORDER BY mc.start_date DESC LIMIT 10";

	SQLProcessor sqlProcessor = new SQLProcessor(delegator, delegator.getGroupHelperInfo("org.ofbiz"));
	rs = sqlProcessor.executeQuery(sqlQurey);
	Debug.logInfo("==================== Generated information "+sqlQurey,"");
	int count = 0;
	
	if (rs != null) {
		while (rs.next()) {
			
			Debug.logError("=====countingstarts  here itself ===="+count, "");
			/*itemmap = [:];
			itemmap.csr_party_id = rs.getString("csr_party_id");
			itemmap.marketingCampaignId = rs.getString("marketing_campaign_id");
			itemmap.campaign_name = rs.getString("campaign_name");
			itemmap.email = rs.getString("email");
			itemmap.customers = rs.getString("customers");
			itemmap.open = rs.getString("open");
			itemmap.not_open = rs.getString("not_open");
			itemmap.clicked = rs.getString("clicked");
			itemmap.converted = rs.getString("converted");
			itemmap.unsubscribed = rs.getString("unsubscribed");
			itemmap.boun = rs.getString("boun");
			itemmap.black = rs.getString("black");
			println("Data resutl is "+itemmap);
			salesTrigger.add(itemmap);*/
			temp = [:];
			csrPartyId = rs.getString("csr_party_id");
			acctPartyId = rs.getString("acct_party_id");
			partyId = rs.getString("party_id");
			marketingCampaignId = rs.getString("campaign_id");
			
			if(marketingCampaignId)
			{
				temp.marketingCampaignId = marketingCampaignId;
			}
			if(UtilValidate.isNotEmpty(csrPartyId))
			{
				temp.rmPartyName = PartyHelper.getPartyName(delegator, csrPartyId, false);
			}

			if(UtilValidate.isNotEmpty(acctPartyId))
			{
				temp.accntPartyName = PartyHelper.getPartyName(delegator, acctPartyId, false);
			}

			if(UtilValidate.isNotEmpty(partyId))
			{
				temp.cin = partyId;
				temp.contactPartyName = PartyHelper.getPartyName(delegator, partyId, false);


				party = delegator.findOne("Party", UtilMisc.toMap("partyId",partyId), false)

				if(UtilValidate.isNotEmpty(party))
				{
					contactMech =  ContactHelper.getContactMechByPurpose(party, "PRIMARY_EMAIL", false)
					if(contactMech)
					{
						contactMechFirst = EntityUtil.getFirst(contactMech);
						if(contactMechFirst)
						{
							temp.contactPartyEmail = contactMechFirst.infoString;
						}


					}

					contactMechFrPhone =  ContactHelper.getContactMechByPurpose(party, "PRIMARY_PHONE", false)
					if(contactMechFrPhone)
					{
						contactMechFirst = EntityUtil.getFirst(contactMechFrPhone);
						if(contactMechFirst)
						{
							GenericValue telecomNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId",contactMechFirst.contactMechId), false);
							if(telecomNumber)
							{
								temp.phoneNumber = (telecomNumber.countryCode?telecomNumber.countryCode:"")+""+(telecomNumber.areaCode?telecomNumber.areaCode:"")+""+(telecomNumber.contactNumber?telecomNumber.contactNumber:"");
							}
						}


					}


				}

				temp.open = rs.getString("open");
				temp.not_open = rs.getString("not_open");
				temp.clicked = rs.getString("clicked");
				
				
				if(marketingCampaignId)
				{
					marketCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
					if(marketCampaign)
					{
						if(!triggerType.equalsIgnoreCase(marketCampaign.salesTriggerType))
						{
							continue;
						}
						temp.product = marketCampaign.product;
						temp.reportDate =marketCampaign.startDate;
						temp.campaignName = marketCampaign.campaignName;
						//Debug.logError("=====marketing Campaign id is ==="+marketCampaign.fromDate, "");
					}
					
				}
			}
			//osBalance
			
			partyMetricIndicator = delegator.findOne("PartyMetricIndicator", UtilMisc.toMap("groupId","LOAN_ACCT_CODE","customFieldId","LOAN_ACCT_TOTAL_OUTSTD_AMT","partyId",partyId), false);
			if(partyMetricIndicator)
			{
				temp.osBalance = marketCampaign.propertyValue;
			}
			
			
			
			converte = Integer.parseInt(rs.getString("converted"));
			Debug.logError("=====marketing Campaign id is ===----"+rs.getString("converted"), "");
			if(converte!=0)
			{
				continue;
			}
			
			/*count = count+1;
			if(count==10)
			{
				Debug.logError("=====Breaking here itself ===="+count, "");
				break;
			}*/
			
			campaignDetails.add(temp);
			
			
			
		}

	}

	//context.salesTrigger = salesTrigger;
	context.campaignDetails = campaignDetails;

	rs = sqlProcessor.executeQuery(nonSalesTriggerSqlQurey);
	Debug.logInfo("==================== Generated information #1 "+nonSalesTriggerSqlQurey,"");
	if (rs != null) {
		while (rs.next()) {
			/*marketingCampaignId = rs1.getString("marketing_campaign_id");
			 if(UtilValidate.isNotEmpty(marketingCampaignId))
			 {
			 GenericValue marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
			 if(marketingCampaign)
			 {
			 salesTriggerType = marketingCampaign.salesTriggerType;
			 if("SALES_TRIGGER".equals(salesTriggerType))
			 {
			 continue;
			 }
			 itemmap1 = [:];
			 itemmap1.campaign_name = marketingCampaign.getString("campaignName")+"("+marketingCampaignId+")";
			 GenericValue campaignTypeIds = delegator.findOne("CampaignType", UtilMisc.toMap("campaignTypeId",marketingCampaign.getString("campaignTypeId")), false);
			 itemmap1.campaignTypeId = campaignTypeIds?campaignTypeIds.getString("description"):"";
			 GenericValue statusIds = delegator.findOne("StatusItem", UtilMisc.toMap("statusId",marketingCampaign.getString("statusId")), false);
			 itemmap1.statusId = statusIds?statusIds.getString("description"):"";
			 itemmap1.startDate = marketingCampaign.getTimestamp("startDate");
			 itemmap1.endDate = marketingCampaign.getTimestamp("endDate");
			 println("Data resutl is "+itemmap1);
			 nonSalesTrigger.add(itemmap1);
			 }
			 }*/
			itemmap = [:];
			itemmap.csr_party_id = rs.getString("csr_party_id");
			itemmap.marketingCampaignId = rs.getString("marketing_campaign_id");
			
			//for getting the product information
			
			if(rs.getString("marketing_campaign_id"))
			{
					marketCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",rs.getString("marketing_campaign_id")), false);
					if(marketCampaign)
					{
						pro = marketCampaign.product;
						if(pro)
						{
							enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId",pro), false);
							if(enumeration)
							{
									itemmap.product = enumeration.description;
							}
							
						}
						
					
						SimpleDateFormat formater = new SimpleDateFormat("MMMM YYYY");
						String STdatas = marketCampaign.startDate;
						String ENDdatas = marketCampaign.endDate;
						if(STdatas)
						{
							STdatas = formater.format(Timestamp.valueOf(STdatas));
						}
						if(ENDdatas)
						{
							
							ENDdatas = formater.format(Timestamp.valueOf(ENDdatas));
						}
						
						itemmap.startDate = STdatas+" - "+ENDdatas;
					}
			}
			itemmap.campaign_name = rs.getString("campaign_name");
			itemmap.email = rs.getString("email");
			itemmap.customers = rs.getString("customers");
			itemmap.open = rs.getString("open");
			itemmap.not_open = rs.getString("not_open");
			itemmap.clicked = rs.getString("clicked");
			itemmap.converted = rs.getString("converted");
			itemmap.unsubscribed = rs.getString("unsubscribed");
			itemmap.subscribe = rs.getString("subscribe");
			itemmap.boun = rs.getString("boun");
			itemmap.black = rs.getString("black");
			println("Data resutl is "+itemmap);
			nonSalesTrigger.add(itemmap);

		}

	}

	Debug.logInfo("====================="+nonSalesTrigger,"");
	context.nonSalesTriggerSqlQurey = nonSalesTrigger;



	
	noteContainer = [];
	
	mkCampaign = delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("createdByUserLogin",userLoginId), null, false);
	/*for(GenericValue mk:mkCampaign)
	{
		Debug.logInfo("======= bsr ======"+mk.getString("marketingCampaignId"),"");
		
		partyNoteViewList = delegator.findByAnd("PartyNote", UtilMisc.toMap("campaignId",mk.getString("marketingCampaignId")), null, false);
		if(partyNoteViewList) {
			Debug.logInfo("======= note view list ======"+partyNoteViewList,"");
			for(GenericValue g:partyNoteViewList)
			{
				Debug.logInfo("======= b ======"+g,"");
				noteData = delegator.findOne("NoteData", UtilMisc.toMap("noteId",g.getString("noteId")), false);
				if(noteData) {
					Debug.logInfo("=====Note data====="+noteData,"");
					tmp = [:];
					ntType = noteData.noteType;
					if(ntType) {
						enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId",ntType), false);
						if(enumeration)
						{
							tmp.noteId = enumeration.description;
						}
						
					}
					
					tmp.noteName = noteData.noteName;
					tmp.noteInfo = noteData.noteInfo;
					tmp.campaignName = mk.campaignName;
					Debug.logInfo("======= b party id ======"+g.getString("partyId"),"");
					
					tmp.callBackDate = noteData.callBackDate;
					
					
					noteContainer.add(tmp);
				}
			}
			
			
		}
	}*/
	noteDatas = delegator.findByAnd("NoteData", UtilMisc.toMap("noteParty",userLogin.partyId), null, false);
	if(noteDatas) {
		
		//noteDatas = EntityUtil.orderBy(noteDatas, UtilMisc.toList("callBackDate ASC"));
		for(GenericValue noteData:noteDatas)
		{
			if(!noteData.callBackDate)
			{
				continue;
			}
			Debug.logInfo("=====Note data====="+noteData,"");
			tmp = [:];
			ntType = noteData.noteType;
			
			if(ntType) {
				enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId",ntType), false);
				if(enumeration)
				{
					tmp.noteId = enumeration.description;
				}
				
			}
			
			subNtType = noteData.subProduct;
			
			if(subNtType) {
				enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId",subNtType), false);
				if(enumeration)
				{
					tmp.subNtType = enumeration.description;
				}
				
			}
			
			tmp.noteName = noteData.noteName;
			tmp.noteInfo = noteData.noteInfo;
			partyNotes = delegator.findByAnd("PartyNote", UtilMisc.toMap("noteId",noteData.noteId), null, false);
			if(partyNotes)
			{
				partyNote = EntityUtil.getFirst(partyNotes);
				if(partyNote)
				{
					tmp.campaignName = PartyHelper.getPartyName(delegator, partyNote.partyId, false);
				}
			}
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
			String date = noteData.callBackDate;
			date=date+" 00:00:00";
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-d");
			
			//convert String to LocalDate
			LocalDate localDate = LocalDate.parse(formater.format(Timestamp.valueOf(date)), formatter);
			
			Debug.logInfo("===Local date of the callback====="+localDate,"");
			
			LocalDate today = LocalDate.now();
			LocalDate monday = today;
			while (monday.getDayOfWeek() != DayOfWeek.MONDAY)
			{
				monday = monday.minusDays(1);
			}
	
			// Go forward to get Sunday
			LocalDate sunday = today;
			while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY)
			{
				sunday = sunday.plusDays(1);
			}
			
			if(localDate.isAfter(monday) && localDate.isBefore(sunday) )
			{
						Debug.logInfo("OK","");
						
						tmp.callBackDate = noteData.callBackDate;
						noteContainer.add(tmp);
			}
				
				
		}
		
		
	}
	//context.partyNoteViewList = partyNoteViewList;
	context.noteContainer = noteContainer;
}catch(Exception e)
{
	e.printStackTrace();
}finally {
	if(rs!=null)
	{
		rs.close();
	}
}