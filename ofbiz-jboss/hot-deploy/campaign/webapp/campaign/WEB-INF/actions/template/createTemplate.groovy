import java.sql.Timestamp
import java.util.HashSet
import java.util.List
import java.util.Set

import org.fio.util.DataHelper
import org.ofbiz.base.util.UtilDateTime
import org.ofbiz.base.util.UtilMisc
import org.ofbiz.base.util.UtilValidate
import org.ofbiz.entity.GenericValue
import org.ofbiz.entity.condition.EntityCondition
import org.ofbiz.entity.condition.EntityOperator
import org.ofbiz.entity.util.EntityUtil

import javolution.util.FastList


charSetMap = new LinkedHashMap();
charSetMap.put("UTF-8", "Unicode (UTF-8)");
charSetMap.put("ISO 8859-15", "ISO 8859-15");
charSetMap.put("ISO 8859-1","ISO 8859-1");
charSetMap.put("GB2312","Chinese simplified (GB2312)");
context.put("charSetList", charSetMap);

mailFormat = new LinkedHashMap();
mailFormat.put("0","Only Text");
mailFormat.put("1","Text and HTML");
mailFormat.put("2","Text, HTML and Offline-HTML");
context.put("mailFormat", mailFormat);

emailLineFeedMap = new LinkedHashMap();
emailLineFeedMap.put("0", "No line feed");
emailLineFeedMap.put("60", "60 Characters");
emailLineFeedMap.put("61", "61 Characters");
emailLineFeedMap.put("62", "62 Characters");
emailLineFeedMap.put("63", "63 Characters");
emailLineFeedMap.put("64", "64 Characters");
emailLineFeedMap.put("65", "65 Characters");
emailLineFeedMap.put("66", "66 Characters");
emailLineFeedMap.put("67", "67 Characters");
emailLineFeedMap.put("68", "68 Characters");
emailLineFeedMap.put("69", "69 Characters");
emailLineFeedMap.put("70", "70 Characters");
emailLineFeedMap.put("71", "71 Characters");
emailLineFeedMap.put("72", "72 Characters");
emailLineFeedMap.put("73", "73 Characters");
emailLineFeedMap.put("74", "74 Characters");
emailLineFeedMap.put("75", "75 Characters");
emailLineFeedMap.put("76", "76 Characters");
emailLineFeedMap.put("77", "77 Characters");
emailLineFeedMap.put("78", "78 Characters");
emailLineFeedMap.put("79", "79 Characters");
emailLineFeedMap.put("80", "80 Characters");

context.put("emailLineFeedList", emailLineFeedMap);

measureOpenRateMap = new LinkedHashMap();
measureOpenRateMap.put("top", "At Top of email");
measureOpenRateMap.put("bottom", "At Bottom of email");
measureOpenRateMap.put("none", "No");
context.put("measureOpenRateList", measureOpenRateMap);

templateTagTypeList = delegator.findAll("TemplateTagType", false)
context.put("templateTagTypeList", DataHelper.getDropDownOptions(templateTagTypeList, "templateTagTypeId", "description"))

//Pre Templates
preTemplatesList = delegator.findList("TemplateMaster", EntityCondition.makeCondition("templateId",EntityOperator.LIKE, "PRE_%"), null, null, null, false);
context.put("preTemplatesList", DataHelper.getDropDownOptions(preTemplatesList, "templateId", "templateName"));
 
templateCategoryList = delegator.findByAnd("TemplateCategory", null, UtilMisc.toList("templateCategoryName"), false);
context.put("templateCategoryList",DataHelper.getDropDownOptions(templateCategoryList, "templateCategoryId", "templateCategoryName"));
 

templateTagTypes = delegator.findAll("TemplateTagType", false)
context.put("templateTagTypes", templateTagTypes)


// template id
templateId = request.getParameter("templateId");
if(UtilValidate.isNotEmpty(templateId)) {
	List<EntityCondition> conditionList = FastList.newInstance();
	Set<String> fieldToSelect = new HashSet<String>();
	fieldToSelect.add("tagId");
	fieldToSelect.add("tagName");
	fieldToSelect.add("templateTagTypeId");
	Timestamp nowTimestamp = UtilDateTime.nowTimestamp();
	conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isEnabled",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("isEnabled",EntityOperator.EQUALS,"Y")));
	conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("fromDate",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("fromDate",EntityOperator.LESS_THAN_EQUAL_TO,nowTimestamp)));
	conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("thruDate",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("thruDate",EntityOperator.GREATER_THAN_EQUAL_TO,nowTimestamp)));


	// Get the configured tag list
	List<String> configuredTagIds = null;
	if(UtilValidate.isNotEmpty(templateId)) {
		List<GenericValue> templateTag = delegator.findByAnd("TemplateTag", UtilMisc.toMap("templateId",templateId), null, false);
		if(templateTag !=null && templateTag.size() > 0) {
			configuredTagIds = EntityUtil.getFieldListFromEntityList(templateTag, "tagId", true);
		}
	}
	List customTagList = FastList.newInstance();
	List standardTagList = FastList.newInstance();
	if(configuredTagIds !=null && configuredTagIds.size()>0) {
		conditionList.add(EntityCondition.makeCondition("tagId",EntityOperator.IN,configuredTagIds));
		List<EntityCondition> condition1 = FastList.newInstance();
		condition1.addAll(conditionList);
		condition1.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,"CUSTOM_FIELD"))
		List<GenericValue> dataTagConfigurations = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(condition1,EntityOperator.AND), fieldToSelect, null, null, false);
		if(dataTagConfigurations != null && dataTagConfigurations.size() >0) {
			context.put("attributeTagList", dataTagConfigurations);
		}

		// standard tags
		List<EntityCondition> condition2 = FastList.newInstance();
		condition2.addAll(conditionList);
		condition2.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,"STANDARD"))
		List<GenericValue> dataTagConfigurations1 = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(condition2,EntityOperator.AND), fieldToSelect, null, null, false);
		if(dataTagConfigurations1 != null && dataTagConfigurations1.size() >0) {
			context.put("standardTagList", dataTagConfigurations1);
		}
		
		List<EntityCondition> condition3 = FastList.newInstance();
		condition3.addAll(conditionList);
		condition3.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,"ECONOMIC_METRIC"))
		List<GenericValue> dataTagConfigurations2 = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(condition3,EntityOperator.AND), fieldToSelect, null, null, false);
		if(dataTagConfigurations2 != null && dataTagConfigurations2.size() >0) {
			context.put("economicTagList", dataTagConfigurations2);
		}
		
		List<EntityCondition> condition4 = FastList.newInstance();
		condition4.addAll(conditionList);
		condition4.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,"SEGMENTATION"))
		List<GenericValue> dataTagConfigurations3 = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(condition4,EntityOperator.AND), fieldToSelect, null, null, false);
		if(dataTagConfigurations3 != null && dataTagConfigurations3.size() >0) {
			context.put("segmentationTagList", dataTagConfigurations3);
		}
	}
}
 
 