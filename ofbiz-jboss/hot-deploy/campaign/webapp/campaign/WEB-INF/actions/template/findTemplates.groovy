import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.condition.EntityOperator;
import org.fio.util.DataHelper
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.util.EntityFindOptions;

delegator = request.getAttribute("delegator");

templateCategoryId = request.getParameter("templateCategoryId");

templateType  = request.getParameter("templateType");
System.out.println("templateType--111-->"+templateType);

EntityCondition tempTypeCondition = null;
conditionsList = FastList.newInstance();
if (UtilValidate.isNotEmpty(templateType)) {
    context.put("templateType",templateType);
    tempTypeCondition = EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, templateType);
    conditionsList.add(tempTypeCondition);
}else{
    tempTypeCondition = EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, "EMAIL_BLAST");
    conditionsList.add(tempTypeCondition);
}
subject  = request.getParameter("subject");
if(UtilValidate.isNotEmpty(subject))
{
    context.put("subject",subject);
    tempTypeCondition = EntityCondition.makeCondition("subject", EntityOperator.LIKE, "%"+subject+"%");
    conditionsList.add(tempTypeCondition);
}
templateList = delegator.findList("TemplateMaster",EntityCondition.makeCondition(conditionsList,EntityOperator.AND),null,UtilMisc.toList("templateId DESC"),null,false);	
context.put("templateList",templateList);
/*
if (UtilValidate.isNotEmpty(templateCategoryId)) {
    conditionsList.add(EntityCondition.makeCondition("templateCategoryId",EntityOperator.EQUALS,templateCategoryId));
    //conditionsList.add(tempTypeCondition);
    EntityCondition makeConditions = EntityCondition.makeCondition(conditionsList,EntityOperator.AND);
    List<GenericValue> templateToCategory = delegator.findList("templateToCategory",makeConditions,null,UtilMisc.toList("templateId DESC"),null,false);
    System.out.println("templateToCategory---->"+templateToCategory);
}*/

//Dropdown temp type
EntityFindOptions findOpts = new EntityFindOptions();
findOpts.setDistinct(true);
//listOfTempType = delegator.findByAnd("template", null, UtilMisc.toList("templateType","description"), false);
listOfTempType = delegator.findList("TemplateMaster", null,  ['templateType'] as Set, null, findOpts, false);
context.put("listOfTempType",listOfTempType);

if (UtilValidate.isNotEmpty(templateCategoryId)) {
    context.put("templateCategoryId",templateCategoryId);
}


templateCategoryList = delegator.findByAnd("TemplateCategory", null, UtilMisc.toList("templateCategoryName"), false);
context.put("templateCategoryList", DataHelper.getDropDownOptions(templateCategoryList, "templateCategoryId", "templateCategoryName"));


templateCategory = delegator.findByAnd("TemplateToCategory", null, UtilMisc.toList("templateId"), false);
context.put("templateCategory",templateCategory);


