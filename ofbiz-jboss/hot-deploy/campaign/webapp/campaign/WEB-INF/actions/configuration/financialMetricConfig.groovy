import org.ofbiz.entity.util.EntityQuery;
import org.groupfio.custom.field.util.DataHelper;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;

/*enumerationSMTList = EntityQuery.use(delegator).from("Enumeration").where("enumTypeId","SOCIAL_MEDIA_TYPE").orderBy("sequenceId").queryList();
context.put("enumerationSMT", DataHelper.getDropDownOptions(enumerationSMTList, "enumId", "description"));*/

/*enumerationFMCList = EntityQuery.use(delegator).from("Enumeration").where("enumTypeId","FINANCIAL_CONFIG").orderBy("sequenceId").queryList();
context.put("enumerationFMC", DataHelper.getDropDownOptions(enumerationFMCList, "enumId", "description"));*/

campaignType = EntityQuery.use(delegator).from("CampaignType").orderBy("sequenceNo").queryList();
context.put("campaignTypeList", DataHelper.getDropDownOptions(campaignType, "campaignTypeId", "description"));
//List of Financial Metrics Configuration

financialMetricConfigList = delegator.findAll("FinancialMetricConfig", false);
context.put("financialMetricConfigList", financialMetricConfigList);

financialConfigId = request.getParameter("financialConfigId");
if(UtilValidate.isNotEmpty(financialConfigId)) {
   context.put("financialConfigId", financialConfigId);
   financialMetricConfigGV = EntityQuery.use(delegator).from("FinancialMetricConfig").where("financialConfigId",financialConfigId).queryOne();
   /*if(financialMetricConfigGV != null && financialMetricConfigGV.size() > 0 ) {
      context.put("costAmount", financialMetricConfigGV.getString(financialMetricConfigGV.getString("financialConfigType")));
      costNameEnum = EntityQuery.use(delegator).from("Enumeration").where("enumId",financialMetricConfigGV.getString("financialConfigType")).queryOne();
      if(costNameEnum != null && costNameEnum.size() > 0 ) {
         context.put("costName", costNameEnum.getString("description"));
      }
   }*/
   context.put("financialMetricConfigGV", financialMetricConfigGV);
}