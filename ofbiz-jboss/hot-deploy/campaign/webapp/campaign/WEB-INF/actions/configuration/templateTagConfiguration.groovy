import org.fio.util.DataHelper
import org.ofbiz.base.util.UtilMisc
import org.ofbiz.base.util.UtilValidate
import org.ofbiz.entity.GenericValue
import org.ofbiz.entity.condition.EntityCondition
import org.ofbiz.entity.condition.EntityOperator

List<GenericValue> templateTagTypeList = delegator.findAll("TemplateTagType", false);
context.put("templateTagTypeList", DataHelper.getDropDownOptions(templateTagTypeList, "templateTagTypeId", "description"));

flagMap = new LinkedHashMap();
flagMap.put("N", "No");
flagMap.put("Y", "Yes");
context.put("flagMap", flagMap);


tagId = request.getParameter("tagId");

if(UtilValidate.isNotEmpty(tagId)) {
	GenericValue dataTagConfig = delegator.findOne("DataTagConfiguration", UtilMisc.toMap("tagId",tagId), false);
}


List<GenericValue> templateTagTypes = delegator.findList("TemplateTagType",EntityCondition.makeCondition("templateTagTypeId",EntityOperator.NOT_EQUAL,"STANDARD"),null,null,null,false);
context.put("templateTagTypes", DataHelper.getDropDownOptions(templateTagTypes, "templateTagTypeId", "description"));
