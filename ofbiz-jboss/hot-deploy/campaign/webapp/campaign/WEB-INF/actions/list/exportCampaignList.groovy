import java.text.SimpleDateFormat

import org.fio.campaign.util.ExportFileUtil
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker
import org.ofbiz.base.component.ComponentConfig
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator
import org.ofbiz.entity.util.EntityFindOptions
import org.ofbiz.entity.util.EntityQuery
import org.ofbiz.entity.util.EntityUtil

import groovy.json.StringEscapeUtils
import javolution.util.FastList
import javolution.util.FastMap;

marketingCampaignId = request.getParameter("marketingCampaignId");
contactListId = request.getParameter("contactListId");
SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
Date now = new Date();
try {
	if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(contactListId)){
		
		systemProperty = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "system","systemPropertyId","downloadServer"), false);
		if(systemProperty != null && systemProperty.size() > 0){
			String downloadServer = systemProperty.getString("systemPropertyValue");
			if(UtilValidate.isNotEmpty(downloadServer)) {
				
			}
		}
		List campaignList = FastList.newInstance();
		Set<String> fieldToSelect = new HashSet<String>();
		fieldToSelect.add("csrPartyId");
		fieldToSelect.add("partyId");
		fieldToSelect.add("isApproved");
		fieldToSelect.add("acctPartyId");
		fieldToSelect.add("contactListId");
		
		EntityFindOptions efo = new EntityFindOptions();
		efo.setDistinct(true);
		efo.setLimit(1000);
		List <GenericValue> campaignContactListPartyList = delegator.findList("CampaignContactListParty", EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS,contactListId), fieldToSelect, null, null, false);
		for(GenericValue campaignContactListParty: campaignContactListPartyList) {
			String contactPartyId = campaignContactListParty.getString("partyId");
			if (UtilValidate.isNotEmpty(contactPartyId)) {
				Map <String, Object> dataMap = new LinkedHashMap();
				String firstName = "";
				String lastName = "";
				String contactName = "";
				String phoneNumber = "";
				String infoString = "";
				String rmName = "";
				String accountName = "";
				String accountId = "";
				String designation = "";
				String csrPartyId = "";
				String companyName = "";
				//String listType = "";
				String status = "Not Reviewed";
				String isApproved = campaignContactListParty.getString("isApproved");
				String acctPartyId = campaignContactListParty.getString("acctPartyId");
				//String contactListType = campaignContactListParty.getString("contactPurposeTypeId");
				/*if("LIVE".equals(contactListType)) {
					listType = "Production";
				} else {
					listType = "Test";
				}*/
				
				GenericValue contactPerson = EntityQuery.use(delegator).from("Person").where("partyId", contactPartyId).queryOne();
				if (contactPerson != null && contactPerson.size() > 0) {
					contactName = (UtilValidate.isNotEmpty(contactPerson.getString("firstName")) ? contactPerson.getString("firstName"):"").concat(UtilValidate.isNotEmpty(contactPerson.getString("lastName")) ? " " + contactPerson.getString("lastName"):"");
					contactName = contactName.replaceAll(",", "");
				}
				
				Map < String, String > partyContactInfo = PartyPrimaryContactMechWorker.getPartyPrimaryContactMechValueMaps(delegator, contactPartyId);
				if(partyContactInfo != null && partyContactInfo.size() > 0) {
					if (UtilValidate.isNotEmpty(partyContactInfo.get("PrimaryPhone"))) {
						phoneNumber = partyContactInfo.get("PrimaryPhone");
						phoneNumber = phoneNumber.substring(phoneNumber.lastIndexOf('-') + 1);
					}
					if(UtilValidate.isNotEmpty(partyContactInfo.get("EmailAddress"))) {
						infoString = partyContactInfo.get("EmailAddress");
					}
				}
				
				if (UtilValidate.isNotEmpty(isApproved) && "Y".equals(isApproved)) {
					status = "Approved";
				} else if (UtilValidate.isNotEmpty(isApproved) && "X".equals(isApproved)) {
					status = "Not Approved";
				}
				if (UtilValidate.isNotEmpty(acctPartyId)) {
					GenericValue partyGroupGV = EntityQuery.use(delegator).from("PartyGroup").where("partyId", acctPartyId).queryOne();
					if (partyGroupGV != null && partyGroupGV.size() > 0) {
						accountId = partyGroupGV.getString("partyId");
						accountName = partyGroupGV.getString("groupName");
						accountName = accountName.replaceAll(",", "");
					}
				}
				csrPartyId = campaignContactListParty.getString("csrPartyId");
				if (UtilValidate.isNotEmpty(csrPartyId)) {
					GenericValue rmDetails = EntityQuery.use(delegator).from("Person").where("partyId", csrPartyId).queryOne();
					if (rmDetails != null && rmDetails.size() > 0) {
						rmName = (UtilValidate.isNotEmpty(rmDetails.getString("firstName")) ? rmDetails.getString("firstName") : "").concat(UtilValidate.isNotEmpty(rmDetails.getString("lastName")) ? " " + rmDetails.getString("lastName") : "");
						rmName = rmName.replaceAll(",", "");
					}
				}
	
				//Designation
				List<GenericValue> hdpContactDesignationAssoc = EntityQuery.use(delegator).from("HdpContactDesignationAssoc").where("contactId", contactPartyId).queryList();
				
				if(hdpContactDesignationAssoc != null && hdpContactDesignationAssoc.size() > 0) {
					List<String> designationList = EntityUtil.getFieldListFromEntityList(hdpContactDesignationAssoc, "designationName", true);
					for(int i=0;i<designationList.size();i++) {
						if(i == 0) {
							designation = designationList.get(i);
						} else {
							designation = designation+"| "+designationList.get(i);
						}
					}
				}
				
				String rm = rmName.concat(UtilValidate.isNotEmpty(csrPartyId) ? "("+csrPartyId+")" : "");
				String contact = contactName.concat(UtilValidate.isNotEmpty(contactPartyId) ? "("+contactPartyId+")" : "");
				String account = accountName.concat(UtilValidate.isNotEmpty(accountId) ? "("+accountId+")" : "");
				dataMap.put("RMName", rm);
				dataMap.put("designation", designation);
				dataMap.put("idNubmer", contactPartyId);
				dataMap.put("contactName", contactName);
				dataMap.put("lcin", accountId);
				dataMap.put("accountName", accountName);
				dataMap.put("phoneNumber", phoneNumber);
				dataMap.put("infoString", infoString);
				dataMap.put("status", status);
				//dataMap.put("listType", listType);
				//dataMap.put("contactListId", campaignContactListParty.getString("contactListId"));
				//dataMap.put("partyId", contactPartyId);
				//dataMap.put("firstName", firstName);
				//dataMap.put("lastName", lastName);
				//dataMap.put("accountId", accountId);
				campaignList.add(dataMap);
			}
		}
		// contruct the file
		if(campaignList != null && campaignList.size() > 0) {
			String fileName = "CampaignList_"+marketingCampaignId+"_"+df.format(now)+".csv";
			String location = ComponentConfig.getRootLocation("campaign")+"/webapp/campaign-resource/files";
			String delimiter = ",";
			List<String> headers = new ArrayList<String>();
			headers.add("RM Name");
			headers.add("Designation");
			headers.add("ID Number");
			headers.add("Contact Name");
			headers.add("LCIN");
			headers.add("Account Name");
			headers.add("Phone Number");
			headers.add("Email");
			headers.add("Status");
			//headers.add("ListType");
			ExportFileUtil.constructFileFromList(delegator, campaignList, null, fileName, location, delimiter, true);
			Thread.sleep(1000);
			ExportFileUtil.downloadFile(request,response,location+File.separatorChar+fileName);
			boolean isdelete = false;
			if(isdelete) {
				File file = new File(location+File.separatorChar+fileName);
				file.delete();
			}
		}
	}
	return "success";
} catch(Exception e) {
	e.printStackTrace();
	return "error";
}


