import org.fio.crm.ajax.AjaxEvents
import org.fio.crm.constants.CrmConstants
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.groupfio.custom.field.util.DataUtil;
import org.ofbiz.party.party.PartyHelper;
import org.fio.campaign.util.LoginFilterUtil;
import org.fio.campaign.util.ResponseUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.ofbiz.base.util.UtilDateTime;

import java.util.TimeZone;

import javolution.util.FastList;



				HttpSession session = request.getSession();
				userLoginId = userLogin.getAt('partyId');

				List < String > accountList = new ArrayList < String > ();
				if (LoginFilterUtil.checkEmployeePosition(delegator, userLoginId)) {
				    Map < String, Object > dataSecurityMetaInfo = (Map < String, Object > ) session.getAttribute("dataSecurityMetaInfo");
				    if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {
				        List < String > lowerPositionPartyIds = (List < String > ) dataSecurityMetaInfo.get("lowerPositionPartyIds");
				        accountList = LoginFilterUtil.getCampaignsAccountList(delegator, lowerPositionPartyIds);
				    }
				}
				String campaignId = request.getParameter("campaignId");
				String campaignConId = campaignId + '_PROD_LIST';
				String customFieldName = request.getParameter("customFieldName");
				accountSearchPartyId = parameters.get("accountSearchPartyId");
				searchFirstName = parameters.get("searchFirstName");
				searchEmailId = parameters.get("searchEmailId");
				searchPhoneNum = parameters.get("searchPhoneNum");
				searchRoleTypeId = parameters.get("searchRoleTypeId");

				String draw = request.getParameter("draw");
				String start = request.getParameter("start");
				String length = request.getParameter("length");

				delegator = request.getAttribute("delegator");
				conditionsList = FastList.newInstance();

				EntityCondition partyCondition = null;
				if (UtilValidate.isNotEmpty(campaignId)) {
				    EntityCondition con = EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, campaignConId);
				    conditionsList.add(con);
				}


				if (UtilValidate.isNotEmpty(searchRoleTypeId)) {
				    println("searchRoleTypeId>> " + searchRoleTypeId);
				    conditionsList.add(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, searchRoleTypeId));
				}

				if (UtilValidate.isNotEmpty(accountSearchPartyId)) {

				    partyCondition = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, accountSearchPartyId);
				    conditionsList.add(partyCondition);
				}

				if (UtilValidate.isNotEmpty(searchFirstName)) {
				    EntityCondition nameCondition = EntityCondition.makeCondition([EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("firstName")), EntityOperator.LIKE, searchFirstName.toUpperCase() + "%"),
				        EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("groupName")), EntityOperator.LIKE, searchFirstName.toUpperCase() + "%")
				    ], EntityOperator.OR);
				    conditionsList.add(nameCondition);
				}
				column = null;
				if (("Opened").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("opened", EntityOperator.EQUALS, "Y");
				    column = "opened";
				    conditionsList.add(partyCondition);
				} else if (("Not Opened").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("notOpen", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "notOpen";
				} else if (("Sent").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("sent", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "sent";
				} else if (("Clicked").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("clicked", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "clicked";
				} else if (("Unsubscribed").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("unsubscribed", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "unsubscribed";
				} else if (("Subscribed").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("subscribed", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "subscribed";
				} else if (("Bounced").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("bounced", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "bounced";
				} else if (("Converted").equals(customFieldName)) {
				    partyCondition = EntityCondition.makeCondition("converted", EntityOperator.EQUALS, "Y");
				    conditionsList.add(partyCondition);
				    column = "converted";
				}
				if (UtilValidate.isNotEmpty(accountList) && accountList.size() != 0) {
				    EntityCondition accountCondition = EntityCondition.makeCondition("acctPartyId", EntityOperator.IN, accountList);
				    conditionsList.add(accountCondition);
				}
				EntityCondition partyCond = EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, "LIVE");
				conditionsList.add(partyCond);
				mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
				//EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
				//EntityFindOptions efo = new EntityFindOptions();
				//efo.setDistinct(false);
				
				String sortDir = "";
				String orderField = "";
				//String column = null;
				String orderColumnId = request.getParameter("order[0][column]");
				if(UtilValidate.isNotEmpty(orderColumnId)) {
					int sortColumnId = Integer.parseInt(orderColumnId);
					String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
					sortDir = request.getParameter("order[0][dir]").toUpperCase();
					orderField = sortColumnName;
				}else {
					orderField = "partyId";
				}
				
				EntityFindOptions efo = new EntityFindOptions();
				efo.setDistinct(false);
				int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
				int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
				efo.setOffset(startInx);
				efo.setLimit(endInx);
				
				long count = 0;
				EntityFindOptions efoNum= new EntityFindOptions();
				efoNum.setDistinct(true);
				efoNum.getDistinct();
				efoNum.setFetchSize(1000);
				
				count = delegator.findCountByCondition("CampaignContactListPartyAndContact", mainConditons, null, UtilMisc.toSet("partyId"), efoNum);
				
				/*
				EntityFindOptions  efoNum= new EntityFindOptions();
				efoNum.setDistinct(false);
				
				List < GenericValue > partyList =null;
				partyList = delegator.findList("CampaignContactListPartyAndContact", mainConditons,UtilMisc.toSet("partyId"), null, efoNum, false);
				int count = partyList.size();
				*/
				
				countList = null;
				if (column != null) {
				    acctconditionsList = FastList.newInstance();
				    countListCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, campaignConId),
				        EntityCondition.makeCondition(column, EntityOperator.EQUALS, "Y")
				    ], EntityOperator.AND);

				    if (UtilValidate.isNotEmpty(accountList) && accountList.size() != 0) {
				        EntityCondition accCondition = EntityCondition.makeCondition("acctPartyId", EntityOperator.IN, accountList);
				        acctconditionsList.add(accCondition);
				    }
				    acctconditionsList.add(countListCondition);
				    EntityCondition acctmainConditons = EntityCondition.makeCondition(acctconditionsList, EntityOperator.AND);
				    countList = delegator.findCountByCondition('CampaignContactListParty', acctmainConditons, null, null);
				}
				
				List < GenericValue >  partyListCall = null;
				partyListCall = delegator.findList('CampaignContactListPartyAndContact', mainConditons, UtilMisc.toSet("partyId","firstName","lastName"), null, efo, false);
				
				JSONObject obj = new JSONObject();
				JSONArray dataMap = new JSONArray();

				long recordsFiltered = count;
				long recordsTotal = count;

				if (column != null && countList != null && partyListCall != null && partyListCall.size() > 0) {
				    int id = 1;
				    for (GenericValue party: partyListCall) {
				         partyId = party.getString("partyId");	
				        partySummary = delegator.findOne("Party", ["partyId": partyId], false);
				        if (partySummary != null && partySummary.size() > 0) {
				            JSONObject list = new JSONObject();
				           // String groupName = partySummary.getString("groupName");
				            String statusId = partySummary.getString("statusId");
				            String statusItemDesc = "";
				            //String name = partySummary.getString("firstName") + " " + partySummary.getString("lastName");
				            if (UtilValidate.isNotEmpty(statusId)) {
				                statusItem = delegator.findOne("StatusItem", ["statusId": statusId], false);
				                if (statusItem != null && statusItem.size() > 0) {
				                    statusItemDesc = statusItem.getString("description");
				                }
				            }
				            //name = PartyHelper.getPartyName(delegator, partyId, false);
							String name = party.getString("firstName") + " " + party.getString("lastName");
				            String city = "";
				            String state = "";
				            String phoneNumber = "";
				            String infoString = "";
				            Map < String, String > partyContactInfo = PartyPrimaryContactMechWorker.getPartyPrimaryContactMechValueMaps(delegator, partyId);
				            phoneNumber = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("PrimaryPhone") : "";
				            infoString = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("EmailAddress") : "";

				            GenericValue postalAddress = PartyPrimaryContactMechWorker.getPartyPrimaryPostal(delegator, partyId);

				            String nameWithId = name + " (" + partyId + ")";
				            String nameWithIdLink = "";

				            conditionsListt = FastList.newInstance();

				            partyConditionn = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
				            conditionsListt.add(partyConditionn);
				            roleCondition = EntityCondition.makeCondition(
				                [EntityCondition.makeCondition(EntityFieldValue.makeFieldValue("roleTypeId"), EntityOperator.EQUALS, "CONTACT"),
				                    EntityCondition.makeCondition(EntityFieldValue.makeFieldValue("roleTypeId"), EntityOperator.EQUALS, "LEAD")
				                ],
				                EntityOperator.OR);
				            conditionsListt.add(roleCondition);

				            mainConditonss = EntityCondition.makeCondition(conditionsListt, EntityOperator.AND);


				            partyDetail = EntityUtil.getFirst(delegator.findList("PartyRole", mainConditonss, null, UtilMisc.toList("-partyId"), null, false));
				            if (UtilValidate.isNotEmpty(partyDetail) && UtilValidate.isNotEmpty(partyDetail.getString("roleTypeId"))) {
				                if (partyDetail.getString("roleTypeId").equals("CONTACT")) {
				                    nameWithIdLink = '<a target="_blank" href="/crm/control/viewContact?partyId=' + partyId + '">' + nameWithId + '</a>';
				                } else if (partyDetail.getString("roleTypeIdFrom").equals("LEAD")) {
				                    nameWithIdLink = '<a target="_blank" href="/crm/control/viewLead?partyId=' + partyId + '">' + nameWithId + '</a>';
				                } else {
				                    nameWithIdLink = nameWithId;
				                }
				            }
				            id = id + 1;
				            list.put("id", id + "");
				            list.put("partyId", partyId);
				            list.put("name", name);
				            list.put("nameWithId", nameWithId);
				            list.put("nameWithIdLink", nameWithIdLink);
				            list.put("statusId", statusItemDesc);
				            list.put("phoneNumber", phoneNumber);
				            list.put("infoString", infoString);

				            dataMap.add(list);
				        }
				    }

				    obj.put("data", dataMap);
				    obj.put("draw", draw);
				    obj.put("recordsTotal", recordsTotal);
				    obj.put("recordsFiltered", recordsFiltered);

				    return AjaxEvents.doJSONResponse(response, obj);

				} else {

				    obj.put("data", dataMap);
				    obj.put("draw", draw);
				    obj.put("recordsTotal", 0);
				    obj.put("recordsFiltered", 0);

				    return AjaxEvents.doJSONResponse(response, obj);

				}