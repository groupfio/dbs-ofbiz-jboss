import java.util.Map;
import org.ofbiz.entity.*;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityCondition;
import javolution.util.FastList;
import javolution.util.FastMap;

Map rmMap = new LinkedHashMap();
rmMap.put("ACCOUNT_MANAGER", "Account Manager");
rmMap.put("CUST_SERVICE_REP", "Customer Service Rep");
context.put("rmMap", rmMap);
String marketingCampaignId = parameters.get("marketingCampaignId");
campaignCsrAssocList = delegator.findByAnd("CampaignCsrAssoc", UtilMisc.toMap("campaignId", marketingCampaignId), null, false);
if(campaignCsrAssocList != null && campaignCsrAssocList.size() > 0) {
    context.put("campaignCsrAssocListRMToCamp", campaignCsrAssocList);
}