
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.util.EntityUtil;
import org.apache.wink.json4j.JSONObject;
import org.ofbiz.entity.util.EntityFindOptions;

import javolution.util.FastList;



contactListId = request.getParameter("contactListId");
updateFlag = request.getParameter("updateFlag");

if(contactListId!=null){

	contactListCriteria = delegator.findOne("ContactList",UtilMisc.toMap("contactListId", contactListId), false);
	if(contactListCriteria!=null){
		context.criteria = contactListCriteria.criteria;
		isProcessed = contactListCriteria.getString("isProcessed");
		isSmartList = contactListCriteria.getString("isSmartList");
		if((UtilValidate.isEmpty(isProcessed) || "N".equals(isProcessed)) && "Y".equals(isSmartList)) {
			context.put("executeList","Y");
		}
	}	
}

List msgList = FastList.newInstance();
if(UtilValidate.isNotEmpty(updateFlag))
{
	if("Y".equals(updateFlag)) {
		System.out.println("msg----@M--->"+updateFlag);
		msgList.add("Smart List Updated successfully");
		request.setAttribute("eventMessageList", msgList);
	}
	if("N".equals(updateFlag))
	{
		System.out.println("msg---@M--->"+updateFlag);
		msgList.add("Smart List Created successfully");
		request.setAttribute("eventMessageList", msgList);
	}
}

/*List seqList = new ArrayList();
	Map finalJson = new HashMap();
	contactListCriteria = delegator.findByAnd("ContactListCriteria",[contactListId:contactListId], null, false);
	 if(contactListCriteria!=null){
		 String action = "";
		 List innerJsonList = new ArrayList();
		 //Map innerjson = null;
		 for(GenericValue gv : contactListCriteria)
		  {
			  Map<String, String> innerjson = new HashMap<String, String>();
			  Map map = new HashMap();

			  map.put("contactListId", contactListId);
			  int sequence = gv.get("sequence");
			  map.put("sequence", sequence);
			 // seqList.add(sequence);
			  contactListCriteria = delegator.findOne("ContactListCriteria",map, false);
			  if(contactListCriteria!=null){
				  JSONObject obj = new JSONObject();
				  String field = contactListCriteria.get("fieldType");
				  println("fields--->"+field);
				  String criteria = contactListCriteria.get("criteria");
				  action = contactListCriteria.get("action");
				  if(field.equalsIgnoreCase("configuration"))
				  {
					  String val = contactListCriteria.get("value");

					  innerjson.put("field", field);
					  innerjson.put("criteria", criteria);
					  innerjson.put("val", val);
				  }
				  else
				  {
					  innerjson.put("field", field);
					  innerjson.put("criteria", criteria);
				  }  	  
			  }
			  innerJsonList.add(innerjson);
		  }
		  finalJson.put("condition", action);
		 finalJson.put("rules", innerJsonList);
	  }
	String resultCriteria = JSONValue.toJSONString(finalJson);
	 context.put("criteria",resultCriteria);
	 println("Final json --->"+resultCriteria);
}*/