import java.sql.Timestamp;
import java.util.HashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker;
import org.groupfio.custom.field.util.DataHelper;

delegator = request.getAttribute("delegator");
List exprListForParametersAL = [];
List approveListAL = [];
List orConditionList = [];
userLogin = request.getAttribute("userLogin");
String marketingCampaignId = parameters.get("marketingCampaignId");
String campaignIdAL = parameters.get("campaignIdAL");
String statusIdAL = parameters.get("statusIdAL");
String rmUserLoginIdAL = parameters.get("rmUserLoginIdAL");
String contactTypeId = parameters.get("contactTypeId");
String emailCampaignAL = "N";
String phoneCampaignAL = "N";
if (UtilValidate.isNotEmpty(marketingCampaignId)) {
    context.put("marketingCampaignId", marketingCampaignId);
}
if (UtilValidate.isNotEmpty(campaignIdAL)) {
    context.put("campaignIdAL", campaignIdAL);
}
if (UtilValidate.isNotEmpty(rmUserLoginIdAL)) {
    context.put("rmUserLoginIdAL", rmUserLoginIdAL);
}
if (UtilValidate.isNotEmpty(statusIdAL)) {
    context.put("statusIdAL", statusIdAL);
}
if (UtilValidate.isNotEmpty(contactTypeId)) {
	context.put("contactTypeId", contactTypeId);
}
Map optionMap = new LinkedHashMap();
optionMap.put("N", "Not Reviewed");
optionMap.put("Y", "Approved");
optionMap.put("X", "Not Approved");
context.put("optionMap", optionMap);



//Contact Type DropDown
Map listContMap = new HashMap();
listContMap.put("TEST", "Test");
listContMap.put("LIVE", "Production");

context.put("contactType",listContMap);

//Get RM responsible for
//List rmResponsibleForAL = delegator.findAll("CampaignContactListParty", false);
EntityQuery rmResponsibleForAL = EntityQuery.use(delegator).select("csrPartyId").from("CampaignContactListParty");
Set < String > csrPartyIdSet = EntityUtil.getFieldListFromEntityList(rmResponsibleForAL.queryList(), "csrPartyId", true);
if (csrPartyIdSet != null && csrPartyIdSet.size() > 0) {
    List personListAL = EntityQuery.use(delegator).from("Person")
        .where(EntityCondition.makeCondition("partyId", EntityOperator.IN, csrPartyIdSet))
        .queryList();
    if (personListAL != null && personListAL.size() > 0) {
        Map < String, Object > optionsAL = new HashMap < String, Object > ();
        for (GenericValue personAL: personListAL) {
            name = personAL.getString("firstName");
            if(UtilValidate.isNotEmpty(personAL.getString("lastName"))) {
                name = name +" "+personAL.getString("lastName");
            }
            optionsAL.put(personAL.getString("partyId"), name);
        }
        context.put("responsibleForAL", optionsAL);
    }
}

//Get List of marketing campaign
conditionMKTAL = EntityCondition.makeCondition(UtilMisc.toList(
    EntityCondition.makeCondition("endDate", EntityOperator.GREATER_THAN_EQUAL_TO, UtilDateTime.nowTimestamp()),
    EntityCondition.makeCondition("endDate", EntityOperator.EQUALS, null)), EntityOperator.OR);
List marketingCampaignListAL = delegator.findList("MarketingCampaign", conditionMKTAL, null, null, null, false);
context.put("marketingCampaignListAL", DataHelper.getDropDownOptions(marketingCampaignListAL, "marketingCampaignId", "campaignName"));

if (UtilValidate.isEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(campaignIdAL)) {
    marketingCampaignId = campaignIdAL;
}

if (UtilValidate.isNotEmpty(marketingCampaignId)) {
    context.put("marketingCampaignIdAjaxCall", marketingCampaignId);
    marketingCampaignAL = EntityQuery.use(delegator).from("MarketingCampaign").where("marketingCampaignId", marketingCampaignId).queryOne();
    if(marketingCampaignAL != null && marketingCampaignAL.size() > 0) {
        String campaignTypeId = marketingCampaignAL.getString("campaignTypeId");
        if(UtilValidate.isNotEmpty(campaignTypeId) && "EMAIL".equalsIgnoreCase(campaignTypeId)) {
            emailCampaignAL = "Y";
        } else if(UtilValidate.isNotEmpty(campaignTypeId) && ("PHONE_CALL".equalsIgnoreCase(campaignTypeId) || "SMS".equalsIgnoreCase(campaignTypeId))) {
            phoneCampaignAL = "Y";
        }
    }
    marketingCampaignContactListAL = EntityQuery.use(delegator).from("MarketingCampaignContactList")
        .where("marketingCampaignId", marketingCampaignId).filterByDate().orderBy("-fromDate").queryList();
    if (marketingCampaignContactListAL != null && marketingCampaignContactListAL.size() > 0) {
        List < String > contactListIdStringAL = EntityUtil.getFieldListFromEntityList(marketingCampaignContactListAL, "contactListId", true);
        if (contactListIdStringAL != null && contactListIdStringAL.size() > 0) {
            contactListAL = EntityQuery.use(delegator).from("ContactList")
                .where(EntityCondition.makeCondition([EntityCondition.makeCondition("contactListId", EntityOperator.IN, contactListIdStringAL),
                    EntityCondition.makeCondition("contactListTypeId", EntityOperator.EQUALS, "CAMPAIGN_LIST")
                ])).queryFirst();
            if (contactListAL != null && contactListAL.size() > 0) {
                contactListIdAL = contactListAL.getString("contactListId");
                if (UtilValidate.isNotEmpty(contactListIdAL)) {
                    context.put("contactListIdAL", contactListIdAL);
                }
            }
        }
    }
}
context.put("phoneCampaignAL", phoneCampaignAL);
context.put("emailCampaignAL", emailCampaignAL);
context.put("approveListAL", approveListAL);