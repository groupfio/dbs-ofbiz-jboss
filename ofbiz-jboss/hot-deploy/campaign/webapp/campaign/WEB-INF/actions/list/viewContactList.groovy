import org.ofbiz.base.util.UtilValidate;
import org.fio.campaign.common.MarketingHelper;
import org.ofbiz.entity.condition.*;

contactListId = request.getParameter("contactListId");
contactList = from('ContactList').where('contactListId', contactListId).queryOne();
context.put("contactList",contactList);
context.put("isUpdate","true");

contactListParty = delegator.findByAnd("ContactListParty", [contactListId: contactListId], null, false);
numberOfMembers = contactListParty.size();
context.put("numberOfMembers",numberOfMembers);

totalNoPartyFound="0";
totaldedupCount="0";
totalDisableParty="0";
totalNoOfSucess="0";
totalNoOfParties="0";

totalNoDefaultCount="0";
roleNotFoundCount="0";
noAccountCount="0";
noUniqueContactCount="0";
noFlagCount="0";
totalSuccessCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","SUCCESS_COUNT").queryOne();
if(UtilValidate.isNotEmpty(totalSuccessCount)){
    totalNoOfSucess=totalSuccessCount.get("attrValue");
}
context.put("totalNoOfSucess",totalNoOfSucess);
totalNoPartyFound = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E135").queryOne(); 
if(UtilValidate.isNotEmpty(totalNoPartyFound)){
    totalNoPartyFound=totalNoPartyFound.get("attrValue");
}
context.put("totalNoPartyFound",totalNoPartyFound);

totaldedupCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E131").queryOne();
if(UtilValidate.isNotEmpty(totaldedupCount)){
	totaldedupCount=totaldedupCount.get("attrValue");
}
context.put("totaldedupCount",totaldedupCount);

totalDisableParty = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E134").queryOne();
if(UtilValidate.isNotEmpty(totalDisableParty)){
	totalDisableParty=totalDisableParty.get("attrValue");
}
context.put("totalDisableParty",totalDisableParty);

totalPartiesCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","TOTAL_COUNT").queryOne(); 
if(UtilValidate.isNotEmpty(totalPartiesCount)){
    totalNoOfParties=totalPartiesCount.get("attrValue");
}
context.put("totalNoOfParties",totalNoOfParties);

totalNoDefaultCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E138").queryOne(); 
if(UtilValidate.isNotEmpty(totalNoDefaultCount)){
    totalNoDefaultCount=totalNoDefaultCount.get("attrValue");
}
context.put("totalNoDefaultCount",totalNoDefaultCount);

roleNotFoundCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E136").queryOne(); 
if(UtilValidate.isNotEmpty(roleNotFoundCount)){
    roleNotFoundCount=roleNotFoundCount.get("attrValue");
}
context.put("roleNotFoundCount",roleNotFoundCount);

noUniqueContactCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E139").queryOne(); 
if(UtilValidate.isNotEmpty(noUniqueContactCount)){
    noUniqueContactCount=noUniqueContactCount.get("attrValue");
}
context.put("noUniqueContactCount",noUniqueContactCount);

noAccountCount = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E137").queryOne(); 
if(UtilValidate.isNotEmpty(noAccountCount)){
    noAccountCount=noAccountCount.get("attrValue");
}
context.put("noAccountCount",noAccountCount);

noFlagCountList = from('ContactListAttribute').where('contactListId', contactListId,"attrName","E142").queryOne();
if(UtilValidate.isNotEmpty(noFlagCountList)){
	noFlagCount=noFlagCountList.get("attrValue");
}
context.put("noFlagCount",noFlagCount);

int value = java.text.NumberFormat.instance.parse( totalNoOfParties );
int value1 = java.text.NumberFormat.instance.parse( totalNoOfSucess );
 ecl = EntityCondition.makeCondition([
                                    EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                                    EntityCondition.makeCondition("errorLog", EntityOperator.NOT_EQUAL, "E140")],
                                EntityOperator.AND);
errorCount = from('FioContactListErrorLog').where(ecl).queryList(); 
if(UtilValidate.isNotEmpty(errorCount)){
    totalNoOfError=errorCount.size();
}else{
    totalNoOfError=0;
}
context.put("totalNoOfError", totalNoOfError)


boolean isCreated = false;
	//uploadFileStatus = delegator.findOne("ContactListAttribute",[contactListId:contactListId,attrName:"UPLOAD_FILE_STATUS"],null,false);
	uploadFileStatus = from('ContactListAttribute').where('contactListId', contactListId,"attrName","UPLOAD_FILE_STATUS").queryOne();
	
	if(UtilValidate.isNotEmpty(uploadFileStatus))
	{
		String attrValue = uploadFileStatus.getAt("attrValue");
		
		if("CREATED".equals(attrValue))
		{
			isCreated = true;
		}
	}	
	context.put("isCreated",isCreated);
	
//Error Log view
	//List errorLog = [];
	errorLog = from('FioContactListErrorLog').where('contactListId', contactListId).orderBy("ErrorLogId DESC").queryList();
	if(UtilValidate.isNotEmpty(errorLog))
		{
			context.put("errorLog",errorLog);
			
		}

//Check contact list association with Campaign
		boolean isAssociated = false;		
		associatedContactList = from('MarketingCampaignContactList').where('contactListId', contactListId).queryList();
		if(UtilValidate.isNotEmpty(associatedContactList))
			{
				isAssociated= true;
				
			}
			context.put("isAssociated",isAssociated);

accountInListCount=MarketingHelper.countAccountListMembers(contactListId,delegator);
context.put("accountInListCount",accountInListCount);

Map roleList=new HashMap();
roleList.put("LEAD","Lead");
roleList.put("ACCOUNT","Account");
//roleList.put("CONTACT","Contact");
context.put("roleList",roleList);
			
roleTypeVal = from('ContactListAttribute').where('contactListId', contactListId,"attrName","ROLE_TYPE_ID").queryOne();
if(UtilValidate.isNotEmpty(roleTypeVal)){
    roleDesc="";
    roleTypeIdVal=roleTypeVal.get("attrValue");
    if(roleTypeIdVal=="LEAD"){
       roleDesc="Lead";
    }else if(roleTypeIdVal=="ACCOUNT"){
       roleDesc="Account";
    }else if(roleTypeIdVal=="CONTACT"){
       roleDesc="Contact";
    }
    context.put("roleDesc",roleDesc);	
    context.put("roleTypeIdVal",roleTypeIdVal);			
    
}
isValidateList = from("ContactListAttribute").where("contactListId", contactListId,"attrName","IS_VALIDATE").queryOne();
if(UtilValidate.isNotEmpty(isValidateList)){
isValidate= isValidateList.getString("attrValue");			
context.put("isValidate",isValidate);
}		
