import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import org.ofbiz.base.util.UtilMisc;
import java.util.List;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.util.EntityFindOptions;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilValidate;
import org.fio.crm.constants.CrmConstants;

outboundCall = "N";
pretailLoyaltyGlobalParameter = delegator.findByAnd("PretailLoyaltyGlobalParameters", UtilMisc.toMap("parameterId", "OUTBOUND_CALL", "value", "Y"), null, false);
if (pretailLoyaltyGlobalParameter != null && pretailLoyaltyGlobalParameter.size() > 0) {
    partyId = parameters.get("partyId");
    outboundCall = "Y";
    callStatusHistoryList = [];
    callHistoryConditions = [];
    partyCallCond = [];
    if (UtilValidate.isNotEmpty(partyId)) {
        List partyNotesList = new ArrayList();
            accountPartyNote = "N";
            EntityCondition roleCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
                EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
                EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, CrmConstants.PartyRelationshipTypeConstants.CONTACT_REL_INV)
            ], EntityOperator.AND);

            callHistoryConditions.add(roleCondition);

            EntityCondition statusCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
                EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
            ], EntityOperator.OR);

            callHistoryConditions.add(statusCondition);

            callHistoryConditions.add(EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyId));


            EntityFindOptions efo1 = new EntityFindOptions();
            efo1.setDistinct(true);
            callHistoryConditions.add(EntityUtil.getFilterByDateExpr());
            partyFromRelnListNote = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", EntityCondition.makeCondition(callHistoryConditions, EntityOperator.AND), null, UtilMisc.toList("createdDate"), efo1, false);
            if (partyFromRelnListNote != null && partyFromRelnListNote.size() > 0) {
                partyFromRelnNote = EntityUtil.getFieldListFromEntityList(partyFromRelnListNote, "partyIdFrom", true);
                if (partyFromRelnNote != null && partyFromRelnNote.size() > 0) {
                    partyCallCond.add(EntityCondition.makeCondition("partyId", EntityOperator.IN, partyFromRelnNote));
                }
            }
            partyCallCond.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId));

            callRecordMasterList = delegator.findList("CallRecordDetails", EntityCondition.makeCondition(partyCallCond, EntityOperator.OR), null, UtilMisc.toList("lastUpdatedTxStamp DESC"), null, false);

            if (callRecordMasterList != null && callRecordMasterList.size() > 0) {
                for (GenericValue callRecordMaster: callRecordMasterList) {
                    Map callStatusHistoryMap = new HashMap();

                    marketingCampaignId = callRecordMaster.getString("marketingCampaignId");
                    callStatusId = callRecordMaster.getString("callStatusId");
                    callSubStatusId = callRecordMaster.getString("callSubStatusId");
                    createdStamp = callRecordMaster.getString("createdStamp");
                    csrPartyId = callRecordMaster.getString("csrPartyId");
                    if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                        marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId", marketingCampaignId), false);
                        if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                            callStatusHistoryMap.put("campaignName", marketingCampaign.getString("campaignName"));
                        }
                    }
                    
                    if (UtilValidate.isNotEmpty(callStatusId)) {
                        enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId", callStatusId), false);
                        if (UtilValidate.isNotEmpty(enumeration)) {
                            callStatusHistoryMap.put("description", enumeration.getString("description"));
                        }
                    }
                    if (UtilValidate.isNotEmpty(callSubStatusId)) {
                        enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId", callSubStatusId), false);
                        if (UtilValidate.isNotEmpty(enumeration)) {
                            callStatusHistoryMap.put("subStatusDescription", enumeration.getString("description"));
                        }
                    }
                    
                    if (UtilValidate.isNotEmpty(csrPartyId)) {
                        person = delegator.findOne("Person", UtilMisc.toMap("partyId", csrPartyId), false);
                        if (UtilValidate.isNotEmpty(person)) {
                            csrName = person.getString("firstName") + " " + person.getString("lastName");
                            callStatusHistoryMap.put("csrName", csrName);
                        }
                    }
                    if (UtilValidate.isNotEmpty(createdStamp)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yy");
                        String fromDate1 = null;
                        if (UtilValidate.isNotEmpty(createdStamp)) {
                            date1 = sdf.parse(createdStamp);
                            fromDate1 = sdf1.format(date1);
                            callStatusHistoryMap.put("createdStamp", fromDate1);
                        }
                    }
                    callStatusHistoryList.add(callStatusHistoryMap);
                }
                context.put("callStatusHistory", callStatusHistoryList);
            }
    }
}
context.put("outboundCall", outboundCall);