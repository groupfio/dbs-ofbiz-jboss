import java.text.SimpleDateFormat

import org.fio.campaign.util.ExportFileUtil
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker
import org.ofbiz.base.component.ComponentConfig
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator
import org.ofbiz.entity.util.EntityFindOptions
import org.ofbiz.entity.util.EntityQuery
import org.ofbiz.entity.util.EntityUtil

import groovy.json.StringEscapeUtils
import javolution.util.FastList
import javolution.util.FastMap;

contactListId = request.getParameter("contactListId");
SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
Date now = new Date();
try {
	if(UtilValidate.isNotEmpty(contactListId)){
		List contactList = FastList.newInstance();
		EntityFindOptions efo = new EntityFindOptions();
		efo.setDistinct(true);
		efo.setLimit(1000);
		contactName = "";
		GenericValue contactListName = select("contactListName") .from("ContactList").where("contactListId", contactListId).queryOne();
		if (contactListName != null && contactListName.size() > 0) {
			contactName = contactListName.getString("contactListName");
		}
		
		List <GenericValue> campaignContactErrorList = delegator.findList("FioContactListErrorLog", EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS,contactListId), null, null, null, false);
		for(GenericValue campaignContactError: campaignContactErrorList) {
			String contactPartyId = campaignContactError.getString("contactListId");
			String checkErrorLog = campaignContactError.getString("errorLog");
			if('E140'.equals(checkErrorLog)){
				if (UtilValidate.isNotEmpty(contactPartyId)) {
					Map <String, Object> dataMap = new LinkedHashMap();
					String errorLogId = "";
					String customerId = "";	
					String errorCode = "";
					errorDescription = "";
					errorLogId = campaignContactError.getString("ErrorLogId");
					//contactName = contactListName;
					customerId = campaignContactError.getString("partyId");;
					rm = campaignContactError.getString("csrPartyId");
					errorCode = campaignContactError.getString("errorLog");
					errorDescriptionList = select('codeDescription') .from("ErrorCode").where("errorCodeId", errorCode).queryOne();
					if (errorDescriptionList != null && errorDescriptionList.size() > 0) {
						errorDescription = errorDescriptionList.getString("codeDescription");
					}
					dataMap.put("errorLogId", errorLogId);
					dataMap.put("contactName", contactName);
					dataMap.put("customerId", customerId);
					dataMap.put("errorLogId", errorLogId);
					dataMap.put("errorCode", errorCode);
					dataMap.put("errorDescription", errorDescription);		
					contactList.add(dataMap);
				}
			}	
		}
		// contruct the file
		if(contactList != null && contactList.size() > 0) {
			String fileName = "ErrorLog"+contactListId+".csv";
			String location = ComponentConfig.getRootLocation("campaign")+"/webapp/campaign-resource/files";
			String delimiter = ",";
			List<String> headers = new ArrayList<String>();
			headers.add("Log Id");
			headers.add("Contact List Name");
			headers.add("Customer Id");	
			headers.add("Error Code");
			headers.add("Error Description");
			ExportFileUtil.constructFileFromList(delegator, contactList, headers, fileName, location, delimiter, true);
			Thread.sleep(1000);
			ExportFileUtil.downloadFile(request,response,location+File.separatorChar+fileName);
			boolean isdelete = false;
			if(isdelete) {
				File file = new File(location+File.separatorChar+fileName);
				file.delete();
			}
		}
	}
	return "success";
} catch(Exception e) {
	return "error";
}


