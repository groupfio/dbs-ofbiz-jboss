import java.util.List

import javolution.util.FastList
import net.sf.json.JSONArray
import net.sf.json.JSONObject

import org.fio.campaign.events.AjaxEvents
import org.fio.util.DataHelper
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition
import org.ofbiz.entity.condition.EntityOperator
import org.ofbiz.entity.util.EntityFindOptions

delegator = request.getAttribute("delegator");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String smartListId = request.getParameter("smartListId");

JSONObject obj = new JSONObject();
JSONArray dataMap = new JSONArray();
long recordsFiltered = 0;
long recordsTotal = 0;
List < String > orderBy = FastList.newInstance();
String orderField = "";
String sortDir = "ASC";
String orderColumnId = request.getParameter("order[0][column]");
if (UtilValidate.isNotEmpty(orderColumnId)) {
    int sortColumnId = Integer.parseInt(orderColumnId);
    String sortColumnName = request.getParameter("columns[" + sortColumnId + "][data]");
    sortDir = request.getParameter("order[0][dir]").toUpperCase();
    orderField = sortColumnName;
} else {
    orderField = "smartListId";
}
List < EntityCondition > conditionList = FastList.newInstance();
if (UtilValidate.isNotEmpty(smartListId)) {
    conditionList.add(EntityCondition.makeCondition("smartListId", EntityOperator.EQUALS, smartListId));
}
/*if(UtilValidate.isNotEmpty(type)) {
	conditionList.add(EntityCondition.makeCondition("type",EntityOperator.EQUALS,"CAMPAIGN_TYPE"));
}*/

EntityCondition condition = EntityCondition.makeCondition(conditionList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

long count = 0;
EntityFindOptions  efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("SmartlistTemplateBuilder", mainConditons, null, UtilMisc.toSet("smartListId"), efoNum);

/*
List < GenericValue > smartListTemplateList1 = delegator.findList("SmartlistTemplateBuilder", condition, null, null, efo, false);
int count = 0;
if (smartListTemplateList1 != null && smartListTemplateList1.size() > 0) {
    count = smartListTemplateList1.size();
}
*/

recordsFiltered = count;
recordsTotal = count;

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
efo.setOffset(startInx);
efo.setLimit(endInx);

List < GenericValue > smartListTemplateList1 = delegator.findList("SmartlistTemplateBuilder", condition, null, UtilMisc.toList("lastUpdatedTxStamp DESC"), efo, false);
if (smartListTemplateList1 != null && smartListTemplateList1.size() > 0) {
    for (int i = 0; i < smartListTemplateList1.size(); i++) {
    	GenericValue smartListTemplateGv = smartListTemplateList1.get(i);
        JSONObject listObj = new JSONObject();
        String smartListTemplateId = smartListTemplateGv.getString("smartListId");

        listObj.put("smartListId", smartListTemplateId);
        listObj.put("name", UtilValidate.isNotEmpty(smartListTemplateGv.getString("name")) ? smartListTemplateGv.getString("name") : "");
        listObj.put("type", UtilValidate.isNotEmpty(smartListTemplateGv.getString("type")) ? smartListTemplateGv.getString("type") : "");
        listObj.put("marketingCampaignId", UtilValidate.isNotEmpty(smartListTemplateGv.getString("marketingCampaignId")) ? smartListTemplateGv.getString("marketingCampaignId") : "");
        listObj.put("isProcessed", UtilValidate.isNotEmpty(smartListTemplateGv.getString("isProcessed")) ? smartListTemplateGv.getString("isProcessed") : "");
        dataMap.add(listObj);
    }
    obj.put("data", dataMap);
    obj.put("draw", draw);
    obj.put("recordsTotal", recordsTotal);
    obj.put("recordsFiltered", recordsFiltered);
    //System.out.println("obj-->"+obj);
    return AjaxEvents.doJSONResponse(response, obj);
} else {
}