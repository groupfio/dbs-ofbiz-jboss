import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.fio.util.DataHelper;
smartListId = request.getParameter("smartListId");
if(UtilValidate.isNotEmpty(smartListId)){
	context.put("smartListId",smartListId);
	GenericValue smartListTemplate = delegator.findOne("SmartlistTemplateBuilder",UtilMisc.toMap("smartListId",smartListId),false);
	if(smartListTemplate != null && smartListTemplate.size() > 0){
		context.put("criteria",smartListTemplate.getString("criteria"));
		context.put("smartListTemplate", smartListTemplate);
	}
}

List < GenericValue > smartListTemplateList = delegator.findAll("SmartlistTemplateBuilder", false);
if (smartListTemplateList != null && smartListTemplateList.size() > 0) {
    context.put("smartListTemplateList", DataHelper.getDropDownOptions(smartListTemplateList, "smartListId", "name"));
}
