import java.sql.Timestamp;
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.groupfio.custom.field.util.DataHelper;

delegator = request.getAttribute("delegator");
userLogin = session.getAttribute("userLogin");
userLoginId = "";
if (userLogin != null) {
    userLoginId = userLogin.getString("userLoginId");
}
generatePhoneCallList = false;
//Get RM responsible for
List rmResponsibleForOCL = delegator.findAll("CallRecordMaster", false);
if (rmResponsibleForOCL != null && rmResponsibleForOCL.size() > 0) {
    List personListOCL = EntityQuery.use(delegator).from("Person")
        .where(EntityCondition.makeCondition("partyId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(rmResponsibleForOCL, "csrPartyId", true)))
        .queryList();
    if (personListOCL != null && personListOCL.size() > 0) {
        Map < String, Object > optionsOCL = new LinkedHashMap < String, Object > ();
        for (GenericValue personOCL: personListOCL) {
            optionsOCL.put(personOCL.getString("partyId"), personOCL.getString("firstName") + " " + personOCL.getString("lastName"));
        }
        context.put("responsibleForOCL", optionsOCL);
    }
}

//Get List of marketing campaign
campaignCheckingUri = request.getRequestURI();
marketingCampaignId = parameters.get("marketingCampaignId");
if (UtilValidate.isNotEmpty(marketingCampaignId)) {
    context.put("marketingCampaignId", marketingCampaignId);
}
List marketingCampaignListOCL = null;
if (campaignCheckingUri.contains("viewMarketingCampaign") && UtilValidate.isNotEmpty(marketingCampaignId)) {
    conditionMKTOCL = EntityCondition.makeCondition(UtilMisc.toList(
        EntityCondition.makeCondition("campaignTypeId", EntityOperator.EQUALS, "PHONE_CALL"),
        EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId)), EntityOperator.AND);
    marketingCampaignListOCL = delegator.findList("MarketingCampaign", conditionMKTOCL, null, null, null, false);

    campaignCsrAssoc = EntityQuery.use(delegator).from("CampaignCsrAssoc").where("campaignId", marketingCampaignId).queryList();
    if (campaignCsrAssoc != null && campaignCsrAssoc.size() > 0) {
        mktCampVal = EntityQuery.use(delegator).from("MarketingCampaign").where("marketingCampaignId", marketingCampaignId, "campaignTypeId", "PHONE_CALL")
            .filterByDate("startDate", "endDate").queryOne();
        if (mktCampVal != null && mktCampVal.size() > 0) {
           generatePhoneCallList = true;
        }
    }
} else {
    conditionMKTOCL = EntityCondition.makeCondition(UtilMisc.toList(
        EntityCondition.makeCondition("campaignTypeId", EntityOperator.EQUALS, "PHONE_CALL"),
        EntityCondition.makeConditionDate("startDate", "endDate")), EntityOperator.AND);
    marketingCampaignListOCL = delegator.findList("MarketingCampaign", conditionMKTOCL, null, null, null, false);
}
context.put("generatePhoneCallList", generatePhoneCallList);
context.put("marketingCampaignListOCL", DataHelper.getDropDownOptions(marketingCampaignListOCL, "marketingCampaignId", "campaignName"));

enumerationCallStatusList = delegator.findByAnd("Enumeration", UtilMisc.toMap("enumTypeId", "CALL_STATUS"), UtilMisc.toList("sequenceId"), true);
context.put("callStatusListOCL", DataHelper.getDropDownOptions(enumerationCallStatusList, "enumId", "description"));

enumerationTimeZoneList = delegator.findByAnd("Enumeration", UtilMisc.toMap("enumTypeId", "TIME_ZONE"), UtilMisc.toList("enumId"), true);
context.put("enumerationTimeZoneList", DataHelper.getDropDownOptions(enumerationTimeZoneList, "enumId", "description"));

Map numberCountOCL = new LinkedHashMap();
for (int i = 1; i <= 100; i++) {
    numberCountOCL.put(Integer.toString(i), Integer.toString(i));
}
context.put("numberCountOCL", numberCountOCL);

if (UtilValidate.isNotEmpty(userLoginId)) {
    outboundCallListDetail = delegator.findOne("OutboundCallListDetails", UtilMisc.toMap("userLoginId", userLoginId), false);
    if (outboundCallListDetail != null && outboundCallListDetail.size() > 0) {
        context.put("campaignIdOCL", outboundCallListDetail.getString("campaignId"));
        context.put("rmUserLoginIdOCL", outboundCallListDetail.getString("csrPartyId"));
        context.put("callStatusOCL", outboundCallListDetail.getString("callStatus"));
        context.put("defaultTimeZoneIdOCL", outboundCallListDetail.getString("localTimeZone"));
        context.put("noOfDaysSinceLastCallOCL", outboundCallListDetail.getString("noOfDaysSinceLastCall"));
        context.put("callBackDateOCL", outboundCallListDetail.getString("callBackDate"));
    }
}

userLoginPartyId = userLogin.getString("partyId");
if (UtilValidate.isNotEmpty(userLoginId) && UtilValidate.isNotEmpty(userLoginPartyId)) {
    condition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId),
        EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")),
        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);
    List userLoginSecurityGroup = delegator.findList("UserLoginSecurityGroup", condition, null, null, null, false);
    if (userLoginSecurityGroup != null && userLoginSecurityGroup.size() > 0) {
        context.put("csrValidation", "Y");
    }
}

csrPartyLists = [];
EntityCondition rmCondition = EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_MANAGER");
List < GenericValue > partyToSummartyByRoleRM = delegator.findList("PartyToSummaryByRole", rmCondition, null, null, null, false);
//"PartyRole","CUST_SERVICE_REP"
if (partyToSummartyByRoleRM.size() > 0 && partyToSummartyByRoleRM != null) {
    Map < String, Object > partyToSummartyByRoleMap = new LinkedHashMap < String, Object > ();
    for (GenericValue partyToSummartyByRoleGV: partyToSummartyByRoleRM) {
        partyToSummartyByRoleMap.put(partyToSummartyByRoleGV.getString("partyId"), partyToSummartyByRoleGV.getString("firstName") + " " + partyToSummartyByRoleGV.getString("lastName"));
    }
    context.put("rmMap", partyToSummartyByRoleMap);
}