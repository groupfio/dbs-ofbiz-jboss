import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityExpr;
import javolution.util.FastMap;
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker;

delegator = request.getAttribute("delegator");
contactId = request.getParameter("contactListId");
customerId = request.getParameter("customerId");
firstName  = request.getParameter("firstName");
lastName  = request.getParameter("lastName");

if (UtilValidate.isNotEmpty(contactId))  {
	conditionsList = FastList.newInstance();
	context.put("contactId", contactId);
	EntityCondition contactListId = EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactId);
	conditionsList.add(contactListId);
	
if (UtilValidate.isNotEmpty(customerId)) {
		context.put("customerId", customerId);
		EntityCondition nameCondition = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, customerId);
		conditionsList.add(nameCondition);
}
	
if (UtilValidate.isNotEmpty(firstName)) {
	context.put("firstName", firstName);
	EntityCondition nameCondition = EntityCondition.makeCondition("firstName", EntityOperator.LIKE, "%"+firstName+"%");
	conditionsList.add(nameCondition);
}

if (UtilValidate.isNotEmpty(lastName)) {
    context.put("lastName", lastName);
    EntityCondition lastNameCondition = EntityCondition.makeCondition("lastName", EntityOperator.LIKE, "%"+lastName+"%");
    conditionsList.add(lastNameCondition);
}
List contactListData = [];
EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);
efo.setLimit(1000);
contactListDataCount = delegator.findList("PartyContactInfoAndContactList",EntityCondition.makeCondition(conditionsList,EntityOperator.AND),UtilMisc.toSet("partyId"),UtilMisc.toList("contactListId DESC"),efo,false);
if(contactListDataCount != null && contactListDataCount.size() > 0 ) {
    List < String > partyIdList = EntityUtil.getFieldListFromEntityList(contactListDataCount, "partyId", true);
    if (partyIdList != null && partyIdList.size() > 0) {
        for (String partyId: partyIdList) {
			partySummaryDetailsView = delegator.findOne("PartySummaryDetailsView", ["partyId": partyId], false);
			if (partySummaryDetailsView != null && partySummaryDetailsView.size() > 0) {
				Map partyDetails = new HashMap();
				String phoneNumber = "";
				String infoString = "";
				Map < String, String > partyContactInfo = PartyPrimaryContactMechWorker.getPartyPrimaryContactMechValueMaps(delegator, partyId);
				phoneNumber = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("PrimaryPhone") : "";
				if (UtilValidate.isNotEmpty(phoneNumber)) {
					phoneNumber = phoneNumber.substring(phoneNumber.lastIndexOf('-') + 1);
				}
				infoString = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("EmailAddress") : "";
				String phoneSolicitation = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("phoneSolicitation") : "";
				String emailSolicitation = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("emailSolicitation") : "";
				
				partyDetails.put("partyId", partySummaryDetailsView.getString("partyId"));
				partyDetails.put("firstName", partySummaryDetailsView.getString("firstName"));
				partyDetails.put("lastName", partySummaryDetailsView.getString("lastName"));
				partyDetails.put("groupName", partySummaryDetailsView.getString("groupName"));
				partyDetails.put("contactNumber", phoneNumber);
				partyDetails.put("infoString", infoString);
				partyDetails.put("phoneSolicitation", phoneSolicitation);
				partyDetails.put("emailSolicitation", emailSolicitation);
				contactListData.add(partyDetails);
			}
		}
    }
context.put("contactListMemberData",contactListData);
}
uploadFileStatus = from('ContactList').where('contactListId', contactId).queryOne();

context.put("contactListName", uploadFileStatus.getAt("contactListName"))
}

//Check contact list association with Campaign
boolean isAssociated = false;
associatedContactList = from('MarketingCampaignContactList').where('contactListId', contactId).queryList();
if(UtilValidate.isNotEmpty(associatedContactList))
	{
		isAssociated= true;
		
	}
	context.put("isAssociated",isAssociated);


/*// not include partyClassificationGroupId, else might repeat result

searchConditions = new ArrayList();
searchConditions.add(EntityUtil.getFilterByDateExpr());   // this filters out expired contact mechs using PartyContactMech

	//fieldsToSelect = UtilMisc.toListArray(new String[] {"partyId", "groupName", "firstName", "lastName", "fromDate", "thruDate", "contactMechId", "infoString", "contactMechTypeId"});
	entityName = "PartyContactInfo";
	// if a contactListId is given, filter out parties that are already member of this contact list
	contactListId = request.getParameter("contactListId");
	if (contactListId != null) {
		alreadyMembersConditions = UtilMisc.toList(
												 new EntityExpr("contactListId", EntityOperator.EQUALS, contactListId),
												 EntityUtil.getFilterByDateExpr("memberFromDate", "memberThruDate"));
		alreadyMembers = delegator.findList("PartyContactInfoAndContactList", new EntityConditionList(alreadyMembersConditions, EntityOperator.AND), null, UtilMisc.toList("partyId"),null,false);
		alreadyMembersPartyIds = EntityUtil.getFieldListFromEntityList(alreadyMembers, "partyId", true);
		if (UtilValidate.isNotEmpty(alreadyMembersPartyIds)) {
			searchConditions.add(new EntityExpr("partyId", EntityOperator.NOT_IN, alreadyMembersPartyIds));
		}
	}
searchConditionList = new EntityConditionList(searchConditions, EntityOperator.AND);
// get the contact list members as a list iterator for pagination purposes (these lists can be large)
//members = delegator.findListIteratorByCondition(entityName, searchConditionList,  null, null, null,
//		new EntityFindOptions(true, EntityFindOptions.TYPE_SCROLL_INSENSITIVE, EntityFindOptions.CONCUR_READ_ONLY, true));
members = delegator.findList("PartyContactInfo", searchConditionList,null, null,  null, false);
//members = from(entityName).where(searchConditionList).cursorScrollInsensitive().distinct().queryIterator();	
context.put("contactListParties", members);*/