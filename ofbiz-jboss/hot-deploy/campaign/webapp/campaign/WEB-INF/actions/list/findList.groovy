import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.util.EntityFindOptions;

contactListName = request.getParameter("contactListName");
contactType  = request.getParameter("contactType");
contactListId = request.getParameter("contactListId");
delegator = request.getAttribute("delegator");
conditionsList = FastList.newInstance();
if (UtilValidate.isNotEmpty(contactListId)) {
    context.put("contactListId",contactListId);
    tempIdCondition = EntityCondition.makeCondition("contactListId", EntityOperator.LIKE, contactListId);
    conditionsList.add(tempIdCondition);
}
if (UtilValidate.isNotEmpty(contactListName)) {
    context.put("contactListName",contactListName);
    tempNameCondition = EntityCondition.makeCondition("contactListName", EntityOperator.LIKE, "%"+contactListName+"%");
    conditionsList.add(tempNameCondition);
}
if(UtilValidate.isNotEmpty(contactType)){
    context.put("contactType",contactType);
    tempTypeCondition = EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, contactType);
    conditionsList.add(tempTypeCondition);
}
campListCond = EntityCondition.makeCondition("contactListTypeId", EntityOperator.NOT_EQUAL, "CAMPAIGN_LIST");
conditionsList.add(campListCond);
EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);
efo.setLimit(1000);
contactListData = delegator.findList("ContactList",EntityCondition.makeCondition(conditionsList,EntityOperator.AND),null,UtilMisc.toList("contactListId DESC"),efo,false);	
context.put("contactListData",contactListData);



contactTypeMap = new LinkedHashMap();
contactTypeMap.put("EMAIL_ADDRESS", "Email Address");
contactTypeMap.put("POSTAL_ADDRESS", "Postal Address");
contactTypeMap.put("TELECOM_NUMBER","Phone Number");
context.put("contactTypeList", contactTypeMap);

contactPurposeTypeMap = new LinkedHashMap();
contactPurposeTypeMap.put("TEST", "Test List");
contactPurposeTypeMap.put("LIVE", "Production List");
context.put("contactPurposeTypeList", contactPurposeTypeMap);

flagMap = new LinkedHashMap();
flagMap.put("N", "No");
flagMap.put("Y", "Yes");
context.put("flagMap", flagMap);