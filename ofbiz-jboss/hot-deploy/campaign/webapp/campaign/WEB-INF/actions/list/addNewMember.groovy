import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityExpr;
import javolution.util.FastMap;
// possible fields we're searching by
contactListId = request.getParameter( "contactListId");
groupName = request.getParameter("groupName");
infoString = request.getParameter( "infoString");
contactNumber = request.getParameter( "contactNumber");
roleType = request.getParameter( "roleType");
firstName  = request.getParameter("firstName");
lastName  = request.getParameter("lastName");

if (UtilValidate.isNotEmpty(contactListId))  {
context.put("contactId", contactListId);
contactListGV = from('ContactList').where('contactListId', contactListId).queryOne();
if(contactListGV != null && contactListGV.size() > 0) {
    context.put("contactListName", contactListGV.getString("contactListName"))
}
// search conditions
searchConditions = new ArrayList();
searchConditions.add(EntityUtil.getFilterByDateExpr());   // this filters out expired contact mechs using PartyContactMech
if(UtilValidate.isNotEmpty(contactListId)) {
    GenericValue contactListAttribute = EntityQuery.use(delegator).from("ContactListAttribute")
    .where("contactListId", contactListId, "attrName", "ROLE_TYPE_ID").queryOne();
    if(contactListAttribute != null && contactListAttribute.size() > 0) {
        attrValue = contactListAttribute.getString("attrValue");
        if(UtilValidate.isNotEmpty(attrValue)) {
            roleType = attrValue;
            context.put("contactListRoleType", roleType);
        }
    }
}

// find parameters for the sorting headers
findParams = "";
Map<String, Object> andMap = FastMap.newInstance();
andMap.put("contactListId", contactListId);
andMap.put("firstName", firstName);
andMap.put("lastName", lastName);
andMap.put("groupName", groupName);
andMap.put("infoString", infoString);
andMap.put("roleType", roleType);

Map<String, Object> result = dispatcher.runSync("getContactListParties", andMap);

if(UtilValidate.isNotEmpty(roleType)){
	context.selectedRoleType = roleType;
}

if(UtilValidate.isNotEmpty(result)){
	context.contactListParties = result.contactListParties;
}

}