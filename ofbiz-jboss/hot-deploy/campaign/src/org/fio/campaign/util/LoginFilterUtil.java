package org.fio.campaign.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

public class LoginFilterUtil {
	private static String MODULE = LoginFilterUtil.class.getName();
	
//get list of rm from lower Positioned Party's	
public static List<String> getRmList(Delegator delegator, List<String> lowerPositionPartyIds) {
		
		List<String> rmList = new ArrayList<String>();
		
		if (UtilValidate.isEmpty(lowerPositionPartyIds)) {
			return rmList;
		}
		
		try {
			EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("partyId", EntityOperator.IN, lowerPositionPartyIds),
					EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_MANAGER")
					);
			
			List<GenericValue> resultList = delegator.findList("PartyRole", searchConditions, UtilMisc.toSet("partyId"), null, null, false);
			if (UtilValidate.isNotEmpty(resultList)) {
				rmList = EntityUtil.getFieldListFromEntityList(resultList, "partyId", true);
			}
			
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return rmList;
	}

//Get Campaign List id from lower Positioned Party's
public static List<String> getCampaignList(Delegator delegator, List<String> lowerPositionPartyIds) {
	
	List<String> rmList = new ArrayList<String>();
	List<String> campaignIdList = new ArrayList<String>();
	if (UtilValidate.isEmpty(lowerPositionPartyIds)) {
		return campaignIdList;
	}
	
	try {
		
		rmList = getRmList(delegator, lowerPositionPartyIds);
		
		EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
				EntityCondition.makeCondition("csrPartyId", EntityOperator.IN, rmList)
				);
		List<GenericValue> resultList = delegator.findList("CampaignContactListParty", searchConditions, null, null, null, false);
		if (UtilValidate.isNotEmpty(resultList)) {
			campaignIdList = EntityUtil.getFieldListFromEntityList(resultList, "contactListId", true);
		}
		
	} catch (GenericEntityException e) {
		e.printStackTrace();
	}
	
	return campaignIdList;
}

//get list of Accounts from lower Positioned Party's	
public static List<String> getCampaignsAccountList(Delegator delegator, List<String> lowerPositionPartyIds) {
		
		List<String> accountList = new ArrayList<String>();
		List<String> campaignIdList = new ArrayList<String>();		
		if (UtilValidate.isEmpty(lowerPositionPartyIds)) {
			return accountList;
		}		
		try {
			campaignIdList = getCampaignList(delegator,lowerPositionPartyIds);
			EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("contactListId", EntityOperator.IN, campaignIdList));			
			List<GenericValue> resultList = delegator.findList("CampaignContactListParty", searchConditions, UtilMisc.toSet("acctPartyId"), null, null, false);
			if (UtilValidate.isNotEmpty(resultList)) {
				accountList = EntityUtil.getFieldListFromEntityList(resultList, "acctPartyId", true);
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return accountList;
	}

//Check Full Access Role Type	
public static boolean checkEmployeePosition(Delegator delegator, String userLoginId) {
		
		List<String> roleList = new ArrayList<String>();
		try {
			if (UtilValidate.isNotEmpty(userLoginId)) {
				EntityCondition searchCond = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("systemResourceId", EntityOperator.EQUALS, "campaign_admin_role"));
				List<GenericValue> resultList = delegator.findList("SystemProperty", searchCond, UtilMisc.toSet("systemPropertyValue"), null, null, false);
				if (UtilValidate.isNotEmpty(resultList)) {
					roleList = EntityUtil.getFieldListFromEntityList(resultList, "systemPropertyValue", true);
				}
				
				if (UtilValidate.isNotEmpty(roleList)) {
					EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, userLoginId),
							EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, roleList));
					List<GenericValue> roleResultList = delegator.findList("PartyRole", searchConditions, UtilMisc.toSet("roleTypeId"), null, null, false);
					if (UtilValidate.isNotEmpty(roleResultList)) {
						return false;
					}
				}
				else {
		            Debug.logInfo("Admin Role Property is not configured for the user", MODULE);
		            return true;
				}
			}
			else {
	            Debug.logWarning("Received a null User Login Id from HttpServletRequest", MODULE);
			}
			
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		
		return true;
	}


}
