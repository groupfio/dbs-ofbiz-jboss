package org.fio.campaign.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.ofbiz.base.util.Debug;

import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.model.ModelEntity;

/**
 * @author Mahendran T
 * @since 08-08-2018
 * @desc Construct the file from the list of generic value
 * */
public class ExportFileUtil {
	private final static String module = ExportFileUtil.class.getName();
	public static void constructFileFromGenericValue(Delegator delegator, List<GenericValue> rows, List<String> headers,String filename,String location,String delimiter,boolean isHeaderRequird) {
		try {
			if(rows != null && rows.size() > 0) {
				ModelEntity entity = rows.get(0).getModelEntity();
				List<String> fields = entity.getAllFieldNames();
				String data = "";
				List<String> headerFields = new ArrayList<String>();
				if(isHeaderRequird) {

					if(headers != null && headers.size() > 0) {
						headerFields.addAll(headers);
					} else {
						// get the header data from the generic value
						headerFields.addAll(fields);
					}
					data = fileHeaders(headerFields, delimiter);
				}
				int noOfRows = 0;
				for(GenericValue row : rows) {
					noOfRows = noOfRows+1;
					String columnValues = "";
					for(int i=0 ; i<fields.size() ; i++) {
						String value = UtilValidate.isNotEmpty(row.getString(fields.get(i))) ? row.getString(fields.get(i)) : "";
						if(i == 0) {
							columnValues = columnValues.concat(value);
						} else {
							columnValues = columnValues.concat(delimiter.concat(value));
						}
					}
					data = data.concat(columnValues) + "\n";
				}
				if(!"".equals(data) && noOfRows > 0) {
					String filePath = location+File.separatorChar+filename;
					File file = new File(filePath);
					FileUtils.writeStringToFile(file, data, "UTF-8");
					Debug.logInfo("File Exported with "+ noOfRows +" rows", module);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void constructFileFromList(Delegator delegator, List<Map<String, Object>> rows, List<String> headers,String filename,String location,String delimiter,boolean isHeaderRequird) {
		try {
			if(rows != null && rows.size() > 0) {
				String data = "";
				List<String> headerFields = new ArrayList<String>();
				Set<String> fieldSet = rows.get(0).keySet();
				if(isHeaderRequird) {

					if(headers != null && headers.size() > 0) {
						headerFields.addAll(headers);
					} else {
						// get the header data from the generic value
						fieldSet.forEach(e->{
							headerFields.add(e);
						});
					}
					data = fileHeaders(headerFields, delimiter);
				}
				int noOfRows = 0;
				for(Map<String, Object> row : rows) {
					noOfRows = noOfRows+1;
					String columnValues = "";
					int i=0;
					for(String key : fieldSet) {
						String value = UtilValidate.isNotEmpty((String) row.get(key)) ? (String) row.get(key) : "";
						if(i == 0) {
							columnValues = columnValues.concat(value);
						} else {
							columnValues = columnValues.concat(delimiter.concat(value));
						}
						i++;
					}
					data = data.concat(columnValues) + "\n";
				}
				if(!"".equals(data) && noOfRows > 0) {
					String filePath = location+File.separatorChar+filename;
					File file = new File(filePath);
					FileUtils.writeStringToFile(file, data, "UTF-8");
					Debug.logInfo("File Exported with "+ noOfRows +" rows", module);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String fileHeaders(List<String> headers,String delimiter) {
		String headerString = "";
		if(headers != null && headers.size() > 0) {
			try {
				//for(String header : headers) {
				for(int i =0;i<headers.size();i++) {
					String header = UtilValidate.isNotEmpty((String)headers.get(i)) ? headers.get(i) : "";
					if(i == 0) {
						headerString = header;
					} else {
						headerString = headerString.concat(delimiter.concat(header));
					}
				}
				headerString = headerString.concat("\n");
			} catch (Exception e) {
				Debug.logError("Error : "+e.getMessage(), module);
			}
		}
		return headerString;
	}

	public static String downloadFile(HttpServletRequest request,HttpServletResponse response,String filePath){
		FileInputStream fis = null;
		try{
			if(UtilValidate.isNotEmpty(filePath)) {
				File file = new File(filePath);
				String fileName = file.getName();
				if(UtilValidate.isEmpty(file) || !file.exists()){
					return "error";
				}

				fis = new FileInputStream(file);  
				//System.out.println("file inputtttt "+fis);
				byte b[];  
				int x = fis.available();  
				b = new byte[x];   
				fis.read(b);  

				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", "attachment; filename='"+fileName+"'");
				OutputStream os = response.getOutputStream();  
				os.write(b);  
				os.flush();
				os.close();
				fis.close();
			}

		}catch(Exception e){
			e.printStackTrace();
			Debug.logInfo("Eception is ::"+e.getMessage(), "");	
			return "error";
		}
		return "success";
	}


}
