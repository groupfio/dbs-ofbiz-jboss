/**
 * 
 */
package org.fio.campaign.util;

import java.util.Map;

import org.fio.campaign.constants.CampConstants.GroupType;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Sharif
 *
 */
public class DataUtil {
	
	private static String MODULE = DataUtil.class.getName();
	
	public static String getFormatedValue (String value) {
		
		if (UtilValidate.isNotEmpty(value)) {
			value = value.toLowerCase().trim();
			value = value.substring(0, 1).toUpperCase() + value.substring(1);
			
			Debug.log(value);
		}
		return value;
	}
	
	public static String getSegmentationValueAssociatedEntityName (Delegator delegator, String groupId) {
		
		String entityName = "CustomFieldPartyClassification";
		
		try {
			if (UtilValidate.isNotEmpty(groupId)) {
				
				GenericValue customFieldGroup = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup", UtilMisc.toMap("groupId", groupId), null, false) );
				if (UtilValidate.isNotEmpty(customFieldGroup) && (UtilValidate.isNotEmpty(customFieldGroup.getString("isUseDynamicEntity")) && customFieldGroup.getString("isUseDynamicEntity").equals("Y"))) {
					groupId = DataUtil.getFormatedValue(groupId);
					entityName = "CustomFieldSeg" + groupId;
				}
				
				if (UtilValidate.isNotEmpty(customFieldGroup) && customFieldGroup.getString("groupType").equals(GroupType.ECONOMIC_METRIC)) {
					entityName = "PartyMetricIndicator";
				}				
			}
		} catch (Exception e) {
			Debug.logError("Error in getSegmentationValueAssociatedEntityName=="+e.getMessage(), MODULE);
		}
		
		return entityName;
	}
	
    public static String getPartyRelAssocId(Delegator delegator, Map<String, Object> context) {
		
		String partyIdFrom = (String) context.get("partyIdFrom");
		String partyIdTo = (String) context.get("partyIdTo");
		String roleTypeIdFrom = (String) context.get("roleTypeIdFrom");
		String roleTypeIdTo = (String) context.get("roleTypeIdTo");
		String partyRelationshipTypeId = (String) context.get("partyRelationshipTypeId");
		
		try {
			if (UtilValidate.isNotEmpty(partyIdFrom) && UtilValidate.isNotEmpty(partyIdTo) && UtilValidate.isNotEmpty(roleTypeIdFrom) && UtilValidate.isNotEmpty(roleTypeIdTo) && UtilValidate.isNotEmpty(partyRelationshipTypeId)) {
				EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyIdFrom),
						EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyIdTo),
						EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeIdFrom),
						EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, roleTypeIdTo),
						EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, partyRelationshipTypeId),
						EntityUtil.getFilterByDateExpr());
				
				GenericValue existingRelationship = EntityUtil.getFirst( delegator.findList("PartyRelationship", searchConditions,null, null, null, false) );
				if (UtilValidate.isNotEmpty(existingRelationship)) {
					return existingRelationship.getString("partyRelAssocId");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
