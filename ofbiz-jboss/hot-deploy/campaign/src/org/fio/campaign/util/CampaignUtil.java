/**
 * 
 */
package org.fio.campaign.util;

import java.util.List;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;

/**
 * @author Sharif
 *
 */
public class CampaignUtil {

	public static long getCampaignMemberCount(Delegator delegator, String marketingCampaignId) {

		long campCountMember = 0;
		try {
			EntityConditionList conditionsCampList = new EntityConditionList(
					UtilMisc.toList(new EntityExpr("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId)),
					EntityOperator.AND);
			List<GenericValue> campListCountListId = delegator.findList("ContactList", conditionsCampList, null, null,
					null, false);
			
			if (campListCountListId.size() > 0) {
				String campListId = campListCountListId.get(0).getString("contactListId");
				if (campListId != null) {
					EntityConditionList conditionsContList = new EntityConditionList(
							UtilMisc.toList(new EntityExpr("contactListId", EntityOperator.EQUALS, campListId),
									new EntityExpr("contactPurposeTypeId", EntityOperator.EQUALS, "LIVE")),
							EntityOperator.AND);
					campCountMember = delegator.findCountByCondition("CampaignContactListParty",conditionsContList, null,null);

				}
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
		return campCountMember;
	}

}
