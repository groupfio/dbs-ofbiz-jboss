package org.fio.campaign.util;

import java.util.List;

import org.ofbiz.base.util.Debug;

public class StringUtil {
	private static final String module = StringUtil.class.getName();
	public static String fileHeaders(List<String> headers,String delimiter) {
		String headerString = "";
		if(headers != null && headers.size() > 0) {
			try {
				//for(String header : headers) {
				for(int i =0;i<headers.size();i++) {
					String header = StringUtil.isNotEmpty((String)headers.get(i)) ? headers.get(i) : "";
					if(i == 0) {
						headerString = header;
					} else {
						headerString = headerString.concat(delimiter.concat(header));
					}
				}
				headerString = headerString.concat("\n");
			} catch (Exception e) {
				Debug.logError("Error : "+e.getMessage(), module);
			}
		}
		return headerString;
	}
	
	public static boolean isNotEmpty(String s) {
        return ((s != null) && (s.length() > 0));
    }
	
	public static boolean isEmpty(String s) {
        return ((s == null) || (s.length() == 0));
    }
	
	public static String toList(List<String> list,String prefix){
		String str ="";
		
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				if(i == 0){
					if(isNotEmpty(prefix)){
						str= str+"'"+prefix+list.get(i)+"'";
					}else{
						str= str+"'"+list.get(i)+"'";
					}
				}
				else{
					if(isNotEmpty(prefix)){
						str = str+",'"+prefix+list.get(i)+"'";
					}else{
						str = str+",'"+list.get(i)+"'";
					}
				}
			}
		}
		
		return str;
	}
}
