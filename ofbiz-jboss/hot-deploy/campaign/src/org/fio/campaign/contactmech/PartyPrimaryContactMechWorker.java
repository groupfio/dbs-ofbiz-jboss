package org.fio.campaign.contactmech;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

public class PartyPrimaryContactMechWorker {
    private static final String module = PartyPrimaryContactMechWorker.class.getName();
    public static Map < String, String > getPartyPrimaryContactMechValueMaps(Delegator delegator, String partyId) {
        Map < String, String > PartyPrimayContactMechValues = new HashMap < String, String > ();

        List < String > partyContactMechIds = new ArrayList < String > ();
        try {
            List < GenericValue > partyContactMechs = delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId", partyId), null, true);
            partyContactMechs = EntityUtil.filterByDate(partyContactMechs);

            for (GenericValue partyContactMech: partyContactMechs) {
                if (UtilValidate.isNotEmpty(partyContactMech.getString("contactMechId"))) {
                    partyContactMechIds.add(partyContactMech.getString("contactMechId"));
                }
            }

            Set < String > findOptions = UtilMisc.toSet("contactMechId");
            List < String > orderBy = UtilMisc.toList("createdStamp DESC");

            EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
            EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, partyContactMechIds);


            EntityCondition primaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE")));
            List < GenericValue > primaryPhones = delegator.findList("PartyContactMechPurpose", primaryPhoneConditions, findOptions, orderBy, null, true);
            if (UtilValidate.isNotEmpty(primaryPhones)) {
                GenericValue primaryPhone = EntityUtil.getFirst(primaryPhones);
                GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", primaryPhone.getString("contactMechId")), false);
                String phoneNumber = "";
                if (UtilValidate.isNotEmpty(primaryPhoneNumber)) {
                    String countryCode = primaryPhoneNumber.getString("countryCode");
                    String areaCode = primaryPhoneNumber.getString("areaCode");
                    String contactNumber = primaryPhoneNumber.getString("contactNumber");
                    if (countryCode != null && areaCode != null && contactNumber != null) {
                        phoneNumber += countryCode + "-" + areaCode + "-" + contactNumber;
                    } else if (areaCode != null && contactNumber != null) {
                        phoneNumber += areaCode + "-" + contactNumber;
                    } else if (contactNumber != null) {
                        phoneNumber = contactNumber;
                    }
                }
                PartyPrimayContactMechValues.put("primaryPhoneContactMechId", primaryPhone.getString("contactMechId"));
                PartyPrimayContactMechValues.put("PrimaryPhone", phoneNumber);
            }

            EntityCondition mobilePhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PHONE_MOBILE")));
            List < GenericValue > mobilePhones = delegator.findList("PartyContactMechPurpose", mobilePhoneConditions, findOptions, orderBy, null, true);
            if (UtilValidate.isNotEmpty(mobilePhones)) {
                GenericValue mobilePhone = EntityUtil.getFirst(mobilePhones);
                GenericValue mobilePhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", mobilePhone.getString("contactMechId")), false);
                String phoneNumber = "";
                if (UtilValidate.isNotEmpty(mobilePhoneNumber)) {
                    String countryCode = mobilePhoneNumber.getString("countryCode");
                    String areaCode = mobilePhoneNumber.getString("areaCode");
                    String contactNumber = mobilePhoneNumber.getString("contactNumber");
                    if (countryCode != null && areaCode != null && contactNumber != null) {
                        phoneNumber += countryCode + "-" + areaCode + "-" + contactNumber;
                    } else if (areaCode != null && contactNumber != null) {
                        phoneNumber += areaCode + "-" + contactNumber;
                    }
                }

                PartyPrimayContactMechValues.put("MobilePhone", phoneNumber);
            }

            EntityCondition primaryEmailaddressConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL")));
            List < GenericValue > EmailAddresses = delegator.findList("PartyContactMechPurpose", primaryEmailaddressConditions, findOptions, orderBy, null, true);
            if (UtilValidate.isNotEmpty(EmailAddresses)) {
                GenericValue EmailAddress = EntityUtil.getFirst(EmailAddresses);
                GenericValue EmailAddressValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", EmailAddress.getString("contactMechId")), false);
                String EmailAddressId = "";
                if (UtilValidate.isNotEmpty(EmailAddressValue)) {
                    if (UtilValidate.isNotEmpty(EmailAddressValue.getString("infoString")))
                        EmailAddressId = EmailAddressValue.getString("infoString");
                }
                PartyPrimayContactMechValues.put("emailAddressContactMechId", EmailAddress.getString("contactMechId"));
                PartyPrimayContactMechValues.put("EmailAddress", EmailAddressId);
            }
            EntityCondition skypeConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_SKYPE")));
            List < GenericValue > skypes = delegator.findList("PartyContactMechPurpose", skypeConditions, findOptions, orderBy, null, true);
            if (UtilValidate.isNotEmpty(skypes)) {
                GenericValue skype = EntityUtil.getFirst(skypes);
                //GenericValue skypeValue = delegator.findByPrimaryKey("ContactMech",UtilMisc.toMap("contactMechId",skype.getString("contactMechId")));
                GenericValue skypeValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", skype.getString("contactMechId")), false);
                String skypeId = "";
                if (UtilValidate.isNotEmpty(skypeValue)) {
                    if (UtilValidate.isNotEmpty(skypeValue.getString("infoString")))
                        skypeId = skypeValue.getString("infoString");
                }
                PartyPrimayContactMechValues.put("SkypeId", skypeId);
            }

            EntityCondition webUrlConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_WEB_URL")));
            List < GenericValue > webUrls = delegator.findList("PartyContactMechPurpose", webUrlConditions, findOptions, orderBy, null, true);
            if (UtilValidate.isNotEmpty(webUrls)) {
                GenericValue webUrl = EntityUtil.getFirst(webUrls);
                //GenericValue webUrlValue = delegator.findByPrimaryKey("ContactMech",UtilMisc.toMap("contactMechId",webUrl.getString("contactMechId")));
                GenericValue webUrlValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", webUrl.getString("contactMechId")), false);
                String webURL = "";
                if (UtilValidate.isNotEmpty(webUrlValue)) {
                    if (UtilValidate.isNotEmpty(webUrlValue.getString("infoString")))
                        webURL = webUrlValue.getString("infoString");
                }
                PartyPrimayContactMechValues.put("webURL", webURL);
            }


        } catch (GenericEntityException ge) {
            Debug.logInfo("Error: " + ge.getMessage(), module);
        }

        return PartyPrimayContactMechValues;
    }

    public static GenericValue getPartyPrimaryPostal(Delegator delegator, String partyId) {
        List < String > partyContactMechIds = new ArrayList < String > ();
        GenericValue postalAddress = null;
        try {
            List < GenericValue > partyContactMechs = delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId", partyId), null, true);
            partyContactMechs = EntityUtil.filterByDate(partyContactMechs);

            for (GenericValue partyContactMech: partyContactMechs) {
                if (UtilValidate.isNotEmpty(partyContactMech.getString("contactMechId"))) {
                    partyContactMechIds.add(partyContactMech.getString("contactMechId"));
                }
            }

            Set < String > findOptions = UtilMisc.toSet("contactMechId");
            List < String > orderBy = UtilMisc.toList("createdStamp DESC");

            EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
            EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, partyContactMechIds);

            EntityCondition postalAddressConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_LOCATION")));
            List < GenericValue > primaryAddressList = delegator.findList("PartyContactMechPurpose", postalAddressConditions, findOptions, orderBy, null, false);
            if (primaryAddressList != null && primaryAddressList.size() > 0) {
                GenericValue primaryAddress = EntityUtil.getFirst(EntityUtil.filterByDate(primaryAddressList));
                if (UtilValidate.isNotEmpty(primaryAddress)) {
                    postalAddress = delegator.findOne("PostalAddress", UtilMisc.toMap("contactMechId", primaryAddress.getString("contactMechId")), false);
                }
            }
        } catch (GenericEntityException e) {
            Debug.logInfo("Error: " + e.getMessage(), module);
        }
        return postalAddress;
    }


}