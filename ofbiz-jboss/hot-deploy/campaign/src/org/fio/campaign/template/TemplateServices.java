package org.fio.campaign.template;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.parser.JSONParser;
import org.ofbiz.base.util.Base64;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * @author Mahendran T
 * @since 19-06-2018
 * */
public class TemplateServices {
	public static final String resource = "campaignUiLabels";
	public static final String module = TemplateServices.class.getName();
	public static Map<String, Object> createEmailTemplate(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Map<String, Object> result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource,"templateCreatedSuccessfully", locale));
		
		Boolean privateForm = "Y".equals(context.get("privateForm"));
		String templateCategoryId = (String) context.get("templateCategoryId");
		String fromDate = (String) context.get("fromDate");
		String thruDate = (String) context.get("thruDate");
		String templateFormContent = (String) context.get("templateFormContent");
		
		Timestamp fromTimestamp = null;
        Timestamp thruTimestamp = null;
		GenericValue templateMaster = null;
		try {
			if(UtilValidate.isNotEmpty(fromDate)) {
        		Date date = df.parse(fromDate);
        		fromTimestamp = new Timestamp(date.getTime());
        		context.put("fromDate", fromTimestamp);
        	}
        	if(UtilValidate.isNotEmpty(thruDate)) {
        		Date date1 = df.parse(thruDate);
        		thruTimestamp = new Timestamp(date1.getTime());
        		context.put("thruDate", thruTimestamp);
        	}
        	if(UtilValidate.isNotEmpty(templateFormContent)) {
        		context.put("templateFormContent", Base64.base64Encode(templateFormContent));
        	}
			String templateId = delegator.getNextSeqId("TemplateMaster");
			templateMaster = delegator.makeValue("TemplateMaster");
	        templateMaster.set("templateId", templateId);
	        templateMaster.setNonPKFields(context);
	        if (!privateForm) {
	        	templateMaster.set("partyId", null);
	        }
	        // persist the template content
	        delegator.createOrStore(templateMaster);
	        if(UtilValidate.isNotEmpty(templateCategoryId)) {
	        	GenericValue templateToCategory = delegator.makeValue("TemplateToCategory");
	        	templateToCategory.set("templateId", templateId);
	        	templateToCategory.set("templateCategoryId", templateCategoryId);
	        	// Associate template with template category
	        	delegator.createOrStore(templateToCategory);
	        }
	        result.put("templateId", templateId);
		} catch (Exception e) {
			result = ServiceUtil.returnError(e.getMessage());
		}
		return result;
		
	}
	
	
	public static Map<String, Object> updateEmailTemplate(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Map<String, Object> result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource,"templateUpdatedSuccessfully", locale));
		
		Boolean privateForm = "Y".equals(context.get("privateForm"));
		String fromDate = (String) context.get("fromDate");
		String thruDate = (String) context.get("thruDate");
		String templateId = (String) context.get("templateId");
		Timestamp fromTimestamp = null;
        Timestamp thruTimestamp = null;
		try {
			if(UtilValidate.isNotEmpty(fromDate)) {
        		Date date = df.parse(fromDate);
        		fromTimestamp = new Timestamp(date.getTime());
        		context.put("fromDate", fromTimestamp);
        	}
        	if(UtilValidate.isNotEmpty(thruDate)) {
        		Date date1 = df.parse(thruDate);
        		thruTimestamp = new Timestamp(date1.getTime());
        		context.put("thruDate", thruTimestamp);
        	}
			GenericValue templateMaster = delegator.findOne("TemplateMaster", UtilMisc.toMap("templateId",templateId), false);
			if(templateMaster != null && templateMaster.size() > 0) {
				templateMaster.setNonPKFields(context);
				templateMaster.store();
				
				String templateCategoryId = (String) context.get("templateCategoryId");
				if(UtilValidate.isNotEmpty(templateCategoryId)) {
					GenericValue templateToCategory = delegator.findOne("TemplateToCategory", UtilMisc.toMap("templateId",templateId,"templateCategoryId",templateCategoryId), false);
					if(templateToCategory == null || templateToCategory.size() == 0) {
						//remove the 
						delegator.removeByAnd("TemplateToCategory", UtilMisc.toMap("templateId",templateId));
						templateToCategory = delegator.makeValue("TemplateToCategory");
						templateToCategory.set("templateId", templateId);
						templateToCategory.set("templateCategoryId", templateCategoryId);
						// Associate template with template category
						delegator.createOrStore(templateToCategory);
					}
				}
			}
	        result.put("templateId", templateId);
		} catch (Exception e) {
			result = ServiceUtil.returnError(e.getMessage());
		}
		return result;
		
	}
	
	public static Map<String, Object> addTagsToTemplate(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		Map<String, Object> result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource,"templateTagConfigSuccessfully", locale));
		
		String templateId = (String) context.get("templateId");
		String templateTagJson = (String) context.get("templateTagJson");
		String tagMandatory = (String) context.get("tagMandatory");
		String templateTagTypeId = (String) context.get("templateTagTypeId");
		try {
			// get the configured tag
			List<String> templateTags = FastList.newInstance();
			List<String> orderBy = FastList.newInstance();
			orderBy.add("groupSequenceNo");
			orderBy.add("fieldSequenceNo");
			List<EntityCondition> conditions = FastList.newInstance();
			if(UtilValidate.isNotEmpty(templateTagTypeId)) {
				conditions.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,templateTagTypeId));
			}
			conditions.add(EntityCondition.makeCondition("templateId",EntityOperator.EQUALS,templateId));
			List<GenericValue> dataTagConfiguration = delegator.findList("DataTagAndTemplateTagConfiguration", EntityCondition.makeCondition(conditions, EntityOperator.AND), null, orderBy, null, false);
			if(dataTagConfiguration != null && dataTagConfiguration.size() > 0) {
				templateTags = EntityUtil.getFieldListFromEntityList(dataTagConfiguration, "tagId", true);
			}
			
			templateTagJson = templateTagJson.replace("quot;", "\"");
			JSONArray jsonArray = JSONArray.fromObject(templateTagJson);
			
			if(jsonArray!=null && jsonArray.size() > 0) {
				List<String> tagIds = FastList.newInstance();
				for (int i = 0; i < jsonArray.size() ;i++) {
					JSONObject obj = jsonArray.getJSONObject(i);
					String tagJson = (String) obj.get("id");
					String tagId = tagJson.substring(tagJson.lastIndexOf("[")+1, tagJson.lastIndexOf("]"));
					tagIds.add(tagId);
					String tagName = tagJson.substring(0,tagJson.lastIndexOf("[")-1);
					
					Debug.logInfo("obj-->"+obj, module);
				}
				
				if(tagIds !=null && tagIds.size() > 0  && UtilValidate.isNotEmpty(templateId)) {
					// remove the tags before we add to the template
					templateTags.removeAll(tagIds);
					
					EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("templateId",EntityOperator.EQUALS,templateId),
							EntityCondition.makeCondition("tagId",EntityOperator.IN,templateTags)
							);
					delegator.removeByCondition("TemplateTag", condition);
					
					
					// add tags to template
					for(int i=0;i<tagIds.size();i++) {
						String templateTagId = tagIds.get(i);
						GenericValue templateTag = delegator.findOne("TemplateTag", UtilMisc.toMap("templateId",templateId,"tagId",templateTagId), false);
						if(templateTag != null && templateTag.size() > 0) {
							templateTag.set("tagMandatory", tagMandatory);
							delegator.store(templateTag);
						}else {
							templateTag = delegator.makeValue("TemplateTag",UtilMisc.toMap("templateId",templateId,"tagId",templateTagId));
							templateTag.set("tagSequence", i+"");
							templateTag.set("tagMandatory", tagMandatory);
							delegator.createOrStore(templateTag);
						}
					}
				}
			} else {
				// remove all the tags for the template if no tags in the right side
				if(UtilValidate.isEmpty(templateTagTypeId)) {
					EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("templateId",EntityOperator.EQUALS,templateId)
							);
					delegator.removeByCondition("TemplateTag", condition);
				} else {
					
					EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("templateId",EntityOperator.EQUALS,templateId),
							EntityCondition.makeCondition("tagId",EntityOperator.IN,templateTags)
							);
					delegator.removeByCondition("TemplateTag", condition);
				}
				
			}
	        result.put("templateId", templateId);
	        result.put("templateTagTypeId", templateTagTypeId);
		} catch (Exception e) {
			result = ServiceUtil.returnError(e.getMessage());
			Debug.logError("Error : "+e.getMessage(), module);
		}
		return result;
		
	}
	
	
	public static Map<String, Object> createSMSTemplate(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Map<String, Object> result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource,"templateCreatedSuccessfully", locale));
		
		String templateCategoryId = (String) context.get("templateCategoryId");
		
		GenericValue templateMaster = null;
		try {
		
			String templateId = delegator.getNextSeqId("TemplateMaster");
			templateMaster = delegator.makeValue("TemplateMaster");
	        templateMaster.set("templateId", templateId);
	        templateMaster.setNonPKFields(context);
	        
	        // persist the template content
	        delegator.createOrStore(templateMaster);
	        if(UtilValidate.isNotEmpty(templateCategoryId)) {
	        	GenericValue templateToCategory = delegator.makeValue("TemplateToCategory");
	        	templateToCategory.set("templateId", templateId);
	        	templateToCategory.set("templateCategoryId", templateCategoryId);
	        	// Associate template with template category
	        	delegator.createOrStore(templateToCategory);
	        }
	        result.put("templateId", templateId);
		} catch (Exception e) {
			result = ServiceUtil.returnError(e.getMessage());
		}
		return result;
		
	}
	
	
	public static Map<String, Object> updateSMSTemplate(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Map<String, Object> result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource,"templateUpdatedSuccessfully", locale));
		
		String templateId = (String) context.get("templateId");
		
		try {
			GenericValue templateMaster = delegator.findOne("TemplateMaster", UtilMisc.toMap("templateId",templateId), false);
			if(templateMaster != null && templateMaster.size() > 0) {
				templateMaster.setNonPKFields(context);
				templateMaster.store();
				
				String templateCategoryId = (String) context.get("templateCategoryId");
				if(UtilValidate.isNotEmpty(templateCategoryId)) {
					GenericValue templateToCategory = delegator.findOne("TemplateToCategory", UtilMisc.toMap("templateId",templateId,"templateCategoryId",templateCategoryId), false);
					if(templateToCategory == null || templateToCategory.size() == 0) {
						//remove the 
						delegator.removeByAnd("TemplateToCategory", UtilMisc.toMap("templateId",templateId));
						templateToCategory = delegator.makeValue("TemplateToCategory");
						templateToCategory.set("templateId", templateId);
						templateToCategory.set("templateCategoryId", templateCategoryId);
						// Associate template with template category
						delegator.createOrStore(templateToCategory);
					}
				}
			}
	        result.put("templateId", templateId);
		} catch (Exception e) {
			result = ServiceUtil.returnError(e.getMessage());
		}
		return result;
		
	}
}
