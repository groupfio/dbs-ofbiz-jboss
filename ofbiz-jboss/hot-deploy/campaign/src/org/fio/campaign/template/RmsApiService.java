package org.fio.campaign.template;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.ServiceUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Writer;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javolution.util.FastList;


public class RmsApiService {
	public static final String module = RmsApiService.class.getName();
	public static final String resource = "RmsErrorUiLabels";
	/**
	 * Create Merge Form
	 * 
	 * @param dctx
	 * @param context
	 * @return
	 */
	public static Map < String, Object > createMergeForm(DispatchContext dctx, Map < String, Object > context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		Boolean privateForm = "Y".equals(context.get("privateForm"));
		String text = (String) context.get("text");
		String templateType = (String) context.get("templateType");
		String mergeFormName = (String) context.get("mergeFormName");
		String description = (String) context.get("description");
		String templateContent = (String) context.get("mergeFormText");
		String subject = (String) context.get("subject");
		String senderEmail = (String) context.get("senderEmail");
		String senderName = (String) context.get("senderName");
		String replayToName = (String) context.get("replayToName");
		/*String senderEmailReplay = (String) context.get("senderEmailReplay");
        String emailCharSet = (String) context.get("emailCharSet");
        String emailLineFeed = (String) context.get("emailLineFeed");
        String mailFormat = (String) context.get("mailFormat");
        String emailOnePixel = (String) context.get("emailOnePixel");*/

		Map result = null;
		GenericValue mergeForm = null;
		mergeForm = delegator.makeValue("MergeForm");
		String mergeFormId = delegator.getNextSeqId("MergeForm");
		mergeForm.set("mergeFormId", mergeFormId);
		mergeForm.setNonPKFields(context);
		if (!privateForm) {
			mergeForm.set("partyId", null);
		}
		try {
			if(text == null && senderEmail == null && subject == null && description ==null && mergeFormName==null){
				return ServiceUtil.returnError("Could not create the Template.");
			}
			delegator.create(mergeForm);
			GenericValue mergeFormCategory = delegator.makeValue("MergeFormToCategory");
			mergeFormCategory.put("mergeFormCategoryId", "Application");
			mergeFormCategory.put("mergeFormId", mergeFormId);
			mergeFormCategory.create();
			result = ServiceUtil.returnSuccess("Template created successfully");
			result.put("mergeFormId", mergeFormId);
		} catch (GenericEntityException e) {
			Debug.logError(e.getMessage(), module);
			return ServiceUtil.returnError(e.getMessage());
		}
		return result;
	}
	/**
	 * Create Merge Form
	 * 
	 * @param dctx
	 * @param context
	 * @return
	 */
	public static Map < String, Object > createMergeFormSMS(DispatchContext dctx, Map < String, Object > context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		String text = (String) context.get("text");
		String mergeFormName = (String) context.get("mergeFormName");
		String templateContent = (String) context.get("mergeFormText");
		String subject = (String) context.get("subject");
		String templateType = (String) context.get("templateType");

		Map result = null;
		try {
			if (text != null) {
				if (text.length() > 160) {
					return ServiceUtil.returnError("Text content should not be > 160 ");
				}
			}
			// checking the company setup start
			String activeStatus = null;

			GenericValue mergeForm = null;
			String mergeFormId = delegator.getNextSeqId("MergeForm");
			Map < String, Object > newMergeFormMap = UtilMisc. < String, Object > toMap("mergeFormId", mergeFormId);
			mergeForm = delegator.makeValue("MergeForm", newMergeFormMap);
			if (templateType != null && templateType.contains("?")) {
				context.remove("templateType");
				templateType = templateType.substring(0, templateType.indexOf("?"));
				context.put("templateType", templateType);
			}
			mergeForm.setNonPKFields(context);
			Debug.log("==templateType===="+templateType);
			try {
				delegator.create(mergeForm);
				GenericValue mergeFormCategory = delegator.makeValue("MergeFormToCategory");
				mergeFormCategory.put("mergeFormCategoryId", "Application");
				mergeFormCategory.put("mergeFormId", mergeFormId);
				mergeFormCategory.create();
				result = ServiceUtil.returnSuccess();
				result.put("mergeFormId", mergeFormId);
			} catch (GenericEntityException e) {
				Debug.logError(e.getMessage(), module);
				return ServiceUtil.returnError(e.getMessage());
			}
		} catch (Exception e) {
		}
		return result;
	}
	/**
	 * Delete Merge Form
	 *
	 * @param dctx
	 * @param context
	 * @return
	 */
	public static Map < String, Object > deleteMergeForm(DispatchContext dctx, Map < String, Object > context) {
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		String mergeFormId = (String) context.get("mergeFormId");

		try {
			if(mergeFormId != null){
				// first we remove all association MergeFormToCategory
				delegator.removeByAnd("MergeFormToCategory",UtilMisc.toMap("mergeFormId", mergeFormId));
				// then remove the form
				delegator.removeByAnd("MergeForm",UtilMisc.toMap("mergeFormId", mergeFormId));
			}
		} catch (GenericEntityException e) {
			Debug.logError(e.getMessage(), module);
			return ServiceUtil.returnError(e.getMessage());
		}
		return ServiceUtil.returnSuccess("Template Removed Successfully");
	}
	/**
	 * Create Contact List
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws GenericEntityException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public static String rmsMailingListRequest(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException, ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Map paramMap = UtilHttp.getParameterMap(request);
		// checking the company setup end
		String shortName = request.getParameter("contactListName");
		String description = request.getParameter("description");
		String contactMechTypeId = request.getParameter("contactMechTypeId");
		String contactPurposeTypeId = request.getParameter("contactPurposeTypeId");
		String isValidate = request.getParameter("isValidate");
		String emailType = request.getParameter("emailType");
		String contactListId = "";
		contactListId = delegator.getNextSeqId("ContactList");
		if (!shortName.equals("") && !description.equals("")) {
			GenericValue contactList = delegator.makeValue("ContactList");
			contactList.put("contactListId", contactListId);
			contactList.put("contactListTypeId", "GENERALLIST");
			contactList.put("contactMechTypeId", contactMechTypeId);
			contactList.put("contactPurposeTypeId", contactPurposeTypeId);
			contactList.put("contactListName", shortName);
			contactList.put("description", description);
			contactList.put("emailType", emailType);
			contactList.create();
			if(UtilValidate.isNotEmpty(isValidate)){
				GenericValue contactListAttr = delegator.makeValue("ContactListAttribute");
				contactListAttr.put("contactListId", contactList.getString("contactListId"));
				contactListAttr.put("attrName", "IS_VALIDATE");
				contactListAttr.put("attrValue", isValidate );
				contactListAttr.create();
			}
			request.setAttribute("_EVENT_MESSAGE_", "Contact List Created Successfully");
			request.setAttribute("contactListId", contactListId);
		} else {
			request.setAttribute("_ERROR_MESSAGE_", "Contact List Name or Description Empty");
			return "error";
		}
		return "success";
	}
	public static String updateList(HttpServletRequest request,HttpServletResponse response) throws GenericEntityException,ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Map paramMap = UtilHttp.getParameterMap(request);	
		String paramContactListId = request.getParameter("contactListId");
		String shortName = request.getParameter("contactListName");
		String description = request.getParameter("description");
		String contactMechTypeId = request.getParameter("contactMechTypeId");
		String contacPurposeTypeId = request.getParameter("contactPurposeTypeId");
		String emailType = request.getParameter("emailType");
		
		if(! contactMechTypeId.equals("TELECOM_NUMBER")){
			try {
				if (!shortName.equals("") && !description.equals("")) {
					String mailingListID = paramContactListId;
					GenericValue findGv = delegator.findOne("ContactList",UtilMisc.toMap("contactListId", mailingListID), false);
					if (UtilValidate.isNotEmpty(findGv)) {
						findGv.put("contactListName", shortName);
						findGv.put("description", description);
						findGv.put("contactMechTypeId",contactMechTypeId);
						findGv.put("contactPurposeTypeId",contacPurposeTypeId);
						if(UtilValidate.isNotEmpty(emailType))
							findGv.put("emailType", emailType);
						findGv.store();
						request.setAttribute("_EVENT_MESSAGE_","Contact List Updated Successfully");
					}
				} else {
					request.setAttribute("_ERROR_MESSAGE_","Contact List Not updated");
					return "error";
				}


			} catch (Exception e) {
				System.out.println("Exception" + e);
			}
		}else {
			try {
				if (!shortName.equals("") && !description.equals("")) {
					String mailingListID = paramContactListId;

					GenericValue findGv = delegator.findOne("ContactList",UtilMisc.toMap("contactListId", mailingListID), false);
					if (UtilValidate.isNotEmpty(findGv)) {
						findGv.put("contactListName", shortName);
						findGv.put("description", description);
						findGv.put("contactMechTypeId", contactMechTypeId);
						findGv.put("contactPurposeTypeId", contacPurposeTypeId);
						findGv.store();
						request.setAttribute("_EVENT_MESSAGE_","Mailing List  updated");
					} else {
						request.setAttribute("_ERROR_MESSAGE_","Mailing List Not updated");
						return "error";
					}
				}
			} catch (Exception e) {
				System.out.println("Exception" + e);
			}
		}
		// request.setAttribute("contactListId", contactListId);
		return "success";
	}
	/**
	 * Update Merge Form
	 * 
	 * @param dctx
	 * @param context
	 * @return
	 */
	public static Map < String, Object > updateMergeFormSMS(DispatchContext dctx, Map < String, Object > context) {
		Map < String, Object > result = ServiceUtil.returnSuccess();
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		String text = (String) context.get("text");
		String mergeFormName = (String) context.get("mergeFormName");
		String mergeFormId = (String) context.get("mergeFormId");
		String templateContent = (String) context.get("mergeFormText");
		String subject = (String) context.get("subject");
		String templateType = (String) context.get("templateType");

		try {
			if (text !=null) {
				if (text.length() > 160) {
					return ServiceUtil.returnError("Text content should not be > 160 ");
				}
			}
			// checking the company setup start
			String activeStatus = null;
			GenericValue mergeForm = null;
			try {
				mergeForm = EntityQuery.use(delegator).from("MergeForm").where("mergeFormId", mergeFormId).queryOne();
				/*if (templateType !=null  && templateType.contains("?")) {
                    context.remove("templateType");
                    templateType = templateType.substring(0, templateType.indexOf("?"));
                    context.put("templateType", templateType);
                }*/
				mergeForm.setNonPKFields(context);
				delegator.store(mergeForm);
				return ServiceUtil.returnSuccess("Template Updated Successfully");
			} catch (GenericEntityException e) {
				Debug.logError(e.getMessage(), module);
				return ServiceUtil.returnError(e.getMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		result = ServiceUtil.returnSuccess();
		result.put("mergeFormId", mergeFormId);
		return result;
	}
	/**
	 * Update Merge Form
	 * 
	 * @param dctx
	 * @param context
	 * @return
	 */
	public static Map < String, Object > updateMergeForm(DispatchContext dctx, Map < String, Object > context) {
		Map < String, Object > result = ServiceUtil.returnSuccess();
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		String text = (String) context.get("text");
		String mergeFormId = (String) context.get("mergeFormId");
		Boolean privateForm = "Y".equals(context.get("privateForm"));
		String mergeFormName = (String) context.get("mergeFormName");
		String description = (String) context.get("description");
		String templateContent = (String) context.get("mergeFormText");
		String senderEmail = (String) context.get("senderEmail");
		String senderName = (String) context.get("senderName");
		String replayToName = (String) context.get("replayToName");
		String senderEmailReplay = (String) context.get("senderEmailReplay");
		String templateType = (String) context.get("templateType");
		String emailCharSet = (String) context.get("emailCharSet");
		String emailLineFeed = (String) context.get("emailLineFeed");
		String mailFormat = (String) context.get("mailFormat");
		String emailOnePixel = (String) context.get("emailOnePixel");
		String subject = (String) context.get("subject");
		GenericValue mergeForm = null;
		try {
			mergeForm = EntityQuery.use(delegator).from("MergeForm").where("mergeFormId", mergeFormId).queryOne();
			mergeForm.setNonPKFields(context);
			/*if ((!privateForm)) {
                mergeForm.set("partyId", null);
            }*/
			delegator.store(mergeForm);
			//return ServiceUtil.returnSuccess("Template Updated Successfully");
		} catch (GenericEntityException e) {
			Debug.logError(e.getMessage(), module);
			return ServiceUtil.returnError(e.getMessage());
		}
		result = ServiceUtil.returnSuccess("Template Updated Successfully");
		result.put("mergeFormId", mergeFormId);
		return result;
	}



	public static String addExistingSmartLisTemplate(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		String smartListId = request.getParameter("contactSmartListId");
		
		try {
			if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(smartListId)){
				GenericValue smartListTemplate = delegator.findOne("SmartlistTemplateBuilder", UtilMisc.toMap("smartListId",smartListId), false);
				GenericValue campaignTypeSmartList = EntityUtil.getFirst(delegator.findByAnd("SmartlistTemplateBuilder", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), UtilMisc.toList("lastUpdatedTxStamp DESC"), false));
				if(campaignTypeSmartList == null) {
					if(smartListTemplate != null && smartListTemplate.size() >0) {
						String criteria = smartListTemplate.getString("criteria");
						String name = smartListTemplate.getString("name");
						
						// create campaign type smart list
						GenericValue createSmartListTemplate = delegator.makeValue("SmartlistTemplateBuilder");
						createSmartListTemplate.set("smartListId", delegator.getNextSeqId("SmartlistTemplateBuilder"));
						createSmartListTemplate.set("name", name+"_"+marketingCampaignId);
						createSmartListTemplate.set("marketingCampaignId", marketingCampaignId);
						createSmartListTemplate.set("criteria", criteria);
						createSmartListTemplate.set("type", "CAMPAIGN_TYPE");
						createSmartListTemplate.create();
					}
				} else {
					String isProcessed = campaignTypeSmartList.getString("isProcessed");
					//String campaignTypeSmId = campaignTypeSmartList.getString("smartListId");
					if(!"Y".equals(isProcessed)) {
						String newCriteria = smartListTemplate.getString("criteria");
						String newName = smartListTemplate.getString("name");
						campaignTypeSmartList.set("criteria", newCriteria);
						campaignTypeSmartList.set("name", newName);
						campaignTypeSmartList.store();
					}
				}
				
				request.setAttribute("_EVENT_MESSAGE_","Smart list template successfully assigned to campaign");
				request.setAttribute("marketingCampaignId", marketingCampaignId);
				request.setAttribute("activeTab", "smartList");
				request.setAttribute("smartListId", smartListId);
			}else{
				request.setAttribute("_ERROR_MESSAGE_","Error in smart list assign.");
				return "error";
			}
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}

		return "success";
	}

	public static String getSmartListTemplate(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		String smartListId=request.getParameter("smartListId");

		try {
			if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(smartListId)){				
				request.setAttribute("marketingCampaignId", marketingCampaignId);
				request.setAttribute("activeTab", "smartList");
				request.setAttribute("smartListId", smartListId);
			}else{
				request.setAttribute("_ERROR_MESSAGE_","Error getting smart list.");
				return "error";
			}
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}

		return "success";
	}

	/**
	 * Add Recepients
	 * 
	 * @param ctx
	 * @param context
	 * @return
	 * @throws GenericServiceException
	 */
	public static Map<String, Object> createContactListParties(DispatchContext dctx, Map<String, ? extends Object> context)throws GenericServiceException {
		Map < String, Object > result = ServiceUtil.returnSuccess();
		Delegator delegator = dctx.getDelegator();
		Locale locale = (Locale) context.get("locale");
		Timestamp now = UtilDateTime.nowTimestamp();
		String firstName = "";
		String contactMechId = "";
		int recepientsId = 0;
		String lastName = "";
		String groupName = "";
		String partyId = (String) context.get("partyId");
		String contactListId = (String) context.get("contactListId");
		String infoString = (String) context.get("infoString");
		int contactListId1 = Integer.valueOf(contactListId);
		String preferredContactMechId = (String) context.get("preferredContactMechId");
		String emailFormat = null;
		try {
			/*
			 * GenericValue getSolicitation = EntityUtil.getFirst(
			 * delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId",
			 * partyId, "contactMechId",preferredContactMechId)) );
			 * if(UtilValidate.isNotEmpty(getSolicitation)){ solicitation =
			 * getSolicitation.getString("allowSolicitation"); }
			 */
			GenericValue gv = delegator.findOne("Party",UtilMisc.toMap("partyId", partyId),false);
			if (UtilValidate.isNotEmpty(gv)) {
				GenericValue gv1 = delegator.findOne("Person",UtilMisc.toMap("partyId", partyId),false);
				if (UtilValidate.isNotEmpty(gv1)) {
					firstName = gv1.getString("firstName");
					lastName = gv1.getString("lastName");
				}
			} else if (UtilValidate.isEmpty(gv)) {

				GenericValue group = delegator.findOne("PartyGroup",UtilMisc.toMap("partyId", partyId),false);
				if (UtilValidate.isNotEmpty(group)) {
					groupName = group.getString("groupName");
				}
			}
			/*// get Email Format
			GenericValue getEmailFormat = delegator.findOne("PartyAttribute", UtilMisc.toMap("partyId", partyId,"attrName", "EMAIL_FORMAT"),false);
			if (UtilValidate.isEmpty(getEmailFormat)) {
				emailFormat = "1";
			} else {
				String format = getEmailFormat.getString("attrValue");
				if ("feedback".equals(format)) {
					emailFormat = "1";
				} else {
					emailFormat = "0";
				}
			}*/
			// check the contact List Purpose Type
			String contactPurposeType = "";
			String contactMechTypeId = "";
			String contactMechTypeVal = "";
			String contactNumber = "";
			GenericValue checkListId = delegator.findOne("ContactList",UtilMisc.toMap("contactListId", contactListId),false);
			if (UtilValidate.isNotEmpty(checkListId)) {
				contactPurposeType = checkListId.getString("contactPurposeTypeId");
				contactMechTypeId = checkListId.getString("contactMechTypeId");
			}
			if ("TELECOM_NUMBER".equals(contactMechTypeId)) {
				GenericValue gv2 = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", preferredContactMechId),false);
				if (UtilValidate.isNotEmpty(gv2)) {
					contactNumber = gv2.getString("contactNumber");
				}
				contactMechTypeVal="PRIMARY_PHONE";
			} else if ("EMAIL_ADDRESS".equals(contactMechTypeId)) {
				GenericValue gv2 = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", preferredContactMechId),false);
				if (UtilValidate.isNotEmpty(gv2)) {
					infoString = gv2.getString("infoString");
				}
				contactMechTypeVal="PRIMARY_EMAIL";
			} else if ("POSTAL_ADDRESS".equals(contactMechTypeId)) {
				GenericValue gv2 = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", preferredContactMechId),false);
				if (UtilValidate.isNotEmpty(gv2)) {
					infoString = gv2.getString("infoString");
				}
				contactMechTypeVal="PRIMARY_LOCATION";
			}
			/*if (contactMechTypeId.equals("TELECOM_NUMBER")) {
				infoString = partyId + "@gmail.com";
			}*/
			/*String openEMMService = TenantEnabledUtilProperties.getPropertyValue(delegator, "rms", "rms.openEMM.service");*/
			// using String array
			/*boolean yes = true;
				String[] ColumnName = { "email", "firstname", "lastname","phone_number", "emailFormat", "customField1" };
				String[] values = { infoString, firstName, lastName,contactNumber, emailFormat, partyId };
				String recepientId = "";*/
			EntityCondition isAccountCondition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT"),
					EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)
					), EntityOperator.AND);
			EntityCondition isContactCondition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "CONTACT"),
					EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)
					), EntityOperator.AND);
			Long isAccountCountToComp = delegator.findCountByCondition("PartyRole",isAccountCondition, null,null);
			Long isContactCountToComp = delegator.findCountByCondition("PartyRole",isAccountCondition, null,null);
			String partyAccId=null;
			if(isAccountCountToComp >0){
				String partyIdContact=null;
				Set <String> findOptions = UtilMisc.toSet("partyIdTo");
				findOptions.add("partyIdFrom");
				List conditions = FastList.newInstance();
				EntityCondition roleTypeCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
						EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
						EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV"),
						EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT")
						), EntityOperator.AND);
				conditions.add(roleTypeCondition1);
				EntityCondition partyStatusCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
						EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
						), EntityOperator.OR);
				conditions.add(partyStatusCondition1);

				if (UtilValidate.isNotEmpty(partyId)) {
					conditions.add(EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyId));
				}
				EntityFindOptions efo1 = new EntityFindOptions();
				efo1.setDistinct(true);
				conditions.add(EntityUtil.getFilterByDateExpr());
				EntityCondition roleConditionF = EntityCondition.makeCondition(EntityCondition.makeCondition(conditions, EntityOperator.AND));

				List<GenericValue>  partyFromReln = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", roleConditionF, findOptions, UtilMisc.toList("createdDate"), efo1, false);
				if(partyFromReln.size()>0){
					for(GenericValue partyContact:partyFromReln){
						partyIdContact= partyContact.getString("partyIdFrom");
						partyAccId=partyId;
						boolean created=checkAndAddCustomer(delegator,partyIdContact,partyAccId,contactListId,contactMechTypeVal);
						if(created){
							//successCount++;
						}else{
							//dedupCount++;
						}

					}

					/*partyIdContact= partyFromReln.get(0).getString("partyIdFrom");
					partyAccId=partyId;
					partyId=partyIdContact;
					System.out.println("partyAccId"+partyAccId+"partyId"+partyId);
					GenericValue gv5 = delegator.makeValue("ContactListParty");
					gv5.put("contactListId", contactListId);
					gv5.put("partyId", partyId);
					gv5.put("fromDate", UtilDateTime.nowTimestamp());
					gv5.put("statusId", "CLPT_ACCEPTED");
					gv5.put("acctPartyId", partyAccId);
					gv5.put("preferredContactMechId", preferredContactMechId);
					gv5.create();*/


				}else{/*

				GenericValue contactListErroLog = delegator.makeValue("FioContactListErrorLog");
				contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
				contactListErroLog.set("contactListId",contactListId);

				contactListErroLog.set("partyId",partyId);
				contactListErroLog.set("status","ERROR");
				contactListErroLog.set("errorLog",CampConstants.ErrorLogs.NOCONTACTASSOC);
				contactListErroLog.set("createdTime", UtilDateTime.nowTimestamp());
				contactListErroLog.create();

				 */}
			}else if(isContactCountToComp >0){
				Set <String> findOptions = UtilMisc.toSet("partyIdTo");
				findOptions.add("partyIdFrom");
				List conditions = FastList.newInstance();
				EntityCondition roleTypeCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
						EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
						EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV"),
						EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT")
						), EntityOperator.AND);
				conditions.add(roleTypeCondition1);
				EntityCondition partyStatusCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
						EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
						), EntityOperator.OR);
				conditions.add(partyStatusCondition1);

				if (UtilValidate.isNotEmpty(partyId)) {
					conditions.add(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId));
				}
				EntityFindOptions efo1 = new EntityFindOptions();
				efo1.setDistinct(true);
				conditions.add(EntityUtil.getFilterByDateExpr());
				EntityCondition roleConditionF = EntityCondition.makeCondition(EntityCondition.makeCondition(conditions, EntityOperator.AND));

				List<GenericValue>  partyFromReln = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", roleConditionF, findOptions, UtilMisc.toList("createdDate"), efo1, false);
				if(partyFromReln.size()>0){
					for(GenericValue partyContact:partyFromReln){
						partyAccId=partyContact.getString("partyIdTo");
						boolean created=checkAndAddCustomer(delegator,partyId,partyAccId,contactListId,contactMechTypeVal);
						if(created){
							//successCount++;
						}else{
							//dedupCount++;
						}

					}

				}
				/*System.out.println("partyAccId22222222222"+partyAccId+"partyId"+partyId);
				GenericValue gv5 = delegator.makeValue("ContactListParty");
				gv5.put("contactListId", contactListId);
				gv5.put("partyId", partyId);
				gv5.put("fromDate", UtilDateTime.nowTimestamp());
				gv5.put("statusId", "CLPT_ACCEPTED");
				gv5.put("acctPartyId", partyAccId);
				gv5.put("preferredContactMechId", preferredContactMechId);
				gv5.create();
				 */
			}else{
				boolean created=checkAndAddCustomer(delegator,partyId,partyAccId,contactListId,contactMechTypeVal);
			}



		} catch (GenericEntityException e) {
			String errMsgs = "Failure in Add List Member , rolling back transaction: "+ e.toString();
			Debug.logError(e, errMsgs, module);
			return ServiceUtil.returnError(errMsgs);
		}

		return result;
	}
	public static boolean checkAndAddCustomer(Delegator delegator,String partyId,String acctId,String contactListId,String contactMechType) throws GenericEntityException{
		List conditionList = FastList.newInstance();
		conditionList.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
		conditionList.add(EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, acctId));
		conditionList.add(EntityCondition.makeCondition("partyId",EntityOperator.EQUALS,partyId));
		conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("thruDate", EntityOperator.EQUALS, null),EntityOperator.OR,EntityCondition.makeCondition("thruDate", EntityOperator.GREATER_THAN, UtilDateTime.nowTimestamp())));

		List<GenericValue> partyContactMechPurpose = delegator.findByAnd("PartyContactMechPurpose",UtilMisc.toMap("partyId",partyId,"contactMechPurposeTypeId",contactMechType),UtilMisc.toList("lastUpdatedStamp DESC"),false);
		partyContactMechPurpose= EntityUtil.filterByDate(partyContactMechPurpose, true);
		EntityCondition mainCondition = EntityCondition.makeCondition(EntityCondition.makeCondition(conditionList, EntityOperator.AND));
		List contactPartyLists = delegator.findList("ContactListParty", mainCondition, null, null,null,false);

		if(contactPartyLists.size()>0){
			//Duplicate party - already available
			return false;
		}else{
			EntityCondition getSeqCondition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
					EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)
					), EntityOperator.AND);
			Long seqCount = delegator.findCountByCondition("ContactListParty",getSeqCondition, null,null);

			GenericValue contactListParty = delegator.makeValue("ContactListParty",UtilMisc.toMap("contactListId",contactListId,"partyId",partyId,"fromDate",UtilDateTime.nowTimestamp(),"contactListSeqId", seqCount+1+""));
			contactListParty.set("statusId", "CLPT_ACCEPTED");
			if(UtilValidate.isNotEmpty(partyContactMechPurpose)){
				String contactMechId = partyContactMechPurpose.get(0).getString("contactMechId");
				contactListParty.set("preferredContactMechId", contactMechId);
			}
			contactListParty.put("statusId", "CLPT_ACCEPTED");
			contactListParty.set("acctPartyId", acctId);
			//contactListParty.set("contactListSeqId", seqCount+1+"");
			contactListParty.create();
			return true;

		}
	}

	public static String addExistingSmartLisToCampagin(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		String contactSmartListId=request.getParameter("contactSmartListId");
		try {
			if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(contactSmartListId)){
				String contactListId="";
				String contactPurposeType="";
				if (UtilValidate.isNotEmpty(contactSmartListId)) {

					if( contactSmartListId.indexOf("(")>0 && contactSmartListId.indexOf(")")>0 ){

						contactListId=contactSmartListId.substring(contactSmartListId.indexOf("(")+1, contactSmartListId.indexOf(")"));
					}
					else{
						contactListId=contactSmartListId;
					}
					GenericValue contactListGV=delegator.findOne("ContactList",UtilMisc.toMap("contactListId", contactListId),false);
					if (UtilValidate.isNotEmpty(contactListGV)) {
						contactPurposeType=contactListGV.getString("contactPurposeTypeId");
					}
				}
				List<GenericValue> existingContactList = delegator.findByAnd("MarketingCampaignContactList",UtilMisc.toMap("contactListId",contactListId,"marketingCampaignId",marketingCampaignId),null,false);
				if(existingContactList!=null && existingContactList.size()>0){
					request.setAttribute("_ERROR_MESSAGE_","This List already assigned for this campaign.");
					return "error";
				}

				GenericValue marketingCampaignContactList = delegator.makeValue("MarketingCampaignContactList");
				String campaignListId =delegator.getNextSeqId("MarketingCampaignContactList"); 
				marketingCampaignContactList.set("campaignListId",campaignListId);
				marketingCampaignContactList.set("marketingCampaignId",marketingCampaignId);
				marketingCampaignContactList.set("contactListId",contactListId);
				marketingCampaignContactList.set("fromDate",UtilDateTime.nowTimestamp());
				marketingCampaignContactList.set("contactPurposeType",contactPurposeType);

				delegator.create(marketingCampaignContactList);
				TransactionUtil.commit();


				Long sequenceNumberToComp = delegator.findCountByCondition("ContactListParty",
						EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId), null,
						null);
				if(sequenceNumberToComp==0){
					marketingCampaignContactList.put("isProcessed", "Y");
					marketingCampaignContactList.store();
				}else{
					String jarMainClassName = UtilProperties.getPropertyValue("campaign","fio-CampList-jar-main-class-name");
					String ofbizHome = System.getProperty("ofbiz.home");
					String dirPath = null;
					GenericValue systemProperty = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "campaignlist","systemPropertyId","filepath").queryOne();
					if(UtilValidate.isNotEmpty(systemProperty)){
						dirPath = systemProperty.getString("systemPropertyValue");
					}
					if(UtilValidate.isEmpty(dirPath)){
						request.setAttribute("_ERROR_MESSAGE_","Uploaded Filepath is not configured.");
						return "error";
					}

					/*To get location from config.properties
				String location = UtilProperties.getPropertyValue("campaign", "fio-CampList-jar-location");*/

					/*To get location using absolute path 
				 File directory = new File("./");
			    String abPath=directory.getAbsolutePath();
			    String location = abPath+"/hot-deploy/campaign/config/external-App/";*/


					String location = ofbizHome + dirPath;
					System.out.println(location+"locationlocation");
					String jarName = UtilProperties.getPropertyValue("campaign", "fio-CampList-jar-name");
					String fileExtension = UtilProperties.getPropertyValue("campaign", "fio-CampList-file-extension");		
					if(UtilValidate.isEmpty(fileExtension)){
						fileExtension = ".sh";
					}
					//String fileName=location+File.separator+marketingCampaignId+fileExtension;
					String fileName=location+marketingCampaignId+fileExtension;
					File listAssoc = new File(fileName);
					Writer writer = new java.io.FileWriter(listAssoc);
					writer.append("cd "+location);
					writer.append("\n");
					writer.append("java -cp "+jarName+" "+jarMainClassName+" "+marketingCampaignId/*+" "+"&&"*/);
					writer.close();
					listAssoc.setExecutable(true);
					listAssoc.setReadable(true);
					listAssoc.setWritable(true);
					System.out.println(fileName);
					ProcessBuilder processBuilder = new ProcessBuilder(fileName);
					Process p = processBuilder.start();
					BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line = null;
					while ((line = reader.readLine()) != null)
					{
						System.out.println(line);
					}
					try{
						p.waitFor();
						System.out.println("after starting");
						listAssoc.delete();
					}catch(Exception s){
						s.printStackTrace();
					}
				}
				request.setAttribute("_EVENT_MESSAGE_","List assigned to campaign successfully.");
				request.setAttribute("marketingCampaignId", marketingCampaignId);
				request.setAttribute("activeTab", "contact");

			}else{
				request.setAttribute("_ERROR_MESSAGE_","Error in smart list assign.");
				return "error";
			}
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		return "success";
	}


	public static String generateCallList(HttpServletRequest request, HttpServletResponse response) {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Timestamp now = UtilDateTime.nowTimestamp();
		String firstName = "";
		String tenantId = delegator.getDelegatorTenantId();
		//String csrPartyId = (String) request.getParameter("csrPartyId");
		String marketingCampaignId = (String) request.getParameter("marketingCampaignId");

		if (UtilValidate.isNotEmpty(marketingCampaignId)) {
			try {
				String fileExtension = UtilProperties.getPropertyValue("campaign", "fio-princess-file-extension");
				if (UtilValidate.isEmpty(fileExtension)) {
					fileExtension = ".sh";
				}

				String jarMainClassName = UtilProperties.getPropertyValue("campaign","fio-callList-jar-main-class-name");
				String ofbizHome = System.getProperty("ofbiz.home");
				String dirPath = null;
				GenericValue systemProperty = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "campaignlist","systemPropertyId","filepath").queryOne();
				if(UtilValidate.isNotEmpty(systemProperty)){
					dirPath = systemProperty.getString("systemPropertyValue");
				}
				if(UtilValidate.isEmpty(dirPath)){
					request.setAttribute("_ERROR_MESSAGE_","Uploaded Filepath is not configured.");
					return "error";
				}

				String location = ofbizHome + dirPath;
				System.out.println(location+"locationlocation");
				String jarName = UtilProperties.getPropertyValue("campaign", "fio-CampList-jar-name");

				//File f = new File(location + File.separator + marketingCampaignId + fileExtension);
				String fileName=location+ File.separator +marketingCampaignId+fileExtension;
				File genList = new File(fileName);
				Debug.log("f---->" + genList);
				Writer writer = new java.io.FileWriter(genList);
				writer.append("cd " + location);
				writer.append("\n");
				writer.append("java -cp " + jarName + " " + jarMainClassName  + " " + marketingCampaignId);
				writer.close();
				genList.setExecutable(true);
				genList.setReadable(true);
				genList.setWritable(true);
				System.out.println("before  starting");
				ProcessBuilder processBuilder = new ProcessBuilder(fileName);
				Process p = processBuilder.start();
				try {
					Debug.log("statrted ewaiting");
					p.waitFor();
					Debug.log("after starting");
					genList.delete();
					Debug.log("jarrss n full---->" + jarName + " " + jarMainClassName  + " " + marketingCampaignId);
				} catch (Exception e) {

				}
				request.setAttribute("_EVENT_MESSAGE_", "Call List Generated successfully");
			} catch (Exception e) {

			}
		}
		return "success";
	}

	/*public static String rmssendEmailsRequest(HttpServletRequest request,HttpServletResponse response) throws GenericEntityException,ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Locale locale = UtilHttp.getLocale(request);
		String sendDate = request.getParameter("sendDate");
		int startDate = 0;
		String getLiveContactListId = "";
		int mailListId = 0;
		String description = "";
		String shortName = "";
		int templateId = 0;
		String strTemplate = "";
		String campaignTypeId = "";
		String errMsg = null;
		String openEmmCampaignId = "";
		String marketingCampignId = request.getParameter("marketingCampaignId");
		int mailCampId = Integer.parseInt(marketingCampignId);
		List<GenericValue> getCampInfo = delegator.findByAnd("MarketingCampaignContactList", UtilMisc.toMap("marketingCampaignId", marketingCampignId,"contactPurposeType", "LIVE"),null,false);
		if (getCampInfo.size() > 0) {
			GenericValue getTestContact = getCampInfo.get(0);
			getLiveContactListId = getTestContact.getString("contactListId");
			mailListId = Integer.parseInt(getLiveContactListId);
		}
		// get Campaign Information
		GenericValue CampInfo = delegator.findOne("MarketingCampaign",UtilMisc.toMap("marketingCampaignId", marketingCampignId),false);
		if (UtilValidate.isNotEmpty(CampInfo)) {
			shortName = CampInfo.getString("campaignName");
			description = CampInfo.getString("campaignSummary");
			strTemplate = CampInfo.getString("campaignTemplate");
			campaignTypeId = CampInfo.getString("campaignTypeId");
			openEmmCampaignId = CampInfo.getString("openEmmCampaignId");
			if (UtilValidate.isNotEmpty(strTemplate)) {
				templateId = Integer.parseInt(strTemplate);
			}
		}
		int mktgCampaignId = Integer.parseInt(openEmmCampaignId);
		Object[] mkObj = { new Integer(mktgCampaignId) };
		String templatesId = "";
		String rmsUserName = "";
		String rmsPassword = "";
		String clientName = "";
		mailCampId = Integer.parseInt(openEmmCampaignId);
		OpenemmClientRMS opc = new OpenemmClientRMS();
		try {
			String openEmmService = TenantEnabledUtilProperties.getPropertyValue(delegator, "rms","rms.openEMM.service");
			if ("Y".equalsIgnoreCase(openEmmService)) {
			GenericValue clientid = delegator.findOne("PartyAttribute", UtilMisc.toMap("partyId", "Company","attrName", "alternateOpenEmmClientId"),false);
			if (UtilValidate.isNotEmpty(clientid)) {
				clientName = (String) clientid.getString("attrValue");
			}
			GenericValue userName = delegator.findOne("PartyAttribute", UtilMisc.toMap("partyId", "Company","attrName", "rmsUserName"),false);
			if (UtilValidate.isNotEmpty(userName)) {
				rmsUserName = (String) userName.getString("attrValue");
			}
			GenericValue password = delegator.findOne("PartyAttribute", UtilMisc.toMap("partyId", "Company","attrName", "rmsPassword"),false);
			if (UtilValidate.isNotEmpty(password)) {
				rmsPassword = (String) password.getString("attrValue");
			}
			int companyId = 0;
			if (UtilValidate.isEmpty(clientid)) {
				companyId = 0;
			} else {
				companyId = Integer.parseInt(clientName);
			}

			// checking the company setup start
			String activeStatus = null;
			if (UtilValidate.isNotEmpty(companyId)) {

				GenericValue rmsCompanyConfigurationGV = delegator.findOne("RmsCompanyConfiguration",UtilMisc.toMap("companyId", companyId),false);
				if (UtilValidate.isNotEmpty(rmsCompanyConfigurationGV)) {
					activeStatus = rmsCompanyConfigurationGV.getString("status");
				}
			}

			if (!activeStatus.equalsIgnoreCase("active")) {
				request.setAttribute("_ERROR_MESSAGE_","Access Denied :Please check the company configuration set up.");
				return "error";
			}

			// get Tenant Properties
			String accountId = "";
			String accountToken = "";
			String fromNumber = "";
			List<GenericValue> getTenantValues = delegator.findByAnd("TenantProperties", UtilMisc.toMap("resourceName", "sms","propertyName", "twilio.realAccountSid"),null,false);
			if (getTenantValues.size() > 0) {
				GenericValue gh = getTenantValues.get(0);
				accountId = gh.getString("propertyValue");
			}
			List<GenericValue> getTenantValues1 = delegator.findByAnd("TenantProperties", UtilMisc.toMap("resourceName", "sms","propertyName", "twilio.realAuthToken"),null,false);
			if (getTenantValues1.size() > 0) {
				GenericValue gh1 = getTenantValues1.get(0);
				accountToken = gh1.getString("propertyValue");
			}
			List<GenericValue> getTenantValues2 = delegator.findByAnd("TenantProperties", UtilMisc.toMap("resourceName", "sms","propertyName", "twilio.realSmsFrom"),null,false);
			if (getTenantValues2.size() > 0) {
				GenericValue gh2 = getTenantValues2.get(0);
				fromNumber = gh2.getString("propertyValue");
			}
			String sendMailId = "";
			String smsResult = "";
			int mailingsId = opc.UpdateMailCampiagn(shortName, description,mailListId, "", "", "", "", mailCampId, companyId, "",rmsUserName, rmsPassword, templateId, campaignTypeId);
			if (mailingsId == mailCampId) {
				if (campaignTypeId.equals("EMAIL")) {
					sendMailId = opc.sendMailingRMS(mktgCampaignId, companyId,rmsUserName, rmsPassword, "LIVE");
					if (!"EM-E100".equals(sendMailId)) {
						if ("EM-S100".equals(sendMailId)) {
							GenericValue findGv = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampignId),false);
							if (UtilValidate.isNotEmpty(findGv)) {
								findGv.put("statusId", "MKTG_CAMP_PUBLISHED");
								findGv.store();

							}
							errMsg = UtilProperties.getMessage(resource,"EM-S100", locale);
							request.setAttribute("_EVENT_MESSAGE_", errMsg);
							return "success";
						} else {
							errMsg = UtilProperties.getMessage(resource,"CouldnotPublishtothisCampaign", locale);
							request.setAttribute("_ERROR_MESSAGE_", errMsg);
							return "error";
						}
					} else if ("EM-E999".equals(sendMailId)) {
						errMsg = UtilProperties.getMessage(resource, "EM-E999",locale);
						request.setAttribute("_ERROR_MESSAGE_", errMsg);
						return "error";
					} else if ("EM-E220".equals(sendMailId)) {
						errMsg = UtilProperties.getMessage(resource, "EM-E220",locale);
						request.setAttribute("_ERROR_MESSAGE_", errMsg);
						return "error";
					} else if ("EM-E221".equals(sendMailId)) {
						errMsg = UtilProperties.getMessage(resource, "EM-E221",locale);
						request.setAttribute("_ERROR_MESSAGE_", errMsg);
						return "error";
					} else if ("EM-E222".equals(sendMailId)) {
						request.setAttribute("_ERROR_MESSAGE_", errMsg);
						return "error";
					} else {
						errMsg = UtilProperties.getMessage(resource, "EM-E222",locale);
						request.setAttribute("_ERROR_MESSAGE_", errMsg);
						return "error";
					}
				} else if (campaignTypeId.equals("SMS")) {
					smsResult = opc.Sendsms(rmsUserName, rmsPassword,accountId, accountToken, fromNumber,mktgCampaignId, companyId, "LIVE");
					if (smsResult != null && smsResult.equals("EM-S105")) {
						GenericValue findGv = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampignId),false);
						if (UtilValidate.isNotEmpty(findGv)) {

							findGv.put("statusId", "MKTG_CAMP_PUBLISHED");
							findGv.store();
						}
						request.setAttribute("_EVENT_MESSAGE_","Campaign Published");

					} else if (smsResult.equals("EM-E999")) {
						request.setAttribute("_ERROR_MESSAGE_"," Run time Exception,Contact Admin");
						return "error";

					} else if (smsResult.equals("EM-E100")) {
						request.setAttribute("_ERROR_MESSAGE_","Access Denied :Could not Publish to this Campaign");
						return "error";

					} else {
						// Twilio SMS Exceptions
						request.setAttribute("_ERROR_MESSAGE_", smsResult);
						return "error";
					}
				}

			} else {
				request.setAttribute("_ERROR_MESSAGE_", "Invalid Campaign");
				return "error";

			}
			}
		} catch (Exception e) {
			System.out.println("Exception" + e);

		}
		return "success";
	}
	 */
}