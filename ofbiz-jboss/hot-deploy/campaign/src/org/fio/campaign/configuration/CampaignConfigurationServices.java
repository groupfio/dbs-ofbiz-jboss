package org.fio.campaign.configuration;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

public class CampaignConfigurationServices {

	public static final String resource = "campaignUiLabels";
	public static final String MODULE = CampaignConfigurationServices.class.getName();
	public static Map<String, Object> createOrUpdateFinancialMetricConfig(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		String userLoginId = userLogin.getString("partyId");
		Locale locale = (Locale) context.get("locale");
		String returnMsg = null;
		String financialConfigId = (String) context.get("financialConfigId");
		String financialConfigName = (String) context.get("financialConfigName");
		String startDate = (String) context.get("startDate");
		String endDate = (String) context.get("endDate");
		String financialConfigType = (String) context.get("financialConfigType");
		Map<String, Object> result = ServiceUtil.returnSuccess();
		try {
			if(UtilValidate.isNotEmpty(financialConfigName) && UtilValidate.isNotEmpty(financialConfigType) && UtilValidate.isNotEmpty(startDate) && UtilValidate.isNotEmpty(endDate)) {
				SimpleDateFormat  formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date financialConfigFromDate = (Date)formatter.parse(startDate);
				Date financialConfigThruDate = (Date)formatter.parse(endDate);
				Timestamp financialConfigFromDateTs = UtilDateTime.getDayStart(new Timestamp(financialConfigFromDate.getTime()));
				Timestamp financialConfigThruDateTs = UtilDateTime.getDayEnd(new Timestamp(financialConfigThruDate.getTime()));
				BigDecimal costPerValue = (BigDecimal) context.get("costPerValue");
				GenericValue financialMetricConfig = null;
				if(UtilValidate.isNotEmpty(financialConfigId)) {
					financialMetricConfig = EntityQuery.use(delegator).from("FinancialMetricConfig").where("financialConfigId", financialConfigId).queryOne();
					financialMetricConfig.put("financialConfigName", financialConfigName);
					financialMetricConfig.put("financialConfigType", financialConfigType);
					financialMetricConfig.put("financialConfigFromDate", financialConfigFromDateTs);
					financialMetricConfig.put("financialConfigThruDate", financialConfigThruDateTs);
					financialMetricConfig.put("costPerValue", costPerValue);
					financialMetricConfig.put("createdModifiedUserLogin", userLoginId);
					financialMetricConfig.store();
					returnMsg = UtilProperties.getMessage(resource,"financialMetricConfigUpdatedSuccessfully", locale);
				} else {
					if(UtilValidate.isNotEmpty(financialConfigType)) {

						List<EntityCondition> condExprs = new LinkedList<EntityCondition>();
						condExprs.add(EntityCondition.makeCondition("financialConfigType", EntityOperator.EQUALS, financialConfigType));
						condExprs.add(EntityCondition.makeCondition("financialConfigName", EntityOperator.EQUALS, financialConfigName));
						condExprs.add(EntityCondition.makeCondition("costPerValue", EntityOperator.EQUALS, costPerValue));
						List<EntityCondition> exprs = new LinkedList<EntityCondition>();
						exprs.add(EntityCondition.makeCondition("isExpired", EntityOperator.EQUALS, "N"));
						exprs.add(EntityCondition.makeCondition("isExpired", EntityOperator.EQUALS, ""));
						exprs.add(EntityCondition.makeCondition("isExpired", EntityOperator.EQUALS, null));
						condExprs.add(EntityCondition.makeCondition(exprs, EntityOperator.OR));
						EntityCondition cond = EntityCondition.makeCondition(condExprs, EntityOperator.AND);

						List<GenericValue> expireConfigurationDuplicateVal = EntityQuery.use(delegator).from("FinancialMetricConfig").where(cond).queryList();
						if(expireConfigurationDuplicateVal != null && expireConfigurationDuplicateVal.size() > 0) {
							returnMsg = UtilProperties.getMessage(resource,"financialMetricConfigAlreadyExists", locale);
							return ServiceUtil.returnError(returnMsg);
						}
						List<GenericValue> expireConfiguration = EntityQuery.use(delegator).from("FinancialMetricConfig").where("financialConfigType", financialConfigType).queryList();
						if(expireConfiguration != null && expireConfiguration.size() > 0) {
							for (GenericValue expireConfigurationGV : expireConfiguration) {
								expireConfigurationGV.set("isExpired", "Y");
							}
							delegator.storeAll(expireConfiguration);
						}
					}
					financialConfigId = delegator.getNextSeqId("FinancialMetricConfig");
					financialMetricConfig = delegator.makeValue("FinancialMetricConfig", UtilMisc.toMap("financialConfigId", financialConfigId));
					financialMetricConfig.put("financialConfigName", financialConfigName);
					financialMetricConfig.put("financialConfigType", financialConfigType);
					financialMetricConfig.put("financialConfigFromDate", financialConfigFromDateTs);
					financialMetricConfig.put("financialConfigThruDate", financialConfigThruDateTs);
					financialMetricConfig.put("costPerValue", costPerValue);
					financialMetricConfig.put("createdModifiedUserLogin", userLoginId);
					financialMetricConfig.put("isExpired", "N");
					financialMetricConfig.create();
					returnMsg = UtilProperties.getMessage(resource,"financialMetricConfigCreatedSuccessfully", locale);
				}
			} else {
				returnMsg = UtilProperties.getMessage(resource,"financialMetricConfigMissingRequiredFields", locale);
				return ServiceUtil.returnError(returnMsg);
			}
		} catch (Exception e) {
			Debug.logError("Exception in createOrUpdateFinancialMetricConfig"+e.getMessage(), MODULE);
			returnMsg = UtilProperties.getMessage(resource,"errorInFinancialMetricConfig", locale);
			return ServiceUtil.returnError(returnMsg);
		}
		result = ServiceUtil.returnSuccess(returnMsg);
		result.put("financialConfigId", financialConfigId);
		return result; 
	}

	public static String removeFinancialMetricsConfig(HttpServletRequest request,HttpServletResponse response){
		GenericDelegator delegator = (GenericDelegator) request.getAttribute("delegator");
		try {
			String financialConfigId = request.getParameter("financialConfigId");
			Locale locale = UtilHttp.getLocale(request);
			String returnMsg = null;
			if(UtilValidate.isNotEmpty(financialConfigId)) {
				GenericValue financialMetricConfig = EntityQuery.use(delegator).from("FinancialMetricConfig").where("financialConfigId", financialConfigId).queryOne();
				if(financialMetricConfig != null && financialMetricConfig.size() > 0) {
					financialMetricConfig.remove();
					returnMsg = UtilProperties.getMessage(resource,"fMCRemovedSuccessfully", locale);
					request.setAttribute("_EVENT_MESSAGE_", returnMsg);
				} else {
					returnMsg = UtilProperties.getMessage(resource,"fMCNotFound", locale);
					request.setAttribute("_ERROR_MESSAGE_", returnMsg);
					return "error";
				}
			} else {
				returnMsg = UtilProperties.getMessage(resource,"fMCNotFound", locale);
				request.setAttribute("_ERROR_MESSAGE_", returnMsg);
				return "error";
			}
		} catch (Exception e) {
			Debug.log("------removeFinancialMetricsConfig------" + e.getMessage());
		}
		return "success";
	}

	public static String uploadPartiesFile(HttpServletRequest request,HttpServletResponse response){
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String contactListId = request.getParameter("contactListId");
		String roleTypeId = request.getParameter("roleTypeId");
		//String fileName = contactListId+".txt";
		String fileName = contactListId;
		String extension=null;
		File filepath = null;
		try
		{ 
			String ofbizHome = System.getProperty("ofbiz.home");
			String localPath = null;
			fileName = "ContactList_"+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			GenericValue systemProperty = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "smartlist","systemPropertyId","filepath").queryOne();
			if(UtilValidate.isNotEmpty(systemProperty)){
				localPath = systemProperty.getString("systemPropertyValue");
			}	
			String dirPath = ofbizHome + localPath;
			if(UtilValidate.isEmpty(dirPath))
			{
				String componentPath = ComponentConfig.getRootLocation("campaign");
				String path = "/smartlist/";
				dirPath = componentPath+path;
				filepath = new File(dirPath);
				filepath.mkdirs();
			}
			else{
				filepath = new File(dirPath);
			}
			Debug.log("ServletFileUpload.isMultipartContent---@M-->"+ServletFileUpload.isMultipartContent(request));
			if(ServletFileUpload.isMultipartContent(request)){
				try {
					List<FileItem> multiparts = new ServletFileUpload(
							new DiskFileItemFactory()).parseRequest(request);
					String fname = null;
					for(FileItem item : multiparts){

						if (item.isFormField()) {
							String name = item.getFieldName();
							if("roleTypeId".equals(name)){
								String value = item.getString();
								roleTypeId=value;
							}else if("roleType".equals(name)){
								String value = item.getString();
								if(roleTypeId==null){
									roleTypeId=value;
								}
							}else if("extension".equals(name)){
								String value = item.getString();
								extension=value;
							}
						}
					}
					System.out.println("extension"+extension);
					for(FileItem item : multiparts){
						if(!item.isFormField()){
							String name = new File(item.getName()).getName();

							if(name.endsWith(fileName))
							{
								fname = name;
							}
							else
							{
								fname = fileName+"."+extension;; 
							}
							File f = new File(filepath + File.separator + fname);
							if(f.exists())
							{
								f.delete();
							}
							item.write( f);
							String importedFilePath = filepath + File.separator + fname;
						}

					}
					String isValidate="N";
					GenericValue isValidateList = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","IS_VALIDATE").queryOne();
					if(isValidateList!=null && isValidateList.size()>0)
						isValidate= isValidateList.getString("attrValue");

					GenericValue uploadFileName = delegator.findOne("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_NAME"),false);
					if(uploadFileName!=null && uploadFileName.size()>0)
					{
						uploadFileName.set("attrValue", fname);
						uploadFileName.store();
					}
					else
					{
						uploadFileName = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_NAME"));
						uploadFileName.set("attrValue", fname);
						uploadFileName.create();
					}
					if("Y".equals(isValidate)){
						GenericValue listRoleType = delegator.findOne("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","ROLE_TYPE_ID"),false);
						if(listRoleType!=null && listRoleType.size()>0)
						{
							listRoleType.set("attrValue", roleTypeId);
							listRoleType.store();
						}
						else
						{
							listRoleType = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","ROLE_TYPE_ID"));
							listRoleType.set("attrValue", roleTypeId);
							listRoleType.create();
						}
					}
					GenericValue uploadFileStatus = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId",contactListId,"attrName","UPLOAD_FILE_STATUS").queryOne();

					if(uploadFileStatus!=null && uploadFileStatus.size()>0)
					{
						uploadFileStatus.set("attrValue", "CREATED");
						uploadFileStatus.store();

					}
					else
					{
						uploadFileStatus = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_STATUS"));
						uploadFileStatus.set("attrValue", "CREATED");
						uploadFileStatus.create();
					}
					//File uploaded successfully
					request.setAttribute("_EVENT_MESSAGE_", "File Uploaded Successfully");
				} catch (Exception ex) {
					request.setAttribute("_ERROR_MESSAGE_", "File Upload Failed due to " + ex);
					return "error";
				}
			}else{

				request.setAttribute("_ERROR_MESSAGE_","Servlet only handles file upload request");
				return "error";
			} 
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * Mahendran T
	 * @since 16-06-18
	 * */

	public static String createTemplateTagConfiguration(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException, ClassNotFoundException, ParseException {
		GenericDelegator delegator = (GenericDelegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession().getAttribute("userLogin");
		Locale locale = UtilHttp.getLocale(request);
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String tagId = request.getParameter("tagId");
		String tagName = request.getParameter("tagName");

		String templateTagTypeId = request.getParameter("templateTagTypeId");

		String fromDate = request.getParameter("fromDate");
		String thruDate = request.getParameter("thruDate");
		String isEnabled = request.getParameter("isEnabled");
		String isContact = request.getParameter("isContact");
		String groupSequenceNo = UtilValidate.isNotEmpty(request.getParameter("groupSequenceNo"))?request.getParameter("groupSequenceNo"):"0";
		String fieldSequenceNo = UtilValidate.isNotEmpty(request.getParameter("fieldSequenceNo"))?request.getParameter("fieldSequenceNo"):"0";

		String returnMsg = null;
		String customFieldGroupId = null;
		String customFieldId = null;
		String groupingCode = null;
		Timestamp fromTimestamp = null;
		Timestamp thruTimestamp = null;
		String groupType = request.getParameter("groupType");

		try {
			if("CUSTOM_FIELD".equals(templateTagTypeId)) {
				String attributeGroup = request.getParameter("attributeGroup");
				String attributeField = request.getParameter("attributeField");
				customFieldGroupId = attributeGroup;
				customFieldId = attributeField;
			} else if("SEGMENTATION".equals(templateTagTypeId)) {
				String segmentGroup = request.getParameter("segmentCode");
				String segmentGroupingCode = request.getParameter("segmentGroupingCode");
				//String segmentValue = request.getParameter("segmentationValue");
				customFieldGroupId = segmentGroup;
				groupingCode = segmentGroupingCode;
				//customFieldId = segmentValue;
			} else if("ECONOMIC_METRIC".equals(templateTagTypeId)) {
				String economicGroupingCode = request.getParameter("economicGroupingCode");
				String economicMetric = request.getParameter("economicMetric");
				String economicValue = request.getParameter("economicValue");
				customFieldGroupId = economicMetric;
				customFieldId = economicValue;
				groupingCode = economicGroupingCode;
			}
			if(UtilValidate.isNotEmpty(fromDate)) {
				Date date = df.parse(fromDate);
				fromTimestamp = new Timestamp(date.getTime());
			}
			if(UtilValidate.isNotEmpty(thruDate)) {
				Date date1 = df.parse(thruDate);
				thruTimestamp = new Timestamp(date1.getTime());
			}

			String userLoginId = userLogin.getString("userLoginId");
			if(UtilValidate.isNotEmpty(userLoginId)){
				List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup").where("userLoginId", userLoginId, "groupId", org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")).filterByDate().queryList();
				if(userLoginSecurityGroup != null && userLoginSecurityGroup.size() > 0) {
					GenericValue dataTagConfiguration = delegator.findOne("DataTagConfiguration", UtilMisc.toMap("tagId",tagId), false);
					if(dataTagConfiguration == null || dataTagConfiguration.size() == 0) {
						dataTagConfiguration = delegator.makeValue("DataTagConfiguration");
						dataTagConfiguration.put("tagId", tagId);
						dataTagConfiguration.put("tagName", tagName);
						dataTagConfiguration.put("templateTagTypeId", templateTagTypeId);
						dataTagConfiguration.put("customFieldGroupId", customFieldGroupId);
						dataTagConfiguration.put("customFieldId", customFieldId);
						dataTagConfiguration.put("groupingCode", groupingCode);
						dataTagConfiguration.put("fromDate", fromTimestamp);
						dataTagConfiguration.put("thruDate", thruTimestamp);
						dataTagConfiguration.put("isEnabled", isEnabled);
						dataTagConfiguration.put("isContact",isContact);
						dataTagConfiguration.put("groupSequenceNo", Long.valueOf(groupSequenceNo));
						dataTagConfiguration.put("fieldSequenceNo", Long.valueOf(fieldSequenceNo));
						dataTagConfiguration.create();
						returnMsg = UtilProperties.getMessage(resource,"tagCreatedSuccessfully", locale);
						request.setAttribute("_EVENT_MESSAGE_", returnMsg);
					}
				} else {
					returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
					request.setAttribute("_ERROR_MESSAGE_", returnMsg);
					return "error";
				}
			} else {
				returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
				request.setAttribute("_ERROR_MESSAGE_", returnMsg);
				return "error";
			}
		} catch (Exception e) {
			request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
			return "error";
		}
		return "success";
	}


	public static String deleteTemplateTagConfiguration(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException, ClassNotFoundException, ParseException {
		GenericDelegator delegator = (GenericDelegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession().getAttribute("userLogin");
		String tagId = request.getParameter("tagId");


		Locale locale = UtilHttp.getLocale(request);
		String returnMsg = null;
		try {
			String userLoginId = userLogin.getString("userLoginId");
			if(UtilValidate.isNotEmpty(userLoginId)){
				List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup").where("userLoginId", userLoginId, "groupId", org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")).filterByDate().queryList();
				if(userLoginSecurityGroup != null && userLoginSecurityGroup.size() > 0) {
					GenericValue dataTagConfiguration = delegator.findOne("DataTagConfiguration", UtilMisc.toMap("tagId",tagId), false);
					if(dataTagConfiguration != null && dataTagConfiguration.size() > 0) {
						dataTagConfiguration.remove();
						returnMsg = UtilProperties.getMessage(resource,"tagDeleteSuccessfully", locale);
						request.setAttribute("_EVENT_MESSAGE_", returnMsg);
					}
				} else {
					returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
					request.setAttribute("_ERROR_MESSAGE_", returnMsg);
					return "error";
				}
			} else {
				returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
				request.setAttribute("_ERROR_MESSAGE_", returnMsg);
				return "error";
			}
		} catch (Exception e) {
			request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
			return "error";
		}
		return "success";
	}

	public static String updateTemplateTagConfiguration(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException, ClassNotFoundException, ParseException {
		GenericDelegator delegator = (GenericDelegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession().getAttribute("userLogin");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String tagId = request.getParameter("tagId");
		String tagName = request.getParameter("tagName");

		String groupType = request.getParameter("groupType");

		String fromDate = request.getParameter("fromDate");
		String thruDate = request.getParameter("thruDate");
		String isEnabled = request.getParameter("isEnabled");
		String isContact = request.getParameter("isContact");
		String groupSequenceNo = request.getParameter("groupSequenceNo");
		String fieldSequenceNo = request.getParameter("fieldSequenceNo");

		Locale locale = UtilHttp.getLocale(request);
		String returnMsg = null;
		Timestamp fromTimestamp = null;
		Timestamp thruTimestamp = null;
		try {

			if(UtilValidate.isNotEmpty(fromDate)) {
				Date date = df.parse(fromDate);
				fromTimestamp = new Timestamp(date.getTime());
			}
			if(UtilValidate.isNotEmpty(thruDate)) {
				Date date1 = df.parse(thruDate);
				thruTimestamp = new Timestamp(date1.getTime());
			}

			String userLoginId = userLogin.getString("userLoginId");
			if(UtilValidate.isNotEmpty(userLoginId)){
				List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup").where("userLoginId", userLoginId, "groupId", org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")).filterByDate().queryList();
				if(userLoginSecurityGroup != null && userLoginSecurityGroup.size() > 0) {
					GenericValue dataTagConfiguration = delegator.findOne("DataTagConfiguration", UtilMisc.toMap("tagId",tagId), false);
					if(dataTagConfiguration != null && dataTagConfiguration.size() > 0) {
						dataTagConfiguration.put("tagName", tagName);
						dataTagConfiguration.put("fromDate", fromTimestamp);
						dataTagConfiguration.put("thruDate", thruTimestamp);
						dataTagConfiguration.put("isEnabled", isEnabled);
						dataTagConfiguration.put("isContact",isContact);
						dataTagConfiguration.put("groupSequenceNo", Long.valueOf(groupSequenceNo));
						dataTagConfiguration.put("fieldSequenceNo", Long.valueOf(fieldSequenceNo));
						dataTagConfiguration.store();
						returnMsg = UtilProperties.getMessage(resource,"tagUpdatedSuccessfully", locale);
						request.setAttribute("_EVENT_MESSAGE_", returnMsg);
					}
				} else {
					returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
					request.setAttribute("_ERROR_MESSAGE_", returnMsg);
					return "error";
				}
			} else {
				returnMsg = UtilProperties.getMessage(resource,"permissionDenied", locale);
				request.setAttribute("_ERROR_MESSAGE_", returnMsg);
				return "error";
			}
		} catch (Exception e) {
			request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
			return "error";
		}
		return "success";
	}
}
