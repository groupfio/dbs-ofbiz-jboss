package org.fio.campaign.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javolution.util.FastList;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.json.simple.parser.JSONParser;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityUtil;
import org.fio.campaign.template.RmsApiService;
public class AutoComplete {

	private AutoComplete() {
	}

	private static final String MODULE = AutoComplete.class.getName();
	public static final int WSV_DEFAULT_RESULTS_SIZE = 10;

	public static String getAutoCompleteProdMailingLists(
			HttpServletRequest request, HttpServletResponse response) {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Map<String, Object> resp = new HashMap<String, Object>();
		String keyword = String.valueOf(request.getParameter("mailingListId"));
		String keyword123 = (String) request.getAttribute("mailingListId");
		request.setAttribute("mailingIdKey", keyword);
		EntityCondition orCondition = EntityCondition.makeCondition(
				EntityOperator.OR, EntityCondition.makeCondition(
						"contactListId", EntityOperator.LIKE, "%" + keyword
						+ "%"), EntityCondition.makeCondition(
								"contactListName", EntityOperator.LIKE, "%" + keyword
								+ "%"), EntityCondition.makeCondition(
										EntityFunction.UPPER_FIELD("contactListName"),
										EntityOperator.LIKE, "%" + keyword + "%"));
		EntityCondition condition = EntityCondition.makeCondition(
				EntityOperator.AND, orCondition, EntityCondition.makeCondition(
						"contactPurposeTypeId", EntityOperator.EQUALS, "LIVE"));
		keyword = keyword.trim();
		List mailingIds = FastList.newInstance();
		HashMap listmap = new HashMap();
		if (keyword.length() > 0)
			try {
				List<GenericValue> listIds = delegator.findList("ContactList",
						condition, null, null, null, false);
				for (GenericValue list : listIds) {
					String listId = list.getString("contactListId");
					String listname = list.getString("contactListName");
					/*
					 * listmap.put("listId", list.getString("contactListId"));
					 * listmap.put("listName",
					 * list.getString("contactListName"));
					 */
					listmap.put(listId, listname);

					// mailingIds.add(listmap);
				}
				resp.put("mailingIds", listmap);
			} catch (Exception e) {

			}
		return doJSONResponse(response, listmap);
	}

	public static String doJSONResponse(HttpServletResponse response, Map map) {
		return doJSONResponse(response, JSONObject.fromObject(map));
	}

	public static String doJSONResponse(HttpServletResponse response,
			JSONObject jsonObject) {
		return doJSONResponse(response, jsonObject.toString());
	}

	public static String doJSONResponse(HttpServletResponse response,
			String jsonString) {
		String result = "success";

		response.setContentType("application/x-json");
		try {
			response.setContentLength(jsonString.getBytes("UTF-8").length);
		} catch (UnsupportedEncodingException e) {
			Debug.logWarning(
					"Could not get the UTF-8 json string due to UnsupportedEncodingException: "
							+ e.getMessage(), MODULE);
			response.setContentLength(jsonString.length());
		}

		Writer out;
		try {
			out = response.getWriter();
			out.write(validate(jsonString));
			out.flush();
		} catch (IOException e) {
			Debug.logError(e, "Failed to get response writer", MODULE);
			result = "error";
		}
		return result;
	}

	public static String validate(String str) {
		return str;
	}

	public static void getSmartFilters(HttpServletRequest request,
			HttpServletResponse response) throws GenericEntityException,
			ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String roleType = request.getParameter("roleTypeId");

		net.sf.json.JSONArray jsonArray = new net.sf.json.JSONArray();
		try {
			List<GenericValue> listofSegments = null;
			if (UtilValidate.isNotEmpty(roleType)) {
				listofSegments = delegator.findByAnd(
						"SegmentCodeConfigurationRoleType",
						UtilMisc.toMap("roleTypeId", roleType), null, false);

			}

			List conditionsList = FastList.newInstance();
			if (UtilValidate.isEmpty(roleType)) {
				EntityConditionList serachEnableConditions = EntityCondition
						.makeCondition(UtilMisc.toList(EntityCondition
								.makeCondition("enabled",
										EntityOperator.EQUALS, "Y"),
										EntityCondition.makeCondition("enabled",
												EntityOperator.EQUALS, null)),
												EntityOperator.OR);
				conditionsList.add(serachEnableConditions);
			}

			if (UtilValidate.isNotEmpty(listofSegments)
					&& UtilValidate.isNotEmpty(roleType)) {
				List<String> segmentIds = EntityUtil
						.getFieldListFromEntityList(listofSegments,
								"segmentCodeId", true);
				EntityConditionList roleConditions = EntityCondition
						.makeCondition(UtilMisc.toList(EntityCondition
								.makeCondition("partyClassificationTypeId",
										EntityOperator.IN, segmentIds)));
				conditionsList.add(roleConditions);
			}

			EntityCondition contactMechIdCondition = EntityCondition
					.makeCondition(conditionsList, EntityOperator.AND);
			List<GenericValue> PartyClassificationType = null;
			if (UtilValidate.isEmpty(listofSegments)
					&& UtilValidate.isNotEmpty(roleType)) {

			} else {
				PartyClassificationType = delegator.findList(
						"PartyClassificationType", contactMechIdCondition,
						null, null, null, false);
			}

			List allClasificationList = UtilMisc.toList("COUNTRY", "STATE",
					"AGE_GROUP", "GENDER_CODE", "CUST_CLASSIFICATION",
					"LEAD_CLASSIFICATION");
			for (GenericValue PartyClassificationTypeGv : PartyClassificationType) {
				String classificationGroup = PartyClassificationTypeGv
						.getString("partyClassificationTypeId");
				List<GenericValue> PartyClassificationGroup = delegator
						.findByAnd("PartyClassificationGroup", UtilMisc.toMap(
								"partyClassificationTypeId",
								classificationGroup), UtilMisc
								.toList("sequenceNum"), false);
				if (UtilValidate.isEmpty(PartyClassificationType)) {
					continue;
				}
				net.sf.json.JSONObject jsonObject1 = new net.sf.json.JSONObject();
				jsonObject1.put("id", PartyClassificationTypeGv
						.getString("partyClassificationTypeId"));
				jsonObject1.put("label",
						PartyClassificationTypeGv.getString("description"));
				jsonObject1.put("type", "string");
				jsonObject1.put("input", "select");
				net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
				operatorArray.add("equal");
				jsonObject1.put("operators", operatorArray.toString());

				net.sf.json.JSONArray partyClassificationGroupArray = new net.sf.json.JSONArray();
				for (GenericValue PartyClassificationGroupGv : PartyClassificationGroup) {
					net.sf.json.JSONObject PartyClassificationGroupGvJson = new net.sf.json.JSONObject();
					PartyClassificationGroupGvJson
					.put(PartyClassificationGroupGv
							.getString("partyClassificationGroupId"),
							PartyClassificationGroupGv
							.getString("description"));
					partyClassificationGroupArray
					.add(PartyClassificationGroupGvJson);
				}

				jsonObject1.put("values",
						partyClassificationGroupArray.toString());

				jsonArray.add(jsonObject1.toString());
			}
			PrintWriter pw = response.getWriter();
			pw.write(jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getSmartListJsonCustom(HttpServletRequest request,
			HttpServletResponse response) throws GenericEntityException,
			ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String roleType = request.getParameter("roleTypeId");

		net.sf.json.JSONArray jsonArray = new net.sf.json.JSONArray();
		try {
			List conditionsList = FastList.newInstance();

			if (UtilValidate.isNotEmpty(roleType)
					&& "PROSPECT".equals(roleType)) {
				roleType = "MY_LEAD";
			} else if (UtilValidate.isNotEmpty(roleType)
					&& "ACCOUNT".equals(roleType)) {
				roleType = "MY_ACCOUNT";
			} else if (UtilValidate.isNotEmpty(roleType)
					&& "CUSTOMER".equals(roleType)) {
				roleType = "MY_PERSON";
			} else if (UtilValidate.isNotEmpty(roleType)
					&& "CONTACT".equals(roleType)) {
				roleType = "MY_CONTACT";
			}

			EntityConditionList hideConditions = EntityCondition.makeCondition(
					UtilMisc.toList(EntityCondition.makeCondition("hide",
							EntityOperator.NOT_EQUAL, "Y"), EntityCondition
							.makeCondition("hide", EntityOperator.EQUALS, ""),
							EntityCondition.makeCondition("hide",
									EntityOperator.EQUALS, null)),
									EntityOperator.OR);

			// EntityConditionList hideConditions =
			// EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("hide",
			// EntityOperator.NOT_EQUAL,"Y")));
			conditionsList.add(hideConditions);
			if (UtilValidate.isNotEmpty(roleType)) {
				EntityConditionList roleConditions = EntityCondition
						.makeCondition(UtilMisc.toList(EntityCondition
								.makeCondition("roleTypeId",
										EntityOperator.EQUALS, roleType)));
				conditionsList.add(roleConditions);
			}
			EntityCondition customFieldsCondition = EntityCondition
					.makeCondition(conditionsList, EntityOperator.AND);

			List<GenericValue> PartyClassificationType = delegator.findList(
					"FioCustomFieldTemplate", customFieldsCondition, null,
					null, null, false);
			if (UtilValidate.isEmpty(PartyClassificationType)) {

				net.sf.json.JSONObject jsonObject1 = new net.sf.json.JSONObject();
				jsonObject1.put("id", "none");
				jsonObject1.put("label", "none");
				jsonObject1.put("type", "string");
				// jsonObject1.put("input", "text");
				net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
				operatorArray.add("equal");
				operatorArray.add("greater");
				operatorArray.add("less");
				operatorArray.add("greater_or_equal");
				operatorArray.add("less_or_equal");
				jsonObject1.put("operators", operatorArray.toString());
				jsonArray.add(jsonObject1.toString());
			}
			for (GenericValue PartyClassificationTypeGv : PartyClassificationType) {

				String fioCustomFieldType = PartyClassificationTypeGv
						.getString("fioCustomFieldType");

				if ("MULTI".equalsIgnoreCase(fioCustomFieldType)) {

					net.sf.json.JSONObject jsonObject1 = new net.sf.json.JSONObject();
					jsonObject1.put("id", PartyClassificationTypeGv
							.getString("fioCustomFieldId"));
					jsonObject1.put("label", PartyClassificationTypeGv
							.getString("fioCustomFieldName"));
					jsonObject1.put("type", "string");
					// Get the Multi Values
					List<GenericValue> fioCustomFieldMultiValuesList = delegator
							.findByAnd(
									"FioCustomFieldMultiValues",
									UtilMisc.toMap(
											"fioCustomFieldId",
											PartyClassificationTypeGv
											.getString("fioCustomFieldId")),
											UtilMisc.toList("sequenceNumber"), false);

					if (fioCustomFieldMultiValuesList.size() > 0) {

						net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
						operatorArray.add("equal");
						jsonObject1.put("operators", operatorArray.toString());

						jsonObject1.put("input", "select");
						net.sf.json.JSONArray partyClassificationGroupArray = new net.sf.json.JSONArray();
						for (GenericValue fioCustomFieldMultiValuesGv : fioCustomFieldMultiValuesList) {
							net.sf.json.JSONObject PartyClassificationGroupGvJson = new net.sf.json.JSONObject();
							PartyClassificationGroupGvJson.put(
									fioCustomFieldMultiValuesGv
									.getString("value"),
									fioCustomFieldMultiValuesGv
									.getString("discription"));
							partyClassificationGroupArray
							.add(PartyClassificationGroupGvJson);
						}
						jsonObject1.put("values",
								partyClassificationGroupArray.toString());

					} else {

						net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
						operatorArray.add("equal");
						operatorArray.add("greater");
						operatorArray.add("less");
						operatorArray.add("greater_or_equal");
						operatorArray.add("less_or_equal");

						jsonObject1.put("operators", operatorArray.toString());

						jsonObject1.put("input", "text");
					}
					jsonArray.add(jsonObject1.toString());
				} else {
					net.sf.json.JSONObject jsonObject1 = new net.sf.json.JSONObject();
					jsonObject1.put("id", PartyClassificationTypeGv
							.getString("fioCustomFieldId"));
					jsonObject1.put("label", PartyClassificationTypeGv
							.getString("fioCustomFieldName"));
					jsonObject1.put("type", "string");
					jsonObject1.put("input", "text");
					net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
					operatorArray.add("equal");
					operatorArray.add("greater");
					operatorArray.add("less");
					operatorArray.add("greater_or_equal");
					operatorArray.add("less_or_equal");
					jsonObject1.put("operators", operatorArray.toString());
					jsonArray.add(jsonObject1.toString());
				}

			}
			PrintWriter pw = response.getWriter();
			pw.write(jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getPartyAddJson(HttpServletRequest request,
			HttpServletResponse response) throws GenericEntityException,
			ClassNotFoundException, ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");

		net.sf.json.JSONArray jsonArray = new net.sf.json.JSONArray();
		try {
			List<GenericValue> AdditionalType = delegator.findAll(
					"AdditionalType", false);

			for (GenericValue PartyAdditionalDataGv : AdditionalType) {
				if (UtilValidate.isEmpty(AdditionalType)) {
					continue;
				}
				net.sf.json.JSONObject jsonObject1 = new net.sf.json.JSONObject();
				jsonObject1.put("id",
						PartyAdditionalDataGv.getString("addlDataTypeId"));
				jsonObject1.put("label",
						PartyAdditionalDataGv.getString("description"));
				jsonObject1.put("type", "string");
				jsonObject1.put("input", "text");
				net.sf.json.JSONArray operatorArray = new net.sf.json.JSONArray();
				operatorArray.add("equal");
				operatorArray.add("greater");
				operatorArray.add("less");
				operatorArray.add("greater_or_equal");
				operatorArray.add("less_or_equal");

				jsonObject1.put("operators", operatorArray.toString());
				// jsonObject1.put("values", "test");
				jsonArray.add(jsonObject1.toString());
			}
			PrintWriter pw = response.getWriter();
			pw.write(jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String createSmartContactListValues(
			HttpServletRequest request, HttpServletResponse response)
					throws GenericEntityException, ClassNotFoundException,
					ParseException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		HttpSession session = request.getSession();
		String jsonString = request.getParameter("jsonString");
		String marketingCampaignId = request
				.getParameter("marketingCampaignId");
		String contactListTypeId = "";
		String roleTypeId = "";
		boolean isUpdate = false;
		if (UtilValidate.isNotEmpty(request.getParameter("contactListTypeId"))) {
			contactListTypeId = request.getParameter("contactListTypeId");
		}
		String contactMechTypeId = "";
		if (UtilValidate.isNotEmpty(request.getParameter("contactMechTypeId"))) {
			contactMechTypeId = request.getParameter("contactMechTypeId");
		}
		String contactListName = "";
		if (UtilValidate.isNotEmpty(request.getParameter("contactListName"))) {
			contactListName = request.getParameter("contactListName");
		}
		String contactPurposeTypeId = "";
		if (UtilValidate.isNotEmpty(request
				.getParameter("contactPurposeTypeId"))) {
			contactPurposeTypeId = request.getParameter("contactPurposeTypeId");
		}
		String description = "";
		if (UtilValidate.isNotEmpty(request.getParameter("description"))) {
			description = request.getParameter("description");
		}
		String contactListId = "";
		if (UtilValidate.isNotEmpty(request.getParameter("contactListId"))) {
			contactListId = request.getParameter("contactListId");
			GenericValue gv = delegator.findOne("ContactList",
					UtilMisc.toMap("contactListId", contactListId), false);

			roleTypeId = gv.getString("roleTypeId");
		}
		if (UtilValidate.isNotEmpty(request.getParameter("roleTypeParameter"))) {
			roleTypeId = request.getParameter("roleTypeParameter");
		}

		try {

			Debug.log("jsonString " + jsonString);
			if (UtilValidate.isEmpty(jsonString)) {
				request.setAttribute("_ERROR_MESSAGE_", "Invalid Conditions");
				return "success";
			}
			jsonString = jsonString.replace("quot;", "\"");
			org.json.simple.JSONObject root = (org.json.simple.JSONObject) new JSONParser()
			.parse(jsonString);

			org.json.simple.JSONObject segmentJsonObject = (org.json.simple.JSONObject) root
					.get("segment");
			org.json.simple.JSONObject customJsonObject = (org.json.simple.JSONObject) root
					.get("custom");
			org.json.simple.JSONObject partyAddlJsonObject = (org.json.simple.JSONObject) root
					.get("partyAdditionalData");

			org.json.simple.JSONArray segmentRules = (org.json.simple.JSONArray) segmentJsonObject
					.get("rules");
			org.json.simple.JSONArray customRules=null;
			if(UtilValidate.isNotEmpty(customJsonObject)){
				customRules = (org.json.simple.JSONArray) customJsonObject.get("rules");
			}
			org.json.simple.JSONArray partyAddlRules =null;
			if(UtilValidate.isNotEmpty(partyAddlJsonObject)){
				partyAddlRules = (org.json.simple.JSONArray) partyAddlJsonObject
						.get("rules");
			}  
			String action = (String) segmentJsonObject.get("condition");
			System.out.println("action--------@M->" + action);
			// System.out.println("size of json Array--@M-->:"+segmentRules.size());
			// System.out.println(segmentRules.toString());

			if (request.getParameter("contactListId") == null) {
				RmsApiService.rmsMailingListRequest(request, response);
				String errorMessage = (String)request.getAttribute("_ERROR_MESSAGE_");
				Debug.log("errorMessage is " + errorMessage);
				if (UtilValidate.isNotEmpty(errorMessage)) {
					return "error";
				}
			}

			contactListId = (String) request.getAttribute("contactListId");
			if (contactListId == null)
				contactListId = request.getParameter("contactListId");

			GenericValue findContact = delegator.findOne("ContactList",
					UtilMisc.toMap("contactListId", contactListId), false);
			if (UtilValidate.isNotEmpty(findContact)) {
				findContact.put("criteria", jsonString);
				findContact.put("isProcessed", "");
				findContact.put("isSmartList", "Y");
				findContact.put("download", "");
				findContact.put("roleTypeId", roleTypeId);
				findContact.store();
			}
			boolean success = false;
			int seqValue = 1;

			// Remove the all cretiria for the particular contactList from the
			// contactListCriteria table
			List<GenericValue> contactListCriteriaList = delegator
					.findByAnd("ContactListCriteria",
							UtilMisc.toMap("contactListId", contactListId),
							null, false);
			if (UtilValidate.isNotEmpty(contactListCriteriaList)) {
				delegator.removeAll(contactListCriteriaList);
				isUpdate = true;
			}
			String segmentCondtion = (String) segmentJsonObject
					.get("condition");
			if (UtilValidate.isNotEmpty(segmentRules)) {
				for (int i = 0; i < segmentRules.size(); i++) {
					org.json.simple.JSONObject data = (org.json.simple.JSONObject) segmentRules
							.get(i);
					String id = (String) data.get("id");
					String field = (String) data.get("field");
					String criteriaValue = (String) data.get("value");
					String operator = (String) data.get("operator");
					String recurrenceModel = "REAL";
					String sequence = Integer.toString(seqValue);

					if (UtilValidate.isNotEmpty(sequence)) {
						sequence = StringUtils.leftPad(sequence, 4, '0');
					}
					System.out.println("sequence--->" + sequence);
					Map map = new HashMap();
					if (UtilValidate.isNotEmpty(contactListId)) {
						map.put("contactListId", contactListId);
						map.put("sequence", sequence);

						GenericValue ContactListCriteria = delegator.findOne(
								"ContactListCriteria", map, false);
						if (ContactListCriteria == null) {
							ContactListCriteria = delegator.makeValue(
									"ContactListCriteria", map);
							ContactListCriteria.put("fieldType", id);
							ContactListCriteria.put("criteria", field);
							ContactListCriteria.put("value", criteriaValue);
							ContactListCriteria.put("action", action);
							ContactListCriteria.put("recurrenceModel",
									recurrenceModel);
							ContactListCriteria.put("operator", operator);
							ContactListCriteria.put("operation",
									segmentCondtion);
							ContactListCriteria.create();
						}
					}
					seqValue++;
					success = true;
				}
			}
			// custom field
			String customFieldCondtion=null;
			if (UtilValidate.isNotEmpty(customJsonObject)){
				customFieldCondtion = (String) customJsonObject
						.get("condition");
			}
			if (UtilValidate.isNotEmpty(customRules)) {
				for (int i = 0; i < customRules.size(); i++) {
					org.json.simple.JSONObject data = (org.json.simple.JSONObject) customRules
							.get(i);
					String id = (String) data.get("id");
					String field = (String) data.get("field");
					String criteriaValue = (String) data.get("value");
					String operator = (String) data.get("operator");
					/*
					 * String value="";
					 * if(id.equalsIgnoreCase("CUSTOM_FIELD")||id
					 * .equalsIgnoreCase("PARTY_ADDITIONAL_DATA")) {
					 * value=data.getString("val"); }
					 */
					String recurrenceModel = "REAL";
					String sequence = Integer.toString(seqValue);

					if (UtilValidate.isNotEmpty(sequence)) {
						sequence = StringUtils.leftPad(sequence, 4, '0');
					}
					System.out.println("sequence--->" + sequence);
					Map map = new HashMap();
					if (UtilValidate.isNotEmpty(contactListId)) {
						map.put("contactListId", contactListId);
						map.put("sequence", sequence);

						GenericValue ContactListCriteria = delegator.findOne(
								"ContactListCriteria", map, false);
						Debug.log("ContactListCriteria is "
								+ ContactListCriteria);
						if (ContactListCriteria == null) {
							ContactListCriteria = delegator.makeValue(
									"ContactListCriteria", map);
							ContactListCriteria
							.put("fieldType", "CUSTOM_FIELD");
							ContactListCriteria.put("criteria", field);
							ContactListCriteria.put("value", criteriaValue);
							ContactListCriteria.put("action", action);
							ContactListCriteria.put("recurrenceModel",
									recurrenceModel);
							ContactListCriteria.put("operator", operator);
							ContactListCriteria.put("operation",
									customFieldCondtion);
							ContactListCriteria.create();
						}
					}
					seqValue++;
					success = true;
				}
			}
			System.out.println(partyAddlRules+"partyAddlRulespartyAddlRules");
			// party Additional data
			String partyAddlcondtion=null;
			if(UtilValidate.isNotEmpty(partyAddlJsonObject)){
				partyAddlcondtion = (String) partyAddlJsonObject
						.get("condition");
			}
			if (UtilValidate.isNotEmpty(partyAddlRules)) {
				for (int i = 0; i < partyAddlRules.size(); i++) {
					org.json.simple.JSONObject data = (org.json.simple.JSONObject) partyAddlRules
							.get(i);
					String id = (String) data.get("id");
					String field = (String) data.get("field");
					String criteriaValue = (String) data.get("value");
					String operator = (String) data.get("operator");
					/*
					 * String value="";
					 * if(id.equalsIgnoreCase("CUSTOM_FIELD")||id
					 * .equalsIgnoreCase("PARTY_ADDITIONAL_DATA")) {
					 * value=data.getString("val"); }
					 */
					String recurrenceModel = "REAL";
					String sequence = Integer.toString(seqValue);

					if (UtilValidate.isNotEmpty(sequence)) {
						sequence = StringUtils.leftPad(sequence, 4, '0');
					}
					System.out.println("sequence--->" + sequence);
					Map map = new HashMap();
					if (UtilValidate.isNotEmpty(contactListId)) {
						map.put("contactListId", contactListId);
						map.put("sequence", sequence);

						GenericValue ContactListCriteria = delegator.findOne(
								"ContactListCriteria", map, false);
						Debug.log("ContactListCriteria is "
								+ ContactListCriteria);
						if (ContactListCriteria == null) {
							ContactListCriteria = delegator.makeValue(
									"ContactListCriteria", map);
							ContactListCriteria.put("fieldType",
									"PARTY_ADDITIONAL_DATA");
							ContactListCriteria.put("criteria", field);
							ContactListCriteria.put("value", criteriaValue);
							ContactListCriteria.put("action", action);
							ContactListCriteria.put("recurrenceModel",
									recurrenceModel);
							ContactListCriteria.put("operator", operator);
							ContactListCriteria.put("operation",
									partyAddlcondtion);
							ContactListCriteria.create();
						}
					}
					seqValue++;
					success = true;
				}
			}
			if (success) {
				if (UtilValidate.isNotEmpty(marketingCampaignId)) {
					GenericValue marketingCampaignContactList = delegator
							.makeValue("MarketingCampaignContactList");
					String campaignListId = delegator
							.getNextSeqId("MarketingCampaignContactList");
					marketingCampaignContactList.set("campaignListId",
							campaignListId);
					marketingCampaignContactList.set("marketingCampaignId",
							marketingCampaignId);
					marketingCampaignContactList.set("contactListId",
							contactListId);
					marketingCampaignContactList.set("fromDate",
							UtilDateTime.nowTimestamp());

					delegator.create(marketingCampaignContactList);
					request.setAttribute("marketingCampaignId",
							marketingCampaignId);
					return "viewcampaign";
				}
			}

		} catch (Exception e) {
			// org.json.JSONArray sportsArray;
			e.printStackTrace();
			System.out.println("Error msg--->" + e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",
					"Error Occured While Insert the Criteria.");
			return "error";
		}
		Debug.log("RETURN SUCCESS");
		if (isUpdate) {
			request.setAttribute("updateFlag", "Y");
		} else {
			request.setAttribute("updateFlag", "N");
		}
		request.setAttribute("_EVENT_MESSAGE_",
				"Smart List Generated successfully");
		request.setAttribute("contactListId", contactListId);
		request.setAttribute("id", roleTypeId);
		session.setAttribute("id", roleTypeId);
		return "success";
	}
}
