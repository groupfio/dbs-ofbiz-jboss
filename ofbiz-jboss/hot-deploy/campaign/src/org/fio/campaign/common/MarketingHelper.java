package org.fio.campaign.common;

import java.util.List;
import java.util.Set;

import javolution.util.FastList;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;

public class MarketingHelper {
	private MarketingHelper() { }

    /** Counts the number of active, ACCEPTED members in a contact list with the given condition. */
    public static long countContactListMembers(String contactListId, EntityCondition condition, Delegator delegator) throws GenericEntityException {
        
    	List<EntityCondition> conditionList = FastList.newInstance();
    	conditionList.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
    	conditionList.add(EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, "CLPT_ACCEPTED"));
/*    	List<EntityCondition> conditionList = UtilMisc.toList(
                    EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                    EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, "CLPT_ACCEPTED"),
                    EntityUtil.getFilterByDateExpr(),
                    EntityUtil.getFilterByDateExpr("contactMechFromDate", "contactMechThruDate")
                    );*/
        if (condition != null) {
            conditionList.add(condition);
        }
        EntityCondition conditions = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
        GenericValue value = EntityUtil.getFirst(delegator.findList("ContactListPartyCount",conditions, null, UtilMisc.toList("partyId"), null, false));
        
        if (value == null || value.get("partyId") == null) {
            return 0;
        }
        return value.getLong("partyId").intValue();
    }

    /** Count all active, ACCEPTED members of a contact list. */
    public static long countContactListMembers(String contactListId, Delegator delegator) throws GenericEntityException {
        return countContactListMembers(contactListId, null, delegator);
    }

    /** Count all active, ACCEPTED members of a contact list in a given countryGeoId. */
    public static long countContactListMembersByCountry(String contactListId, String countryGeoId, Delegator delegator) throws GenericEntityException {
        EntityCondition condition = EntityCondition.makeCondition("countryGeoId", EntityOperator.EQUALS, countryGeoId);
        return countContactListMembers(contactListId, condition, delegator);
    }
    
    public static long countAccountListMembers(String contactListId, Delegator delegator) throws GenericEntityException {
    	EntityFindOptions findOptions= new EntityFindOptions();
    	findOptions.setDistinct(true);
    	Set <String> setParam = UtilMisc.toSet("acctPartyId");
    
    	List conditions = FastList.newInstance();
		EntityCondition roleTypeCondition1 = EntityCondition.makeCondition(UtilMisc.toList(
				EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
				EntityCondition.makeCondition("acctPartyId", EntityOperator.NOT_EQUAL, null)
				), EntityOperator.AND);
		conditions.add(roleTypeCondition1);
    	
		EntityCondition accountCountCond = EntityCondition.makeCondition(EntityCondition.makeCondition(conditions, EntityOperator.AND));
		List<GenericValue>  accountCountList = delegator.findList("ContactListParty", accountCountCond,setParam,UtilMisc.toList("acctPartyId"), findOptions, false);
		Long accountCount=new Long(0);
		int accountCountInt=0;
		 if(accountCountList!=null && accountCountList.size()>0){
		 accountCountInt =accountCountList.size();
		 accountCount=new Long(accountCountInt);
		 }
		/*Long accountCount = delegator.findCountByCondition("ContactListParty",
				EntityCondition.makeCondition(accountCountCond,EntityOperator.AND), null,
				findOptions);*/
    	
        return accountCount;
    }

   
}
