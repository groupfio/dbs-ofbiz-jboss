package org.fio.campaign.common;

import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityFindOptions;

import javolution.util.FastMap;

public class UtilCommon {
	public static final EntityFindOptions DISTINCT_READ_OPTIONS = new EntityFindOptions(true, EntityFindOptions.TYPE_SCROLL_INSENSITIVE, EntityFindOptions.CONCUR_READ_ONLY, true);

	public static final EntityFindOptions READ_ONLY_OPTIONS = new EntityFindOptions(true, EntityFindOptions.TYPE_SCROLL_INSENSITIVE, EntityFindOptions.CONCUR_READ_ONLY, false);


	public static GenericValue getUserLogin(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (GenericValue) session.getAttribute("userLogin");
	}
	public static Map<String, ?> makeHistoryEntry(String text, String view, List<String> override) {
		Map<String, Object> entry = FastMap.<String, Object>newInstance();
		if (text == null) {
			throw new IllegalArgumentException("Argument \"text\" can't be null");
		}
		entry.put("text", text);
		if (view != null) {
			entry.put("view", view);
		}
		if (override != null) {
			entry.put("override", override);
		}
		return entry;
	}

	/**
	 * Prepares an History entry for later processing.
	 * @param text the text that should be displayed as the label of the entry
	 * @param view the view name
	 * @return the history entry <code>Map</code>
	 */
	public static Map<String, ?> makeHistoryEntry(String text, String view) {
		return makeHistoryEntry(text, view, null);
	}

	/**
	 * Prepares an History entry for later processing.
	 * @param text the text that should be displayed as the label of the entry
	 * @return the history entry <code>Map</code>
	 */
	public static Map<String, ?> makeHistoryEntry(String text) {
		return makeHistoryEntry(text, null, null);
	}

	public static Locale getLocale(HttpServletRequest request) {
		return UtilHttp.getLocale(request);
	}
    /**
     * Checks if a request has an error set.
     * @param request a <code>HttpServletRequest</code> value
     * @return a <code>Boolean</code> value
     */
    @SuppressWarnings("unchecked")
    public static Boolean hasError(HttpServletRequest request) {
        Enumeration<String> attrs = request.getAttributeNames();
        while (attrs.hasMoreElements()) {
            String a = attrs.nextElement();
            if ("_ERROR_MESSAGE_LIST_".equals(a) || "_ERROR_MESSAGE_".equals(a)) {
                return true;
            }
        }
        return false;
    }


}
