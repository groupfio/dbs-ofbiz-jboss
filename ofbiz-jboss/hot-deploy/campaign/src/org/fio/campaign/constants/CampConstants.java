package org.fio.campaign.constants;

public class CampConstants {

    public static final String configResource = "campaign";
    public static final String uiLabelMap = "campaignUiLabels";
    
    public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	

    /**
     * ERRORTYPE constant values.
     */
    public static final class ErrorLogs {
        private ErrorLogs() { }
        public static final String ALREADYADDED = "E131";//Customer already added in List
        public static final String SOLICITATIONCHECK = "E132";//Customer contact solicitation is not Active
        public static final String NOEAMIL = "E133";//Customer Email Not Available
        public static final String PARTYDISABLED = "E134";//Customer Disabled
        public static final String NOTAVAILABLE = "E135";//Customer Not Available
        public static final String ROLENOTMATCH = "E136";//Role Not Available
        public static final String NOACCOUNTASSOC = "E137";// No Assoc Available
        public static final String NOCONTACTASSOC = "E138";// No Contact Available
        public static final String NOUNIQUECONTACT = "E139";// No Unique Contact Available
        public static final String DEDUPEDLIST = "E140";// Deduped List
        public static final String DELIVERABLECHECK = "E141";// Deliverable flag check
        public static final String FLAGCHECKCOUNT = "E142"; // total deliverable and solicitation 
    }
    public static final class GroupType {
        private GroupType() { }
        public static final String CUSTOM_FIELD = "CUSTOM_FIELD";
        public static final String SEGMENTATION = "SEGMENTATION";
        public static final String ATTRIBUTE = "ATTRIBUTE";
        public static final String ECONOMIC_METRIC = "ECONOMIC_METRIC";
    }
}
