package org.fio.campaign.outboundCallList;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

public class CallList {
    private static final String MODULE = CallList.class.getName();
    
    public static Map < String, Object > createCsrList(DispatchContext ctx, Map < String, ? extends Object > context) {
        Map < String, Object > result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Locale locale = (Locale) context.get("locale");
        List<String> partyIdList = UtilGenerics.checkList(context.get("partyId"));
        String marketingCampaignId = (String) context.get("marketingCampaignId");
        String errMsg = null;
        try {
            if(UtilValidate.isNotEmpty(marketingCampaignId) && partyIdList != null && partyIdList.size() > 0) {
            for(String partyId: partyIdList) {
            GenericValue campExist = delegator.findOne("CampaignCsrAssoc", UtilMisc.toMap("csrPartyId", partyId, "campaignId", marketingCampaignId), false);
            if (campExist == null || campExist.size() < 1) {
                GenericValue CampaignCsrAssoc = delegator.makeValue("CampaignCsrAssoc");
                CampaignCsrAssoc.put("campaignId", marketingCampaignId);
                CampaignCsrAssoc.put("csrPartyId", partyId);
                CampaignCsrAssoc.create();
            }
            }
            }
        } catch (GenericEntityException e) {
            String errMsgs = "Failure in inserting record" + e.toString();
            Debug.logError(e, errMsgs, MODULE);
            return ServiceUtil.returnError(errMsgs);
        }
        return ServiceUtil.returnSuccess("RM assigned successfully");
    }
    
    public static String removeCsrCampagin(HttpServletRequest request,HttpServletResponse response){
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        //Locale locale = (Locale) context.get("locale");
        String marketingCampaignId=request.getParameter("marketingCampaignId");
        String partyId=request.getParameter("partyId");
        String errMsg = null;
        try {
            GenericValue campaignCsrAssoc = delegator.findOne("CampaignCsrAssoc", UtilMisc.toMap("csrPartyId", partyId, "campaignId", marketingCampaignId), false);
            if (campaignCsrAssoc != null && campaignCsrAssoc.size() > 0) {
                campaignCsrAssoc.remove();
            }
        } catch (GenericEntityException e) {
            String errMsgs = "Failure in inserting record" + e.toString();
            Debug.logError(e, errMsgs, MODULE);
            request.setAttribute("_ERROR_MESSAGE_",errMsgs);
            return "error";
        }
        request.setAttribute("activeTab", "assignRM");
        request.setAttribute("marketingCampaignId", marketingCampaignId);
        request.setAttribute("_EVENT_MESSAGE_","RM Removed successfully");
        return "success";
    }
    
    public static Map < String, Object > createOrUpdateOutboundCallList(DispatchContext dctx, Map < ? , ? > context) {
        Map < String, Object > result = ServiceUtil.returnSuccess();
        String campaignId = (String) context.get("campaignId");
        String csrPartyId = (String) context.get("rmUserLoginId");
        String callStatus = (String) context.get("callStatus");
        String noOfDaysSinceLastCall = (String) context.get("noOfDaysSinceLastCall");
        String callBackDate = (String) context.get("callBackDate");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String defaultTimeZoneId = (String) context.get("defaultTimeZoneId");
        Delegator delegator = dctx.getDelegator();
        GenericValue outboundCallListDetail = null;
        if (UtilValidate.isNotEmpty(userLogin)) {
            try {
                String userLoginId = userLogin.getString("userLoginId");
                if(UtilValidate.isNotEmpty(userLoginId)) {
                    outboundCallListDetail = delegator.findOne("OutboundCallListDetails", UtilMisc.toMap("userLoginId", userLoginId), false);
                    if((UtilValidate.isNotEmpty(campaignId) || UtilValidate.isNotEmpty(csrPartyId) || UtilValidate.isNotEmpty(callStatus) || UtilValidate.isNotEmpty(noOfDaysSinceLastCall) || UtilValidate.isNotEmpty(callBackDate) || UtilValidate.isNotEmpty(defaultTimeZoneId))) {
                        if (UtilValidate.isNotEmpty(outboundCallListDetail)) {
                            outboundCallListDetail.put("campaignId", campaignId);
                            outboundCallListDetail.put("callStatus", callStatus);
                            outboundCallListDetail.put("noOfDaysSinceLastCall", noOfDaysSinceLastCall);
                            outboundCallListDetail.put("callBackDate", callBackDate);
                            outboundCallListDetail.put("csrPartyId", csrPartyId);
                            outboundCallListDetail.put("localTimeZone", defaultTimeZoneId);
                            outboundCallListDetail.store();
                        } else {
                            outboundCallListDetail = delegator.makeValue("OutboundCallListDetails");
                            outboundCallListDetail.set("campaignId", campaignId);
                            outboundCallListDetail.set("callStatus", callStatus);
                            outboundCallListDetail.set("noOfDaysSinceLastCall", noOfDaysSinceLastCall);
                            outboundCallListDetail.set("callBackDate", callBackDate);
                            outboundCallListDetail.set("csrPartyId", csrPartyId);
                            outboundCallListDetail.set("userLoginId", userLoginId);
                            outboundCallListDetail.put("localTimeZone", defaultTimeZoneId);
                            outboundCallListDetail.create();
                        }
                    } else if(UtilValidate.isNotEmpty(outboundCallListDetail)){
                        delegator.removeByPrimaryKey(outboundCallListDetail.getPrimaryKey());
                    }
                }
            } catch (Exception e) {
                Debug.logError("Exception in createOrUpdateOutboundCallList", MODULE);
            }
        }
        return result;
    }
    
    public static String addCallCountSegment(String partyId, Delegator delegator){
        SQLProcessor sqlProcessor = null;
        ResultSet rs = null;
        try{
            int countSeg= 0;
            int nextCallNum= 0;
            //int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            String groupId="CUST_CALL_NUM";
            String maxCountSeg=null;
            //ResultSet rs = null;
            String selGroup = "org.ofbiz";
            GenericHelperInfo helperInfo = delegator.getGroupHelperInfo(selGroup);
            GenericValue maxCallNumExisting = delegator.findOne("PretailLoyaltyGlobalParameters",UtilMisc.toMap("parameterId","CUST_CALL_NUM"),false);
            String maxCallNum="";
            String callCustElg ="";
            int maxCallCust = 0;
            if(UtilValidate.isNotEmpty(maxCallNumExisting)){
                maxCallNum = maxCallNumExisting.getString("value");
                if(UtilValidate.isNotEmpty(maxCallNum)){
                    maxCallCust = Integer.parseInt(maxCallNum);
                }
            }
            GenericValue callNumberClassification = null;
           /* List callSegmentType = delegator.findList("PartyClassificationGroup", EntityCondition.makeCondition("partyClassificationTypeId", EntityOperator.EQUALS, partyClassificationType), UtilMisc.toSet("partyClassificationGroupId"), null, null, true);
            List<String> segmentIds = EntityUtil.getFieldListFromEntityList(callSegmentType, "partyClassificationGroupId", true);*/
            List callSegmentType = delegator.findList("CustomField", EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupId), UtilMisc.toSet("customFieldId"), null, null, true);
            List<String> segmentIds = EntityUtil.getFieldListFromEntityList(callSegmentType, "customFieldId", true);
            Debug.log("===segmentIds==="+segmentIds);
            EntityCondition partyclassificationGroup = null;
            if(UtilValidate.isNotEmpty(callSegmentType)){
                partyclassificationGroup = EntityCondition.makeCondition(UtilMisc.toList(
                    EntityCondition.makeCondition("partyId",EntityOperator.EQUALS,partyId),
                    EntityCondition.makeCondition("customFieldId",EntityOperator.IN,segmentIds)),
                    EntityOperator.AND);
                List<GenericValue> partyClassification = delegator.findList("CustomFieldPartyClassification",partyclassificationGroup,null,null,null,false);
                if(UtilValidate.isNotEmpty(partyClassification)){
                    callNumberClassification = partyClassification.get(0);
                    if(UtilValidate.isNotEmpty(callNumberClassification)){
                        String callNumGroupId =  callNumberClassification.getString("customFieldId");
                        if(UtilValidate.isNotEmpty(callNumGroupId) && !callNumGroupId.contains("ELG") && !callNumGroupId.contains("NON")){
                            String[] splitCalNum = callNumGroupId.split("_");
                            String calNumCount = splitCalNum[2];
                            countSeg = Integer.parseInt(calNumCount);
                            nextCallNum = countSeg+1;
                        }
                    }
                }
            }
            Debug.log("callNumberClassification==="+callNumberClassification);
            if(nextCallNum == 0){
                nextCallNum = 1;
            }
            List<GenericValue> callSegmentNonElgCheck = delegator.findByAnd("CustomFieldPartyClassification",UtilMisc.toMap("partyId",partyId,"customFieldId","MAX_CUST_NON_ELG"),null,false);
            if(UtilValidate.isEmpty(callSegmentNonElgCheck)){
                if(nextCallNum <= maxCallCust){
                    maxCountSeg="CUST_CALL"+"_"+0+nextCallNum;
                    List<GenericValue> checkPartyClasification = delegator.findByAnd("CustomFieldPartyClassification",UtilMisc.toMap("partyId",partyId),null,false);
                    if(UtilValidate.isNotEmpty(callNumberClassification)){
                        callNumberClassification.remove();
                        Debug.log("callNumberClassification==="+callNumberClassification);
                    }
                    GenericValue addCountSeg=delegator.makeValue("CustomFieldPartyClassification");
                    addCountSeg.put("partyId",partyId );
                    addCountSeg.put("customFieldId",maxCountSeg);
                    addCountSeg.put("groupId",groupId );
                    addCountSeg.put("inceptionDate", UtilDateTime.nowTimestamp());
                    addCountSeg.create();
                }
                /*List<GenericValue> callSegmentElg = delegator.findList("PartyClassification", EntityCondition.makeCondition("partyClassificationGroupId", EntityOperator.IN, UtilMisc.toList("MAX_CUST_ELG","MAX_CUST_NON_ELG")), UtilMisc.toSet("partyClassificationGroupId"), null, null, true);
                GenericValue callNumElg = null;
                if(UtilValidate.isNotEmpty(callSegmentElg)){
                    delegator.removeAll(callSegmentElg);
                }*/
                if(nextCallNum < maxCallCust){
                    callCustElg = "MAX_CUST_ELG";
                    List<GenericValue> callSegmentElg = delegator.findByAnd("CustomFieldPartyClassification",UtilMisc.toMap("partyId",partyId,"customFieldId",callCustElg),null,false);
                    if(UtilValidate.isEmpty(callSegmentElg)){
                        GenericValue addHHSegmentElg=delegator.makeValue("CustomFieldPartyClassification");
                        addHHSegmentElg.put("partyId",partyId );
                        addHHSegmentElg.put("customFieldId",callCustElg);
                        addHHSegmentElg.put("groupId",groupId );
                        addHHSegmentElg.put("inceptionDate", UtilDateTime.nowTimestamp());
                        addHHSegmentElg.create();
                    }
                }else{
                    String callCustNonElg = "MAX_CUST_NON_ELG";
                    List<GenericValue> callSegmentElgDel = delegator.findByAnd("CustomFieldPartyClassification",UtilMisc.toMap("partyId",partyId,"customFieldId","MAX_CUST_ELG"),null,false);
                    if(UtilValidate.isNotEmpty(callSegmentElgDel)){
                        delegator.removeAll(callSegmentElgDel);
                    }
                    List<GenericValue> callSegmentNonElg = delegator.findByAnd("CustomFieldPartyClassification",UtilMisc.toMap("partyId",partyId,"customFieldId",callCustNonElg),null,false);
                    if(UtilValidate.isEmpty(callSegmentNonElg)){
                      GenericValue addHHSegmentElg=delegator.makeValue("CustomFieldPartyClassification");
                      addHHSegmentElg.put("partyId",partyId );
                      addHHSegmentElg.put("customFieldId",callCustNonElg);
                      addHHSegmentElg.put("groupId",groupId );
                      addHHSegmentElg.put("inceptionDate", UtilDateTime.nowTimestamp());
                      addHHSegmentElg.create();
                   }
               }
            }
            
        }catch(Exception e){
            Debug.log("Error in add call count segment "+e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                 }
            } catch (Exception e) {
                Debug.log("Error in close connection "+e.getMessage());
            }
        }
        return "error";
    }
    
    public static Map<String, Object> rmPartyReassign(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        String toRM = (String) context.get("toRM");
        List<String> partyLists =  (List<String>) context.get("partyLists");
        List partyLi = Arrays.asList(partyLists.toArray());
        Debug.log("===partyLists==="+partyLists.toArray());
        Map result = ServiceUtil.returnSuccess("RM Reassign Process Successfully Completed");
        try {
            if(partyLi.size()>0) {
                for(Object partyLis : partyLi) {
                    String dataObj = partyLis.toString();
                    String[] splitLi = dataObj.split(",");
                    String[] splitListData = splitLi;
                    for(Object splitData : splitListData) {
                       String splitDataObj = splitData.toString();
                       String[] splitDataList = splitDataObj.split("_");
                       String partyIds = splitDataList[0];
                       String campaignIds = splitDataList[1];
                       List<EntityCondition> callRecordMasterconditionsList = new ArrayList<EntityCondition>();
                       callRecordMasterconditionsList.add(EntityCondition.makeCondition(EntityOperator.AND,
                           EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, campaignIds)
                           ));
                       callRecordMasterconditionsList.add(EntityCondition.makeCondition(EntityOperator.AND,
                               EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyIds)
                               ));
                       callRecordMasterconditionsList.add(EntityCondition.makeCondition(EntityOperator.OR,
                           EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, null),
                           EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, ""),
                           EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, "N")));
                       
                       EntityCondition callRecordMastercondition = EntityCondition.makeCondition(callRecordMasterconditionsList, EntityOperator.AND); 
                       List<GenericValue> callRecordMasterList = delegator.findList("CallRecordMaster", callRecordMastercondition, null, null, null, false);
                       if(callRecordMasterList != null && callRecordMasterList.size() > 0) {
                           for(GenericValue callRecordMaster : callRecordMasterList) {
                               callRecordMaster.put("csrPartyId", toRM);
                               callRecordMaster.store();
                           }
                       }
                    }
                }
            }
        } catch(Exception e) {
            Debug.logError("CSR Reassign======"+e.getMessage(), MODULE);
        }
        return result;
    }
    
    public static String callStatusUpdate(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        String marketingCampaignId = request.getParameter("marketingCampaignId");
        String callStatus = request.getParameter("callStatus");
        String callBackDate = request.getParameter("callBackDate");
        String partyId = request.getParameter("partyId");
        String contactListId = request.getParameter("contactListId");
        if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(contactListId)){
            try {
                    Map<String, Object> callRecordList = new HashMap<String,Object>();
                    callRecordList.put("partyId",partyId);
                    callRecordList.put("marketingCampaignId",marketingCampaignId);
                    callRecordList.put("contactListId",contactListId);
                    callRecordList.put("callStatus",callStatus);
                    callRecordList.put("callBackDate",callBackDate);
                    Debug.log("callRecordList==="+callRecordList);
                    Map<String, Object> recordResult  = dispatcher.runSync("callListStatus", callRecordList);
                    Debug.log("recordResult==="+ServiceUtil.isSuccess(recordResult));
            } catch(Exception e) {
                Debug.logError("Call Status Update"+e.getMessage(), MODULE);
            }
        
        }
        return "Success";
    }
    
    public static Map < String, Object > callListStatus(DispatchContext dctx, Map < ? , ? > context) {
    	
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
        Map < String, Object > result = ServiceUtil.returnSuccess();
        String partyId = (String) context.get("partyId");
        String marketingCampaignId = (String) context.get("marketingCampaignId");
        String contactListId = (String) context.get("contactListId");
        
        String callStatus = (String) context.get("callStatus");
        String callSubStatus = (String) context.get("callSubStatus");
        
        String callId = (String) context.get("callId");
        String noteId = (String) context.get("noteId");
        String callBackDate = (String) context.get("callBackDate");
        Timestamp callEndTime = null;
        Timestamp callStartTime = null;
        String duration = null;
        Delegator delegator = dctx.getDelegator();
        GenericValue callRecordMaster = null;
        List < GenericValue > callLogHistory = null;
        if (UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(contactListId) && (UtilValidate.isNotEmpty(callBackDate) || UtilValidate.isNotEmpty(callStatus))) {
            try {
                String callNumber = "";
                String callFinished = "N";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String lastContactDate = sdf.format(new Date());
                //String lastContactDate =  null;
                /*callRecordMaster = delegator.findByPrimaryKey("CallRecordMaster", UtilMisc.toMap("partyId", partyId, "marketingCampaignId", marketingCampaignId, "contactListId", contactListId));*/

                List < EntityCondition > callRecordMasterconditionsList = new ArrayList < EntityCondition > ();
                callRecordMasterconditionsList.add(EntityCondition.makeCondition(EntityOperator.AND,
                    EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId),
                    EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                    EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)));
                callRecordMasterconditionsList.add(EntityCondition.makeCondition(EntityOperator.OR,
                    EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, null),
                    EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, ""),
                    EntityCondition.makeCondition("callFinished", EntityOperator.EQUALS, "N")));

                EntityCondition callRecordMastercondition = EntityCondition.makeCondition(callRecordMasterconditionsList, EntityOperator.AND);
                callRecordMaster = EntityUtil.getFirst(delegator.findList("CallRecordMaster", callRecordMastercondition, null, UtilMisc.toList("lastUpdatedStamp DESC"), null, false));
                
                if (UtilValidate.isEmpty(callRecordMaster)) {
                	
                	callRecordMaster = delegator.makeValue("CallRecordMaster");
                	callRecordMaster.set("seqId", delegator.getNextSeqId("CallRecordMaster"));
                	callRecordMaster.set("marketingCampaignId", marketingCampaignId);
                	callRecordMaster.set("contactListId", contactListId);
                	callRecordMaster.set("partyId", partyId);
                	
                	GenericValue supplementalData = EntityUtil.getFirst( delegator.findByAnd("PartySupplementalData",UtilMisc.toMap("partyId", partyId), null, false));
                	if (UtilValidate.isNotEmpty(supplementalData)) {
                		callRecordMaster.set("firstName", supplementalData.getString("partyFirstName"));
                		callRecordMaster.set("lastName", supplementalData.getString("partyLastName"));
                		callRecordMaster.set("createdDate", UtilDateTime.nowTimestamp());
                		callRecordMaster.set("areaCode", supplementalData.getString("postalCode"));
                		callRecordMaster.set("phoneNumber", supplementalData.getString("primaryPhoneNumber"));
                		callRecordMaster.set("city", supplementalData.getString("city"));
                		callRecordMaster.set("state", supplementalData.getString("stateProvinceGeoId"));
                	}
                	callRecordMaster.create();
                }
                
                if (UtilValidate.isNotEmpty(callRecordMaster)) {
                    String csrPartyId = callRecordMaster.getString("csrPartyId");
                    String seqId = callRecordMaster.getString("seqId");
                    if (UtilValidate.isNotEmpty(callStatus)) {
                        GenericValue enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId", callStatus), false);
                        if (UtilValidate.isNotEmpty(enumeration)) {
                            callNumber = enumeration.getString("sequenceId");
                            String description = enumeration.getString("description");
                            if (UtilValidate.isNotEmpty(description)) {
                                if (description.contains("Not Interested") || description.contains("DNC") || description.contains("Purchased")) {
                                    callFinished = "Y";
                                }
                            }
                        }
                    }

                    if (UtilValidate.isNotEmpty(callRecordMaster)) {
                        String lastCallStatusId = callRecordMaster.getString("lastCallStatusId");
                        callRecordMaster.set("lastCallStatusId", UtilValidate.isNotEmpty(callSubStatus) ? callSubStatus : callStatus);
                        callRecordMaster.set("lastContactDate", java.sql.Date.valueOf(lastContactDate));
                        if (UtilValidate.isNotEmpty(callBackDate)) {
                            Date callBackDate1 = new SimpleDateFormat("dd-MM-yyyy").parse(callBackDate);
                            callBackDate = sdf.format(callBackDate1);
                            callRecordMaster.set("callBackDate", java.sql.Date.valueOf(callBackDate));
                        } else {
                            callRecordMaster.set("callBackDate", null);
                        }
                        if (UtilValidate.isNotEmpty(callNumber)) {
                            callRecordMaster.set("callNumber", new BigDecimal(callNumber));
                        }
                        callRecordMaster.set("callFinished", callFinished);
                        callRecordMaster.store();
                        //Process to store call Duration details in CallListRecord 
                        EntityCondition callLogHistoryconditionsList = EntityCondition.makeCondition(EntityOperator.AND,
                            EntityCondition.makeCondition("callRecMastId", EntityOperator.EQUALS, seqId),
                            EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId),
                            EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                            EntityCondition.makeCondition("custPartyId", EntityOperator.EQUALS, partyId),
                            EntityCondition.makeCondition("callStoreStatus", EntityOperator.EQUALS, null));
                        callLogHistory = delegator.findList("CallLogHistory", callLogHistoryconditionsList, null, null, null, false);

                        if (UtilValidate.isNotEmpty(callLogHistory)) {
                            long callduration = 0;
                            for (GenericValue callLogHistoryValue: callLogHistory) {
                                String callLogId = callLogHistoryValue.getString("seqId");
                                String loginPartyId = callLogHistoryValue.getString("partyId");
                                callStartTime = callLogHistoryValue.getTimestamp("callStartTime");
                                callEndTime = callLogHistoryValue.getTimestamp("callEndTime");

                                if (UtilValidate.isNotEmpty(callStartTime) && UtilValidate.isNotEmpty(callEndTime)) {
                                    callduration = callduration + (callEndTime.getTime() - callStartTime.getTime());
                                }

                                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                                duration = timeFormat.format(new Date(callduration));

                                GenericValue updatecallLogHistory = delegator.findOne("CallLogHistory", UtilMisc.toMap("seqId", callLogId, "partyId", loginPartyId), false);
                                if (UtilValidate.isNotEmpty(updatecallLogHistory)) {
                                    updatecallLogHistory.set("callStoreStatus", "Y");
                                    updatecallLogHistory.store();
                                }
                            }
                        }
                        
                        if (UtilValidate.isNotEmpty(callStatus)) {
                            if (!callStatus.equals(lastCallStatusId)) {
                                GenericValue callRecordDetails = delegator.makeValue("CallRecordDetails");
                                callRecordDetails.set("seqId", delegator.getNextSeqId("CallRecordDetails"));
                                callRecordDetails.set("partyId", partyId);
                                callRecordDetails.set("contactListId", contactListId);
                                callRecordDetails.set("marketingCampaignId", marketingCampaignId);
                                
                                callRecordDetails.set("callStatusId", callStatus);
                                callRecordDetails.set("callSubStatusId", callSubStatus);
                                
                                callRecordDetails.set("callId", callId);
                                if (UtilValidate.isNotEmpty(callNumber)) {
                                    callRecordDetails.set("callNumber", new BigDecimal(callNumber));
                                } else {
                                    callRecordDetails.set("callNumber", null);
                                }
                                callRecordDetails.set("noteId", noteId);
                                callRecordDetails.set("csrPartyId", UtilValidate.isNotEmpty(csrPartyId) ? csrPartyId : userLogin.getString("partyId"));
                                if (UtilValidate.isNotEmpty(callEndTime)) {
                                    callRecordDetails.put("callEndTime", callEndTime);
                                }
                                if (UtilValidate.isNotEmpty(callStartTime)) {
                                    callRecordDetails.put("callStartTime", callStartTime);
                                }
                                if (UtilValidate.isNotEmpty(duration)) {
                                    callRecordDetails.put("callDuration", duration);
                                }
                                callRecordDetails.create();
                            }
                        }
                    }
                    if (callFinished == "Y") {
                    	System.out.println("callFinished"+callFinished);
                        Debug.log("calling segment method");
                        addCallCountSegment(partyId,delegator);
                        Debug.log("Cll segment Has Created");
                        if (UtilValidate.isNotEmpty(callBackDate) && UtilValidate.isNotEmpty(partyId)) {
                            GenericValue person = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);
                            if (UtilValidate.isNotEmpty(person)) {
                                person.set("callBackDate", callBackDate);
                                person.store();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Debug.logError("Exception in createOrUpdateOutboundCallList", MODULE);
            }
        }
        return result;
    }
}