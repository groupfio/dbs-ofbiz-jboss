package org.fio.campaign.job;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javolution.util.FastList;

import org.apache.commons.io.FileUtils;
import org.fio.campaign.constants.CampConstants;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;
public class SmartListFileReadJob extends Thread{

	Delegator delegator;
	LocalDispatcher dispatcher;
	String contactListId;
	String contactMechType;
	String dirPath;
	String roleTypeId;
	String emailType;
	String debug = SmartListFileReadJob.class.getName();
	public void run(){

		try{

			Debug.logInfo("SmartListFileReadJob started ",debug );
			//GenericValue contactListAttributeGv = delegator.findByPrimaryKey("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_NAME"));
			GenericValue contactListAttributeGv = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","UPLOAD_FILE_NAME").queryOne();
			GenericValue isValidateList = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","IS_VALIDATE").queryOne();
			String isValidate= isValidateList.getString("attrValue");
			String filepath = contactListAttributeGv.getString("attrValue");
			File file = new File(dirPath+filepath);
			Debug.logInfo("dirPath " +dirPath,dirPath );
			Debug.logInfo("file " +file,dirPath );
			Debug.logInfo("filepath " +filepath,dirPath );
			String[] data = null;
			if(file!=null){
				String fileData = FileUtils.readFileToString(file,"UTF-8");
				data = org.apache.commons.lang.StringUtils.split(fileData, "\n");
			}
			int successCount = 0;
			int partyDisabledCount = 0;
			int dedupCount = 0;
			int noPartyCount = 0;
			int noRoleCount = 0;
			int noDefault = 0;
			int noUniqueDefault = 0;
			int noAccount=0;
			int noFlagCount=0;
			Timestamp fromDate = UtilDateTime.nowTimestamp();
			if(data!=null){


				for(int i=1;i<data.length;i++){
                    String rmId=null;
					String partyId = data[i];
					partyId =partyId.replace("\r","");
					partyId =partyId.replace(",","");
					GenericValue contactListErroLog = delegator.makeValue("FioContactListErrorLog");
					contactListErroLog.set("contactListId",contactListId);

					GenericValue partyAvailable = EntityQuery.use(delegator).from("Party").where("partyId",partyId).queryOne();

					if(UtilValidate.isNotEmpty(partyAvailable)){
						EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
								EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
								EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED")), EntityOperator.AND);
						List<GenericValue> partyEnable = EntityQuery.use(delegator).from("Party").where(condition).queryList();
						if(UtilValidate.isNotEmpty(partyEnable)){

							String contactMechId = null;
							String contactMechIdAcc = null;
							String partyAccId=null;
							if("Y".equals(isValidate)){
								if(roleTypeId!=null){
									List roleconditionList = FastList.newInstance();
									roleconditionList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId));
									roleconditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, roleTypeId)));
									EntityCondition roleCondition = EntityCondition.makeCondition(EntityCondition.makeCondition(roleconditionList, EntityOperator.AND));
									List rolePartyLists = delegator.findList("PartyRole", roleCondition, null, null,null,false);
									if(rolePartyLists==null || rolePartyLists.size()==0){
										noRoleCount++;
										
										EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
						                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
						                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
						                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

						                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();
										    if(responsibleFor!=null  && responsibleFor.size()>0){
										    	rmId=responsibleFor.get(0).getString("partyIdTo");
										    	
										    }
										contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
										contactListErroLog.set("partyId",partyId);
										contactListErroLog.set("status","ERROR");
										contactListErroLog.set("errorLog",CampConstants.ErrorLogs.ROLENOTMATCH);
										contactListErroLog.set("csrPartyId",rmId);
										contactListErroLog.set("createdTime", fromDate);
										contactListErroLog.create();
										continue;
									}
									if("ACCOUNT".equals(roleTypeId)){
										
										String partyIdContact=null;
										Set <String> findOptions = UtilMisc.toSet("partyIdTo");
										findOptions.add("partyIdFrom");
										EntityCondition checkAccountExistCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
												EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, partyId)
												), EntityOperator.AND);
										
										Long checkAccountExist = delegator.findCountByCondition("ContactListParty",checkAccountExistCondition, null,null);
										if(checkAccountExist > 0){
											dedupCount++;
											EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
							                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
							                        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeId),
							                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
							                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

							                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();
							                    if(responsibleFor!=null  && responsibleFor.size()>0){
											    	rmId=responsibleFor.get(0).getString("partyIdTo");
											    	
											    }
							                contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
											contactListErroLog.set("partyId",partyId);
											contactListErroLog.set("status","ERROR");
											contactListErroLog.set("errorLog",CampConstants.ErrorLogs.DEDUPEDLIST);
											contactListErroLog.set("csrPartyId",rmId);
											contactListErroLog.set("createdTime", fromDate);
											contactListErroLog.create();
											continue;
										}
										
										
										
										List<EntityCondition> conditions = FastList.newInstance();
										EntityCondition roleTypeCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
												EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
												EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV")
												//EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT")
												), EntityOperator.AND);
										conditions.add(roleTypeCondition1);
										
										if("NONSENSITIVE".equals(emailType)) {
											conditions.add(EntityCondition.makeCondition("relationshipIsMarketable", EntityOperator.EQUALS, "Y"));
										} else {
											conditions.add(EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT"));
										}
										
										EntityCondition partyStatusCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
												EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
												), EntityOperator.OR);
										conditions.add(partyStatusCondition1);

										if (UtilValidate.isNotEmpty(partyId)) {
											conditions.add(EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyId));
										}
										EntityFindOptions efo1 = new EntityFindOptions();
										efo1.setDistinct(true);
										conditions.add(EntityUtil.getFilterByDateExpr());
										EntityCondition roleConditionF = EntityCondition.makeCondition(conditions, EntityOperator.AND);
										List<GenericValue>  partyFromReln = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", roleConditionF, findOptions, UtilMisc.toList("createdDate"), efo1, false);
										
										
										if(partyFromReln.size()>0){
											for(GenericValue partyContact:partyFromReln){
												partyIdContact= partyContact.getString("partyIdFrom");
												partyAccId=partyId;
												//Checking solicitation and Deliverable flag
												String checkFlag=checkSolicitation(partyIdContact,partyAccId,contactListId,contactMechType,fromDate);
												if("TRUE".equals(checkFlag)){
												boolean created=checkAndAddCustomer(partyIdContact,partyAccId,contactListId);
												successCount++;
												}else if("FALSE".equals(checkFlag)){
													noFlagCount++;
													continue;
												}
												/*if(created){
												     successCount++;
												}else{
													dedupCount++;
												}*/

											}
											

										}else{
											EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
							                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
							                        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeId),
							                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
							                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

							                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();
							                    if(responsibleFor!=null  && responsibleFor.size()>0){
											    	rmId=responsibleFor.get(0).getString("partyIdTo");
											    }
											noDefault++;
											contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
											contactListErroLog.set("partyId",partyId);
											contactListErroLog.set("status","ERROR");
											contactListErroLog.set("csrPartyId",rmId);
											contactListErroLog.set("errorLog",CampConstants.ErrorLogs.NOCONTACTASSOC);
											contactListErroLog.set("createdTime", fromDate);
											contactListErroLog.create();
											continue;

										}
									}else if("CONTACT".equals(roleTypeId)){

										Set <String> findOptions = UtilMisc.toSet("partyIdTo");
										findOptions.add("partyIdFrom");
										List<EntityCondition> conditions = FastList.newInstance();
										EntityCondition roleTypeCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
												EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
												EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV")
												//EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT")
												), EntityOperator.AND);
										conditions.add(roleTypeCondition1);
										
										if("NONSENSITIVE".equals(emailType)) {
											conditions.add(EntityCondition.makeCondition("relationshipIsMarketable", EntityOperator.EQUALS, "Y"));
										} else {
											conditions.add(EntityCondition.makeCondition("relationshipStatusId", EntityOperator.EQUALS, "PARTY_DEFAULT"));
										}
										EntityCondition partyStatusCondition1 = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
												EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
												), EntityOperator.OR);
										conditions.add(partyStatusCondition1);

										if (UtilValidate.isNotEmpty(partyId)) {
											conditions.add(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId));
										}
										EntityFindOptions efo1 = new EntityFindOptions();
										efo1.setDistinct(true);
										conditions.add(EntityUtil.getFilterByDateExpr());
										EntityCondition roleConditionF = EntityCondition.makeCondition(conditions, EntityOperator.AND);

										List<GenericValue>  partyFromReln = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", roleConditionF, findOptions, UtilMisc.toList("createdDate"), efo1, false);
										if(partyFromReln.size()>0){
											for(GenericValue partyContact:partyFromReln){
												partyAccId=partyContact.getString("partyIdTo");	
												//Checking solicitation and Deliverable flag
												String checkFlag=checkSolicitation(partyId,partyAccId,contactListId,contactMechType,fromDate);
												if("TRUE".equals(checkFlag)){
												boolean created=checkAndAddCustomer(partyId,partyAccId,contactListId);
													if(created){
														successCount++;
													}else{
														dedupCount++;
													}
												}else if("FALSE".equals(checkFlag)){
													noFlagCount++;
													continue;
												}
											}

										}else{
											noAccount++;
											EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
							                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
							                        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeId),
							                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
							                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

							                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();
											    if(responsibleFor!=null  && responsibleFor.size()>0){
											    	rmId=responsibleFor.get(0).getString("roleTypeIdFrom");
											    	
											    }
											contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
											contactListErroLog.set("partyId",partyId);
											contactListErroLog.set("status","ERROR");
											contactListErroLog.set("csrPartyId",rmId);
											contactListErroLog.set("errorLog",CampConstants.ErrorLogs.NOACCOUNTASSOC);
											contactListErroLog.set("createdTime", fromDate);
											contactListErroLog.create();
											continue;
										}


									}else if("LEAD".equals(roleTypeId)){
										boolean created=checkAndAddCustomer(partyId,partyAccId,contactListId);
										}

								}
							}
							/*
							 * commented to call trigger
							 * List<GenericValue> partyContactMechPurpose = delegator.findByAnd("PartyContactMechPurpose",UtilMisc.toMap("partyId",partyId,"contactMechPurposeTypeId",contactMechType),UtilMisc.toList("lastUpdatedStamp DESC"),false);
							partyContactMechPurpose= EntityUtil.filterByDate(partyContactMechPurpose, true);
							List<GenericValue> contactContactMechPurpose = delegator.findByAnd("PartyContactMechPurpose",UtilMisc.toMap("partyId",partyId,"contactMechPurposeTypeId",contactMechType),UtilMisc.toList("lastUpdatedStamp DESC"),false);
							partyContactMechPurpose= EntityUtil.filterByDate(contactContactMechPurpose, true);
							EntityCondition mainCondition = EntityCondition.makeCondition(EntityCondition.makeCondition(conditionList, EntityOperator.AND),EntityOperator.AND,EntityCondition.makeCondition("partyId",EntityOperator.EQUALS,partyId));
							List contactPartyLists = delegator.findList("ContactListParty", mainCondition, null, null,null,false);

							if(contactPartyLists.size()>0){
								dedupCount++;
								//Duplicate party - already available
								if(UtilValidate.isNotEmpty(contactListId)){
									contactListErroLog.set("partyId",partyId);
									contactListErroLog.set("status","ERROR");
									contactListErroLog.set("errorLog",CampConstants.ErrorLogs.ALREADYADDED);
									contactListErroLog.set("createdTime", fromDate);
									contactListErroLog.create();
									continue;
								}
							}else{

								GenericValue contactListParty = delegator.makeValue("ContactListParty",UtilMisc.toMap("contactListId",contactListId,"partyId",partyId,"fromDate",fromDate));
								contactListParty.set("statusId", "CLPT_ACCEPTED");
								if(UtilValidate.isNotEmpty(partyContactMechPurpose)){
									contactMechId = partyContactMechPurpose.get(0).getString("contactMechId");
									contactListParty.set("preferredContactMechId", contactMechId);
								}
								contactListParty.set("acctPartyId", partyAccId);
								contactListParty.create();
								successCount++;

							}
							 */






							/*}else{
								errorCount++;
								//Email solicitation is not 'Y'
								if(UtilValidate.isNotEmpty(contactListId)){
									contactListErroLog.set("partyId",partyId);
									contactListErroLog.set("status","ERROR");
									contactListErroLog.set("errorLog",CampConstants.ErrorLogs.SOLICITATIONCHECK);
									contactListErroLog.set("createdTime", fromDate);
									contactListErroLog.create();
								}
							}*/

							/*	}else{
							errorCount++;
							//email is not AVAILABLE
							String errorLog ="Party Email is Not Available";
							if(UtilValidate.isNotEmpty(contactListId)){
								contactListErroLog.set("partyId",partyId);
								contactListErroLog.set("status","ERROR");
								contactListErroLog.set("errorLog",CampConstants.ErrorLogs.NOEAMIL);
								contactListErroLog.set("createdTime", fromDate);
								contactListErroLog.create();
							}
						}*/

						}else{
							//Not Found
							partyDisabledCount++;
							//Party Disabled
							if(UtilValidate.isNotEmpty(contactListId)){
								contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
								contactListErroLog.set("partyId",partyId);
								contactListErroLog.set("status","ERROR");
								contactListErroLog.set("errorLog",CampConstants.ErrorLogs.PARTYDISABLED);
								contactListErroLog.set("createdTime", fromDate);
								contactListErroLog.create();
							}
						}
					}else{
						//Not Found
						noPartyCount++;
						//Party not available
						if(UtilValidate.isNotEmpty(contactListId)){
							contactListErroLog.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
							contactListErroLog.set("partyId",partyId);
							contactListErroLog.set("status","ERROR");
							contactListErroLog.set("errorLog",CampConstants.ErrorLogs.NOTAVAILABLE);
							contactListErroLog.set("createdTime", fromDate);
							contactListErroLog.create();
						}
					}	
				}
				//GenericValue totalCountGv = delegator.findByPrimaryKey("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","TOTAL_COUNT"));
				int success=0;
				GenericValue totalCountGv = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","TOTAL_COUNT").queryOne();
				if(UtilValidate.isNotEmpty(totalCountGv)){
					success = Integer.parseInt(totalCountGv.getString("attrValue"));
					totalCountGv.set("attrValue", data.length-1+success+"");
					totalCountGv.store();
				}else{
					totalCountGv = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","TOTAL_COUNT"));
					totalCountGv.set("attrValue", data.length-1+"");
					totalCountGv.create();
				}

				//GenericValue successCountGv = delegator.findByPrimaryKey("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","SUCCESS_COUNT"));
				if(UtilValidate.isNotEmpty(successCount) && successCount != 0){
					GenericValue successCountGv = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","SUCCESS_COUNT").queryOne();
					if(UtilValidate.isNotEmpty(successCountGv)){
						success = Integer.parseInt(successCountGv.getString("attrValue"));
						successCountGv.set("attrValue", successCount+success+"");
						successCountGv.store();
					}else{
						successCountGv = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","SUCCESS_COUNT"));
						successCountGv.set("attrValue", successCount+"");
						successCountGv.create();
					}
				}
				int value = 0;
				if(UtilValidate.isNotEmpty(partyDisabledCount) && partyDisabledCount != 0){
					GenericValue errorCountPd = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.PARTYDISABLED).queryOne();
					if(UtilValidate.isNotEmpty(errorCountPd)){
						value = Integer.parseInt(errorCountPd.getString("attrValue"));
						//int count = value + partyDisabledCount;
						//errorCountPd.set("attrValue", count+"");
						errorCountPd.set("attrValue", partyDisabledCount+value+"");
						errorCountPd.store();
					}else{
						errorCountPd = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						errorCountPd.set("attrName",CampConstants.ErrorLogs.PARTYDISABLED);
						errorCountPd.set("attrValue", partyDisabledCount+"");
						errorCountPd.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(dedupCount) && dedupCount != 0){
					GenericValue errorCountDc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.ALREADYADDED).queryOne();
					if(UtilValidate.isNotEmpty(errorCountDc)){
						value = Integer.parseInt(errorCountDc.getString("attrValue"));
						//int count1 = value + dedupCount;
						//errorCountDc.set("attrValue", count1+"");
						errorCountDc.set("attrValue", dedupCount+value+"");
						errorCountDc.store();
					}else{
						errorCountDc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						errorCountDc.set("attrName",CampConstants.ErrorLogs.ALREADYADDED);
						errorCountDc.set("attrValue", dedupCount+"");
						errorCountDc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noPartyCount) && noPartyCount != 0){
					GenericValue errorCountNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.NOTAVAILABLE).queryOne();
					if(UtilValidate.isNotEmpty(errorCountNpc)){
						value = Integer.parseInt(errorCountNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						errorCountNpc.set("attrValue", noPartyCount+value+"");
						errorCountNpc.store();
					}else{
						errorCountNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						errorCountNpc.set("attrName",CampConstants.ErrorLogs.NOTAVAILABLE);
						errorCountNpc.set("attrValue", noPartyCount+"");
						errorCountNpc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noRoleCount) && noRoleCount != 0){
					GenericValue noRoleCountNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.ROLENOTMATCH).queryOne();
					if(UtilValidate.isNotEmpty(noRoleCountNpc)){
						value = Integer.parseInt(noRoleCountNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						noRoleCountNpc.set("attrValue", noRoleCount+value+"");
						noRoleCountNpc.store();
					}else{
						noRoleCountNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						noRoleCountNpc.set("attrName",CampConstants.ErrorLogs.ROLENOTMATCH);
						noRoleCountNpc.set("attrValue", noRoleCount+"");
						noRoleCountNpc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noAccount) && noAccount != 0){
					GenericValue noAccountNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.NOACCOUNTASSOC).queryOne();
					if(UtilValidate.isNotEmpty(noAccountNpc)){
						value = Integer.parseInt(noAccountNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						noAccountNpc.set("attrValue", noAccount+value+"");
						noAccountNpc.store();
					}else{
						noAccountNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						noAccountNpc.set("attrName",CampConstants.ErrorLogs.NOACCOUNTASSOC);
						noAccountNpc.set("attrValue", noAccount+"");
						noAccountNpc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noDefault) && noDefault != 0){
					GenericValue noDefaultNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.NOCONTACTASSOC).queryOne();
					if(UtilValidate.isNotEmpty(noDefaultNpc)){
						value = Integer.parseInt(noDefaultNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						noDefaultNpc.set("attrValue", noDefault+value+"");
						noDefaultNpc.store();
					}else{
						noDefaultNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						noDefaultNpc.set("attrName",CampConstants.ErrorLogs.NOCONTACTASSOC);
						noDefaultNpc.set("attrValue", noDefault+"");
						noDefaultNpc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noUniqueDefault) && noUniqueDefault != 0){
					GenericValue noUniqueDefaultNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.NOUNIQUECONTACT).queryOne();
					if(UtilValidate.isNotEmpty(noUniqueDefaultNpc)){
						value = Integer.parseInt(noUniqueDefaultNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						noUniqueDefaultNpc.set("attrValue", noUniqueDefault+value+"");
						noUniqueDefaultNpc.store();
					}else{
						noUniqueDefaultNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						noUniqueDefaultNpc.set("attrName",CampConstants.ErrorLogs.NOUNIQUECONTACT);
						noUniqueDefaultNpc.set("attrValue", noUniqueDefault+"");
						noUniqueDefaultNpc.create();
					}
				}
				value = 0;
				if(UtilValidate.isNotEmpty(noFlagCount) && noFlagCount != 0){
					GenericValue noFlagCountNpc = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName",CampConstants.ErrorLogs.FLAGCHECKCOUNT).queryOne();
					if(UtilValidate.isNotEmpty(noFlagCountNpc)){
						value = Integer.parseInt(noFlagCountNpc.getString("attrValue"));
						//int count2 = value + noPartyCount;
						//errorCountNpc.set("attrValue", count2+"");
						noFlagCountNpc.set("attrValue", noFlagCount+value+"");
						noFlagCountNpc.store();
					}else{
						noFlagCountNpc = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId));
						noFlagCountNpc.set("attrName",CampConstants.ErrorLogs.FLAGCHECKCOUNT);
						noFlagCountNpc.set("attrValue", noFlagCount+"");
						noFlagCountNpc.create();
					}
				}



			}
			GenericValue changeStatus = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","UPLOAD_FILE_STATUS").queryOne();

			if(UtilValidate.isEmpty(changeStatus)){// || UtilValidate.isEmpty(contactListAttributeGv.getString("attrValue"))){
				changeStatus = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_STATUS"));
				changeStatus.set("attrValue","FINISHED");
				changeStatus.create();
			}else{
				changeStatus.set("attrValue","FINISHED");
				changeStatus.store();
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public boolean checkAndAddCustomer(String partyId,String acctId,String contactListId) throws GenericEntityException{
		List conditionList = FastList.newInstance();
		conditionList.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
		conditionList.add(EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, acctId));
		conditionList.add(EntityCondition.makeCondition("partyId",EntityOperator.EQUALS,partyId));
		conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("thruDate", EntityOperator.EQUALS, null),EntityOperator.OR,EntityCondition.makeCondition("thruDate", EntityOperator.GREATER_THAN, UtilDateTime.nowTimestamp())));

		List<GenericValue> partyContactMechPurpose = delegator.findByAnd("PartyContactMechPurpose",UtilMisc.toMap("partyId",partyId,"contactMechPurposeTypeId",contactMechType),UtilMisc.toList("lastUpdatedStamp DESC"),false);
		partyContactMechPurpose= EntityUtil.filterByDate(partyContactMechPurpose, true);
		EntityCondition mainCondition = EntityCondition.makeCondition(EntityCondition.makeCondition(conditionList, EntityOperator.AND));
		List contactPartyLists = delegator.findList("ContactListParty", mainCondition, null, null,null,false);

		if(contactPartyLists.size()>0){
			//Duplicate party - already available
			return false;
		}else{
			EntityCondition getSeqCondition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
					EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)
					), EntityOperator.AND);
			Long seqCount = delegator.findCountByCondition("ContactListParty",getSeqCondition, null,null);

			GenericValue contactListParty = delegator.makeValue("ContactListParty",UtilMisc.toMap("contactListId",contactListId,"partyId",partyId,"fromDate",UtilDateTime.nowTimestamp(),"contactListSeqId", seqCount+1+""));
			contactListParty.set("statusId", "CLPT_ACCEPTED");
			if(UtilValidate.isNotEmpty(partyContactMechPurpose)){
				String contactMechId = partyContactMechPurpose.get(0).getString("contactMechId");
				contactListParty.set("preferredContactMechId", contactMechId);
			}
			contactListParty.set("acctPartyId", acctId);
			//contactListParty.set("contactListSeqId", seqCount+1+"");
			contactListParty.create();
			return true;

		}
	}
	
	public String checkSolicitation(String partyId,String acctId,String contactListId,String contactMechType,Timestamp fromDate) throws GenericEntityException{
		List<GenericValue> partyContactMechPurpose = delegator.findByAnd("PartyContactMechPurpose",UtilMisc.toMap("partyId",partyId,"contactMechPurposeTypeId",contactMechType),UtilMisc.toList("lastUpdatedStamp DESC"),false);
		partyContactMechPurpose= EntityUtil.filterByDate(partyContactMechPurpose, true);
		if(partyContactMechPurpose != null && partyContactMechPurpose.size()>0 ){
			String contactMechId = partyContactMechPurpose.get(0).getString("contactMechId");			
			GenericValue flagStatus = EntityQuery.use(delegator).from("PartyContactMech").where("partyId",partyId,"contactMechId",contactMechId).queryOne();
			String soliciationflag = flagStatus.getString("allowSolicitation");
			String deliverable = flagStatus.getString("emailValidInd");
			GenericValue contactListErroLogFlag = delegator.makeValue("FioContactListErrorLog");
			contactListErroLogFlag.set("contactListId",contactListId);
			if("N".equals(soliciationflag)){
				contactListErroLogFlag.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
				contactListErroLogFlag.set("partyId",partyId);
				contactListErroLogFlag.set("status","ERROR");
				contactListErroLogFlag.set("errorLog",CampConstants.ErrorLogs.SOLICITATIONCHECK);
				contactListErroLogFlag.set("createdTime", fromDate);
				contactListErroLogFlag.create();
				return "FALSE";
			}
			
			if("PRIMARY_EMAIL".equals(contactMechType)){
			    if("N".equals(deliverable)){
				contactListErroLogFlag.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
				contactListErroLogFlag.set("partyId",partyId);
				contactListErroLogFlag.set("status","ERROR");
				contactListErroLogFlag.set("errorLog",CampConstants.ErrorLogs.DELIVERABLECHECK);
				contactListErroLogFlag.set("createdTime", fromDate);
				contactListErroLogFlag.create();
				return "FALSE";
			    }
			}else if("PRIMARY_PHONE".equals(contactMechType)){
				GenericValue deliverableFlagStatus = EntityQuery.use(delegator).from("TelecomNumber").where("contactMechId",contactMechId).queryOne();	
				String phoneDeliverableFlag = deliverableFlagStatus.getString("phoneValidInd");
				if("N".equals(phoneDeliverableFlag)){
				contactListErroLogFlag.set("ErrorLogId", delegator.getNextSeqId("FioContactListErrorLog"));
				contactListErroLogFlag.set("partyId",partyId);
				contactListErroLogFlag.set("status","ERROR");
				contactListErroLogFlag.set("errorLog",CampConstants.ErrorLogs.DELIVERABLECHECK);
				contactListErroLogFlag.set("createdTime", fromDate);
				contactListErroLogFlag.create();
				return "FALSE";
				}
			}
			
		}
		return "TRUE";
	}

	public Delegator getDelegator() {
		return delegator;
	}

	public void setDelegator(Delegator delegator) {
		this.delegator = delegator;
	}

	public LocalDispatcher getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(LocalDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	public String getContactListId() {
		return contactListId;
	}

	public void setContactListId(String contactListId) {
		this.contactListId = contactListId;
	}

	public void setContactMechType(String contactMechType) {
		this.contactMechType = contactMechType;
	}

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}
	public String getRoleTypeId() {
		return roleTypeId;
	}
	public void setRoleTypeId(String roleTypeId) {
		this.roleTypeId = roleTypeId;
	}
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	
}
