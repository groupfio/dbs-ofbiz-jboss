package org.fio.campaign.events;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityComparisonOperator;
import org.ofbiz.entity.condition.EntityJoinOperator;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Mahendran T
 * @since 06-07-2018
 * */
public class CampaignEvents {
	private static final String module = CampaignEvents.class.getName();
	public static Map<String, EntityComparisonOperator<?, ?>> entityOperators;

    static {
        entityOperators =  new LinkedHashMap<String, EntityComparisonOperator<?, ?>>();
        entityOperators.put("between", EntityOperator.BETWEEN);
        entityOperators.put("equal", EntityOperator.EQUALS);
        entityOperators.put("greaterThan", EntityOperator.GREATER_THAN);
        entityOperators.put("greaterThanEqualTo", EntityOperator.GREATER_THAN_EQUAL_TO);
        entityOperators.put("in", EntityOperator.IN);
        entityOperators.put("not-in", EntityOperator.NOT_IN);
        entityOperators.put("lessThan", EntityOperator.LESS_THAN);
        entityOperators.put("lessThanEqualTo", EntityOperator.LESS_THAN_EQUAL_TO);
        entityOperators.put("like", EntityOperator.LIKE);
        entityOperators.put("notLike", EntityOperator.NOT_LIKE);
        entityOperators.put("not", EntityOperator.NOT);
        entityOperators.put("notEqual", EntityOperator.NOT_EQUAL);
    }
    
	public static Map<String, EntityJoinOperator> entityJoinOperators;

    static {
    	entityJoinOperators =  new LinkedHashMap<String, EntityJoinOperator>();
    	entityJoinOperators.put("AND", EntityOperator.AND);
    	entityJoinOperators.put("OR", EntityOperator.OR);
    }

	public static String createOrUpdateSmartlistTemplate(HttpServletRequest request, HttpServletResponse response) {
		Delegator delegator = (Delegator)request.getAttribute("delegator");
		Locale locale = UtilHttp.getLocale(request);
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	String jsonString = request.getParameter("jsonString");
    	String marketingCampaignId = request.getParameter("marketingCampaignId");
    	String smartListId = request.getParameter("smartListId");
    	String name = request.getParameter("name");
    	String type = request.getParameter("type");
    	String isProcessed = request.getParameter("isProcessed");
    	try {
    		if(UtilValidate.isEmpty(jsonString)) {
    			request.setAttribute("_ERROR_MESSAGE_", "Invalid Condition");
    			return "error";
    		}
    		jsonString = jsonString.replaceAll("quot;", "\"");
    		
    		//JSONObject root = (JSONObject) new JSONParser().parse(jsonString);
    		GenericValue smartlistTemplateBuilder = null;
    		if(UtilValidate.isNotEmpty(smartListId)) {
    			smartlistTemplateBuilder = delegator.findOne("SmartlistTemplateBuilder", UtilMisc.toMap("smartListId",smartListId), false);
    		} 
    		if(smartlistTemplateBuilder != null && smartlistTemplateBuilder.size() > 0) {
    			if(UtilValidate.isNotEmpty(name))
    				smartlistTemplateBuilder.set("name", name);
    			smartlistTemplateBuilder.set("marketingCampaignId",marketingCampaignId);
    			smartlistTemplateBuilder.set("criteria",jsonString);
    			delegator.store(smartlistTemplateBuilder);
    			request.setAttribute("_EVENT_MESSAGE_", "Smart list template updated successfully.");
    		} else {
    			smartListId = delegator.getNextSeqId("SmartlistTemplateBuilder");
    			smartlistTemplateBuilder = delegator.makeValue("SmartlistTemplateBuilder");
    			smartlistTemplateBuilder.set("smartListId", smartListId);
    			smartlistTemplateBuilder.set("name",name);
    			smartlistTemplateBuilder.set("type",type);
    			smartlistTemplateBuilder.set("marketingCampaignId",marketingCampaignId);
    			smartlistTemplateBuilder.set("criteria",jsonString);
    			smartlistTemplateBuilder.set("isProcessed", UtilValidate.isNotEmpty(isProcessed)?isProcessed:"N");
    			delegator.createOrStore(smartlistTemplateBuilder);
    			request.setAttribute("_EVENT_MESSAGE_", "Smart list template created successfully.");
    		}
    	} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), module);
			request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
			return "error";
		}
    	request.setAttribute("smartListId", smartListId);
    	request.setAttribute("marketingCampaignId", marketingCampaignId);
		request.setAttribute("activeTab", "smartList");
		return "success";
	}
	
	public static String deleteSmartListTemplate(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
		Delegator delegator = (Delegator)request.getAttribute("delegator");
		Locale locale = UtilHttp.getLocale(request);
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	String smartListId = request.getParameter("smartListId");
    	if(UtilValidate.isNotEmpty(smartListId)) {
    		GenericValue smartListTemplate = delegator.findOne("SmartlistTemplateBuilder", UtilMisc.toMap("smartListId",smartListId), false);
    		if(smartListTemplate !=null && smartListTemplate.size() > 0) {
    			smartListTemplate.remove();
    		}
    	}
    	return "success";
	}
	
	
	public static String executeSmartList(HttpServletRequest request,HttpServletResponse response) {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Locale locale = (Locale) request.getAttribute("locale");

		String marketingCampaignId = (String) request.getParameter("marketingCampaignId");
		String processName = request.getParameter("processName");
		String smartListId = request.getParameter("smartListId");
		String activeTab = request.getParameter("activeTab");
		String mainClassName = (String) request.getParameter("mainClassName");

		String jarName = "";
		String jarPath = "";
		String fileExt = "";


		Debug.log("marketingCampaignId--->"+marketingCampaignId);
		try {
			/*jarName =  EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "externalApp","systemPropertyId","smartListJarName").queryOne().getString("systemPropertyValue");
					//UtilProperties.getPropertyValue("campaign", "externalAppJarName","dbs_external_app.jar");
			jarPath = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "externalApp","systemPropertyId","smartListJarPath").queryOne().getString("systemPropertyValue");
			
			mainClassName = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "externalApp","systemPropertyId","smartListMainClassName").queryOne().getString("systemPropertyValue");
					//UtilProperties.getPropertyValue("campaign", "externalAppPath", "/clients/ofbiz/");
			if(UtilValidate.isEmpty(fileExt)){
				fileExt = ".sh";
			}
			System.out.println("Filepath " + jarPath+marketingCampaignId+fileExt);
			String filePath =jarPath+marketingCampaignId+fileExt;
			File f = new File(filePath);
			Writer writer = new FileWriter(f);
			writer.write("#!/bin/bash");
			writer.write("\n");
			writer.write("cd "+jarPath);
			writer.write("\n");

			writer.write("java -cp " +jarName + " " + mainClassName + " " + marketingCampaignId);
			writer.close();

			f.setExecutable(true);
			f.setReadable(true);
			f.setWritable(true);
			
			ProcessBuilder pb = new ProcessBuilder(filePath);
			Process p = pb.start();
			
			BufferedReader output = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String resultOutput = output.readLine();
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ( (line = output.readLine()) != null) {
				builder.append(line);
			}
			String result = builder.toString();
			Debug.logInfo("execute Smart List output : " + result,module);

			output = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			resultOutput = output.readLine();
			StringBuilder errorBuilder = new StringBuilder();
			String errorLine = null;
			while ( (line = output.readLine()) != null) {
				errorBuilder.append(errorLine);
			}
			String errorResult = builder.toString();
			Debug.logError("execute Smart List error output : " + errorResult,module);
			if(f.exists()) {
				f.delete();
			}
			Debug.logInfo("shell script removed",module);*/
			request.setAttribute("smartListId", smartListId);
			request.setAttribute("marketingCampaignId", marketingCampaignId);
			request.setAttribute("activeTab", activeTab);
			
			GenericValue systemProperty = delegator.findOne("SystemProperty", UtilMisc.toMap("systemResourceId", "externalApp","systemPropertyId",processName), false);
			if(systemProperty != null && systemProperty.size() > 0) {
				String shellScriptPath = systemProperty.getString("systemPropertyValue");
				//String shellScriptPath = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "externalApp","systemPropertyId",processName).queryOne().getString("systemPropertyValue");
				Debug.logInfo(processName +" --shellScriptPath-->"+shellScriptPath, module);
				if (UtilValidate.isNotEmpty(shellScriptPath)) {
					File file = new File(shellScriptPath);
					if (file.exists()) {
						file.setExecutable(true);
						file.setReadable(true);
						file.setWritable(true);

						String cmd = shellScriptPath;
						if(UtilValidate.isNotEmpty(marketingCampaignId)) {
							cmd = cmd.concat(" " + marketingCampaignId);
						}
						
						ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
						System.out.println("execute Smart List Start");
						Process p = pb.start();
						try {
							
							BufferedReader output = new BufferedReader(new InputStreamReader(p.getInputStream()));
							StringBuilder builder = new StringBuilder();
							String line = null;
							while ( (line = output.readLine()) != null) {
								builder.append(line);
							}
							String result = builder.toString();
							Debug.logInfo("execute Smart List output : " + result,module);
							output.close();
							
							BufferedReader output1 = new BufferedReader(new InputStreamReader(p.getErrorStream()));
							StringBuilder errorBuilder = new StringBuilder();
							String errorLine = null;
							while ( (errorLine = output1.readLine()) != null) {
								errorBuilder.append(errorLine);
							}
							String errorResult = errorBuilder.toString();
							Debug.logError("execute Smart List error output : " + errorResult,module);
							output1.close();
							p.waitFor();
						} catch (InterruptedException  e) {
					      	Debug.logInfo("shellTimer wait for command execution  exception : "+e,module);
						}finally{
						       	p.destroy();
						}
						
					} else {
						Debug.log("execute Smart List not exist");
					}
				}
			} else {
				Debug.logError("Please configure the script path", module);
				return "error";
			}
			Debug.logInfo("Smart list executed successfully", module);

		} catch (Exception e) {
			Debug.logError(e, module);
			return "error";
		}
		return "success";
	}
	
	public static String smartListSchedulerConfig(HttpServletRequest request,HttpServletResponse response) {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Locale locale = (Locale) request.getAttribute("locale");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
		String marketingCampaignId = request.getParameter("marketingCampaignId");
		String smartListId = request.getParameter("smartListId");
		String activeTab = request.getParameter("activeTab");
		String executionType = request.getParameter("executionType");
		String duration = request.getParameter("duration");
		String isEnable = request.getParameter("isJobEnable");
		String fromDate = request.getParameter("fromDate");
		String thruDate = request.getParameter("thruDate");
		String specificDate = request.getParameter("specificDate");
		String configType = request.getParameter("configType");
		String startDate = request.getParameter("startDate");
		
		try {
			duration = UtilValidate.isNotEmpty(duration)? duration : "0";
			
			if(UtilValidate.isEmpty(fromDate)) {
				fromDate = df.format(new Date());
			} else {
				fromDate = df.format(df1.parse(fromDate));
			}
			Timestamp endDate = null;
			if(UtilValidate.isEmpty(thruDate)) {
				Date date1 = new Date();
				if(UtilValidate.isNotEmpty(duration)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, Integer.parseInt(duration));
					date1 = cal.getTime();
				}
				thruDate = df.format(date1);
			} else {
				thruDate = df.format(df1.parse(thruDate));
			}
			endDate = Timestamp.valueOf(thruDate+" 23:59:59");
			
			if("SINGLE".equals(executionType)) {
				if(UtilValidate.isEmpty(startDate)) {
					fromDate = df.format(new Date());
				} else {
					fromDate = df.format(df1.parse(startDate));
				}
				configType = null;
				duration = null;
				endDate = null;
			}
			
			Timestamp specDate = null;
			if("SPEC_DATE".equals(configType)) {
				duration = null;
				if(UtilValidate.isNotEmpty(specificDate)) {
					specificDate = df.format(df1.parse(specificDate));
				}
				specDate = Timestamp.valueOf(specificDate+" 00:00:00");
			} else {
				specDate = null;	
			}
			GenericValue smartListBuilder = delegator.findOne("SmartlistTemplateBuilder", UtilMisc.toMap("smartListId",smartListId), false);
			GenericValue marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
			if((smartListBuilder != null && smartListBuilder.size() > 0) && (marketingCampaign != null && marketingCampaign.size() > 0)) {
				GenericValue smartListJob = EntityUtil.getFirst(delegator.findByAnd("SmartlistJobScheduler", UtilMisc.toMap("smartListId",smartListId,"campaignId",marketingCampaignId) , UtilMisc.toList("lastUpdatedTxStamp DESC"), false));
				if(smartListJob != null && smartListJob.size() > 0) {
					smartListJob.set("smartListName", smartListBuilder.getString("name"));
					smartListJob.set("executionType", executionType);
					smartListJob.set("campaignType", marketingCampaign.getString("campaignTypeId"));
					smartListJob.set("campaignId", marketingCampaignId);
					smartListJob.set("configType", configType);
					smartListJob.set("startDate", Timestamp.valueOf(fromDate+" 00:00:00"));
					smartListJob.set("endDate", endDate);
					smartListJob.set("specificDate", specDate);
					smartListJob.set("intervalDays", duration);
					smartListJob.set("isEnable", isEnable);
					smartListJob.store();
				} else {
					smartListJob = delegator.makeValue("SmartlistJobScheduler");
					smartListJob.set("jobId", delegator.getNextSeqId("SmartlistJobScheduler"));
					smartListJob.set("smartListId", smartListId);
					smartListJob.set("smartListName", smartListBuilder.getString("name"));
					smartListJob.set("executionType", executionType);
					smartListJob.set("campaignType", marketingCampaign.getString("campaignTypeId"));
					smartListJob.set("campaignId", marketingCampaignId);
					smartListJob.set("configType", configType);
					smartListJob.set("startDate", Timestamp.valueOf(fromDate+" 00:00:00"));
					smartListJob.set("endDate", endDate);
					smartListJob.set("specificDate", specDate);
					smartListJob.set("dateExecuted", null);
					smartListJob.set("nextOccurrenceDate", null);
					smartListJob.set("intervalDays", duration);
					smartListJob.set("isEnable", isEnable);
					smartListJob.set("status", "CREATED");
					smartListJob.create();
				}
			}else {
				request.setAttribute("_ERROR_MESSAGE_", "Check smartlist and marketing campaign");
				Debug.logError("Error : Check smartlist and marketing campaign", module);
				return "error";
			}
		} catch (Exception e) {
			request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
			Debug.logError("Error : "+e.getMessage(), module);
			return "error";
		}
		request.setAttribute("_EVENT_MESSAGE_", "Smart list job has been successfully scheduled");
		request.setAttribute("smartListId", smartListId);
    	request.setAttribute("marketingCampaignId", marketingCampaignId);
		request.setAttribute("activeTab", activeTab);
		return "success";
	}
	
	
}
