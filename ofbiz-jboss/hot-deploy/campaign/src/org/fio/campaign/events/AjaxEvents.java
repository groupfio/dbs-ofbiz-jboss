/*
 * Copyright (c) Open Source Strategies, Inc.
 *
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.fio.campaign.events;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.fio.campaign.contactmech.PartyPrimaryContactMechWorker;
import org.fio.campaign.util.DataUtil;
import org.fio.campaign.util.ResponseUtils;
import org.fio.campaign.util.LoginFilterUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityJoinOperator;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;
import javolution.util.FastMap;

public final class AjaxEvents {

    private AjaxEvents() { }

    private static final String MODULE = AjaxEvents.class.getName();

    public static String doJSONResponse(HttpServletResponse response, JSONObject jsonObject) {
        return doJSONResponse(response, jsonObject.toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Collection<?> collection) {
        return doJSONResponse(response, JSONArray.fromObject(collection).toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Map map) {
        return doJSONResponse(response, JSONObject.fromObject(map));
    }

    public static String doJSONResponse(HttpServletResponse response, String jsonString) {
        String result = "success";

        response.setContentType("application/x-json");
        try {
            response.setContentLength(jsonString.getBytes("UTF-8").length);
        } catch (UnsupportedEncodingException e) {
            Debug.logWarning("Could not get the UTF-8 json string due to UnsupportedEncodingException: " + e.getMessage(), MODULE);
            response.setContentLength(jsonString.length());
        }

        Writer out;
        try {
            out = response.getWriter();
            out.write(jsonString);
            out.flush();
        } catch (IOException e) {
            Debug.logError(e, "Failed to get response writer", MODULE);
            result = "error";
        }
        return result;
    }
    
    public static GenericValue getUserLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (GenericValue) session.getAttribute("userLogin");
    }
    
    
    
    public static String getTemplates(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		List<Map<String, Object>> finalList = new ArrayList<Map<String,Object>>();
		List<GenericValue> templateAndCategory = null;
		try {
			List<EntityCondition> conditionList = FastList.newInstance();
			
			String templateType  = request.getParameter("templateType");
			
			if (UtilValidate.isNotEmpty(templateType)) {
				conditionList.add(EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, templateType));
			}else{
				conditionList.add(EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, "EMAIL_BLAST"));
			}
			
			String templateName = request.getParameter("tempalateName");
			if(UtilValidate.isNotEmpty(templateName)) {
				templateName = templateName.trim();
				conditionList.add(EntityCondition.makeCondition("templateName",EntityOperator.LIKE,templateName+"%"));
			}
			String[] templateCategories = request.getParameterValues("templateCategories[]");
			if(UtilValidate.isNotEmpty(templateCategories)) {
				List<String> categoryIds = Arrays.asList(templateCategories);
				conditionList.add(EntityCondition.makeCondition("templateCategoryId",EntityOperator.IN,categoryIds));
			}
			
			
			templateAndCategory = delegator.findList("TemplateAndCategory", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, UtilMisc.toList("templateId DESC"), null, false);
			if(UtilValidate.isNotEmpty(templateAndCategory)) {
				templateAndCategory.forEach(e->{
					Map<String, Object> returnMap = new HashMap<String, Object>();
					returnMap.put("templateId", e.getString("templateId"));
					returnMap.put("templateName", UtilValidate.isNotEmpty(e.getString("templateName")) ? e.getString("templateName") : "");
					returnMap.put("templateFormContent", UtilValidate.isNotEmpty(e.getString("templateFormContent"))? e.getString("templateFormContent") :"");
					returnMap.put("templateType", UtilValidate.isNotEmpty(e.getString("templateType")) ? e.getString("templateType") : "");
					returnMap.put("thumbnailUrl", UtilValidate.isNotEmpty(e.getString("thumbnailUrl")) ? e.getString("thumbnailUrl") : "");
					finalList.add(returnMap);
				});
			}
		} catch(Exception e) {
			Debug.logInfo("Error-"+e.getMessage(), MODULE);
		}
		return doJSONResponse(response, finalList);
	}
    
    
    public static String getTemplatesForDataTable(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		List<GenericValue> templateAndCategory = null;
		
		String draw = request.getParameter("draw");
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		
		JSONObject obj = new JSONObject();
		JSONArray dataMap = new JSONArray();
		long recordsFiltered = 0;
		long recordsTotal = 0;
		
		try {
			List<EntityCondition> conditionList = FastList.newInstance();
			long totalCount = 0;
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			totalCount = delegator.findCountByCondition("TemplateAndCategory", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, UtilMisc.toSet("templateId"), efoNum);
			
			/*templateAndCategory = delegator.findList("TemplateAndCategory", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, null, efo, false);
			if(templateAndCategory != null && templateAndCategory.size() > 0) {
				totalCount = templateAndCategory.size();
			}*/
			
			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
			efo.setOffset(startInx);
			efo.setLimit(endInx);
			
			String orderField = "";
			String sortDir= "ASC";
			String orderColumnId = request.getParameter("order[0][column]");
			if(UtilValidate.isNotEmpty(orderColumnId)) {
				int sortColumnId = Integer.parseInt(orderColumnId);
				String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
				sortDir = request.getParameter("order[0][dir]").toUpperCase();
				orderField = sortColumnName;
			}else {
				orderField = "templateId";
			}
			
			String templateType  = request.getParameter("templateType");
			
			if (UtilValidate.isNotEmpty(templateType)) {
				conditionList.add(EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, templateType));
			}/*else{
				conditionList.add(EntityCondition.makeCondition("templateType", EntityOperator.EQUALS, "EMAIL_BLAST"));
			}*/
			
			String templateName = request.getParameter("tempalateName");
			if(UtilValidate.isNotEmpty(templateName)) {
				templateName = templateName.trim();
				EntityCondition templateNameAndIdCond = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("templateName",EntityOperator.LIKE,"%"+templateName+"%"),
			             EntityCondition.makeCondition("templateId",EntityOperator.LIKE,"%"+templateName+"%")
			             ), EntityOperator.OR);
				conditionList.add(templateNameAndIdCond);
			}
			String filterCategory = request.getParameter("templateCategories");
			if(UtilValidate.isNotEmpty(filterCategory)) {
				String[] templateCategories = filterCategory.split(",");
				if(templateCategories != null && templateCategories.length >0) {
					List<String> categoryIds = Arrays.asList(templateCategories);
					conditionList.add(EntityCondition.makeCondition("templateCategoryId",EntityOperator.IN,categoryIds));
				}
			}
			
			templateAndCategory = delegator.findList("TemplateAndCategory", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
			if(UtilValidate.isNotEmpty(templateAndCategory)) {
				recordsFiltered = totalCount;
				recordsTotal = totalCount;
				templateAndCategory.forEach(e->{
					JSONObject listObj = new JSONObject();
					listObj.put("templateId", e.getString("templateId"));
					listObj.put("templateName", UtilValidate.isNotEmpty(e.getString("templateName")) ? e.getString("templateName") : "");
					listObj.put("templateFormContent", UtilValidate.isNotEmpty(e.getString("templateFormContent"))? e.getString("templateFormContent") :"");
					listObj.put("templateType", UtilValidate.isNotEmpty(e.getString("templateType")) ? e.getString("templateType") : "");
					listObj.put("subject", UtilValidate.isNotEmpty(e.getString("subject")) ? e.getString("subject") : "");
					listObj.put("fromDate", UtilValidate.isNotEmpty(e.getTimestamp("fromDate")) ? df.format(e.getTimestamp("fromDate")) : "");
					listObj.put("thruDate", UtilValidate.isNotEmpty(e.getTimestamp("thruDate")) ? df.format(e.getTimestamp("thruDate")) : "");
					dataMap.add(listObj);
				});
			}
			obj.put("data", dataMap);
			obj.put("draw", draw);
			obj.put("recordsTotal", recordsTotal);
			obj.put("recordsFiltered", recordsFiltered);
		} catch(Exception e) {
			Debug.logInfo("Error-"+e.getMessage(), MODULE);
		}
		return doJSONResponse(response, obj);
	}
    
    public static String getTemplateContent(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException { 
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	
    	Map<String, String> resultData = FastMap.newInstance();
    	JSONObject obj = new JSONObject();
    	String htmlContent = null;
    	try {
    		String templateId  = request.getParameter("templateId");
    		if(UtilValidate.isNotEmpty(templateId)) {
    			String templateFormContent = EntityQuery.use(delegator).from("TemplateMaster").where("templateId",templateId).queryOne().getString("templateFormContent");
    			if(UtilValidate.isEmpty(templateFormContent)) {
    				String text = EntityQuery.use(delegator).from("TemplateMaster").where("templateId",templateId).queryOne().getString("textContent");
    				if(UtilValidate.isNotEmpty(text)) {
    					htmlContent = text;
    				}
    			} else {
    				htmlContent = templateFormContent;
    			}
    		}
    		if(UtilValidate.isNotEmpty(htmlContent)) {
    			obj.put("htmlContent", htmlContent);
    		} else {
    			obj.put("htmlContent", "");
    		}

    	} catch(Exception e) {
    		Debug.logInfo("Error-"+e.getMessage(), MODULE);
    	}
    	return doJSONResponse(response, obj);
    }
    
    
    public static String getTemplateTagConfigList(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException { 
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		
		Locale locale = UtilHttp.getLocale(request);
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String draw = request.getParameter("draw");
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		/*String templateTagTypeId = request.getParameter("templateTagTypeId");
		String keyword =  request.getParameter("keyword");
		String groupingCode = request.getParameter("groupingCode");
		String groupId = request.getParameter("groupId");*/
		String customFieldGroupName = request.getParameter("customFieldGroupName");
		String customFieldName = request.getParameter("customFieldName");
		String tagId = request.getParameter("tagId");
		String tagName = request.getParameter("tagName");
		JSONObject obj = new JSONObject();
		JSONArray dataMap = new JSONArray();
		long recordsFiltered = 0;
		long recordsTotal = 0;
		List<GenericValue> dataTagConfiguration = null;
		try {
			List<String> orderBy = FastList.newInstance();
			String orderField = "";
			String sortDir= "ASC";
			String orderColumnId = request.getParameter("order[0][column]");
			if(UtilValidate.isNotEmpty(orderColumnId)) {
				int sortColumnId = Integer.parseInt(orderColumnId);
				String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
				sortDir = request.getParameter("order[0][dir]").toUpperCase();
				orderField = sortColumnName;
				//sortColumnName = sortColumnName+" "+sortDir;
				//orderBy.add(sortColumnName);
			} else {
				//orderBy.add("tagId");
				orderField = "tagId";
			}
			List<EntityCondition> conditionList = FastList.newInstance();
			/*if(UtilValidate.isNotEmpty(keyword))
			{
				List<EntityCondition> keywordList = FastList.newInstance();
				List<GenericValue> templateTagDesc = delegator.findList("TemplateTagType", EntityCondition.makeCondition("description",EntityOperator.LIKE,"%"+keyword+"%"), null, null, null, false);
				if(UtilValidate.isNotEmpty(templateTagDesc))
				{
					keywordList.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.IN,EntityUtil.getFieldListFromEntityList(templateTagDesc, "templateTagTypeId", true)));
				}
				
				keywordList.add(EntityCondition.makeCondition("customFieldGroupName",EntityOperator.LIKE,"%"+keyword+"%"));
				keywordList.add(EntityCondition.makeCondition("customFieldName",EntityOperator.LIKE,"%"+keyword+"%"));
				keywordList.add(EntityCondition.makeCondition("tagId",EntityOperator.LIKE,"%"+keyword+"%"));
				keywordList.add(EntityCondition.makeCondition("tagName",EntityOperator.LIKE,"%"+keyword+"%"));
				//keywordList.add(EntityCondition.makeCondition("fromDate",EntityOperator.LIKE,"%"+keyword+"%"));
				//keywordList.add(EntityCondition.makeCondition("thruDate",EntityOperator.LIKE,"%"+keyword+"%"));
				if(keyword.equalsIgnoreCase("y") || keyword.equalsIgnoreCase("ye") || keyword.equalsIgnoreCase("yes"))
				{
					keyword = "Y";
				}
				
				if(keyword.equalsIgnoreCase("n") || keyword.equalsIgnoreCase("no"))
				{
					keyword = "N";
				}
				keywordList.add(EntityCondition.makeCondition("isEnabled",EntityOperator.LIKE,"%"+keyword+"%"));
				keywordList.add(EntityCondition.makeCondition("isContact",EntityOperator.LIKE,"%"+keyword+"%"));
				EntityCondition condition = EntityCondition.makeCondition(keywordList,EntityOperator.OR);
				conditionList.add(condition);
				
			}

			if(UtilValidate.isNotEmpty(templateTagTypeId)) {
				conditionList.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,templateTagTypeId));
			}
			if(UtilValidate.isNotEmpty(groupingCode)) {
				conditionList.add(EntityCondition.makeCondition("groupingCode",EntityOperator.EQUALS,groupingCode));
			}
			if(UtilValidate.isNotEmpty(groupId)) {
				conditionList.add(EntityCondition.makeCondition("customFieldGroupId",EntityOperator.EQUALS,groupId));
			}*/

			if(UtilValidate.isNotEmpty(customFieldGroupName)) {
				conditionList.add(EntityCondition.makeCondition("customFieldGroupName",EntityOperator.LIKE, "%"+customFieldGroupName+"%"));
			}
			if(UtilValidate.isNotEmpty(customFieldName)) {
				conditionList.add(EntityCondition.makeCondition("customFieldName",EntityOperator.LIKE, "%"+customFieldName+"%"));
			}
			if(UtilValidate.isNotEmpty(tagId)) {
				conditionList.add(EntityCondition.makeCondition("tagId",EntityOperator.LIKE,"%"+tagId+"%"));
			}
			if(UtilValidate.isNotEmpty(tagName)) {
				conditionList.add(EntityCondition.makeCondition("tagName",EntityOperator.LIKE,"%"+tagName+"%"));
			}
			
			//conditionList.add(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N"));
			orderBy.add("customFieldGroupId");
			orderBy.add("groupSequenceNo");
			orderBy.add("fieldSequenceNo");
			//orderBy.add(orderField);
			EntityCondition condition = EntityCondition.makeCondition(conditionList,EntityOperator.AND);
			
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);	
			
			long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("TemplateTagAndCustomGroupField", condition, null, UtilMisc.toSet("tagId"), efoNum);
			
			/*List<GenericValue> tagList = delegator.findList("TemplateTagAndCustomGroupField", condition, UtilMisc.toSet("tagId"), null, efo, false);
			int count = tagList.size();*/
			
			recordsFiltered = count;
			recordsTotal = count;
			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
			efo.setOffset(startInx);
			efo.setLimit(endInx);
			
			
			dataTagConfiguration = delegator.findList("TemplateTagAndCustomGroupField", condition, null, orderBy, efo, false);
			if(dataTagConfiguration != null && dataTagConfiguration.size() > 0) {
				dataTagConfiguration.forEach(e->{
					JSONObject listObj = new JSONObject();
					String groupType = e.getString("templateTagTypeId");
					String groupTypeDesc = "";
					if(UtilValidate.isNotEmpty(groupType)) {
						try {
							groupTypeDesc = EntityQuery.use(delegator).from("TemplateTagType").where("templateTagTypeId",groupType).queryOne().getString("description");
						} catch (GenericEntityException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					listObj.put("groupType", groupTypeDesc);
					listObj.put("customFieldGroupId", UtilValidate.isNotEmpty(e.getString("customFieldGroupName")) ? e.getString("customFieldGroupName") : "");
					listObj.put("customFieldGupId", UtilValidate.isNotEmpty(e.getString("customFieldGroupId")) ? e.getString("customFieldGroupId") : "");
					listObj.put("customFieldId", UtilValidate.isNotEmpty(e.getString("customFieldName")) ? e.getString("customFieldName") : "");
					listObj.put("tagId", e.getString("tagId"));
					listObj.put("tagName", UtilValidate.isNotEmpty(e.getString("tagName")) ? e.getString("tagName") : "");
					listObj.put("fromDate", UtilValidate.isNotEmpty(e.getTimestamp("fromDate")) ? df.format(e.getTimestamp("fromDate")) : "");
					listObj.put("thruDate", UtilValidate.isNotEmpty(e.getTimestamp("thruDate")) ? df.format(e.getTimestamp("thruDate")) : "");
					listObj.put("isEnabled", UtilValidate.isNotEmpty(e.getString("isEnabled")) ? e.getString("isEnabled") : "");
					listObj.put("isContact", UtilValidate.isNotEmpty(e.getString("isContact")) ? e.getString("isContact") : "");
					dataMap.add(listObj);
				});
			}
			/*dataTagConfiguration = delegator.findList("DataTagConfiguration", null, null, UtilMisc.toList(orderField+ " " + sortDir), null, false);
			
			if(!dataTagConfiguration.isEmpty()) {
				recordsFiltered = dataTagConfiguration.size();
				recordsTotal = dataTagConfiguration.size();
				dataTagConfiguration.forEach(e->{
					JSONObject listObj = new JSONObject();
					listObj.put("tagId", e.getString("tagId"));
					listObj.put("tagName", UtilValidate.isNotEmpty(e.getString("tagName")) ? e.getString("tagName") : "");
					String customFieldGroupId = UtilValidate.isNotEmpty(e.getString("customFieldGroupId"))? e.getString("customFieldGroupId") :"";
					String customFieldId = UtilValidate.isNotEmpty(e.getString("customFieldId")) ? e.getString("customFieldId") : "";
					String customFieldGroupName = "";
					String customFieldName = "";
					try {
						if(UtilValidate.isNotEmpty(customFieldGroupId))
							customFieldGroupName = EntityQuery.use(delegator).from("CustomFieldGroup").where("groupId",customFieldGroupId).queryOne().getString("groupName");
						if(UtilValidate.isNotEmpty(customFieldId))
							customFieldName = EntityQuery.use(delegator).from("CustomField").where("customFieldId",customFieldId).queryOne().getString("customFieldName");
					} catch (GenericEntityException e1) {
						e1.printStackTrace();
					}
					listObj.put("customFieldGroupId", customFieldGroupName);
					listObj.put("customFieldId", customFieldName);
					listObj.put("isDisabled", UtilValidate.isNotEmpty(e.getString("isDisabled")) ? e.getString("isDisabled") : "");
					dataMap.add(listObj);
				});
			}*/
			obj.put("data", dataMap);
			obj.put("draw", draw);
			obj.put("recordsTotal", recordsTotal);
			obj.put("recordsFiltered", recordsFiltered);
		} catch(Exception e) {
			Debug.logInfo("Error-"+e.getMessage(), MODULE);
		}
		return doJSONResponse(response, obj);
    }
    
    public static String updateApproveCustomerDetails(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        //Locale locale = UtilHttp.getLocale(request);
        //HttpSession session = request.getSession(true);
        Map < String, String > returnMap = new HashMap < String, String > ();
        try {
            //List < String > partyContactMechIds = new ArrayList < String > ();
            String partyId = request.getParameter("partyId");
            if (UtilValidate.isNotEmpty(partyId)) {
                String phoneContactMechId = request.getParameter("phoneContactMechId");
                String phoneNumber = request.getParameter("phoneNumber");
                String emailContactMechId = request.getParameter("emailContactMechId");
                String infoString = request.getParameter("infoString");
                String countryCode = request.getParameter("countryCode");
                String areaCode = request.getParameter("areaCode");
                String extension = request.getParameter("extension");
                String askForName = request.getParameter("askForName");
                String accountId = request.getParameter("accountId");
                
                //PHONE
                if(UtilValidate.isNotEmpty(phoneContactMechId) && UtilValidate.isNotEmpty(phoneNumber)) {
                    GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", phoneContactMechId), false);
                    if (primaryPhoneNumber != null && primaryPhoneNumber.size() > 0) {
                        primaryPhoneNumber.set("contactNumber", phoneNumber);
                        primaryPhoneNumber.store();
                    }
                } else{
                	EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
                			EntityCondition.makeCondition("partyIdFrom",EntityOperator.EQUALS,partyId),
                			EntityCondition.makeCondition("partyIdTo",EntityOperator.EQUALS,accountId),
                			EntityCondition.makeCondition("roleTypeIdFrom",EntityOperator.EQUALS,"CONTACT"),
                			EntityCondition.makeCondition("roleTypeIdTo",EntityOperator.EQUALS,"ACCOUNT"),
                			EntityUtil.getFilterByDateExpr()
                			);
                	GenericValue partyRelationship = null;
                	String partyRelAssocId = "";
                	String assocSeqId = "0";
                	List<GenericValue> partyRelationships = delegator.findList("PartyRelationship", condition, null, null, null, false);
                	if(partyRelationships != null && partyRelationships.size() > 0){
                		partyRelationship = partyRelationships.get(0);
                		partyRelAssocId = partyRelationship.getString("partyRelAssocId");
                		if(UtilValidate.isNotEmpty(partyRelAssocId)){
                			List<GenericValue> partyRelationshipAssocList = delegator.findList("PartyRelationshipAssoc", EntityCondition.makeCondition("partyRelAssocId",EntityOperator.EQUALS,partyRelAssocId), UtilMisc.toSet("assocSeqId"), UtilMisc.toList("assocSeqId DESC"), null, false);
                			if(partyRelationshipAssocList != null && partyRelationshipAssocList.size() >0) {
                				GenericValue partyRelationshipAssoc = partyRelationshipAssocList.get(0);
                				assocSeqId = partyRelationshipAssoc.getString("assocSeqId");
                			}
                		}
                	}
                	
                	
                	Map < String, Object > result = null;
                	Map < String, Object > input = UtilMisc.toMap("partyId", partyId, "userLogin", userLogin, "contactMechPurposeTypeId", "PRIMARY_PHONE", "allowSolicitation", "Y");
                	input.put("countryCode", countryCode);
                	input.put("areaCode", areaCode);
                	input.put("contactNumber", phoneNumber);
                	input.put("extension", extension);
                	input.put("extension", extension);
                	input.put("askForName", askForName);
                	input.put("phoneValidInd", "Y");
                	
                	result = dispatcher.runSync("createPartyTelecomNumber", input);
                	if (ServiceUtil.isSuccess(result)){
                		int seqId = Integer.parseInt(assocSeqId)+1;
                		String assSeqId = StringUtils.leftPad(""+seqId, 4, "0");
                		String newContactMechId = (String) result.get("contactMechId");
                		if(UtilValidate.isEmpty(partyRelAssocId)){
                			partyRelAssocId = delegator.getNextSeqId("PartyRelationshipAssoc");
                			partyRelationship.set("partyRelAssocId", partyRelAssocId);
                			partyRelationship.store();
                		}
                		GenericValue partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc",UtilMisc.toMap("partyRelAssocId",partyRelAssocId));
                		partyRelAssoc.set("assocSeqId", Long.valueOf(assSeqId));
                		partyRelAssoc.set("assocTypeId", "PHONE");
                		partyRelAssoc.set("assocId", newContactMechId);
                		partyRelAssoc.set("solicitationStatus", "Y");
                		partyRelAssoc.create();
                		
                        List<GenericValue> partyContactMechPurposeList = EntityQuery.use(delegator).from("PartyContactMechPurpose")
                            .where("partyId", partyId, "contactMechId", newContactMechId).filterByDate().queryList();
                        Debug.log("partyContactMechPurposeList======="+partyContactMechPurposeList);
                        if(partyContactMechPurposeList != null && partyContactMechPurposeList.size() > 0) {
                            for(GenericValue partyContactMechPurposeGV : partyContactMechPurposeList) {
                                partyContactMechPurposeGV.set("partyRelAssocId", partyRelAssocId);
                            }
                            delegator.storeAll(partyContactMechPurposeList);
                        }
                	}
                }
                
                // EMAIL
                if(UtilValidate.isNotEmpty(emailContactMechId) && UtilValidate.isNotEmpty(infoString)) {
                    GenericValue EmailAddressValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", emailContactMechId), false);
                    if (UtilValidate.isNotEmpty(EmailAddressValue)) {
                        EmailAddressValue.set("infoString", infoString);
                        EmailAddressValue.store();
                    }
                } else{
                	EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
                			EntityCondition.makeCondition("partyIdFrom",EntityOperator.EQUALS,partyId),
                			EntityCondition.makeCondition("partyIdTo",EntityOperator.EQUALS,accountId),
                			EntityCondition.makeCondition("roleTypeIdFrom",EntityOperator.EQUALS,"CONTACT"),
                			EntityCondition.makeCondition("roleTypeIdTo",EntityOperator.EQUALS,"ACCOUNT"),
                			EntityUtil.getFilterByDateExpr()
                			);
                	GenericValue partyRelationship = null;
                	String partyRelAssocId = "";
                	String assocSeqId = "0";
                	List<GenericValue> partyRelationships = delegator.findList("PartyRelationship", condition, null, null, null, false);
                	if(partyRelationships != null && partyRelationships.size() > 0){
                		partyRelationship = partyRelationships.get(0);
                		partyRelAssocId = partyRelationship.getString("partyRelAssocId");
                		if(UtilValidate.isNotEmpty(partyRelAssocId)){
                			//GenericValue partyRelationshipAssoc = delegator.findOne("PartyRelationshipAssoc", UtilMisc.toMap("partyRelAssocId",partyRelAssocId), false);
                			List<GenericValue> partyRelationshipAssocList = delegator.findList("PartyRelationshipAssoc", EntityCondition.makeCondition("partyRelAssocId",EntityOperator.EQUALS,partyRelAssocId), UtilMisc.toSet("assocSeqId"), UtilMisc.toList("assocSeqId DESC"), null, false);
                			if(partyRelationshipAssocList != null && partyRelationshipAssocList.size() >0) {
                				GenericValue partyRelationshipAssoc = partyRelationshipAssocList.get(0);
                				assocSeqId = partyRelationshipAssoc.getString("assocSeqId");
                			}
                		}
                	}
                	
                	
                	Map < String, Object > result = null;
                	Map < String, Object > input = UtilMisc.toMap("partyId", partyId, "userLogin", userLogin, "contactMechPurposeTypeId", "PRIMARY_EMAIL", "allowSolicitation", "Y");
                	input.put("emailAddress", infoString);
                	input.put("emailValidInd", "Y");
                	result = dispatcher.runSync("createPartyEmailAddress", input);
                	if (ServiceUtil.isSuccess(result)){
                		int seqId = Integer.parseInt(assocSeqId)+1;
                		String assSeqId = StringUtils.leftPad(""+seqId, 4, "0");
                		String newContactMechId = (String) result.get("contactMechId");
                		if(UtilValidate.isEmpty(partyRelAssocId)){
                			partyRelAssocId = delegator.getNextSeqId("PartyRelationshipAssoc");
                			partyRelationship.set("partyRelAssocId", partyRelAssocId);
                			partyRelationship.store();
                		}
                		GenericValue partyRelAssoc = delegator.makeValue("PartyRelationshipAssoc",UtilMisc.toMap("partyRelAssocId",partyRelAssocId));
                		partyRelAssoc.set("assocSeqId", Long.valueOf(assSeqId));
                		partyRelAssoc.set("assocTypeId", "EMAIL");
                		partyRelAssoc.set("assocId", newContactMechId);
                		partyRelAssoc.set("solicitationStatus", "Y");
                		partyRelAssoc.create();
                		
                        List<GenericValue> partyContactMechPurposeList = EntityQuery.use(delegator).from("PartyContactMechPurpose")
                            .where("partyId", partyId, "contactMechId", newContactMechId).filterByDate().queryList();
                        Debug.log("partyContactMechPurposeList======="+partyContactMechPurposeList);
                        if(partyContactMechPurposeList != null && partyContactMechPurposeList.size() > 0) {
                            for(GenericValue partyContactMechPurposeGV : partyContactMechPurposeList) {
                                partyContactMechPurposeGV.set("partyRelAssocId", partyRelAssocId);
                            }
                            delegator.storeAll(partyContactMechPurposeList);
                        }
                	}
                }
                /*List < GenericValue > partyContactMechs = delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId", partyId), null, true);
                partyContactMechs = EntityUtil.filterByDate(partyContactMechs);

                for (GenericValue partyContactMech: partyContactMechs) {
                    if (UtilValidate.isNotEmpty(partyContactMech.getString("contactMechId"))) {
                        partyContactMechIds.add(partyContactMech.getString("contactMechId"));
                    }
                }

                Set < String > findOptions = UtilMisc.toSet("contactMechId");
                List < String > orderBy = UtilMisc.toList("createdStamp DESC");

                EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
                EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, partyContactMechIds);

                if (UtilValidate.isNotEmpty(phoneNumber)) {
                    EntityCondition primaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE")));
                    List < GenericValue > primaryPhones = delegator.findList("PartyContactMechPurpose", primaryPhoneConditions, findOptions, orderBy, null, true);
                    if (UtilValidate.isNotEmpty(primaryPhones)) {
                        GenericValue primaryPhone = EntityUtil.getFirst(primaryPhones);
                        GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", primaryPhone.getString("contactMechId")), false);
                        if (primaryPhoneNumber != null && primaryPhoneNumber.size() > 0) {
                            primaryPhoneNumber.set("contactNumber", phoneNumber);
                            primaryPhoneNumber.store();
                        }
                    }
                    if (UtilValidate.isNotEmpty(infoString)) {
                        EntityCondition primaryEmailaddressConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL")));
                        List < GenericValue > EmailAddresses = delegator.findList("PartyContactMechPurpose", primaryEmailaddressConditions, findOptions, orderBy, null, true);
                        if (UtilValidate.isNotEmpty(EmailAddresses)) {
                            GenericValue EmailAddress = EntityUtil.getFirst(EmailAddresses);
                            GenericValue EmailAddressValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", EmailAddress.getString("contactMechId")), false);
                            if (UtilValidate.isNotEmpty(EmailAddressValue)) {
                                EmailAddressValue.set("infoString", infoString);
                                EmailAddressValue.store();
                            }
                        }
                    }

                }*/
            }
        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
        }
        returnMap.put("status", "Data Updated Successfully");
        return doJSONResponse(response, returnMap);
    }
    
    public static String getCostPerAction(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        Map < String, String > returnMap = new HashMap < String, String > ();
        BigDecimal costValue=null;
        try {
            List < String > partyContactMechIds = new ArrayList < String > ();
            String typeId = request.getParameter("typeId");
            if (UtilValidate.isNotEmpty(typeId)) {
            	
            	List<EntityCondition> getActiveConfigCond = new ArrayList<EntityCondition>();
				
            	getActiveConfigCond.add(EntityCondition.makeCondition("financialConfigType", EntityOperator.EQUALS, typeId));
            	getActiveConfigCond.add(EntityCondition.makeCondition("isExpired", EntityOperator.EQUALS, "N"));
				
				EntityCondition costperFinalCondition = EntityCondition.makeCondition(getActiveConfigCond, EntityOperator.AND);

				List<GenericValue> activeCostPerAct=delegator.findList("FinancialMetricConfig", costperFinalCondition, null,null,null,false);						
            	costValue = activeCostPerAct.get(0).getBigDecimal("costPerValue");
            	String configId = activeCostPerAct.get(0).getString("financialConfigId");
            	String configName = activeCostPerAct.get(0).getString("financialConfigName");
            	if(costValue!=null){
            	     returnMap.put("costValue",costValue.toString());
            	     returnMap.put("configId",configId);
            	     returnMap.put("configName",configName);
            	}else{
            		returnMap.put("costValue",null);
            	}
            		
            }
        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
        }
        returnMap.put("status", "Data Updated Successfully");
        return doJSONResponse(response, returnMap);
    }
    /**
     * @author Mahendran T
     * @since 19-06-2018
     * */
    public static String getCodeOrGroupOrMetric(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	String templateTagTypeId = request.getParameter("templateTagTypeId");
    	String groupingCode = request.getParameter("groupingCode");

    	Locale locale = UtilHttp.getLocale(request);
    	HttpSession session = request.getSession(true);
    	JSONArray dataMap = new JSONArray();
    	try {
    		if(UtilValidate.isNotEmpty(templateTagTypeId)) {

    			List<EntityCondition> conditions = FastList.newInstance();
    			if(("SEGMENTATION".equals(templateTagTypeId) || "ECONOMIC_METRIC".equals(templateTagTypeId)) && UtilValidate.isNotEmpty(groupingCode)) {
    				conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isActive",EntityOperator.NOT_EQUAL,"N")));
    				conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,templateTagTypeId));
    				conditions.add(EntityCondition.makeCondition("groupingCode",EntityOperator.EQUALS,groupingCode));
    			} else if("CUSTOM_FIELD".equals(templateTagTypeId)) {
    				conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    				conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,templateTagTypeId));
    			}
    			
    			if(conditions != null && conditions.size() > 0) {
    				EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
        			Set<String> fieldToSelect = new HashSet<String>();
        			fieldToSelect.add("groupId");
        			fieldToSelect.add("groupName");
        			fieldToSelect.add("sequence");

        			EntityFindOptions efo = new EntityFindOptions();
        			efo.setDistinct(true);
        			List<GenericValue> customFieldGroup = delegator.findList("CustomFieldGroup", condition, fieldToSelect, UtilMisc.toList("sequence ASC"), efo, false);

        			if(customFieldGroup != null && customFieldGroup.size() > 0) {
        				customFieldGroup.forEach(e->{
        					JSONObject listObj = new JSONObject();
        					listObj.put("groupId", e.getString("groupId"));
        					listObj.put("groupName", UtilValidate.isNotEmpty(e.getString("groupName")) ? e.getString("groupName") : "");
        					listObj.put("sequenceNo", UtilValidate.isNotEmpty(e.getString("sequence")) ? e.getString("sequence") : "");
        					dataMap.add(listObj);
        				});
        			}
    			}
    		} 
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);
    	}
    	return doJSONResponse(response, dataMap);
    }
    
    /**
     * @author Mahendran T
     * @since 19-06-2018
     * */
    public static String getValuesOrFields(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	String selectedGroup = request.getParameter("group");
    	String groupType = request.getParameter("groupType");
    	String partyId = request.getParameter("partyId");
    	
    	Locale locale = UtilHttp.getLocale(request);
    	HttpSession session = request.getSession(true);
    	List<EntityCondition> conditions = FastList.newInstance();
    	JSONArray dataMap = new JSONArray();
    	try {
    		if(UtilValidate.isNotEmpty(selectedGroup)) {
    			if(("SEGMENTATION".equals(groupType) || "ECONOMIC_METRIC".equals(groupType))) {
    				conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N")));
    				conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    				conditions.add(EntityCondition.makeCondition("groupId",EntityOperator.EQUALS,selectedGroup));
    			} else if("CUSTOM_FIELD".equals(groupType)) {
    				conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    				conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    				conditions.add(EntityCondition.makeCondition("groupId",EntityOperator.EQUALS,selectedGroup));
    			}
    		
    			EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    			Set<String> fieldToSelect = new HashSet<String>();
    			fieldToSelect.add("customFieldId");
    			fieldToSelect.add("customFieldName");
    			fieldToSelect.add("sequenceNumber");
    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);
    			List<GenericValue> customFields = delegator.findList("CustomField", condition, fieldToSelect, UtilMisc.toList("sequenceNumber ASC"), efo, false);

    			if(UtilValidate.isNotEmpty(customFields)) {
    				customFields.forEach( customField -> {
    					
    					try {
							if (UtilValidate.isNotEmpty(partyId)) {
								GenericValue valueAssociation = EntityUtil.getFirst(delegator.findByAnd("CustomFieldPartyClassification", UtilMisc.toMap("partyId", partyId, "customFieldId", customField.getString("customFieldId")), null, false));
								if (UtilValidate.isNotEmpty(valueAssociation)) {
									return;
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
    					
    					JSONObject listObj = new JSONObject();
    					listObj.put("customFieldId", customField.getString("customFieldId"));
    					listObj.put("customFieldName", UtilValidate.isNotEmpty(customField.getString("customFieldName")) ? customField.getString("customFieldName") : "");
    					listObj.put("sequenceNo", UtilValidate.isNotEmpty(customField.getString("sequenceNumber")) ? customField.getString("sequenceNumber") : "");
    					dataMap.add(listObj);
    				});
    			}
    		}	
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);

    	}
    	return doJSONResponse(response, dataMap);
    }
    
    /**
     * @author Mahendran T
     * @since 19-06-2018
     * */
    public static String getTemplateTagsAjax(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	String templateId= request.getParameter("templateId");
    	String templateTagTypeId = request.getParameter("templateTagTypeId");
    	List<String> selectTagList = FastList.newInstance();
    	List<String> nonSelectTagList = FastList.newInstance();
    	JSONObject obj = new JSONObject();
    	try {
    		List<EntityCondition> conditionList = FastList.newInstance();
    		Set<String> fieldToSelect = new HashSet<String>();
    		fieldToSelect.add("tagId");
    		fieldToSelect.add("tagName");
    		fieldToSelect.add("templateTagTypeId");
    		Timestamp nowTimestamp = UtilDateTime.nowTimestamp();
    		//conditionList.add(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N"));
    		conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("fromDate",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("fromDate",EntityOperator.LESS_THAN_EQUAL_TO,nowTimestamp)));
    		conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("thruDate",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("thruDate",EntityOperator.GREATER_THAN_EQUAL_TO,nowTimestamp)));
    		
    		if(UtilValidate.isNotEmpty(templateTagTypeId)) {
    			conditionList.add(EntityCondition.makeCondition("templateTagTypeId",EntityOperator.EQUALS,templateTagTypeId));
    		}
    		
    		// Get the configured tag list
    		List<String> configuredTagIds = null;
    		if(UtilValidate.isNotEmpty(templateId)) {
				List<GenericValue> templateTag = delegator.findByAnd("TemplateTag", UtilMisc.toMap("templateId",templateId), null, false);
				if(templateTag !=null && templateTag.size() > 0) {
					configuredTagIds = EntityUtil.getFieldListFromEntityList(templateTag, "tagId", true);
				}	
			}
    		
    		if(configuredTagIds !=null && configuredTagIds.size()>0) {
    			List<EntityCondition> condition1 = FastList.newInstance();
    			condition1.add(EntityCondition.makeCondition("tagId",EntityOperator.IN,configuredTagIds));
    			condition1.addAll(conditionList);
				List<GenericValue> dataTagConfigurations = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(condition1,EntityOperator.AND), fieldToSelect, null, null, false);
        		if(dataTagConfigurations != null && dataTagConfigurations.size() >0) {
        			dataTagConfigurations.forEach(e->{
        				selectTagList.add(e.getString("tagName")+" ["+e.getString("tagId")+"]");
        			});
        		}
    		}
			
    		
    		// Get not configured tag list
    		if(configuredTagIds !=null && configuredTagIds.size()>0) {
    			conditionList.add(EntityCondition.makeCondition("tagId",EntityOperator.NOT_IN,configuredTagIds));
    		}
    		
    		List<GenericValue> notConfiguredTagList = delegator.findList("DataTagConfiguration", EntityCondition.makeCondition(conditionList,EntityOperator.AND), fieldToSelect, null, null, false);
    		//EntityUtil.filterByDate(notConfiguredTagList, UtilDateTime.nowTimestamp());
    		if(notConfiguredTagList != null && notConfiguredTagList.size() >0) {
    			notConfiguredTagList.forEach(e->{
    				nonSelectTagList.add(e.getString("tagName")+" ["+e.getString("tagId")+"]");
    			});
    		}
    		obj.put("candidateItems", nonSelectTagList);
    		obj.put("selectionItems", selectTagList);
    	} catch (Exception e) {
			Debug.logInfo("Error :"+e.getMessage(), MODULE);
		}
    	
    	
    	return doJSONResponse(response, obj);
    	
    }
    
    /**
     * @author Mahendran T
     * @since 2018-06-27
     * */
    public static String getGroupingCode(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	String templateTagTypeId = request.getParameter("templateTagTypeId");

    	Locale locale = UtilHttp.getLocale(request);
    	HttpSession session = request.getSession(true);

    	JSONArray dataMap = new JSONArray();
    	try {
    		if(UtilValidate.isNotEmpty(templateTagTypeId)) {
    			/*List<EntityCondition> conditions = FastList.newInstance();

    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.EQUALS,null), EntityOperator.OR, EntityCondition.makeCondition("hide",EntityOperator.EQUALS,"Y")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,templateTagTypeId));*/
    			/*EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    			
    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);*/
    			//List<GenericValue> customFieldGroup = delegator.findList("CustomFieldGroup", condition, null, UtilMisc.toList("sequence ASC"), efo, false);
    			List<GenericValue> customFieldGroupingCode = delegator.findByAnd("CustomFieldGroupingCode", UtilMisc.toMap("groupType",templateTagTypeId), UtilMisc.toList("sequenceNumber ASC"), false);
    			if(customFieldGroupingCode != null && customFieldGroupingCode.size() > 0) {
    				customFieldGroupingCode.forEach(e->{
    					JSONObject listObj = new JSONObject();
    					listObj.put("customFieldGroupingCodeId", e.getString("customFieldGroupingCodeId"));
    					listObj.put("groupingCode", UtilValidate.isNotEmpty(e.getString("groupingCode")) ? e.getString("groupingCode") : "");
    					listObj.put("description", UtilValidate.isNotEmpty(e.getString("description")) ? e.getString("description") : "");
    					listObj.put("sequenceNo", UtilValidate.isNotEmpty(e.getString("sequenceNumber")) ? e.getString("sequenceNumber") : "");
    					dataMap.add(listObj);
    				});
    			}
    		} 
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);
    	}
    	return doJSONResponse(response, dataMap);
    }
    
    /**
     * @author Mahendran T
     * @since 2018-06-29
     * */
    public static void getSmartListJsonData(HttpServletRequest request,HttpServletResponse response) {
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	Locale locale = UtilHttp.getLocale(request);
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	JSONArray jsonArray = new JSONArray();
    	String roleType = request.getParameter("roleTypeId");
		
    	//String selectedGroup = request.getParameter("group");
    	
    	String groupType = request.getParameter("groupType");
    	List<EntityCondition> conditions = FastList.newInstance();

    	try {
    		List<GenericValue> campaignGroupingGvList = delegator.findList("CustomFieldGroupingCode", EntityCondition.makeCondition("groupingCode",EntityOperator.LIKE,"CAMPAIGN_%"), UtilMisc.toSet("customFieldGroupingCodeId"), null, null, false);
        	List<String> campaignGroupingList = FastList.newInstance();
        	if(campaignGroupingGvList != null && campaignGroupingGvList.size() > 0) {
        		campaignGroupingList = EntityUtil.getFieldListFromEntityList(campaignGroupingGvList, "customFieldGroupingCodeId", true);
    			if("SEGMENTATION".equals(groupType)){
    	    		conditions.add(EntityCondition.makeCondition("groupingCode",EntityOperator.NOT_IN,campaignGroupingList));
    			}
        	}
    		if(("SEGMENTATION".equals(groupType) || "ECONOMIC_METRIC".equals(groupType))) {
    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isActive",EntityOperator.NOT_EQUAL,"N")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    			
    		} else if("CUSTOM_FIELD".equals(groupType)) {
    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		}
    		
    		if(UtilValidate.isNotEmpty(roleType)) {
    			List<String> roleSegmentList = FastList.newInstance();
    			List<GenericValue> roleTypeSegmentList = delegator.findList("CustomFieldRoleConfig", EntityCondition.makeCondition("roleTypeId",EntityOperator.EQUALS,roleType), null, null, null, false);
    			roleSegmentList = EntityUtil.getFieldListFromEntityList(roleTypeSegmentList, "groupId", true);
    			conditions.add(EntityCondition.makeCondition("groupId",EntityOperator.IN,roleSegmentList));
    		}
    		
    		
    		if(conditions != null && conditions.size() > 0) {
				EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    			Set<String> fieldToSelect = new HashSet<String>();
    			fieldToSelect.add("groupId");
    			fieldToSelect.add("groupName");
    			fieldToSelect.add("sequence");

    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);
    			List<GenericValue> customFieldGroup = delegator.findList("CustomFieldGroup", condition, fieldToSelect, UtilMisc.toList("sequence ASC"), efo, false);
    			if(customFieldGroup != null && customFieldGroup.size() > 0) {
    				customFieldGroup.forEach(e->{
    					List<EntityCondition> valueConditions = FastList.newInstance();
    	    			if(("SEGMENTATION".equals(groupType) || "ECONOMIC_METRIC".equals(groupType))) {
    	    				valueConditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N")));
    	    				valueConditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		    		} else if("CUSTOM_FIELD".equals(groupType)) {
    		    			valueConditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    		    			valueConditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		    		}
    					JSONObject listObj = new JSONObject();
    					String groupId = e.getString("groupId");
    					listObj.put("id", groupId);
    					listObj.put("label", UtilValidate.isNotEmpty(e.getString("groupName")) ? e.getString("groupName") : "");
    					listObj.put("type", "string");
    					listObj.put("input", "select");
    					
    					JSONArray operatorArray = new JSONArray();
    					operatorArray.add("equal");
    					operatorArray.add("not_equal");
    					operatorArray.add("in");
    					operatorArray.add("not_in");
    					operatorArray.add("less");
    					operatorArray.add("less_or_equal");
    					operatorArray.add("greater");
    					operatorArray.add("greater_or_equal");
    					operatorArray.add("between");
    					operatorArray.add("not_between");
    					operatorArray.add("is_null");
    					operatorArray.add("is_not_null");
    					listObj.put("operators", operatorArray.toString());
    					JSONArray valuesArray = new JSONArray();
						try {
							valueConditions.add(EntityCondition.makeCondition("groupId",EntityOperator.EQUALS,groupId));
							List<GenericValue> customField = delegator.findList("CustomField",EntityCondition.makeCondition(valueConditions, EntityOperator.AND) , null, UtilMisc.toList("sequenceNumber ASC"), efo, false);
							if(customField != null && customField.size() > 0) {
	    		    			customField.forEach(e1->{
	    		    				JSONObject listObj1 = new JSONObject();
	    		    				listObj1.put(e1.getString("customFieldId"),UtilValidate.isNotEmpty(e1.getString("customFieldName")) ? e1.getString("customFieldName") : "");
	    		    				valuesArray.add(listObj1);
	    		    			});
	    		    		}
						} catch (GenericEntityException e2) {
							Debug.logError("Error : "+e2.getMessage(), MODULE);
						}
						listObj.put("values",valuesArray.toString());
						jsonArray.add(listObj.toString());
    				});
    			}
			}
    		PrintWriter pw = response.getWriter();
			pw.write(jsonArray.toString());
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);

    	}
    }
    
    /**
     * @author Mahendran T
     * @since 06-07-2018
     * */
    public static void getSmartlistSegmentationJsonData(HttpServletRequest request,HttpServletResponse response) {
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	Locale locale = UtilHttp.getLocale(request);
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

    	JSONArray jsonArray = new JSONArray();

    	String groupType = request.getParameter("groupType");
    	List<EntityCondition> conditions = FastList.newInstance();

    	try {
    		List<GenericValue> campaignGroupingGvList = delegator.findList("CustomFieldGroupingCode", EntityCondition.makeCondition("groupingCode",EntityOperator.LIKE,"CAMPAIGN_%"), UtilMisc.toSet("customFieldGroupingCodeId"), null, null, false);
    		List<String> campaignGroupingList = FastList.newInstance();

    		if("SEGMENTATION".equals(groupType)) {
    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isActive",EntityOperator.NOT_EQUAL,"N")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    			if(campaignGroupingGvList != null && campaignGroupingGvList.size() > 0) {
    				campaignGroupingList = EntityUtil.getFieldListFromEntityList(campaignGroupingGvList, "customFieldGroupingCodeId", true);
    				conditions.add(EntityCondition.makeCondition("groupingCode",EntityOperator.NOT_IN,campaignGroupingList));
    			}
    		} 

    		if(conditions != null && conditions.size() > 0) {
    			EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    			Set<String> fieldToSelect = new HashSet<String>();
    			fieldToSelect.add("groupId");
    			fieldToSelect.add("groupName");
    			fieldToSelect.add("sequence");

    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);
    			List<GenericValue> customFieldGroup = delegator.findList("CustomFieldGroup", condition, fieldToSelect, UtilMisc.toList("sequence ASC"), efo, false);
    			if(customFieldGroup != null && customFieldGroup.size() > 0) {
    				customFieldGroup.forEach(e->{
    					List<EntityCondition> valueConditions = FastList.newInstance();
    					if("SEGMENTATION".equals(groupType)) {
    						valueConditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N")));
    						valueConditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    					}
    					JSONObject listObj = new JSONObject();
    					String groupId = e.getString("groupId");
    					listObj.put("id", groupId);
    					listObj.put("label", UtilValidate.isNotEmpty(e.getString("groupName")) ? e.getString("groupName") : "");
    					listObj.put("type", "string");
    					listObj.put("input", "select");

    					JSONArray operatorArray = new JSONArray();
    					operatorArray.add("equal");
    					operatorArray.add("not_equal");
    					operatorArray.add("in");
    					operatorArray.add("not_in");
    					operatorArray.add("less");
    					operatorArray.add("less_or_equal");
    					operatorArray.add("greater");
    					operatorArray.add("greater_or_equal");
    					operatorArray.add("between");
    					//operatorArray.add("not_between");
    					operatorArray.add("is_null");
    					operatorArray.add("is_not_null");
    					listObj.put("operators", operatorArray.toString());
    					
    					
    					JSONArray valuesArray = new JSONArray();
    					try {
    						valueConditions.add(EntityCondition.makeCondition("groupId",EntityOperator.EQUALS,groupId));
    						List<GenericValue> customField = delegator.findList("CustomField",EntityCondition.makeCondition(valueConditions, EntityOperator.AND) , null, UtilMisc.toList("sequenceNumber ASC"), efo, false);
    						if(customField != null && customField.size() > 0) {
    							customField.forEach(e1->{
    								JSONObject listObj1 = new JSONObject();
    								listObj1.put(e1.getString("customFieldId"),UtilValidate.isNotEmpty(e1.getString("customFieldName")) ? e1.getString("customFieldName") : "");
    								valuesArray.add(listObj1);
    							});
    						}
    					} catch (GenericEntityException e2) {
    						Debug.logError("Error : "+e2.getMessage(), MODULE);
    					}
    					listObj.put("values",valuesArray.toString());
    					jsonArray.add(listObj.toString());
    				});
    			}
    		}
    		PrintWriter pw = response.getWriter();
    		pw.write(jsonArray.toString());
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);

    	}
    }
    
    /**
     * @author Mahendran T
     * @since 06-07-2018
     * */
    public static void getSmartlistAttrAndEconomicJsonData(HttpServletRequest request,HttpServletResponse response) {
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	Locale locale = UtilHttp.getLocale(request);
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	
    	JSONArray jsonArray = new JSONArray();
    	String roleType = request.getParameter("roleTypeId");
		
    	//String selectedGroup = request.getParameter("group");
    	
    	String groupType = request.getParameter("groupType");
    	List<EntityCondition> conditions = FastList.newInstance();

    	try {
    		/*List<GenericValue> campaignGroupingGvList = delegator.findList("CustomFieldGroupingCode", EntityCondition.makeCondition("groupingCode",EntityOperator.LIKE,"CAMPAIGN_%"), UtilMisc.toSet("customFieldGroupingCodeId"), null, null, false);
        	List<String> campaignGroupingList = FastList.newInstance();
        	if(campaignGroupingGvList != null && campaignGroupingGvList.size() > 0) {
        		campaignGroupingList = EntityUtil.getFieldListFromEntityList(campaignGroupingGvList, "customFieldGroupingCodeId", true);
        	}*/
    		if( "ECONOMIC_METRIC".equals(groupType)) {
    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isActive",EntityOperator.NOT_EQUAL,"N")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    			
    		} else if("CUSTOM_FIELD".equals(groupType)) {
    			conditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    			conditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		}
    		
    		/*if(UtilValidate.isNotEmpty(roleType)) {
    			List<String> roleSegmentList = FastList.newInstance();
    			List<GenericValue> roleTypeSegmentList = delegator.findList("CustomFieldRoleConfig", EntityCondition.makeCondition("roleTypeId",EntityOperator.EQUALS,roleType), null, null, null, false);
    			roleSegmentList = EntityUtil.getFieldListFromEntityList(roleTypeSegmentList, "groupId", true);
    			conditions.add(EntityCondition.makeCondition("groupId",EntityOperator.IN,roleSegmentList));
    		}*/
    		
    		
    		if(conditions != null && conditions.size() > 0) {
				EntityCondition condition = EntityCondition.makeCondition(conditions, EntityOperator.AND);
    			Set<String> fieldToSelect = new HashSet<String>();
    			fieldToSelect.add("groupId");
    			fieldToSelect.add("groupName");
    			fieldToSelect.add("sequence");

    			EntityFindOptions efo = new EntityFindOptions();
    			efo.setDistinct(true);
    			List<GenericValue> customFieldGroup = delegator.findList("CustomFieldGroup", condition, fieldToSelect, UtilMisc.toList("sequence ASC"), efo, false);
    			if(customFieldGroup != null && customFieldGroup.size() > 0) {
    				customFieldGroup.forEach(e->{
    					List<EntityCondition> valueConditions = FastList.newInstance();
    	    			if("ECONOMIC_METRIC".equals(groupType)) {
    	    				valueConditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("isEnabled",EntityOperator.NOT_EQUAL,"N")));
    	    				valueConditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		    		} else if("CUSTOM_FIELD".equals(groupType)) {
    		    			valueConditions.add(EntityCondition.makeCondition(EntityCondition.makeCondition("hide",EntityOperator.NOT_EQUAL,"Y")));
    		    			valueConditions.add(EntityCondition.makeCondition("groupType",EntityOperator.EQUALS,groupType));
    		    		}
    	    			String groupId = e.getString("groupId");
    	    			String groupName = UtilValidate.isNotEmpty(e.getString("groupName")) ? e.getString("groupName") : "";
    					JSONArray valuesArray = new JSONArray();
						try {
							valueConditions.add(EntityCondition.makeCondition("groupId",EntityOperator.EQUALS,groupId));
							List<GenericValue> customField = delegator.findList("CustomField",EntityCondition.makeCondition(valueConditions, EntityOperator.AND) , null, UtilMisc.toList("sequenceNumber ASC"), efo, false);
							if(customField != null && customField.size() > 0) {
	    		    			customField.forEach(e1->{
	    		    				JSONObject listObj = new JSONObject();
	    	    					
	    	    					listObj.put("id", e1.getString("customFieldId"));
	    	    					listObj.put("label", UtilValidate.isNotEmpty(e1.getString("customFieldName")) ? "("+groupName+")" + e1.getString("customFieldName") : "");
	    	    					listObj.put("type", "string");
	    	    					listObj.put("input", "text");
	    	    					
	    	    					JSONArray operatorArray = new JSONArray();
	    	    					operatorArray.add("equal");
	    	    					operatorArray.add("not_equal");
	    	    					operatorArray.add("in");
	    	    					operatorArray.add("not_in");
	    	    					operatorArray.add("less");
	    	    					operatorArray.add("less_or_equal");
	    	    					operatorArray.add("greater");
	    	    					operatorArray.add("greater_or_equal");
	    	    					operatorArray.add("between");
	    	    					//operatorArray.add("not_between");
	    	    					operatorArray.add("is_null");
	    	    					operatorArray.add("is_not_null");
	    	    					listObj.put("operators", operatorArray.toString());
	    	    					listObj.put("values","");
	    							jsonArray.add(listObj.toString());
	    		    			});
	    		    		}
						} catch (GenericEntityException e2) {
							Debug.logError("Error : "+e2.getMessage(), MODULE);
						}
    				});
    			}
			}
    		PrintWriter pw = response.getWriter();
			pw.write(jsonArray.toString());
    	} catch (Exception e) {
    		Debug.logInfo("Error : "+e.getMessage(), MODULE);

    	}
    }
    
    public static String getAccountRelatedContacts(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String accountPartyId = request.getParameter("accountPartyId");
        String contactListId = request.getParameter("contactListId");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > contactList = FastList.newInstance();
        try {
            if (UtilValidate.isNotEmpty(accountPartyId) && UtilValidate.isNotEmpty(contactListId)) {
                List < EntityCondition > conditions = new ArrayList < EntityCondition > ();
                List < EntityCondition > contactConditions = new ArrayList < EntityCondition > ();
                conditions.add(EntityCondition.makeCondition(EntityOperator.AND,
                    EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
                    EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
                    EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, accountPartyId),
                    EntityUtil.getFilterByDateExpr(),
                    EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV")));

                // remove disabled parties
                conditions.add(EntityCondition.makeCondition(EntityOperator.OR,
                    EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
                    EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)));

                List < GenericValue > partyFromReln = delegator.findList("PartyFromByRelnAndContactInfoAndPartySupplemantalData", EntityCondition.makeCondition(conditions, EntityOperator.AND), UtilMisc.toSet("partyId"), UtilMisc.toList("createdDate"), null, false);
                if (partyFromReln != null && partyFromReln.size() > 0) {
                    List < String > partyIdFromReln = EntityUtil.getFieldListFromEntityList(partyFromReln, "partyId", true);
                    if (partyIdFromReln != null && partyIdFromReln.size() > 0) {
                        contactConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.IN, partyIdFromReln));
                        List < GenericValue > campaignContactListParty = EntityQuery.use(delegator).from("CampaignContactListParty")
                            .where(EntityCondition.makeCondition("partyId", EntityOperator.IN, partyIdFromReln),
                                EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                                EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, accountPartyId))
                            .filterByDate().queryList();
                        if (campaignContactListParty != null && campaignContactListParty.size() > 0) {
                            contactConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.NOT_IN, EntityUtil.getFieldListFromEntityList(campaignContactListParty, "partyId", true)));
                        }
                        
                        List < GenericValue > partySummaryDetailsViewCount = delegator.findList("PartySummaryDetailsView", EntityCondition.makeCondition(contactConditions, EntityOperator.AND), UtilMisc.toSet("partyId"), UtilMisc.toList("createdDate"), null, false);
                        if (partySummaryDetailsViewCount != null && partySummaryDetailsViewCount.size() > 0) {
                            int count = partySummaryDetailsViewCount.size();
                            int recordsFiltered = count;
                            int recordsTotal = count;
                            EntityFindOptions efo = new EntityFindOptions();
                            efo.setDistinct(true);
                            int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                            int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                            efo.setOffset(startInx);
                            efo.setLimit(endInx);
                            List < GenericValue > PartySummaryDetailsViewList = delegator.findList("PartySummaryDetailsView", EntityCondition.makeCondition(contactConditions, EntityOperator.AND), null, UtilMisc.toList("createdDate"), efo, false);
                            if (PartySummaryDetailsViewList != null && PartySummaryDetailsViewList.size() > 0) {
                                int id = 1;
                                for (GenericValue partySummaryDetailsView: PartySummaryDetailsViewList) {
                                    Map < String, Object > partyDetails = FastMap.newInstance();
                                    String emailSolicitation = "N";
                                    String phoneSolicitation = "N";
                                    String infoString = "";
                                    String phoneNumber = "";
                                    id = id + 1;
                                    List < GenericValue > partyContactMechs = EntityQuery.use(delegator).from("PartyContactMech")
                                        .where("partyId", partySummaryDetailsView.getString("partyId"), "allowSolicitation", "Y")
                                        .filterByDate().queryList();

                                    if (partyContactMechs != null && partyContactMechs.size() > 0) {
                                        List < String > contactMechId = EntityUtil.getFieldListFromEntityList(partyContactMechs, "contactMechId", true);
                                        Set < String > findOptions = UtilMisc.toSet("contactMechId");
                                        List < String > orderBy = UtilMisc.toList("createdStamp DESC");

                                        EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partySummaryDetailsView.getString("partyId"));
                                        EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, contactMechId);
                                        EntityCondition primaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE")));
                                        List < GenericValue > primaryPhones = delegator.findList("PartyContactMechPurpose", primaryPhoneConditions, findOptions, orderBy, null, true);
                                        if (primaryPhones != null && primaryPhones.size() > 0) {
                                            phoneSolicitation = "Y";
                                            GenericValue primaryPhone = EntityUtil.getFirst(primaryPhones);
                                            GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", primaryPhone.getString("contactMechId")), false);
                                            if (primaryPhoneNumber != null && primaryPhoneNumber.size() > 0) {
                                                String countryCode = primaryPhoneNumber.getString("countryCode");
                                                String areaCode = primaryPhoneNumber.getString("areaCode");
                                                String contactNumber = primaryPhoneNumber.getString("contactNumber");
                                                if (countryCode != null && areaCode != null && contactNumber != null) {
                                                    phoneNumber += countryCode + "-" + areaCode + "-" + contactNumber;
                                                } else if (areaCode != null && contactNumber != null) {
                                                    phoneNumber += areaCode + "-" + contactNumber;
                                                } else if (contactNumber != null) {
                                                    phoneNumber = contactNumber;
                                                }
                                            }
                                        }
                                        EntityCondition primaryEmailaddressConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL")));
                                        List < GenericValue > EmailAddresses = delegator.findList("PartyContactMechPurpose", primaryEmailaddressConditions, findOptions, orderBy, null, true);
                                        if (EmailAddresses != null && EmailAddresses.size() > 0) {
                                            emailSolicitation = "Y";

                                            GenericValue EmailAddress = EntityUtil.getFirst(EmailAddresses);
                                            GenericValue EmailAddressValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", EmailAddress.getString("contactMechId")), false);
                                            if (EmailAddressValue != null && EmailAddressValue.size() > 0) {
                                                if (UtilValidate.isNotEmpty(EmailAddressValue.getString("infoString")))
                                                    infoString = EmailAddressValue.getString("infoString");
                                            }
                                        }
                                    }
                                    partyDetails.put("id", id + "");
                                    partyDetails.put("name", partySummaryDetailsView.getString("firstName") + " " + partySummaryDetailsView.getString("lastName"));
                                    partyDetails.put("partyId", partySummaryDetailsView.getString("partyId"));
                                    partyDetails.put("title", partySummaryDetailsView.getString("generalProfTitle"));
                                    partyDetails.put("emailAddress", infoString);
                                    partyDetails.put("phoneNumber", phoneNumber);
                                    partyDetails.put("emailSolicitation", emailSolicitation);
                                    partyDetails.put("phoneSolicitation", phoneSolicitation);
                                    contactList.add(partyDetails);
                                }

                                returnMap.put("data", contactList);
                                returnMap.put("draw", draw);
                                returnMap.put("recordsTotal", recordsTotal);
                                returnMap.put("recordsFiltered", recordsFiltered);
                            }
                        } else {
                            returnMap.put("data", contactList);
                            returnMap.put("draw", draw);
                            returnMap.put("recordsTotal", 0);
                            returnMap.put("recordsFiltered", 0);
                            return AjaxEvents.doJSONResponse(response, returnMap);
                        }
                    }
                } else {
                    returnMap.put("data", contactList);
                    returnMap.put("draw", draw);
                    returnMap.put("recordsTotal", 0);
                    returnMap.put("recordsFiltered", 0);
                    return AjaxEvents.doJSONResponse(response, returnMap);
                }
            }
        } catch (Exception e) {
            Debug.logError("Exception in Get Account Related Contact" + e.getMessage(), MODULE);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    
    public static String getOutBoundCallList(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        Map < String, Object > result = FastMap.newInstance();
        List < Object > data = FastList.newInstance();
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String campaignId = request.getParameter("campaignId");
        String csrPartyId = request.getParameter("csrPartyId");
        String csrPartyList = request.getParameter("csrPartyList");
        String callStatus = request.getParameter("callStatus");
        String noOfDaysSinceLastCall = request.getParameter("noOfDaysSinceLastCall");
        String callBackDate = request.getParameter("callBackDate");
        String defaultTimeZoneId = request.getParameter("defaultTimeZoneId");
        int recordsTotal = 0;
        int recordsFiltered = 0;
        EntityListIterator eli = null;
        try {
            List < EntityCondition > conditions = FastList.newInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
            DynamicViewEntity callRecordMasterAndMktCampViewEntity = new DynamicViewEntity();
            callRecordMasterAndMktCampViewEntity.addMemberEntity("MKTC", "MarketingCampaign");
            callRecordMasterAndMktCampViewEntity.addMemberEntity("CRM", "CallRecordMaster");
            callRecordMasterAndMktCampViewEntity.addViewLink("MKTC", "CRM", Boolean.FALSE, UtilMisc.toList(new ModelKeyMap("marketingCampaignId", "marketingCampaignId")));
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "marketingCampaignId", null, null, null, Boolean.TRUE, null);
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "contactListId", null, null, null, Boolean.TRUE, null);
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "partyId", null, null, null, Boolean.TRUE, null);
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "loyaltyId");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "externalReferenceId");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "firstName");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "lastName");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "createdDate");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "countryCode");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "areaCode");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "phoneNumber");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "solicitationStatus");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "csrPartyId");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "lastCallStatusId");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "lastCallDuration");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "totalCallsMakeContact");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "totalCallsByCamp");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "totalCallDuration");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "firstContactDate");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "lastContactDate");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "nextActionStatusId");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "callNumber");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "callBackDate");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "localTimeZone");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "accountManager");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "city");
            callRecordMasterAndMktCampViewEntity.addAlias("CRM", "state");
            callRecordMasterAndMktCampViewEntity.addAlias("MKTC", "campaignTypeId");
            callRecordMasterAndMktCampViewEntity.addAlias("MKTC", "statusId");
            callRecordMasterAndMktCampViewEntity.addAlias("MKTC", "campaignName");
            callRecordMasterAndMktCampViewEntity.addAlias("MKTC", "startDate");
            callRecordMasterAndMktCampViewEntity.addAlias("MKTC", "endDate");

            if (UtilValidate.isNotEmpty(campaignId)) {
                conditions.add(EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, campaignId));
            }
            if (UtilValidate.isNotEmpty(csrPartyId)) {
                conditions.add(EntityCondition.makeCondition("csrPartyId", EntityOperator.EQUALS, csrPartyId));
            } else if (UtilValidate.isEmpty(csrPartyId) && UtilValidate.isNotEmpty(csrPartyList)) {
                csrPartyList = csrPartyList.replace("[", "(");
                csrPartyList = csrPartyList.replace("]", ")");
                conditions.add(EntityCondition.makeCondition("csrPartyId", EntityOperator.IN, csrPartyList));
            }
            if (UtilValidate.isNotEmpty(callStatus)) {
                conditions.add(EntityCondition.makeCondition("lastCallStatusId", EntityOperator.EQUALS, callStatus));
            }
            if (UtilValidate.isNotEmpty(noOfDaysSinceLastCall)) {
                Integer i = Integer.valueOf(noOfDaysSinceLastCall);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, -i);
                String sinceCallDate = formatter1.format(cal.getTime());
                conditions.add(EntityCondition.makeCondition("lastContactDate", EntityOperator.EQUALS, java.sql.Date.valueOf(sinceCallDate)));
            }
            if (UtilValidate.isNotEmpty(callBackDate)) {
                Date callBackDate1 = new SimpleDateFormat("dd-MM-yyyy").parse(callBackDate);
                callBackDate = formatter1.format(callBackDate1);
                conditions.add(EntityCondition.makeCondition("callBackDate", EntityOperator.EQUALS, java.sql.Date.valueOf(callBackDate)));
            }
            /*if (UtilValidate.isNotEmpty(defaultTimeZoneId)) {
                conditions.add(EntityCondition.makeCondition("localTimeZone", EntityOperator.EQUALS, defaultTimeZoneId));
            }*/
            conditions.add(EntityCondition.makeConditionDate("startDate", "endDate"));
            List < GenericValue > CRMAndMKCT = new LinkedList < GenericValue > ();
            // get the index for the partial list
            int viewSize = Integer.parseInt(length);
            int viewIndex = Integer.parseInt(start);
            int lowIndex = (viewIndex + 1);
            int highIndex = viewSize;
            eli = EntityQuery.use(delegator).from(callRecordMasterAndMktCampViewEntity)
                .where(conditions)
                .queryIterator();
            recordsTotal = eli.getResultsSizeAfterPartialList();
            eli.beforeFirst();
            if (recordsTotal > highIndex) {
                CRMAndMKCT = eli.getPartialList(lowIndex, highIndex);
            } else if (recordsTotal > 0) {
                CRMAndMKCT = eli.getCompleteList();
            }

            if (highIndex > recordsTotal) {
                highIndex = recordsTotal;
            }

            if (CRMAndMKCT != null && CRMAndMKCT.size() > 0) {
                for (GenericValue CRMAndMKCTGV: CRMAndMKCT) {
                    Map < String, Object > outboundCallMap = FastMap.newInstance();
                    List < Map < String, Object >> outboundCallList = new ArrayList < Map < String, Object >> ();
                    String campaignListId = "";
                    String csrFirstName = "";
                    String csrLastName = "";
                    String noteValidation = "N";
                    String diffDays = "";
                    String callStatusDesc = "";
                    String countryCode = "";
                    String areaCode = "";
                    String contactNumber = "";
                    String accountName = "";
                    String accountId = "";
                    String partyStatusId = "";
                    String partyId = CRMAndMKCTGV.getString("partyId");

                    String marketingCampaignId = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("marketingCampaignId")) ? CRMAndMKCTGV.getString("marketingCampaignId") : "";
                    String campaignName = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("campaignName")) ? CRMAndMKCTGV.getString("campaignName") : "";
                    String firstName = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("firstName")) ? CRMAndMKCTGV.getString("firstName") : "";
                    String lastName = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("lastName")) ? CRMAndMKCTGV.getString("lastName") : "";
                    String callBackDates = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("callBackDate")) ? CRMAndMKCTGV.getString("callBackDate") : "";
                    String assigned = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("csrPartyId")) ? CRMAndMKCTGV.getString("csrPartyId") : "";
                    String contactListId = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("contactListId")) ? CRMAndMKCTGV.getString("contactListId") : "";
                    String startDate = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("startDate")) ? CRMAndMKCTGV.getString("startDate") : "";
                    String endDate = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("endDate")) ? CRMAndMKCTGV.getString("endDate") : "";
                    String lastContactDate = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("lastContactDate")) ? CRMAndMKCTGV.getString("lastContactDate") : "";
                    String accountManager = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("accountManager")) ? CRMAndMKCTGV.getString("accountManager") : "N";
                    String callStatusId = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("lastCallStatusId")) ? CRMAndMKCTGV.getString("lastCallStatusId") : "";
                    String city = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("city")) ? CRMAndMKCTGV.getString("city") : "";
                    String state = UtilValidate.isNotEmpty(CRMAndMKCTGV.getString("state")) ? CRMAndMKCTGV.getString("state") : "";

                    if (UtilValidate.isNotEmpty(startDate)) {
                        Date startDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate);
                        startDate = formatter.format(startDate1);
                    }
                    if (UtilValidate.isNotEmpty(endDate)) {
                        Date endDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
                        endDate = formatter.format(endDate1);
                    }
                    if (UtilValidate.isNotEmpty(assigned)) {
                        GenericValue person = delegator.findOne("Person", UtilMisc.toMap("partyId", assigned), false);
                        if (UtilValidate.isNotEmpty(person)) {
                            csrFirstName = person.getString("firstName");
                            csrLastName = person.getString("lastName");
                        }
                    }

                    GenericValue marketingCampaignContactList = EntityUtil.getFirst(delegator.findByAnd("MarketingCampaignContactList", UtilMisc.toMap("contactListId", contactListId, "marketingCampaignId", marketingCampaignId), UtilMisc.toList("lastUpdatedStamp DESC"), false));
                    if (UtilValidate.isNotEmpty(marketingCampaignContactList)) {
                        campaignListId = marketingCampaignContactList.getString("campaignListId");
                    }

                    List < GenericValue > partyNoteViewList = delegator.findByAnd("PartyNoteView", UtilMisc.toMap("targetPartyId", partyId, "isImportant", "Y"), null, false);
                    if (partyNoteViewList != null && partyNoteViewList.size() > 0) {
                        noteValidation = "Y";
                    }

                    if (UtilValidate.isNotEmpty(lastContactDate)) {
                        String currentDate = formatter.format(new Date());
                        Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(lastContactDate);
                        Date d2 = formatter.parse(currentDate);
                        long diff = d2.getTime() - d1.getTime();
                        long diffDay = diff / (24 * 60 * 60 * 1000);
                        diffDays = String.valueOf(diffDay);
                    }

                    if (UtilValidate.isNotEmpty(callStatusId)) {
                        GenericValue enumeration = delegator.findOne("Enumeration", UtilMisc.toMap("enumId", callStatusId), false);
                        if (UtilValidate.isNotEmpty(enumeration)) {
                            callStatusDesc = enumeration.getString("description");
                        }
                    }
                    if (UtilValidate.isNotEmpty(callBackDates)) {
                        Date callBackDates1 = new SimpleDateFormat("yyyy-MM-dd").parse(callBackDates);
                        callBackDates = formatter.format(callBackDates1);
                    }
                    if (UtilValidate.isNotEmpty(lastContactDate)) {
                        Date lastContactDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(lastContactDate);
                        lastContactDate = formatter.format(lastContactDate1);
                    }

                    List < GenericValue > partyContactMechs = delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId", partyId, "allowSolicitation", "Y"), null, false);
                    if (partyContactMechs != null && partyContactMechs.size() > 0) {
                        partyContactMechs = EntityUtil.filterByDate(partyContactMechs);
                        if (partyContactMechs != null && partyContactMechs.size() > 0) {
                            partyContactMechs = EntityUtil.getFieldListFromEntityList(partyContactMechs, "contactMechId", true);
                        }
                        if (partyContactMechs != null && partyContactMechs.size() > 0) {
                            Set < String > findOptions = UtilMisc.toSet("contactMechId");
                            List < String > orderBy = UtilMisc.toList("createdStamp DESC");

                            EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
                            EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, partyContactMechs);

                            EntityCondition primaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE")));
                            List < GenericValue > primaryPhones = delegator.findList("PartyContactMechPurpose", primaryPhoneConditions, findOptions, orderBy, null, true);
                            if (primaryPhones != null && primaryPhones.size() > 0) {
                                GenericValue primaryPhone = EntityUtil.getFirst(EntityUtil.filterByDate(primaryPhones));
                                if (UtilValidate.isNotEmpty(primaryPhone)) {
                                    GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", primaryPhone.getString("contactMechId")), false);
                                    if (UtilValidate.isNotEmpty(primaryPhoneNumber)) {
                                        countryCode = primaryPhoneNumber.getString("countryCode");
                                        areaCode = primaryPhoneNumber.getString("areaCode");
                                        contactNumber = primaryPhoneNumber.getString("contactNumber");

                                    }
                                }
                            }
                        }
                    }
                    GenericValue partyRelationshipAccountGV = EntityQuery.use(delegator).from("PartyRelationship")
                        .where(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
                            EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT"),
                            EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "CONTACT_REL_INV")
                        ))).filterByDate().orderBy("lastUpdatedStamp DESC").queryFirst();
                    if (partyRelationshipAccountGV != null && partyRelationshipAccountGV.size() > 0) {
                        GenericValue partyGroupGV = EntityQuery.use(delegator).from("PartyGroup").where("partyId", partyRelationshipAccountGV.getString("partyIdTo")).queryOne();
                        if (partyGroupGV != null && partyGroupGV.size() > 0) {
                            accountId = partyGroupGV.getString("partyId");
                            accountName = partyGroupGV.getString("groupName");
                        }
                    }

                    GenericValue partyGVAL = EntityQuery.use(delegator).from("Party").where("partyId", partyId).queryOne();
                    if (partyGVAL != null && partyGVAL.size() > 0) {
                        partyStatusId = partyGVAL.getString("statusId");
                    }
                    List < GenericValue > partyRoleList = EntityQuery.use(delegator).from("PartyRole")
                        .where(EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, UtilMisc.toList("LEAD", "CONTACT")),
                            EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)).queryList();
                    if (partyRoleList != null && partyRoleList.size() > 1) {
                        if ("LEAD_CONVERTED".equals(partyStatusId)) {
                            partyStatusId = "CONTACT";
                        } else {
                            partyStatusId = "LEAD";
                        }
                    } else if (partyRoleList != null && partyRoleList.size() == 1) {
                        partyStatusId = partyRoleList.get(0).getString("roleTypeId");
                    }

                    outboundCallMap.put("partyId", partyId);
                    outboundCallMap.put("contactListId", contactListId);
                    outboundCallMap.put("campaignListId", campaignListId);
                    outboundCallMap.put("marketingCampaignId", marketingCampaignId);
                    outboundCallMap.put("campaignName", campaignName);
                    outboundCallMap.put("startDate", startDate);
                    outboundCallMap.put("endDate", endDate);
                    outboundCallMap.put("contactName", firstName + " " + lastName);
                    outboundCallMap.put("partyStatusId", partyStatusId);
                    outboundCallMap.put("accountId", accountId);
                    outboundCallMap.put("accountName", accountName);
                    outboundCallMap.put("countryCode", countryCode);
                    outboundCallMap.put("areaCode", areaCode);
                    outboundCallMap.put("contactNumber", contactNumber);
                    outboundCallMap.put("city", city);
                    outboundCallMap.put("state", state);
                    outboundCallMap.put("accountManager", accountManager);
                    outboundCallMap.put("rmId", assigned);
                    outboundCallMap.put("rmName", csrFirstName + " " + csrLastName);
                    outboundCallMap.put("callStatus", callStatusDesc);
                    outboundCallMap.put("diffDays", diffDays);
                    outboundCallMap.put("noOfDateSinceLastCall", lastContactDate);
                    outboundCallMap.put("callBackDate", callBackDates);
                    outboundCallMap.put("noteValidation", noteValidation);
                    data.add(outboundCallMap);

                }
            }
            result.put("draw", draw);
            result.put("data", data);
            result.put("recordsTotal", recordsTotal);
            result.put("recordsFiltered", recordsTotal);

        } catch (Exception e) {
            Debug.logError("Exception in Get Outbound Call List" + e.getMessage(), MODULE);
            result.put("data", data);
            result.put("draw", draw);
            result.put("recordsTotal", 0);
            result.put("recordsFiltered", 0);
            return doJSONResponse(response, result);
        } finally {
            if (eli != null) {
                try {
                    eli.close();
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, e.getMessage(), MODULE);
                }
            }
        }
        return doJSONResponse(response, result);
    }
    
    public static String setCallListToSession(HttpServletRequest request, HttpServletResponse response) {
        String marketingCampaignId = request.getParameter("marketingCampaignId");
        String partyId = request.getParameter("partyId");
        System.out.println("marketingCampaignId========="+marketingCampaignId);
        System.out.println("partyId========="+partyId);
        Map < String, Object > result = FastMap.newInstance();
        HttpSession session = request.getSession();
        if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(partyId)){
            try {
                session.setAttribute("setMarketingCampaignId_"+partyId, marketingCampaignId);
                session.setAttribute("setCampaignPartyId_"+partyId, partyId);
                result.put("status", "success");
            } catch(Exception e) {
                Debug.logError("Call Status Update"+e.getMessage(), MODULE);
            }
        
        }
        return doJSONResponse(response, result);
    }
    
    public static String getPartyNotes(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        Map < String, Object > result = FastMap.newInstance();
        String partyId = request.getParameter("partyId");
        List < Map < String, Object >> data = new ArrayList < Map < String, Object >> ();
        try {
        List<GenericValue> partyNoteViewList = delegator.findByAnd("PartyNoteView", UtilMisc.toMap("targetPartyId", partyId, "isImportant", "Y"), UtilMisc.toList("-noteDateTime"), false);
        if( partyNoteViewList != null && partyNoteViewList.size()>0){
            for(GenericValue partyNoteView : partyNoteViewList){
                Map< String, Object > partyNoteViewMap = new HashMap< String, Object >();
                String noteParty = partyNoteView.getString("noteParty");
                String noteDateTime = partyNoteView.getString("noteDateTime");
                if(UtilValidate.isNotEmpty(noteParty)){
                    String partyName = PartyHelper.getPartyName(delegator, noteParty, false);
                    partyNoteViewMap.put("partyName", partyName);
                }
                if(UtilValidate.isNotEmpty(noteDateTime)){
                    Date noteDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(noteDateTime);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    noteDateTime = formatter.format(noteDate);
                    partyNoteViewMap.put("noteDateTime", noteDateTime);
                }
                partyNoteViewMap.put("noteInfo", partyNoteView.getString("noteInfo"));
                //partyNoteViewMap.putAll(partyNoteView);
                data.add(partyNoteViewMap);
            }
        }
        result.put("partyNotes", data);
        } catch(Exception e) {
            Debug.logError("Exception in Get Party Notes"+e.getMessage(), MODULE);
            result.put("partyNotes", data);
            return doJSONResponse(response, result);
        }
        return doJSONResponse(response, result);
    }
    
    public static String getSmartListTemplateBuilderList(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException { 
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		Locale locale = UtilHttp.getLocale(request);
		
		String draw = request.getParameter("draw");
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		String smartListName = request.getParameter("smartListName");
		
		JSONObject obj = new JSONObject();
		JSONArray dataMap = new JSONArray();
		long recordsFiltered = 0;
		long recordsTotal = 0;
		List<GenericValue> smartListTemplateList = null;
		try {
			List<String> orderBy = FastList.newInstance();
			String orderField = "";
			String sortDir= "ASC";
			String orderColumnId = request.getParameter("order[0][column]");
			if(UtilValidate.isNotEmpty(orderColumnId)) {
				int sortColumnId = Integer.parseInt(orderColumnId);
				String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
				sortDir = request.getParameter("order[0][dir]").toUpperCase();
				orderField = sortColumnName;
			} else {
				orderField = "smartListId";
			}
			List<EntityCondition> conditionList = FastList.newInstance();
			if(UtilValidate.isNotEmpty(smartListName)) {
				//conditionList.add(EntityCondition.makeCondition("name",EntityOperator.LIKE, "%"+ smartListName+ "%"));
				conditionList.add(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("name",EntityOperator.LIKE, "%"+ smartListName+ "%"),
			             EntityCondition.makeCondition("smartListId",EntityOperator.LIKE, "%"+ smartListName+ "%")
			             ), EntityOperator.OR));
			}

			EntityCondition condition = EntityCondition.makeCondition(conditionList,EntityOperator.AND);
			
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);			
			
			long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("SmartlistTemplateBuilder", condition, null, UtilMisc.toSet("smartListId"), efoNum);
			
			/*List<GenericValue> smartListTemplateList1 = delegator.findList("SmartlistTemplateBuilder", condition, null, null, efo, false);
			int count = smartListTemplateList1.size();*/
			
			recordsFiltered = count;
			recordsTotal = count;
			
			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
			efo.setOffset(startInx);
			efo.setLimit(endInx);
			
			smartListTemplateList = delegator.findList("SmartlistTemplateBuilder", condition, null, UtilMisc.toList("lastUpdatedTxStamp DESC"), efo, false);
			if(smartListTemplateList != null && smartListTemplateList.size() > 0) {
				smartListTemplateList.forEach(e->{
					JSONObject listObj = new JSONObject();
					String id = e.getString("smartListId");
					
					listObj.put("smartListId", id);
					listObj.put("name", UtilValidate.isNotEmpty(e.getString("name")) ? e.getString("name") : "");
					listObj.put("type", UtilValidate.isNotEmpty(e.getString("type")) ? e.getString("type") : "");
					listObj.put("marketingCampaignId", UtilValidate.isNotEmpty(e.getString("marketingCampaignId")) ? e.getString("marketingCampaignId") : "");
					listObj.put("isProcessed", UtilValidate.isNotEmpty(e.getString("isProcessed")) ? e.getString("isProcessed") : "");
					dataMap.add(listObj);
				});
			}
			obj.put("data", dataMap);
			obj.put("draw", draw);
			obj.put("recordsTotal", recordsTotal);
			obj.put("recordsFiltered", recordsFiltered);
		} catch(Exception er) {
			Debug.logInfo("Error-"+er.getMessage(), MODULE);
		}
		return doJSONResponse(response, obj);
    }
    
    
    
    
    public static String getCampaignDetails(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String campaignId = request.getParameter("campaignId");
        String campaignName = request.getParameter("campaignName");
        String statusIdVal = request.getParameter("statusId");
        String campaignType = request.getParameter("campaignType");
        String fromDate = request.getParameter("fromDate");
        String thruDate = request.getParameter("thruDate");
        String contactNumber = request.getParameter("contactNumber");
        String parentCampaignId= request.getParameter("parentCampaignId");
        String masterCampaignId= request.getParameter("masterCampaignId");
        String isDripCampaign= request.getParameter("isDripCampaign");
        String createdBy= request.getParameter("createdBy");
        String createdDate= request.getParameter("createdDate");
        String campaignHierarchyType= request.getParameter("campaignHierarchyType");
        String masterParentCampaignId= request.getParameter("masterParentCampaignId");
        
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > findcamp = FastList.newInstance();
        try {
        	String sortDir = "DESC";
            String orderField = "";
            String orderColumnId = request.getParameter("order[0][column]");
            if(UtilValidate.isNotEmpty(orderColumnId)) {
                int sortColumnId = Integer.parseInt(orderColumnId);
                String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
                sortDir = request.getParameter("order[0][dir]").toUpperCase();
                orderField = sortColumnName;
                if("masterCampDesc".equals(orderField)){
                	orderField = "masterCampaignId";
                }
                if("parentCampDesc".equals(orderField)){
                	orderField = "parentCampaignId";
                }
                	
            }else {
                orderField = "marketingCampaignId";
            }
            List < EntityCondition > conditions = new ArrayList < EntityCondition > ();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            
          //Login Based Campaign List Filter
            String userLoginId = userLogin.getString("partyId");
            if(LoginFilterUtil.checkEmployeePosition(delegator, userLoginId)){
            	Debug.logInfo("userLoginId>>>"+userLoginId, MODULE);
	            List<String> rmCampaignIds = new ArrayList<String>();
	            List<String> marketingCampaignIdList = new ArrayList<String>();
	            Map<String, Object> dataSecurityMetaInfo = (Map<String, Object>) session.getAttribute("dataSecurityMetaInfo");
				if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {	
					List<String> lowerPositionPartyIds = (List<String>) dataSecurityMetaInfo.get("lowerPositionPartyIds");
					if (UtilValidate.isNotEmpty(lowerPositionPartyIds)) {
						rmCampaignIds = LoginFilterUtil.getCampaignList(delegator, lowerPositionPartyIds);
						EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
								EntityCondition.makeCondition("contactListId", EntityOperator.IN, rmCampaignIds)
								);
						List<GenericValue> resultList = delegator.findList("MarketingCampaignContactList", searchConditions, null, null, null, false);
						if (UtilValidate.isNotEmpty(resultList)) {
							marketingCampaignIdList = EntityUtil.getFieldListFromEntityList(resultList, "marketingCampaignId", true);
						}
						//if (UtilValidate.isNotEmpty(marketingCampaignIdList)) {
						EntityCondition marketingIdLoginCondition = EntityCondition.makeCondition("marketingCampaignId", EntityOperator.IN, marketingCampaignIdList);
			            conditions.add(marketingIdLoginCondition);
						//}
					}
				}
            }
			//End//
            
            if (UtilValidate.isNotEmpty(campaignId)) {
                EntityCondition campaignIdCondition = EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, campaignId);
                conditions.add(campaignIdCondition);
            }

            if (UtilValidate.isNotEmpty(campaignName)) {
                EntityCondition campaignNameCondition = EntityCondition.makeCondition("campaignName", EntityOperator.LIKE, "%"+campaignName+"%");
                conditions.add(campaignNameCondition);
            }
            if (UtilValidate.isNotEmpty(campaignType)) {
                EntityCondition campaignTypeCondition = EntityCondition.makeCondition("campaignTypeId", EntityOperator.EQUALS, campaignType);
                conditions.add(campaignTypeCondition);
            }
            if (UtilValidate.isNotEmpty(statusIdVal)) {
                EntityCondition statusIdCondition = EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, statusIdVal);
                conditions.add(statusIdCondition);
            }
            if (UtilValidate.isNotEmpty(fromDate)) {
            	
            	String fromdateVal = fromDate+" "+"00:00:00.0";
            	Date parsedDateFrom = dateFormat.parse(fromdateVal);
            	Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());
            	conditions.addAll( UtilMisc.toList( new EntityExpr( "startDate" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "startDate" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
            }
            if (UtilValidate.isNotEmpty(thruDate)) {
            	
            	String thruDateVal = thruDate+" "+"23:23:59.0";
            	Date parsedDateThru = dateFormat.parse(thruDateVal);
            	Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
            	conditions.addAll( UtilMisc.toList( new EntityExpr( "endDate" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "endDate" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
            	
            }
            
            if (UtilValidate.isNotEmpty(parentCampaignId)) {
                EntityCondition parentCampaignIdCondition = EntityCondition.makeCondition("parentCampaignId", EntityOperator.EQUALS, parentCampaignId);
                conditions.add(parentCampaignIdCondition);
            }
            if (UtilValidate.isNotEmpty(masterCampaignId)) {
                EntityCondition masterCampaignIdCondition = EntityCondition.makeCondition("masterCampaignId", EntityOperator.EQUALS, masterCampaignId);
                conditions.add(masterCampaignIdCondition);
            }
            
            //for created by 
            if (UtilValidate.isNotEmpty(createdBy)) {
                EntityCondition createdByUserLoginCondition = EntityCondition.makeCondition("createdByUserLogin", EntityOperator.EQUALS, createdBy);
                conditions.add(createdByUserLoginCondition);
            }
            
            // createdDate
            if (UtilValidate.isNotEmpty(createdDate)) {
                conditions.add(EntityCondition.makeCondition("createdStamp", EntityOperator.BETWEEN, UtilMisc.toList(Timestamp.valueOf(createdDate+" 00:00:00"),Timestamp.valueOf(createdDate+" 23:59:59"))));
            }
            String fieldToSelect = "marketingCampaignId";
            
            if (UtilValidate.isNotEmpty(campaignHierarchyType)) {
	            if("MASTER_CAMPAIGN".equals(campaignHierarchyType))	{
	            	List<String> lists = new LinkedList<String>();
	            	if(UtilValidate.isNotEmpty(masterParentCampaignId)) {
	            		//conditions.add(EntityCondition.makeCondition("masterCampaignId", EntityOperator.EQUALS, masterParentCampaignId));
	            		List<String> childCampList =  getAllChildCampaignIds(delegator,masterParentCampaignId,UtilMisc.toList(masterParentCampaignId),lists);
	            		if(UtilValidate.isNotEmpty(campaignId)) {
	            			conditions.add(EntityCondition.makeCondition("parentCampaignId", EntityOperator.EQUALS, masterParentCampaignId));
	            		} else {
	            			conditions.add(EntityCondition.makeCondition("marketingCampaignId", EntityOperator.IN, childCampList));
	            		}
	            		orderField = "startDate";
	            		sortDir ="ASC";
	            	} else {
	            		EntityCondition masterConditions = EntityCondition.makeCondition(EntityOperator.OR,
	            				EntityCondition.makeCondition("masterCampaignId",EntityOperator.EQUALS,null),
	            				EntityCondition.makeCondition("masterCampaignId",EntityOperator.EQUALS,"")
	            				); 
	            		conditions.add(masterConditions);
	            		EntityCondition parentConditions = EntityCondition.makeCondition(EntityOperator.OR,
	            				EntityCondition.makeCondition("parentCampaignId",EntityOperator.EQUALS,null),
	            				EntityCondition.makeCondition("parentCampaignId",EntityOperator.EQUALS,"")
	            				);
	            		conditions.add(parentConditions);
	            		//fieldToSelect ="masterCampaignId";
	            	}
	            } 
	            if("PARENT_CAMPAIGN".equals(campaignHierarchyType)) {
	            	
	            	List<String> lists = new LinkedList<String>();
	            	if(UtilValidate.isNotEmpty(masterParentCampaignId)) {
	            		//conditions.add(EntityCondition.makeCondition("parentCampaignId", EntityOperator.EQUALS, masterParentCampaignId));
	            		List<String> childCampList =  getAllChildCampaignIds(delegator,masterParentCampaignId,UtilMisc.toList(masterParentCampaignId),lists);
	            		if(UtilValidate.isNotEmpty(campaignId)) {
	            			conditions.add(EntityCondition.makeCondition("parentCampaignId", EntityOperator.EQUALS, masterParentCampaignId));
	            		} else {
	            			conditions.add(EntityCondition.makeCondition("marketingCampaignId", EntityOperator.IN, childCampList));
	            		}
	            		orderField = "startDate";
	            		sortDir ="ASC";
	            	}else {
	            		/*EntityCondition masterConditions = EntityCondition.makeCondition(EntityOperator.OR,
	            				EntityCondition.makeCondition("masterCampaignId",EntityOperator.EQUALS,null),
	            				EntityCondition.makeCondition("masterCampaignId",EntityOperator.EQUALS,"")
	            				); 
	            		conditions.add(masterConditions);
	            		*/
	            		EntityCondition parentConditions = EntityCondition.makeCondition(EntityOperator.OR,
	            				EntityCondition.makeCondition("parentCampaignId",EntityOperator.NOT_EQUAL,null),
	            				EntityCondition.makeCondition("parentCampaignId",EntityOperator.NOT_EQUAL,"")
	            				);
	            		conditions.add(parentConditions);
	            		
	            		fieldToSelect ="parentCampaignId";
	            	}
	            }
	            
            }
           
            
            EntityCondition mainConditons = EntityCondition.makeCondition(conditions, EntityOperator.AND);
            EntityFindOptions efo = new EntityFindOptions();
            efo.setDistinct(true);
            
            long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("MarketingCampaign", mainConditons, null, UtilMisc.toSet("marketingCampaignId"), efoNum);
            
            /*int count = 0;
            List < GenericValue > campaignListCount = delegator.findList("MarketingCampaign", mainConditons, UtilMisc.toSet(fieldToSelect), UtilMisc.toList("createdStamp DESC"), efo, false);
            if (campaignListCount != null && campaignListCount.size() > 0) {
                count = campaignListCount.size();
            }*/
            
			long recordsFiltered = count;
			long recordsTotal = count;
            
            int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
            int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
            efo.setOffset(startInx);
            efo.setLimit(endInx);
            List < GenericValue > campaignList = delegator.findList("MarketingCampaign", mainConditons, UtilMisc.toSet(fieldToSelect), UtilMisc.toList(orderField+ " " + sortDir), efo, false);
             // List < GenericValue > parties = delegator.findList("PartyFromByRelnAndContactInfoAndPartySupplemantalData", mainConditons, UtilMisc.toSet("partyId"), UtilMisc.toList("createdDate DESC"), efo, false);
            if (campaignList != null && campaignList.size() > 0) {
            	List <String> campaignIdList = new LinkedList<String>();
                if(UtilValidate.isNotEmpty(masterParentCampaignId) && campaignIdList.contains(masterParentCampaignId)) {
            		campaignIdList.add(masterParentCampaignId);
            	}		
                campaignIdList.addAll(EntityUtil.getFieldListFromEntityList(campaignList, fieldToSelect, true));
                if (campaignIdList != null && campaignIdList.size() > 0) {
                	/*if(UtilValidate.isNotEmpty(masterParentCampaignId)) {
                		campaignIdList.add(masterParentCampaignId);
                	}*/
                    for (String marketingCampaignId: campaignIdList) {
                        GenericValue marketingCampaignIdView = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId", marketingCampaignId), false);
                        if (marketingCampaignIdView != null && marketingCampaignIdView.size() > 0) {
                            Map < String, Object > campDetails = new HashMap < String, Object > ();
                            String campaignNameVal = marketingCampaignIdView.getString("campaignName");
                            String campaignTypeId = marketingCampaignIdView.getString("campaignTypeId");
                            String masterMktCampaignId = marketingCampaignIdView.getString("masterCampaignId");
                            String parentMktCampaignId = marketingCampaignIdView.getString("parentCampaignId");
                            String statusId = marketingCampaignIdView.getString("statusId");
                            String statusItemDesc = "";
                            String campItemDesc = "";
                            String startDate=marketingCampaignIdView.getString("startDate");
                            String endDate=marketingCampaignIdView.getString("endDate");
                            String createdByUser = marketingCampaignIdView.getString("createdByUserLogin");
                            if (UtilValidate.isNotEmpty(statusId)) {
                                GenericValue statusItem = delegator.findOne("StatusType", UtilMisc.toMap("statusTypeId", statusId), false);
                                if (statusItem != null && statusItem.size() > 0) {
                                    statusItemDesc = statusItem.getString("description");
                                }
                            }
                            if (UtilValidate.isNotEmpty(campaignTypeId)) {
                                GenericValue campaignTypeDesc = delegator.findOne("CampaignType", UtilMisc.toMap("campaignTypeId", campaignTypeId), false);
                                if (campaignTypeDesc != null && campaignTypeDesc.size() > 0) {
                                	campItemDesc = campaignTypeDesc.getString("description");
                                }
                            }
                            
                            String masterCampDesc = "";
                            if(UtilValidate.isNotEmpty(masterMktCampaignId)) {
                            	GenericValue masterCampaign = EntityQuery.use(delegator).from("MarketingCampaign").where("marketingCampaignId",masterMktCampaignId).queryOne();
                            	if(masterCampaign != null && masterCampaign.size() > 0) {
                            		masterCampDesc = masterCampaign.getString("campaignName");
                            	}
                            }
                            
                            String parentCampDesc = "";
                            if(UtilValidate.isNotEmpty(parentMktCampaignId)) {
                            	GenericValue parentCampaign = EntityQuery.use(delegator).from("MarketingCampaign").where("marketingCampaignId",parentMktCampaignId).queryOne();
                            	if(parentCampaign != null && parentCampaign.size() > 0) {
                            		parentCampDesc = parentCampaign.getString("campaignName");
                            	}
                            }
                            
                            String startDateVal="";
                            String endDateVal="";
                            if(UtilValidate.isNotEmpty(startDate)){
                            	startDateVal=startDate.substring(0, 10);
                            }
                            if(UtilValidate.isNotEmpty(endDate)){
                            	endDateVal=endDate.substring(0, 10);
                            }
                            
                            
                            GenericValue mktSummary = delegator.findOne("MarketingCampaignAnalysisSummary", UtilMisc.toMap("campaignId",marketingCampaignId,"domainName","DBS"), false);
                            int sent = 0;
                            int open = 0;
                            int clicked = 0;
                            int bounced = 0;
                            int unSub = 0;
                            int converted = 0;
                            if(mktSummary != null && mktSummary.size() > 0) {
                            	sent = mktSummary.getString("sentMailCount") != null ? Integer.parseInt(mktSummary.getString("sentMailCount")) : 0;
                            	open = mktSummary.getString("openedMailCount") != null ? Integer.parseInt(mktSummary.getString("openedMailCount")) : 0;
                            	clicked = mktSummary.getString("clickedMailCount") != null ? Integer.parseInt(mktSummary.getString("clickedMailCount")) : 0;
                            	bounced = mktSummary.getString("bouncedMailCount") != null ? Integer.parseInt(mktSummary.getString("bouncedMailCount")) : 0;
                            	unSub = mktSummary.getString("unsubscribeCount") != null ? Integer.parseInt(mktSummary.getString("unsubscribeCount")) : 0;
                            	converted = 0;
                            	
                            }
                            String campaignListId = marketingCampaignId+"_PROD_LIST";
                            EntityFindOptions efo1 = new EntityFindOptions();
                            efo1.getDistinct();
                            
                            long notOpen = delegator.findCountByCondition("CampaignContactListParty", EntityCondition.makeCondition(EntityOperator.AND,EntityCondition.makeCondition("contactListId",EntityOperator.EQUALS,campaignListId),EntityCondition.makeCondition("contactPurposeTypeId",EntityOperator.EQUALS,null),EntityCondition.makeCondition("notOpen",EntityOperator.EQUALS,"Y")), null, efo1);
                            
                            campDetails.put("marketingCampaignId", marketingCampaignId);
                            campDetails.put("campaignName", campaignNameVal);
                            campDetails.put("statusId", statusItemDesc);
                            campDetails.put("campaignTypeId", campItemDesc);
                            campDetails.put("masterCampaignId", masterMktCampaignId);
                            campDetails.put("masterCampDesc", masterCampDesc);
                            campDetails.put("parentCampaignId", parentMktCampaignId);
                            campDetails.put("parentCampDesc", parentCampDesc);
                            campDetails.put("startDate", startDateVal);
                            campDetails.put("endDate", endDateVal);
                            campDetails.put("createdByUserLogin", createdByUser);
                            campDetails.put("createdStamp", startDateVal);
                            campDetails.put("notOpen", notOpen);
                        	campDetails.put("sent", sent);
                        	campDetails.put("opened", open);
                        	campDetails.put("clicked", clicked);
                        	campDetails.put("bounced", bounced);
                        	campDetails.put("unSub", unSub);
                        	campDetails.put("converted", converted);
                            findcamp.add(campDetails);
                        }
                    }
                }
                returnMap.put("data", findcamp);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", recordsTotal);
                returnMap.put("recordsFiltered", recordsFiltered);
            } else {
                returnMap.put("data", findcamp);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", 0);
                returnMap.put("recordsFiltered", 0);
                return AjaxEvents.doJSONResponse(response, returnMap);
            }
        } catch (Exception e) {

            returnMap.put("data", findcamp);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
            Debug.logError("Exception in Get Account Details" + e.getMessage(), MODULE);
            return AjaxEvents.doJSONResponse(response, returnMap);
        
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    
    public static List<String> getAllChildCampaignIds(Delegator delegator,String masterParentCampaignId, List<String> campaignList,List<String> campaignIds) {
    	List<String> results = new LinkedList<String>();
    	try {
    		if(UtilValidate.isNotEmpty(masterParentCampaignId)) {
    			campaignIds.add(masterParentCampaignId);
    		}
    		if(campaignList != null && campaignList.size() > 0) {
    			
    			EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
    					EntityCondition.makeCondition("parentCampaignId",EntityOperator.IN,campaignList)
    					);
    			List< GenericValue > campList = delegator.findList("MarketingCampaign", condition, UtilMisc.toSet("marketingCampaignId"), null, null, false);
    			if(campList != null && campList.size() > 0) {
    				campaignList = EntityUtil.getFieldListFromEntityList(campList, "marketingCampaignId", true);
    				campaignIds.addAll(campaignList);
    				getAllChildCampaignIds(delegator, "", campaignList, campaignIds);
    			}
    		}

    	} catch (Exception e) {
    		Debug.logError("Error : "+e.getMessage(), MODULE);
    	}
    	return campaignIds;
    }
    
    public static String findContactListAjax(HttpServletRequest request, HttpServletResponse response) {
    	
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String listName = request.getParameter("listName");
        String emailType = request.getParameter("emailType");
        String contactTypeId = request.getParameter("contactTypeId");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > findList = FastList.newInstance();
        try {
            List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
            if (UtilValidate.isNotEmpty(listName)) {
            	EntityCondition	 listIdCond = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("contactListId",EntityOperator.EQUALS,listName),
			             EntityCondition.makeCondition("contactListName",EntityOperator.LIKE,"%"+listName+"%")
			             ), EntityOperator.OR);
						conditionList.add(listIdCond);
            }
            if(UtilValidate.isNotEmpty(emailType)) {
            	conditionList.add(EntityCondition.makeCondition("emailType",EntityOperator.EQUALS,emailType));
            }
            if(UtilValidate.isNotEmpty(contactTypeId)) {
            	conditionList.add(EntityCondition.makeCondition("contactPurposeTypeId",EntityOperator.EQUALS,contactTypeId));
            }
            EntityCondition	 isSmartCond=	EntityCondition.makeCondition("isSmartList",EntityOperator.EQUALS,null);
			conditionList.add(isSmartCond);
			EntityCondition	 isCampList=	EntityCondition.makeCondition("contactListTypeId",EntityOperator.NOT_EQUAL,"CAMPAIGN_LIST");
			conditionList.add(isCampList);
            EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
            EntityFindOptions efo = new EntityFindOptions();
            efo.setDistinct(true);
            int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
            int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
            efo.setOffset(startInx);
            efo.setLimit(endInx);
            
            long count = 0;
            
            EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("ContactList", mainConditons, null, UtilMisc.toSet("contactListId"), efoNum);
            
            /*List < GenericValue > ListCount = delegator.findList("ContactList", mainConditons, UtilMisc.toSet("contactListId"), UtilMisc.toList("createdStamp DESC"), null, false);
            if (ListCount != null && ListCount.size() > 0) {
                count = EntityUtil.getFieldListFromEntityList(ListCount, "contactListId", true).size();
            }*/
            
			long recordsFiltered = count;
			long recordsTotal = count;
             List < GenericValue > contactFindList = delegator.findList("ContactList", mainConditons, UtilMisc.toSet("contactListId"), UtilMisc.toList("createdStamp DESC"), efo, false);
            if (contactFindList != null && contactFindList.size() > 0) {
                List < String > contactIdList = EntityUtil.getFieldListFromEntityList(contactFindList, "contactListId", true);
                if (contactIdList != null && contactIdList.size() > 0) {
                    for (String listId: contactIdList) {
                        GenericValue contactListView = delegator.findOne("ContactList", UtilMisc.toMap("contactListId", listId), false);
                        if (contactListView != null && contactListView.size() > 0) {
                            Map < String, Object > contactDetails = new HashMap < String, Object > ();
                            String contactListNameVal = contactListView.getString("contactListName");
                            String contactListTypeId = contactListView.getString("contactPurposeTypeId");
                            String contactListType = "";
                            if ("TEST".equals(contactListTypeId)){
                            	contactListType ="Test";
                            }else if("LIVE".equals(contactListTypeId)){
                            	contactListType ="Production";
                            }
                           
                            contactDetails.put("contactListId", listId);
                            contactDetails.put("contactListName", contactListNameVal);
                            contactDetails.put("contactListType", contactListType);
                            findList.add(contactDetails);
                            
                        }
                    }
                }
                returnMap.put("data", findList);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", recordsTotal);
                returnMap.put("recordsFiltered", recordsFiltered);

            } else {
                returnMap.put("data", findList);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", 0);
                returnMap.put("recordsFiltered", 0);
                return AjaxEvents.doJSONResponse(response, returnMap);
            }
        } catch (Exception e) {
        	returnMap.put("data", findList);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
            Debug.logError("Exception in Get Contact List Details" + e.getMessage(), MODULE);
            return AjaxEvents.doJSONResponse(response, returnMap);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    public static String findCampaignListsAjax(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String campaignNameToFind = request.getParameter("campaignNameToFind");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > findCampaign = FastList.newInstance();
        try {
            List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
            if (UtilValidate.isNotEmpty(campaignNameToFind)) {
            	EntityCondition	 listIdCond = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("marketingCampaignId",EntityOperator.EQUALS,campaignNameToFind),
			             EntityCondition.makeCondition("campaignName",EntityOperator.LIKE,"%"+campaignNameToFind+"%")
			             ), EntityOperator.OR);
						conditionList.add(listIdCond);
						
            }
            EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
            EntityFindOptions efo = new EntityFindOptions();
            efo.setDistinct(true);
            int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
            int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
            efo.setOffset(startInx);
            efo.setLimit(endInx);
            
            long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("MarketingCampaign", mainConditons, null, UtilMisc.toSet("marketingCampaignId"), efoNum);
            
            /*int count = 0;
            List < GenericValue > campaignCount = delegator.findList("MarketingCampaign", mainConditons, UtilMisc.toSet("marketingCampaignId"), UtilMisc.toList("createdStamp DESC"), null, false);
            if (campaignCount != null && campaignCount.size() > 0) {
                count = EntityUtil.getFieldListFromEntityList(campaignCount, "marketingCampaignId", true).size();
            }*/
            
			long recordsFiltered = count;
			long recordsTotal = count;
			
            List < GenericValue > campaignFindList = delegator.findList("MarketingCampaign", mainConditons, UtilMisc.toSet("marketingCampaignId"), UtilMisc.toList("createdStamp DESC"), efo, false);
            if (campaignFindList != null && campaignFindList.size() > 0) {
                List < String > campaignIdList = EntityUtil.getFieldListFromEntityList(campaignFindList, "marketingCampaignId", true);
                if (campaignIdList != null && campaignIdList.size() > 0) {
                    for (String campaignId: campaignIdList) {
                        GenericValue campaignIdListView = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId", campaignId), false);
                        if (campaignIdListView != null && campaignIdListView.size() > 0) {
                            Map < String, Object > contactDetails = new HashMap < String, Object > ();
                            String campaignNameVal = campaignIdListView.getString("campaignName");
                            String startDate=campaignIdListView.getString("startDate");
                            String endDate=campaignIdListView.getString("endDate");
                            String startDateVal="";
                            String endDateVal="";
                            if(UtilValidate.isNotEmpty(startDate)){
                            	startDateVal=startDate.substring(0, 10);
                            }
                            if(UtilValidate.isNotEmpty(endDate)){
                            	endDateVal=endDate.substring(0, 10);
                            }
                            contactDetails.put("campaignId", campaignId);
                            contactDetails.put("campaignName", campaignNameVal);
                            contactDetails.put("startDate", startDateVal);
                            contactDetails.put("endDate", endDateVal);
                            findCampaign.add(contactDetails);
                            
                        }
                    }
                }
                returnMap.put("data", findCampaign);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", recordsTotal);
                returnMap.put("recordsFiltered", recordsFiltered);

            } else {
                returnMap.put("data", findCampaign);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", 0);
                returnMap.put("recordsFiltered", 0);
                return AjaxEvents.doJSONResponse(response, returnMap);
            }
        } catch (Exception e) {
        	returnMap.put("data", findCampaign);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
            Debug.logError("Exception in Get Campaigns List Details" + e.getMessage(), MODULE);
            return AjaxEvents.doJSONResponse(response, returnMap);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    
    public static String findtemplateListAjax(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String templateName = request.getParameter("templateName");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > findList = FastList.newInstance();
        try {
            List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
            if (UtilValidate.isNotEmpty(templateName)) {
            	EntityCondition	 listIdCond = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("smartListId",EntityOperator.EQUALS,templateName),
			             EntityCondition.makeCondition("name",EntityOperator.LIKE,templateName+"%")
			             ), EntityOperator.OR);
						conditionList.add(listIdCond);
						
            }           
			EntityCondition	 tempalteList=	EntityCondition.makeCondition("type",EntityOperator.EQUALS,"TEMPLATE_TYPE");
			conditionList.add(tempalteList);
            EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
            EntityFindOptions efo = new EntityFindOptions();
            efo.setDistinct(true);
            int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
            int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
            efo.setOffset(startInx);
            efo.setLimit(endInx);
            
            long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("SmartlistTemplateBuilder", mainConditons, null, UtilMisc.toSet("smartListId"), efoNum);
            
            /*int count = 0;
            List < GenericValue > ListCount = delegator.findList("SmartlistTemplateBuilder", mainConditons, UtilMisc.toSet("smartListId"), UtilMisc.toList("createdStamp DESC"), null, false);
            if (ListCount != null && ListCount.size() > 0) {
                count = EntityUtil.getFieldListFromEntityList(ListCount, "smartListId", true).size();
            }*/
            
			long recordsFiltered = count;
			long recordsTotal = count;
			
            List < GenericValue > templateFindList = delegator.findList("SmartlistTemplateBuilder", mainConditons, UtilMisc.toSet("smartListId"), UtilMisc.toList("createdStamp DESC"), efo, false);
            if (templateFindList != null && templateFindList.size() > 0) {
                List < String > templateIdList = EntityUtil.getFieldListFromEntityList(templateFindList, "smartListId", true);
                if (templateIdList != null && templateIdList.size() > 0) {
                    for (String listId: templateIdList) {
                        GenericValue templateListView = delegator.findOne("SmartlistTemplateBuilder", UtilMisc.toMap("smartListId", listId), false);
                        if (templateListView != null && templateListView.size() > 0) {
                            Map < String, Object > templateDetails = new HashMap < String, Object > ();
                            String templateListNameVal = templateListView.getString("name");
                           
                            templateDetails.put("smartListId", listId);
                            templateDetails.put("templateListName", templateListNameVal);
                            findList.add(templateDetails);
                            
                        }
                    }
                }
                returnMap.put("data", findList);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", recordsTotal);
                returnMap.put("recordsFiltered", recordsFiltered);

            } else {
                returnMap.put("data", findList);
                returnMap.put("draw", draw);
                returnMap.put("recordsTotal", 0);
                returnMap.put("recordsFiltered", 0);
                return AjaxEvents.doJSONResponse(response, returnMap);
            }
        } catch (Exception e) {
        	returnMap.put("data", findList);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
            Debug.logError("Exception in Get Template List Details" + e.getMessage(), MODULE);
            return AjaxEvents.doJSONResponse(response, returnMap);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    
    public static String getApproveListData(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String marketingCampaignId = request.getParameter("marketingCampaignId");
        String contactListIdAL = request.getParameter("contactListIdAL");
        String rmUserLoginIdAL = request.getParameter("rmUserLoginIdAL");
        String statusIdAL = request.getParameter("statusIdAL");
        String contactTypeId = request.getParameter("contactTypeId");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > approveListAL = FastList.newInstance();
        long recordsFiltered = 0;
        long recordsTotal = 0;
        try {
            String sortDir = "";
            String orderField = "";
            String orderColumnId = request.getParameter("order[0][column]");
            if(UtilValidate.isNotEmpty(orderColumnId)) {
                int sortColumnId = Integer.parseInt(orderColumnId);
                String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
                sortDir = request.getParameter("order[0][dir]").toUpperCase();
                orderField = sortColumnName;
            }else {
                orderField = "groupName";
            }
            List < EntityCondition > exprListForParametersAL = new ArrayList < EntityCondition > ();
            List < EntityCondition > orConditionList = new ArrayList < EntityCondition > ();
            if (UtilValidate.isNotEmpty(contactListIdAL)) {
                if (UtilValidate.isNotEmpty(rmUserLoginIdAL)) {
                    exprListForParametersAL.add(EntityCondition.makeCondition("csrPartyId", EntityOperator.EQUALS, rmUserLoginIdAL));
                }
                if (UtilValidate.isNotEmpty(statusIdAL) && "Y".equals(statusIdAL)) {
                   exprListForParametersAL.add(EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, "Y"));
                } else if (UtilValidate.isNotEmpty(statusIdAL) && "N".equals(statusIdAL)) {
                   orConditionList.add(EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, "N"));
                   orConditionList.add(EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, null));
                   orConditionList.add(EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, ""));
                   exprListForParametersAL.add(EntityCondition.makeCondition(orConditionList, EntityOperator.OR));
                } else if (UtilValidate.isNotEmpty(statusIdAL) && "X".equals(statusIdAL)) {
                   exprListForParametersAL.add(EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, "X"));
                }
                if (UtilValidate.isNotEmpty(rmUserLoginIdAL)) {
                    exprListForParametersAL.add(EntityCondition.makeCondition("csrPartyId", EntityOperator.EQUALS, rmUserLoginIdAL));
                }
                if (UtilValidate.isNotEmpty(contactTypeId)) {
                	//if("TEST".equals(contactTypeId)){
                    exprListForParametersAL.add(EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, contactTypeId));
                	//}else{
                    //exprListForParametersAL.add(EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS,null));
                	//}
                }
                
                //Login Based Campaign List Filter
                String userLoginId = userLogin.getString("partyId");
                if(LoginFilterUtil.checkEmployeePosition(delegator, userLoginId)){
                	Debug.logInfo("userLoginId>>>"+userLoginId, MODULE);
	                List<String> rmList = new ArrayList<String>();
	                Map<String, Object> dataSecurityMetaInfo = (Map<String, Object>) session.getAttribute("dataSecurityMetaInfo");
	    			if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {	
	    				List<String> lowerPositionPartyIds = (List<String>) dataSecurityMetaInfo.get("lowerPositionPartyIds");
	    				if (UtilValidate.isNotEmpty(lowerPositionPartyIds)) {	
	    					rmList = LoginFilterUtil.getRmList(delegator, lowerPositionPartyIds);
	    				}
	    			}
	    			if (UtilValidate.isNotEmpty(rmList)) {
	    			exprListForParametersAL.add(EntityCondition.makeCondition("csrPartyId", EntityOperator.IN, rmList));
	    			}
                }
    			//End//
                exprListForParametersAL.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListIdAL));
                exprListForParametersAL.add(EntityUtil.getFilterByDateExpr());
                
                long count = 0;
    			EntityFindOptions  efoNum= new EntityFindOptions();
    			efoNum.setDistinct(true);
    			efoNum.getDistinct();
    			efoNum.setFetchSize(1000);
    			
    			count = delegator.findCountByCondition("CampaignContactListPartyAndContact", EntityCondition.makeCondition(exprListForParametersAL, EntityOperator.AND), null, UtilMisc.toSet("partyId"), efoNum);
                
                /*List < GenericValue > campaignCLPAL1 = delegator.findList("CampaignContactListPartyAndContact", EntityCondition.makeCondition(exprListForParametersAL, EntityOperator.AND), null, UtilMisc.toList("-fromDate"), null, false);*/
                
                if (count > 0) {
                    
                    recordsFiltered = count;
                    recordsTotal = count;
                    EntityFindOptions efo = new EntityFindOptions();
                    efo.setDistinct(true);
                    int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                    int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                    efo.setOffset(startInx);
                    efo.setLimit(endInx);
                    List < GenericValue > campaignCLPAL = delegator.findList("CampaignContactListPartyAndContact", EntityCondition.makeCondition(exprListForParametersAL, EntityOperator.AND), null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
                        for (GenericValue contactListPartyGVAL: campaignCLPAL) {
                            String partyIdAL = contactListPartyGVAL.getString("partyId");
                            if (UtilValidate.isNotEmpty(partyIdAL)) {
                                Map < String, Object > approveMap = FastMap.newInstance();
                                String firstName = "";
                                String lastName = "";
                                String contactName = "";
                                String phoneNumber = "";
                                String infoString = "";
                                String status = "Not Reviewed";
                                String isApproved = contactListPartyGVAL.getString("isApproved");
                                String rmUserLogin = "";
                                String accountName = "";
                                String accountId = "";
                                String partyStatusIdAL = "";
                                String title = "";
                                String designation = "";
                                String csrPartyId = "";
                                String companyName = "";
                                String opened="N";
                                String clicked="N";
                                String converted="N";
                                String phoneContactMechId = "";
                                String emailContactMechId = "";
                                String contactPurpose= "";
                                String sentDate = "";
                                //contact details
                                firstName = contactListPartyGVAL.getString("firstName");
                                lastName = contactListPartyGVAL.getString("lastName");
                                contactName = firstName;
                                if(UtilValidate.isNotEmpty(lastName)) {
                                    contactName = contactName +" "+ lastName;
                                }
                                title = contactListPartyGVAL.getString("generalProfTitle");
                                companyName = contactListPartyGVAL.getString("companyName");
                                partyStatusIdAL = contactListPartyGVAL.getString("statusId");
                                
                                //account details
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("acctPartyId"))) {
                                    accountId = contactListPartyGVAL.getString("acctPartyId");
                                    accountName = contactListPartyGVAL.getString("groupName");
                                }
                                
                                //rm details
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("csrPartyId"))) {
                                    String csrFirstName = contactListPartyGVAL.getString("csrFirstName");
                                    String csrLastName = contactListPartyGVAL.getString("csrLastName");
                                    rmUserLogin = csrFirstName;
                                    if(UtilValidate.isNotEmpty(csrLastName)) {
                                        rmUserLogin = rmUserLogin +" "+ csrLastName;
                                    }
                                }
                                
                                // get open, clicked and converted details
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("opened"))) {
                                    opened = contactListPartyGVAL.getString("opened");
                                }
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("clicked"))) {
                                    clicked = contactListPartyGVAL.getString("clicked");
                                }
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("converted"))) {
                                    converted = contactListPartyGVAL.getString("converted");
                                }
                                
                                //get sent date
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getTimestamp("sentDate"))) {
                                    Timestamp sentDateTS = contactListPartyGVAL.getTimestamp("sentDate");
                                    if(UtilValidate.isNotEmpty(sentDateTS)) {
                                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                                        sentDate = df.format(sentDateTS);
                                    }
                                }
                                
                                // get phone and email details
                                if(UtilValidate.isNotEmpty(accountId)) {
                                    String partyRelAssocId = DataUtil.getPartyRelAssocId(delegator, UtilMisc.toMap("roleTypeIdFrom", "CONTACT",
                                         "roleTypeIdTo", "ACCOUNT", "partyRelationshipTypeId", "CONTACT_REL_INV", "partyIdFrom", partyIdAL,
                                         "partyIdTo", accountId));
                                    if(UtilValidate.isNotEmpty(partyRelAssocId)) {
                                        EntityCondition condition1 = EntityCondition.makeCondition(UtilMisc.toList(
                                                EntityCondition.makeCondition("solicitationStatus", EntityOperator.EQUALS, "Y"),
                                                EntityCondition.makeCondition("solicitationStatus", EntityOperator.EQUALS, null),
                                                EntityCondition.makeCondition("solicitationStatus", EntityOperator.EQUALS, "")), EntityOperator.OR);
                                        
                                        EntityCondition condition2 = EntityCondition.makeCondition("partyRelAssocId", EntityOperator.EQUALS, partyRelAssocId);
                                        EntityCondition condition3 = EntityCondition.makeCondition("assocTypeId", EntityOperator.EQUALS, "EMAIL");
                                        EntityCondition condition4 = EntityCondition.makeCondition("assocTypeId", EntityOperator.EQUALS, "PHONE");

                                        EntityCondition emailCondition = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, condition3));
                                        EntityCondition phoneCondition = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, condition4));
                                        List < GenericValue > email = delegator.findList("PartyRelationshipAssoc", emailCondition, null, null, null, true);
                                        if(email != null && email.size() > 0) {
                                            /*GenericValue pcmpEmail = EntityQuery.use(delegator).from("PartyContactMechPurpose")
                                                .where(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(email, "assocId", true)),
                                                       EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyIdAL),
                                                       EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL"))
                                                .filterByDate().orderBy("createdStamp DESC").queryFirst();*/
                                            
                                            GenericValue  pcmpEmail = EntityQuery.use(delegator).from("PartyContactWithPurpose")
                                                    .where(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(email, "assocId", true)),
                                                        EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "EMAIL_ADDRESS"),
                                                        EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL"),
                                                        EntityCondition.makeCondition("partyId",EntityOperator.EQUALS, partyIdAL))
                                                    .filterByDate("contactFromDate", "contactThruDate", "purposeFromDate", "purposeThruDate")
                                                    .orderBy("contactFromDate DESC").queryFirst();
                                            if (pcmpEmail != null && pcmpEmail.size() > 0) {
                                                GenericValue emailAddressValue = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", pcmpEmail.getString("contactMechId")), false);
                                                if (emailAddressValue != null && emailAddressValue.size() > 0  && UtilValidate.isNotEmpty(emailAddressValue.getString("infoString"))) {
                                                    emailContactMechId = emailAddressValue.getString("contactMechId");
                                                    infoString = emailAddressValue.getString("infoString");
                                                }
                                            }
                                        }
                                        
                                        List < GenericValue > phone = delegator.findList("PartyRelationshipAssoc", phoneCondition, null, null, null, true);
                                        if(phone != null && phone.size() > 0) {
                                            /*GenericValue pcmpPhone = EntityQuery.use(delegator).from("PartyContactMechPurpose")
                                                .where(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(phone, "assocId", true)),
                                                       EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyIdAL),
                                                       EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE"))
                                                .filterByDate().orderBy("createdStamp DESC").queryFirst();*/
                                            
                                            GenericValue  pcmpPhone = EntityQuery.use(delegator).from("PartyContactWithPurpose")
                                                    .where(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, EntityUtil.getFieldListFromEntityList(phone, "assocId", true)),
                                                        EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "TELECOM_NUMBER"),
                                                        EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE"),
                                                        EntityCondition.makeCondition("partyId",EntityOperator.EQUALS, partyIdAL))
                                                    .filterByDate("contactFromDate", "contactThruDate", "purposeFromDate", "purposeThruDate")
                                                    .orderBy("contactFromDate DESC").queryFirst();
                                            
                                            if (pcmpPhone != null && pcmpPhone.size() > 0) {
                                                GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", pcmpPhone.getString("contactMechId")), false);
                                                if (primaryPhoneNumber != null && primaryPhoneNumber.size() > 0  && UtilValidate.isNotEmpty(primaryPhoneNumber.getString("contactNumber"))) {
                                                    phoneContactMechId = primaryPhoneNumber.getString("contactMechId");
                                                    phoneNumber = primaryPhoneNumber.getString("contactNumber");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                Map < String, String > partyContactInfo = PartyPrimaryContactMechWorker.getPartyPrimaryContactMechValueMaps(delegator, partyIdAL);
                                    if(partyContactInfo != null && partyContactInfo.size() > 0) {
                                        if (UtilValidate.isNotEmpty(partyContactInfo.get("PrimaryPhone"))) {
                                            phoneNumber = partyContactInfo.get("PrimaryPhone");
                                            phoneNumber = phoneNumber.substring(phoneNumber.lastIndexOf('-') + 1);
                                            phoneContactMechId = partyContactInfo.get("primaryPhoneContactMechId");
                                        }
                                        if(UtilValidate.isNotEmpty(partyContactInfo.get("EmailAddress"))) {
                                            infoString = partyContactInfo.get("EmailAddress");
                                            emailContactMechId = partyContactInfo.get("emailAddressContactMechId");
                                        }
                                    }
                                }
                                //RM Approved status
                                if (UtilValidate.isNotEmpty(isApproved) && "Y".equals(isApproved)) {
                                    status = "Approved";
                                } else if (UtilValidate.isNotEmpty(isApproved) && "X".equals(isApproved)) {
                                    status = "Not Approved";
                                }
                                
                                //Contact Role
                                List<GenericValue> partyRoleGVAL = EntityQuery.use(delegator).from("PartyRole")
                                    .where(EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, UtilMisc.toList("LEAD","CONTACT")),
                                           EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyIdAL)).queryList();
                                if(partyRoleGVAL != null && partyRoleGVAL.size() > 1) {
                                    if("LEAD_CONVERTED".equals(partyStatusIdAL)) {
                                        partyStatusIdAL = "CONTACT";
                                    } else {
                                        partyStatusIdAL = "LEAD";
                                    }
                                } else if(partyRoleGVAL != null && partyRoleGVAL.size() == 1) {
                                    partyStatusIdAL = partyRoleGVAL.get(0).getString("roleTypeId");
                                }
                                
                                //get designation details
                                List<GenericValue> hdpContactDesignationAssoc = EntityQuery.use(delegator).from("HdpContactDesignationAssoc")
                                        .where("contactId", partyIdAL).queryList();
                                int i = 1;
                                if(hdpContactDesignationAssoc != null && hdpContactDesignationAssoc.size() > 0) {
                                    for(GenericValue hdpContactDesignationAssocGV : hdpContactDesignationAssoc) {
                                        String designationName = hdpContactDesignationAssocGV.getString("designationName");
                                        if(UtilValidate.isNotEmpty(designationName)) {
                                            designation = designation + designationName;
                                            if(i < hdpContactDesignationAssoc.size())
                                            designation = designation + ", ";
                                        }
                                        i++;
                                    }
                                }
                                
                              //get contact Type
                                if(UtilValidate.isNotEmpty(contactListPartyGVAL.getString("contactPurposeTypeId"))) {
                                	contactPurpose = contactListPartyGVAL.getString("contactPurposeTypeId");
                                }
                                approveMap.put("contactListId", contactListPartyGVAL.getString("contactListId"));
                                approveMap.put("partyId", partyIdAL);
                                approveMap.put("partyStatusIdAL", partyStatusIdAL);
                                approveMap.put("firstName", firstName);
                                approveMap.put("lastName", lastName);
                                approveMap.put("companyName", companyName);
                                approveMap.put("contactName", contactName);
                                approveMap.put("designation", designation);
                                approveMap.put("title", title);
                                approveMap.put("accountId", accountId);
                                approveMap.put("accountName", accountName);
                                approveMap.put("phoneContactMechId", phoneContactMechId);
                                approveMap.put("phoneNumber", phoneNumber);
                                approveMap.put("emailContactMechId", emailContactMechId);
                                approveMap.put("infoString", infoString);
                                approveMap.put("rmUserLogin", rmUserLogin);
                                approveMap.put("status", status);
                                approveMap.put("isApproved", isApproved);
                                approveMap.put("csrPartyId", csrPartyId);
                                approveMap.put("opened", opened);
                                approveMap.put("clicked", clicked);
                                approveMap.put("converted", converted);
                                approveMap.put("contactPurpose", contactPurpose);
                                approveMap.put("sentDate", sentDate);
                                approveListAL.add(approveMap);
                            }
                        }
                }
            }
            returnMap.put("data", approveListAL);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", recordsTotal);
            returnMap.put("recordsFiltered", recordsFiltered);
        } catch (Exception e) {
            Debug.logError("Exception in Get Account Related Contact" + e.getMessage(), MODULE);
            returnMap.put("data", approveListAL);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    public static String getSubProductsDataJSON(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        String productId = request.getParameter("productId");
        List < GenericValue > subProducts = null;
        try {
            if (UtilValidate.isNotEmpty(productId)) {
                subProducts = EntityQuery.use(delegator).select("enumId", "description")
                    .from("Enumeration").where("parentEnumId", productId, "enumTypeId", "SUB_PRODUCT")
                    .orderBy("sequenceId").queryList();
            }
        } catch (Exception e) {
            Debug.log("Exception in get Sub Products"+e.getMessage());
            return doJSONResponse(response, FastList.newInstance());
        }
        return doJSONResponse(response, subProducts);
    }
    
    public static String getSegmentData(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		List<GenericValue> segmentList = null;
		
		String draw = request.getParameter("draw");
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		String segmentName = request.getParameter("segmentName");
		
		JSONObject obj = new JSONObject();
		JSONArray dataMap = new JSONArray();
		long recordsFiltered = 0;
		long recordsTotal = 0;
		
		try {
			List<EntityCondition> conditionList = FastList.newInstance();
			conditionList.add(EntityCondition.makeCondition("groupType", EntityOperator.EQUALS, "SEGMENTATION"));
			if (UtilValidate.isNotEmpty(segmentName)) {
            	EntityCondition	 listIdCond = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("groupId",EntityOperator.LIKE,"%"+segmentName+"%"),
			             EntityCondition.makeCondition("groupName",EntityOperator.LIKE,"%"+segmentName+"%")
			             ), EntityOperator.OR);
						conditionList.add(listIdCond);
						
            } 
			
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			
			long count = 0;
			EntityFindOptions  efoNum= new EntityFindOptions();
			efoNum.setDistinct(true);
			efoNum.getDistinct();
			efoNum.setFetchSize(1000);
			
			count = delegator.findCountByCondition("CustomField", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, UtilMisc.toSet("customFieldId"), efoNum);
			
			/*segmentList = delegator.findList("CustomField", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, null, efo, false);
			if(segmentList != null && segmentList.size() > 0) {
				totalCount = segmentList.size();
			}*/
			
			recordsFiltered = count;
			recordsTotal = count;
			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
			efo.setOffset(startInx);
			efo.setLimit(endInx);
			
			String orderField = "";
			String sortDir= "ASC";

			segmentList = delegator.findList("CustomField", EntityCondition.makeCondition(conditionList,EntityOperator.AND), null, UtilMisc.toList("createdStamp DESC"), efo, false);
			if(UtilValidate.isNotEmpty(segmentList)) {
				segmentList.forEach(e->{
					JSONObject listObj = new JSONObject();
					listObj.put("groupId", e.getString("groupId"));
					listObj.put("groupName", UtilValidate.isNotEmpty(e.getString("groupName")) ? e.getString("groupName") : "");
					listObj.put("customFieldId", UtilValidate.isNotEmpty(e.getString("customFieldId"))? e.getString("customFieldId") :"");
					listObj.put("customFieldName", UtilValidate.isNotEmpty(e.getString("customFieldName")) ? e.getString("customFieldName") : "");
					dataMap.add(listObj);
				});
			}
			obj.put("data", dataMap);
			obj.put("draw", draw);
			obj.put("recordsTotal", recordsTotal);
			obj.put("recordsFiltered", recordsFiltered);
		} catch(Exception e) {
			Debug.logInfo("Error-"+e.getMessage(), MODULE);
		}
		return doJSONResponse(response, obj);
	}

    public static String getMasterParentCampaignDetails( HttpServletRequest request, HttpServletResponse response ) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
    	List<Object> list = new LinkedList<Object>();
    	String marketingCampaignId = request.getParameter("marketingCampaignId");
    	try {
    		if(UtilValidate.isNotEmpty(marketingCampaignId)) {
    			
    			/*String test = "['Research', 'Find sources', null,new Date(2015, 0, 1), new Date(2015, 0, 5), null,  100,  null],"
    					+ " ['Write', 'Write paper', 'write',null, new Date(2015, 0, 9), daysToMilliseconds(3), 25, 'Research,Outline'],"
    					+ " ['Cite', 'Create bibliography', 'write',null, new Date(2015, 0, 7), daysToMilliseconds(1), 20, 'Research'],"
    					+ " ['Complete', 'Hand in paper', 'complete',null, new Date(2015, 0, 10), daysToMilliseconds(1), 0, 'Cite,Write'],"
    					+ " ['Outline', 'Outline paper', 'write',null, new Date(2015, 0, 6), daysToMilliseconds(1), 100, 'Research']";
    			list.add(test);*/
    			
    			list = getRecursiveMarketingCampaign(delegator, marketingCampaignId, UtilMisc.toList(marketingCampaignId), list);
    			
    			if(list != null && list.size() == 1) {
    				list = new LinkedList<Object>();
    			}
    			
    		}
    	} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), MODULE);
		}
    	return doJSONResponse(response, list);
    }
    public static List<Object> getRecursiveMarketingCampaign(Delegator delegator, String masterCampaignId, List<String> marketingCampaignIds,List<Object> finalList) {
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	List<String> historicalCampId = new LinkedList<String>();
		try {
			if(UtilValidate.isNotEmpty(masterCampaignId)) {
				List<Object> masterCampaignData = new LinkedList<Object>();
				GenericValue masterCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",masterCampaignId), false);
				if(masterCampaign != null && masterCampaign.size() > 0) {
					masterCampaignData.add(masterCampaign.getString("marketingCampaignId"));
					masterCampaignData.add(masterCampaign.getString("campaignName"));
					masterCampaignData.add(null);
					String startDateStr = masterCampaign.getString("startDate");
					String endDateStr = masterCampaign.getString("endDate");
					Date startDate = df.parse(startDateStr);
					Date endDate = df.parse(endDateStr);
					Calendar c = Calendar.getInstance(); 
					c.setTime(endDate); 
					c.add(Calendar.DATE, 1);
					endDate = c.getTime();
					long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
					
					masterCampaignData.add(null);
					masterCampaignData.add(null);
					masterCampaignData.add(86400000L);
					masterCampaignData.add(0);
					masterCampaignData.add(null);
					finalList.add(masterCampaignData);
				}
			}
			if(marketingCampaignIds != null && marketingCampaignIds.size() > 0) {
				EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("parentCampaignId",EntityOperator.IN,marketingCampaignIds)
						); 
				//List<GenericValue> dripCampaignList = delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("parentCampaignId",marketingCampaignId), null, false);
				List<GenericValue> dripCampaignList = delegator.findList("MarketingCampaign", condition, UtilMisc.toSet("marketingCampaignId"), null, null, false);
				if(dripCampaignList != null && dripCampaignList.size() > 0) {
					List<String> mktCampList = EntityUtil.getFieldListFromEntityList(dripCampaignList, "marketingCampaignId", true);
					for(String mktCampaignId : mktCampList) {
						List<Object> childCampaignData = new LinkedList<Object>();
						GenericValue mktCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",mktCampaignId), false);
						if(mktCampaign != null && mktCampaign.size() >0 ) {
							childCampaignData.add(mktCampaign.getString("marketingCampaignId"));
							childCampaignData.add(mktCampaign.getString("campaignName"));
							childCampaignData.add(null);
							String startDateStr = mktCampaign.getString("startDate");
							String endDateStr = mktCampaign.getString("endDate");
							Date startDate = df.parse(startDateStr);
							Date endDate = df.parse(endDateStr);
							Calendar c = Calendar.getInstance(); 
							c.setTime(endDate); 
							c.add(Calendar.DATE, 1);
							endDate = c.getTime();
							long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
							childCampaignData.add(null);
							childCampaignData.add(null);
							childCampaignData.add(86400000L);
							childCampaignData.add(0);
							childCampaignData.add(mktCampaign.getString("parentCampaignId"));
							finalList.add(childCampaignData);
							historicalCampId.add(mktCampaignId);
						}
					}
					getRecursiveMarketingCampaign(delegator, "",historicalCampId,finalList);
				}
			}
		} catch (GenericEntityException | ParseException e) {
			e.printStackTrace();
		}
		return finalList;
    }
    
    public static String getIndividualCampaignDetails( HttpServletRequest request, HttpServletResponse response ) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
    	List<Object> list = new LinkedList<Object>();
    	String marketingCampaignId = request.getParameter("mktCampId");
    	try {
    		if(UtilValidate.isNotEmpty(marketingCampaignId)) {
    			GenericValue marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
    			if(marketingCampaign != null && marketingCampaign.size() > 0) {
    				Map<String, String> data = new HashMap<String,String>();
    				data.put("campaignId", marketingCampaign.getString("marketingCampaignId"));
    				data.put("campaignName", marketingCampaign.getString("campaignName"));
    				data.put("startDate", marketingCampaign.getString("startDate"));
    				data.put("endDate", marketingCampaign.getString("endDate"));
    				data.put("status", marketingCampaign.getString("statusId"));
    				data.put("templateId", marketingCampaign.getString("campaignTemplateId"));
					list.add(data);
    			}
    		}
    	} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), MODULE);
		}
    	return doJSONResponse(response, list);
    }
    
    
    // gantt chart implementation for campaign hierarchy

    public static String getMasterParentCampaignHierarchy( HttpServletRequest request, HttpServletResponse response ) throws GenericEntityException {
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	DecimalFormat df2 = new DecimalFormat( "#,###,###,###" );
    	List<Object> list = new LinkedList<Object>();
    	String marketingCampaignId = request.getParameter("marketingCampaignId");
    	Map<String, Object> result = new LinkedHashMap<String,Object>();
    	try {
    		Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
	        cal.set(Calendar.HOUR_OF_DAY, 0);  
	        cal.set(Calendar.MINUTE, 0);  
	        cal.set(Calendar.SECOND, 0);  
	        cal.set(Calendar.MILLISECOND, 0);
	        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date now = cal.getTime();
    		Map<String, Object> jsonObj = new LinkedHashMap<String,Object>();
    		List<Map<String, Object>> jsonArray = new LinkedList<Map<String, Object>>();	
    		if(UtilValidate.isNotEmpty(marketingCampaignId)) {
    			GenericValue masterCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",marketingCampaignId), false);
				if(masterCampaign != null && masterCampaign.size() > 0) {
					String campaignName = masterCampaign.getString("campaignName");
					jsonObj.put("name", campaignName);
					
					String startDateStr = masterCampaign.getString("startDate");
					Date startDate = df.parse(startDateStr);
					long min = getDateTimeInMilliSecond(now, startDate,0,-15);
					result.put("min", min);
										
					String endDateStr = masterCampaign.getString("endDate");
					Date endDate = df.parse(endDateStr);
					long max = getDateTimeInMilliSecond(now, endDate,15,0);
					result.put("max", max);
				}
				
    			list = getChildCampaignHierarchy(delegator, marketingCampaignId, UtilMisc.toList(marketingCampaignId), list);
    			
    			if(list != null && list.size() == 1) {
    				list = new LinkedList<Object>();
    			}
    			jsonObj.put("data", list);
    			jsonArray.add(jsonObj);
    		}
    		if(list == null || list.size() == 0) {
    			result.put("chartData", new LinkedList<Map<String, Object>>());
    		} else {
    			result.put("chartData", jsonArray);
    		}
    		
    	} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), MODULE);
		}
    	return doJSONResponse(response, result);
    }
    public static List<Object> getChildCampaignHierarchy(Delegator delegator, String masterCampaignId, List<String> marketingCampaignIds,List<Object> finalList) {
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	DecimalFormat df2 = new DecimalFormat( "#,###,###,###" );
    	List<String> historicalCampId = new LinkedList<String>();
		try {
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
	        cal.set(Calendar.HOUR_OF_DAY, 0);  
	        cal.set(Calendar.MINUTE, 0);  
	        cal.set(Calendar.SECOND, 0);  
	        cal.set(Calendar.MILLISECOND, 0);
	        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date now = cal.getTime();
			
			if(UtilValidate.isNotEmpty(masterCampaignId)) {
				GenericValue masterCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",masterCampaignId), false);
				if(masterCampaign != null && masterCampaign.size() > 0) {
					Map<String, Object> data = new LinkedHashMap<String,Object>();
					String campaignId = masterCampaign.getString("marketingCampaignId");
					String campaignName = masterCampaign.getString("campaignName");
					String statusId = masterCampaign.getString("statusId");
					
					List<GenericValue> countList = delegator.findByAnd("MarketingCampaignAnalysisSummary", UtilMisc.toMap("campaignId",campaignId), UtilMisc.toList("createdStamp DESC"), false);
					long totalSent = 0L;
					String totalCount = "0";
					if(countList != null && countList.size() > 0) {
						totalCount =  countList.get(0).getString("sentMailCount") != null ? countList.get(0).getString("sentMailCount") : totalCount;
						try {
							totalSent = Long.parseLong(totalCount);
						} catch(Exception e){
							totalSent = Long.parseLong("0");
						}
						totalCount = df2.format(totalSent);
					}
					data.put("name", campaignName+" #"+totalCount);
					data.put("id", campaignId);
					if("MKTG_CAMP_PUBLISHED".equals(statusId)) {
						data.put("color", "green");
					} else if("MKTG_CAMP_CREATED".equals(statusId)) {
						data.put("color", "CornflowerBlue");
					}
					
		            String startDateStr = masterCampaign.getString("startDate");
					Date startDate = df.parse(startDateStr);
					long start = getDateTimeInMilliSecond(now, startDate,0,0);
					data.put("start", start);
					
					String endDateStr = masterCampaign.getString("endDate");
					Date endDate = df.parse(endDateStr);
					long end = getDateTimeInMilliSecond(now, endDate,0,0);
					data.put("end", end);
					
					finalList.add(data);
				}
			}
			if(marketingCampaignIds != null && marketingCampaignIds.size() > 0) {
				EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("parentCampaignId",EntityOperator.IN,marketingCampaignIds)
						); 
				//List<GenericValue> dripCampaignList = delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("parentCampaignId",marketingCampaignId), null, false);
				List<GenericValue> dripCampaignList = delegator.findList("MarketingCampaign", condition, UtilMisc.toSet("marketingCampaignId"), null, null, false);
				if(dripCampaignList != null && dripCampaignList.size() > 0) {
					List<String> mktCampList = EntityUtil.getFieldListFromEntityList(dripCampaignList, "marketingCampaignId", true);
					for(String mktCampaignId : mktCampList) {
						Map<String, Object> data = new LinkedHashMap<String,Object>();
						GenericValue mktCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",mktCampaignId), false);
						if(mktCampaign != null && mktCampaign.size() >0 ) {
							String campaignId = mktCampaign.getString("marketingCampaignId");
							String campaignName = mktCampaign.getString("campaignName");
							String statusId = mktCampaign.getString("statusId");
							
							List<GenericValue> countList = delegator.findByAnd("MarketingCampaignAnalysisSummary", UtilMisc.toMap("campaignId",campaignId), UtilMisc.toList("createdStamp DESC"), false);
							long totalSent = 0L;
							String totalCount = "0";
							if(countList != null && countList.size() > 0) {
								totalCount =  countList.get(0).getString("sentMailCount") != null ? countList.get(0).getString("sentMailCount") : totalCount;
								try {
									totalSent = Long.parseLong(totalCount);
								} catch(Exception e){
									totalSent = Long.parseLong("0");
								}
								totalCount = df2.format(totalSent);
							}
							data.put("name", campaignName+" #"+totalCount);
							data.put("id", campaignId);
							if("MKTG_CAMP_PUBLISHED".equals(statusId)) {
								data.put("color", "green");
							} else if("MKTG_CAMP_CREATED".equals(statusId)) {
								data.put("color", "CornflowerBlue");
							}
							
				            String startDateStr = mktCampaign.getString("startDate");
							Date startDate = df.parse(startDateStr);
							long start = getDateTimeInMilliSecond(now, startDate,0,0);
							data.put("start", start);
							
							String endDateStr = mktCampaign.getString("endDate");
							Date endDate = df.parse(endDateStr);
							long end = getDateTimeInMilliSecond(now, endDate,0,0);
							data.put("end", end);
							data.put("dependency",mktCampaign.getString("parentCampaignId"));
							data.put("parent",mktCampaign.getString("parentCampaignId"));
							finalList.add(data);
							historicalCampId.add(mktCampaignId);
						}
					}
					getChildCampaignHierarchy(delegator, "",historicalCampId,finalList);
				}
			}
		} catch (GenericEntityException | ParseException e) {
			e.printStackTrace();
		}
		return finalList;
    }
    /**
     * @author Mahendran T
     * @param now
     * @param date
     * @param add
     * @param subtract
     *  To get the milli seconds
     * */
    public static long getDateTimeInMilliSecond(Date now,Date date,int add, int subtract) {
    	long milli = 0L;
    	try {
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(date);
    		if(add != 0)
    			cal.add(Calendar.DATE, add);
    		if(subtract !=0)
    			cal.add(Calendar.DATE, subtract);
            cal.set(Calendar.HOUR_OF_DAY, 0);  
            cal.set(Calendar.MINUTE, 0);  
            cal.set(Calendar.SECOND, 0);  
            cal.set(Calendar.MILLISECOND, 0);
            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = cal.getTime();
    		long diff = Math.abs(now.getTime() - date.getTime());
    	    long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    	    long currentDate = now.getTime();
    		int oneDay = 1000 * 60 * 60 * 24;
    		
    		if(date.before(now)) {
    			milli = currentDate-days*oneDay;	
    		} else if(date.after(now)) {
    			milli = currentDate+days*oneDay;
    		} else {
    			milli = currentDate;
    		}
    	}catch (Exception e) {
    		Debug.logError("Error : "+e.getMessage(), MODULE);
		}
    	
    	return milli;
    }
    
    
    public static String approveImportErrorLogs(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
    	String processId = (String) request.getParameter("processId");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
    	String draw = request.getParameter("draw");
    	String start = request.getParameter("start");
    	String length = request.getParameter("length");
    	Long totalCount = Long.valueOf("0");
    	Map < String, Object > returnMap = FastMap.newInstance();
    	List < Object > approveImportErrorLogs = FastList.newInstance();
    	try {
    		if (UtilValidate.isNotEmpty(processId)) {
    			conditionList.add(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("processId", EntityOperator.EQUALS, processId)
    					), EntityOperator.AND));
    			EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
    			totalCount = EntityQuery.use(delegator).from("ApproveListImportLog").where(mainConditons).queryCount();
    			if (totalCount != null && totalCount > 0) {
    				EntityFindOptions efo = new EntityFindOptions();
    				efo.setDistinct(true);
    				int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
    				int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
    				efo.setOffset(startInx);
    				efo.setLimit(endInx);
    				List < GenericValue > approveImportList = delegator.findList("ApproveListImportLog", mainConditons, null, null, efo, false);
    				if (approveImportList != null && approveImportList.size() > 0) {
    					for (GenericValue approveImportListGV: approveImportList) {
    						Map < String, Object > approveImportMap = FastMap.newInstance();
    						approveImportMap.put("processId", approveImportListGV.getString("processId"));
    						approveImportMap.put("logId", approveImportListGV.getString("logId"));
    						approveImportMap.put("campaignId", approveImportListGV.getString("campaignId"));
    						approveImportMap.put("errorDescription", approveImportListGV.getString("errorDescription"));
    						approveImportMap.put("acctId", approveImportListGV.getString("acctId"));
    						approveImportMap.put("partyId", approveImportListGV.getString("partyId"));
    						approveImportErrorLogs.add(approveImportMap);
    					}
    				}
    			}
    		}
    		returnMap.put("data", approveImportErrorLogs);
    		returnMap.put("draw", draw);
    		returnMap.put("recordsTotal", totalCount);
    		returnMap.put("recordsFiltered", totalCount);
    	} catch (Exception e) {
    		returnMap.put("data", approveImportErrorLogs);
    		returnMap.put("draw", draw);
    		returnMap.put("recordsTotal", 0);
    		returnMap.put("recordsFiltered", 0);
    		Debug.logError("Exception in Approve List Import Error Logs" + e.getMessage(), MODULE);
    		return AjaxEvents.doJSONResponse(response, returnMap);
    	}
    	return AjaxEvents.doJSONResponse(response, returnMap);
    }

    @SuppressWarnings("unused")
    public static String downloadApproveListErrorLogs(HttpServletRequest request, HttpServletResponse response) {
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
    	HttpSession session = request.getSession();
    	Locale locale = UtilHttp.getLocale(request);
    	Map < String, Object > resp = new HashMap < String, Object > ();
    	String processIds = request.getParameter("processId");

    	if (UtilValidate.isNotEmpty(processIds)) {
    		try (DataOutputStream outputStream = new DataOutputStream(response.getOutputStream())){
    			response.setContentType("text/csv");
    			response.setHeader("Content-Disposition", "attachment; filename=\"Approve List Error Logs.csv\"");

    			List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
    			conditionList.add(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("processId", EntityOperator.EQUALS, processIds)
    					), EntityOperator.AND));
    			EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
    			List < GenericValue > approveListImportTempList = delegator.findList("ApproveListImportLog", mainConditons, null, null, null, false);

    			String outputResult = "ProcessId, Log Id, Campaign Id,Error Description,Account Id,Party Id\n";
    			outputStream.write(outputResult.getBytes());
    			if (approveListImportTempList != null && approveListImportTempList.size() > 0) {
    				for (GenericValue approveListImportTempListTemp: approveListImportTempList) {
    					System.out.println(approveListImportTempListTemp+"approveListImportTempListTemp");
    					String processId = "";
    					String logId = "";
    					String campaignId = "";
    					String errorCode = "";
    					String accountId = "";
    					String partyId = "";
    					processId = approveListImportTempListTemp.getString("processId");
    					logId = approveListImportTempListTemp.getString("logId");
    					campaignId = approveListImportTempListTemp.getString("campaignId");
    					errorCode= approveListImportTempListTemp.getString("errorDescription");
    					accountId = approveListImportTempListTemp.getString("acctId");
    					partyId = approveListImportTempListTemp.getString("partyId");
    					outputResult = processId + "," + logId + "," + campaignId + "," + errorCode + "," + accountId +"," + partyId + "\n";
    					outputStream.write(outputResult.getBytes());

    				}
    			} else {
    				outputResult = "No data available in table\n";
    				outputStream.write(outputResult.getBytes());
    			}
    			resp.put("fields", "Success");
    			outputStream.flush();
    		} catch (Exception e) {
    			Debug.logError(e.getMessage(), MODULE);
    			resp.put("fields", "Failed");
    		} 
    	}
    	return doJSONResponse(response, resp);
    }
    
    public static String rmReassignFormAjax(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        String partyId = (String) request.getParameter("partyId");
        String contactListId = (String) request.getParameter("contactListId");
        String rmPartyId = (String) request.getParameter("rmPartyId");
        String roleTypeIdFrom = (String) request.getParameter("roleTypeIdFrom");
        String accountPartyId = (String) request.getParameter("accountPartyId");
        String partyIdFrom = "";
        java.sql.Timestamp nowTimestamp = UtilDateTime.nowTimestamp();
        Locale locale = UtilHttp.getLocale(request);
        try {
            String roleTypeIdTo = "";
            String securityGroupId = "";
            // To assaign Role Type to new party
            if (UtilValidate.isNotEmpty(rmPartyId) && UtilValidate.isNotEmpty(contactListId) && UtilValidate.isNotEmpty(partyId)) {
                EntityConditionList < EntityCondition > roleCondition = EntityCondition.makeCondition(UtilMisc.toList(
                        EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, rmPartyId),
                        EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_MANAGER")),
                    EntityOperator.AND);
                List < GenericValue > partyRoleCondition = delegator.findList("PartyRole", roleCondition, null, null, null, false);

                if (partyRoleCondition != null && partyRoleCondition.size() > 0) {
                    roleTypeIdTo = "ACCOUNT_MANAGER";
                    securityGroupId = "ACCOUNT_OWNER";
                } else {
                    return doJSONResponse(response, UtilMisc.toMap("message", UtilProperties.getMessage("crmUiLabels", "partyNotFound", locale)));
                }
                if("CONTACT".equals(roleTypeIdFrom)) {
                    roleTypeIdFrom = "ACCOUNT";
                    partyIdFrom = accountPartyId;
                } else {
                    partyIdFrom = partyId;
                }
                
                if (UtilValidate.isNotEmpty(partyIdFrom) && UtilValidate.isNotEmpty(roleTypeIdFrom)) {
                    EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyIdFrom),
                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
                        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeIdFrom),
                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();

                    if (responsibleFor != null && responsibleFor.size() > 0) {
                        for (GenericValue partyRelationship: responsibleFor) {
                            partyRelationship.set("thruDate", nowTimestamp);
                            partyRelationship.store();
                        }
                    }
                    GenericValue partyRelationshipcreate = delegator.makeValue("PartyRelationship");
                    partyRelationshipcreate.set("partyIdFrom", partyIdFrom);
                    partyRelationshipcreate.set("partyIdTo", rmPartyId);
                    partyRelationshipcreate.set("roleTypeIdFrom", roleTypeIdFrom);
                    partyRelationshipcreate.set("roleTypeIdTo", roleTypeIdTo);
                    partyRelationshipcreate.set("securityGroupId", securityGroupId);
                    partyRelationshipcreate.set("fromDate", nowTimestamp);
                    partyRelationshipcreate.set("partyRelationshipTypeId", "RESPONSIBLE_FOR");
                    partyRelationshipcreate.create();


                    List < EntityCondition > campaignContactListParty = new ArrayList < EntityCondition > ();
                    campaignContactListParty.add(EntityCondition.makeCondition(EntityOperator.AND,
                        EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
                        EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                        EntityUtil.getFilterByDateExpr()));
                    if("ACCOUNT".equals(roleTypeIdFrom)) {
                        campaignContactListParty.add(EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, partyIdFrom));
                    } 
                    
                    EntityCondition CampListPartycondition = EntityCondition.makeCondition(campaignContactListParty, EntityOperator.AND);
                    List < GenericValue > campaignContactListPartyList = delegator.findList("CampaignContactListParty", CampListPartycondition, null, null, null, false);
                    if (campaignContactListPartyList != null && campaignContactListPartyList.size() > 0) {
                        for (GenericValue campaignContactListPartyGV: campaignContactListPartyList) {
                            campaignContactListPartyGV.set("csrPartyId", rmPartyId);
                            campaignContactListPartyGV.store();
                        }
                    }
                } else {
                    return doJSONResponse(response, UtilMisc.toMap("message", UtilProperties.getMessage("crmUiLabels", "partyNotFound", locale)));
                }
            } else {
                return doJSONResponse(response, UtilMisc.toMap("message", UtilProperties.getMessage("crmUiLabels", "reassignPartyNotFound", locale)));
            }
        } catch (GenericEntityException e) {
            Debug.logError("Exception in Update Person Responsible For" + e.getMessage(), MODULE);
            return doJSONResponse(response, UtilMisc.toMap("message", UtilProperties.getMessage("crmUiLabels", "reassignProcessFailed", locale)));
        }
        return doJSONResponse(response, UtilMisc.toMap("message", UtilProperties.getMessage("crmUiLabels", "partySuccessFullyReassign", locale), "code", "200"));
    }
}
