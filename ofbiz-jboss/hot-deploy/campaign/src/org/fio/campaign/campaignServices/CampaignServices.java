package org.fio.campaign.campaignServices;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.fio.campaign.util.DataUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;


public class CampaignServices {
	public static final String resource = "campaignUiLabels";
	private static final String MODULE = CampaignServices.class.getName();
	
	public static Map<String, Object> rmsCreateCampaignRequest(DispatchContext ctx,
			Map<String, ? extends Object> context) throws ParseException {
		Map<String, Object> result = FastMap.newInstance();
		Delegator delegator = ctx.getDelegator();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		Locale locale = (Locale) context.get("locale");
		String shortName = (String) context.get("campaignName");
		String campaignTypeId = (String) context.get("campaignTypeId");
		String campaignStartDate = (String) context.get("fromDate");
		String campaignEndDate = (String) context.get("thruDate");
		Timestamp timeStampEndDate = null;
		Timestamp timeStampStartDate = null;
		if (UtilValidate.isNotEmpty(campaignEndDate)) {
			DateFormat formatter;
			formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date = (Date) formatter.parse(campaignEndDate);
			timeStampEndDate = UtilDateTime.getDayEnd(new Timestamp(date.getTime()));
		}
		if (UtilValidate.isNotEmpty(campaignStartDate)) {
			DateFormat formatter;
			formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date = (Date) formatter.parse(campaignStartDate);
			timeStampStartDate = UtilDateTime.getDayStart(new Timestamp(date.getTime()));
		}
		String contactListIds = (String) context.get("mailingListId");
		String parentCampaignId = (String) context.get("parentCampaignId");
		String masterCampaignId = (String) context.get("masterCampaignId");
		BigDecimal budgetSpend = (BigDecimal) context.get("budgetSpend");
		BigDecimal budgetRevenue = (BigDecimal) context.get("budgetRevenue");
		BigDecimal budgetMargin = (BigDecimal) context.get("budgetMargin");
		BigDecimal openTarget = (BigDecimal) context.get("openTarget");
		BigDecimal clickTarget = (BigDecimal) context.get("clickTarget");
		BigDecimal conversionTarget = (BigDecimal) context.get("conversionTarget");
		BigDecimal actualSpendValue = (BigDecimal) context.get("actualSpendValue");
		BigDecimal actualBudgetRevenue = (BigDecimal) context.get("actualBudgetRevenue");
		BigDecimal actualBudgetMargin = (BigDecimal) context.get("actualBudgetMargin");
		BigDecimal actualOpenTarget = (BigDecimal) context.get("actualOpenTarget");
		BigDecimal actualClickTarget = (BigDecimal) context.get("actualClickTarget");
		BigDecimal actualConversionTarget = (BigDecimal) context.get("actualConversionTarget");
		BigDecimal contactsMade = (BigDecimal) context.get("contactsMade");
		BigDecimal actualContactsMade = (BigDecimal) context.get("actualContactsMade");
		BigDecimal offerCodeCount = (BigDecimal) context.get("offerCodeCount");
		String costPerAction = (String) context.get("costPerAction");
		String timeZone = (String) context.get("timeZoneCamp");
		String campaignCode = (String) context.get("campaignCode");
		String objectives = (String) context.get("objectives");
		String classifyId= (String) context.get("classifyId");
		String product = (String) context.get("product");
		String subProduct = (String) context.get("subProduct");
		String measurementOfSuccess = (String) context.get("measurementOfSuccess");
		String roleTypeId = (String) context.get("roleTypeId");
		String campaignEmailType=(String) context.get("campaignEmailType");
		String salesTriggerType=(String)context.get("salesTriggerType");
		String multiTemplate=(String)context.get("multiTemplate");
		List templateIdList = (List) context.get("specDateTemplateSelected");
		List segmentGroupId = (List) context.get("specSegmentSelected");
		List segmentCustomFieldId = (List) context.get("specCustFieldSelected");
		String rmBccStatus=(String)context.get("rmBccStatus");
		
		String errMsg = null;
		if (UtilValidate.isNotEmpty(campaignCode))
			campaignCode = campaignCode.trim();

		if (parentCampaignId != null && parentCampaignId != "") {
			if (parentCampaignId.indexOf("(") > 0 && parentCampaignId.indexOf(")") > 0) {
				parentCampaignId = parentCampaignId.substring(parentCampaignId.indexOf("(") + 1,
						parentCampaignId.indexOf(")"));
			}
			
            if(UtilValidate.isNotEmpty(parentCampaignId)) {
                Boolean parentCampaignValidation = campaignValidation(delegator, parentCampaignId);
                if (!parentCampaignValidation) {
                    errMsg = UtilProperties.getMessage(resource, "invalidParentCampaign", locale);
                    return ServiceUtil.returnError(errMsg);
                }
            }
		}
		if (masterCampaignId != null && masterCampaignId != "") {
			if (masterCampaignId.indexOf("(") > 0 && masterCampaignId.indexOf(")") > 0) {
				masterCampaignId = masterCampaignId.substring(masterCampaignId.indexOf("(") + 1,
						masterCampaignId.indexOf(")"));
			}
			
            if(UtilValidate.isNotEmpty(masterCampaignId)) {
                Boolean masterCampaignValidation = campaignValidation(delegator, masterCampaignId);
                if (!masterCampaignValidation) {
                    errMsg = UtilProperties.getMessage(resource, "invalidMasterCampaign", locale);
                    return ServiceUtil.returnError(errMsg);
                }
            
            }
		}

		String mailingListId = "";
		String testMailingListId = "";
		String mergeFormSmsId = "";
		int mailListId = 0;
		if (contactListIds != null && contactListIds != "") {
			mailingListId = contactListIds;
		}
		String testListId = (String) context.get("testMailingListId");
		testMailingListId = testListId;

		String mergeFormId = (String) context.get("mergeFormId");
		if (mergeFormId != null && mergeFormId != "") {
			if (mergeFormId.indexOf("(") > 0 && mergeFormId.indexOf(")") > 0) {
				mergeFormId = mergeFormId.substring(mergeFormId.lastIndexOf("(") + 1, mergeFormId.lastIndexOf(")"));
			}
		}
		if (costPerAction != null && costPerAction != "") {
			if (costPerAction.indexOf("(") > 0 && costPerAction.indexOf(")") > 0) {
				costPerAction = costPerAction.substring(costPerAction.lastIndexOf("(") + 1, costPerAction.lastIndexOf(")"));
			}
		}
		String text = (String) context.get("text");
		String description = (String) context.get("campaignSummary");
		String templateContent = "";
		String mergeFormName = "";
		try {

			if (mergeFormId != null && mergeFormId != "") {
				List<GenericValue> mergeList = delegator.findByAnd("TemplateAndCategory",
						UtilMisc.toMap("templateId", mergeFormId), null, false);
				if (mergeList != null && mergeList.size()>0) {
					GenericValue merge = mergeList.get(0);
					if (merge != null && merge.size() > 0) {
						mergeFormName = merge.getString("templateName");
						templateContent = merge.getString("textContent");
						// text = merge.getString("text");
					}
				}
			}

			if (!shortName.equals("") && !shortName.equals(null) && !campaignTypeId.equals("")
					&& !campaignTypeId.equals(null)) {
				GenericValue marketingCampaign = delegator.makeValue("MarketingCampaign");
				marketingCampaign.put("marketingCampaignId", delegator.getNextSeqId("MarketingCampaign"));
				marketingCampaign.put("statusId", "MKTG_CAMP_CREATED");
				marketingCampaign.put("campaignName", shortName);
				marketingCampaign.put("campaignTemplateId", mergeFormId);
				marketingCampaign.put("campaignTemplateName", mergeFormName);
				marketingCampaign.put("objectives", objectives);
				marketingCampaign.put("campaignSummary", description);
				marketingCampaign.put("startDate", timeStampStartDate);
				marketingCampaign.put("endDate", timeStampEndDate);
				marketingCampaign.put("campaignTypeId", campaignTypeId);
				marketingCampaign.put("campaignCode", campaignCode);
				marketingCampaign.put("classificationType", classifyId);
				marketingCampaign.put("parentCampaignId", parentCampaignId);
				marketingCampaign.put("masterCampaignId", masterCampaignId);
				marketingCampaign.put("timeZoneId", timeZone);
				marketingCampaign.put("product", product);
				marketingCampaign.put("subProduct", subProduct);
				marketingCampaign.put("measurementOfSuccess", measurementOfSuccess);
				marketingCampaign.put("offerCodeCount", offerCodeCount);
				marketingCampaign.put("roleTypeId", roleTypeId);
				marketingCampaign.put("campaignEmailType", campaignEmailType);
				marketingCampaign.put("salesTriggerType", salesTriggerType);
				Debug.log("---------CAMPAIGN USER LOGIN INFORMATION -----"+userLogin);
				marketingCampaign.put("createdByUserLogin", userLogin.getString("userLoginId"));
				marketingCampaign.put("lastModifiedByUserLogin", userLogin.getString("userLoginId"));
				marketingCampaign.put("multiTemplate", multiTemplate);
				marketingCampaign.put("rmBccStatus", rmBccStatus);
				marketingCampaign.create();
				result = ServiceUtil.returnSuccess(UtilProperties.getMessage(resource, "campaignSuccessfullyCreated", locale));
				result.put("marketingCampaignId", marketingCampaign.getString("marketingCampaignId"));
				Debug.log("------------campaign success------------" + marketingCampaign.getString("marketingCampaignId"));
				String marketingCampaignId = marketingCampaign.getString("marketingCampaignId");
				String marketingCampaignName = marketingCampaign.getString("campaignName");
				if (UtilValidate.isNotEmpty(costPerAction)) {
					GenericValue marketingFinancial = delegator.findOne("FinancialMetricConfig",
							UtilMisc.toMap("financialConfigId", costPerAction), false);
					if (marketingFinancial != null && marketingFinancial.size() > 0) {
						GenericValue mktFinancialMetrics = delegator.makeValue("MarketingCampaignFinancialMetrics");
						mktFinancialMetrics.put("marketingCampaignId", marketingCampaignId);
						mktFinancialMetrics.put("financialConfigId", costPerAction);
						mktFinancialMetrics.put("budgetSpendValue", budgetSpend);
						if (timeStampStartDate != null) {
							mktFinancialMetrics.put("startDate", timeStampStartDate);
						} else {
							mktFinancialMetrics.put("startDate", UtilDateTime.nowTimestamp());
						}
						if (timeStampEndDate != null) {
							mktFinancialMetrics.put("endDate", timeStampEndDate);
						} else {
							mktFinancialMetrics.put("endDate", UtilDateTime.nowTimestamp());
						}
						mktFinancialMetrics.put("budgetRevenueValue", budgetRevenue);
						mktFinancialMetrics.put("budgetMarginValue", budgetMargin);
						mktFinancialMetrics.put("openTargetPerc", openTarget);
						mktFinancialMetrics.put("clickTargetPerc", clickTarget);
						mktFinancialMetrics.put("conversionTargetPerc", conversionTarget);
						mktFinancialMetrics.put("actualSpendValue", actualSpendValue);
						mktFinancialMetrics.put("actualBudgetRevenue", actualBudgetRevenue);
						mktFinancialMetrics.put("actualBudgetMargin", actualBudgetMargin);
						mktFinancialMetrics.put("actualOpenTarget", actualOpenTarget);
						mktFinancialMetrics.put("actualClickTarget", actualClickTarget);
						mktFinancialMetrics.put("actualConversionTarget", actualConversionTarget);
						mktFinancialMetrics.put("contactsMade", contactsMade);
						mktFinancialMetrics.put("actualContactsMade", actualContactsMade);
						mktFinancialMetrics.create();
					}
				}
				if("Y".equals(multiTemplate)){
					if(templateIdList != null && segmentCustomFieldId != null && templateIdList.size() > 0 && segmentCustomFieldId.size() > 0){
						if( templateIdList.size() == segmentCustomFieldId.size() ){
							for(int i = 0; i < segmentCustomFieldId.size(); i++){
								String tempId = (String)templateIdList.get(i);
								if(tempId != null){
								GenericValue storeMultiTemp = delegator.makeValue("CampaignMultiTemplate");
								storeMultiTemp.put("seqId", delegator.getNextSeqId("CampaignMultiTemplate"));
								storeMultiTemp.put("marketingCampaignId", marketingCampaignId);
								storeMultiTemp.put("templateId", templateIdList.get(i));
								storeMultiTemp.put("groupId", segmentGroupId.get(i));
								storeMultiTemp.put("customFieldId", segmentCustomFieldId.get(i));
								storeMultiTemp.create();
								}
							}
							Debug.log("------------M-tem-seg--stored-successfully----------");
						}
					}
				}
				
				return result;

			} else {
				errMsg = UtilProperties.getMessage(resource, "CouldnotcreatetheCampaign", locale);
				return ServiceUtil.returnError(errMsg);
			}
		} catch (Exception e) {
			Debug.log("error while updating campaign" + e.getMessage());
		}
		return result;

	}
	
	public static String activeAndExpireMarketingContactList(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		String campaignListId=request.getParameter("campaignListId");
		String isExpire=request.getParameter("isExpire");
		try {

			request.setAttribute("marketingCampaignId", marketingCampaignId);
			if(UtilValidate.isNotEmpty(marketingCampaignId) && UtilValidate.isNotEmpty(campaignListId) && UtilValidate.isNotEmpty(isExpire)){
				GenericValue marketingCampaignContactList = delegator.findOne("MarketingCampaignContactList",
						UtilMisc.toMap("campaignListId", campaignListId), false);
				if(marketingCampaignContactList!=null && marketingCampaignContactList.size()>0){
					if(isExpire.equals("Y")){
						marketingCampaignContactList.set("thruDate",UtilDateTime.nowTimestamp());
						request.setAttribute("_EVENT_MESSAGE_","Contact list expired successfully.");
					}else if(isExpire.equals("N")){
						marketingCampaignContactList.set("thruDate",null);
						request.setAttribute("_EVENT_MESSAGE_","Contact list activated successfully.");
					}
					marketingCampaignContactList.store();
				}
			}else{
				request.setAttribute("_ERROR_MESSAGE_","Error in contact list status change.");
				return "error";
			}
		}catch(Exception e){
			Debug.log("error while Creating  campaign"+e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		return "success";
	}
	public static String removeContactListCampagin(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String contactListId=request.getParameter("contactListId");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		Timestamp thruDate = UtilDateTime.nowTimestamp();
		try {
			if(UtilValidate.isNotEmpty(contactListId) && UtilValidate.isNotEmpty(marketingCampaignId)){
				List<GenericValue> contactList =delegator.findByAnd("MarketingCampaignContactList",UtilMisc.toMap("marketingCampaignId",marketingCampaignId,"contactListId",contactListId), null, false);
				if(contactList!=null && contactList.size()>0 ){
					for(GenericValue contact : contactList){
						contact.remove();
					}
				}
			}
		}catch(Exception e){
			Debug.log("error while removing  campaign"+e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		request.setAttribute("_EVENT_MESSAGE_","Contact list removed from campagin successfully.");
		return "success";
	}
	public static String publishMarketingCampaign(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		try {
			
			
			if(UtilValidate.isNotEmpty(marketingCampaignId)){
				GenericValue marketingCampaign =delegator.findOne("MarketingCampaign",
						UtilMisc.toMap("marketingCampaignId", marketingCampaignId), false);
				if(marketingCampaign!=null && marketingCampaign.size()>0){
					marketingCampaign.set("statusId", "MKTG_CAMP_PUBLISHED");
					marketingCampaign.set("isApproved", "Y");
					//marketingCampaign.set("campTestStatus", "C");
					marketingCampaign.store();
				}
				
				// auto publish the drip campaigns which were associated with this campaign
				publishRecursiveMarketingCampaign(delegator, UtilMisc.toList(marketingCampaignId));
				/*List<GenericValue> dripCampaignList = delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("parentCampaignId",marketingCampaignId), null, false);
				if(dripCampaignList != null && dripCampaignList.size() > 0) {
					for(GenericValue dripCampaign : dripCampaignList) {
						String mktCampaignId = dripCampaign.getString("marketingCampaignId");
						List<GenericValue> isDripSet = delegator.findByAnd("CustomFieldCampaignConfigAssoc", UtilMisc.toMap("marketingCampaignId",mktCampaignId), null, false);
						if(isDripSet != null && isDripSet.size() > 0) {
							dripCampaign.set("statusId", "MKTG_CAMP_PUBLISHED");
							dripCampaign.set("isApproved", "Y");
							dripCampaign.store();
						}
					}
				}*/
			}
		}catch(Exception e){
			Debug.log("error while Campaign Publish"+e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		request.setAttribute("_EVENT_MESSAGE_","Campaign Published successfully.");
		return "success";
	}
	
	public static void publishRecursiveMarketingCampaign(Delegator delegator, List<String> marketingCampaignId) {
		List<String> historicalCampId = new LinkedList<String>();
		try {
			if(marketingCampaignId != null && marketingCampaignId.size() > 0) {
				EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("parentCampaignId",EntityOperator.IN,marketingCampaignId),
						EntityCondition.makeCondition(EntityOperator.OR,
								EntityCondition.makeCondition("salesTriggerType",EntityOperator.EQUALS,null),
								EntityCondition.makeCondition("salesTriggerType",EntityOperator.EQUALS,"")
								)
						); 
				//List<GenericValue> dripCampaignList = delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("parentCampaignId",marketingCampaignId), null, false);
				List<GenericValue> dripCampaignList = delegator.findList("MarketingCampaign", condition, UtilMisc.toSet("marketingCampaignId"), null, null, false);
				if(dripCampaignList != null && dripCampaignList.size() > 0) {
					List<String> mktCampList = EntityUtil.getFieldListFromEntityList(dripCampaignList, "marketingCampaignId", true);
					for(String mktCampaignId : mktCampList) {
						//String mktCampaignId = dripCampaign.getString("marketingCampaignId");
						GenericValue mktCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",mktCampaignId), false);
						List<GenericValue> isDripSet = delegator.findByAnd("CustomFieldCampaignConfigAssoc", UtilMisc.toMap("marketingCampaignId",mktCampaignId), null, false);
						if(isDripSet != null && isDripSet.size() > 0) {
							mktCampaign.set("statusId", "MKTG_CAMP_PUBLISHED");
							mktCampaign.set("isApproved", "Y");
							//mktCampaign.set("campTestStatus", "C");
							mktCampaign.store();
							historicalCampId.add(mktCampaignId);
						}
					}
					publishRecursiveMarketingCampaign(delegator, historicalCampId);
				}
			}
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
	}
	
	public static String publishTestMarketingCampaign(HttpServletRequest request,HttpServletResponse response){
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String marketingCampaignId=request.getParameter("marketingCampaignId");
		try {
			if(UtilValidate.isNotEmpty(marketingCampaignId)){
				enableTestContactList(delegator,marketingCampaignId);
				GenericValue marketingCampaign =delegator.findOne("MarketingCampaign",
						UtilMisc.toMap("marketingCampaignId", marketingCampaignId), false);
				if(marketingCampaign!=null && marketingCampaign.size()>0){
					marketingCampaign.set("campTestStatus", "Y");
					marketingCampaign.store();
				}
				
			}
		}catch(Exception e){
			Debug.log("error while Test Campaign Publish"+e.getMessage());
			request.setAttribute("_ERROR_MESSAGE_",e.getMessage());
			return "error";
		}
		request.setAttribute("_EVENT_MESSAGE_","Test Campaign Published successfully.");
		return "success";
	}
	
	public static void enableTestContactList(Delegator delegator, String marketingCampaignId) {	
		try {
			EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("marketingCampaignId",EntityOperator.EQUALS,marketingCampaignId),
					EntityCondition.makeCondition("contactListTypeId",EntityOperator.EQUALS,"CAMPAIGN_LIST"),
					EntityCondition.makeCondition("contactListId",EntityOperator.NOT_EQUAL,"RMSales_Trigger_Monday_EveryMonth")
					); 
			List<GenericValue> getContactList = delegator.findList("ContactList", condition,null, null, null, false);
			if(getContactList != null && getContactList.size() > 0) {
				for(GenericValue contactList : getContactList) {
					String contactListId = contactList.getString("contactListId");
					EntityCondition contactCondition = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("contactListId",EntityOperator.EQUALS,contactListId),
							EntityCondition.makeCondition("contactPurposeTypeId",EntityOperator.EQUALS,"TEST"),
							EntityCondition.makeCondition("contactListId",EntityOperator.NOT_EQUAL,"RMSales_Trigger_Monday_EveryMonth")
							);
					List<GenericValue> getCampaignList = delegator.findList("CampaignContactListParty", contactCondition, null, null, null, false);
					if(getCampaignList != null && getCampaignList.size() > 0) {
						for(GenericValue campaignList : getCampaignList) {
							String campaigncontactListId = campaignList.getString("contactListId");
							String campaignPartyId = campaignList.getString("partyId");
							String campaignAcctId = campaignList.getString("acctPartyId");
							 List<EntityCondition> campaignListCond = new LinkedList<EntityCondition>();
							 campaignListCond.add(EntityCondition.makeCondition("contactListId",EntityOperator.EQUALS,campaigncontactListId));
							 campaignListCond.add(EntityCondition.makeCondition("partyId",EntityOperator.EQUALS,campaignPartyId));
							 campaignListCond.add(EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, campaignAcctId));
							 campaignListCond.add(EntityCondition.makeCondition("contactPurposeTypeId",EntityOperator.EQUALS,"TEST"));
							 campaignListCond.add(EntityCondition.makeCondition("contactListId",EntityOperator.NOT_EQUAL,"RMSales_Trigger_Monday_EveryMonth"));	
							 GenericValue updateCampaignList = EntityQuery.use(delegator).from("CampaignContactListParty").where(campaignListCond).filterByDate().orderBy("-fromDate").queryFirst();
							 if(updateCampaignList != null && updateCampaignList.size() > 0) {
							 updateCampaignList.set("statusId",null);
							 updateCampaignList.store();
							 }
						}
					}
				}
			}	
		} catch (GenericEntityException e) {
			e.printStackTrace();
		}
	}
	
	public static Map<String, Object> rmsUpdateCampaignRequest(DispatchContext ctx, Map<String, ? extends Object> context) {
		Map<String, Object> result = FastMap.newInstance();
		Delegator delegator = ctx.getDelegator();
		LocalDispatcher dispatcher = ctx.getDispatcher();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		Locale locale = (Locale) context.get("locale");
		Timestamp now = UtilDateTime.nowTimestamp();
		String mailingId = (String) context.get("marketingCampaignId");
		String remainder= (String)context.get("remainder");
		String parentCampaignId =(String)context.get("parentCampaignId");
		String classificationType = (String) context.get("classificationType"); 
		String contactListIds = (String) context.get("mailingListId");
		String masterCampaignId =(String)context.get("masterCampaignId");
		BigDecimal budgetSpend =(BigDecimal)context.get("budgetSpend");
		BigDecimal budgetRevenue =(BigDecimal)context.get("budgetRevenue");
		BigDecimal budgetMargin =(BigDecimal)context.get("budgetMargin");
		BigDecimal openTarget =(BigDecimal)context.get("openTarget");
		BigDecimal clickTarget =(BigDecimal)context.get("clickTarget");
		BigDecimal conversionTarget =(BigDecimal)context.get("conversionTarget");
		BigDecimal actualSpendValue =(BigDecimal)context.get("actualSpendValue");
		BigDecimal actualBudgetRevenue =(BigDecimal)context.get("actualBudgetRevenue");
		BigDecimal actualBudgetMargin =(BigDecimal)context.get("actualBudgetMargin");
		BigDecimal actualOpenTarget =(BigDecimal)context.get("actualOpenTarget");
		BigDecimal actualClickTarget =(BigDecimal)context.get("actualClickTarget");
		BigDecimal actualConversionTarget =(BigDecimal)context.get("actualConversionTarget");
		BigDecimal offerCodeCount = (BigDecimal) context.get("offerCodeCount");
		String costPerAction =(String)context.get("costPerAction");
		String timeZone =(String)context.get("timeZoneCamp");
		String objectives=(String) context.get("objectives");
		BigDecimal contactsMade =(BigDecimal)context.get("contactsMade");
		BigDecimal actualContactsMade =(BigDecimal)context.get("actualContactsMade");
		String classifyId= (String) context.get("classifyId");
		String product = (String) context.get("product");
		String subProduct = (String) context.get("subProduct");
		String measurementOfSuccess = (String) context.get("measurementOfSuccess");
		String roleTypeId = (String) context.get("roleTypeId");
		String campaignEmailType=(String) context.get("campaignEmailType");
		String salesTriggerType=(String) context.get("salesTriggerType");
		String multiTemplate=(String)context.get("multiTemplate");
		List templateIdList = (List) context.get("specDateTemplateSelected");
		List segmentGroupId = (List) context.get("specSegmentSelected");
		List segmentCustomFieldId = (List) context.get("specCustFieldSelected");
		List tempSeqId = (List) context.get("tempSeqId");
		String rmBccStatus=(String)context.get("rmBccStatus");
		Boolean isCampaignTypeChanged = false;
		String oldCampaignType = "";
		Debug.logInfo("Update Campaign:::", "");
		String errMsg = null;
		try {
			GenericValue mktCmp = delegator.findOne("MarketingCampaign",
					UtilMisc.toMap("marketingCampaignId", mailingId), false);
			String shortName = (String) context.get("campaignName");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(shortName) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getString("campaignName"))) {
				shortName = mktCmp.getString("campaignName");
			}
			String campaignTypeId = (String) context.get("campaignTypeId");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(campaignTypeId) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getString("campaignTypeId"))) {
				campaignTypeId = mktCmp.getString("campaignTypeId");
			}
			result = ServiceUtil.returnSuccess("Campaign Updated Successfully");
			if(mktCmp != null && mktCmp.size() > 0 && UtilValidate.isNotEmpty(classifyId) && "BATCH".equalsIgnoreCase(classifyId)) {
				if(UtilValidate.isNotEmpty(campaignTypeId) && UtilValidate.isNotEmpty(mktCmp.getString("campaignTypeId")) && !(campaignTypeId.equals(mktCmp.getString("campaignTypeId")))) {
					List<GenericValue> marketingCampaignContactList = delegator.findByAnd("MarketingCampaignContactList", UtilMisc.toMap("marketingCampaignId", mailingId), null, false);
					if(marketingCampaignContactList != null && marketingCampaignContactList.size() > 0) {
						return ServiceUtil.returnError("List already associated cannot change delivery method");
					} else {
						String groupId = mktCmp.getString("campaignTypeId")+ "_" + mktCmp.getString("marketingCampaignId");
						String segmentationValueAssociatedEntityName = DataUtil.getSegmentationValueAssociatedEntityName(delegator, groupId);
						List<GenericValue> segmentationValueAssociation = delegator.findByAnd(segmentationValueAssociatedEntityName, UtilMisc.toMap("groupId", groupId), null, false);
						if(segmentationValueAssociation == null || segmentationValueAssociation.size() < 1) {
						    isCampaignTypeChanged = true;
						    oldCampaignType = mktCmp.getString("campaignTypeId");
						} else {
							return ServiceUtil.returnError("Customers exists in the current campaign segment cannot change delivery method");
						}
					}
				}
			}
			String statusId = (String) context.get("statusId");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(statusId) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getString("statusId"))) {
				statusId = mktCmp.getString("statusId");
			}
			if(costPerAction !=null && costPerAction != ""){
				if( costPerAction.indexOf("(")>0 && costPerAction.indexOf(")")>0 ){
					costPerAction=costPerAction.substring(costPerAction.indexOf("(")+1, costPerAction.indexOf(")"));   
				}
			}

			if(parentCampaignId !=null && parentCampaignId != ""){
				if( parentCampaignId.indexOf("(")>0 && parentCampaignId.indexOf(")")>0 ){
					parentCampaignId=parentCampaignId.substring(parentCampaignId.indexOf("(")+1, parentCampaignId.indexOf(")"));   
				}
                if(UtilValidate.isNotEmpty(parentCampaignId)) {
                    Boolean parentCampaignValidation = campaignValidation(delegator, parentCampaignId);
                    if (!parentCampaignValidation) {
                        errMsg = UtilProperties.getMessage(resource, "invalidParentCampaign", locale);
                        return ServiceUtil.returnError(errMsg);
                    }
                }
			}
			if(masterCampaignId !=null && masterCampaignId != ""){
				if( masterCampaignId.indexOf("(")>0 && masterCampaignId.indexOf(")")>0 ){
					masterCampaignId=masterCampaignId.substring(masterCampaignId.indexOf("(")+1, masterCampaignId.indexOf(")"));   
				}
				
                if(UtilValidate.isNotEmpty(masterCampaignId)) {
                    Boolean masterCampaignValidation = campaignValidation(delegator, masterCampaignId);
                    if (!masterCampaignValidation) {
                        errMsg = UtilProperties.getMessage(resource, "invalidMasterCampaign", locale);
                        return ServiceUtil.returnError(errMsg);
                    }
                    
                }
			}
			/*Timestamp startDate1 = (Timestamp) context.get("fromDate");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(startDate1) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getTimestamp("startDate"))) {
				startDate1 = mktCmp.getTimestamp("startDate");
			}
			String startDate = String.valueOf(startDate1);*/
			String testListId = (String) context.get("testMailingListId");

			String campaignCode = (String) context.get("campaignCode");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(campaignCode) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getString("campaignCode"))) {
				campaignCode = mktCmp.getString("campaignCode");
			}
			
			if (UtilValidate.isNotEmpty(campaignCode))
				campaignCode = campaignCode.trim();

			String mailingListId = "";
			String testMailingListId = "";
			int mailListId = 0;
			Debug.logInfo("contactListIds:::" + contactListIds, "");
			if (contactListIds != null && contactListIds != "") {/*
				StringTokenizer st = new StringTokenizer(contactListIds, "()");
				Debug.logInfo("st tkens:::" + st.countTokens(), "");
				if (st.countTokens() == 2) {
					String mg1 = st.nextToken();
					mailingListId = st.nextToken();
					mailListId = Integer.parseInt(mailingListId);
				} else {
					mailingListId = (String) context.get("mailingListId");
					mailListId = Integer.parseInt((String) context.get("mailingListId"));
				}
			*/}

			
			
			String mergeFormName = "";
			String mergeFormId = (String) context.get("mergeFormId");
			if(mergeFormId !=null && mergeFormId != ""){
				if( mergeFormId.indexOf("(")>0 && mergeFormId.indexOf(")")>0 ){
					mergeFormId=mergeFormId.substring(mergeFormId.lastIndexOf("(")+1, mergeFormId.lastIndexOf(")"));   
				}
			}
			Debug.logInfo("testListId:::" + testListId, "");
			if (testListId != null && testListId != "") {/*
				StringTokenizer st1 = new StringTokenizer(testListId, "()");
				Debug.logInfo("st1 tkens:::" + st1.countTokens(), "");
				if (st1.countTokens() == 2) {
					String mg22 = st1.nextToken();
					testMailingListId = st1.nextToken();
				} else {
					testMailingListId = (String) context.get("testMailingListId");
				}
			*/}
			String campaignStartDateIntial = (String) context.get("fromDate");
			String campaignEndDateInitial = (String) context.get("thruDate");
			Timestamp timeStampEndDate=null;
			Timestamp timeStampStartDate=null;
			if(UtilValidate.isNotEmpty(campaignStartDateIntial)){
				DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date date = (Date) formatter.parse(campaignStartDateIntial);
				timeStampStartDate = UtilDateTime.getDayStart(new Timestamp(date.getTime()));
			}
			if(UtilValidate.isNotEmpty(campaignEndDateInitial)){
				DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date date = (Date) formatter.parse(campaignEndDateInitial);
				timeStampEndDate = UtilDateTime.getDayEnd(new Timestamp(date.getTime()));
			}
			Timestamp campaignStartDate = timeStampStartDate;
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(campaignStartDate) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getTimestamp("startDate"))) {
				campaignStartDate = mktCmp.getTimestamp("startDate");
			}
			Debug.logInfo("campaignStartDate:::" + campaignStartDate, "");
			Timestamp campaignEndDate = timeStampEndDate;
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(campaignEndDate) && mktCmp != null && UtilValidate.isNotEmpty(mktCmp.getTimestamp("endDate"))) {
				campaignEndDate = mktCmp.getTimestamp("endDate");
			}
			int templateId = 0;
			if (UtilValidate.isNotEmpty(mergeFormId)) {

				templateId = Integer.parseInt(mergeFormId);
			}
			Debug.logInfo("templateId:::" + templateId, "");
			String description = (String) context.get("campaignSummary");
			if (UtilValidate.isNotEmpty(mktCmp) && UtilValidate.isEmpty(description) && mktCmp != null&& UtilValidate.isNotEmpty(mktCmp.getString("campaignSummary"))) {
				description = mktCmp.getString("campaignSummary");
			}
			Debug.logInfo("description:::" + description, "");
			Debug.logInfo("contactListIds:::" + contactListIds, "");
			String templateContent = "";
			Debug.logInfo("mergeFormId:::" + mergeFormId, "");
			if (mergeFormId != null && mergeFormId != "") {
				List<GenericValue> mergeList=delegator.findByAnd("TemplateAndCategory",UtilMisc.toMap("templateId",mergeFormId), null, false);
				if(mergeList!=null){
					GenericValue merge=mergeList.get(0);
					if (merge!=null && merge.size()>0) {
						mergeFormName = merge.getString("templateName");
						templateContent = merge.getString("textContent");
						//text = merge.getString("text");
					}
				}
			}
			Debug.logInfo("mergeFormName:::" + mergeFormName, "");
			if (!shortName.equals("") && !shortName.equals(null)) {
				Debug.logInfo("campaignTypeId:::" + campaignTypeId, "");
				if (!campaignTypeId.equals("APPS")) {
					result.put("marketingCampaignId", mailingId);
					GenericValue mktCampaign = delegator.findOne("MarketingCampaign",
							UtilMisc.toMap("marketingCampaignId", mailingId), false);
					if (mktCampaign!=null && mktCampaign.size()>0) {
						
						mktCampaign.put("campaignName", shortName);
						mktCampaign.put("campaignSummary", description);
						mktCampaign.put("campaignTemplateId", mergeFormId);
						mktCampaign.put("campaignTemplateName", mergeFormName);
						mktCampaign.put("startDate", campaignStartDate);
						mktCampaign.put("endDate", campaignEndDate);
						mktCampaign.put("campaignTypeId", campaignTypeId);
						mktCampaign.put("campaignCode", campaignCode);
						mktCampaign.put("remainder", remainder);
						mktCampaign.put("classificationType", classifyId);
						mktCampaign.put("parentCampaignId", parentCampaignId);
						mktCampaign.put("masterCampaignId", masterCampaignId);
						mktCampaign.put("timeZoneId", timeZone);
						mktCampaign.put("objectives", objectives);
						mktCampaign.put("product", product);
						mktCampaign.put("subProduct", subProduct);
						mktCampaign.put("measurementOfSuccess", measurementOfSuccess);
						mktCampaign.put("offerCodeCount", offerCodeCount);
						mktCampaign.put("roleTypeId", roleTypeId);
						if(UtilValidate.isNotEmpty(campaignEmailType))
							mktCampaign.put("campaignEmailType", campaignEmailType);
						mktCampaign.put("salesTriggerType", salesTriggerType);
						mktCampaign.put("multiTemplate", multiTemplate);
						mktCampaign.put("rmBccStatus", rmBccStatus);
						if("PHONE_CALL".equals(campaignTypeId)){/*
							mktCampaign.put("classificationType",classificationType);
						*/}
						mktCampaign.store();
						if("Y".equals(multiTemplate)){
							if(tempSeqId == null){
								if(templateIdList != null && segmentCustomFieldId != null && templateIdList.size() > 0 && segmentCustomFieldId.size() > 0){
									if( templateIdList.size() == segmentCustomFieldId.size() ){
										for(int i = 0; i < segmentCustomFieldId.size(); i++){
											GenericValue storeMultiTemp = delegator.makeValue("CampaignMultiTemplate");
											String tempId = (String)templateIdList.get(i);
											if(tempId != null){
												storeMultiTemp.put("seqId", delegator.getNextSeqId("CampaignMultiTemplate"));
												storeMultiTemp.put("marketingCampaignId", mailingId);
												storeMultiTemp.put("templateId", templateIdList.get(i));
												storeMultiTemp.put("groupId", segmentGroupId.get(i));
												storeMultiTemp.put("customFieldId", segmentCustomFieldId.get(i));
												storeMultiTemp.create();
											}
										}
										Debug.log("---M-tem-seg--stored-successfully--");
									}
								}	
							}
							else{
								if(tempSeqId != null && segmentCustomFieldId != null && tempSeqId.size() > 0 && segmentCustomFieldId.size() > 0){
									if( tempSeqId.size() == segmentCustomFieldId.size() ){
										for(int i = 0; i < tempSeqId.size(); i++){
											String seqId = (String)tempSeqId.get(i);
											GenericValue storeMultiTemp=delegator.findOne("CampaignMultiTemplate", UtilMisc.toMap("seqId",seqId),false);
											if(storeMultiTemp!=null && storeMultiTemp.size()>0){
											storeMultiTemp.put("templateId", templateIdList.get(i));
											storeMultiTemp.put("groupId", segmentGroupId.get(i));
											storeMultiTemp.put("customFieldId", segmentCustomFieldId.get(i));
											storeMultiTemp.store();
											}
										}
										Debug.log("----M-template-updated-successfully--");
									}
								}	
							}
							
						}
						if("N".equals(multiTemplate)){
							if(tempSeqId != null && segmentCustomFieldId != null && tempSeqId.size() > 0 && segmentCustomFieldId.size() > 0){
								if( tempSeqId.size() == segmentCustomFieldId.size() ){
									for(int i = 0; i < tempSeqId.size(); i++){
										String seqIdRm = (String)tempSeqId.get(i);
										GenericValue storeMultiTempRmv=delegator.findOne("CampaignMultiTemplate", UtilMisc.toMap("seqId",seqIdRm),false);
										if(storeMultiTempRmv!=null && storeMultiTempRmv.size()>0 ){	
											storeMultiTempRmv.remove();	
										}
											
									}
									
								}
							
							}
									Debug.log("--M-template-removed-successfully---");
						}
						
						if(UtilValidate.isNotEmpty(costPerAction)){
							GenericValue marketingFinancial = delegator.findOne("FinancialMetricConfig",
									UtilMisc.toMap("financialConfigId",costPerAction), false);
							if(marketingFinancial!=null && marketingFinancial.size()>0){
								List<GenericValue> mktFinancial=delegator.findByAnd("MarketingCampaignFinancialMetrics", UtilMisc.toMap("marketingCampaignId",mailingId,"financialConfigId",costPerAction),null,false);
								if(mktFinancial!=null && mktFinancial.size()>0){
									GenericValue mktFinancialValue=mktFinancial.get(0);
									mktFinancialValue.put("budgetSpendValue", budgetSpend);
									mktFinancialValue.put("budgetRevenueValue", budgetRevenue);
									mktFinancialValue.put("budgetMarginValue", budgetMargin);
									mktFinancialValue.put("openTargetPerc", openTarget);
									mktFinancialValue.put("clickTargetPerc", clickTarget);
									mktFinancialValue.put("conversionTargetPerc", conversionTarget);
									mktFinancialValue.put("actualSpendValue", actualSpendValue);
									mktFinancialValue.put("actualBudgetRevenue", actualBudgetRevenue);
									mktFinancialValue.put("actualBudgetMargin", actualBudgetMargin);
									mktFinancialValue.put("actualOpenTarget", actualOpenTarget);
									mktFinancialValue.put("actualClickTarget", actualClickTarget);
									mktFinancialValue.put("actualConversionTarget", actualConversionTarget);
									mktFinancialValue.put("contactsMade", contactsMade);
									mktFinancialValue.put("actualContactsMade", actualContactsMade);
									mktFinancialValue.store();
								}else{
									List<GenericValue> mktFinancialToDelete=delegator.findByAnd("MarketingCampaignFinancialMetrics", UtilMisc.toMap("marketingCampaignId",mailingId),null,false);
	                                if(mktFinancialToDelete!=null && mktFinancialToDelete.size()>0){
	                                	GenericValue toDeleteFin= mktFinancialToDelete.get(0);
	                                	toDeleteFin.remove();
	                                }
								GenericValue mktFinancialMetrics = delegator.makeValue("MarketingCampaignFinancialMetrics");
								mktFinancialMetrics.put("marketingCampaignId", mailingId);
								mktFinancialMetrics.put("financialConfigId", costPerAction);
								mktFinancialMetrics.put("budgetSpendValue", budgetSpend);
								if(campaignStartDate!=null){
									mktFinancialMetrics.put("startDate", campaignStartDate);
								}else{
									mktFinancialMetrics.put("startDate", UtilDateTime.nowTimestamp());
								}
								if(timeStampEndDate!=null){
									mktFinancialMetrics.put("endDate", campaignEndDate);
								}else{
									mktFinancialMetrics.put("endDate", UtilDateTime.nowTimestamp());
								}
								mktFinancialMetrics.put("budgetRevenueValue", budgetRevenue);
								mktFinancialMetrics.put("budgetMarginValue", budgetMargin);
								mktFinancialMetrics.put("openTargetPerc", openTarget);
								mktFinancialMetrics.put("clickTargetPerc", clickTarget);
								mktFinancialMetrics.put("conversionTargetPerc", conversionTarget);
								mktFinancialMetrics.put("actualSpendValue", actualSpendValue);
								mktFinancialMetrics.put("actualBudgetRevenue", actualBudgetRevenue);
								mktFinancialMetrics.put("actualBudgetMargin", actualBudgetMargin);
								mktFinancialMetrics.put("actualOpenTarget", actualOpenTarget);
								mktFinancialMetrics.put("actualClickTarget", actualClickTarget);
								mktFinancialMetrics.put("actualConversionTarget", actualConversionTarget);
								mktFinancialMetrics.put("contactsMade", contactsMade);
								mktFinancialMetrics.put("actualContactsMade", actualContactsMade);
								mktFinancialMetrics.create();
								}
							}
						}
						
						if(UtilValidate.isNotEmpty(isCampaignTypeChanged) && isCampaignTypeChanged && UtilValidate.isNotEmpty(oldCampaignType)) {
						    Map<String, Object> autoCampaignSegmentationContext = new HashMap<String, Object>();
						    autoCampaignSegmentationContext.put("isCampaignTypeChanged", isCampaignTypeChanged);
						    autoCampaignSegmentationContext.put("oldCampaignType", oldCampaignType);
						    autoCampaignSegmentationContext.put("marketingCampaignId", mailingId);
						    if(UtilValidate.isNotEmpty(offerCodeCount)) {
						        autoCampaignSegmentationContext.put("offerCodeCount", String.valueOf(offerCodeCount));
						    }
						    autoCampaignSegmentationContext.put("userLogin", userLogin);
						    Map<String, Object> autoCampaignSegmentation = dispatcher.runSync("segment.autoCampaignSegmentation", autoCampaignSegmentationContext);
							if (ServiceUtil.isSuccess(autoCampaignSegmentation)) {
								Debug.logInfo("Successfully delete old segment code and create new segment code: ", MODULE);
							}
						}
						return ServiceUtil.returnSuccess("Campaign Updated Successfully");
					}
				} /*else {

					GenericValue mktCampaign = delegator.findOne("MarketingCampaign",
							UtilMisc.toMap("marketingCampaignId", mailingId), false);
					Debug.logInfo("mktCampaign:::" + mktCampaign, "");
					if (mktCampaign!=null && mktCampaign.size()>0) {
						mktCampaign.put("marketingCampaignId", mailingId);
						mktCampaign.put("campaignName", shortName);
						mktCampaign.put("campaignSummary", description);
						mktCampaign.put("campaignTemplateId", mergeFormId);
						mktCampaign.put("campaignTemplateName", mergeFormName);
						mktCampaign.put("prevStartDate", preStartDate);
						mktCampaign.put("prevEndDate", preEndDate);
						mktCampaign.put("approvalStartDate", rmsApprvStartDate);
						mktCampaign.put("approvalEndDate", rmsApprvEndDate);
						mktCampaign.put("startDate", campaignStartDate);
						mktCampaign.put("endDate", campaignEndDate);
						mktCampaign.put("campaignTypeId", campaignTypeId);
						mktCampaign.put("campaignCode", campaignCode);
						mktCampaign.put("preference", preference);
						mktCampaign.put("remainder", remainder);
						mktCampaign.put("parentCampaignId", parentCampaignId);
						mktCampaign.put("masterCampaignId", masterCampaignId);
						mktCampaign.put("objectives", objectives);
						if("PHONE_CALL".equals(campaignTypeId)){
							mktCampaign.put("classificationType",classificationType);
						}
						mktCampaign.store();
						if(UtilValidate.isNotEmpty(costPerAction)){
							GenericValue marketingFinancial = delegator.findOne("FinancialMetricConfig",
									UtilMisc.toMap("financialConfigId",costPerAction), false);
							if(marketingFinancial!=null && marketingFinancial.size()>0){
								List<GenericValue> mktFinancial=delegator.findByAnd("MarketingCampaignFinancialMetrics", UtilMisc.toMap("marketingCampaignId",mailingId,"financialConfigId",costPerAction),null,false);
								if(mktFinancial!=null && mktFinancial.size()>0){
									GenericValue mktFinancialValue=mktFinancial.get(0);
									mktFinancialValue.put("budgetSpendValue", budgetSpend);
									mktFinancialValue.put("budgetRevenueValue", budgetRevenue);
									mktFinancialValue.put("budgetMarginValue", budgetMargin);
									mktFinancialValue.put("openTargetPerc", openTarget);
									mktFinancialValue.put("clickTargetPerc", clickTarget);
									mktFinancialValue.put("conversionTargetPerc", conversionTarget);
									mktFinancialValue.put("actualSpendValue", actualSpendValue);
									mktFinancialValue.put("actualBudgetRevenue", actualBudgetRevenue);
									mktFinancialValue.put("actualBudgetMargin", actualBudgetMargin);
									mktFinancialValue.put("actualOpenTarget", actualOpenTarget);
									mktFinancialValue.put("actualClickTarget", actualClickTarget);
									mktFinancialValue.put("actualConversionTarget", actualConversionTarget);
									mktFinancialValue.put("contactsMade", contactsMade);
									mktFinancialValue.put("actualContactsMade", actualContactsMade);
									mktFinancialValue.store();
								}else{
									List<GenericValue> mktFinancialToDelete=delegator.findByAnd("MarketingCampaignFinancialMetrics", UtilMisc.toMap("marketingCampaignId",mailingId),null,false);
	                                if(mktFinancialToDelete!=null && mktFinancialToDelete.size()>0){
	                                	GenericValue toDeleteFin= mktFinancialToDelete.get(0);
	                                	toDeleteFin.remove();
	                                }
								GenericValue mktFinancialMetrics = delegator.makeValue("MarketingCampaignFinancialMetrics");
								mktFinancialMetrics.put("marketingCampaignId", mailingId);
								mktFinancialMetrics.put("budgetSpendValue", budgetSpend);
								mktFinancialMetrics.put("budgetRevenueValue", budgetRevenue);
								mktFinancialMetrics.put("budgetMarginValue", budgetMargin);
								mktFinancialMetrics.put("openTargetPerc", openTarget);
								mktFinancialMetrics.put("clickTargetPerc", clickTarget);
								mktFinancialMetrics.put("conversionTargetPerc", conversionTarget);
								mktFinancialMetrics.put("actualSpendValue", actualSpendValue);
								mktFinancialMetrics.put("actualBudgetRevenue", actualBudgetRevenue);
								mktFinancialMetrics.put("actualBudgetMargin", actualBudgetMargin);
								mktFinancialMetrics.put("actualOpenTarget", actualOpenTarget);
								mktFinancialMetrics.put("actualClickTarget", actualClickTarget);
								mktFinancialMetrics.put("actualConversionTarget", actualConversionTarget);
								mktFinancialMetrics.put("contactsMade", contactsMade);
								mktFinancialMetrics.put("actualContactsMade", actualContactsMade);
								mktFinancialMetrics.create();
								}
							}
						}
						result.put("marketingCampaignId",mktCampaign.getString("marketingCampaignId"));
					}

				}*/
			} else {

				errMsg = UtilProperties.getMessage(resource,"CouldnotUpdatetheCampaign", locale);
				return ServiceUtil.returnError(errMsg);
			}
		} catch (Exception e) {
			Debug.log("error while updating campaign"+e.getMessage());
		}
		return result;
	}
	
	public static Map<String, Object> getContactListParties(DispatchContext dctx, Map<String, ? extends Object> context) {
	        Delegator delegator = dctx.getDelegator();	
			String contactListId = (String) context.get("contactListId");
			String firstName = (String) context.get("firstName");
			String lastName = (String) context.get("lastName");		
			String groupName = (String) context.get("groupName");
			String roleType = (String) context.get("roleType");
			String infoString = (String) context.get("infoString");	
			List impList = (List) context.get("importParties");
			List<GenericValue> contactListParties = FastList.newInstance();
			List<GenericValue> finalList = FastList.newInstance();
			List alreadyMembers = FastList.newInstance();
			List alreadyMembersPartyIds = FastList.newInstance();
			List<EntityCondition> searchConditions = FastList.newInstance();
			GenericValue contactList=null;
			EntityFindOptions findOpts = new EntityFindOptions(true, EntityFindOptions.TYPE_SCROLL_INSENSITIVE, EntityFindOptions.CONCUR_READ_ONLY, true);
			Set<String> fieldsToSelect=UtilMisc.toSet("partyId", "groupName", "firstName", "lastName", "fromDate");
			fieldsToSelect.add("thruDate");
			fieldsToSelect.add("infoString");
			fieldsToSelect.add("contactMechId");
			fieldsToSelect.add("contactMechTypeId");
						
			List<String> orderBy = UtilMisc.toList("firstName", "lastName", "groupName");
	          try {
						contactList = EntityQuery.use(delegator).from("ContactList").where("contactListId",contactListId).queryOne();
						String type=contactList.getString("contactMechTypeId");
						searchConditions = UtilMisc.toList(EntityUtil.getFilterByDateExpr());
						
						if( UtilValidate.isNotEmpty(firstName)){
							searchConditions.add(EntityCondition.makeCondition("firstName", EntityOperator.LIKE, "%" + firstName + "%"));
						}
						
						if( UtilValidate.isNotEmpty(impList)){
							searchConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.IN, impList));
						}
						
						if( UtilValidate.isNotEmpty(lastName)){
							searchConditions.add(EntityCondition.makeCondition("lastName", EntityOperator.LIKE, "%" + lastName + "%"));
						}
						
						if( UtilValidate.isNotEmpty(groupName) && "EMAIL_ADDRESS".equals(type) ){
							searchConditions.add(EntityCondition.makeCondition("groupName", EntityOperator.LIKE, "%" + groupName + "%"));
						}
						if( UtilValidate.isNotEmpty(groupName) && "TELECOM_NUMBER".equals(type) ){
							searchConditions.add(EntityCondition.makeCondition("contactNumber", EntityOperator.LIKE, "%" + groupName + "%"));
						}
						
						
						if( UtilValidate.isNotEmpty(infoString) && "EMAIL_ADDRESS".equals(type) ){
							searchConditions.add(EntityCondition.makeCondition("infoString", EntityOperator.LIKE, "%" + infoString + "%"));
						}
						if( UtilValidate.isNotEmpty(infoString) && "TELECOM_NUMBER".equals(type) ){
							searchConditions.add(EntityCondition.makeCondition("areaCode", EntityOperator.LIKE, "%" + infoString + "%"));
						}
						
						
						if( UtilValidate.isNotEmpty(contactList)){
							searchConditions.add(EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, contactList.getString("contactMechTypeId"))); 
						}
						
						// if a contactListId is given, filter out parties that are already member of this contact list
						if( UtilValidate.isNotEmpty(contactList)){ 
						
							List<EntityCondition> alreadyMembersConditions = new ArrayList<EntityCondition>();
							
							alreadyMembersConditions.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
							alreadyMembersConditions.add(EntityUtil.getFilterByDateExpr("memberFromDate", "memberThruDate"));
							
							EntityCondition alMemCondition = EntityCondition.makeCondition(alreadyMembersConditions, EntityOperator.AND);

							alreadyMembers=delegator.findList("PartyContactInfoAndContactList", alMemCondition, fieldsToSelect,UtilMisc.toList("partyId"),findOpts,false);						
							
							alreadyMembersPartyIds = EntityUtil.getFieldListFromEntityList(alreadyMembers, "partyId", true);
							
							if (alreadyMembersPartyIds != null && alreadyMembersPartyIds.size() > 0) {
								searchConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.NOT_IN, alreadyMembersPartyIds));
							}
							
							if("TELECOM_NUMBER".equals(type)){
							//searchConditions.add(EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PHONE_HOME"));
							searchConditions.add(EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE"));
							}
							else if("EMAIL_ADDRESS".equals(type)){
								searchConditions.add(EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL"));
							}
							else{
								searchConditions.add(EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "BILLING_LOCATION"));
							}
							
						}
			
						List<GenericValue> RoleBaseParty = null;
						List<String> Roles =UtilMisc.toList("ACCOUNT","CUSTOMER","LEAD","CONTACT");
						List<EntityCondition> roleTypeConditions = FastList.newInstance();
						EntityCondition partyStatusCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
					             EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)
					             ), EntityOperator.OR);
					    roleTypeConditions.add(partyStatusCondition);
					    roleTypeConditions.add(EntityUtil.getFilterByDateExpr());
						if(UtilValidate.isNotEmpty(roleType)){
							if("LEAD".equalsIgnoreCase(roleType)) {
								EntityCondition leadRoleCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "LEAD"),
								         EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "LEAD_CONVERTED")
								         ), EntityOperator.AND);
								roleTypeConditions.add(leadRoleCondition);
							} else if("ACCOUNT".equalsIgnoreCase(roleType)) {
								EntityCondition accountRoleCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"));
								roleTypeConditions.add(accountRoleCondition);
							} else if("CONTACT".equalsIgnoreCase(roleType)) {
								EntityCondition contactRoleCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"));
								roleTypeConditions.add(contactRoleCondition);
							}
							//RoleBaseParty = delegator.findList("PartyRole",EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS,roleType),UtilMisc.toSet("partyId"),UtilMisc.toList("partyId"),findOpts,false);
						}else{
							EntityCondition roleCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.IN, Roles));
							roleTypeConditions.add(roleCondition);
							//RoleBaseParty = delegator.findList("PartyRole",EntityCondition.makeCondition("roleTypeId", EntityOperator.IN,Roles),UtilMisc.toSet("partyId"),UtilMisc.toList("partyId"),findOpts,false);
						}
						RoleBaseParty = delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", EntityCondition.makeCondition(roleTypeConditions, EntityOperator.AND), UtilMisc.toSet("partyId"), UtilMisc.toList("createdDate"), null, false);
						List<String> FinalPartyRoles =  EntityUtil.getFieldListFromEntityList(RoleBaseParty, "partyId", true);
						if(FinalPartyRoles != null && FinalPartyRoles.size() > 0){
							searchConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.IN, FinalPartyRoles));
						}
						EntityCondition sclCondition = EntityCondition.makeCondition(searchConditions, EntityOperator.AND);
						if("EMAIL_ADDRESS".equals(type)){
							searchConditions.add(EntityCondition.makeCondition("allowSolicitation", EntityOperator.NOT_EQUAL, "N"));
							searchConditions.add(EntityCondition.makeCondition("infoString", EntityOperator.NOT_EQUAL, null));
						}
						//searchConditions.add(EntityCondition.makeCondition("infoString", EntityOperator.NOT_EQUAL, null));
						findOpts.setMaxRows(1000);
						contactListParties=delegator.findList("PartyContactInfoInfo", sclCondition, fieldsToSelect,orderBy,findOpts,false);
						finalList.addAll(contactListParties);
				} catch (GenericEntityException e) {
					//Debug.logError(e);
				}

	        Map<String, Object> result = FastMap.newInstance();
			result.put("contactListParties", finalList);
	        return result;
	    }
    public static Boolean campaignValidation(Delegator delegator, String marketingCampaignId) {
        Boolean validCampaign = false;
        if (UtilValidate.isNotEmpty(marketingCampaignId)) {
            List < EntityCondition > campaignConditions = new ArrayList < EntityCondition > ();
            campaignConditions.add(EntityCondition.makeCondition(EntityOperator.AND,
                EntityCondition.makeCondition("marketingCampaignId", EntityOperator.EQUALS, marketingCampaignId)
                /*EntityUtil.getFilterByDateExpr()*/));

            List < GenericValue > marketingCampaign;
            try {
                marketingCampaign = delegator.findList("MarketingCampaign", EntityCondition.makeCondition(campaignConditions, EntityOperator.AND), null, null, null, false);
                if (marketingCampaign != null && marketingCampaign.size() > 0) {
                    validCampaign = true;
                }
            } catch (GenericEntityException e) {
                // TODO Auto-generated catch block
                Debug.log("Exception in valid campaign"+e.getMessage());
            }

        }
        return validCampaign;
    }
}
