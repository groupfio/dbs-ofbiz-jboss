package org.fio.campaign.campaignServices;

import java.io.File;
import java.io.FileReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.fio.campaign.common.UtilCommon;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import au.com.bytecode.opencsv.CSVReader;

public class ApproveList {

    private ApproveList() { }
    
    private static final String MODULE = ApproveList.class.getName();
    public static final String resource = "PartyErrorUiLabels";
    public static final String crmResource = "crmUiLabels";
    
    public static Map<String, Object> approveListUpdate(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Map<String, Object> result = ServiceUtil.returnSuccess();
        try {
            List<String> partyIdList = UtilGenerics.checkList(context.get("partyId"));
            String approveListStatus = (String) context.get("approveListStatus");
            String contactListId = (String) context.get("contactListId");
            String marketingCampaignId = (String) context.get("marketingCampaignId");
            if(UtilValidate.isNotEmpty(marketingCampaignId)) {
               result.put("marketingCampaignId",marketingCampaignId);
            }

            if(UtilValidate.isNotEmpty(contactListId) && partyIdList != null && partyIdList.size() > 0) {
                for(String partyId : partyIdList) {
                    String[] splitDataList = partyId.split("&accountId=");
                    String contactId = splitDataList[0];
                    String accountId = "";
                    String contactPurposeTypeId = null;
                    List<EntityCondition> exprList = new LinkedList<EntityCondition>();
                    exprList.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
                    exprList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, contactId));
                    
                    if(splitDataList.length > 1) {
                        accountId = splitDataList[1];
                        if(UtilValidate.isNotEmpty(accountId)) {
                            exprList.add(EntityCondition.makeCondition("acctPartyId", EntityOperator.EQUALS, accountId));
                        }
                    }
                    // test and production record condition
                    if(splitDataList.length > 2) {
                     contactPurposeTypeId = splitDataList[2];
                     exprList.add(EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, contactPurposeTypeId));
                    }else{
                        exprList.add(EntityCondition.makeCondition("contactPurposeTypeId", EntityOperator.EQUALS, contactPurposeTypeId));
                    }
                    GenericValue contactListParty = EntityQuery.use(delegator).from("CampaignContactListParty").where(exprList)
                        .filterByDate().orderBy("-fromDate").queryFirst();
                    if(contactListParty != null && contactListParty.size() > 0) {
                        contactListParty.set("isApproved",approveListStatus);
                        contactListParty.store();
                    }
                }
                /*List<GenericValue> contactListParty = EntityQuery.use(delegator).from("CampaignContactListParty").where(EntityCondition.makeCondition("partyId", EntityOperator.IN, partyIdList),
                        EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId)
                ).queryList();
                if(contactListParty != null && contactListParty.size() > 0) {
                    for(GenericValue contactListPartyGV : contactListParty) {
                        contactListPartyGV.set("isApproved",approveListStatus);
                    }
                    delegator.storeAll(contactListParty);
                }*/
                
                int approvedSize = 0;
                if(UtilValidate.isEmpty(marketingCampaignId)) {
                    GenericValue mktCampCL = EntityQuery.use(delegator).from("MarketingCampaignContactList")
                        .where("contactListId", contactListId).queryFirst();
                    if(mktCampCL != null && mktCampCL.size() > 0) {
                        marketingCampaignId = mktCampCL.getString("marketingCampaignId");
                    }
                }
                
                if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                    List < GenericValue > contactListPartyApproved = EntityQuery.use(delegator).from("CampaignContactListParty")
                        .where("isApproved", "Y", "contactListId", contactListId,"contactPurposeTypeId","LIVE").queryList();
                    if (contactListPartyApproved != null && contactListPartyApproved.size() > 0) {
                        approvedSize = contactListPartyApproved.size();
                    }
                    String approvedSizeString = String.valueOf(approvedSize);
                    GenericValue campCLSummary = delegator.makeValue("CampaignContactListSummary", UtilMisc.toMap("contactListId", contactListId,
                        "campaignId", marketingCampaignId,
                        "attrName", "RM_APPROVED",
                        "attrValue", approvedSizeString));
                    delegator.createOrStore(campCLSummary);
                }
            } 
        } catch(Exception e) {
            Debug.logError("Exception in approveListUpdate"+e.getMessage(), MODULE);
        }
        
        return result; 
    }
    public static Map < String, Object > addContactToListForm(DispatchContext dctx, Map < String, Object > context) {
        Delegator delegator = dctx.getDelegator();
        Map < String, Object > result = ServiceUtil.returnSuccess();
        try {
            List < String > partyIdList = UtilGenerics.checkList(context.get("partyId"));
            String contactListId = (String) context.get("contactListId");
            String marketingCampaignId = (String) context.get("marketingCampaignId");
            String acctPartyId = (String) context.get("acctPartyId");
            String contactPurposeType = (String) context.get("contactPurposeType");
            Timestamp fromDate = UtilDateTime.nowTimestamp();
            String csrPartyId = null;
            if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                result.put("marketingCampaignId", marketingCampaignId);
            }

            if (UtilValidate.isNotEmpty(contactListId) && partyIdList != null && partyIdList.size() > 0) {
                for (String partyId: partyIdList) {
                    if (UtilValidate.isNotEmpty(partyId)) {
                        EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, acctPartyId),
                            EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
                            EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"),
                            EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
                            EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

                        GenericValue responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryFirst();
                        EntityCondition getSeqCondition = EntityCondition.makeCondition(UtilMisc.toList(
                            EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                            EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)), EntityOperator.AND);
                        Long seqCount = delegator.findCountByCondition("CampaignContactListParty",getSeqCondition, null,null);
                        if (responsibleFor != null && responsibleFor.size() > 0) {
                            csrPartyId = responsibleFor.getString("partyIdTo");
                        }
                        GenericValue campCLParty = delegator.makeValue("CampaignContactListParty", UtilMisc.toMap("contactListId", contactListId,
                            "partyId", partyId,
                            "fromDate", fromDate,
                            "acctPartyId", acctPartyId,
                            "contactListSeqId", String.valueOf(seqCount+1),
                            "isApproved", "N"));
                        if (UtilValidate.isNotEmpty(contactPurposeType)) {
                            campCLParty.set("contactPurposeTypeId",contactPurposeType);
                        }
                        if (UtilValidate.isNotEmpty(csrPartyId)) {
                            campCLParty.set("csrPartyId", csrPartyId);
                        }
                        campCLParty.create();
                    }
                }
            }
        } catch (Exception e) {
            Debug.logError("Exception in approveListUpdate" + e.getMessage(), MODULE);
        }
        return result;
    }
    
    /**
     * This method will read the donePage parameter and return it to the
     * controller as the result. It is called from controller.xml using <event
     * type="java" path="org.opentaps.common.event.CommonEvents"
     * invoke="approvePageRequestHelper"/> Then it can be used with <response
     * name="${returnValue}" .../> to determine what to do next.
     * 
     * @param request
     *            a <code>HttpServletRequest</code> value
     * @param response
     *            a <code>HttpServletResponse</code> value
     * @return The donePage or APPROVE_PAGE parameter if it exists, otherwise
     *         "error"
     */
    public static String approvePageRequestHelper(HttpServletRequest request, HttpServletResponse response) {
        Map < String, Object > parameters = UtilHttp.getParameterMap(request);
        String approvePage = (String) parameters.get("approvePage");
        if (approvePage == null) {
            approvePage = "error";
        }
        request.setAttribute("activeTab", parameters.get("activeTab"));
        request.setAttribute("campaignIdAL", parameters.get("campaignIdAL"));
        request.setAttribute("rmUserLoginIdAL", parameters.get("rmUserLoginIdAL"));
        request.setAttribute("statusIdAL", parameters.get("statusIdAL"));
        String errorPage = (String) parameters.get("errorPage");
        if (errorPage != null && UtilCommon.hasError(request)) {
            Debug.logError("approvePageRequestHelper: goto errorPage [" + errorPage + "]", MODULE);
            return errorPage;
        }
        return approvePage;
    }
    
    public static Map < String, Object > updateRMReassign(DispatchContext dctx, Map < String, Object > context) {
        Delegator delegator = dctx.getDelegator();
        String partyId = (String) context.get("partyId");
        String contactListId = (String) context.get("contactListId");
        String rmPartyId = (String) context.get("rmPartyId");
        String roleTypeIdFrom = (String) context.get("roleTypeIdFrom");
        java.sql.Timestamp nowTimestamp = UtilDateTime.nowTimestamp();
        Map < String, Object > result = ServiceUtil.returnSuccess();
        Locale locale = (Locale) context.get("locale");
        String errMsg = null;
        try {
            String roleTypeIdTo = "";
            String securityGroupId = "";
            // To assaign Role Type to new party
            if (UtilValidate.isNotEmpty(rmPartyId) && UtilValidate.isNotEmpty(contactListId) && UtilValidate.isNotEmpty(partyId)) {
                EntityConditionList < EntityCondition > roleCondition = EntityCondition.makeCondition(UtilMisc.toList(
                        EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, rmPartyId),
                        EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_MANAGER")),
                    EntityOperator.AND);
                List < GenericValue > partyRoleCondition = delegator.findList("PartyRole", roleCondition, null, null, null, false);

                if (partyRoleCondition != null && partyRoleCondition.size() > 0) {
                    roleTypeIdTo = "ACCOUNT_MANAGER";
                    securityGroupId = "ACCOUNT_OWNER";
                } else {
                    result = ServiceUtil.returnError(UtilProperties.getMessage(crmResource, "partyNotFound", locale));
                    result.put("partyId", partyId);
                    return result;
                }

                if (UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(roleTypeIdFrom)) {
                    EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
                        EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
                        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeIdFrom),
                        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
                        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

                    List < GenericValue > responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryList();

                    if (responsibleFor != null && responsibleFor.size() > 0) {
                        for (GenericValue partyRelationship: responsibleFor) {
                            partyRelationship.set("thruDate", nowTimestamp);
                            partyRelationship.store();
                        }
                    }
                    GenericValue partyRelationshipcreate = delegator.makeValue("PartyRelationship");
                    partyRelationshipcreate.set("partyIdFrom", partyId);
                    partyRelationshipcreate.set("partyIdTo", rmPartyId);
                    partyRelationshipcreate.set("roleTypeIdFrom", roleTypeIdFrom);
                    partyRelationshipcreate.set("roleTypeIdTo", roleTypeIdTo);
                    partyRelationshipcreate.set("securityGroupId", securityGroupId);
                    partyRelationshipcreate.set("fromDate", nowTimestamp);
                    partyRelationshipcreate.set("partyRelationshipTypeId", "RESPONSIBLE_FOR");
                    partyRelationshipcreate.create();


                    List < EntityCondition > campaignContactListParty = new ArrayList < EntityCondition > ();
                    campaignContactListParty.add(EntityCondition.makeCondition(EntityOperator.AND,
                        EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
                        EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId),
                        EntityUtil.getFilterByDateExpr()));
                    /*campaignContactListParty.add(EntityCondition.makeCondition(EntityOperator.OR,
                        EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, null),
                        EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, ""),
                        EntityCondition.makeCondition("isApproved", EntityOperator.EQUALS, "N")));*/

                    EntityCondition CampListPartycondition = EntityCondition.makeCondition(campaignContactListParty, EntityOperator.AND);
                    List < GenericValue > campaignContactListPartyList = delegator.findList("CampaignContactListParty", CampListPartycondition, null, null, null, false);
                    if (campaignContactListPartyList != null && campaignContactListPartyList.size() > 0) {
                        for (GenericValue campaignContactListPartyGV: campaignContactListPartyList) {
                            campaignContactListPartyGV.set("csrPartyId", rmPartyId);
                            campaignContactListPartyGV.store();
                        }
                    }
                } else {
                    result = ServiceUtil.returnError(UtilProperties.getMessage(crmResource, "partyNotFound", locale));
                    result.put("partyId", partyId);
                    return result;
                }
            } else {
                result = ServiceUtil.returnError(UtilProperties.getMessage(crmResource, "reassignPartyNotFound", locale));
                result.put("partyId", partyId);
                return result;
            }
        } catch (GenericEntityException e) {
            Debug.logError("Exception in Update Person Responsible For" + e.getMessage(), MODULE);
            result = ServiceUtil.returnError(UtilProperties.getMessage(crmResource, "reassignProcessFailed", locale));
            result.put("partyId", partyId);
            return result;
        }
        result = ServiceUtil.returnSuccess(UtilProperties.getMessage(crmResource, "partySuccessFullyReassign", locale));
        result.put("partyId", partyId);
        return result;
    }
    
    public static String uploadApproveListImport(HttpServletRequest request, HttpServletResponse response) throws ComponentException {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        String filePath = ComponentConfig.getRootLocation("campaign") + "webapp/campaign-resource/importFiles/";
        File store = new File(filePath);
        String marketingCampaignId = request.getParameter("marketingCampaignId");
        Locale locale = UtilHttp.getLocale(request);
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
        fileItemFactory.setRepository(store);
        String csvFile = "";
        CSVReader reader = null;
        int successCount = 0;
        int errorCount = 0;
        int totalCount = 0;
        try {
            String name = "";
            if (ServletFileUpload.isMultipartContent(request)) {
                @SuppressWarnings("unchecked")
                List < FileItem > multiparts = new ServletFileUpload(
                    new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item: multiparts) {
                    if (!item.isFormField()) {
                        name = new File(item.getName()).getName();
                        item.write(new File(filePath + File.separator + name));
                    }
                    if (item.isFormField()) {
                        String fName = item.getFieldName();
                        String fValue = item.getString();
                        if ("marketingCampaignId".equals(fName)) {
                            marketingCampaignId = fValue;
                        }
                    }
                }
                //File uploaded successfully

                if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                    GenericValue marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId", marketingCampaignId), false);
                    if(marketingCampaign == null || marketingCampaign.size() < 1) {
                        Debug.logInfo("Invalid Campaign", MODULE);
                        request.setAttribute("marketingCampaignId", marketingCampaignId);
                        request.setAttribute("_ERROR_MESSAGE_", "Invalid Campaign");
                        return "error";
                    }
                    String contactListId = marketingCampaignId + "_PROD_LIST";
                    List<GenericValue> campaignContactListParty = delegator.findByAnd("CampaignContactListParty", UtilMisc.toMap("contactListId", contactListId), null, false);
                    if(campaignContactListParty == null || campaignContactListParty.size() < 1) {
                        Debug.logInfo("Campaign list not available", MODULE);
                        request.setAttribute("marketingCampaignId", marketingCampaignId);
                        request.setAttribute("_ERROR_MESSAGE_", "Campaign list not available");
                        return "error";
                    }
                    
                    Debug.logInfo("==============" + "File Uploaded Successfully, Request Id==" + marketingCampaignId, MODULE);
                    csvFile = filePath + name;
                    reader = new CSVReader(new FileReader(csvFile), ',', '"', 1);
                    List < String[] > records = reader.readAll();
                    if (records != null && records.size() > 0) {
                        totalCount = records.size();

                        GenericValue errorProcessInsert = delegator.makeValue("ApproveListImportProcess");
                        String processId = delegator.getNextSeqId("ApproveListImportProcess");
                        errorProcessInsert.put("processId", processId);
                        errorProcessInsert.put("campaignId", marketingCampaignId);
                        errorProcessInsert.put("totalCount", totalCount + "");
                        errorProcessInsert.create();
                        
                        Debug.logInfo("Contact List Id: " + contactListId, MODULE);
                        for (String[] record: records) {
                            if (record.length > 0) {
                                String accountId = record[0];
                                if (UtilValidate.isNotEmpty(accountId)) {
                                    accountId = accountId.trim();
                                }
                                String partyId = record[1];
                                if (UtilValidate.isNotEmpty(partyId)) {
                                    partyId = partyId.trim();
                                }
                                String isApproved = record[2];
                                if (UtilValidate.isNotEmpty(isApproved)) {
                                    isApproved = isApproved.trim();
                                }
                                String contactListType = record[3];
                                if (UtilValidate.isNotEmpty(contactListType)) {
                                	contactListType = contactListType.trim();
                                }

                                if (UtilValidate.isNotEmpty(accountId)) {
                                    List < EntityCondition > accountCondition = new LinkedList < EntityCondition > ();
                                    accountCondition.add(EntityCondition.makeCondition(EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null),
                                        EntityOperator.OR, EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED")));

                                    accountCondition.add(EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"),
                                        EntityOperator.AND, EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, accountId)));
                                    accountCondition.add(EntityUtil.getFilterByDateExpr());
                                    long account = delegator.findCountByCondition("PartyFromByRelnAndContactInfoAndPartyClassification", EntityCondition.makeCondition(accountCondition, EntityOperator.AND), null, null);
                                    if (account < 1) {
                                        GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                        errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                        errorInsert.put("processId", processId);
                                        errorInsert.put("campaignId", marketingCampaignId);
                                        errorInsert.put("errorDescription", "LCIN is invalid");
                                        errorInsert.set("acctId", accountId);
                                        errorInsert.set("partyId", partyId);
                                        errorInsert.create();
                                        errorCount++;
                                        continue;
                                    }
                                } else {
                                    GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                    errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                    errorInsert.put("processId", processId);
                                    errorInsert.put("campaignId", marketingCampaignId);
                                    errorInsert.put("errorDescription", "LCIN is empty");
                                    errorInsert.set("acctId", accountId);
                                    errorInsert.set("partyId", partyId);
                                    errorInsert.create();
                                    errorCount++;
                                    continue;
                                }

                                if (UtilValidate.isNotEmpty(partyId)) {
                                    List < EntityCondition > contactCondition = new LinkedList < EntityCondition > ();
                                    contactCondition.add(EntityCondition.makeCondition(EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null),
                                        EntityOperator.OR, EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED")));

                                    contactCondition.add(EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "CONTACT"),
                                        EntityOperator.AND, EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)));
                                    contactCondition.add(EntityUtil.getFilterByDateExpr());
                                    long account = delegator.findCountByCondition("PartyFromByRelnAndContactInfoAndPartyClassification", EntityCondition.makeCondition(contactCondition, EntityOperator.AND), null, null);
                                    if (account < 1) {
                                        GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                        errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                        errorInsert.put("processId", processId);
                                        errorInsert.put("campaignId", marketingCampaignId);
                                        errorInsert.put("errorDescription", "Contact ID is invalid");
                                        errorInsert.set("acctId", accountId);
                                        errorInsert.set("partyId", partyId);
                                        errorInsert.create();
                                        errorCount++;
                                        continue;
                                    }
                                } else {
                                    GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                    errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                    errorInsert.put("processId", processId);
                                    errorInsert.put("campaignId", marketingCampaignId);
                                    errorInsert.put("errorDescription", "Contact ID is empty");
                                    errorInsert.set("acctId", accountId);
                                    errorInsert.set("partyId", partyId);
                                    errorInsert.create();
                                    errorCount++;
                                    continue;
                                }

                                if ((UtilValidate.isNotEmpty(contactListType) && (!"LIVE".equals(contactListType) && !"TEST".equals(contactListType) )) || UtilValidate.isEmpty(contactListType)) {
                                        GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                        errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                        errorInsert.put("processId", processId);
                                        errorInsert.put("campaignId", marketingCampaignId);
                                        if (UtilValidate.isNotEmpty(contactListType) && (!"LIVE".equals(contactListType) && !"TEST".equals(contactListType))) {
                                            errorInsert.put("errorDescription", "Invalid Data for the Contact List Type");
                                        } else {
                                            errorInsert.put("errorDescription", "Contact List Type is empty");
                                        }
                                        errorInsert.set("acctId", accountId);
                                        errorInsert.set("partyId", partyId);
                                        errorInsert.create();
                                        errorCount++;
                                        continue;
                                }
                                
                             // if ((UtilValidate.isNotEmpty(isApproved) && (!"Y".equals(isApproved) && !"N".equals(isApproved) && !"X".equals(isApproved))) || UtilValidate.isEmpty(isApproved)) {
                                if ((UtilValidate.isNotEmpty(isApproved) && (!"Approved".equals(isApproved) &&  !"Not Approved".equals(isApproved))) || UtilValidate.isEmpty(isApproved)) {
                                GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                    errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                    errorInsert.put("processId", processId);
                                    errorInsert.put("campaignId", marketingCampaignId);
                                    //if (UtilValidate.isNotEmpty(isApproved) && (!"Y".equals(isApproved) && !"N".equals(isApproved) && !"X".equals(isApproved))) {
                                    if (UtilValidate.isNotEmpty(isApproved) && (!"Approved".equals(isApproved) && !"Not Approved".equals(isApproved))) {
                                        errorInsert.put("errorDescription", "Invalid Data for the Approve Flag");
                                    } else {
                                        errorInsert.put("errorDescription", "Approval Flag is empty");
                                    }
                                    errorInsert.set("acctId", accountId);
                                    errorInsert.set("partyId", partyId);
                                    errorInsert.create();
                                    errorCount++;
                                    continue;
                               }
                                
                            	if("Approved".equals(isApproved)){
                            		isApproved = "Y";
                            	}else if("Not Approved".equals(isApproved)){
                            		isApproved = "X";
                            	}
                                List < GenericValue > campListRecord = EntityQuery.use(delegator).from("CampaignContactListParty").where("contactListId", contactListId,
                                    "partyId", partyId, "acctPartyId", accountId, "contactPurposeTypeId", contactListType).queryList();
                                if (campListRecord != null && campListRecord.size() > 0) {
                                    GenericValue campListRecordVal = (GenericValue) campListRecord.get(0);
                                    campListRecordVal.set("isApproved", isApproved);
                                    campListRecordVal.store();
                                    successCount++;
                                } else {
                                    GenericValue errorInsert = delegator.makeValue("ApproveListImportLog");
                                    errorInsert.put("logId", delegator.getNextSeqId("ApproveListImportLog"));
                                    errorInsert.put("processId", processId);
                                    errorInsert.put("campaignId", marketingCampaignId);
                                    errorInsert.put("errorDescription", "LCIN and Contact relationship is not matching");
                                    errorInsert.set("acctId", accountId);
                                    errorInsert.set("partyId", partyId);
                                    errorInsert.create();
                                    errorCount++;
                                    continue;
                                }
                                Debug.logInfo(accountId + "_" + partyId + "_" + isApproved, MODULE);
                            }
                        }
                        GenericValue updateProcessCount = EntityQuery.use(delegator).from("ApproveListImportProcess").where("processId", processId,
                            "campaignId", marketingCampaignId).queryFirst();
                        if (updateProcessCount != null && updateProcessCount.size() > 0) {
                            updateProcessCount.set("successCount", successCount + "");
                            updateProcessCount.set("errorCount", errorCount + "");
                            updateProcessCount.store();
                        }
                        int approvedSize = 0;
                        if (UtilValidate.isNotEmpty(marketingCampaignId)) {
                            List < GenericValue > contactListPartyApproved = EntityQuery.use(delegator).from("CampaignContactListParty")
                                .where("isApproved", "Y", "contactListId", contactListId, "contactPurposeTypeId", "LIVE").queryList();
                            if (contactListPartyApproved != null && contactListPartyApproved.size() > 0) {
                                approvedSize = contactListPartyApproved.size();
                            }
                            String approvedSizeString = String.valueOf(approvedSize);
                            GenericValue campCLSummary = delegator.makeValue("CampaignContactListSummary", UtilMisc.toMap("contactListId", contactListId,
                                "campaignId", marketingCampaignId,
                                "attrName", "RM_APPROVED",
                                "attrValue", approvedSizeString));
                            delegator.createOrStore(campCLSummary);
                        }
                    } else {
                        Debug.log("No data available in the file");
                        request.setAttribute("marketingCampaignId", marketingCampaignId);
                        request.setAttribute("_ERROR_MESSAGE_", "No data available in the file");
                        return "error";
                    }
                }
            }
        } catch (Exception e1) {

            Debug.log("Exception in Upload Approve List Service" + e1.getMessage());
            request.setAttribute("marketingCampaignId", marketingCampaignId);
            request.setAttribute("_ERROR_MESSAGE_", e1.getMessage());
            return "error";
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                Debug.log("Exception in csv reader and bufferreader closing in Upload Approve List Service" + e.getMessage());
            }
        }
        String returnMsg = "Approval list imported successfully";
        if (successCount < 1) {
            returnMsg = "Approval list import failed, due to invalid data";
        }
        request.setAttribute("marketingCampaignId", marketingCampaignId);
        request.setAttribute("_EVENT_MESSAGE_", returnMsg);
        return "success";
    }
}
