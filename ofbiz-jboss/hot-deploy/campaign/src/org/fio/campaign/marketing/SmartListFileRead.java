package org.fio.campaign.marketing;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javolution.util.FastList;

import org.apache.commons.io.FileUtils;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;
import org.fio.campaign.job.SmartListFileReadJob;


public class SmartListFileRead {

	public static Map<String, Object> processSmartListFile(DispatchContext dctx, Map<String, Object> context){
		Delegator delegator=dctx.getDelegator();
		LocalDispatcher dispatcher=dctx.getDispatcher();
		String contactListId=(String)context.get("contactListId");
		String contactMechType=(String)context.get("contactMechType");
		String roleTypeId=(String)context.get("roleTypeId");
		Map result = ServiceUtil.returnSuccess("Contacts imported Successfully into the list");
		try {
			String emailType = "";
			GenericValue contactList = EntityQuery.use(delegator).from("ContactList").where("contactListId",contactListId).queryOne();
			if(contactList != null && contactList.size() > 0) {
				emailType = contactList.getString("emailType");
			}
			List conditionList = FastList.newInstance();
			if(UtilValidate.isNotEmpty(contactListId))
				conditionList.add(EntityCondition.makeCondition("contactListId", EntityOperator.EQUALS, contactListId));
			conditionList.add(EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "UPLOAD_FILE_STATUS"));
			conditionList.add(EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, "CREATED"));

			EntityFindOptions findOptions = new EntityFindOptions();
			findOptions.setDistinct(true);	

			List<GenericValue> contactListAttribute = delegator.findList("ContactListAttribute",EntityCondition.makeCondition(conditionList, EntityOperator.AND), UtilMisc.toSet("contactListId"), UtilMisc.toList("contactListId"), findOptions, false);

			if(UtilValidate.isEmpty(contactListAttribute)){
				return ServiceUtil.returnError("File is not uploaded or File is processed");
			}
			String dirPath = null;
			//String dirPath =  TenantEnabledUtilProperties.getPropertyValue(delegator,"smartlist", "filepath");
			GenericValue systemProperty = EntityQuery.use(delegator).from("SystemProperty").where("systemResourceId", "smartlist","systemPropertyId","filepath").queryOne();
			if(UtilValidate.isNotEmpty(systemProperty)){
				dirPath = systemProperty.getString("systemPropertyValue");
			}
			if(UtilValidate.isEmpty(dirPath)){
				return ServiceUtil.returnError("Uploaded Filepath is not configured");
			}

			contactListId = contactListAttribute.get(0).getString("contactListId");

			//GenericValue contactListAttributeGv = delegator.findByPrimaryKey("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_NAME"));
			GenericValue contactListAttributeGv = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","UPLOAD_FILE_NAME").queryOne();

			if(UtilValidate.isEmpty(contactListAttributeGv) || UtilValidate.isEmpty(contactListAttributeGv.getString("attrValue"))){
				return ServiceUtil.returnError("File not uploaded");
			}

			//GenericValue changeStaus = delegator.findByPrimaryKey("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_STATUS"));
			GenericValue changeStaus = EntityQuery.use(delegator).from("ContactListAttribute").where("contactListId", contactListId,"attrName","UPLOAD_FILE_STATUS").queryOne();

			if(UtilValidate.isEmpty(changeStaus)){// || UtilValidate.isEmpty(contactListAttributeGv.getString("attrValue"))){
				changeStaus = delegator.makeValue("ContactListAttribute",UtilMisc.toMap("contactListId",contactListId,"attrName","UPLOAD_FILE_STATUS"));
				changeStaus.set("attrValue","PROCESSING");
				changeStaus.create();
			}else{
				changeStaus.set("attrValue","PROCESSING");
				changeStaus.store();
			}
			String ofbizHome = System.getProperty("ofbiz.home");
			String  dirPaths = ofbizHome + dirPath;
			Debug.log("Execute Job");
			SmartListFileReadJob job = new SmartListFileReadJob();
			job.setContactListId(contactListId);
			job.setRoleTypeId(roleTypeId);
			job.setContactMechType(contactMechType);
			job.setDelegator(delegator);
			job.setDispatcher(dispatcher);
			job.setDirPath(dirPaths);
			job.setEmailType(emailType);
			job.start();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
}
