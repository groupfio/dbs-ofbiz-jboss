<#macro includeContent id>
    <#if context??>
        <#assign empty>${context.put("contentId", id)!}</#assign>
        ${screens.render("component://tooth_to_tail/widget/Tooth2TailScreens.xml#include-content")}
    </#if>
</#macro>
<#macro portalButton label url="javascript:">
    <a href="${url}" class="portal-button"><span><span><span>${label}</span></span></span></a>
</#macro>

<#macro hiddenSubmit>
    <input type="submit" tabindex="-1" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>
</#macro>

<#function getBaseUrl https=false>
    <#return Static["org.ofbiz.webapp.control.RequestHandler"].getDefaultServerRootUrl(request, https)>
</#function>

<#function createUrl url>
    <#return response.encodeUrl(url)>
</#function>

<#macro fullUrlPath url>
<#compress>
    <#assign baseUrl=getBaseUrl()>
    <#if url?starts_with("http")>
        ${url}
    <#elseif StringUtil.wrapString(url)?starts_with("/")>
        ${baseUrl}${url}
    <#else>
        ${baseUrl!}/${url}
    </#if>
</#compress>
</#macro>

<#macro addBottomHtmlContent>
<#compress>
<#assign content><#nested></#assign>
<#assign null=page.put("bottomHtmlContent", StringUtil.wrapString(page.bottomHtmlContent!"") + content)!>
</#compress>
</#macro>

<#macro getBottomHtmlContent>
${StringUtil.wrapString(page.bottomHtmlContent!"")}
</#macro>

<#macro contextMenuLayout content left_menu top_box="" breadcrumbs="" bottom_content="">
    <#if (breadcrumbs?trim?length > 0)>
        <div id="breadcrumbs">
            ${breadcrumbs}
        </div>
    </#if>
    <div id="context_menu_layout">
        <div id="context_menu">
            ${left_menu}
        </div>
        <div id="context_menu_content">
            <#if (top_box?trim?length > 0)>
                <div id="wrap-v2-top-content">
                    <div id="top-content">
                        ${top_box}
                    </div>
                </div>
            </#if>
            ${content}
        </div>
    </div>
    ${bottom_content}
</#macro>

<#-- This macro is to be called from a content item -->
<#macro displayProductList product_list top_box="" breadcrumbs="">
    <#assign left_menu>
        ${thisContent.subcontent.left_menu!""}
    </#assign>
    <#assign content>
        <div id="display_products">
            ${product_list}
        </div>
    </#assign>
    <#assign bottom_content>
        <div id="product_pagination"></div>
    </#assign>
    
    <@contextMenuLayout content=content left_menu=left_menu top_box=top_box breadcrumbs=breadcrumbs bottom_content=bottom_content/>
</#macro>

<#-- This macro is to be called from a content item -->
<#macro displayProductItem content related_products="" breadcrumbs="">
    <#assign left_menu>
        ${thisContent.subcontent.left_menu!""}
    </#assign>
    <#assign wrapped_content>
        <div id="product_item_info">
            ${content}
        </div>
    </#assign>
    <#assign bottom_content>
        <#if (related_products?trim?length > 0)>
            <h3 class="related-products">You May Also Like:</h3>
            <div id="display_products" class="four-column">${related_products}</div>
        </#if>
    </#assign>
    
    <@contextMenuLayout content=wrapped_content left_menu=left_menu breadcrumbs=breadcrumbs bottom_content=bottom_content/>
</#macro>

<#-- This macro is to be called from a content item -->
<#macro productDetails productId size="" price="" image="" name="" largeImage="" popupText="" productPage="" type="" addToCart=true>
    <#assign uuid=Static["java.util.UUID"].randomUUID()>
    <#assign product = delegator.findOne("Product", {"productId" : productId}, false)!>

    <#assign shortTextContent><#nested></#assign>
    <#assign nameContent=name>
    <#assign priceContent=price>
    <#assign imageContent=image>
    <#assign largeImageContent = largeImage>
    <#assign popupTextContent = popupText>
    
    <#if shortTextContent == "" && product?has_content>
        <#assign shortTextContent = product.description!>
    </#if>
    <#if nameContent == "" && product?has_content>
        <#assign nameContent = product.productName!>
    </#if>
    <#if priceContent == "" && product?has_content>
        <#assign productStoreGroupId = Static["com.openbox.wholesaler.controller.OrderTypeCheckController"].getProductStoreGroupId(delegator, (userLogin.partyId)!(parameters.ecomAssociatedPartyId)!)!>
        
        <#assign priceContext = dispatcher.runSync("calculateProductPrice", {
                "product" : product,
                "webSiteId" : "OpenBox-ERP",
                "prodCatalogId" : "PS_CATALOG",
                "productStoreId" : ((Static["org.ofbiz.product.store.ProductStoreWorker"].getProductStoreId(request))!"TTT_1001"),
                "productStoreGroupId" : (productStoreGroupId!"PS_GROUP"),
                "quantity" : 1,
                "currencyUomId" : "USD",
                "customAttributes" : {"srpOnly" : "true" != (request.getSession().getAttribute("isGenericLogin")!"false")} 
            })>
        <#assign priceContent><@ofbizCurrency amount=priceContext.price isoCode="USD"/></#assign>
    </#if>
    <#if imageContent == "" && product?has_content>
        <#assign imageContent = product.detailImageUrl!>
    </#if>
    
    <#if largeImageContent == "" && product?has_content>
        <#assign largeImageContent = product.largeImageUrl!>
    <#elseif largeImageContent?trim == "">
        <#assign largeImageContent = image>
    </#if>
    
    <#if popupTextContent == "" && product?has_content>
        <#assign popupTextContent = product.longDescription!>
    <#elseif popupTextContent?trim == "">
        <#assign popupTextContent><#nested></#assign>
    </#if>

    <div class="product">
        <div class="preview">
            <a href="javascript:" data-for="${uuid}">
                <img src="${imageContent}" alt="${nameContent}"/>
                <div class="quick-look">
                    <span>QUICK LOOK</span>
                </div>
            </a>
        </div>
        <h2>${type?upper_case}</h2>
        <h3>
            <#if productPage != "">
                <a href="${createUrl("/control/product?name=${productPage}")}">
                    ${nameContent}
                </a>
            <#else>
                ${nameContent}
            </#if>
        </h3>
        <p>
            ${shortTextContent}
        </p>
        <div class="size-and-price">
            <div class="size">${size}</div>
            <div class="price">${priceContent}</div>
        </div>
        <div class="product-bottom">
            <div class="add-to-cart">
                <#if addToCart>
                    <a class="action-button small full-width" href="javascript:" onclick="Perio.addToCart('${productId}', 1, this);">ADD TO CART</a>
                </#if>
            </div>
            <#if productPage != "">
                <div class="learn-more">
                    <a href="${createUrl("/control/product?name=${productPage}")}">learn More &raquo;</a>
                </div>
            </#if>
        </div>
    </div>
    <@addBottomHtmlContent>
        <div style="display:none;" class="product-preview-pane" id="${uuid}">
            <div class="product-preview-pane-container">
                <div class="image-side">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td valign="middle" align="center">
                                <img src="${largeImageContent}"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-side">
                    <div class="heading">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="col1">
                                    <h3>${nameContent}</h3>
                                </td>
                                <td class="col2">
                                    <div class="size">${size}</div>
                                </td>
                                <td class="col3">
                                    <div class="type">${type?lower_case?capitalize}</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="description">
                        <p>${popupTextContent}</p>
                    </div>
                    <#if addToCart>
                    <form action="javascript:" onsubmit="Perio.addToCart('${productId}', this.elements.quantity.value, jQuery(this).find('.action-button'));">
                        <div class="specify-quantity">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr class="quantity">
                                    <td class="label">Quantity</td>
                                    <td class="select-container">
                                        <select name="quantity">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="price">
                                    <td class="label">Price</td>
                                    <td class="amount">${priceContent}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="actions">
                            <div class="links">
                                <#if productPage != "">
                                    <a href="${createUrl("/control/product?name=${productPage}")}">View full Product Details &raquo;</a>
                                </#if>
                                <a href="javascript:jQuery('#${uuid}').dialog('close');">Continue Shopping &raquo;</a>
                            </div>
                            <div class="add-to-cart">
                                <a class="action-button" href="javascript:" onclick="jQuery(this).closest('form').submit();">ADD TO CART</a>
                            </div>
                        </div>
                    </form>
                    <#else>
                        <div class="actions">
                            <div class="links">
                                <#if productPage != "">
                                    <a href="${createUrl("/control/product?name=${productPage}")}">View full Product Details &raquo;</a>
                                </#if>
                                <a href="javascript:jQuery('#${uuid}').dialog('close');">Continue Shopping &raquo;</a>
                            </div>
                        </div>
                    </#if>
                </div>
            </div>
        </div>
    </@addBottomHtmlContent>
</#macro>

<#-- This macro is to be called from a content item -->
<#macro productDisplay productId name="" price="" image="" type="" size="" itemNo="" addToCart=true>
    <#assign product = delegator.findOne("Product", {"productId" : productId}, false)!>

    <#assign textContent><#nested></#assign>
    <#assign nameContent=name>
    <#assign priceContent=price>
    <#assign imageContent=image>
    <#assign itemNoContent = itemNo>
    
    <#if textContent == "" && product?has_content>
        <#assign textContent = product.longDescription!>
    </#if>
    <#if nameContent == "" && product?has_content>
        <#assign nameContent = product.productName!>
    </#if>
    <#if priceContent == "" && product?has_content>
        <#assign productStoreGroupId = Static["com.openbox.wholesaler.controller.OrderTypeCheckController"].getProductStoreGroupId(delegator, (userLogin.partyId)!(parameters.ecomAssociatedPartyId)!)!>
        
        <#assign priceContext = dispatcher.runSync("calculateProductPrice", {
                "product" : product,
                "webSiteId" : "OpenBox-ERP",
                "prodCatalogId" : "PS_CATALOG",
                "productStoreId" : ((Static["org.ofbiz.product.store.ProductStoreWorker"].getProductStoreId(request))!"TTT_1001"),
                "productStoreGroupId" : (productStoreGroupId!"PS_GROUP"),
                "quantity" : 1,
                "currencyUomId" : "USD",
                "customAttributes" : {"srpOnly" : "true" != (request.getSession().getAttribute("isGenericLogin")!"false") }
            })>
        <#assign priceContent><@ofbizCurrency amount=priceContext.price isoCode="USD"/></#assign>
    </#if>
    <#if imageContent == "" && product?has_content>
        <#assign imageContent = product.largeImageUrl!>
    </#if>
    <#if itemNoContent == "" && product?has_content>
        <#assign itemNoContent = product.internalName!>
    </#if>
    <div class="product-display">
        <div class="product-image">
            <img src="${imageContent}"/>
        </div>
        <div class="product-header-details">
            <div class="item-no">Item #${itemNoContent}</div>
            <h1>${nameContent}</h1>
            <div class="type">
                <b>${type?upper_case}</b>
            </div>
            <div class="description">
                ${textContent}
            </div>
            <div class="order-form">
                <#if addToCart>
                <form action="javascript:" onsubmit="Perio.addToCart('${productId}', this.elements.quantity.value, jQuery(this).find('.action-button'));">
                    <div class="quantity-info">
                        <div class="size">
                            ${size}
                        </div>
                        <div class="quantity">
                            QTY
                            <select name="quantity">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div class="price">
                            ${priceContent}
                        </div>
                    </div>
                    <div class="add-to-cart">
                        <a class="action-button small" href="javascript:" onclick="jQuery(this).closest('form').submit();">ADD TO CART</a>
                    </div>
                </form>
                </#if>
            </div>
            <div class="social-media">
                <div class="facebook-share">
                    <div class="fb-like" data-send="true" data-width="285" data-show-faces="false"></div>
                </div>
                <div class="twitter-share">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-text="${uiLabelMap.get(pageTitleLabel)}" data-via="PerioSciences">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>
</#macro>

<#-- This macro is to be called from a content item -->
<#macro ingredientList type description="">
    <div class="ingredient-list">
        <div class="title">
            <h3>${type}</h3>
            <div class="description">
                ${description}
            </div>
        </div>
        <#nested>
    </div>
</#macro>

