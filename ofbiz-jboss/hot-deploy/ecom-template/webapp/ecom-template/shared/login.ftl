<@import location="component://tooth_to_tail/templates/lib/portalContentMacros.ftl" />
<div class="welcome">
	<a id="welcome_message">
		Welcome!
	</a>
</div>
<div class="login-action">
	<#if userLogin?exists>
		${userLogin.userLoginId}
	<#else>
		LogIn
	</#if>
	<img src="<@ofbizContentUrl>white_expand_arrow.png</@ofbizContentUrl>" width="7" height="4" />
</div>

<@addBottomHtmlContent>
<#if !userLogin?exists>
	<div style="display:none;">
		<div id="forgot_username">
			<h3>In most cases, your Username is the same as your e-mail address.  However, if you are an exception, please enter your e-mail address and we will send you your Username.</h3>
			<div class="action-form">
				<form method="POST" action="javascript:">
					<label>E-mail Address on Your Account</label>
					<input name="emailAddress" type="text"/>
					<@hiddenSubmit/>
					<div class="action-bar">
						<a class="action-button submit" href="javascript:">SEND USERNAME</a>
					</div>
				</form>
			</div>
		</div>
		<div id="forgot_password">
			<h3>Please enter your username and we will reset your password.</h3>
			<div class="action-form">
				<form method="POST" action="javascript:">
					<label>Your Account's Username</label>
					<input name="USERNAME" type="text"/>
					<@hiddenSubmit/>
					<div class="action-bar">
						<a class="action-button submit" href="javascript:">SEND PASSWORD</a>
					</div>
				</form>
			</div>
		</div>
		<div id="create_account_popup">
			<div class="error-box" style="display:none;">
				<h3>Oops! Looks like we are missing some information.</h3>
				<ul>
				</ul>
			</div>
			<div class="info-box">
				<div class="info">
					<h3>Access to discounts and promotions</h3>
					<p>Be the first to know about special products and promotions.</p>
				</div>
				<div class="info">
					<h3>News &amp; Offers</h3>
					<p>Be the first to find out when we develop new products.</p>
				</div>
				<div class="info">
					<h3>More Convenient Shopping</h3>
					<p>saved address book, order history and track orders</p>
				</div>
			</div>
			<div class="login-information">
				<form method="POST" action="javascript:">
					<input type="hidden" name="USE_ADDRESS" value="false"/>
					<input type="hidden" name="emailProductStoreId" value="TTT_1001" />
					<input type="hidden" name="USER_FIRST_NAME" value="Unknown" />
					<input type="hidden" name="USER_LAST_NAME" value="Unknown" />
					<input type="hidden" id="create_account_email_hidden" name="USERNAME" value="" />
					<div class="create-account-form">
						<div class="left-side">
							<h2>LogIn Information</h2>
							<label for="create_account_email">E-mail*</label>
							<div class="input-box">
								<input id="create_account_email" name="CUSTOMER_EMAIL" type="text"/>
							</div>
							<label for="create_account_password">Password*</label>
							<div class="input-box">
								<input id="create_account_password" name="PASSWORD" type="password"/>
							</div>
						</div>
						<div class="right-side">
							<div class="required-info">*Required Fields</div>
							<label for="create_account_password2">Confirm Password*</label>
							<div class="input-box">
								<input id="create_account_password2" name="CONFIRM_PASSWORD" type="password"/>
							</div>
							<div class="input-info-box">
								Password must contain at least 6 characters,
								at least one number and one letter
							</div>
						</div>
					</div>
					<div class="action-bar">
						<input type="submit" class="submit-button" value="CREATE ACCOUNT">
					</div>
				</form>
			</div>
		</div>
		<div id="success_message_box">
			<div class="message-area">
				<h3>We have just sent your temporary password to</h3>
				email address
				We have just sent your username to
				email address
			</div>
			<div class="action-bar">
				<a class="action-button submit" href="javascript:">CLOSE</a>
			</div>
		</div>
	</div>

	<script>
	(function($) {
		var url = '/ajaxCheckLogin';
		$.ajax({
			url : url,
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				if (data.login) {
					var url = window.location.href;
					if (url.match(/\?/)) {
						url += '&';
					} else {
						url += '?';
					}
					
					url += data.loginKey;
				
					window.location = url;
				}
			}
		});
	})(jQuery);
	
	
	jQuery(function($) {
		var $dropdown = null;
		var dropdownShown = false;
		var submittingLogin = false;
	
		var $loginButton = $('#top_header_v2 .login').click(function() {
			//if (!window.location.protocol.match(/^https/)) {
			//	window.location = '${StringUtil.wrapString(getBaseUrl(true))}' + window.location.pathname + window.location.search + '#login';
			//}
			showDropdown();
		});
		
		if (window.location.hash == '#login') {
			showDropdown();
		}
		
		window.forgotPasswordPopup = forgotPasswordPopup;
		
		return;
		
		function createDropdown() {
			if ($dropdown) return;
			
			(function() {
				var $currentNode;
				$dropdown = $('<div id="signup_dropdown" style="display:none;"></div>');
				$dropdown.append('<div class="top-bar"><div class="action">LogIn <img src="<@ofbizContentUrl>/images/gray_expand_arrow.png</@ofbizContentUrl>" width="7" height="4" /></div></div>');
				
				$currentNode = $('<div class="wrap-content"></div>').appendTo($dropdown);
				$currentNode.append('<div class="welcome"><strong>Welcome!</strong> Sign in using your username and password.</div>');
				
				$currentNode = $('<div class="signup-form"></div>').appendTo($currentNode);
				$currentNode = $('<form method="post" action="javascript:" id="loginForm"></form>').submit(submitAction).appendTo($currentNode);
				$currentNode.append('<div class="messages"></div>');
				$currentNode.append('<label for="email_signup">Username</label>');
				$currentNode.append('<input id="email_signup" name="USERNAME" type="text"/>');
				$currentNode.append('<label for="password_signup">Password</label>');
				$currentNode.append('<input id="password_signup" name="PASSWORD" type="password"/>');
				$currentNode = $('<div class="help-links"></div>').appendTo($currentNode);
				$('<a href="javascript:">Forgot Username?</a>').click(forgotUsername).appendTo($currentNode);
				$('<a href="javascript:">Forgot Password?</a>').click(forgotPassword).appendTo($currentNode);
				$currentNode = $currentNode.parent();
				$currentNode = $('<div class="submit-bar"></div>').appendTo($currentNode);
				$('<a class="action-button" href="javascript:">LOGIN</a>').click(function() {$("#loginForm").submit();}).appendTo($currentNode);
				$('<input type="submit" value="LOGIN"/>').appendTo($currentNode);
				$currentNode = $currentNode.parent();
				$currentNode = $currentNode.parent();
				$currentNode = $currentNode.parent();
				
				$currentNode = $('<div class="create-account-area"></div>').appendTo($currentNode);
				$currentNode.append('<strong>Don\'t have an account?</strong>');
				$currentNode.append(
					$('<a href="javascript:">create an account now</a>')
					.click(createAccountPopup)
				);
				$currentNode = $currentNode.parent();
				
				$currentNode = $('<div class="more-info-area"></div>').appendTo($currentNode);
				$currentNode.append('Create a <b>free</b> account to access discounts, promotions and to experience a higher level of customer service.');
			})();
			
			$dropdown.find('.top-bar').click(hideDropdown);
			
					
			$('body').append($dropdown).click(function(e) {
				if (dropdownShown && !$.contains($dropdown[0], e.target) && !$.contains($loginButton[0], e.target) && $dropdown[0] != e.target && $loginButton[0] != e.target) {
					hideDropdown();
				}
			});
			$(window).resize(function() {
				hideDropdown();
			});
		}
		
		function hideDropdown() {
			$dropdown.hide();
			dropdownShown = false;
		}
		
		function showDropdown() {
			createDropdown();
			$dropdown.show();
	
			var loginOffset = $loginButton.find('.login-action').offset();
			var loginWidth = $loginButton.find('.login-action').outerWidth();
			var topBarHeight =  $('#top_header_v2').height() - loginOffset.top;
			$dropdown.css({top : loginOffset.top, left : loginOffset.left + loginWidth + 13 - $dropdown.outerWidth()});
			$dropdown.find('.top-bar').css('height', topBarHeight);
			$dropdown.find('.top-bar .action').css('height', topBarHeight - 4);
			$dropdown.find('#email_signup').focus();
	
			dropdownShown = true;
		}
		
		function forgotPasswordPopup(username) {
			createDropdown();
			if (username) {
				$('#email_signup').val(username)
			}
			forgotPassword();
		}
		
		function forgotPassword() {
			var $content = $('#forgot_password');
			
			Perio.openSimplePopup($content, {
				initialize : function($dialog) {
					$dialog.find('a.submit').click(function() {$(this).closest('form').submit();});
					$dialog.find('form').submit(function() {
						$dialog.dialog('close');
						loadMessageBox('Submitting your username...', 'loading');
						$.ajax({
							url : '/ajaxForgotPassword',
							data : $(this).serialize(),
							type : 'POST',
							dataType : 'json',
							success : function(data) {
								if (data.responseMessage == 'success') {
									loadMessageBox($dialog.find('input[type="text"]').val(), 'user-input', 'We have just sent your temporary password to');
								} else {
									loadMessageBox(data.errorMessage, 'error', undefined, forgotPassword);
								}
							},
							error: function(xhr, textStatus, error) {
								var message;
								switch (textStatus) {
									case 'parsererror' :
										message = 'Invalid response from the server.  Please refresh this page and try again.';
										break;
									case 'timeout' :
									case 'error' :
									case 'abort' :
									default:
										message = 'Could not connect to server to reset your password.  Please refresh this page and try again.';
										break;
								}
								loadMessageBox(message, 'error', undefined, forgotPassword);
							}
						});
					});
				},
				onOpen : function($dialog) {
					$dialog.find('input[type="text"]').val($('#email_signup').val())
				}
			});
		}
		
		function forgotUsername() {
			var $content = $('#forgot_username');
			
			Perio.openSimplePopup($content, {
				initialize : function($dialog) {
					$dialog.find('a.submit').click(function() {$(this).closest('form').submit();});
					$dialog.find('form').submit(function() {
						$dialog.dialog('close');
						loadMessageBox('Submitting your e-mail address...', 'loading');
						$.ajax({
							url : '/ajaxForgotUsername',
							data : $(this).serialize(),
							type : 'POST',
							dataType : 'json',
							success : function(data) {
								if (data.responseMessage == 'success') {
									loadMessageBox($dialog.find('input[type="text"]').val(), 'user-input', 'We have just sent your username to');
								} else {
									loadMessageBox(data.errorMessage, 'error', undefined, forgotUsername);
								}
							},
							error: function(xhr, textStatus, error) {
								var message;
								switch (textStatus) {
									case 'parsererror' :
										message = 'Invalid response from the server.  Please refresh this page and try again.';
										break;
									case 'timeout' :
									case 'error' :
									case 'abort' :
									default:
										message = 'Could not connect to server to find your username.  Please refresh this page and try again.';
										break;
								}
								loadMessageBox(message, 'error', undefined, forgotUsername);
							}
						});
					});
				},
				onOpen : function($dialog) {
					$dialog.find('input[type="text"]').val('');
				}
			});
		}

		function createAccountPopup() {
			var $content = $('#create_account_popup');
			
			Perio.openSimplePopup($content, {
				title : '<h2>Create an Account</h2>',
				width: 600,
				initialize : function($dialog) {
					$dialog.find('form').submit(function() {
				    	$('#create_account_email_hidden').val($('#create_account_email').val());
						accountPopupClearErrors($dialog);
						if (validateAccountPopup($dialog)) {
							$dialog.dialog('close');
							loadMessageBox('Creating a user account...', 'loading');
							$.ajax({
								url : '/createAccountAjax',
								data : $(this).serialize(),
								type : 'POST',
								dataType : 'json',
								success : function(data) {
									if (data.responseMessage == 'success') {
										loadMessageBox($dialog.find('input[type="text"]').val(), 'user-input', 'We have just created your account and are logging you in');
										window.location.reload();
									} else {
										$dialog.dialog('open');
										accountPopupError($dialog, data.errorMessage ? (data.errorMessage.message || data.errorMessage) : 'Unknown Error');
									}
								},
								error: function(xhr, textStatus, error) {
									var message;
									switch (textStatus) {
										case 'parsererror' :
											message = 'Invalid response from the server.  Please refresh this page and try again.';
											break;
										case 'timeout' :
										case 'error' :
										case 'abort' :
										default:
											message = 'Could not connect to server to create your account.  Please refresh this page and try again.';
											break;
									}
									$dialog.dialog('open');
									accountPopupError($dialog, message);
								},
								complete : function() {
									closeMessageBox();
								}
							});
						}
					});
				},
				onOpen : function($dialog) {
					accountPopupClearErrors($dialog);
					$dialog.find('input[type="text"]').val($('#email_signup').val());
					$dialog.find('input[type="password"]').val('');
				}
			});
		}
		
		function validateAccountPopup($dialog) {
			var $email = $('#create_account_email');
			var $password = $('#create_account_password');
			var $password2 = $('#create_account_password2');
			var isValid = true;
			if (!Perio.validators.email($email.val())) {
				accountPopupError($dialog, "don't recognize email address");
				$email.addClass('invalid');
				isValid = false;
			}
			if (!Perio.validators.password($password.val())) {
				accountPopupError($dialog, "password must be more than 6 characters and contain a letter and a number.");
				$password.addClass('invalid');
				isValid = false;
			}
			if ($password2.val() != $password.val()) {
				accountPopupError($dialog, "passwords must match.");
				$password2.addClass('invalid');
				isValid = false;
			}
			return isValid;
		}
		
		function accountPopupClearErrors($dialog) {
			$dialog.find('.invalid').removeClass('invalid');
			$dialog.find('.error-box').hide().find('ul').empty();
			$dialog.find('.info-box').show();
		}

		function accountPopupError($dialog, error) {
			$dialog.find('.error-box').show()
			$dialog.find('.info-box').hide();

			var $ul = $dialog.find('.error-box ul');
			var $li = $('<li>').text(error);
			$ul.append($li);
		}
		
		function loadMessageBox(message, type, heading, onClose) {
			var $content = $('#success_message_box');

			Perio.openSimplePopup($content, {
				initialize : function($dialog) {
					$dialog.find('a.submit').click(function() { $dialog.dialog('close'); });
				},
				onOpen : function($dialog) {
					$dialog.unbind('dialogclose').bind('dialogclose', function() {
						if (onClose) {
							onClose();
						}
					});
					
					var $messageArea = $dialog.find('.message-area');
					$messageArea.empty();
					if (heading) {
						$('<h3></h3>').text(heading).appendTo($messageArea);
					}
					
					if (message) {
						$('<div class="'+type+'"></div>').text(message).appendTo($messageArea);
					}
				}
			});
		}
		
		function closeMessageBox() {
			$('#success_message_box').dialog('close');
		}
		
		function submitAction(e) {
			if (submittingLogin) return;
			
			submittingLogin = true;
			var $form = $(this);
			var $messageField = $dropdown.find('.messages');
			var url = '/ajaxLogin';
			$messageField.html('<div class="loading">Verifying Username and Password...</div>');
			var username = $.trim($('#email_signup').val().toLowerCase());
			$('#email_signup').val(username);
			$.ajax({
				url : url,
				data : $form.serialize(),
				type : 'POST',
				dataType : 'json',
				success : function(data) {
					if (data.login) {
						$messageField.html('<div class="message">Password Validated</div>');
						var location = window.location.pathname;
						var currentApp = window.location.pathname.replace(/\/control\/.*$/, '').replace(/^\//, '');
						if(location == "/") {
						  currentApp = 'tooth_to_tail';
						}else if (currentApp == '' || currentApp == 'portal') {
                            currentApp = 'ecom';
                        }
						else if (currentApp == 'tooth_to_tail') {
							currentApp = 'tooth_to_tail';
						}
						if (currentApp == data.preferredApp) {
							$.ajax({
								url : data.ecomLogin,
								type : 'GET',
								complete : function() {
									window.location.reload(true);
								}
							});
						} else {
							window.location = data.redirectUrl;
						}
					} else {
						$messageField.html('<div class="error">The username address or password entered is incorrect. Please try again.</div>');
					}
				},
				error: function(xhr, textStatus, error) {
					var message;
					switch (textStatus) {
						case 'parsererror' :
							message = 'Invalid response from the server.  Please refresh this page and try again.';
							break;
						case 'timeout' :
						case 'error' :
						case 'abort' :
						default:
							message = 'Could not connect to server to verify username and password.  Please refresh this page and try again.';
							break;
					}
					$messageField.html('<div class="error">'+message+'</div>');
				},
				complete : function() {
					submittingLogin  = false;
				}
			});
		}
	});
	</script>
<#else>
	<div style="display:none;">
		<div id="signup_dropdown">
			<div class="top-bar">
				<div class="action"></div>
			</div>
			<div class="wrap-content">
				<div class="submit-bar">
					<a style="width:180px;" href="<@ofbizUrl>myAccount?</@ofbizUrl><#if externalKeyParam?has_content>${StringUtil.wrapString(externalKeyParam)}</#if>" class="action-button">VIEW ACCOUNT</a>
					<br>
					<br>
					<a style="width:180px;" href="<@ofbizUrl>orderHistory?</@ofbizUrl><#if externalKeyParam?has_content>${StringUtil.wrapString(externalKeyParam)}</#if>" class="action-button">ORDER HISTORY</a>
					<br>
					<br>
					<a style="width:180px;" href="javascript:" class="action-button" id="dropdown_logout">LOGOUT</a>
				</div>
			</div>
		</div>
	</div>
	<script>
	jQuery(function($) {
		var $dropdown = null;
		var $loginButton = $('#top_header_v2 .login').click(showDropdown);
	
		function createDropdown() {
			if ($dropdown) return;
			
			$dropdown = $('#signup_dropdown').appendTo('body');
			$dropdown.find('.top-bar .action')
				.append($loginButton.find('.login-action').contents().clone())
				.css('width', $loginButton.find('.login-action').width())
				.find('img').attr('src', '/images/gray_expand_arrow.png');
			
			$dropdown.find('.top-bar').click(hideDropdown);
					
			$('body').append($dropdown).click(function(e) {
				if (dropdownShown && !$.contains($dropdown[0], e.target) && !$.contains($loginButton[0], e.target) && $dropdown[0] != e.target && $loginButton[0] != e.target) {
					hideDropdown();
				}
			});
			$(window).resize(function() {
				hideDropdown();
			});
		}
		
		function hideDropdown() {
			$dropdown.hide();
			dropdownShown = false;
		}
		
		function showDropdown() {
			createDropdown();
			$dropdown.show();
	
			var loginOffset = $loginButton.find('.login-action').offset();
			var loginWidth = $loginButton.find('.login-action').outerWidth();
			var topBarHeight =  $('#top_header_v2').height() - loginOffset.top;
			$dropdown.css({top : loginOffset.top, left : loginOffset.left + loginWidth + 13 - $dropdown.outerWidth()});
			$dropdown.find('.top-bar').css('height', topBarHeight);
			$dropdown.find('.top-bar .action').css('height', topBarHeight - 4);
	
			dropdownShown = true;
		}


		var $logoutButton = $('#dropdown_logout').click(function() {
			var functionQueue = [
				$.noop,
				function() {
					window.location = '';
				}
			];
			
		
			$.ajax({
				url : '/ajaxLogout',
				type : 'GET',
				dataType : 'json',
				complete : function(data) {
					functionQueue.shift()();
				}
			});
			$.ajax({
				url : '/ajaxLogout',
				type : 'GET',
				dataType : 'json',
				complete : function(data) {
					functionQueue.shift()();
				}
			});
		});
	});
	</script>
</#if>
</@addBottomHtmlContent>