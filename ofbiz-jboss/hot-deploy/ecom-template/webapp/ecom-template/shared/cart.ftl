<@import location="component://tooth_to_tail/templates/lib/portalContentMacros.ftl" />
<div class="cart-text">
	Cart - <span class="count">? items</span><br>
	Checkout
</div>

<@addBottomHtmlContent>
	<div id="cart_dropdown" style="display:none;">
		<div class="top-bar">
			<div class="action">
			</div>
		</div>
		<div class="wrap-content">
			<div class="default-message">
				There <span class="language-are">are</span> <span class="count">? items</span> in this Cart.
				<a href="<@ofbizUrl>products</@ofbizUrl>">Go shop!</a> 
			</div>
			<div class="message">
			</div>
			<div class="cart-update">
				<div class="image-side">
					<div class="image-container">
						
					</div>
				</div>
				<div class="content-side">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<th class="name"></th>
							<th colspan="2">QTY</th>
						</tr>
						<tr class="description-row">
							<td class="description"></td>
							<td>
								<select name="quantity">
									<#list ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99"] as index>
										<option value="${index}">${index}</option>
									</#list>
									<option value="100" class="extra">100</option>
								</select>
							</td>
							<td class="price">
							</td>
						</tr>
						<tr class="action-row">
							<td>
								<a class="remove-item" href="javascript:">REMOVE ITEM</a>
							</td>
							<td colspan="2">
								<a class="update-item" href="javascript:">UPDATE</a>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="cart-details">
				<div class="subtotal">
					Subtotal
					<span class="amount"></span>
				</div>
				<div class="actions">
					<a href="<@ofbizUrl>shopping-cart</@ofbizUrl>">View/Edit Shopping Cart &raquo;</a>
					<a href="<@ofbizUrl>products</@ofbizUrl>">Continue Shopping &raquo;</a>
				</div>
				<div class="final-action">
					<a class="action-button" href="<@ofbizUrl>shopping-cart</@ofbizUrl>">CHECK OUT</a>
				</div>
			</div>
		</div>
	</div>
	
	<div id="promo_code_popup" style="display:none;">
		<h3>Please enter your ProCode:</h3>
		<div class="action-form">
			<form method="POST" action="javascript:">
				<label>ProCode</label>
				<input name="promo_code" type="text" />
				<@hiddenSubmit/>
				<div class="action-bar">
					<a class="action-button submit" href="javascript:">Submit ProCode</a>
				</div>
			</form>
		</div>
	</div>
	
	
	<script>
	jQuery(function($) {
		var $cart = $('#top_header_v2 .cart');
		var $dropdown = null;
		var dropdownShown = false;
		
		<#if !(disableCartModification!false)>
			$cart.click(showDropdown);
		</#if>
		$('body').bind('addToCart', updateCart);
		
		$('#cart_dropdown .remove-item').click(function() {
			Perio.removeFromCart(Perio.cart.recentItem.index);
		});
		$('#cart_dropdown .update-item').click(function() {
			Perio.updateCartQuantity(Perio.cart.recentItem.index, $('#cart_dropdown [name="quantity"]').val());
		});
		
		$.ajax({
			url : '<@ofbizUrl>viewCartAjax</@ofbizUrl>',
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				if (data.responseMessage == 'success') {
					Perio.cart.recentItem = data.recentItem;
					Perio.cart.itemCount = data.itemCount;
					Perio.cart.subtotal = data.subtotal;
					Perio.cart.promos = data.promos;
				} else {
					_setCartMessage(data.errorMessage, 'error');
				}
			},
			complete : function() {
				updateCart();
			}
		});
		
		function updateCart() {
			createDropdown();
			
			var productCount, languageAre;
			if (Perio.cart.itemCount == 0) {
				productCount = 'no items';
				languageAre = 'are';
			} else if (Perio.cart.itemCount == 1) {
				productCount = '1 item';
				languageAre = 'is';
			} else {
				productCount = Perio.cart.itemCount + ' items';
				languageAre = 'are';
			}
			$('#top_header_v2 .cart-text .count, #cart_dropdown .default-message .count').text(productCount);
			$('#cart_dropdown .default-message .language-are').text(languageAre);
			
			if (Perio.cart.recentItem && Perio.cart.recentItem.productId) {
				var $productBox = $dropdown.find('.cart-update').show();
				var recentItem = Perio.cart.recentItem;
				var $quantityField = $productBox.find('[name="quantity"]');
				
				$productBox.find('.image-container').empty();
				if (recentItem.smallImageUrl) {
					$productBox.find('.image-container').append('<img src="'+recentItem.smallImageUrl+'">');
				}
				$productBox.find('.name').text(recentItem.name);
				$productBox.find('.description').text(recentItem.description);
				$quantityField.val(recentItem.quantity);
				$productBox.find('.price').text('$'+parseFloat(recentItem.price).toFixed(2));
				
				if ($quantityField.val() != recentItem.quantity) {
					var $option = $quantityField.find('.extra');
					$option.attr('value', recentItem.quantity);
					$option.text(recentItem.quantity);
					$quantityField.append($option);
					$quantityField.val(recentItem.quantity);
				}
			} else {
				$dropdown.find('.cart-update').hide();
			}
			
			if (Perio.cart.message.text) {
				$dropdown.find('.default-message').hide();
				var $message = $dropdown.find('.message').show().empty();
				$message.attr('class', 'message ' + Perio.cart.message.type);
				
				if (Perio.cart.message.type == 'success') {
					$message.append('<strong>Success!</strong>');
				} else if (Perio.cart.message.type == 'error') {
					$message.append('<strong>Error!</strong>');
				}
				$message.append(' ');
				
				$('<span>').text(Perio.cart.message.text).appendTo($message);
				
				if (Perio.cart.autoOpen || Perio.cart.message.type == 'error') {
					showDropdown();
				}
			} else {
				$dropdown.find('.default-message').show();
				$dropdown.find('.message').hide();
			}
			
			if (Perio.cart.itemCount > 0) {
				$dropdown.find('.cart-details').show();
				$dropdown.find('.subtotal .amount').text('$' + parseFloat(Perio.cart.subtotal).toFixed(2));
				$('#cart_dropdown .default-message a').hide();
			} else {
				$dropdown.find('.cart-details').hide();
				$('#cart_dropdown .default-message a').show();
			}
			
			if (Perio.cart.promos.length > 0) {
				$('#promo_code_popup input[name="promo_code"]').val(Perio.cart.promos[0].promoCode);
			}
		}
		
		function createDropdown() {
			if ($dropdown) return;
			$dropdown = $('#cart_dropdown');
			
			$dropdown.find('.top-bar').click(hideDropdown);
			$('body').append($dropdown).click(function(e) {
				if (dropdownShown && !$.contains($dropdown[0], e.target) && !$.contains($cart[0], e.target) && $dropdown[0] != e.target && $cart[0] != e.target) {
					hideDropdown();
				}
			});
			$(window).resize(function() {
				hideDropdown();
			});
		}
		
		function hideDropdown() {
			$dropdown.hide();
			dropdownShown = false;
		}
		
		function showDropdown() {
			createDropdown();
			$dropdown.show();
	
			var cartOffset = $cart.offset();
			var cartWidth = $cart.outerWidth();
			
			var dropdownTop = cartOffset.top + 6;
			
			$dropdown.find('.top-bar .action').html($cart.find('.cart-text').html());
			
			$dropdown.css({top : dropdownTop, left : cartOffset.left + cartWidth - 1 - $dropdown.outerWidth()});
			$dropdown.find('.top-bar').css('height', $('#top_header_v2').height() - dropdownTop);
	
			dropdownShown = true;
		}
	});
	</script>
</@addBottomHtmlContent>