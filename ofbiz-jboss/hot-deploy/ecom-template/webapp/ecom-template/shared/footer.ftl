<footer class="footer">
  <div class="container-fluid">
  <div class="row">	  
    <div class="col-md-6 col-sm-6">
      <p class="text-muted">		 Time Zone : Eastern Daylight Time</p>
    </div>
    <div class="col-md-6 col-sm-6">
      <p class="text-muted float-right">		2018 © <img alt="Group FiO" src="/bootstrap/images/logo.png" height="20" width="44" border="0" > 
        Professional Edition of FiO RMS Build 2018.2. All Rights Reserved
      </p>
    </div>
    </div>
  </div>
</footer>
