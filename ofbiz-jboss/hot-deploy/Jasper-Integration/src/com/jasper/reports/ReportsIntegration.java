package com.jasper.reports;

import org.ofbiz.entity.Delegator;
import java.util.List;
import java.util.Map;


public interface ReportsIntegration {
	
	public String generateReport(Delegator delegator,String registryId,Map<String,Object> jrParameters,List<Map<String,Object>> jrDataSource,String reportFormat);
}
