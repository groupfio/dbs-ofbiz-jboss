package com.jasper.reports;

import java.util.Map;
import java.util.List;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;

import javolution.util.FastMap;

public class JasperReports implements ReportsIntegration {

	public static final String module = JasperReports.class.getName();

	public String generateReport(Delegator delegator, String registryId,Map<String,Object> jrParameters,List<Map<String,Object>> jrDataSource,String reportFormat) {
		String reportId = null;
		Map<String, String> reportHistoryMap = FastMap.newInstance();
		try {
			// 1. once report get started need to load the report history to
			// know the report
			GenericValue reportHistory = ReportUtil.prepareReportHistory(delegator, null, null);
			
			if(UtilValidate.isNotEmpty(reportHistory))
			{
				reportId = reportHistory.getString("reportId");
			}
			
			//2. check for report registry information is valid or not otherwise don't proceed
			GenericValue reportRegistry = delegator.findOne("ReportRegistry", false, UtilMisc.toMap("reportRegistrId", registryId));
			if (UtilValidate.isEmpty(reportRegistry)) {
				reportHistoryMap.put("status", ReportsConstant.REPORT_FAILED);
				reportHistoryMap.put("message",
						"Report Registry information is not available, Please contact administrator");
				ReportUtil.prepareReportHistory(delegator, reportId, reportHistoryMap);
				return null;
			}
			
			
			//3. check for Report Configuration is valid or not
			GenericValue reportConfiguration = delegator.findOne("ReportConfiguration", false, UtilMisc.toMap("reportRegistrId", registryId,"reportFormat",reportFormat));
			if(UtilValidate.isEmpty(reportConfiguration))
			{
				reportHistoryMap.put("status", ReportsConstant.REPORT_FAILED);
				reportHistoryMap.put("message",
						"Report Configuration information is not available, Please contact administrator");
				ReportUtil.prepareReportHistory(delegator, reportId, reportHistoryMap);
				return null;
			}
			
			
			//4. once configuration done need to proceed thread because, if report data count is more means will cause time out exception
			JasperJob job = new JasperJob();
			//Thread t = new Thread(job);
			
			job.setDelegator(delegator);
			job.setReportRegistry(reportRegistry);
			job.setReportConfiguration(reportConfiguration);
			job.setReportHistory(reportHistory);
			job.setJrParameters(jrParameters);
			job.setJrDataSource(jrDataSource);
			
			String backgroundReport = reportConfiguration.getString("isBackgroundReport");
			
			//check for back ground thread or normal process, Background thread is required when more data is came
			if("Y".equalsIgnoreCase(backgroundReport))
			{
				Thread t = new Thread(job);
				t.start();
			}else
			{
				job.run();
			}
			 
			

		} catch (GenericEntityException e) {

			Debug.logError("Error occured while validing report", module);
		}
		return reportId;
	}

}
