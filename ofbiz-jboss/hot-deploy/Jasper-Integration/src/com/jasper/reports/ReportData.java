package com.jasper.reports;

import java.util.List;
import java.util.Map;

public class ReportData {
	private List<Map<String, Object>> jrDataSource;
	
	private Map<String, Object> jrParmeters;

	private String reportRegistryId; 
	
	private String downloadFormat;
	
	public List<Map<String, Object>> getJrDataSource() {
		return jrDataSource;
	}
	

	public void setJrDataSource(List<Map<String, Object>> jrDataSource) {
		this.jrDataSource = jrDataSource;
	}
	

	public Map<String, Object> getJrParmeters() {
		return jrParmeters;
	}
	

	public void setJrParmeters(Map<String, Object> jrParmeters) {
		this.jrParmeters = jrParmeters;
	}


	public String getReportRegistryId() {
		return reportRegistryId;
	}
	


	public void setReportRegistryId(String reportRegistryId) {
		this.reportRegistryId = reportRegistryId;
	}
	


	public String getDownloadFormat() {
		return downloadFormat;
	}
	


	public void setDownloadFormat(String downloadFormat) {
		this.downloadFormat = downloadFormat;
	}
	
	
	

	
}
