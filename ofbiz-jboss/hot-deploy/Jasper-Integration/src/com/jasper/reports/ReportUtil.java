package com.jasper.reports;

import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.transaction.GenericTransactionException;
import org.ofbiz.entity.transaction.TransactionUtil;

public class ReportUtil {
	public static final String module = ReportUtil.class.getName();

	public static GenericValue prepareReportHistory(Delegator delegator, String reportId,
			Map<? extends String, ? extends Object> context) {
		GenericValue reportHistory = null;
		try {
			TransactionUtil.begin();
			if (UtilValidate.isNotEmpty(reportId)) {
				reportHistory = delegator.findOne("ReportHistory", false, UtilMisc.toMap("reportId", reportId));
			}
			if (UtilValidate.isEmpty(reportHistory)) {
				reportHistory = delegator.makeValue("ReportHistory");
				reportHistory.put("reportId", delegator.getNextSeqId("ReportRegistry"));
				reportHistory.create();
			} else {
				reportHistory.setNonPKFields(context);
				reportHistory.store();
			}
			TransactionUtil.commit();
		} catch (Exception e) {
			try {
				TransactionUtil.rollback();
			} catch (GenericTransactionException e1) {
				Debug.logError("Error occured while committing.... " + e1.getMessage(), module);
			}
			Debug.logError("Generating reportHistory get failed " + e.getMessage(), module);
		}

		return reportHistory;
	}

	/*
	 * Method helps to get the class object dynamically without changing the reports gateway
	 */
	public ReportsIntegration returnCurrentIntegration() {

		ReportsIntegration integration = null;
		try
		{
			String implementationClass = UtilProperties.getPropertyValue("Jasper-Integration", "report-integration");
			Class c = Class.forName(implementationClass);
			integration = (ReportsIntegration)c.newInstance();
		}catch(Exception e)
		{
			Debug.logError("Unable to generate class object, Please contact development team",module );
		}
		return integration;

	}
}
