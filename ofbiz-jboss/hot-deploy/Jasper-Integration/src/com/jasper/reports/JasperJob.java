package com.jasper.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

//BULK RECORD MAY CAUSE TIME OUT EXCEPTION SO NEED TO PROCEED WITH THREAD
public class JasperJob implements Runnable {
	public static final String module = ReportUtil.class.getName();

	private Delegator delegator;
	private GenericValue reportHistory;
	private GenericValue reportConfiguration;
	private GenericValue reportRegistry;
	private Map<String, Object> jrParameters;
	private List<Map<String, Object>> jrDataSource;

	public Delegator getDelegator() {
		return delegator;
	}

	public void setDelegator(Delegator delegator) {
		this.delegator = delegator;
	}

	public GenericValue getReportHistory() {
		return reportHistory;
	}

	public void setReportHistory(GenericValue reportHistory) {
		this.reportHistory = reportHistory;
	}

	public GenericValue getReportConfiguration() {
		return reportConfiguration;
	}

	public void setReportConfiguration(GenericValue reportConfiguration) {
		this.reportConfiguration = reportConfiguration;
	}

	public GenericValue getReportRegistry() {
		return reportRegistry;
	}

	public void setReportRegistry(GenericValue reportRegistry) {
		this.reportRegistry = reportRegistry;
	}

	public Map<String, Object> getJrParameters() {
		return jrParameters;
	}

	public void setJrParameters(Map<String, Object> jrParameters) {
		this.jrParameters = jrParameters;
	}

	public List<Map<String, Object>> getJrDataSource() {
		return jrDataSource;
	}

	public void setJrDataSource(List<Map<String, Object>> jrDataSource) {
		this.jrDataSource = jrDataSource;
	}

	public void run() {
		try {

			String reportId = reportHistory.getString("reportId");

			// 5. Jasper integration for PDF,Excel so on(Report location should
			// be with file name)
			String reportLocation = reportRegistry.getString("reportLocation");
			if (UtilValidate.isEmpty(reportLocation)) {
				reportHistory.put("status", ReportsConstant.REPORT_FAILED);
				reportHistory.put("message",
						"Report JRXML report location not found, Please configure Jrxml design file location.");
				ReportUtil.prepareReportHistory(delegator, reportId, reportHistory);
			} else {

				// 6. once got root location need to append with the application
				// path specified with jrxml
				String rootLocation = ComponentConfig.getRootLocation(ReportsConstant.COMPONENT_NAME);
				String filePath = rootLocation + reportLocation;

				// 7. Check for file exist or not
				File f = new File(filePath);
				if (!f.isFile()) {

					reportHistory.put("status", ReportsConstant.REPORT_FAILED);
					reportHistory.put("message",
							"Report JRXML report file(JRXML) not found, Please configure Jrxml design file at specified location [ "
									+ filePath + " ]");
					ReportUtil.prepareReportHistory(delegator, reportId, reportHistory);

				} else {

					// 8. once every configuration is valid proceed with jr
					// report
					JasperReport jasperReport = JasperCompileManager.compileReport(filePath);

					JRDataSource datasource = new JRBeanCollectionDataSource(jrDataSource);

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, jrParameters, datasource);
					File outputFile = null;
					if ("PDF".equalsIgnoreCase(reportConfiguration.getString("reportFormat"))) {
						 outputFile = new File(reportConfiguration.getString("reportDownloadLocation")
								+ File.separator + reportConfiguration.getString("reportName")
								+ Long.valueOf(System.currentTimeMillis()).toString() + ".pdf");
						reportHistory.put("reportFileName", reportConfiguration.getString("reportName")+"_"+Long.valueOf(System.currentTimeMillis()) + ".pdf");
						FileOutputStream fos = new FileOutputStream(outputFile);

						JRPdfExporter exporter = new JRPdfExporter();
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
						exporter.exportReport();

						fos.close();

					}

					if ("EXCEL".equalsIgnoreCase(reportConfiguration.getString("reportFormat"))) {
						 outputFile = new File(reportConfiguration.getString("reportDownloadLocation")
								+ File.separator + reportConfiguration.getString("reportName")
								+ Long.valueOf(System.currentTimeMillis()).toString() + ".xls");

						FileOutputStream fos = new FileOutputStream(outputFile);
						reportHistory.put("reportFileName", reportConfiguration.getString("reportName")+"_"+Long.valueOf(System.currentTimeMillis()) + ".xls");
						JRXlsExporter xlExporter = new JRXlsExporter();
						xlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
						xlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						xlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
						xlExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
						xlExporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
						xlExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
						xlExporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
						xlExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,
								Boolean.TRUE);
						xlExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
								Boolean.TRUE);
						xlExporter.exportReport();

						fos.close();

					}

					reportHistory.put("status", ReportsConstant.REPORT_SUCCESS);
					reportHistory.put("isRemovable", reportConfiguration.getString("isDownloadable"));
				
					reportHistory.put("reportLocation", outputFile.getAbsolutePath());
					reportHistory.put("message","");
					ReportUtil.prepareReportHistory(delegator, reportId, reportHistory);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage() + "---------------------------");

		}
	}

}
