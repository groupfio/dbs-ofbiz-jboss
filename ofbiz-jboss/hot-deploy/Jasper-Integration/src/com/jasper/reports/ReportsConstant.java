package com.jasper.reports;

public class ReportsConstant {

	public final static String REPORT_SUCCESS = "SUCCESS";
	public final static String REPORT_FAILED = "FAILED";
	public final static String REPORT_IN_PROGRESS = "IN-PROGRESS";
	public final static String COMPONENT_NAME = "Jasper-Integration";
}
