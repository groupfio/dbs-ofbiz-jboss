package com.jasper.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;



public class ReportGateway {

	public static String generateReport(HttpServletRequest request, HttpServletResponse response,ReportData reporData) {
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String reportRegistryId = reporData.getReportRegistryId();
		ReportUtil util = new ReportUtil();
		String downloadFormat = reporData.getDownloadFormat();
		List<Map<String, Object>> jrDataSource = reporData.getJrDataSource();
		Map<String, Object> jrParmeters = reporData.getJrParmeters();
		if (UtilValidate.isEmpty(reportRegistryId)) {
			return "error";
		}

		if (UtilValidate.isEmpty(downloadFormat)) {

			return "error";
		}

		ReportsIntegration integration = util.returnCurrentIntegration();

		String reportId = integration.generateReport(delegator, reportRegistryId, jrParmeters, jrDataSource,
				downloadFormat);

		//downloadFile(request, response,reportId);
		return reportId;
	}

	public static String downloadFile(HttpServletRequest request, HttpServletResponse response) {
		try {

			String reportId = request.getParameter("reportId");
			if(UtilValidate.isEmpty(reportId))
			{
				reportId = (String)request.getAttribute("reportId");
			}
			Delegator delegator = (Delegator) request.getAttribute("delegator");
			if (UtilValidate.isNotEmpty(reportId)) {
				GenericValue reportHistory = delegator.findOne("ReportHistory", false,
						UtilMisc.toMap("reportId", reportId));
				if (UtilValidate.isNotEmpty(reportHistory)) {
					String reportLocation = reportHistory.getString("reportLocation");
					String reportFileName = reportHistory.getString("reportFileName");
					response.setContentType("application/octet-stream");
					response.setHeader("Content-Disposition", "attachment;filename=" + reportFileName);
					OutputStream out = response.getOutputStream();
					FileInputStream in = new FileInputStream(reportLocation);
					byte[] buffer = new byte[4096];
					int length;
					while ((length = in.read(buffer)) > 0) {
						out.write(buffer, 0, length);
					}
					if(in!=null)
						in.close();
					
					if(out!=null)
						out.close();
					
					if("Y".equalsIgnoreCase(reportHistory.getString("isRemovable")))
					{
						File f = new File(reportLocation);
						f.delete();
						
					}else
					{
						reportHistory.put("status", ReportsConstant.REPORT_SUCCESS);
						reportHistory.put("message","");
						ReportUtil.prepareReportHistory(delegator, reportId, reportHistory);
					}
				}

			}
		} catch (Exception e) {
			Debug.logError("Unable to download due to " + e.getMessage(), ReportGateway.class.getName());
		}
		return "success";
	}
}
