/**
 * 
 */
package org.groupfio.pricing.app.constants;

/**
 * @author Sharif Ul Islam
 * @since June 16, 2015
 *
 */
public class RTConstants {
	
	// Resource bundles	
    public static final String configResource = "pricing-app";
    public static final String uiLabelMap = "PricingAppUiLabels";
    
    public static final int DEFAULT_BUFFER_SIZE = 102400;
    public static final int LOCKBOX_ITEM_SEQUENCE_ID_DIGITS = 5;
    
	public static final class AppStatus {
        private AppStatus() { }
        public static final String ACTIVATED = "ACTIVATED";
        public static final String DEACTIVATED = "DEACTIVATED";
    }
	
	public static final class SourceInvoked {
        private SourceInvoked() { }
        public static final String API = "API";
        public static final String PORTAL = "PORTAL";
        public static final String UNKNOWN = "UNKNOWN";
    }
	
}
