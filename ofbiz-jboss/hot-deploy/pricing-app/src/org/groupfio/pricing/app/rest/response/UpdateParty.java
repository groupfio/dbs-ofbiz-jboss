/**
 * 
 */
package org.groupfio.pricing.app.rest.response;

import java.math.BigDecimal;
import java.util.Map;

import org.groupfio.homeapps.rest.response.Response;
import org.groupfio.homeapps.util.ParamUtil;

/**
 * @author Sharif
 *
 */
public class UpdateParty extends Response {

	private String partyID;
	private String externalAppPartyRef;
	private String partyStatus;
	
	private BigDecimal calculatedPrice;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setPartyID( ParamUtil.getString(context, "partyId") );
		setExternalAppPartyRef( ParamUtil.getString(context, "externalAppPartyRef") );
		setPartyStatus( ParamUtil.getString(context, "partyStatus") );
		
	}

	public String getPartyID() {
		return partyID;
	}

	public void setPartyID(String partyID) {
		this.partyID = partyID;
	}

	public String getExternalAppPartyRef() {
		return externalAppPartyRef;
	}

	public void setExternalAppPartyRef(String externalAppPartyRef) {
		this.externalAppPartyRef = externalAppPartyRef;
	}

	public String getPartyStatus() {
		return partyStatus;
	}

	public void setPartyStatus(String partyStatus) {
		this.partyStatus = partyStatus;
	}

	public BigDecimal getCalculatedPrice() {
		return calculatedPrice;
	}

	public void setCalculatedPrice(BigDecimal calculatedPrice) {
		this.calculatedPrice = calculatedPrice;
	}
	
}
