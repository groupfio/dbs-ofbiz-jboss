/**
 * 
 */
package org.groupfio.pricing.app.rest.resource;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.homeapps.constants.GlobalConstants;
import org.groupfio.homeapps.rest.response.Response;
import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ResponseUtils;
import org.groupfio.pricing.app.rest.response.PriceEnquiryResponse;
import org.groupfio.pricing.app.rest.response.UpdateParty;
import org.groupfio.pricing.app.util.AppUtil;
import org.groupfio.pricing.app.validator.Validator;
import org.groupfio.pricing.app.validator.ValidatorFactory;
import org.groupfio.pricing.app.writer.WriterUtil;
import org.groupfio.pricing.engine.request.PriceRequest;
import org.groupfio.pricing.engine.service.PriceService;
import org.groupfio.pricing.engine.service.SampleService;
import org.groupfio.pricing.engine.service.ServiceFactory;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
@Path("/pp/v1/price")
public class PriceResource {

	private static final String MODULE = PriceResource.class.getName();

	@Context
    HttpHeaders headers;
	
	@POST
	@Path("/testService")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
	public UpdateParty testService(String jsonReq) {
		Debug.log("==========updateParty jsonReq========"+jsonReq);
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        UpdateParty response = new UpdateParty();
		
		response.setResponseRefId("111111111");
		
		SampleService sampleService = ServiceFactory.getSampleService();
		
		Map<String, Object> priceContext = new HashMap<String, Object>();
		
		Map<String, Object> priceResult = sampleService.getPrice(priceContext);
		
		System.out.println(ParamUtil.getBigDecimal(priceResult, "calculatedPrice"));
		System.out.println(priceResult.get("users"));
		
		response.setCalculatedPrice(ParamUtil.getBigDecimal(priceResult, "calculatedPrice"));
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
		
	@POST
	@Path("/priceEnquiry")
	@Produces(MediaType.APPLICATION_JSON)
	//@Consumes(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
	public Response priceEnquiry(String jsonReq) {
		Debug.log("==========priceEnquiry jsonReq========"+jsonReq);
		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        Response response = new PriceEnquiryResponse();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "priceEnquiry")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, GlobalConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
			try {
				
				GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
				
				JSON jsonFeed = JSON.from(jsonReq);
				
				JSONToMap jsonMap = new JSONToMap();
				request = jsonMap.convert(jsonFeed);
				
				Validator validator = ValidatorFactory.getPriceEnquiryDataValidator();
				Map<String, Object> validatorContext = new HashMap<String, Object>();
				validatorContext.put("delegator", delegator);
				validatorContext.put("data", request);
				
				Map<String, Object> validatorResponse = validator.validate(validatorContext);
				if (!ResponseUtils.isError(validatorResponse)) {
					
					PriceService priceService = ServiceFactory.getPriceService();
					
					Map<String, Object> context = new HashMap<String, Object>();
					
					PriceRequest priceRequest = new PriceRequest();
					priceRequest.build(request);
					
					context.put("priceRequest", priceRequest);
					
					Map<String, Object> res = priceService.getPrice(context);
					
					//System.out.println(ParamUtil.getBigDecimal(res, "calculatedPrice"));
					//System.out.println(res.get("users"));
					
					/*
					Map<String, Object> context = new HashMap<String, Object>();
					
					context.put("partyId", request.get("partyId"));
					context.put("partyName", request.get("partyName"));
					context.put("baseCurrency", request.get("baseCurrency"));
					context.put("description", request.get("description"));
					context.put("externalAppPartyRef", request.get("externalAppPartyRef"));
					context.put("partyStatus", request.get("partyStatus"));
					
					context.put("userLogin", userLogin);
					
					Map<String, Object> res = dispatcher.runSync("rt.updateParty", context);
					res.put("delegator", delegator);
					res.put("externalAppPartyRef", request.get("externalAppPartyRef"));*/
					
					if (ServiceUtil.isSuccess(res)) {
						res.put("responseCode", "S200");
					} else {
						res.put("responseCode", "E900");
					}
				
					response.build(res);
				} else {
					response.build(validatorResponse);
				} 
				
			} catch (Exception e) {
				//e.printStackTrace();
				Debug.log("Error in updateParty "+e);
				response.setResponseCode("E900");
			}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String ofbizApiLogId = WriterUtil.writeLog(delegator, "priceEnquiry", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(ofbizApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}

}
