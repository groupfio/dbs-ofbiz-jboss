/**
 * 
 */
package org.groupfio.pricing.app.validator;

import org.groupfio.pricing.app.validator.AccessTokenValidator;
import org.groupfio.pricing.app.validator.UpdatePartyDataValidator;

/**
 * @author Sharif
 *
 */
public final class ValidatorFactory {

	private static final AccessTokenValidator ACCESS_TOKEN_VALIDATOR = new AccessTokenValidator();
	
	private static final UpdatePartyDataValidator UPDATE_PARTY_VALIDATOR = new UpdatePartyDataValidator();
	
	private static final PriceEnquiryDataValidator PRICE_ENQUIRY_VALIDATOR = new PriceEnquiryDataValidator();
	
	public static AccessTokenValidator getAccessTokenValidator () {
		return ACCESS_TOKEN_VALIDATOR;
	}
	
	public static UpdatePartyDataValidator getUpdatePartyDataValidator () {
		return UPDATE_PARTY_VALIDATOR;
	}
	
	public static PriceEnquiryDataValidator getPriceEnquiryDataValidator () {
		return PRICE_ENQUIRY_VALIDATOR;
	}
	
}
