package org.groupfio.pricing.app.rest.response;

import java.util.List;
import java.util.Map;

import org.groupfio.homeapps.rest.response.Response;
import org.groupfio.pricing.engine.request.PriceRequest;
import org.groupfio.pricing.engine.response.PriceResponse;

/**
 * @author Sharif
 *
 */
public class PriceEnquiryResponse extends Response {
	
	private static final String MODULE = PriceEnquiryResponse.class.getName();
    
	private PriceRequest priceRequest;
	
	private List<PriceResponse> priceResponses;
	
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setPriceRequest( (PriceRequest) context.get("priceRequest") );
		setPriceResponses( (List<PriceResponse>) context.get("priceResponses") );
		
	}
	
	public PriceRequest getPriceRequest() {
		return priceRequest;
	}

	public void setPriceRequest(PriceRequest priceRequest) {
		this.priceRequest = priceRequest;
	}

	public List<PriceResponse> getPriceResponses() {
		return priceResponses;
	}

	public void setPriceResponses(List<PriceResponse> priceResponses) {
		this.priceResponses = priceResponses;
	}
	
}