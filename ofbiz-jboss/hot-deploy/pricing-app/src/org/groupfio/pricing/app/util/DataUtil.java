/**
 * 
 */
package org.groupfio.pricing.app.util;

import java.util.Map;

import org.groupfio.pricing.app.constants.RTConstants.SourceInvoked;
import org.groupfio.pricing.app.util.DataUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Sharif
 *
 */
public class DataUtil {
	
	private static String MODULE = DataUtil.class.getName();

	public static void prepareAppStatusData(Map<String, Object> data) {
		if (UtilValidate.isEmpty(data.get("sourceInvoked"))) {
			data.put("sourceInvoked", SourceInvoked.UNKNOWN);
		}
	}
	
	public static String getStatusId (Delegator delegator, String statusCode) {
		try {
			GenericValue statusItem = EntityUtil.getFirst( delegator.findByAnd("StatusItem", UtilMisc.toMap("statusTypeId", "CUSTOM_FIELD_STATUS", "statusCode", statusCode), null, false) );
			if (UtilValidate.isNotEmpty(statusItem)) {
				return statusItem.getString("statusId");
			}
		} catch (Exception e) {
			Debug.logError(e, MODULE);
		}
		return null;
	}
	
}
