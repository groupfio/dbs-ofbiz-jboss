<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>Tab <small>Component</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
				
			<div class="form-body">
			
			<@tabComponent 
					activeTabId="tab_2"
					tabs=tabOptions
					/>
																											
			</div>
			
		</div>
						
	</div>
	
</div>