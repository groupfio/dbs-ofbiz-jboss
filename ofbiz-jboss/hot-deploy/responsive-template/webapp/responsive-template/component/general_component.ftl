<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>General <small>Components</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
				
			<div class="form-body">
			
			<@generalInput 
				id="form_control_1"
				label="Regular input"
				placeholder="Enter your name"
				/>
						
			<@readonlyInput 
				id="form_control_9"
				label="Readonly"
				value="email@example.com"
				/>
				
			<@dropdownInput 
				id="form_control_6"
				label="Dropdown Input"
				hint="Some help goes here..."
				options=dropdownOptions
				/>	
			
			<@dateInput 
				id="form_control_10"
				label="Date Input"
				disableFutureDate=true
				/>
			 
			<@checkboxesInput 
				name="form_control_18"
				label="Checkboxes"
				options=checkboxOptions
				/>	 
				
			<@radioInput 
				name="form_control_17"
				label="Radio Input"
				options=dropdownOptions
				/>	
				
			<@textareaInput 
				id="form_control_7"
				label="Textarea"
				placeholder="Enter more text"
				hint="Some help goes here..."
				rows="3"
				state="success"
				/>	
			
			<#-- 		
			<@dateRangeInput 
				idTo="form_control_11"
				idFrom="form_control_12"
				label="Date Range Input"
				/>		
				
			<@inlineDateInput 
				id="form_control_13"
				label="Inline Date Input"
				/>	
			
			<@dateTimeInput 
				id="form_control_14"
				label="Date Time Input"
				/>	
				
			<@dateTimeAdvanceInput 
				id="form_control_15"
				label="Date Time Advance Input"
				/>	
				
			<@timeInput 
				id="form_control_16"
				label="Time Input"
				/>	
				
			<@radioInput 
				name="form_control_17"
				label="Radio Input"
				options=dropdownOptions
				/>
			
			<@checkboxesInput 
				name="form_control_18"
				label="Checkboxes"
				options=checkboxOptions
				/>	
			 -->
			 
			 <#-- 
			<@generalInputFloating 
				id="form_control_19"
				label="Regular input"
				/>
			
			<@inputWithHint 
				id="form_control_2"
				label="Input with hint"
				placeholder="Enter your email"
				hint="Some help goes here..."
				/>
			
			<@successInput 
				id="form_control_3"
				label="Success Input"
				placeholder="Success state"
				hint="Some help goes here..."
				/>
				
			<@warningInput 
				id="form_control_4"
				label="Warning Input"
				placeholder="Warning state"
				hint="Some help goes here..."
				/>	
				
			<@errorInput 
				id="form_control_5"
				label="Error State"
				placeholder="Error State"
				hint="Some help goes here..."
				/>
			
			<@dropdownInput 
				id="form_control_6"
				label="Dropdown Input"
				hint="Some help goes here..."
				options=dropdownOptions
				/>
			
			<@textareaInput 
				id="form_control_7"
				label="Textarea"
				placeholder="Enter more text"
				hint="Some help goes here..."
				rows="3"
				state="success"
				/>
			 -->
																											
			</div>
			
			<@fromCommonAction showCancelBtn=false/>
			
							
		</div>
						
	</div>
	
</div>