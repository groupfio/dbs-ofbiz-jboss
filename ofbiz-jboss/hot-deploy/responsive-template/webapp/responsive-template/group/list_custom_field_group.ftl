<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.List} ${uiLabelMap.CustomFieldGroup}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list-custom-field-group">
	<thead>
	<tr>
		<th>${uiLabelMap.groupId!}</th>
		<th>${uiLabelMap.groupName!}</th>
		<th>${uiLabelMap.hide!}</th>
		<th>${uiLabelMap.sequence!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	<#if customFieldGroupList?has_content>
		
	<#list customFieldGroupList as ec>
	<tr>
		<td>${ec.groupId!}</td>
		<td>${ec.groupName!}</td>
		<td>${ec.hide!}</td>
		<td>${ec.sequence!}</td>
		<td class="text-center">
			<div class="">
				<a href="customFieldForGroup?groupId=${ec.groupId}" class="btn btn-xs btn-secondary tooltips" data-original-title="Configuration"><i class="fa fa-file-text-o"></i></a>
				<a href="editCustomFieldGroup?groupId=${ec.groupId}" class="btn btn-xs btn-secondary tooltips" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></a>
				<a class="btn btn-xs btn-secondary tooltips confirm-message" href="deleteCustomFieldGroup?groupId=${ec.groupId}" data-original-title="Remove"><i class="fa fa-times red"></i></a>
			</div>
		</td>	
	</tr>
	
	</#list>
		
	</#if>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		

	$('#list-custom-field-group').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	
});	
	
</script>