package org.groupfio.responsive.template.validator;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.homeapps.ResponseCodes;
import org.groupfio.homeapps.constants.GlobalConstants;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Sharif
 *
 */
public class UpdatePartyDataValidator implements Validator {

	private static String MODULE = UpdatePartyDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			
			if (UtilValidate.isEmpty(data.get("partyId"))) {
				setValidate(false);
				message = "E200";
				validationMessage.put("partyId", message);
			} else {
				GenericValue party = EntityUtil.getFirst( delegator.findByAnd("PartyGroup", UtilMisc.toMap("partyId", data.get("partyId")), null, false) ); 
				if (UtilValidate.isEmpty(party)) {
					setValidate(false);
					message = "E206";
					validationMessage.put("partyId", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("partyStatus"))) {
				setValidate(false);
				message = "E207";
				validationMessage.put("partyStatus", message);
			} else {
				GenericValue statusItem = EntityUtil.getFirst( delegator.findByAnd("StatusItem", UtilMisc.toMap("statusCode", data.get("partyStatus"), "statusTypeId", "WALLET_STATUS"), null, false) ); 
				if (UtilValidate.isEmpty(statusItem)) {
					setValidate(false);
					message = "E222";
					validationMessage.put("partyStatus", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("partyName"))) {
				setValidate(false);
				message = "E202";
				validationMessage.put("partyName", message);
			}
			
			if (UtilValidate.isEmpty(data.get("baseCurrency"))) {
				setValidate(false);
				message = "E201";
				validationMessage.put("baseCurrency", message);
			} else {
				GenericValue uom = EntityUtil.getFirst( delegator.findByAnd("Uom", UtilMisc.toMap("uomId", data.get("baseCurrency")), null, false) ); 
				if (UtilValidate.isEmpty(uom)) {
					setValidate(false);
					message = "E221";
					validationMessage.put("baseCurrency", message);
				}
			}
			
			if (!isValidate()) {
				
				response.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(GlobalConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(GlobalConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
