/**
 * 
 */
package org.groupfio.responsive.template.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.responsive.template.util.DataUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
public class PartyServiceImpl {

	private static final String MODULE = PartyServiceImpl.class.getName();
    
    public static Map updateParty(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String partyId = (String) context.get("partyId");
    	String partyName = (String) context.get("partyName");
    	String baseCurrency = (String) context.get("baseCurrency");
    	String description = (String) context.get("description");
    	String externalAppPartyRef = (String) context.get("externalAppPartyRef");
    	String partyStatus = (String) context.get("partyStatus");
    	
    	//Timestamp fromDate = (Timestamp) context.get("fromDate");
    	//Timestamp thruDate = (Timestamp) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		Map<String, Object> pgContext = new HashMap<String, Object>();
    		
    		pgContext.put("partyId", partyId);
    		pgContext.put("groupName", partyName);
    		pgContext.put("comments", description);
    		pgContext.put("preferredCurrencyUomId", baseCurrency);
    		//pgContext.put("statusId", DataUtil.getStatusId(delegator, partyStatus));
    		
    		pgContext.put("userLogin", userLogin);
    		
    		Map<String, Object> pgRes = dispatcher.runSync("updatePartyGroup", pgContext);
    		
    		if (ServiceUtil.isSuccess(pgRes)) {
    			
    			GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
    			
    			String currentStatus = DataUtil.getStatusId(delegator, partyStatus);
    			String previousStatus = party.getString("statusId");
    			
    			if (!previousStatus.equals(currentStatus)) {
    				GenericValue ps = delegator.makeValue("PartyStatus");
    				ps.put("statusId", currentStatus);
    				ps.put("partyId", partyId);
    				ps.put("statusDate", UtilDateTime.nowTimestamp());
    				ps.create();
    			}
    			
    			party.put("statusId", DataUtil.getStatusId(delegator, partyStatus));
    			party.put("description", description);
    			party.store();
    			
    			if (UtilValidate.isNotEmpty(externalAppPartyRef)) {
    				Map<String, Object> piContext = new HashMap<String, Object>();
        			
    				piContext.put("partyIdentificationTypeId", "ABC_EXT_REF");
    				piContext.put("partyId", partyId);
    				piContext.put("idValue", externalAppPartyRef);
        			
    				piContext.put("userLogin", userLogin);
        			
        			Map<String, Object> prRes = dispatcher.runSync("updatePartyIdentification", piContext);
        			
        			if (ServiceUtil.isSuccess(prRes)) {
        				Debug.logInfo("Update party identification for party#"+partyId, MODULE);
        			}
    			}
    			
    			result.put("partyId", partyId);
    			result.put("partyStatus", partyStatus);
    			
    		}
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated party.."));
    	
    	return result;
    	
    }
    
}
