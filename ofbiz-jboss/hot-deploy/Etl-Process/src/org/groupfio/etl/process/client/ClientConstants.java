/**
 * 
 */
package org.groupfio.etl.process.client;

/**
 * @author Group Fio
 *
 */
public class ClientConstants {

	public static final class ChannelAccessType {
        private ChannelAccessType() { }
        public static final String ACCESS_TYPE_PARTY = "PARTY";
        public static final String ACCESS_TYPE_WALLET = "WALLET";
    }
	
}
