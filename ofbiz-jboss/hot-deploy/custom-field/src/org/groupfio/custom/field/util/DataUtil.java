/**
 * 
 */
package org.groupfio.custom.field.util;

import java.util.Map;

import org.groupfio.custom.field.constants.CustomFieldConstants.GroupType;
import org.groupfio.custom.field.constants.CustomFieldConstants.SourceInvoked;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Sharif
 *
 */
public class DataUtil {
	
	private static String MODULE = DataUtil.class.getName();

	public static void prepareAppStatusData(Map<String, Object> data) {
		if (UtilValidate.isEmpty(data.get("sourceInvoked"))) {
			data.put("sourceInvoked", SourceInvoked.UNKNOWN);
		}
	}
	
	public static String getStatusId (Delegator delegator, String statusCode) {
		try {
			GenericValue statusItem = EntityUtil.getFirst( delegator.findByAnd("StatusItem", UtilMisc.toMap("statusTypeId", "CUSTOM_FIELD_STATUS", "statusCode", statusCode), null, false) );
			if (UtilValidate.isNotEmpty(statusItem)) {
				return statusItem.getString("statusId");
			}
		} catch (Exception e) {
			Debug.logError(e, MODULE);
		}
		return null;
	}
	
	public static String getFormatedValue (String value) {
		
		if (UtilValidate.isNotEmpty(value)) {
			value = value.toLowerCase().trim();
			value = value.substring(0, 1).toUpperCase() + value.substring(1);
			
			System.out.println(value);
		}
		return value;
	}
	
	public static String getSegmentationValueAssociatedEntityName (Delegator delegator, String groupId) {
		
		String entityName = "CustomFieldPartyClassification";
		
		try {
			if (UtilValidate.isNotEmpty(groupId)) {
				
				GenericValue customFieldGroup = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup", UtilMisc.toMap("groupId", groupId), null, false) );
				if (UtilValidate.isNotEmpty(customFieldGroup) && (UtilValidate.isNotEmpty(customFieldGroup.getString("isUseDynamicEntity")) && customFieldGroup.getString("isUseDynamicEntity").equals("Y"))) {
					groupId = DataUtil.getFormatedValue(groupId);
					entityName = "CustomFieldSeg" + groupId;
				}
				
				if (UtilValidate.isNotEmpty(customFieldGroup) && customFieldGroup.getString("groupType").equals(GroupType.ECONOMIC_METRIC)) {
					entityName = "PartyMetricIndicator";
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return entityName;
	}
	
	public static String getSegmentationValueAssociatedTrkEntityName (Delegator delegator, String groupId) {
		
		String entityName = "CustomFieldPartyClassificationTrk";
		
		try {
			if (UtilValidate.isNotEmpty(groupId)) {
				
				GenericValue customFieldGroup = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup", UtilMisc.toMap("groupId", groupId), null, false) );
				if (UtilValidate.isNotEmpty(customFieldGroup) && (UtilValidate.isNotEmpty(customFieldGroup.getString("isUseDynamicEntity")) && customFieldGroup.getString("isUseDynamicEntity").equals("Y"))) {
					groupId = DataUtil.getFormatedValue(groupId);
					entityName = "CustomFieldSegTrk" + groupId;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return entityName;
	}
	
	public static GenericValue getCampaign (Delegator delegator, String groupId) {
		try {
			if (UtilValidate.isNotEmpty(groupId)) {
				String[] arrOfStr = groupId.split("_", 2);
				if (UtilValidate.isNotEmpty(arrOfStr) && arrOfStr.length > 1) {
					String marketingCampaignId = arrOfStr[1];
					GenericValue campaign = EntityUtil.getFirst( delegator.findByAnd("MarketingCampaign", UtilMisc.toMap("marketingCampaignId", marketingCampaignId), null, false) );
					return campaign;
				}
			}
		} catch (Exception e) {
			Debug.logError(e, MODULE);
		}
		return null;
	}
	
}
