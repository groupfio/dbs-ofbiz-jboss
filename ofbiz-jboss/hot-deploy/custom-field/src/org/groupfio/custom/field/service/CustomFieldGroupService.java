/**
 * 
 */
package org.groupfio.custom.field.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.custom.field.constants.CustomFieldConstants.GroupType;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
public class CustomFieldGroupService {

	private static final String MODULE = CustomFieldGroupService.class.getName();
    
    public static Map createCustomFieldGroup(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	String groupName = (String) context.get("groupName");
    	String sequence = (String) context.get("sequence");
    	String hide = (String) context.get("hide");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		EntityCondition existCondition = EntityCondition.makeCondition(
    				UtilMisc.toList(
    						EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupId),
    		                EntityCondition.makeCondition("groupName", EntityOperator.EQUALS, groupName)
    						), EntityOperator.OR);
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findList("CustomFieldGroup", existCondition, null, null, null, false) );
    		
    		if (UtilValidate.isNotEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Attribute field group already exists!"));
    			return result;
    		}
    		
    		group = delegator.makeValue("CustomFieldGroup");
    		
    		group.put("groupType", GroupType.CUSTOM_FIELD);
    		
    		group.put("groupId", groupId);
    		group.put("groupName", groupName);
    		group.put("hide", UtilValidate.isNotEmpty(hide) ? hide : "N");
    		
    		group.put("sequence", UtilValidate.isNotEmpty(sequence) ? Long.parseLong(sequence) : new Long(1));
    		
    		group.create();
    		
    		result.put("groupId", groupId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Attribute field group.."));
    	
    	return result;
    	
    }
    
    public static Map updateCustomFieldGroup(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	String groupName = (String) context.get("groupName");
    	String sequence = (String) context.get("sequence");
    	String hide = (String) context.get("hide");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	result.put("groupId", groupId);
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Custom field group not exists!"));
    			return result;
    		}
    		
    		EntityCondition existCondition = EntityCondition.makeCondition(
    				UtilMisc.toList(
    						EntityCondition.makeCondition("groupId", EntityOperator.NOT_EQUAL, groupId),
    		                EntityCondition.makeCondition("groupName", EntityOperator.EQUALS, groupName)
    						), EntityOperator.AND);
        	
    		GenericValue existGroup = EntityUtil.getFirst( delegator.findList("CustomFieldGroup", existCondition, null, null, null, false) );
    		if (UtilValidate.isNotEmpty(existGroup)) {
    			result.putAll(ServiceUtil.returnError("Attribute field group name already exists exists!"));
    			return result;
    		}
    		
    		group.put("groupName", groupName);
    		group.put("hide", UtilValidate.isNotEmpty(hide) ? hide : "N");
    		
    		group.put("sequence", UtilValidate.isNotEmpty(sequence) ? Long.parseLong(sequence) : new Long(1));
    		
    		group.store();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Attribute field group.."));
    	
    	return result;
    	
    }
    
    public static Map deleteCustomFieldGroup(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Attribute field group not exists!"));
    			return result;
    		}
    		
    		List<GenericValue> customFields = delegator.findByAnd("CustomField", UtilMisc.toMap("groupId", groupId), null, false);
    		for (GenericValue customField : customFields) {
    			
    			Map<String, Object> customFieldRemoveContext = new HashMap<String, Object>();
        		
    			customFieldRemoveContext.put("customFieldId", customField.getString("customFieldId"));
    			customFieldRemoveContext.put("userLogin", userLogin);
        		
        		Map<String, Object> customFieldRemoveResult = dispatcher.runSync("customfield.deleteCustomField", customFieldRemoveContext);
    			
    		}
    		
    		group.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Attribute field group.."));
    	
    	return result;
    	
    }
    
}
