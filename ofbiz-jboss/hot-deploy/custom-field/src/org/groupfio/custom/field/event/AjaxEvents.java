/*
 * Copyright (c) Open Source Strategies, Inc.
 *
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.groupfio.custom.field.event;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.groupfio.custom.field.ResponseCodes;
import org.groupfio.custom.field.constants.CustomFieldConstants;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

import javolution.util.FastList;
import javolution.util.FastMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Utility class for making Ajax JSON responses.
 * @author Sharif Ul Islam
 */
public final class AjaxEvents {

    private AjaxEvents() { }

    private static final String MODULE = AjaxEvents.class.getName();

    public static String doJSONResponse(HttpServletResponse response, JSONObject jsonObject) {
        return doJSONResponse(response, jsonObject.toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Collection<?> collection) {
        return doJSONResponse(response, JSONArray.fromObject(collection).toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Map map) {
        return doJSONResponse(response, JSONObject.fromObject(map));
    }

    public static String doJSONResponse(HttpServletResponse response, String jsonString) {
        String result = "success";

        response.setContentType("application/x-json");
        try {
            response.setContentLength(jsonString.getBytes("UTF-8").length);
        } catch (UnsupportedEncodingException e) {
            Debug.logWarning("Could not get the UTF-8 json string due to UnsupportedEncodingException: " + e.getMessage(), MODULE);
            response.setContentLength(jsonString.length());
        }

        Writer out;
        try {
            out = response.getWriter();
            out.write(jsonString);
            out.flush();
        } catch (IOException e) {
            Debug.logError(e, "Failed to get response writer", MODULE);
            result = "error";
        }
        return result;
    }
    
    public static GenericValue getUserLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (GenericValue) session.getAttribute("userLogin");
    }

    /*************************************************************************/
    /**                                                                     **/
    /**                      Common JSON Requests                           **/
    /**                                                                     **/
    /*************************************************************************/
    
    @SuppressWarnings("unchecked")
	public static String getRoleConfigs(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String groupId = request.getParameter("groupId");
		String customFieldId = request.getParameter("customFieldId");

		Map<String, Object> operatorList = new HashMap<String, Object>();
		Map<String, Object> resp = new HashMap<String, Object>();

		JSONArray datas = new JSONArray();
		
		try {

			if (UtilValidate.isNotEmpty(groupId) || UtilValidate.isNotEmpty(customFieldId)) {
				
				Map<String, Object> params = new HashMap<String, Object>();
				if (UtilValidate.isNotEmpty(groupId)) {
					params.put("groupId", groupId);
				}
				if (UtilValidate.isNotEmpty(customFieldId)) {
					params.put("customFieldId", customFieldId);
				}
				
				List<GenericValue> roleConfigs = delegator.findByAnd("CustomFieldRoleConfig", params, UtilMisc.toList("sequenceNumber"), false);
				
				if (UtilValidate.isNotEmpty(roleConfigs)) {
					for (GenericValue roleConfig : roleConfigs) {
						
						JSONObject data = new JSONObject();
						
						data.put("roleConfigId", roleConfig.getString("customFieldRoleConfigId"));
						data.put("roleType", roleConfig.getRelatedOne("RoleType").get("description", locale));
						data.put("groupId", groupId);
						data.put("roleTypeId", roleConfig.getString("roleTypeId"));
						data.put("sequenceNumber", roleConfig.get("sequenceNumber"));
						
						datas.add(data);
						
					}
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		resp.put("data", datas);

		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String createRoleConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String isCompleteReset = request.getParameter("isCompleteReset");

		String groupId = request.getParameter("groupId");
		String customFieldId = request.getParameter("customFieldId");
		String roleTypeId = request.getParameter("roleTypeId");
		String sequenceNumber = request.getParameter("sequenceNumber");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(groupId) || UtilValidate.isNotEmpty(customFieldId)) {
				
				Map<String, Object> params = new HashMap<String, Object>();
				if (UtilValidate.isNotEmpty(groupId)) {
					params.put("groupId", groupId);
				}
				if (UtilValidate.isNotEmpty(customFieldId)) {
					params.put("customFieldId", customFieldId);
				}
				
				if (UtilValidate.isNotEmpty(isCompleteReset) && isCompleteReset.equals("Y")) {
					delegator.removeByAnd("CustomFieldRoleConfig", params);
				}
				
				params.put("roleTypeId", roleTypeId);
				
				List<GenericValue> roleConfigs = delegator.findByAnd("CustomFieldRoleConfig", params, null, false);
				
				if (UtilValidate.isNotEmpty(roleConfigs)) {
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "ALready assigned role..");
				} else {
					GenericValue roleConfig = delegator.makeValue("CustomFieldRoleConfig");
					
					String configId = delegator.getNextSeqId("CustomFieldRoleConfig");
		    		
					roleConfig.put("customFieldRoleConfigId", configId);
		    		
					roleConfig.put("groupId", groupId);
					roleConfig.put("customFieldId", customFieldId);
					
					roleConfig.put("roleTypeId", roleTypeId);
					
					roleConfig.put("sequenceNumber", UtilValidate.isNotEmpty(sequenceNumber) ? Long.parseLong(sequenceNumber) : new Long(1));
					
					roleConfig.create();
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully assigned role..");
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String removeRoleConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String roleConfigId = request.getParameter("roleConfigId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(roleConfigId)) {
				
				GenericValue roleConfig = EntityUtil.getFirst( delegator.findByAnd("CustomFieldRoleConfig", UtilMisc.toMap("customFieldRoleConfigId", roleConfigId), null, false) );
				
				if (UtilValidate.isNotEmpty(roleConfig)) {
					
					roleConfig.remove();
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully removed role config..");
					
				} else {
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Not found assigned role..");
				}
				
			}
			
		} catch (Exception e) {
			
			resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    @SuppressWarnings("unchecked")
	public static String getCustomFieldMultiValues(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String customFieldId = request.getParameter("customFieldId");

		Map<String, Object> resp = new HashMap<String, Object>();

		JSONArray datas = new JSONArray();
		
		try {

			if (UtilValidate.isNotEmpty(customFieldId)) {
				
				List<GenericValue> multiValues = delegator.findByAnd("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", customFieldId), UtilMisc.toList("sequenceNumber"), false);
				
				if (UtilValidate.isNotEmpty(multiValues)) {
					for (GenericValue multiValue : multiValues) {
						
						JSONObject data = new JSONObject();
						
						data.put("customFieldId", customFieldId);
						data.put("multiValueId", multiValue.getString("multiValueId"));
						data.put("fieldValue", multiValue.getString("fieldValue"));
						data.put("description", multiValue.getString("description"));
						data.put("hide", multiValue.get("hide"));
						data.put("sequenceNumber", UtilValidate.isEmpty(multiValue.get("sequenceNumber")) ? 1 : multiValue.get("sequenceNumber"));
						
						datas.add(data);
						
					}
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		resp.put("data", datas);

		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String createCustomFieldMultiValue(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String customFieldId = request.getParameter("mvCustomFieldId");
		String fieldValue = request.getParameter("fieldValue");
		String description = request.getParameter("description");
		String hide = request.getParameter("hide");
		String sequenceNumber = request.getParameter("mvSequenceNumber");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(customFieldId)) {
				
				GenericValue multiValue = EntityUtil.getFirst( delegator.findByAnd("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", customFieldId, "fieldValue", fieldValue), null, false) );
				
				if (UtilValidate.isNotEmpty(multiValue)) {
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Already created multi value..");
				} else {
					multiValue = delegator.makeValue("CustomFieldMultiValue");
					
					String multiValueId = delegator.getNextSeqId("CustomFieldMultiValue");
		    		
					multiValue.put("customFieldId", customFieldId);
					multiValue.put("multiValueId", multiValueId);
		    		
					multiValue.put("fieldValue", fieldValue);
					multiValue.put("description", description);
					multiValue.put("hide", hide);
					
					multiValue.put("sequenceNumber", UtilValidate.isNotEmpty(sequenceNumber) ? Long.parseLong(sequenceNumber) : new Long(1));
					
					multiValue.create();
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully created multi value..");
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
			resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, e.getMessage());
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String removeCustomFieldMultiValue(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String customFieldId = request.getParameter("customFieldId");
		String multiValueId = request.getParameter("multiValueId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(customFieldId) && UtilValidate.isNotEmpty(multiValueId)) {
				
				GenericValue multiValue = EntityUtil.getFirst( delegator.findByAnd("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", customFieldId, "multiValueId", multiValueId), null, false) );
				
				if (UtilValidate.isNotEmpty(multiValue)) {
					
					multiValue.remove();
					
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Successfully removed multi value..");
					
				} else {
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Not found role config..");
				}
				
			}
			
		} catch (Exception e) {
			
			resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String removeSelectedMultiValues(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String customFieldId = request.getParameter("customFieldId");
		String rowsSelected[] = request.getParameterValues("rowsSelected[]");

		Map<String, Object> resp = new HashMap<String, Object>();

		int successCount = 0;
		
		try {

			if (UtilValidate.isNotEmpty(customFieldId)) {
				
				GenericValue customField = EntityUtil.getFirst( delegator.findByAnd("CustomField", UtilMisc.toMap("customFieldId", customFieldId), null, false) );
				
				if (UtilValidate.isEmpty(customField)) {
					resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Invalid custom field..");
				} else {
					
					if (UtilValidate.isNotEmpty(rowsSelected)) {
						
						for (int i = 0; i < rowsSelected.length; i++) {
							String multiValueId = rowsSelected[i];
							GenericValue associatedEntity = EntityUtil.getFirst( delegator.findByAnd("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", customFieldId, "multiValueId", multiValueId), null, false) );
							
							if (UtilValidate.isNotEmpty(associatedEntity)) {
								
								associatedEntity.remove();
								
								successCount++;
							}
						}
						
					}
					
				}
				
			}
			
			resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			
			resp.put("successCount", successCount);
			
		} catch (Exception e) {
			
			resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
    public static String getCustomFieldGroupingCodes(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String groupType = request.getParameter("groupType");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	
        	Map<String, Object> requestParams = UtilMisc.toMap();
        	
        	List<Map<String, Object>> groupingCodeList = new ArrayList<Map<String, Object>>();
        	
    		if (UtilValidate.isNotEmpty(groupType)) {
    		
    			List<GenericValue> groupingCodes = delegator.findByAnd("CustomFieldGroupingCode", UtilMisc.toMap("groupType", groupType), UtilMisc.toList("sequenceNumber"), false);
        		
    			for (GenericValue groupingCode : groupingCodes) {
    				Map<String, Object> ser = new HashMap<String, Object>();
    				ser.put("customFieldGroupingCodeId", groupingCode.getString("customFieldGroupingCodeId"));
    				ser.put("groupingCode", groupingCode.getString("groupingCode"));
    				ser.put("description", groupingCode.getString("description"));
    				ser.put("sequenceNumber", groupingCode.getString("sequenceNumber"));
    				ser.put("groupType", groupingCode.getString("groupType"));
    				
    				groupingCodeList.add(ser);
    			}
    			
    		}
    		
        	resp.put("groupingCodes", groupingCodeList);
        	
        	resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
    @SuppressWarnings("unchecked")
    public static String getCustomFieldGroups(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String groupingCode = request.getParameter("groupingCode");
        String customFieldGroupingCodeId = request.getParameter("customFieldGroupingCodeId");

        String roleTypeId = request.getParameter("roleTypeId");
        String isActive = request.getParameter("isActive");
        String groupType = request.getParameter("groupType");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	
        	Map<String, Object> requestParams = UtilMisc.toMap();
        	
        	List<Map<String, Object>> groups = new ArrayList<Map<String, Object>>();
    		if (UtilValidate.isNotEmpty(groupingCode)) {
    		
        		GenericValue cfgc = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroupingCode", UtilMisc.toMap("groupingCode", groupingCode), null, false) );
        		
        		if (UtilValidate.isNotEmpty(cfgc)) {
        			requestParams.put("groupingCode", cfgc.getString("customFieldGroupingCodeId"));
        		}
    		}
    		
    		if (UtilValidate.isNotEmpty(customFieldGroupingCodeId)) {
    			requestParams.put("groupingCode", customFieldGroupingCodeId);
    		}
    		
    		if (UtilValidate.isNotEmpty(roleTypeId)) {
    			requestParams.put("roleTypeId", roleTypeId);
    		}
    		if (UtilValidate.isNotEmpty(isActive)) {
    			requestParams.put("isActive", isActive);
    		}
    		if (UtilValidate.isNotEmpty(groupType)) {
    			requestParams.put("groupType", groupType);
    		}
    		
			List<GenericValue> groupList = delegator.findByAnd("CustomFieldGroupSummary", requestParams, UtilMisc.toList("sequence"), false);
			for (GenericValue group : groupList) {
				Map<String, Object> ser = new HashMap<String, Object>();
				ser.put("groupId", group.getString("groupId"));
				ser.put("groupName", group.getString("groupName"));
				ser.put("sequence", group.getLong("sequence"));
				ser.put("hide", group.getString("hide"));
				ser.put("valueCapture", group.getString("valueCapture"));
				ser.put("customFieldGroupingCodeId", group.getString("groupingCode"));
				
				groups.add(ser);
			}
        	
        	resp.put("groups", groups);
        	
        	resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
    @SuppressWarnings("unchecked")
    public static String getCustomFields(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String groupId = request.getParameter("groupId");
        String isEnabled = request.getParameter("isEnabled");

        String hide = request.getParameter("hide");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	
        	Map<String, Object> requestParams = UtilMisc.toMap();
        	
        	List<Map<String, Object>> fields = new ArrayList<Map<String, Object>>();
        	
    		if (UtilValidate.isNotEmpty(groupId)) {
    			requestParams.put("groupId", groupId);
    		}
    		
    		if (UtilValidate.isNotEmpty(isEnabled)) {
    			requestParams.put("isEnabled", isEnabled);
    		}
    		if (UtilValidate.isNotEmpty(hide)) {
    			requestParams.put("hide", hide);
    		}
    		
			List<GenericValue> fieldList = delegator.findByAnd("CustomFieldSummary", requestParams, UtilMisc.toList("sequenceNumber"), false);
			for (GenericValue field : fieldList) {
				Map<String, Object> ser = new HashMap<String, Object>();
				ser.put("customFieldId", field.getString("customFieldId"));
				ser.put("groupId", field.getString("groupId"));
				ser.put("groupType", field.getString("groupType"));
				ser.put("groupName", field.getString("groupName"));
				ser.put("sequenceNumber", field.getLong("sequenceNumber"));
				ser.put("hide", field.getString("hide"));
				ser.put("isEnabled", field.getString("isEnabled"));
				ser.put("customFieldName", field.getString("customFieldName"));
				
				fields.add(ser);
			}
        	
        	resp.put("fields", fields);
        	
        	resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(CustomFieldConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(CustomFieldConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
    
    @SuppressWarnings("unused")
    public static String downloadFile(HttpServletRequest request, HttpServletResponse response) throws ComponentException {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        Locale locale = UtilHttp.getLocale(request);
        FileInputStream fis = null;
        Map < String, Object > resp = new HashMap < String, Object > ();
        String resourceName = request.getParameter("resourceName");
        String componentName = request.getParameter("componentName");
        String fileName = request.getParameter("fileName");
        String filePath = ComponentConfig.getRootLocation(componentName) + "webapp/"+resourceName+"/template/";
        OutputStream os = null;
        try {
            fis = new FileInputStream(filePath + fileName);
            byte b[];
            int x = fis.available();
            b = new byte[x];
            Debug.log(" b size" + b.length);
            fis.read(b);

            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "attachment; filename="+fileName);

            os = response.getOutputStream();
            os.write(b);
            os.flush();
            resp.put("fields", "Success");
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            resp.put("fields", "Failed");
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                 Debug.logError(e.getMessage(), MODULE);
            }
        }
        return doJSONResponse(response, resp);
    }
    public static String multiValueUpdate(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        String customFieldId = (String) request.getParameter("mvUpdateCustomFieldId");
        String currentFieldId = (String) request.getParameter("currentFieldId");
        if (UtilValidate.isNotEmpty(customFieldId) && UtilValidate.isNotEmpty(currentFieldId)) {
            String splitList[] = currentFieldId.split(",");
            for (String multiValueId: splitList) {
                if (UtilValidate.isNotEmpty(multiValueId)) {
                    String description = request.getParameter("description_" + multiValueId);
                    String hide = request.getParameter("hide_" + multiValueId);
                    String mvSequenceNumber = request.getParameter("mvSequenceNumber_" + multiValueId);
                    if (UtilValidate.isNotEmpty(description)) {
                        GenericValue customFieldMultiValueGV = delegator.findOne("CustomFieldMultiValue", UtilMisc.toMap("customFieldId", customFieldId, "multiValueId", multiValueId), false);
                        if (customFieldMultiValueGV != null && customFieldMultiValueGV.size() > 0) {
                            customFieldMultiValueGV.set("description", description);
                            customFieldMultiValueGV.set("hide", hide);
                            customFieldMultiValueGV.put("sequenceNumber", UtilValidate.isNotEmpty(mvSequenceNumber) ? Long.parseLong(mvSequenceNumber) : new Long(1));
                            customFieldMultiValueGV.store();
                        }
                    }
                }
            }

        }
        return doJSONResponse(response, UtilMisc.toMap("data", null));
    }
    public static String economicMetricErrorLogs(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
        String requestId = (String) request.getParameter("requestId");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        Long totalCount = Long.valueOf("0");
        Map < String, Object > returnMap = FastMap.newInstance();
        List < Object > economicImportErrorLogs = FastList.newInstance();
        try {
            if (UtilValidate.isNotEmpty(requestId)) {
                conditionList.add(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("requestId", EntityOperator.EQUALS, requestId),
                    EntityCondition.makeCondition("customFieldType", EntityOperator.EQUALS, "ECONOMIC_METRIC")
                ), EntityOperator.AND));
                EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
                totalCount = EntityQuery.use(delegator).from("CustomFieldSegmentImportTemp").where(mainConditons).queryCount();
                if (totalCount != null && totalCount > 0) {
                    EntityFindOptions efo = new EntityFindOptions();
                    efo.setDistinct(true);
                    int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                    int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                    efo.setOffset(startInx);
                    efo.setLimit(endInx);
                    List < GenericValue > economicImportList = delegator.findList("CustomFieldSegmentImportTemp", mainConditons, null, null, efo, false);
                    if (economicImportList != null && economicImportList.size() > 0) {
                        for (GenericValue economicImportGV: economicImportList) {
                            Map < String, Object > economicImportMap = FastMap.newInstance();
                            economicImportMap.put("partyId", economicImportGV.getString("partyId"));
                            economicImportMap.put("customFieldGroupId", economicImportGV.getString("customFieldGroupId"));
                            economicImportMap.put("segmentValueId", economicImportGV.getString("segmentValueId"));
                            economicImportMap.put("metricValue", economicImportGV.getString("metricValue"));
                            economicImportMap.put("message", economicImportGV.getString("message"));
                            economicImportMap.put("requestId", economicImportGV.getString("requestId"));
                            economicImportErrorLogs.add(economicImportMap);
                        }
                    }
                }
            }
            returnMap.put("data", economicImportErrorLogs);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", totalCount);
            returnMap.put("recordsFiltered", totalCount);
        } catch (Exception e) {
            returnMap.put("data", economicImportErrorLogs);
            returnMap.put("draw", draw);
            returnMap.put("recordsTotal", 0);
            returnMap.put("recordsFiltered", 0);
            Debug.logError("Exception in Economic Metric Import Error Logs" + e.getMessage(), MODULE);
            return AjaxEvents.doJSONResponse(response, returnMap);
        }
        return AjaxEvents.doJSONResponse(response, returnMap);
    }
    
    @SuppressWarnings("unused")
    public static String downloadEconomicErrorLogs(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        Locale locale = UtilHttp.getLocale(request);
        Map < String, Object > resp = new HashMap < String, Object > ();
        String requestId = request.getParameter("requestId");

        if (UtilValidate.isNotEmpty(requestId)) {
            try (DataOutputStream outputStream = new DataOutputStream(response.getOutputStream())){
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=\"Economic Metric Error Logs.csv\"");

                List < EntityCondition > conditionList = new ArrayList < EntityCondition > ();
                conditionList.add(EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("requestId", EntityOperator.EQUALS, requestId),
                    EntityCondition.makeCondition("customFieldType", EntityOperator.EQUALS, "ECONOMIC_METRIC")
                ), EntityOperator.AND));
                EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
                List < GenericValue > customFieldSegmentImportTempList = delegator.findList("CustomFieldSegmentImportTemp", mainConditons, null, null, null, false);

                String outputResult = "Party Id, Economic Code Id, Metric Id, Metric Value, Message\n";
                outputStream.write(outputResult.getBytes());
                if (customFieldSegmentImportTempList != null && customFieldSegmentImportTempList.size() > 0) {
                    for (GenericValue customFieldSegmentImportTemp: customFieldSegmentImportTempList) {
                        String partyId = "";
                        String codeId = "";
                        String metricId = "";
                        String metricValue = "";
                        String message = "";
                        partyId = customFieldSegmentImportTemp.getString("partyId");
                        codeId = customFieldSegmentImportTemp.getString("customFieldGroupId");
                        metricId = customFieldSegmentImportTemp.getString("segmentValueId");
                        metricValue = customFieldSegmentImportTemp.getString("metricValue");
                        message = customFieldSegmentImportTemp.getString("message");
                        outputResult = partyId + "," + codeId + "," + metricId + "," + metricValue + "," + message + "\n";
                        outputStream.write(outputResult.getBytes());

                    }
                } else {
                    outputResult = "No data available in table\n";
                    outputStream.write(outputResult.getBytes());
                }
                resp.put("fields", "Success");
                outputStream.flush();
            } catch (Exception e) {
                Debug.logError(e.getMessage(), MODULE);
                resp.put("fields", "Failed");
            } 
        }
        return doJSONResponse(response, resp);
    }
    
    public static String checkCampaignTemplate(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        Locale locale = UtilHttp.getLocale(request);
        Map < String, Object > resp = new HashMap < String, Object > ();
        String campaignId = request.getParameter("campaignId");
        String flag = "N";
        if (UtilValidate.isNotEmpty(campaignId)) {
            try {
            	GenericValue marketingCampaign = delegator.findOne("MarketingCampaign", UtilMisc.toMap("marketingCampaignId",campaignId), false);
            	if(marketingCampaign != null && marketingCampaign.size() > 0) {
            		String templateId = marketingCampaign.getString("campaignTemplateId");
            		if(UtilValidate.isNotEmpty(templateId)) {
            			flag = "Y";
            		}
            	}
            } catch (Exception e) {
                Debug.logError(e.getMessage(), MODULE);
            } 
        }
        resp.put("flag", flag);
        return doJSONResponse(response, resp);
    }
}
