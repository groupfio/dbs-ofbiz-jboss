/**
 * 
 */
package org.groupfio.custom.field.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
public class GroupingCodeService {

	private static final String MODULE = GroupingCodeService.class.getName();
    
    public static Map createGroupingCode(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupingCode = (String) context.get("groupingCode");
    	String description = (String) context.get("description");
    	String sequenceNumber = (String) context.get("sequenceNumber");
    	String groupType = (String) context.get("groupType");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue code = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroupingCode", UtilMisc.toMap("groupingCode", groupingCode), null, false) );
    		
    		if (UtilValidate.isNotEmpty(code)) {
    			result.putAll(ServiceUtil.returnError("Grouping Code already exists!"));
    			return result;
    		}
    		
    		code = delegator.makeValue("CustomFieldGroupingCode");
    		
    		String customFieldGroupingCodeId = delegator.getNextSeqId("CustomFieldGroupingCode");
    		
    		code.put("customFieldGroupingCodeId", customFieldGroupingCodeId);
    		code.put("groupingCode", groupingCode);
    		code.put("description", description);
    		
    		code.put("groupType", groupType);
    		
    		code.put("sequenceNumber", UtilValidate.isNotEmpty(sequenceNumber) ? Long.parseLong(sequenceNumber) : new Long(1));
    		
    		code.create();
    		
    		result.put("groupingCodeId", customFieldGroupingCodeId);
    		result.put("groupType", groupType);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Grouping Code.."));
    	
    	return result;
    	
    }
    
    public static Map updateGroupingCode(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupingCodeId = (String) context.get("groupingCodeId");
    	
    	String groupingCode = (String) context.get("groupingCode");
    	String description = (String) context.get("description");
    	String sequenceNumber = (String) context.get("sequenceNumber");
    	String groupType = (String) context.get("groupType");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	result.put("groupingCodeId", groupingCodeId);
    	result.put("groupType", groupType);
    	
    	try {
        	
    		GenericValue code = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroupingCode",UtilMisc.toMap("customFieldGroupingCodeId", groupingCodeId), null, false) );
    		
    		if (UtilValidate.isEmpty(code)) {
    			result.putAll(ServiceUtil.returnError("Grouping Code not exists!"));
    			return result;
    		}
    		
    		code.put("groupingCode", groupingCode);
    		code.put("description", description);
    		
    		code.put("sequenceNumber", UtilValidate.isNotEmpty(sequenceNumber) ? Long.parseLong(sequenceNumber) : new Long(1));
    		
    		code.store();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Grouping Code.."));
    	
    	return result;
    	
    }
    
    public static Map deleteGroupingCode(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupingCodeId = (String) context.get("groupingCodeId");
    	String groupType = (String) context.get("groupType");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue code = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroupingCode",UtilMisc.toMap("customFieldGroupingCodeId", groupingCodeId), null, false) );
    		
    		if (UtilValidate.isEmpty(code)) {
    			result.putAll(ServiceUtil.returnError("Grouping Code not exists!"));
    			return result;
    		}
    		
    		if (UtilValidate.isEmpty(groupType)) {
    			groupType = code.getString("groupType");
    		}
    		result.put("groupType", groupType);
    		
    		List<GenericValue> segmentCodeList = delegator.findByAnd("CustomFieldGroup", UtilMisc.toMap("groupingCode", code.getString("groupingCode")), null, false);
    		if (UtilValidate.isNotEmpty(segmentCodeList)) {
    			for (GenericValue segmentCode : segmentCodeList) {
        			segmentCode.put("groupingCode", null);
        		}
    			delegator.storeAll(segmentCodeList);
    		}
    		
    		code.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Grouping Code.."));
    	
    	return result;
    	
    }
    
}
