<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.Update} ${uiLabelMap.SegmentCode} [ ${customFieldGroup.groupName} ]</h1>
	<div class="float-right">
		<#-- <a href="/cf-resource/data/dynamic-entitymodel.sql;jsessionid=${session.id}" class="btn btn-xs btn-primary m5 tooltips" title="Download Dynamic Entity Creation Script" download>Download SQL</a> -->
		<a href="viewSegmentValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="View Segment Values for ${customFieldGroup.groupName!}" >View Segment Values</a>
		<a href="segmentValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Add Segment Values for ${customFieldGroup.groupName!}" >Add Segment Values</a>
		<#if customFieldGroup.isActive?has_content && customFieldGroup.isActive == "Y" >
			<a href="#" class="btn btn-xs btn-primary m5 tooltips active-btn" title="Inactive Segment Code" data-isActive="N">
				Inactive
			</a>
		<#else>
			<a href="#" class="btn btn-xs btn-primary m5 tooltips active-btn" title="Active Segment Code" data-isActive="Y">
				Active
			</a>
		</#if>
		
	</div>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>updateSegmentCode</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
						
			<div class="form-body">
			
			<@readonlyInput 
				id="groupId"
				label=uiLabelMap.segmentCodeId
				value=customFieldGroup.groupId
				isHiddenInput=true
				/>
			
			<@generalInput 
				id="groupName"
				label=uiLabelMap.segmentCodeName
				placeholder=uiLabelMap.segmentCodeName
				value=customFieldGroup.groupName
				required=true
				maxlength=255
				/>
			
			<#-- <@dropdownInput 
				id="roleTypeId"
				label=uiLabelMap.roleTypeId
				options=roleTypeList
				required=true
				value=customFieldGroup.roleTypeId
				allowEmpty=false
				dataLiveSearch=true
				/> -->							
				
			<@dropdownInput 
				id="groupingCode"
				label=uiLabelMap.groupingCode
				options=groupingCodeList
				required=false
				value=customFieldGroup.groupingCode
				allowEmpty=true
				dataLiveSearch=true
				/>	
			<#-- 	
			<@generalInput 
				id="serviceName"
				label=uiLabelMap.serviceName
				placeholder=uiLabelMap.serviceName
				value=customFieldGroup.serviceName
				required=false
				maxlength=255
				/>
			 -->		
			
			<@dropdownInput 
				id="serviceTypeId"
				label=uiLabelMap.serviceTypeId
				options=serviceTypeList
				required=fales
				value=customFieldGroup.serviceTypeId
				allowEmpty=true
				/>
			
			<input type="hidden" id="configuredServiceId" value="${customFieldGroup.serviceConfigId!}">
			<div class="form-group row">
			   <label class="col-sm-4 col-form-label " for="serviceName">${uiLabelMap.serviceName!}</label>
			   <div class="col-sm-7">
			      <div class="input-icon ">
			         <select class="custom-select ui dropdown search form-control input-sm" id="serviceConfigId" name="serviceConfigId">
				      	<option value="" data-content="<span class='nonselect'>Please Select</span>" selected>Please Select</option>
				      </select>
			         <div class="help-block with-errors" id="serviceConfigId_error"></div>
			         <i class=""></i>
			      </div>
			   </div>
			</div>			
												
			<#-- 			
			<@dropdownInput 
				id="microServiceConfigId"
				label=uiLabelMap.microService
				options=microServiceList
				required=false
				value=customFieldGroup.microServiceConfigId
				allowEmpty=true
				/>																																																																																															
			
			<@dropdownInput 
				id="webhookConfigId"
				label=uiLabelMap.webhook
				options=webhookList
				required=false
				value=customFieldGroup.webhookConfigId
				allowEmpty=true
				/>							
			 -->
			<@dropdownInput 
				id="historicalCapture"
				label=uiLabelMap.historicalCapture
				options=yesNoOptions
				required=fales
				value=customFieldGroup.historicalCapture
				allowEmpty=true
				disabled=true
				/>																																																																							
			
			<@dropdownInput 
				id="valueCapture"
				label=uiLabelMap.valueCapture
				options=valueCaptureList
				required=true
				value=customFieldGroup.valueCapture
				allowEmpty=true
				/>			
				
			<@dropdownInput 
				id="isCampaignUse"
				label=uiLabelMap.isCampaignUse
				options=yesNoOptions
				required=fales
				value=customFieldGroup.isCampaignUse
				allowEmpty=true
				/>																																																																																																																																																																																																																																																																																																																																																																																																																						
			
			<#-- <@dropdownInput 
				id="classType"
				label=uiLabelMap.classType
				options=classTypeList
				required=fales
				value=customFieldGroup.classType
				allowEmpty=true
				/> -->																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			
			
			<@dropdownInput 
				id="type"
				label=uiLabelMap.type
				options=typeList
				required=fales
				value=customFieldGroup.type
				allowEmpty=true
				/>	
				
			<@readonlyInput 
				id="isUseDynamicEntity"
				label=uiLabelMap.isUseDynamicEntity
				value=customFieldGroup.isUseDynamicEntity
				isHiddenInput=true
				/>	
				
			<@generalInput 
				id="sequence"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customFieldGroup.sequence
				inputType="number"
				required=false
				min=1
				/>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																									
			</div>
			
			<@fromCommonAction showCancelBtn=true cancelUrl="findSegmentCode"/>
			
		</form>			
							
		</div>
			
	</div>
	
	<div class="col-md-6 col-sm-6">
	
		<div class="page-header">
			<h2 class="float-left">${uiLabelMap.AssignRole!}</h2>
		</div>
		
		<div class="portlet-body form">
			<form id="createRoleConfigForm" role="form" class="form-horizontal" action="<@ofbizUrl>createRoleConfig</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
			
			<input type="hidden" name="groupId" value="${customFieldGroup.groupId!}" />
			<input type="hidden" name="isCompleteReset" value="Y" />
						
			<div class="form-body">
			
			<@dropdownInput 
				id="roleTypeId"
				label=uiLabelMap.roleTypeId
				options=roleTypeList
				required=true
				value=roleConfig?if_exists
				allowEmpty=true
				dataLiveSearch=true
				/>	
				
			</div>
			
			<div class="clearfix"> </div>
			<div class="col-md-12 col-sm-12">
				<div class="form-group row">
					<div class="offset-sm-4 col-sm-1">
						<button id="roleConfigBtn" type="button" class="btn btn-sm btn-primary mt">Assign</button>
					</div>
					<div class="col-sm-6 mt-3 ml-3">
						<div class="alert-danger small">Assign new Role will be replace with existing Role!!!</div>
					</div>
				</div>
			</div>
			
			</form>			
							
		</div>
		
			
	</div>
	
</div>

<script>

jQuery(document).ready(function() {

$('.active-btn').click(function () {

	resetDefaultEvents();

	//alert($(this).attr("data-isActive"));
	var actionType = $(this).attr("data-isActive");
	var actionUrl = "";
	if (actionType == "Y") {
		actionUrl = "activateSegmemntCode";
	} else if (actionType == "N") {
		actionUrl = "inActivateSegmemntCode";
	}
	
	// title="In Active Segment Code" data-isActive="N"
	
	$.ajax({
			      
		type: "POST",
     	url: actionUrl,
        data:  {"groupId": "${customFieldGroup.groupId!}"},
        success: function (data) {   
            
            if (data.code == 200) {
				showAlert ("success", data.message);
				
				if (actionType == "Y") {
					$('.active-btn').html("Inactive");
					$('.active-btn').attr("title", "Inactive Segment Code");
					$('.active-btn').attr("data-original-title", "Inactive Segment Code");
					$('.active-btn').attr("data-isActive", "N");
				} else {
					$('.active-btn').html("Active");
					$('.active-btn').attr("title", "Active Segment Code");
					$('.active-btn').attr("data-original-title", "Active Segment Code");
					$('.active-btn').attr("data-isActive", "Y");
				}
				
			} else {
				showAlert ("error", returnedData.message);
			}
			    	
        }
        
	});    

});

$('#roleConfigBtn').click(function () {

	if ($('#roleTypeId').val()) {
		$.post('createRoleConfig', $('#createRoleConfigForm').serialize(), function(returnedData) {
	
			if (returnedData.code == 200) {
				
				showAlert ("success", returnedData.message)
				
				$('#createRoleConfigForm')[0].reset();
				findRoleConfigs();
				
			} else {
				showAlert ("error", returnedData.message)
			}
				
		});
	} else {
		showAlert ("error", "fill up all the values");
	}
	
});

if ($("#serviceTypeId").val()) {
	loadServiceList();
}

$("#serviceTypeId").change(function() {
	
	loadServiceList();
		
});

findRoleConfigs();

});

function loadServiceList() {
	var nonSelectContent = "<span class='nonselect'>Please Select</span>";
	var serviceNameOptions = '<option value="" data-content="'+nonSelectContent+'" selected="">Please Select</option>';		
		
	if ( $("#serviceTypeId").val() == "INTERNAL" || $("#serviceTypeId").val() == "WEBHOOK_PUSH" ) {
		
		$.ajax({
			      
			type: "POST",
	     	url: "getCustomFieldGroupServices",
	        data:  {"serviceTypeId": $("#serviceTypeId").val()},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.services.length; i++) {
	            		var service = data.services[i];
	            		serviceNameOptions += '<option value="'+service.serviceConfigId+'">'+service.description+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
	}
	
	$("#serviceConfigId").html( serviceNameOptions );
	
	if ( $("#configuredServiceId").val() ) {
		$("#serviceConfigId").val( $("#configuredServiceId").val() );
	}
	
	$('#serviceConfigId').dropdown('refresh');
}

function findRoleConfigs(){

	 $.ajax({
     url: "getRoleConfigs?groupId=${customFieldGroup.groupId!}",
     cache: false,
     success: function(data, type, row, meta){
     var config = row.roleConfigId;
     $("#roleTypeId").append(config);
     }
  });
	
}	
	
	/*
	$('#role-config-list').DataTable( {
	    "processing": true,
	    "destroy": true,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "pageLength": 10,
        "order": [[ 1, "asc" ]],
        "columns": [
            { "data": "roleType" },
            { "data": "sequenceNumber" },
            { "data": "roleTypeId",
	          "render": function(data, type, row, meta){
	            if(type === 'display'){
	                data = 
	                '<div class="text-center">' +
	                	'<a class="btn btn-xs btn-danger tooltips remove-role-config" href="javascript:removeRoleConfig('+row.roleConfigId+')" data-original-title="Remove" data-config-id="'+row.roleConfigId+'"><i class="fa fa-times red"></i></a>' +
	                '</div>';
	            }
	            return data;
	         }
	      	},
        ],
        "fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});*/


function removeRoleConfig (roleConfigId) {
	
	//alert(roleConfigId);

	$.ajax({
			      
		type: "POST",
     	url: "removeRoleConfig",
        data:  {"roleConfigId": roleConfigId},
        success: function (data) {   
            
            findRoleConfigs();
			    	
        }
        
	});    
	
}

</script>
