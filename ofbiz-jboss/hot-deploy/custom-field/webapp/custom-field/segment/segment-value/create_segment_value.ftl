<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.Create} ${uiLabelMap.SegmentValue} <#if customFieldGroup.groupName?has_content>for [ ${customFieldGroup.groupName} ]</#if> </h1>
	<div class="float-right">
		<#if customFieldGroup.groupId?has_content>
		<#-- <a href="findSegmentValue" class="btn btn-xs btn-primary m5 tooltips" title="Cancel" >Cancel</a> -->
		<a href="viewSegmentValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Back to ${customFieldGroup.groupName!}" >Back</a>
		</#if>
	</div>
</div>

<#if customFieldGroup.groupId?has_content>
	<#assign actionUrl = "createSegmentValue"/>
<#else>
	<#assign actionUrl = "createSegmentValueIndividual"/>
</#if>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form id="segment-value-form" role="form" class="form-horizontal" action="<@ofbizUrl>${actionUrl}</@ofbizUrl>" encType="multipart/form-data" method="post" >
				
			<div class="form-body">
			
			<#if groupId?has_content>	
			<input type="hidden" id="groupId" name="groupId" value="${customField.groupId!}" />					
			<#-- <@readonlyInput 
				id="groupId"
				label=uiLabelMap.segmentCode
				value=customField.groupId
				isHiddenInput=true
				/> -->
			<#else>	
			<@dropdownInput 
				id="groupId"
				label=uiLabelMap.segmentCode
				options=groupList
				required=true
				value=customField.groupId
				allowEmpty=true
				dataLiveSearch=true
				/>
			</#if>
			
			<@generalInput 
				id="customFieldId"
				label=uiLabelMap.segmentValueId
				placeholder=uiLabelMap.segmentValueId
				value=customField.customFieldId
				required=true
				maxlength=250
				/>		
									
			<@generalInput 
				id="customFieldName"
				label=uiLabelMap.segmentValueName
				placeholder=uiLabelMap.segmentValueName
				value=customField.customFieldName
				required=true
				maxlength=255
				/>	
			
			<@readonlyInput 
				id="valueCapture"
				label=uiLabelMap.valueCapture
				value=customFieldGroup.valueCapture
				isHiddenInput=true
				/>
				
			<@dropdownInput 
				id="isEnabled"
				label=uiLabelMap.isEnabled
				options=yesNoOptions
				required=fales
				value=customField.isEnabled
				allowEmpty=true
				/>		
			
			<#-- <div class="page-header">
				<h2>${uiLabelMap.Configuration} ${uiLabelMap.SegmentValue} <span id="value-capture"></span></h2>
			</div> -->
			
			<#-- <input type="hidden" name="valueCapture" value="${customFieldGroup.valueCapture!}" /> -->
			<input type="hidden" name="valueSeqNum" value="1" />		
			
			<div id="range-value-config" style="display: none"> 
			
			<@generalInput 
				id="valueMin"
				label=uiLabelMap.valueMin
				placeholder=uiLabelMap.valueMin
				value=valueConfig.valueMin
				required=true
				inputType="number"
				maxlength=20
				/>	
				
			<@generalInput 
				id="valueMax"
				label=uiLabelMap.valueMax
				placeholder=uiLabelMap.valueMax
				value=valueConfig.valueMax
				required=true
				inputType="number"
				maxlength=20
				/>
			
			</div>
						
			<div id="single-value-config" style="display: none">		
			
			<@generalInput 
				id="valueData"
				label=uiLabelMap.valueData
				placeholder=uiLabelMap.valueData
				value=valueConfig.valueData
				required=true
				/>	
																								
			</div>
			
			<@generalInput 
				id="sequenceNumber"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customField.sequenceNumber
				inputType="number"
				required=false
				min=1
				/>		
																																																																																																																																																																																																												
			</div>
			
			<@fromCommonAction showCancelBtn=false showClearBtn=true/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>
	
<script>

jQuery(document).ready(function() { 

$('#segment-value-form').validator();

loadSegmentCode();

$('#groupId').on('change', function(){

	loadSegmentCode();

});
	
});

function loadSegmentCode() {
	
	$("#single-value-config").hide();
	$("#range-value-config").hide();
	
	var groupId = $("#groupId").val();
	
	if (groupId) {
	
		//$("#segment-value-form").validator('destroy');
	
		$.ajax({
		      
			type: "POST",
	     	url: "loadSegmentCode",
	        data:  {"groupId": groupId},
	        success: function (data) {   
	            
	            if (data.code == 200) {
					
					if (data.segmentCode.valueCapture == "SINGLE") {
						$("#single-value-config").show();
					} else if (data.segmentCode.valueCapture == "MULTIPLE") {
						
					} else if (data.segmentCode.valueCapture == "RANGE") {
						$("#range-value-config").show();
					}
					
					$("#valueCapture").html( "[ " + data.segmentCode.valueCapture + " ]" );
					
					$("#segment-value-form").validator('update');
					
				} else {
					//showAlert ("error", data.message);
				}  
				    	
	        }
	        
		});
	}
	
	
	
}

</script>
