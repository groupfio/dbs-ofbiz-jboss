<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Find} ${uiLabelMap.SegmentValue}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" class="form-horizontal" data-toggle="validator">
      <div class="row">
      	
      	<div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="groupingCode"
				options=groupingCodeList
				required=false
				value=customField.groupingCode
				allowEmpty=true
				tooltip = uiLabelMap.groupingCode
				emptyText = uiLabelMap.groupingCode
				dataLiveSearch=true
				/>
         </div>
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="groupId"
				options=groupList
				required=false
				value=customField.groupId
				allowEmpty=true
				emptyText = uiLabelMap.segmentCode
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="customFieldId"
				placeholder=uiLabelMap.segmentValueId
				value=customField.customFieldId
				tooltip = uiLabelMap.segmentValueId
				required=false
				maxlength=255
				/>		
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="customFieldName"
				placeholder=uiLabelMap.segmentValueName
				value=customField.customFieldName
				tooltip = uiLabelMap.segmentValueName
				required=false
				maxlength=255
				/>		
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="isEnabled"
				options=yesNoOptions
				required=false
				value=customField.isEnabled
				allowEmpty=true
				emptyText = uiLabelMap.isEnabled
				/>	
         </div>
         
         <@fromSimpleAction id="" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {	

	loadSegmentCodeList();
	$("#groupingCode").change(function() {
		loadSegmentCodeList()
	});

});

function loadSegmentCodeList() {
	var nonSelectContent = "<span class='nonselect'>Select ${uiLabelMap.segmentCode!}</span>";
	var groupNameOptions = '<option value="" data-content="'+nonSelectContent+'" selected="">Select ${uiLabelMap.segmentCode!}</option>';		
		
	//if ( $("#groupingCode").val() ) {
		
		$.ajax({
			      
			type: "POST",
	     	url: "getCustomFieldGroups",
	        data:  {"groupingCode": $("#groupingCode").val()},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.groups.length; i++) {
	            		var group = data.groups[i];
	            		groupNameOptions += '<option value="'+group.groupId+'">'+group.groupName+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
		$("#groupId").html( groupNameOptions );
		
		<#if customField.groupId?has_content>
		$("#groupId").val( "${customField.groupId}" );
		</#if>
	
		$('#groupId').dropdown('refresh');
	//}
		
}
		
</script>

