<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.Update} ${uiLabelMap.SegmentValue} <#if customFieldGroup.groupName?has_content>for [ ${customFieldGroup.groupName} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${customField.customFieldName} ]</#if> </h1>
	<div class="float-right">
		<a href="findSegmentValue" class="btn btn-xs btn-primary m5 tooltips" title="Cancel" >Cancel</a>
		
		<#if customFieldGroup.groupId?has_content>
		<a href="editSegmentCode?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Back to ${customFieldGroup.groupName!}" >Back</a>
		</#if>
		
		<#if customField.isEnabled?has_content && customField.isEnabled == "Y" >
			<a href="#" class="btn btn-xs btn-primary m5 tooltips active-btn" title="Disable Segment Value" data-isEnabled="N">
				Disable
			</a>
		<#else>
			<a href="#" class="btn btn-xs btn-primary m5 tooltips active-btn" title="Enable Segment Value" data-isEnabled="Y">
				Enable
			</a>
		</#if>
		<a href="segmentValueCustomer?customFieldId=${customField.customFieldId}&groupId=${customFieldGroup.groupId!}" class="btn btn-xs btn-primary m5 tooltips <#if !customFieldGroup.groupId?has_content>disabled</#if>" title="${uiLabelMap.ManageCustomers!}" >${uiLabelMap.ManageCustomers!}</a>
	</div>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form id="segment-value-form" role="form" class="form-horizontal" action="<@ofbizUrl>updateSegmentValue</@ofbizUrl>" encType="multipart/form-data" method="post" >
					
			<div class="form-body">
			
			<#-- <@dropdownInput 
				id="roleTypeId"
				label=uiLabelMap.roleTypeId
				options=roleTypeList
				required=true
				value=customField.roleTypeId
				allowEmpty=false
				/> -->
							
			<#if groupId?has_content>	
			<input type="hidden" id="groupId" name="groupId" value="${customField.groupId!}" />						
			<#-- <@readonlyInput 
				id="groupId"
				label=uiLabelMap.segmentCode
				value=customField.groupId
				isHiddenInput=true
				/> -->
			<#else>	
			<@dropdownInput 
				id="groupId"
				label=uiLabelMap.segmentCode
				options=groupList
				required=false
				value=customField.groupId
				allowEmpty=true
				/>
			</#if>
			
			<@readonlyInput 
				id="customFieldId"
				label=uiLabelMap.segmentValueId
				value=customField.customFieldId
				isHiddenInput=true
				/>
			<#-- 
			<@generalInput 
				id="customFieldId"
				label=uiLabelMap.segmentValueId
				placeholder=uiLabelMap.segmentValueId
				value=customField.customFieldId
				required=true
				maxlength=20
				/>	
			 -->
			<@generalInput 
				id="customFieldName"
				label=uiLabelMap.segmentValueName
				placeholder=uiLabelMap.segmentValueName
				value=customField.customFieldName
				required=true
				maxlength=255
				/>
				
			<@readonlyInput 
				id="valueCapture"
				label=uiLabelMap.valueCapture
				value=customFieldGroup.valueCapture
				isHiddenInput=true
				/>	
						
			<@dropdownInput 
				id="isEnabled"
				label=uiLabelMap.isEnabled
				options=yesNoOptions
				required=fales
				value=customField.isEnabled
				allowEmpty=true
				/>		
				
			<#if groupId?has_content>
					
			<#-- <div class="page-header"> 
				<h2>${uiLabelMap.Configuration} ${uiLabelMap.SegmentValue}</h2>
			</div> -->
			
			<#-- <input type="hidden" name="valueCapture" value="${customFieldGroup.valueCapture!}" /> -->
			<input type="hidden" name="valueSeqNum" value="1" />	
			
			<div id="range-value-config" style="display: none"> 
			
			<@generalInput 
				id="valueMin"
				label=uiLabelMap.valueMin
				placeholder=uiLabelMap.valueMin
				value=valueConfig.valueMin
				required=true
				maxlength=20
				/>	
				
			<@generalInput 
				id="valueMax"
				label=uiLabelMap.valueMax
				placeholder=uiLabelMap.valueMax
				value=valueConfig.valueMax
				required=true
				maxlength=20
				/>
			
			</div>	
			
			<div id="single-value-config" style="display: none">			
												
			<@generalInput 
				id="valueData"
				label=uiLabelMap.valueData
				placeholder=uiLabelMap.valueData
				value=valueConfig.valueData
				required=true
				/>		
				
			</#if>	
			
			</div>		
			
			<@generalInput 
				id="sequenceNumber"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customField.sequenceNumber
				inputType="number"
				required=false
				min=1
				/>																																																																											
			 																																																																																																																																																																																																																																																	
			</div>
			
			<#if groupId?has_content>
				<#assign cancelActionUrl = "viewSegmentValueForGroup"/>
			<#else>
				<#assign cancelActionUrl = "findSegmentValue"/>
			</#if>
			
			<@fromCommonAction showCancelBtn=true cancelUrl="${cancelActionUrl}?groupId=${groupId!}"/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>

${screens.render("component://custom-field/webapp/widget/custom-field/screens/segment/SegmentScreens.xml#SegmentValueMultiValue")}

${screens.render("component://custom-field/webapp/widget/custom-field/screens/segment/SegmentScreens.xml#SegmentValueCampaignConfig")}

<script>

jQuery(document).ready(function() { 

$('#segment-value-form').validator();

loadSegmentCode();

$('#groupId').on('change', function(){

	loadSegmentCode();

});

$('.active-btn').click(function () {

	resetDefaultEvents();

	//alert($(this).attr("data-isEnabled"));
	var actionType = $(this).attr("data-isEnabled");
	var actionUrl = "";
	if (actionType == "Y") {
		actionUrl = "enabledSegmemntCodeValue";
	} else if (actionType == "N") {
		actionUrl = "disabledSegmemntCodeValue";
	}
	
	// title="In Active Segment Code" data-isActive="N"
	
	$.ajax({
			      
		type: "POST",
     	url: actionUrl,
        data:  {"customFieldId": "${customField.customFieldId!}"},
        success: function (data) {   
            
            if (data.code == 200) {
				showAlert ("success", data.message);
				
				if (actionType == "Y") {
					$('.active-btn').html("Disable");
					$('.active-btn').attr("title", "Disable Segment Code");
					$('.active-btn').attr("data-original-title", "Disable Segment Code");
					$('.active-btn').attr("data-isEnabled", "N");
					$('#isEnabled').val("N");
				} else {
					$('.active-btn').html("Enable");
					$('.active-btn').attr("title", "Enable Segment Code");
					$('.active-btn').attr("data-original-title", "Enable Segment Code");
					$('.active-btn').attr("data-isEnabled", "Y");
					$('#isEnabled').val("Y");
				}
				
			} else {
				showAlert ("error", returnedData.message);
			}
			    	
        }
        
	});    

});

});

function loadSegmentCode() {
	
	$("#single-value-config").hide();
	$("#multiple-value-config").hide();
	$("#range-value-config").hide();
	
	var groupId = $("#groupId").val();
	
	if (groupId) {
	
		//$("#segment-value-form").validator('destroy');
	
		$.ajax({
		      
			type: "POST",
	     	url: "loadSegmentCode",
	        data:  {"groupId": groupId},
	        success: function (data) {   
	            
	            if (data.code == 200) {
					
					if (data.segmentCode.valueCapture == "SINGLE") {
						$("#single-value-config").show();
					} else if (data.segmentCode.valueCapture == "MULTIPLE") {
						$("#multiple-value-config").show();
					} else if (data.segmentCode.valueCapture == "RANGE") {
						$("#range-value-config").show();
					}
					
					$("#value-capture").html( "[ " + data.segmentCode.valueCapture + " ]" );
					
					$("#segment-value-form").validator('update');
					
				} else {
					//showAlert ("error", data.message);
				}  
				    	
	        }
	        
		});
	}
	
}

</script>
