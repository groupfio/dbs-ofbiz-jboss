<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.View} ${uiLabelMap.SegmentValue} <#if customFieldGroup.groupName?has_content>for [ ${customFieldGroup.groupName} ]</#if> </h1>
	<div class="float-right">
		<#if customFieldGroup.groupId?has_content>
		
		<a href="segmentValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Create Segment Value for ${customFieldGroup.groupName!}" >Create Segment Value</a>
		<a href="findSegmentCode" class="btn btn-xs btn-primary m5 tooltips" title="Back to Segment Code" >Back</a>
		
		</#if>
	</div>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			
			
			
		</form>			
							
		</div>
			
	</div>
	
</div>
	
<script>

jQuery(document).ready(function() { 

});

</script>
