<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<#assign campaignInputId = 1000>
<#assign campaignDaySincInputId = 5000>
<#assign campaignTriggerInputId = 10000>

<div class="clearfix"> </div>
<div id="multiple-value-config" class="row padding-r" style="padding-top: 20px;">

	<div class="col-md-6 col-sm-6">
				
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="campaignConfig-heading">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordionMenu"
						href="#accordion-campaignConfig" aria-expanded="false"
						aria-controls="collapseOne"> ${uiLabelMap.CampaignConfig} </a>
				</h4>
			</div>
			<div id="accordion-campaignConfig" class="panel-collapse collapse <#if isOpenCampaignConfig?has_content>show</#if>"
				role="tabpanel" aria-labelledby="campaignConfig-heading">
				<div class="panel-body">
					
					<div class="portlet-body form">
					
						<div class="row">
						
							<div class="col-md-6 col-sm-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Message Type</label>
									<div class="col-sm-7">
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="configType" value="TRIGGER" type="radio" <#if campaignConfig.configType?has_content && campaignConfig.configType == "TRIGGER">checked</#if> >Trigger
											</label>
										</div>
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="configType" value="BATCH" type="radio" <#if campaignConfig.configType?has_content && campaignConfig.configType == "BATCH">checked</#if> >Batch
											</label>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-md-6 col-sm-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Include Coupon</label>
									<div class="col-sm-7">
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="isCouponSegment" value="Y" type="radio" disabled>Yes
											</label>
										</div>
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="isCouponSegment" value="N" type="radio" checked>No
											</label>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						
						<div id="batch-configuration" style="display: <#if campaignConfig.configType?has_content && campaignConfig.configType == "BATCH">block<#else>none</#if>">
						
						<div class="row">
							<div class="col-md-6 col-sm-6">
							
								<div class="form-group row">
						
									<div class="offset-sm-4 col-sm-7">
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="configBatchType" value="SPEC_DATE" type="radio" <#if campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "SPEC_DATE">checked</#if> >
												Specific Calendar Date
											</label>
										</div>
										<div class="form-check-inline">
											<label class="form-check-label"> <input
												class="form-check-input" name="configBatchType" value="DAY_SINCE" type="radio" <#if campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "DAY_SINCE">checked</#if> > 
												# of days Since Start
											</label>
										</div>
									</div>
									
								</div>
							
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12 col-sm-12">
							
								<form id="updateBatchDateCampaignForm">
								
								<input type="hidden" name="groupId" value="${groupId!}">
								<input type="hidden" name="customFieldId" value="${customFieldId!}">
																
								<div id="date-campaign-section" style="display: <#if campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "SPEC_DATE">block<#else>none</#if>">
									<#if campaignConfigAssocList?has_content && campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "SPEC_DATE" >
									<#assign counter = 0>
									<#list campaignConfigAssocList as ca>
									<#assign counter = counter + 1>
									<#assign marketingCampaign = ca.getRelatedOne("MarketingCampaign", false)! />
									<div class="form-group row campaign-content" >
										<label class="col-sm-2 col-form-label">Date/Campaign </label>
										<div class="col-sm-4">
											<@simpleDateInput 
												name="specDateCampaign"
												dateStartFrom=parentCampaign.startDate
												dateEndTo=parentCampaign.endDate
												value=ca.specificDate?string["dd-MM-yyyy"]
												/>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<input id="spec-date-campaign-display-${campaignInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="${marketingCampaign.campaignName!} (${marketingCampaign.marketingCampaignId!})" readonly>
												<input id="spec-date-campaign-${campaignInputId}" type="hidden" name="specDateCampaignSelected" value="${marketingCampaign.marketingCampaignId!}"> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignInputId}" onclick="campaignPicker(this)"> 
													</span>
													<#if (counter > 1) >
														<a class="plus-icon01 rd ml-1" onclick="removeCampaignRepeateContent(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
													</#if> 
													<a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
												</span>
											</div>
							
										</div>
									</div>
									<#assign campaignInputId = campaignInputId + 1>
									</#list>
									
									<#else>
									
									<div class="form-group row campaign-content" >
										<label class="col-sm-2 col-form-label">Date/Campaign </label>
										<div class="col-sm-4">
											<@simpleDateInput 
												name="specDateCampaign"
												dateStartFrom=parentCampaign.startDate
												dateEndTo=parentCampaign.endDate
												/>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<input id="spec-date-campaign-display-${campaignInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="" readonly>
												<input id="spec-date-campaign-${campaignInputId}" type="hidden" name="specDateCampaignSelected" value=""> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignInputId}" onclick="campaignPicker(this)"> 
													</span>
													<a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
												</span>
											</div>
							
										</div>
									</div>	
									
									</#if>
								</div>
																
								</form>
								
								<form id="updateBatchDaysCampaignForm">
								
								<input type="hidden" name="groupId" value="${groupId!}">
								<input type="hidden" name="customFieldId" value="${customFieldId!}">
								
								<div id="days-campaign-section" style="display: <#if campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "DAY_SINCE">block<#else>none</#if>">
									<#if campaignConfigAssocList?has_content && campaignConfig.configBatchType?has_content && campaignConfig.configBatchType == "DAY_SINCE" >
									<#assign counter = 0>
									<#list campaignConfigAssocList as ca>
									<#assign counter = counter + 1>
									<#assign marketingCampaign = ca.getRelatedOne("MarketingCampaign", false)! />
									
									<div class="form-group row campaign-content" >
										<label class="col-sm-2 col-form-label">Days/Campaign </label>
										<div class="col-sm-4">
											<input name="daySinceCampaign" value="${ca.daySince!}" class="form-control input-sm" placeholder="Number of days" type="number" min="1">
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<input id="spec-date-campaign-display-${campaignDaySincInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="${marketingCampaign.campaignName!} (${marketingCampaign.marketingCampaignId!})" readonly>
												<input id="spec-date-campaign-${campaignDaySincInputId}" type="hidden" name="specDateCampaignSelected" value="${marketingCampaign.marketingCampaignId!}"> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignDaySincInputId}" onclick="campaignPicker(this)"> 
													</span>
													<#if (counter > 1) >
														<a class="plus-icon01 rd ml-1" onclick="removeCampaignRepeateContent(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
													</#if> 
													<a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
												</span>
											</div>
										</div>
									</div>
									
									<#assign campaignDaySincInputId = campaignDaySincInputId + 1>
									</#list>
									
									<#else>
									<div class="form-group row campaign-content" >
										<label class="col-sm-2 col-form-label">Days/Campaign </label>
										<div class="col-sm-4">
											<input name="daySinceCampaign" class="form-control input-sm" placeholder="Number of days" type="number" min="1">
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<input id="spec-date-campaign-display-${campaignDaySincInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="" readonly>
												<input id="spec-date-campaign-${campaignDaySincInputId}" type="hidden" name="specDateCampaignSelected" value=""> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignDaySincInputId}" onclick="campaignPicker(this)"> 
													</span>
													<a onclick="addCampaignRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
												</span>
											</div>
										</div>
									</div>
									
									</#if>
								</div>
								
								</form>
								
							</div>
						</div>
						
						</div>
						
						<div id="trigger-configuration" style="display: <#if campaignConfig.configType?has_content && campaignConfig.configType == "TRIGGER">block<#else>none</#if>">
						
						<div class="row">
							<div class="col-md-12 col-sm-12">
								
								<form id="updateTriggerCampaignForm">
								
								<input type="hidden" name="groupId" value="${groupId!}">
								<input type="hidden" name="customFieldId" value="${customFieldId!}">
								
								<#if campaignConfigAssocList?has_content && campaignConfig.configType?has_content && campaignConfig.configType == "TRIGGER" >
								<#assign counter = 0>
								<#list campaignConfigAssocList as ca>
								<#assign counter = counter + 1>
								<#assign marketingCampaign = ca.getRelatedOne("MarketingCampaign", false)! />
								
								<div class="form-group row campaign-content" >
									<label class="col-sm-2 col-form-label">URL/Campaign </label>
									<div class="col-sm-4">
										<input name="triggerUrl" value="${ca.triggerUrl!}" class="form-control input-sm" placeholder="URL" type="url">
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input id="spec-date-campaign-display-${campaignTriggerInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="${marketingCampaign.campaignName!} (${marketingCampaign.marketingCampaignId!})" readonly>
											<input id="spec-date-campaign-${campaignTriggerInputId}" type="hidden" name="specDateCampaignSelected" value="${marketingCampaign.marketingCampaignId!}"> 
											<span class="input-group-addon"> 
												<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignTriggerInputId}" onclick="campaignPicker(this)"> 
												</span>
											</span>
										</div>
									</div>
								</div>
								
								<#assign campaignTriggerInputId = campaignTriggerInputId + 1>
								</#list>
								
								<#else>
								
								<div class="form-group row campaign-content" >
									<label class="col-sm-2 col-form-label">URL/Campaign </label>
									<div class="col-sm-4">
										<input name="triggerUrl" class="form-control input-sm" placeholder="URL" type="url">
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input id="spec-date-campaign-display-${campaignTriggerInputId}" class="form-control input-sm specDateCampaignSelected" placeholder="Select Campaign" type="text" value="" readonly>
											<input id="spec-date-campaign-${campaignTriggerInputId}" type="hidden" name="specDateCampaignSelected" value=""> 
											<span class="input-group-addon"> 
												<span class="glyphicon glyphicon-list-alt campaign-picker" data-campaignInputId="${campaignTriggerInputId}" onclick="campaignPicker(this)"> 
												</span>
											</span>
										</div>
									</div>
								</div>
								
								</#if>
								
								</form>
								
							</div>
						</div>	
						
						</div>
						
						<div class="row">
							<div class="col-md-12 col-sm-12">
								
								<div class="form-group row campaign-content">
									<label class="col-sm-2"></label>
									<div class="col-sm-4">
										<button id="campaignConfigBtn" type="button" class="btn btn-sm btn-primary mt">Save</button>
									</div>
								</div>
									
							</div>
						</div>
							
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</div>	

<div id="select-campaign" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
   <h4 class="modal-title">${uiLabelMap.campaignListView}</h4>
   <button type="reset" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body"> 

<div class="row padding-r">
      <div class="col-md-12 col-sm-12 ">
         <div id="myBtnContainer" class="row mb-2">
            <div class="col-md-3 col-sm-3">
                  <div class="form-group autocomplete">
                     <div class="input-group search-bg autocomplete m5">
                        <input type="text" class="form-control" placeholder="Search Campaigns" id="campaignNameToFind" name="campaignNameToFind" >
                        <span class="input-group-addon"> <a class="btn btn-sm" title="Search Campaigns" href="javascript:findCampaignLists();"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                     </div>
                  </div>               
            </div>
         </div>
      </div>
   </div>
   <div class="table-responsive">
      <table id="list-picker-campaign" class="table table-striped">
         <thead>
            <tr>
               <th>Campaign Name</th>
               <th>Start Date</th>
               <th>End Date</th>
            </tr>
         </thead>
      </table>
   </div>
</div>
</div>
</div>
</div>

<script>
	$(function(){
	  $('#campaignNameToFind').keypress(function(e){
	    if(e.which == 13) {
	    	findCampaignLists();
	    }
	  });
	});
</script>

<script>

jQuery(document).ready(function() { 

$('#list-picker-campaign').DataTable({
	"order": [],
	"fnDrawCallback": function( oSettings ) {
  		resetDefaultEvents();
	}
});

$('input[name="configType"]').on('click', function(){
		
	if ($(this).val() == "BATCH") {
		$("#batch-configuration").show();
		$("#trigger-configuration").hide();
	} else if ($(this).val() == "TRIGGER") {
		$("#batch-configuration").hide();
		$("#trigger-configuration").show();
	}
	
});

$('input[name="configBatchType"]').on('click', function(){
	//alert($(this).val());
	
	if ($(this).val() == "SPEC_DATE") {
		$("#date-campaign-section").show();
		$("#days-campaign-section").hide();
	} else if ($(this).val() == "DAY_SINCE") {
		$("#days-campaign-section").show();
		$("#date-campaign-section").hide();
	}
	
});

$('#select-campaign').on('click', '.campaign-selected', function() {
    var value = $(this).children("span").attr("value");
    //alert(value);
    var campaignId = $(this).children('input[name="campaignId"]').val();
    $.post("checkCampaignTemplate",{"campaignId":campaignId},function(data){
    	if (data.flag == "N") {
    		showAlert ("error", "Please select the campaign which contains template");
    	} else{
    		$("#spec-date-campaign-display-"+specDateCampaignSelected).val( value );
		    $("#spec-date-campaign-"+specDateCampaignSelected).val(campaignId);
		    $('#select-campaign').modal('hide');
    	}
    });
});

$('#campaignConfigBtn').on('click', function(){
	
	var configType = $('input[name="configType"]:checked').val();
	var configBatchType = $('input[name="configBatchType"]:checked').val();
	//alert( $('input[name="configType"]:checked').val() );
	
	if (configType == "BATCH") {
		if (configBatchType == "SPEC_DATE") {
			$.post('updateBatchDateCampaign', $('#updateBatchDateCampaignForm').serialize(), function(returnedData) {
				
				if (returnedData.code == 200) {
					showAlert ("success", returnedData.message);
				}
					
			});
		} else if (configBatchType == "DAY_SINCE") {
			$.post('updateBatchDaysCampaign', $('#updateBatchDaysCampaignForm').serialize(), function(returnedData) {
				
				if (returnedData.code == 200) {
					showAlert ("success", returnedData.message);
				}
					
			});
		}
	} else if (configType == "TRIGGER") {
		$.post('updateTriggerCampaign', $('#updateTriggerCampaignForm').serialize(), function(returnedData) {
			
			if (returnedData.code == 200) {
				showAlert ("success", returnedData.message);
			}
				
		});
	}
	
});

});

var specDateCampaignSelected;

function campaignPicker (actionButton) {
	
	$('#select-campaign').modal("show");
	
	specDateCampaignSelected = $(actionButton).attr("data-campaignInputId");
	
}

var campaignInputId;
function addCampaignRepeateContent (actionButton) {

	var cloneHtml = $(actionButton).closest( ".campaign-content" ).clone();

	if ( $(actionButton).parent().children().find('[class=\"fa fa-minus-circle\"]').length == 0 ) {
        cloneHtml.children().find('[class=\"fa fa-plus-circle\"]').parent().before('<a class="plus-icon01 rd ml-1" onclick="removeCampaignRepeateContent(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>');
    }
    
    if (!campaignInputId) {
    	campaignInputId = Number( $(actionButton).closest( ".campaign-content" ).children().find('.campaign-picker').attr("data-campaignInputId") );
    }
    campaignInputId = campaignInputId + 1;
    
    cloneHtml.children().find('.campaign-picker').attr("data-campaignInputId", campaignInputId);
    cloneHtml.children().find('.specDateCampaignSelected').attr("id", "spec-date-campaign-display-"+campaignInputId);
    cloneHtml.children().find('[name="specDateCampaignSelected"]').attr("id", "spec-date-campaign-"+campaignInputId);
    
    cloneHtml.children().find('[name="specDateCampaignSelected"]').val("");
    cloneHtml.children().find('.form-control').val("");
    
    $(actionButton).closest( ".campaign-content" ).after(cloneHtml);
    
    $('.form_datetime').datetimepicker();
    
}

function removeCampaignRepeateContent (actionButton) {
	$(actionButton).closest( ".campaign-content" ).remove();
}



$(document).ready(function(){
	localStorage.clear();
	findCampaignLists();
});

function findCampaignLists(){

	var campaignNameToFind = $("#campaignNameToFind").val();
	$("#loader").show();
	var url = "findCampaignListsAjax?campaignNameToFind="+campaignNameToFind;
	$('#list-picker-campaign').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "filter" : false,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "Paginate": true,
			"language": {
				"emptyTable": "No data available in table",
				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
				"infoEmpty": "No entries found",
				"infoFiltered": "(filtered1 from _MAX_ total entries)",
				"lengthMenu": "Show _MENU_ entries",
				"zeroRecords": "No matching records found",
				"oPaginate": {
					"sNext": "Next",
					"sPrevious": "Previous"
				}
			},
	         "pageLength": 10,
	         "bAutoWidth":false,
	         "stateSave": true,
	         "columns": [
	           
	            { "data":  null,
					"render": function(data, type, row, meta){
						if(type === 'display'){
                        data = '<a href="#" class="campaign-selected"><span value ="'+row.campaignName+' ('+row.campaignId+')"></span>'+row.campaignName+' ('+row.campaignId+')<input type="hidden" name="campaignId" value="'+row.campaignId+'"></a>';						}
						return data;
					 }
				 },
	            { "data":  "startDate"},
	            { "data":  "endDate"},
	            
				 
	          ]
		});
	$("#loader").hide();
}
</script>