<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<#-- 
<div class="page-header">
	<h2 class="float-left">${uiLabelMap.List} ${uiLabelMap.SegmentValue}</h2>
</div>
 -->
<#assign salesTriggerType = delegator.findByAnd("MarketingCampaignContactList", {"marketingCampaignId": "${marketingCampaignId?if_exists}","contactListId":"RMSales_Trigger_Monday_EveryMonth"},[], false)?if_exists />


<div class="table-responsive">
	<table class="table table-hover" id="list-segment-value">
	<thead>
	<tr>
		
		<th>${uiLabelMap.nextAction!}</th>
		
		<th>Message Type</th>
		<th>Include Coupon</th>
		<th>Date/#Days</th>
		<th># of Account</th>
		<th># of Contacts</th>
        <#if salesTriggerType?has_content>
		<th># of RM</th>
		</#if>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>

	<#if customFieldList?has_content>
	<#list customFieldList as ec>
    <#if ec.customFieldName != "Sent" && ec.customFieldName != "Delivered" >
	<tr>
		
		<td>${ec.customFieldName!}</td>
		
		<td>${ec.campaignConfigType!?lower_case?cap_first}</td>
		<td>
			<#if ec.isCouponSegment?has_content && ec.isCouponSegment == "Y">
				Yes
			<#elseif ec.isCouponSegment?has_content && ec.isCouponSegment == "N">
				No
			</#if>
		</td>
		<td>
			<#if ec.configSummaryList?has_content>
			<#list ec.configSummaryList as configSummary>
				${configSummary!} <br>
			</#list>
			</#if>
		</td>
		<#if salesTriggerType?has_content>
         <td>
			<a href="#" class="btn btn-xs btn-primary m5 tooltips view-member" data-segmentValueName="" data-groupId="" data-customFieldId="" title="${uiLabelMap.ViewMembers!}" >
			<strong>0</strong>
			</a>
			
		</td>
		<td>
			<a href="#" class="btn btn-xs btn-primary m5 tooltips view-contactMember"  data-segmentValueName="" data-campaignId=""  title="${uiLabelMap.ViewMembers!}"  >
			<strong>0</strong>
		</td>		
		<td>		    
			<a href="#" class="btn btn-xs btn-primary m5 tooltips view-member" data-segmentValueName="${ec.customFieldName!}" data-groupId="${ec.groupId!}" data-customFieldId="${ec.customFieldId!}" title="${uiLabelMap.ViewMembers!}" >
			<strong>${ec.coustomerCount!}</strong>
			</a>
		</td>
		<#else>
		<td>		    
			<a href="#" class="btn btn-xs btn-primary m5 tooltips view-member" data-segmentValueName="${ec.customFieldName!}" data-groupId="${ec.groupId!}" data-customFieldId="${ec.customFieldId!}" title="${uiLabelMap.ViewMembers!}" >
			<strong>${ec.coustomerCount!}</strong>
			</a>
			<#-- <a href="/custom-field/control/segmentValueCustomer?customFieldId=${ec.customFieldId}&groupId=${ec.groupId!}" target="_blank" class="btn btn-xs btn-primary m5 tooltips <#if !ec.groupId?has_content>disabled</#if>" title="${uiLabelMap.ManageCustomers!}" >
			<strong>${ec.coustomerCount!}</strong>
			</a> -->
		</td>
		<td> <#assign count = "0" />
			<#if responseCount?exists>
		    <#if ec.customFieldName! == "Not Opened">
            <#assign count = responseCount.notOpen?default(0) />
            <#elseif ec.customFieldName! == "Opened">
			<#assign count = responseCount.openedMailCount?default(0) />
            <#elseif ec.customFieldName! == "Sent">
			<#assign count = responseCount.sentMailCount?default(0) /> 
            <#elseif ec.customFieldName! == "Clicked">
			<#assign count = responseCount.clickedMailCount?default(0) />
            <#elseif ec.customFieldName! == "Bounced">
			<#assign count = responseCount.bouncedMailCount?default(0) />
			<#elseif ec.customFieldName! == "Unsubscribed">
			<#assign count = responseCount.unSubscribed?default(0) />
			<#elseif ec.customFieldName! == "Converted">
			<#assign count = responseCount.convertedCount?default(0) />
			<#elseif ec.customFieldName! == "Subscribed">
			<#assign count = responseCount.subscribed?default(0) />
			</#if>
            </#if>
			<a href="#" class="btn btn-xs btn-primary m5 tooltips view-contactMember" data-segmentValueName="${ec.customFieldName!}" data-campaignId="${marketingCampaign.marketingCampaignId!}"  title="${uiLabelMap.ViewMembers!}" >
			<strong>${count!}</strong></a>
		</td>
		</#if>
		<td class="text-center">
			<div class="">
				<a href="/custom-field/control/editSegmentValueCampaignConfig?customFieldId=${ec.customFieldId}&groupId=${ec.groupId!}&marketingCampaignId=${marketingCampaignId!}${StringUtil.wrapString(externalKeyParam)}" class="btn btn-xs btn-primary tooltips" data-original-title="Edit"><i class="fa fa-pencil info"></i></a>
			</div>
		</td>	
	</tr>
	</#if>
	</#list>
		
	</#if>
	
	</tbody>
	</table>
</div>

<div id="modalMemberView" class="modal fade" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">${uiLabelMap.List} ${uiLabelMap.Member} for [ <span id="segment-value-title"></span> ]</h2>
        <button type="reset" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
        <div class="clearfix"></div>
		<#-- <div class="page-header">
			<h2 class="float-left"></h2>
			<div class="float-right">
		  			
			</div>
		</div> -->
		<div class="table-responsive">
			<table id="view-customer-list" class="table table-striped">
				<thead>
					<tr>
						<th>Member</th>
		                <#-- <th>City</th>
		                <th>State</th> -->
		                <th>Phone Number</th>
		                <th>Email Address</th>
		                <th>Status</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
        
      </div>
      <div class="modal-footer">
        <button type="sbmit" class="btn btn-sm btn-primary navbar-dark" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<#include "component://campaign/webapp/campaign/campaigns/memberContactModel.ftl" />
<script type="text/javascript">

var customFieldId, groupId;

jQuery(document).ready(function() {	

	$('#list-segment-value').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	
	$('.view-member').on('click', function(){
		//event.preventDefault();
		
		$('#modalMemberView').modal("show");
		
		customFieldId = $(this).attr("data-customFieldId");
		groupId = $(this).attr("data-groupId");
		var segmentValueName = $(this).attr("data-segmentValueName");		
		$('#segment-value-title').html( segmentValueName );								
	});
	
	$('.view-contactMember').on('click', function(){
	   
		//event.preventDefault(); 
		$('#modalContactView').modal("show");
		
		customFieldId = $(this).attr("data-segmentValueName");
		groupId = $(this).attr("data-campaignId");
		var segmentValueNames = $(this).attr("data-segmentValueName");		
		
		$('#segment-value-titles').html( segmentValueNames );
	});
	
	$('#modalMemberView').on('shown.bs.modal', function (e) {
	  	findSelectedMembers(groupId, customFieldId);
	});
	
	$('#modalContactView').on('shown.bs.modal', function (e) {
	  	findContactMembers(groupId, customFieldId);
	});	

});

function findSelectedMembers(groupId, customFieldId) {
	
	//var customFieldId = $('#customFieldId').val();
	//var groupId = $('#groupId').val();
	
   	var url = "searchSegmentCustomers?groupId="+groupId+"&customFieldId="+customFieldId;
   	
	$('#view-customer-list').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        //"stateSave": true,
	        
	        "pageLength": 20,
	        
	        /*
	        "columnDefs": [ 
	        	{
					"targets": 9,
					"orderable": false
				} 
			],
			*/		      
	        "columns": [
	        	
	            { "data": "nameWithIdLink" },
	            //{ "data": "city" },
	            //{ "data": "state" },
	            { "data": "phoneNumber" },
	            { "data": "infoString" },
	            { "data": "statusId" },
					            
	        ],
	        "initComplete": function(settings, json) {
			    resetDefaultEvents();
			}
		});
	
}
		
function findContactMembers(groupId, customFieldId) {
	
   	var url = "searchContactMembers?campaignId="+groupId+"&customFieldName="+customFieldId;
   	
	$('#view-contactCustomer-list').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        //"stateSave": true,
	        
	        "pageLength": 20,
	        /*
	        "columnDefs": [ 
	        	{
					"targets": 9,
					"orderable": false
				} 
			],
			*/		      
	        "columns": [
	        	
	            { "data": "nameWithIdLink" },
	            //{ "data": "city" },
	            //{ "data": "state" },
	            { "data": "phoneNumber", 
	            "render": function(data, type, row, meta) {
                        var phoneNum = row.phoneNumber;
                        if (phoneNum != "undefined") {
                        data = phoneNum;
                        }else{
                        data = "";
                        }
                        return data;
                      }
                    },
	            { "data": "infoString" },
	            { "data": "statusId" },
					            
	        ],
	        "initComplete": function(settings, json) {
			    resetDefaultEvents();
			}
		});
}
		
</script>