<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.Find} ${uiLabelMap.EconomicMetric}</h1>
	<div class="float-right">
		
	</div>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" class="form-horizontal" data-toggle="validator">
      <div class="row">
      	
      	<div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="groupingCode"
				options=groupingCodeList
				required=false
				value=customFieldGroup.groupingCode
				allowEmpty=true
				emptyText = uiLabelMap.groupingCode
				dataLiveSearch=true
				/>
         </div>	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="groupId"
				required=false
				value=customFieldGroup.groupId
				allowEmpty=true
				emptyText = uiLabelMap.economicMetric
				/>	
			<#-- 	
         	<@simpleInput 
				id="groupId"
				placeholder=uiLabelMap.economicMetricId
				value=customFieldGroup.groupId
				required=false
				maxlength=250
				/>	 -->
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="groupName"
				placeholder=uiLabelMap.economicMetricName
				value=customFieldGroup.groupName
				required=false
				maxlength=255
				/>
         </div>
         
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="valueCapture"
				options=valueCaptureList
				required=false
				value=customFieldGroup.valueCapture
				allowEmpty=true
				emptyText = uiLabelMap.valueCapture
				/>		
         </div>
         
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="isCampaignUse"
				options=yesNoOptions
				required=false
				value=customFieldGroup.isCampaignUse
				allowEmpty=true
				emptyText = uiLabelMap.isCampaignUse
				/>				
         </div>
         
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="type"
				options=typeList
				required=false
				value=customFieldGroup.type
				allowEmpty=true
				emptyText = uiLabelMap.type
				/>		
         </div>
         
         <@fromSimpleAction id="" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<script>

jQuery(document).ready(function() {

	loadSegmentCodeList();
	$("#groupingCode").change(function() {
		loadSegmentCodeList()
	});

});

function loadSegmentCodeList() {
	var nonSelectContent = "<span class='nonselect'>Select ${uiLabelMap.economicMetric!}</span>";
	var groupNameOptions = '<option value="" data-content="'+nonSelectContent+'" selected="">Select ${uiLabelMap.economicMetric!}</option>';		
		
	//if ( $("#groupingCode").val() ) {
		
		$.ajax({
			      
			type: "POST",
	     	url: "getCustomFieldGroups",
	        data:  {"groupingCode": $("#groupingCode").val()},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.groups.length; i++) {
	            		var group = data.groups[i];
	            		groupNameOptions += '<option value="'+group.groupId+'">'+group.groupName+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
		$("#groupId").html( groupNameOptions );
		
		<#if customFieldGroup.groupId?has_content>
		$("#groupId").val( "${customFieldGroup.groupId}" );
		</#if>
	
		$('#groupId').dropdown('refresh');
	//}
		
}

</script>
