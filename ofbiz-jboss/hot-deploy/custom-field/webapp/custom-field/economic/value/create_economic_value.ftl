<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header border-b">
    <h1 class="float-left">${uiLabelMap.Create} ${uiLabelMap.EconomicValue} <#if customFieldGroup.groupName?has_content>for [ ${customFieldGroup.groupName} ]</#if> </h1>
    <div class="float-right">
        <#if customFieldGroup.groupId?has_content>
        <#-- <a href="findSegmentValue" class="btn btn-xs btn-primary m5 tooltips" title="Cancel" >Cancel</a> -->
        <a href="viewEconomicValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Back to ${customFieldGroup.groupName!}" >Back</a>
        </#if>
    </div>
</div>

<ul class="nav nav-tabs">
    <li class="nav-item"><a data-toggle="tab" href="#createEconomicMetric"
        class="nav-link active">${uiLabelMap.Create} ${uiLabelMap.EconomicValue}</a></li>
    <#if customFieldGroup.groupId?exists && customFieldGroup.groupId?has_content>
    <li class="nav-item"><a data-toggle="tab" href="#uploadEconomicMetric"
        class="nav-link">Upload Customers</a></li>
    </#if>
</ul>
<div class="tab-content">
<div id="createEconomicMetric" class="tab-pane fade show active">

<div class="page-header">
   <h2 class="float-left">${uiLabelMap.Create} ${uiLabelMap.EconomicValue}</h2>
</div>

<#if customFieldGroup.groupId?has_content>
	<#assign actionUrl = "createEconomicValue"/>
<#else>
	<#assign actionUrl = "createEconomicValueIndividual"/>
</#if>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form id="economic-value-form" role="form" class="form-horizontal" action="<@ofbizUrl>${actionUrl}</@ofbizUrl>" encType="multipart/form-data" method="post" >
				
			<div class="form-body">
			
			<#if groupId?has_content>	
			<input type="hidden" id="groupId" name="groupId" value="${customField.groupId!}" />					
			<#-- <@readonlyInput 
				id="groupId"
				label=uiLabelMap.segmentCode
				value=customField.groupId
				isHiddenInput=true
				/> -->
			<#else>	
			<@dropdownInput 
				id="groupId"
				label=uiLabelMap.economicMetric
				options=groupList
				required=true
				value=customField.groupId
				allowEmpty=true
				dataLiveSearch=true
				/>
			</#if>
			
			<@generalInput 
				id="customFieldId"
				label=uiLabelMap.economicValueId
				placeholder=uiLabelMap.economicValueId
				value=customField.customFieldId
				required=true
				maxlength=250
				/>		
									
			<@generalInput 
				id="customFieldName"
				label=uiLabelMap.economicValueName
				placeholder=uiLabelMap.economicValueName
				value=customField.customFieldName
				required=true
				maxlength=255
				/>	
			
			<@readonlyInput 
				id="valueCapture"
				label=uiLabelMap.valueCapture
				value=customFieldGroup.valueCapture
				isHiddenInput=true
				/>
				
			<@dropdownInput 
				id="isEnabled"
				label=uiLabelMap.isEnabled
				options=yesNoOptions
				required=fales
				value=customField.isEnabled
				allowEmpty=true
				/>	
			
			<#-- <div class="page-header">
				<h2>${uiLabelMap.Configuration} ${uiLabelMap.SegmentValue} <span id="value-capture"></span></h2>
			</div> -->
			
			<#-- <input type="hidden" name="valueCapture" value="${customFieldGroup.valueCapture!}" /> -->
			<input type="hidden" name="valueSeqNum" value="1" />		
			
			<div id="range-value-config" style="display: none"> 
			
			<@generalInput 
				id="valueMin"
				label=uiLabelMap.valueMin
				placeholder=uiLabelMap.valueMin
				value=valueConfig.valueMin
				required=true
				inputType="number"
				maxlength=20
				/>	
				
			<@generalInput 
				id="valueMax"
				label=uiLabelMap.valueMax
				placeholder=uiLabelMap.valueMax
				value=valueConfig.valueMax
				required=true
				inputType="number"
				maxlength=20
				/>
			
			</div>
			<#--			
			<div id="single-value-config" style="display: none">		
			
			<@generalInput 
				id="valueData"
				label=uiLabelMap.valueData
				placeholder=uiLabelMap.valueData
				value=valueConfig.valueData
				required=true
				/>	
																								
			</div> -->
			
			<@generalInput 
				id="sequenceNumber"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customField.sequenceNumber
				inputType="number"
				required=false
				min=1
				/>		
																																																																																																																																																																																																												
			</div>
			
			<@fromCommonAction showCancelBtn=false showClearBtn=true/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>
</div>

<#if customFieldGroup.groupId?exists && customFieldGroup.groupId?has_content>
<div id="uploadEconomicMetric" class="tab-pane fade">
   ${screens.render("component://custom-field/webapp/widget/custom-field/screens/economic/EconomicScreens.xml#uploadEconomicMetric")}
</div>
</#if>

</div>
<script>

jQuery(document).ready(function() { 

$('#economic-value-form').validator();

loadEconomicMetric();

$('#groupId').on('change', function(){

	loadEconomicMetric();

});

});

function loadEconomicMetric() {
	
	//$("#single-value-config").hide();
	$("#range-value-config").hide();
	
	var groupId = $("#groupId").val();
	
	if (groupId) {
	
		//$("#economic-value-form").validator('destroy');
	
		$.ajax({
		      
			type: "POST",
	     	url: "loadSegmentCode",
	        data:  {"groupId": groupId},
	        success: function (data) {   
	            
	            if (data.code == 200) {
					
					if (data.segmentCode.valueCapture == "SINGLE") {
						//$("#single-value-config").show();
					} else if (data.segmentCode.valueCapture == "MULTIPLE") {
						
					} else if (data.segmentCode.valueCapture == "RANGE") {
						$("#range-value-config").show();
					}
					
					$("#valueCapture").html( "[ " + data.segmentCode.valueCapture + " ]" );
					
					$("#economic-value-form").validator('update');
					
				} else {
					//showAlert ("error", data.message);
				}  
				    	
	        }
	        
		});
	}
	
	
	
}
jQuery(document).ready(function() { 
    <#if !activeTab?has_content>
        <#assign activeTab = requestParameters.activeTab!>
    </#if>
    
    <#if activeTab?has_content && activeTab == "createEconomicMetric">
        $('.nav-tabs a[href="#createEconomicMetric"]').tab('show');
    <#elseif activeTab?has_content && activeTab == "uploadEconomicMetric">
        $('.nav-tabs a[href="#uploadEconomicMetric"]').tab('show'); 
    <#else>
        $('.nav-tabs a[href="#createEconomicMetric"]').tab('show'); 
    </#if>
});
</script>
