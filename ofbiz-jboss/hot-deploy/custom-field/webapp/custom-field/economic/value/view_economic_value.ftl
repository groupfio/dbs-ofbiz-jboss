<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.View} ${uiLabelMap.EconomicValue} <#if customFieldGroup.groupName?has_content>for [ ${customFieldGroup.groupName} ]</#if> </h1>
	<div class="float-right">
		<#if customFieldGroup.groupId?has_content>
		
		<a href="economicValueForGroup?groupId=${customFieldGroup.groupId}" class="btn btn-xs btn-primary m5 tooltips" title="Create Economic Metric for ${customFieldGroup.groupName!}" >Create Economic Metric</a>
		<a href="findEconomicMetric" class="btn btn-xs btn-primary m5 tooltips" title="Back to Economic Code" >Back</a>
		
		</#if>
	</div>
</div>

<#if customFieldGroup.groupId?has_content>
	<#assign actionUrl = "createEconomicValue"/>
<#else>
	<#assign actionUrl = "createEconomicValueIndividual"/>
</#if>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			
			
			
		</form>			
							
		</div>
			
	</div>
	
</div>
	
<script>

jQuery(document).ready(function() { 

});

</script>
