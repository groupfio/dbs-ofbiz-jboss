<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Find} ${uiLabelMap.CustomFieldGroup}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" class="form-horizontal" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="groupId"
				placeholder=uiLabelMap.groupId
				value=customFieldGroup.groupId
				required=false
				maxlength=20
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="groupName"
				placeholder=uiLabelMap.groupName
				value=customFieldGroup.groupName
				required=false
				maxlength=255
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="hide"
				options=yesNoOptions
				required=false
				value=customFieldGroup.hide
				allowEmpty=true
				emptyText = uiLabelMap.hide
				/>	
         </div>
         
         <@fromSimpleAction id="" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
