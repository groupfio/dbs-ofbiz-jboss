<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Create} ${uiLabelMap.CustomFieldGroup}</h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>createCustomFieldGroup</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
				
			<div class="form-body">
			
			<@generalInput 
				id="groupId"
				label=uiLabelMap.groupId
				placeholder=uiLabelMap.groupId
				value=customFieldGroup.groupId
				required=true
				maxlength=250
				/>
			
			<@generalInput 
				id="groupName"
				label=uiLabelMap.groupName
				placeholder=uiLabelMap.groupName
				value=customFieldGroup.groupName
				required=true
				maxlength=255
				/>
			
			<@generalInput 
				id="sequence"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customFieldGroup.sequence
				inputType="number"
				required=false
				min=1
				/>	
				
			<@dropdownInput 
				id="hide"
				label=uiLabelMap.hide
				options=yesNoOptions
				required=false
				value=customFieldGroup.hide
				allowEmpty=true
				/>								
															
			</div>
			
			<@fromCommonAction showCancelBtn=false showClearBtn=true/>
			
		</form>			
							
		</div>
						
	</div>
	
</div>
