import org.fio.crm.ajax.AjaxEvents
import org.fio.crm.constants.CrmConstants
import org.fio.crm.contactmech.PartyPrimaryContactMechWorker
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.groupfio.custom.field.util.DataUtil;
import org.ofbiz.party.party.PartyHelper;

import org.fio.campaign.util.LoginFilterUtil;
import org.fio.campaign.util.ResponseUtils;

import java.util.HashMap;
import java.util.List;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;
import java.util.ArrayList;
import javolution.util.FastList;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

HttpSession session = request.getSession();
userLoginId = userLogin.getAt('partyId');
List<String> accountList = new ArrayList<String>();

if(LoginFilterUtil.checkEmployeePosition(delegator,userLoginId) && UtilValidate.isNotEmpty(session.getAttribute("dataSecurityMetaInfo"))){
	Map<String, Object> dataSecurityMetaInfo = (Map<String, Object>) session.getAttribute("dataSecurityMetaInfo");
	if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {	
		List<String> lowerPositionPartyIds = (List<String>) dataSecurityMetaInfo.get("lowerPositionPartyIds");
		accountList = LoginFilterUtil.getCampaignsAccountList(delegator, lowerPositionPartyIds);
	}
}

String groupId = request.getParameter("groupId");
String customFieldId = request.getParameter("customFieldId");

accountSearchPartyId = parameters.get("accountSearchPartyId");
searchFirstName = parameters.get("searchFirstName");
//searchCompanyName = parameters.get("searchCompanyName");
searchEmailId = parameters.get("searchEmailId");
searchPhoneNum = parameters.get("searchPhoneNum");
searchRoleTypeId = parameters.get("searchRoleTypeId");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

delegator = request.getAttribute("delegator");
conditionsList = FastList.newInstance();

  if (UtilValidate.isNotEmpty(accountList) && accountList.size() != 0 ){
	EntityCondition accountCondition = EntityCondition.makeCondition("partyId", EntityOperator.IN, accountList);
	conditionsList.add(accountCondition);
	}
	
String groupType = "";
if (UtilValidate.isNotEmpty(groupId)) {
	customFieldGroup = EntityUtil.getFirst( delegator.findByAnd("CustomFieldGroup",UtilMisc.toMap("groupId", groupId), null, false) );
	groupType = customFieldGroup.getString("groupType");
}
context.put("groupType", groupType);

if (UtilValidate.isNotEmpty(searchRoleTypeId)) {
	println("searchRoleTypeId>> "+searchRoleTypeId);
	conditionsList.add(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, searchRoleTypeId));
}

if (UtilValidate.isNotEmpty(accountSearchPartyId)) {
	
	EntityCondition partyCondition = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, accountSearchPartyId);
	conditionsList.add(partyCondition);
}

if (UtilValidate.isNotEmpty(searchFirstName)) {
	EntityCondition nameCondition = EntityCondition.makeCondition([EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("firstName")), EntityOperator.LIKE, searchFirstName.toUpperCase()+"%"),
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("groupName")), EntityOperator.LIKE, searchFirstName.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(nameCondition);
}

List eventExprs = [];

if (UtilValidate.isNotEmpty(searchEmailId) || UtilValidate.isNotEmpty(searchPhoneNum)) {

	if (UtilValidate.isNotEmpty(searchEmailId)) {
		EntityCondition emailCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("infoString", EntityOperator.LIKE, searchEmailId+"%"),
			EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "EMAIL_ADDRESS")
		], EntityOperator.AND);
		eventExprs.add(emailCondition);
	}

	if (UtilValidate.isNotEmpty(searchPhoneNum)) {
		EntityCondition phoneCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("contactNumber", EntityOperator.EQUALS, searchPhoneNum),
			EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "TELECOM_NUMBER")
		], EntityOperator.AND);
		eventExprs.add(phoneCondition);
	}

	conditionsList.add(EntityCondition.makeCondition(eventExprs, EntityOperator.OR));
}

EntityCondition segmentCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupId),
	EntityCondition.makeCondition("customFieldId", EntityOperator.EQUALS, customFieldId)
], EntityOperator.AND);
conditionsList.add(segmentCondition);

String segmentationValueAssociatedEntityName = DataUtil.getSegmentationValueAssociatedEntityName(delegator, groupId);

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition(segmentationValueAssociatedEntityName, mainConditons, null, UtilMisc.toSet("partyId"), efoNum);

/*
partyList = delegator.findList(segmentationValueAssociatedEntityName, mainConditons, UtilMisc.toSet("partyId"), UtilMisc.toList("-createdStamp"), efo, false);
int count = partyList.size();
*/

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);
	
partyList = delegator.findList(segmentationValueAssociatedEntityName, mainConditons, null, UtilMisc.toList("-createdStamp"), efo, false);

JSONObject obj = new JSONObject();
JSONArray dataMap = new JSONArray();

long recordsFiltered = count;
long recordsTotal = count;

if (partyList != null && partyList.size() > 0) {

	int id = 1;
	
	for (GenericValue party : partyList) {
	
		partyId = party.partyId;
	
		partySummary = delegator.findOne("PartySummaryDetailsView", ["partyId": partyId], false);
		if (partySummary != null && partySummary.size() > 0) {
			JSONObject list = new JSONObject();
			String groupName = partySummary.getString("groupName");
			String statusId = partySummary.getString("statusId");
			String statusItemDesc = "";
			String name = partySummary.getString("firstName")+" "+partySummary.getString("lastName");
			if (UtilValidate.isNotEmpty(statusId)) {
				statusItem = delegator.findOne("StatusItem", ["statusId": statusId], false);
				if (statusItem != null && statusItem.size() > 0) {
					statusItemDesc = statusItem.getString("description");
				}
			}
			String dataSourceDesc = ""
			partyDataSource = EntityUtil.getFirst(delegator.findByAnd("PartyDataSource", ["partyId": partyId], ["-fromDate"], false));
			if (partyDataSource != null && partyDataSource.size() > 0) {
				String dataSourceId = partyDataSource.getString("dataSourceId");
				if (UtilValidate.isNotEmpty(dataSourceId)) {
					dataSource = delegator.findOne("DataSource", ["dataSourceId": dataSourceId], false);
					if (dataSource != null && dataSource.size() > 0) {
						dataSourceDesc = dataSource.getString("description");
					}
				}
			}
			
			name = PartyHelper.getPartyName(delegator, partyId, false);
			
			String city = "";
			String state = "";
			String phoneNumber = "";
			String infoString = "";
			Map<String,String> partyContactInfo = PartyPrimaryContactMechWorker.getPartyPrimaryContactMechValueMaps(delegator, partyId);
			phoneNumber = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("PrimaryPhone") : "";
			infoString = UtilValidate.isNotEmpty(partyContactInfo) ? partyContactInfo.get("EmailAddress") : "";
			
			GenericValue postalAddress = PartyPrimaryContactMechWorker.getPartyPrimaryPostal(delegator, partyId);
			if(UtilValidate.isNotEmpty(postalAddress)) {
				city = postalAddress.getString("city");
				
				String stateProvinceGeoId = postalAddress.getString("stateProvinceGeoId");
				if (UtilValidate.isNotEmpty(stateProvinceGeoId)) {
					GenericValue geo = delegator.findOne("Geo", ["geoId": stateProvinceGeoId], false);
					if (UtilValidate.isNotEmpty(geo)) {
						state = geo.getString("geoName");
					}
				}
			}
			
			String nameWithId = name+" ("+partyId+")";
			String nameWithIdLink = "";
			
			condList = FastList.newInstance();
			partyCond = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
			condList.add(partyCond);
			mainCond = EntityCondition.makeCondition(condList, EntityOperator.AND);
			partyDetail = EntityUtil.getFirst( delegator.findList("PartyFromByRelnAndContactInfoAndPartyClassification", mainCond, null, UtilMisc.toList("-partyId"), null, false) );
			if (UtilValidate.isNotEmpty(partyDetail) && UtilValidate.isNotEmpty(partyDetail.getString("roleTypeIdFrom"))) {
				if (partyDetail.getString("roleTypeIdFrom").equals("ACCOUNT")) {
					nameWithIdLink = '<a target="_blank" href="/crm/control/viewAccount?partyId='+partyId+'">'+nameWithId+'</a>';
				} else if (partyDetail.getString("roleTypeIdFrom").equals("CONTACT")) {
					nameWithIdLink = '<a target="_blank" href="/crm/control/viewContact?partyId='+partyId+'">'+nameWithId+'</a>';
				} else if (partyDetail.getString("roleTypeIdFrom").equals("LEAD")) {
					nameWithIdLink = '<a target="_blank" href="/crm/control/viewLead?partyId='+partyId+'">'+nameWithId+'</a>';
				} else {
					nameWithIdLink = nameWithId;
				}
			}
			
			
			id = id+1;
			
			list.put("id",id+"");
			
			list.put("partyId",partyId);
			list.put("name",name);
			list.put("nameWithId", nameWithId);
			list.put("nameWithIdLink", nameWithIdLink);
			
			list.put("statusId",statusItemDesc);
			list.put("phoneNumber",phoneNumber);
			list.put("infoString",infoString);
			list.put("city",city);
			list.put("state",state);
			
			if (UtilValidate.isNotEmpty(groupType) && groupType.equals("SEGMENTATION")) {
				list.put("groupActualValue", party.groupActualValue);
				list.put("inceptionDate", party.inceptionDate);
			} 
			
			if (UtilValidate.isNotEmpty(groupType) && groupType.equals("ECONOMIC_METRIC")) {
				list.put("propertyValue", party.propertyValue);
			} 
			
			list.put("entryDate", UtilDateTime.timeStampToString(party.getTimestamp("createdStamp"), "dd/MM/yyyy", TimeZone.getDefault(), null));
						
			dataMap.add(list);
		}
	}
	
	obj.put("data", dataMap);
	obj.put("draw", draw);
	obj.put("recordsTotal", recordsTotal);
	obj.put("recordsFiltered", recordsFiltered);
	
	return AjaxEvents.doJSONResponse(response, obj);
	
} else {

	obj.put("data", dataMap);
	obj.put("draw", draw);
	obj.put("recordsTotal", 0);
	obj.put("recordsFiltered", 0);
	
	return AjaxEvents.doJSONResponse(response, obj);
	
}

 
