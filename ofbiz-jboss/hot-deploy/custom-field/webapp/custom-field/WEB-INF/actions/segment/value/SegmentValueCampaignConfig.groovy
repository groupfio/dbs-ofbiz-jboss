import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.groupfio.custom.field.util.DataHelper;
import org.groupfio.custom.field.constants.CustomFieldConstants.GroupType;
import org.ofbiz.entity.GenericValue;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import org.groupfio.custom.field.util.DataUtil;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("CustomFieldUiLabels", locale);

campaignConfig = UtilMisc.toMap("configType", "BATCH");
context.put("campaignConfig", campaignConfig);

groupId = request.getParameter("groupId"); 
customFieldId = request.getParameter("customFieldId"); 

context.put("groupId", groupId);
context.put("customFieldId", customFieldId);

println("customFieldId>> "+customFieldId);
if (UtilValidate.isNotEmpty(customFieldId)) {

	campaignConfig = EntityUtil.getFirst( delegator.findByAnd("CustomFieldCampaignConfig", UtilMisc.toMap("customFieldId", customFieldId, "groupId", groupId), null, false) );
	if (UtilValidate.isNotEmpty(campaignConfig)) {
		context.put("campaignConfig", campaignConfig);
	}
	
	condition = UtilMisc.toMap("customFieldId", customFieldId);

	cond = EntityCondition.makeCondition(condition);
	campaignConfigAssocList = delegator.findList("CustomFieldCampaignConfigAssoc", cond, null, ["sequenceNumber"], null, false);
	context.put("campaignConfigAssocList", campaignConfigAssocList);
	println("campaignConfigAssocList>> "+campaignConfigAssocList);
	
	parentCampaign = DataUtil.getCampaign(delegator, groupId);
	context.put("parentCampaign", UtilValidate.isNotEmpty(parentCampaign) ? parentCampaign : UtilMisc.toMap());
}


