<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Create} ${uiLabelMap.GroupingCode} [ <small>${groupingCode.groupType!}</small> ]</h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>createGroupingCode</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
				
			<input type="hidden" name="groupType" value="${groupingCode.groupType!}" />		
				
			<div class="form-body">
			
			<@generalInput 
				id="groupingCode"
				label=uiLabelMap.groupingCode
				placeholder=uiLabelMap.groupingCode
				value=groupingCode.groupingCode
				required=true
				/>	
				
			<@generalInput 
				id="sequenceNumber"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=groupingCode.sequenceNumber
				inputType="number"
				required=false
				min=1
				/>			
				
			<@textareaInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				rows="3"
				value=groupingCode.description
				required=true
				/>													
																																					
			</div>
			
			<@fromCommonAction showCancelBtn=false showClearBtn=true/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>
