<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.List} ${uiLabelMap.GroupingCode}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="grouping-code-list">
	<thead>
	<tr>
		<th>${uiLabelMap.groupingCode!}</th>
		<th>${uiLabelMap.description!}</th>
		<th>${uiLabelMap.sequence!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	<#if groupingCodeList?has_content>
		
	<#list groupingCodeList as ec>
	<tr>
		<td>${ec.groupingCode!}</td>
		<td>${ec.description!}</td>
		<td>${ec.sequenceNumber!}</td>
		<td class="text-center">
			<div class="">
				<a href="editGroupingCode?groupingCodeId=${ec.customFieldGroupingCodeId}" class="btn btn-xs btn-primary tooltips" data-original-title="Edit"><i class="fa fa-pencil info"></i></a>
				<a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="deleteGroupingCode?groupingCodeId=${ec.customFieldGroupingCodeId}" data-original-title="Remove"><i class="fa fa-times red"></i></a>
			</div>
		</td>	
	</tr>
	
	</#list>
		
	</#if>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {	

	$('#grouping-code-list').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	
});	
</script>