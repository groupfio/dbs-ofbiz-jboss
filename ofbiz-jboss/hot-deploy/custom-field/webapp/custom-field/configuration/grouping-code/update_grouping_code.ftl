<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Update} ${uiLabelMap.GroupingCode} [ <small>${groupingCode.groupType!}</small> ]</h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>updateGroupingCode</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
			
			<input type="hidden" name="groupingCodeId" value="${groupingCodeId!}" />	
			<input type="hidden" name="groupType" value="${groupingCode.groupType!}" />	
			
			<div class="form-body">
			
			<@readonlyInput 
				id="groupingCode"
				label=uiLabelMap.groupingCode
				value=groupingCode.groupingCode
				isHiddenInput=true
				/>
				
			<@generalInput 
				id="sequenceNumber"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=groupingCode.sequenceNumber
				inputType="number"
				required=false
				min=1
				/>			
				
			<@textareaInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				rows="3"
				value=groupingCode.description
				required=true
				/>				
																																																																																																																																																																																																																																																	
			</div>
			
			<@fromCommonAction showCancelBtn=true cancelUrl="groupingCode?groupType=${groupType!}"/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>
