/**
 * 
 */
package org.groupfio.user.mgmt.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sharif Ul Islam
 *
 */
public class UserMgmtConstants {
	
	// Resource bundles	
    public static final String configResource = "user-mgmt";
    public static final String uiLabelMap = "ResponsiveTemplateUiLabels";
    
    public static final int DEFAULT_BUFFER_SIZE = 102400;
    public static final int LOCKBOX_ITEM_SEQUENCE_ID_DIGITS = 5;
    
    public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	
	public static final class AppStatus {
        private AppStatus() { }
        public static final String ACTIVATED = "ACTIVATED";
        public static final String DEACTIVATED = "DEACTIVATED";
    }
	
}
