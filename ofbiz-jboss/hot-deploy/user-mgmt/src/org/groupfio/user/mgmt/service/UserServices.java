/**
 * 
 */
package org.groupfio.user.mgmt.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fio.crm.util.VirtualTeamUtil;
import org.groupfio.user.mgmt.util.DataUtil;
import org.groupfio.user.mgmt.util.PartyUtil;
import org.groupfio.user.mgmt.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class UserServices {

	private static final String MODULE = UserServices.class.getName();
    
    public static Map createUser(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String firstName = (String) context.get("firstName");
    	String lastName = (String) context.get("lastName");
    	String rmDisplayName = (String) context.get("rmDisplayName");
    	String mobileNoCountryCode = (String) context.get("mobileNoCountryCode");
    	String mobileNo = (String) context.get("mobileNo");
    	String officeNoCountryCode = (String) context.get("officeNoCountryCode");
    	String officeNo = (String) context.get("officeNo");
    	String emailAddress = (String) context.get("emailAddress");
    	String addressLine1 = (String) context.get("addressLine1");
    	String addressLine2 = (String) context.get("addressLine2");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String city = (String) context.get("city");
    	String postalCode = (String) context.get("postalCode");
    	String username = (String) context.get("username");
    	String roleTypeId = (String) context.get("roleTypeId");
    	String reportingPartyId = (String) context.get("reportingPartyId");

    	String emplTeamId = (String) context.get("emplTeamId");
    	String virtualTeamId = (String) context.get("virtualTeamId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	//result.put("groupId", groupId);
    	
    	try {
    		
    		List<GenericValue> toBeStored = FastList.newInstance();
    		
    		Timestamp importTimestamp = UtilDateTime.nowTimestamp();
    		
    		GenericValue rmUserLogin = EntityUtil.getFirst(delegator.findByAnd("UserLogin", UtilMisc.toMap("userLoginId", username), null, false));
    		if (UtilValidate.isNotEmpty(rmUserLogin)) {
    			result.putAll(ServiceUtil.returnError("User already exists!"));
    			return result;
    		}
        	
    		String rmPartyId = delegator.getNextSeqId("Party");
    		
    		toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("CUSTOMER", "BILL_TO_CUSTOMER"), delegator));
            GenericValue person = delegator.makeValue("Person", UtilMisc.toMap("partyId", rmPartyId, "firstName", firstName, "lastName", lastName));
            person.put("nickname", rmDisplayName);
            toBeStored.add(person);
            
            toBeStored.addAll(UtilImport.makePartyWithRolesExt(rmPartyId, "PERSON", rmPartyId,  UtilMisc.toList("ACCOUNT_MANAGER", "EMPLOYEE", roleTypeId), delegator));
            
            if (UtilValidate.isNotEmpty(emailAddress)) {
            	GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", emailAddress));
                toBeStored.add(emailContactMech);
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, rmPartyId, importTimestamp, delegator));
            }
            
            if (UtilValidate.isNotEmpty(mobileNoCountryCode) && UtilValidate.isNotEmpty(mobileNo)) {
            	GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, mobileNoCountryCode, null, mobileNo, delegator);
                toBeStored.add(contactMech);
                toBeStored.add(primaryNumber);
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_PHONE", primaryNumber, rmPartyId, importTimestamp, delegator));
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp, "extension", null, "allowSolicitation", "Y")));
            }
            
            if (UtilValidate.isNotEmpty(officeNoCountryCode) && UtilValidate.isNotEmpty(officeNo)) {
            	GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, officeNoCountryCode, null, officeNo, delegator);
                toBeStored.add(contactMech);
                toBeStored.add(primaryNumber);
                toBeStored.add(UtilImport.makeContactMechPurpose("PHONE_WORK_SEC", primaryNumber, rmPartyId, importTimestamp, delegator));
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp, "extension", null, "allowSolicitation", "Y")));
            }
            
            if (UtilValidate.isNotEmpty(addressLine1)) {
                // associate this as the GENERAL_LOCATION and BILLING_LOCATION
                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
                GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, null, firstName, lastName, rmDisplayName, addressLine1, addressLine2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
                toBeStored.add(contactMech);
                toBeStored.add(mainPostalAddress);
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, rmPartyId, importTimestamp, delegator));
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
            }
            
            if (UtilValidate.isEmpty(rmUserLogin)) {
            	rmUserLogin = delegator.makeValue("UserLogin");
            	rmUserLogin.put("userLoginId", username);
            	rmUserLogin.put("partyId", rmPartyId);
            	rmUserLogin.put("currentPassword", "{SHA}47b56994cbc2b6d10aa1be30f70165adb305a41a");
            	rmUserLogin.put("enabled", "Y");
            	rmUserLogin.put("countryGeoId", userLogin.getString("countryGeoId"));
            	toBeStored.add(rmUserLogin);
            } else {
            	rmUserLogin.put("enabled", "Y");
            	toBeStored.add(rmUserLogin);
            }
            
            GenericValue makerChecker = EntityUtil.getFirst(delegator.findByAnd("SystemProperty", UtilMisc.toMap("systemResourceId", "security", "systemPropertyId", username), null, false));
            if (UtilValidate.isEmpty(makerChecker)) {
            	makerChecker = delegator.makeValue("SystemProperty");
            	makerChecker.put("systemResourceId", "security");
            	makerChecker.put("systemPropertyId", username);
            	makerChecker.put("systemPropertyValue", "SG-I3BE-MAKERCHECKER");
            	makerChecker.put("description", "MAKERCHECKER");
            	toBeStored.add(makerChecker);
            }
            
            String reportingEmplPositionId = UtilImport.getPartyEmplPositionId(delegator, rmPartyId, roleTypeId, countryGeoId, city);
            if (UtilValidate.isEmpty(reportingEmplPositionId)) {
            	
            	reportingEmplPositionId = assignEmplPosition(delegator, toBeStored, rmPartyId, roleTypeId, countryGeoId, city);
            	
            } else {
            	GenericValue emplPosition = EntityUtil.getFirst(delegator.findByAnd("EmplPosition", UtilMisc.toMap("emplPositionId", reportingEmplPositionId), null, false));
            	if (UtilValidate.isNotEmpty(emplPosition)) {
            		
            		emplPosition.put("statusId", "EMPL_POS_OCCUPIED");
            		toBeStored.add(emplPosition);
            		
            	}
            }
            
            String managedEmplPositionId = null;
            GenericValue dbsRole = DataUtil.getFirstDbsRole(delegator, reportingPartyId);
            if (UtilValidate.isNotEmpty(dbsRole)) {
            	managedEmplPositionId = UtilImport.getPartyEmplPositionId(delegator, reportingPartyId, dbsRole.getString("roleTypeId"), null, null);
            }
            
            // construct reporting structure [start]
            
            EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
    				EntityCondition.makeCondition("emplPositionIdReportingTo", EntityOperator.EQUALS, reportingEmplPositionId),
    				EntityCondition.makeCondition("emplPositionIdManagedBy", EntityOperator.EQUALS, managedEmplPositionId),
    				EntityUtil.getFilterByDateExpr()
    				);
    		
    		GenericValue reportingStruct = EntityUtil.getFirst( delegator.findList("EmplPositionReportingStruct", searchConditions, null, null, null, false) );
    		if (UtilValidate.isEmpty(reportingStruct) 
    				|| ( UtilValidate.isNotEmpty(reportingStruct.getString("emplTeamId")) && !reportingStruct.getString("emplTeamId").equals(emplTeamId))) {
    			
    			if (UtilValidate.isNotEmpty(reportingStruct)) {
    				reportingStruct.put("thruDate", UtilDateTime.nowTimestamp());
    				toBeStored.add(reportingStruct);
    			}
    			
    			GenericValue rs = delegator.makeValue("EmplPositionReportingStruct");
    			rs.put("emplPositionIdReportingTo", reportingEmplPositionId);
    			rs.put("emplPositionIdManagedBy", managedEmplPositionId);
    			rs.put("emplTeamId", emplTeamId);
    			rs.put("fromDate", UtilDateTime.nowTimestamp());
    			rs.put("comments", "Created from user creation screen");
            	toBeStored.add(rs);
    			
    		}
            
    		delegator.storeAll(toBeStored);
    		
    		// construct reporting structure [end]
    		
    		if (UtilValidate.isNotEmpty(virtualTeamId)) {
    			Map<String, Object> teamContext = new HashMap<String, Object>();
    			teamContext.put("teamMemberPartyId", rmPartyId);
    			teamContext.put("accountTeamPartyId", virtualTeamId);
    			//teamContext.put("securityGroupId", "VT_SG_TM");
    			teamContext.put("userLogin", userLogin);
    			
    			dispatcher.runSync("usermgmt.addTeamMember", teamContext);
    		}
    		
    		result.put("partyId", rmPartyId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created User .."));
    	
    	return result;
    	
    }
    
    public static Map updateUser(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String partyId = (String) context.get("partyId");
    	
    	String firstName = (String) context.get("firstName");
    	String lastName = (String) context.get("lastName");
    	String rmDisplayName = (String) context.get("rmDisplayName");
    	String mobileNoCountryCode = (String) context.get("mobileNoCountryCode");
    	String mobileNo = (String) context.get("mobileNo");
    	String officeNoCountryCode = (String) context.get("officeNoCountryCode");
    	String officeNo = (String) context.get("officeNo");
    	String emailAddress = (String) context.get("emailAddress");
    	String addressLine1 = (String) context.get("addressLine1");
    	String addressLine2 = (String) context.get("addressLine2");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String postalCode = (String) context.get("postalCode");
    	String username = (String) context.get("username");
    	String roleTypeId = (String) context.get("roleTypeId");
    	String reportingPartyId = (String) context.get("reportingPartyId");
    	
    	String emplTeamId = (String) context.get("emplTeamId");
    	String virtualTeamId = (String) context.get("virtualTeamId");
    	
    	String city = (String) context.get("city");
    	String previousCity = (String) context.get("city");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	//result.put("groupId", groupId);
    	
    	try {
    		
    		List<GenericValue> toBeStored = FastList.newInstance();
    		
    		Timestamp importTimestamp = UtilDateTime.nowTimestamp();
        	
    		GenericValue rmUserLogin = EntityUtil.getFirst(delegator.findByAnd("UserLogin", UtilMisc.toMap("partyId", partyId), null, false));
    		if (UtilValidate.isEmpty(rmUserLogin)) {
    			result.putAll(ServiceUtil.returnError("User not exists!"));
    			return result;
    		}
    		
    		String rmPartyId = rmUserLogin.getString("partyId");
    		String partyName = PartyUtil.getPartyName(delegator, rmPartyId, false);
    		
    		String userPositionType = null;
            GenericValue dbsRole = DataUtil.getFirstDbsRole(delegator, rmPartyId);
            if (UtilValidate.isNotEmpty(dbsRole)) {
            	userPositionType = dbsRole.getString("roleTypeId");
            }
    		
    		GenericValue person = EntityUtil.getFirst(delegator.findByAnd("Person", UtilMisc.toMap("partyId", rmPartyId), null, false));
			if (UtilValidate.isNotEmpty(person)) {
				person.put("firstName", firstName);
				person.put("lastName", lastName);
				person.put("nickname", rmDisplayName);
				toBeStored.add(person);
			}
			
			dbsRole = delegator.makeValue("PartyRole", UtilMisc.toMap("partyId", rmPartyId, "roleTypeId", roleTypeId));
			toBeStored.add(dbsRole);
			
         	GenericValue emailPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, rmPartyId, "PRIMARY_EMAIL", null);
         	if(UtilValidate.isNotEmpty(emailPurpose)) {
         		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", emailPurpose.getString("contactMechId")));
             	
         		if (UtilValidate.isNotEmpty(emailContactMech.getString("infoString")) && !emailAddress.equals(emailContactMech.getString("infoString"))) {
         			emailContactMech.put("infoString", emailAddress);
         			toBeStored.add(emailContactMech);
		 		}
         		
         	} else if (!UtilValidate.isEmpty(emailAddress)) {
                // make the email address
                GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", emailAddress));
                toBeStored.add(emailContactMech);
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", UtilDateTime.nowTimestamp(), "allowSolicitation", "Y", "emailValidInd", "Y")));
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, rmPartyId, UtilDateTime.nowTimestamp(), delegator));
         	}
         	
         	GenericValue mobilePurpose = DataUtil.getActivePartyContactMechPurpose(delegator, rmPartyId, "PRIMARY_PHONE", null);
         	if(UtilValidate.isNotEmpty(mobilePurpose)){
         		//GenericValue mobileContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", mobilePurpose.getString("contactMechId")));
         		String contactMechId = mobilePurpose.getString("contactMechId");
        		
        		GenericValue phoneContactMech = EntityUtil.getFirst(delegator.findByAnd("TelecomNumber",  UtilMisc.toMap("contactMechId", contactMechId), null, false));
        		if (UtilValidate.isNotEmpty(phoneContactMech)) {
        			//phoneContactMech.put("contactMechId", contactMechId);
	        		phoneContactMech.put("countryCode", mobileNoCountryCode);
	        		//phoneContactMech.put("areaCode", entry.getString("primaryPhoneAreaCode"));
	        		phoneContactMech.put("contactNumber", mobileNo);
	        		toBeStored.add(phoneContactMech);
        		}
         	} else if (!UtilValidate.isEmpty(mobileNo)) {
                // make the mobile no
                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
                
                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, mobileNoCountryCode, null, mobileNo, delegator);
                
                toBeStored.add(contactMech);
                toBeStored.add(primaryNumber);
                
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp, "allowSolicitation", "Y")));
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_PHONE", primaryNumber, rmPartyId, importTimestamp, delegator));
            }
         	
         	GenericValue officeNoPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, rmPartyId, "PHONE_WORK_SEC", null);
         	if(UtilValidate.isNotEmpty(officeNoPurpose)){
         		//GenericValue mobileContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId", mobilePurpose.getString("contactMechId")));
         		String contactMechId = officeNoPurpose.getString("contactMechId");
        		
        		GenericValue phoneContactMech = EntityUtil.getFirst(delegator.findByAnd("TelecomNumber",  UtilMisc.toMap("contactMechId", contactMechId), null, false));
        		if (UtilValidate.isNotEmpty(phoneContactMech)) {
        			//phoneContactMech.put("contactMechId", contactMechId);
	        		phoneContactMech.put("countryCode", officeNoCountryCode);
	        		//phoneContactMech.put("areaCode", entry.getString("primaryPhoneAreaCode"));
	        		phoneContactMech.put("contactNumber", officeNo);
	        		toBeStored.add(phoneContactMech);
        		}
         	} else if (!UtilValidate.isEmpty(officeNo)) {
                // make the mobile no
                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "TELECOM_NUMBER"));
                
                GenericValue primaryNumber = UtilImport.makeTelecomNumber(contactMech, officeNoCountryCode, null, officeNo, delegator);
                
                toBeStored.add(contactMech);
                toBeStored.add(primaryNumber);
                
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp, "allowSolicitation", "Y")));
                toBeStored.add(UtilImport.makeContactMechPurpose("PHONE_WORK_SEC", primaryNumber, rmPartyId, importTimestamp, delegator));
            }
         	
         	GenericValue findPostalContactMech = DataUtil.getActivePartyContactMechPurpose(delegator, rmPartyId, "PRIMARY_LOCATION", null);
        	if(UtilValidate.isNotEmpty(findPostalContactMech)){
        		String contactMechId = findPostalContactMech.getString("contactMechId");
        		GenericValue postalContactMech = delegator.findOne("PostalAddress", false, UtilMisc.toMap("contactMechId",contactMechId));
        		
        		previousCity = postalContactMech.getString("city");
        		
        		postalContactMech.put("toName", partyName);
        		postalContactMech.put("attnName", rmDisplayName);
        		postalContactMech.put("address1", addressLine1);
        		postalContactMech.put("address2", addressLine2);
        		postalContactMech.put("city", city);
        		postalContactMech.put("postalCode", postalCode);
        		postalContactMech.put("postalCodeExt", null);
        		postalContactMech.put("countryGeoId", countryGeoId);
        		postalContactMech.put("stateProvinceGeoId", stateProvinceGeoId);
        		toBeStored.add(postalContactMech);
        	} else if (UtilValidate.isNotEmpty(addressLine1)) {
                // associate this as the GENERAL_LOCATION and BILLING_LOCATION
                GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
                GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, null, firstName, lastName, rmDisplayName, addressLine1, addressLine2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
                toBeStored.add(contactMech);
                toBeStored.add(mainPostalAddress);
                toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, rmPartyId, importTimestamp, delegator));
                toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", contactMech.get("contactMechId"), "partyId", rmPartyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
            }
        	
        	String reportingEmplPositionId = UtilImport.getPartyEmplPositionId(delegator, rmPartyId, roleTypeId, countryGeoId, city);
            if (UtilValidate.isEmpty(reportingEmplPositionId)) {
            	
            	reportingEmplPositionId = assignEmplPosition(delegator, toBeStored, rmPartyId, roleTypeId, countryGeoId, city);
            	
            } else {
            	GenericValue emplPosition = EntityUtil.getFirst(delegator.findByAnd("EmplPosition", UtilMisc.toMap("emplPositionId", reportingEmplPositionId), null, false));
            	if (UtilValidate.isNotEmpty(emplPosition)) {
            		
            		emplPosition.put("statusId", "EMPL_POS_OCCUPIED");
            		toBeStored.add(emplPosition);
            		
            	}
            }
            
            String managedEmplPositionId = null;
            dbsRole = DataUtil.getFirstDbsRole(delegator, reportingPartyId);
            if (UtilValidate.isNotEmpty(dbsRole)) {
            	managedEmplPositionId = UtilImport.getPartyEmplPositionId(delegator, reportingPartyId, dbsRole.getString("roleTypeId"), null, null);
            }
        	
        	// clean previous position [start]
        	
        	if ( (UtilValidate.isNotEmpty(userPositionType) && UtilValidate.isNotEmpty(roleTypeId) && !userPositionType.equals(roleTypeId))
        			|| (UtilValidate.isNotEmpty(city) && UtilValidate.isNotEmpty(previousCity) && !city.equals(previousCity))
        			) {
        		GenericValue userEmplPosition = UtilImport.getPartyEmplPosition(delegator, rmPartyId, userPositionType);
	            if (UtilValidate.isNotEmpty(userEmplPosition) && !userEmplPosition.getString("emplPositionId").equals(reportingEmplPositionId)) {
	            	String userEmplPositionId = userEmplPosition.getString("emplPositionId");
	            	GenericValue emplPosition = EntityUtil.getFirst(delegator.findByAnd("EmplPosition", UtilMisc.toMap("emplPositionId", userEmplPositionId), null, false));
		        	if (UtilValidate.isNotEmpty(emplPosition)) {
		        		userEmplPosition.put("thruDate", UtilDateTime.nowTimestamp());
		        		toBeStored.add(userEmplPosition);
		        		
		        		emplPosition.put("statusId", "EMPL_POS_ACTIVE");
		        		toBeStored.add(emplPosition);
		        		
		        	}
	            }
	            
        	}
        	
        	if ( (UtilValidate.isNotEmpty(userPositionType) && UtilValidate.isNotEmpty(roleTypeId) && !userPositionType.equals(roleTypeId))
        			) {
        		GenericValue existsDbsRole = EntityUtil.getFirst(delegator.findByAnd("PartyRole", UtilMisc.toMap("partyId", rmPartyId, "roleTypeId", userPositionType), null, false));
				if (UtilValidate.isNotEmpty(existsDbsRole)) {
					existsDbsRole.remove();
				}
        	}
        	
        	GenericValue userReportingStruct = UtilImport.getEmplPositionReportingStruct(delegator, rmPartyId, userPositionType);
        	if (UtilValidate.isNotEmpty(userReportingStruct)) {
        		userReportingStruct.remove();
    			/*userReportingStruct.put("thruDate", UtilDateTime.nowTimestamp());
        		toBeStored.add(userReportingStruct);*/
        	}
        	
        	// clean previous position [end]
    		
            // construct reporting structure [start]
            
            /*EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
    				EntityCondition.makeCondition("emplPositionIdReportingTo", EntityOperator.EQUALS, reportingEmplPositionId),
    				EntityCondition.makeCondition("emplPositionIdManagedBy", EntityOperator.EQUALS, managedEmplPositionId),
    				EntityUtil.getFilterByDateExpr()
    				);
    		
    		GenericValue reportingStruct = EntityUtil.getFirst( delegator.findList("EmplPositionReportingStruct", searchConditions, null, null, null, false) );
    		if (UtilValidate.isEmpty(reportingStruct)) {*/
    			
    			GenericValue rs = delegator.makeValue("EmplPositionReportingStruct");
    			rs.put("emplPositionIdReportingTo", reportingEmplPositionId);
    			rs.put("emplPositionIdManagedBy", managedEmplPositionId);
    			rs.put("emplTeamId", emplTeamId);
    			rs.put("fromDate", UtilDateTime.nowTimestamp());
    			rs.put("comments", "Created from user update screen");
            	toBeStored.add(rs);
    			
    		//}
    		
    		// construct reporting structure [end]
    		
    		delegator.storeAll(toBeStored);
    		
    		if (UtilValidate.isNotEmpty(virtualTeamId)) {
    			
    			Map<String, Object> virtualTeam = VirtualTeamUtil.getFirstVirtualTeamMember(delegator, null, rmPartyId);
    			if (UtilValidate.isEmpty(virtualTeam) || !virtualTeam.get("virtualTeamId").equals(virtualTeamId)) {
    				EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
            				EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, rmPartyId),
            				EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT_TEAM"),
            				EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
            				EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "ASSIGNED_TO"),
            				EntityUtil.getFilterByDateExpr()
            				);
            		
            		List<GenericValue> teamMermbers = delegator.findList("PartyRelationship", searchConditions, null, null, null, false);
            		for (GenericValue teamMermber : teamMermbers) {
            			teamMermber.remove();
            		}
            		
        			Map<String, Object> teamContext = new HashMap<String, Object>();
        			teamContext.put("teamMemberPartyId", rmPartyId);
        			teamContext.put("accountTeamPartyId", virtualTeamId);
        			//teamContext.put("securityGroupId", "VT_SG_TM");
        			teamContext.put("userLogin", userLogin);
        			
        			dispatcher.runSync("usermgmt.addTeamMember", teamContext);
    			}
    			
    		}
    		
    		result.put("partyId", rmPartyId);
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated User .."));
    	
    	return result;
    	
    }
    
    public static Map deleteUser(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String username = (String) context.get("username");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
    		
    		GenericValue rmUserLogin = EntityUtil.getFirst(delegator.findByAnd("UserLogin", UtilMisc.toMap("userLoginId", username), null, false));
    		if (UtilValidate.isEmpty(rmUserLogin)) {
    			result.putAll(ServiceUtil.returnError("User not exists!"));
    			return result;
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted User.."));
    	
    	return result;
    	
    }
    
    private static String assignEmplPosition(Delegator delegator, List<GenericValue> toBeStored, String partyId, String positionType, String countryGeo, String city) {
    	String emplPositionId = null;
    	GenericValue vacantPosition = UtilImport.getVacantEmplPosition(delegator, positionType, countryGeo, city);
    	if (UtilValidate.isEmpty(vacantPosition)) {
    		vacantPosition = delegator.makeValue("EmplPosition");
    		
    		emplPositionId = delegator.getNextSeqId("EmplPosition");
    		
    		vacantPosition.put("emplPositionId", emplPositionId);
    		vacantPosition.put("statusId", "EMPL_POS_OCCUPIED");
    		vacantPosition.put("partyId", "Company");
    		vacantPosition.put("emplPositionTypeId", positionType);
    		vacantPosition.put("actualFromDate", UtilDateTime.nowTimestamp());
    		vacantPosition.put("countryGeoId", countryGeo);
    		vacantPosition.put("city", city);
    	} else {
    		emplPositionId = vacantPosition.getString("emplPositionId");
    		vacantPosition.put("statusId", "EMPL_POS_OCCUPIED");
    	}
    	toBeStored.add(vacantPosition);
    	
    	GenericValue fulfillment = delegator.makeValue("EmplPositionFulfillment");
    	fulfillment.put("emplPositionId", emplPositionId);
    	fulfillment.put("partyId", partyId);
    	fulfillment.put("fromDate", UtilDateTime.nowTimestamp());
    	fulfillment.put("comments", "Created from import process");
    	toBeStored.add(fulfillment);
    	
    	return emplPositionId;
    }
	
}
