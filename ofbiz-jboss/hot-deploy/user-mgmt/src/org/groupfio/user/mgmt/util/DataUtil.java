/**
 * 
 */
package org.groupfio.user.mgmt.util;

import java.util.ArrayList;
import java.util.List;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Sharif
 *
 */
public class DataUtil {

	public static GenericValue getFirstDbsRole(Delegator delegator, String partyId) {
		
		try {
			if (UtilValidate.isNotEmpty(partyId)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("roleTypeId", EntityOperator.LIKE, "DBS_%"),
						EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)
						), EntityOperator.AND);
				List<GenericValue> dbsRoleList = delegator.findList("PartyRole", condition, null, null, null, false);
				if (UtilValidate.isNotEmpty(dbsRoleList)) {
					return dbsRoleList.get(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static List<GenericValue> getDbsRoleList(Delegator delegator) {
		
		List<GenericValue> dbsRoleList = new ArrayList<GenericValue>();
		
		try {
			
			EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition("roleTypeId", EntityOperator.LIKE, "DBS_%")
					), EntityOperator.AND);
			
			dbsRoleList = delegator.findList("RoleType", condition, null, null, null, false);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbsRoleList;
	}
	
	public static GenericValue getActivePartyContactMechPurpose(Delegator delegator, String partyId, String contactMechPurposeTypeId, String partyRelAssocId) {
		GenericValue partyContactMechPurpose = null;
		try {
			if (UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(contactMechPurposeTypeId)) {
				EntityCondition searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
						EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, contactMechPurposeTypeId),
						EntityUtil.getFilterByDateExpr());
				
				if (UtilValidate.isNotEmpty(partyRelAssocId)) {
					searchConditions = EntityCondition.makeCondition(EntityOperator.AND,
							EntityCondition.makeCondition("partyRelAssocId", EntityOperator.EQUALS, partyRelAssocId),
							searchConditions);
				}
				
				partyContactMechPurpose = EntityUtil.getFirst( delegator.findList("PartyContactMechPurpose", searchConditions,null, null, null, false) );
				return partyContactMechPurpose;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return partyContactMechPurpose;
	}
	
}
