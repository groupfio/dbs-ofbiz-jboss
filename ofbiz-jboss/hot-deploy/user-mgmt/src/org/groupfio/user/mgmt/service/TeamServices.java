package org.groupfio.user.mgmt.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.groupfio.user.mgmt.util.PartyHelper;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.datasource.GenericHelperInfo;
import org.ofbiz.entity.jdbc.SQLProcessor;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.security.Security;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;

public class TeamServices {

    private static final String MODULE = TeamServices.class.getName();
    public static final String resource = "user-mgmtUiLabels";

    public static Map createUpdateTeam(DispatchContext dctx, Map context) {

        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String emplTeamId = (String) context.get("emplTeamId");
        String teamName = (String) context.get("teamName");
        String description = (String) context.get("description");
        String countryGeoId = (String) context.get("countryGeoId");

        Map < String, Object > result = new HashMap < String, Object > ();
        String msg = null;
        try {

            if (UtilValidate.isNotEmpty(teamName)) {
                teamName = teamName.trim();
                GenericValue emplTeam = null;
                if (UtilValidate.isNotEmpty(emplTeamId)) {
                    emplTeam = delegator.findOne("EmplTeam", UtilMisc.toMap("emplTeamId", emplTeamId), false);
                    if (emplTeam != null && emplTeam.size() > 0) {
                        emplTeam.put("teamName", teamName);
                        emplTeam.put("description", description);
                        emplTeam.put("countryGeoId", countryGeoId);
                        emplTeam.store();

                        msg = "Team updated successfully.";
                    }
                } else {
                    List<GenericValue> teamNameValidation = delegator.findByAnd("EmplTeam", UtilMisc.toMap("teamName", teamName), null, false);
                    if(teamNameValidation != null && teamNameValidation.size() > 0) {
                        result.putAll(ServiceUtil.returnError("Team name already exists"));
                        return result;
                    }
                    emplTeam = delegator.makeValue("EmplTeam");
                    emplTeamId = delegator.getNextSeqId("EmplTeam");
                    emplTeam.put("emplTeamId", emplTeamId);
                    emplTeam.put("teamName", teamName);
                    emplTeam.put("description", description);
                    emplTeam.put("countryGeoId", countryGeoId);
                    emplTeam.put("isActive", "Y");
                    emplTeam.create();

                    msg = "Team created successfully.";
                }
            }

        } catch (Exception e) {
            Debug.logError("Error" + e.getMessage(), MODULE);
            result.putAll(ServiceUtil.returnError("Failed " + e.getMessage()));
            return result;
        }

        result.putAll(ServiceUtil.returnSuccess(msg));
        result.put("emplTeamId", emplTeamId);
        return result;

    }
    
    public static Map updateTeamToReportingStruct(DispatchContext dctx, Map context) {
        
        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String partyId = (String) context.get("partyId");
        String emplTeamId = (String) context.get("emplTeamId");
        Map< String, Object > result = new HashMap < String, Object > ();
        String msg = "Selected member cannot be added to the team";
        try {
            if(UtilValidate.isNotEmpty(partyId) && UtilValidate.isNotEmpty(emplTeamId)) {
                GenericValue emplPositionFulfillment = EntityQuery.use(delegator).from("EmplPositionFulfillment")
                    .where("partyId", partyId).filterByDate().queryFirst();
                if(emplPositionFulfillment != null && emplPositionFulfillment.size() > 0) {
                    GenericValue emplPositionReportingStruct = EntityQuery.use(delegator).from("EmplPositionReportingStruct")
                            .where("emplPositionIdReportingTo", emplPositionFulfillment.getString("emplPositionId"))
                            .filterByDate().queryFirst();
                    if(emplPositionReportingStruct != null && emplPositionReportingStruct.size() > 0) {
                        emplPositionReportingStruct.put("emplTeamId", emplTeamId);
                        emplPositionReportingStruct.store();
                        msg = "Team member added into the team successfully";
                    }
                }
            }
        
        } catch(Exception e) {
            Debug.log("Exception");
        }
        
        result.putAll(ServiceUtil.returnSuccess(msg));
        result.put("emplTeamId", emplTeamId);
        return result;
    	
    	
    }
    
    public static Map removeTeamToReportingStruct(DispatchContext dctx, Map context) {
        
        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String emplTeamId = (String) context.get("emplTeamId");
        String reportingTo = (String) context.get("reportingTo");
        Map< String, Object > result = new HashMap < String, Object > ();
        String msg = null;
        try {
            if(UtilValidate.isNotEmpty(emplTeamId) && UtilValidate.isNotEmpty(reportingTo)) {
                    GenericValue emplPositionReportingStruct = EntityQuery.use(delegator).from("EmplPositionReportingStruct")
                        .where("emplPositionIdReportingTo", reportingTo)
                        .filterByDate().queryFirst();
                    if(emplPositionReportingStruct != null && emplPositionReportingStruct.size() > 0) {
                        emplPositionReportingStruct.put("emplTeamId", "");
                        emplPositionReportingStruct.store();
                        msg = "Member removed from the team successfully.";
                    }
                
            }
        
        } catch(Exception e) {
            Debug.log("Exception");
        }
        
        result.putAll(ServiceUtil.returnSuccess(msg));
        result.put("emplTeamId", emplTeamId);
        return result;
    
    }
    
    public static Map<String, Object> createTeam(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        Security security = dctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String groupName = (String) context.get("groupName");
        String comments = (String) context.get("comments");
        String countryGeoId = (String) context.get("countryGeoId");
        Map<String, Object> result = ServiceUtil.returnSuccess();
        try {
            if(UtilValidate.isNotEmpty(groupName)) {
            groupName = groupName.trim();
            List < EntityCondition > conditionList = FastList.newInstance();
            
            conditionList.add(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_TEAM"));
            conditionList.add(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"));
            if (UtilValidate.isNotEmpty(groupName)) {
                conditionList.add(EntityCondition.makeCondition("groupName", EntityOperator.EQUALS, groupName));
            }
            if (UtilValidate.isNotEmpty(countryGeoId)) {
                conditionList.add(EntityCondition.makeCondition("teamCountryGeoId", EntityOperator.EQUALS, countryGeoId));
            }
            
            List < GenericValue > teamNameValidation = delegator.findList("PartyRoleStatusAndPartyDetail", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
            if(teamNameValidation != null && teamNameValidation.size() > 0) {
                result.putAll(ServiceUtil.returnError("Team name already exists"));
                return result;
            }
            
            Map<String, Object> servRes = dispatcher.runSync("createPartyGroup", UtilMisc.toMap("groupName", groupName, "comments", comments,"teamCountryGeoId", countryGeoId, "userLogin", userLogin));
            if (ServiceUtil.isError(servRes)) {
                return servRes;
            }
            String partyId = (String) servRes.get("partyId");

            servRes = dispatcher.runSync("createPartyRole", UtilMisc.toMap("partyId", partyId, "roleTypeId", "ACCOUNT_TEAM", "userLogin", userLogin));
            if (ServiceUtil.isError(servRes)) {
                return servRes;
            }

            /*// we'll use the system user to add the userLogin to the team
            GenericValue system = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", "system"), false);
            servRes = dispatcher.runSync("usermgmt.addTeamMember", UtilMisc.toMap("teamMemberPartyId", userLogin.get("partyId"),
                        "accountTeamPartyId", partyId, "securityGroupId", "SALES_MANAGER", "userLogin", system));
            if (ServiceUtil.isError(servRes)) {
                return servRes;
            }*/

            result.putAll(ServiceUtil.returnSuccess("Team created successfully."));
            result.put("partyId", partyId);
            
            }
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in create team "+e.getMessage());
        }
        return result;
    }

    public static Map<String, Object> updateTeam(DispatchContext dctx, Map<String, Object> context) {
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        Security security = dctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String partyId = (String) context.get("partyId");
        String groupName = (String) context.get("groupName");
        String comments = (String) context.get("comments");
        String countryGeoId = (String) context.get("countryGeoId");

        try {
            Map<String, Object> servRes = dispatcher.runSync("updatePartyGroup", UtilMisc.toMap("partyId", partyId, "groupName", groupName, "comments", comments, "teamCountryGeoId", countryGeoId, "userLogin", userLogin));
            if (ServiceUtil.isError(servRes)) {
                return servRes;
            }
            return ServiceUtil.returnSuccess("Team updated successfully.");
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in update team "+e.getMessage());
        }
    }

    public static Map<String, Object> deactivateTeam(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        
        GenericHelperInfo ghi = delegator.getGroupHelperInfo("org.ofbiz");
		SQLProcessor sqlProcessor = new SQLProcessor(delegator, ghi);

        String teamPartyId = (String) context.get("partyId");

        try {
            // expire all active relationships for team
            List<GenericValue> relationships = EntityQuery.use(delegator).from("PartyRelationship")
                    .where(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, teamPartyId))
                    .filterByDate().queryList();
            PartyHelper.expirePartyRelationships(relationships, UtilDateTime.nowTimestamp(), dispatcher, userLogin);

            // set the team party to PARTY_DISABLED (note: if your version of ofbiz fails to do this, then replace this with direct entity ops)
            Map<String, Object> servRes = dispatcher.runSync("setPartyStatus", UtilMisc.toMap("partyId", teamPartyId, "statusId", "PARTY_DISABLED", "userLogin", userLogin));
            if (ServiceUtil.isError(servRes)) {
                return servRes;
            }

            // void all lead and team association [start]
            
            String updateQuery = "update party_supplemental_data set virtual_team_id=null where virtual_team_id='"+teamPartyId+"'";
			sqlProcessor.prepareStatement(updateQuery);
			sqlProcessor.executeUpdate(updateQuery);
            
            // void all lead and team association [end]
            
            return ServiceUtil.returnSuccess("Team deactivated successfully.");
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in deactivate team "+e.getMessage());
        }
    }
    
    public static Map<String, Object> addTeamMember(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String teamMemberPartyId = (String) context.get("teamMemberPartyId");
        String accountTeamPartyId = (String) context.get("accountTeamPartyId");
        String securityGroupId = (String) context.get("securityGroupId");

        try {
            // find out whether the accountTeamPartyId is an account or team
            String roleTypeIdFrom = PartyHelper.getFirstValidRoleTypeId(accountTeamPartyId, UtilMisc.toList("ACCOUNT", "ACCOUNT_TEAM"), delegator);
            if (UtilValidate.isEmpty(roleTypeIdFrom)) {
                return ServiceUtil.returnError("Invalid role type id from");
            }

            // the the first valid role for the team member (which could be either ACCOUNT_MANAGER, ACCOUNT_REP, or CUST_SERVICE_REP)
            String roleTypeIdTo = PartyHelper.getFirstValidRoleTypeId(teamMemberPartyId, UtilMisc.toList("ACCOUNT_MANAGER", "ACCOUNT_REP", "CUST_SERVICE_REP"), delegator);
            if (UtilValidate.isEmpty(roleTypeIdTo)) {
                return ServiceUtil.returnError("Invalid role type id to");
            }

            // find out if the candidate is already a member in this role
            List<GenericValue> relationships = delegator.findByAnd("PartyRelationship", UtilMisc.toMap("partyIdFrom", accountTeamPartyId, "partyRelationshipTypeId", "ASSIGNED_TO",
                        "roleTypeIdFrom", roleTypeIdFrom, "partyIdTo", teamMemberPartyId, "roleTypeIdTo", roleTypeIdTo), null, false);
            List<GenericValue> activeRelations = EntityUtil.filterByDate(relationships, UtilDateTime.nowTimestamp()); // filter out expired relationships
            if (activeRelations.size() > 0) {
                return ServiceUtil.returnError("Error in team member already in the team ");
            }

            // Ensure the PartyRoles
            Map<String, Object> ensureResult = dispatcher.runSync("ensurePartyRole", UtilMisc.toMap("partyId", accountTeamPartyId, "roleTypeId", roleTypeIdFrom, "userLogin", userLogin));
            if (ServiceUtil.isError(ensureResult)) {
                return ServiceUtil.returnError("Error in team member already in the team "+ ensureResult);
            }
            ensureResult = dispatcher.runSync("ensurePartyRole", UtilMisc.toMap("partyId", teamMemberPartyId, "roleTypeId", roleTypeIdTo, "userLogin", userLogin));
            if (ServiceUtil.isError(ensureResult)) {
                return ServiceUtil.returnError("Error in team member already in the team "+ ensureResult);
            }

            // create the PartyRelationship
            Map<String, Object> input = UtilMisc.toMap("partyIdFrom", accountTeamPartyId, "roleTypeIdFrom", roleTypeIdFrom, "partyIdTo", teamMemberPartyId, "roleTypeIdTo", roleTypeIdTo);
            input.put("partyRelationshipTypeId", "ASSIGNED_TO");
            input.put("securityGroupId", securityGroupId);
            input.put("fromDate", UtilDateTime.nowTimestamp());
            input.put("userLogin", userLogin);
            Map<String, Object> serviceResults = dispatcher.runSync("createPartyRelationship", input);
            if (ServiceUtil.isError(serviceResults)) {
                return ServiceUtil.returnError("Error in team member added into the team "+ serviceResults);
            }
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in team member added into the team " +e.getMessage());
        } 
        return ServiceUtil.returnSuccess("Team member added into the team successfully");
    }

    public static Map<String, Object> removeTeamMember(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String teamMemberPartyId = (String) context.get("teamMemberPartyId");
        String accountTeamPartyId = (String) context.get("accountTeamPartyId");

        try {
            // expire any active relationships between the team member and the account or team
            String roleTypeIdFrom = PartyHelper.getFirstValidRoleTypeId(accountTeamPartyId, UtilMisc.toList("ACCOUNT", "ACCOUNT_TEAM"), delegator);
            List<GenericValue> relationships = findActiveAccountOrTeamRelationships(accountTeamPartyId, roleTypeIdFrom, teamMemberPartyId, delegator);
            PartyHelper.expirePartyRelationships(relationships, UtilDateTime.nowTimestamp(), dispatcher, userLogin);
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in team member remove "+e.getMessage());
        }
        return ServiceUtil.returnSuccess("Team member removed successfully.");
    }

    public static Map<String, Object> setTeamMemberSecurityGroup(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String teamMemberPartyId = (String) context.get("teamMemberPartyId");
        String accountTeamPartyId = (String) context.get("accountTeamPartyId");
        String securityGroupId = (String) context.get("securityGroupId");

        try {
            // expire any active relationships between the team member and the account or team
            String roleTypeIdFrom = PartyHelper.getFirstValidRoleTypeId(accountTeamPartyId, UtilMisc.toList("ACCOUNT", "ACCOUNT_TEAM"), delegator);
            List<GenericValue> relationships = findActiveAccountOrTeamRelationships(accountTeamPartyId, roleTypeIdFrom, teamMemberPartyId, delegator);
            PartyHelper.expirePartyRelationships(relationships, UtilDateTime.nowTimestamp(), dispatcher, userLogin);

            // recreate the relationships with the new security group
            for (Iterator iter = relationships.iterator(); iter.hasNext();) {
                GenericValue relationship = (GenericValue) iter.next();

                Map<String, Object> input = UtilMisc.toMap("partyIdFrom", accountTeamPartyId, "roleTypeIdFrom", roleTypeIdFrom, "partyIdTo", teamMemberPartyId, "roleTypeIdTo", relationship.getString("roleTypeIdTo"));
                input.put("partyRelationshipTypeId", "ASSIGNED_TO");
                input.put("securityGroupId", securityGroupId);
                input.put("fromDate", UtilDateTime.nowTimestamp());
                input.put("userLogin", userLogin);
                Map<String, Object> serviceResults = dispatcher.runSync("createPartyRelationship", input);
                if (ServiceUtil.isError(serviceResults)) {
                    return ServiceUtil.returnError("Error in team member update " +serviceResults);
                }
            }
            
        } catch (Exception e) {
            return ServiceUtil.returnError("Error in team member update " +e.getMessage());
        }
        
        return ServiceUtil.returnSuccess("Team member updated successfully");
    }
    
    public static List<GenericValue> findActiveAccountOrTeamRelationships(String accountTeamPartyId, String roleTypeIdFrom, String teamMemberPartyId, Delegator delegator) throws GenericEntityException {
        EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
                    EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, roleTypeIdFrom),
                    EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, accountTeamPartyId),
                    EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, teamMemberPartyId),
                    EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "ASSIGNED_TO"),
                    EntityUtil.getFilterByDateExpr());
        return delegator.findList("PartyRelationship",  conditions, null, null, null, false);
    }
}