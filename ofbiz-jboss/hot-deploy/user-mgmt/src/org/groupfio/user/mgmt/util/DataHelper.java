/**
 * 
 */
package org.fio.usermgmt.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.entity.GenericValue;

/**
 * @author Sharif
 *
 */
public class DataHelper {
	
	private static String MODULE = DataHelper.class.getName();
	
	public static Map getDropDownOptions(List<GenericValue> entityList, String keyField, String desField){
		Map<String, Object> options = new HashMap<String, Object>();
		for (GenericValue entity : entityList) {
			options.put(entity.getString(keyField), entity.getString(desField));
		}
		return options;
	}	
}
