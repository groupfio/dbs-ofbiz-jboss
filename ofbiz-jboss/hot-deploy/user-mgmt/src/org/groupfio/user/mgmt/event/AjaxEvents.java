package org.groupfio.user.mgmt.event;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fio.crm.util.VirtualTeamUtil;
import org.groupfio.user.mgmt.constants.ResponseCodes;
import org.groupfio.user.mgmt.constants.UserMgmtConstants;
import org.groupfio.user.mgmt.util.DataUtil;
import org.groupfio.user.mgmt.util.PartyUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

import javolution.util.FastList;
import javolution.util.FastMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AjaxEvents {

    private AjaxEvents() { }

    private static final String MODULE = AjaxEvents.class.getName();

    public static String doJSONResponse(HttpServletResponse response, JSONObject jsonObject) {
        return doJSONResponse(response, jsonObject.toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Collection<?> collection) {
        return doJSONResponse(response, JSONArray.fromObject(collection).toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Map map) {
        return doJSONResponse(response, JSONObject.fromObject(map));
    }

    public static String doJSONResponse(HttpServletResponse response, String jsonString) {
        String result = "success";

        response.setContentType("application/x-json");
        try {
            response.setContentLength(jsonString.getBytes("UTF-8").length);
        } catch (UnsupportedEncodingException e) {
            Debug.logWarning("Could not get the UTF-8 json string due to UnsupportedEncodingException: " + e.getMessage(), MODULE);
            response.setContentLength(jsonString.length());
        }

        Writer out;
        try {
            out = response.getWriter();
            out.write(jsonString);
            out.flush();
        } catch (IOException e) {
            Debug.logError(e, "Failed to get response writer", MODULE);
            result = "error";
        }
        return result;
    }
    
    public static GenericValue getUserLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (GenericValue) session.getAttribute("userLogin");
    }
    
    public static String getTeamMembers(HttpServletRequest request, HttpServletResponse response) {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String teamName = request.getParameter("teamName");
        String countryGeoId = request.getParameter("countryGeoId");
        Map<String, Object> resp = new HashMap<String, Object> ();
        JSONArray datas = new JSONArray();

        List<EntityCondition> conditionList = FastList.newInstance();

        int recordsFiltered = 0;
        int recordsTotal = 0;
        try {
            String sortDir = "";
            String orderField = "";
            String orderColumnId = request.getParameter("order[0][column]");
            if (UtilValidate.isNotEmpty(orderColumnId)) {
                int sortColumnId = Integer.parseInt(orderColumnId);
                String sortColumnName = request.getParameter("columns[" + sortColumnId + "][data]");
                sortDir = request.getParameter("order[0][dir]").toUpperCase();
                orderField = sortColumnName;
            } else {
                orderField = "createdStamp";
                sortDir = "DESC";
            }
            if (UtilValidate.isNotEmpty(teamName)) {
                teamName = teamName.trim();
                conditionList.add(EntityCondition.makeCondition("teamName", EntityOperator.LIKE, "%" + teamName + "%"));
            }

            if (UtilValidate.isNotEmpty(countryGeoId)) {
                conditionList.add(EntityCondition.makeCondition("countryGeoId", EntityOperator.EQUALS, countryGeoId));
            }
            long emplTeamCount = delegator.findCountByCondition("EmplTeam", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null);

            if (emplTeamCount> 0) {
                recordsFiltered = (int) emplTeamCount;
                recordsTotal = (int) emplTeamCount;
                EntityFindOptions efo = new EntityFindOptions();
                efo.setDistinct(true);
                int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                efo.setOffset(startInx);
                efo.setLimit(endInx);
                List<GenericValue> emplTeam = delegator.findList("EmplTeam", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, UtilMisc.toList(orderField + " " + sortDir), efo, false);
                if (UtilValidate.isNotEmpty(emplTeam)) {
                    emplTeam.forEach(emplTeamGV-> {
                        JSONObject data = new JSONObject();
                        data.put("emplTeamId", emplTeamGV.getString("emplTeamId"));
                        data.put("teamName", emplTeamGV.getString("teamName"));
                        data.put("description", UtilValidate.isNotEmpty(emplTeamGV.getString("description")) ? emplTeamGV.getString("description") : "");
                        String countryName = "";
                        if (UtilValidate.isNotEmpty(emplTeamGV.getString("countryGeoId"))) {
                            try {
                                GenericValue geo = EntityQuery.use(delegator).from("Geo")
                                    .where("geoId", emplTeamGV.getString("countryGeoId"), "geoTypeId", "COUNTRY").queryFirst();
                                if (geo != null && geo.size()> 0) {
                                    countryName = geo.getString("geoName");
                                }
                            } catch (GenericEntityException e) {
                                Debug.logInfo("Error-" + e.getMessage(), MODULE);
                            }
                        }
                        data.put("countryGeoId", countryName);
                        datas.add(data);
                    });
                }

            }

        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
            resp.put("data", datas);
            resp.put("draw", draw);
            resp.put("recordsTotal", 0);
            resp.put("recordsFiltered", 0);
            return AjaxEvents.doJSONResponse(response, resp);
        }

        resp.put("data", datas);
        resp.put("draw", draw);
        resp.put("recordsTotal", recordsTotal);
        resp.put("recordsFiltered", recordsFiltered);
        return AjaxEvents.doJSONResponse(response, resp);
    }
    
    public static String getUsers(HttpServletRequest request, HttpServletResponse response) {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String firstName = request.getParameter("firstName");
        String rmDisplayName = request.getParameter("rmDisplayName");
        String userLoginId = request.getParameter("userLoginId");
        String roleTypeId = request.getParameter("roleTypeId");
        
        String emplTeamId = request.getParameter("emplTeamId");
        String virtualTeamId = request.getParameter("virtualTeamId");
        
        Map<String, Object> resp = new HashMap<String, Object> ();
        JSONArray datas = new JSONArray();

        List<EntityCondition> conditionList = FastList.newInstance();

        int recordsFiltered = 0;
        int recordsTotal = 0;
        try {
            String sortDir = "";
            String orderField = "";
            String orderColumnId = request.getParameter("order[0][column]");
            if (UtilValidate.isNotEmpty(orderColumnId)) {
                int sortColumnId = Integer.parseInt(orderColumnId);
                String sortColumnName = request.getParameter("columns[" + sortColumnId + "][data]");
                sortDir = request.getParameter("order[0][dir]").toUpperCase();
                orderField = sortColumnName;
            } else {
                orderField = "firstName";
                sortDir = "ASC";
            }
            if (UtilValidate.isNotEmpty(firstName)) {
                firstName = firstName.trim();
                conditionList.add(EntityCondition.makeCondition("firstName", EntityOperator.LIKE, "%" + firstName + "%"));
            }

            if (UtilValidate.isNotEmpty(rmDisplayName)) {
                rmDisplayName = rmDisplayName.trim();
                conditionList.add(EntityCondition.makeCondition("rmDisplayName", EntityOperator.LIKE, "%" + rmDisplayName + "%"));
            }
            
            if (UtilValidate.isNotEmpty(userLoginId)) {
                userLoginId = userLoginId.trim();
                conditionList.add(EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId));
            }
            
            if (UtilValidate.isNotEmpty(emplTeamId)) {
                conditionList.add(EntityCondition.makeCondition("emplTeamId", EntityOperator.EQUALS, emplTeamId));
            }
            if (UtilValidate.isNotEmpty(virtualTeamId)) {
            	conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
            			EntityCondition.makeCondition("memberTeamPartyId", EntityOperator.EQUALS, virtualTeamId),
            			EntityCondition.makeCondition("memberTeamPartyRoleTypeId", EntityOperator.EQUALS, "ACCOUNT_TEAM"),
            			EntityCondition.makeCondition("memberPartyRoleTypeId", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
            			EntityCondition.makeCondition("memberPartyRelationshipTypeId", EntityOperator.EQUALS, "ASSIGNED_TO"),
            			//EntityCondition.makeCondition("securityGroupId", EntityOperator.EQUALS, "VT_SG_TL"),
                        EntityCondition.makeConditionDate("memberFromDate", "memberThruDate")
                    ));
            }
            
            if (UtilValidate.isNotEmpty(roleTypeId)) {
                conditionList.add(EntityCondition.makeCondition("emplPositionTypeId", EntityOperator.EQUALS, roleTypeId));
            }
            
            conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
                EntityCondition.makeConditionDate("emplPositionFromDate", "emplPositionThruDate"),
                EntityCondition.makeConditionDate("reportingStructFromDate", "reportingStructThruDate")
            ));
            
            long emplPositionAndPersonAndUserLoginCount = delegator.findCountByCondition("EmplPositionAndPersonAndUserLogin", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null);

            if (emplPositionAndPersonAndUserLoginCount> 0) {
                recordsFiltered = (int) emplPositionAndPersonAndUserLoginCount;
                recordsTotal = (int) emplPositionAndPersonAndUserLoginCount;
                EntityFindOptions efo = new EntityFindOptions();
                efo.setDistinct(true);
                int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                efo.setOffset(startInx);
                efo.setLimit(endInx);
                List<GenericValue> emplPositionAndPersonAndUserLogin = delegator.findList("EmplPositionAndPersonAndUserLogin", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, UtilMisc.toList(orderField + " " + sortDir), efo, false);
                if (UtilValidate.isNotEmpty(emplPositionAndPersonAndUserLogin)) {
                    for(GenericValue emplPositionAndPersonAndUserLoginGV : emplPositionAndPersonAndUserLogin) {
                        JSONObject data = new JSONObject();
                        String name = "";
                        String positionType = "";
                        String managedBy = "";
                        String team = "";
                        String emailAddress = "";
                        String rmUserLoginId = emplPositionAndPersonAndUserLoginGV.getString("userLoginId");
                        String partyId = emplPositionAndPersonAndUserLoginGV.getString("partyId");
                        String teamId = emplPositionAndPersonAndUserLoginGV.getString("emplTeamId");
                        String emplPositionIdManagedBy = emplPositionAndPersonAndUserLoginGV.getString("emplPositionIdManagedBy");
                        String emplPositionTypeId = emplPositionAndPersonAndUserLoginGV.getString("emplPositionTypeId");
                        
                        name  = emplPositionAndPersonAndUserLoginGV.getString("firstName");
                        if(UtilValidate.isNotEmpty(emplPositionAndPersonAndUserLoginGV.getString("lastName"))) {
                            name = name +" "+emplPositionAndPersonAndUserLoginGV.getString("lastName");
                        }
                        
                        if (UtilValidate.isNotEmpty(teamId)) {
                            GenericValue emplTeam = EntityQuery.use(delegator).from("EmplTeam")
                                .where("emplTeamId", teamId).queryOne();
                            if (emplTeam != null && emplTeam.size()> 0) {
                                team = emplTeam.getString("teamName");
                            }
                        }

                        if (UtilValidate.isNotEmpty(emplPositionTypeId)) {
                            GenericValue emplPositionType = EntityQuery.use(delegator).from("EmplPositionType")
                                .where("emplPositionTypeId", emplPositionTypeId).queryOne();
                            if (emplPositionType != null && emplPositionType.size()> 0) {
                                positionType = emplPositionType.getString("description");
                            }
                        }
                        
                        if (UtilValidate.isNotEmpty(emplPositionIdManagedBy)) {
                            GenericValue emplPositionFulfillment = EntityQuery.use(delegator).from("EmplPositionFulfillment")
                                .where("emplPositionId", emplPositionIdManagedBy).filterByDate().queryFirst();
                            if(emplPositionFulfillment != null && emplPositionFulfillment.size()> 0) {
                               GenericValue managedByPerson = EntityQuery.use(delegator).from("Person").where("partyId", emplPositionFulfillment.getString("partyId")).queryOne();
                               if (managedByPerson != null && managedByPerson.size()> 0) {
                                   managedBy = managedByPerson.getString("firstName");
                                   if (UtilValidate.isNotEmpty(managedByPerson.getString("lastName"))) {
                                       managedBy = managedBy + " " + managedByPerson.getString("lastName");
                                   }
                               }
                            }
                        }
                        
                        Map<String, String> partyContactInfo = PartyUtil.getPartyPrimaryContactMechValueMaps(delegator, partyId);

                        if (UtilValidate.isNotEmpty(partyContactInfo.get("EmailAddress"))) {
                            emailAddress = partyContactInfo.get("EmailAddress");
                            emailAddress = emailAddress.substring(emailAddress.lastIndexOf('-') + 1);
                        }
                        
                        data.put("name", name);
                        data.put("partyId", partyId);
                        data.put("userLoginId", rmUserLoginId);
                        data.put("emplTeamId", team);
                        data.put("emplPositionTypeId", positionType);
                        data.put("emplPositionIdManagedBy", managedBy);
                        data.put("emailAddress", emailAddress);
                        
                        String virtualTeamName = "";
                        if (UtilValidate.isEmpty(virtualTeamId)) {
                        	Map<String, Object> virtualTeam = VirtualTeamUtil.getFirstVirtualTeamMember(delegator, null, partyId);
                        	virtualTeamName = UtilValidate.isNotEmpty(virtualTeam.get("virtualTeamName")) ? virtualTeam.get("virtualTeamName").toString() : "";
                        } else {
                        	GenericValue virtualTeam = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId", virtualTeamId), false);
                        	if (UtilValidate.isNotEmpty(virtualTeam) && UtilValidate.isNotEmpty(virtualTeam.getString("groupName"))) {
                        		virtualTeamName = virtualTeam.getString("groupName");
                        	}
                        }
                        data.put("virtualTeamName", virtualTeamName);
                        
                        datas.add(data);
                    }
                }

            }

        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
            resp.put("data", datas);
            resp.put("draw", draw);
            resp.put("recordsTotal", 0);
            resp.put("recordsFiltered", 0);
            return AjaxEvents.doJSONResponse(response, resp);
        }

        resp.put("data", datas);
        resp.put("draw", draw);
        resp.put("recordsTotal", recordsTotal);
        resp.put("recordsFiltered", recordsFiltered);
        return AjaxEvents.doJSONResponse(response, resp);
    }
    
    public static String getPinCodes(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
    	
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        String partyId = request.getParameter("partyId");
        String city = request.getParameter("city");

        List<String> candidateItems = FastList.newInstance();
        List<String> selectPinCodeList = FastList.newInstance();
        List<String> selectedPinCodeIdList = FastList.newInstance();
        
        JSONObject jsonResponse = new JSONObject();
        
        try {

            if (UtilValidate.isNotEmpty(partyId)) {
                List<EntityCondition> conditionList = FastList.newInstance();
                conditionList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId));
                conditionList.add(EntityCondition.makeCondition("city", EntityOperator.EQUALS, city));
                List<GenericValue> locationAssocList = delegator.findList("UserLocationAssoc", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
                if (UtilValidate.isNotEmpty(locationAssocList)) {
                    for (GenericValue locationAssoc: locationAssocList) {
                    	
                    	EntityCondition condition = EntityCondition.makeCondition(EntityOperator.OR,
                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoId")), EntityOperator.EQUALS, locationAssoc.getString("postalCode").toUpperCase()),
                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoCode")), EntityOperator.EQUALS, locationAssoc.getString("postalCode").toUpperCase()),
                       			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("geoName")), EntityOperator.EQUALS, locationAssoc.getString("postalCode").toUpperCase())
                       			);   
        				
        				condition = EntityCondition.makeCondition(EntityOperator.AND,
                       			EntityCondition.makeCondition("geoTypeId", EntityOperator.EQUALS, "POSTAL_CODE"),
                       			condition
                       			);                     	
        				
        				GenericValue geo = EntityUtil.getFirst( delegator.findList("Geo", condition, null, UtilMisc.toList("-createdStamp"), null, false) );
                        if (UtilValidate.isNotEmpty(geo)) {
                            selectPinCodeList.add(geo.getString("geoName"));
                            selectedPinCodeIdList.add(geo.getString("geoId"));
                        }
                    }
                }
            }
            
            List<EntityCondition> conditionList = FastList.newInstance();

            if (UtilValidate.isNotEmpty(city)) {
                conditionList.add(EntityCondition.makeCondition(EntityCondition.makeCondition("geoAssocTypeId", EntityOperator.EQUALS, "POSTAL_CODE"),
                    EntityOperator.AND, EntityCondition.makeCondition("geoId", EntityOperator.EQUALS, city)));

                if (selectedPinCodeIdList != null && selectedPinCodeIdList.size()> 0) {
                    conditionList.add(EntityCondition.makeCondition("geoIdTo", EntityOperator.NOT_IN, selectedPinCodeIdList));
                }
                
                List<GenericValue> geoAssocList = delegator.findList("GeoAssocSummary", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
                if (UtilValidate.isNotEmpty(geoAssocList)) {
                	candidateItems.addAll( EntityUtil.getFieldListFromEntityList(geoAssocList, "geoName", true) );
                }
            }
            
            jsonResponse.put("candidateItems", candidateItems);
            jsonResponse.put("selectionItems", selectPinCodeList);
            
        } catch (Exception e) {
            Debug.logInfo("Error :" + e.getMessage(), MODULE);
        }
        
        return doJSONResponse(response, jsonResponse);
    }
    
    public static String getTeam(HttpServletRequest request, HttpServletResponse response) {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String groupName = request.getParameter("teamName");
        String countryGeoId = request.getParameter("countryGeoId");
        Map < String, Object > resp = new HashMap < String, Object > ();
        JSONArray datas = new JSONArray();

        List < EntityCondition > conditionList = FastList.newInstance();

        int recordsFiltered = 0;
        int recordsTotal = 0;
        try {
            String sortDir = "";
            String orderField = "";
            String orderColumnId = request.getParameter("order[0][column]");
            if (UtilValidate.isNotEmpty(orderColumnId)) {
                int sortColumnId = Integer.parseInt(orderColumnId);
                String sortColumnName = request.getParameter("columns[" + sortColumnId + "][data]");
                sortDir = request.getParameter("order[0][dir]").toUpperCase();
                orderField = sortColumnName;
            } else {
                orderField = "groupName";
                sortDir = "ASC";
            }

            conditionList.add(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "ACCOUNT_TEAM"));
            conditionList.add(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"));
            if (UtilValidate.isNotEmpty(groupName)) {
                groupName = groupName.trim();
                conditionList.add(EntityCondition.makeCondition("groupName", EntityOperator.LIKE, "%" + groupName + "%"));
            }


            if (UtilValidate.isNotEmpty(countryGeoId)) {
                conditionList.add(EntityCondition.makeCondition("teamCountryGeoId", EntityOperator.EQUALS, countryGeoId));
            }
            long partyRoleStatusAndPartyDetailCount = delegator.findCountByCondition("PartyRoleStatusAndPartyDetail", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null);

            if (partyRoleStatusAndPartyDetailCount > 0) {
                recordsFiltered = (int) partyRoleStatusAndPartyDetailCount;
                recordsTotal = (int) partyRoleStatusAndPartyDetailCount;

                EntityFindOptions efo = new EntityFindOptions();
                efo.setDistinct(true);
                int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                efo.setOffset(startInx);
                efo.setLimit(endInx);

                Set < String > fieldToSelect = new HashSet < String > ();
                fieldToSelect.add("partyId");
                fieldToSelect.add("statusId");
                fieldToSelect.add("groupName");
                fieldToSelect.add("roleTypeId");
                fieldToSelect.add("teamCountryGeoId");
                fieldToSelect.add("partyGroupComments");

                List < GenericValue > partyRoleStatusAndPartyDetail = delegator.findList("PartyRoleStatusAndPartyDetail", EntityCondition.makeCondition(conditionList, EntityOperator.AND), fieldToSelect, UtilMisc.toList(orderField + " " + sortDir), efo, false);
                if (UtilValidate.isNotEmpty(partyRoleStatusAndPartyDetail)) {
                    for (GenericValue partyRoleStatusAndPartyDetailGV: partyRoleStatusAndPartyDetail) {
                        JSONObject data = new JSONObject();
                        data.put("partyId", partyRoleStatusAndPartyDetailGV.getString("partyId"));
                        data.put("groupName", partyRoleStatusAndPartyDetailGV.getString("groupName"));
                        data.put("description", UtilValidate.isNotEmpty(partyRoleStatusAndPartyDetailGV.getString("partyGroupComments")) ? partyRoleStatusAndPartyDetailGV.getString("partyGroupComments") : "");
                        String countryName = "";
                        if (UtilValidate.isNotEmpty(partyRoleStatusAndPartyDetailGV.getString("teamCountryGeoId"))) {
                            GenericValue geo = EntityQuery.use(delegator).from("Geo")
                                .where("geoId", partyRoleStatusAndPartyDetailGV.getString("teamCountryGeoId"), "geoTypeId", "COUNTRY").queryFirst();
                            if (geo != null && geo.size() > 0) {
                                 countryName = geo.getString("geoName");
                            }
                        }
                        data.put("countryGeoId", countryName);
                        datas.add(data);
                    }
                }
            }
        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
            resp.put("data", datas);
            resp.put("draw", draw);
            resp.put("recordsTotal", 0);
            resp.put("recordsFiltered", 0);
            return AjaxEvents.doJSONResponse(response, resp);
        }

        resp.put("data", datas);
        resp.put("draw", draw);
        resp.put("recordsTotal", recordsTotal);
        resp.put("recordsFiltered", recordsFiltered);
        return AjaxEvents.doJSONResponse(response, resp);
    }
    
    public static String viewTeamMembers(HttpServletRequest request, HttpServletResponse response) {

        Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

        Locale locale = UtilHttp.getLocale(request);
        HttpSession session = request.getSession(true);
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String emplTeamId = request.getParameter("emplTeamId");
        Map<String, Object> resp = new HashMap<String, Object> ();
        JSONArray datas = new JSONArray();

        List<EntityCondition> conditionList = FastList.newInstance();

        int recordsFiltered = 0;
        int recordsTotal = 0;
        try {
            
            conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
                EntityCondition.makeCondition("emplTeamId", EntityOperator.EQUALS, emplTeamId),
                EntityUtil.getFilterByDateExpr()
            ));
            
            long emplPositionReportingStructCount = delegator.findCountByCondition("EmplPositionReportingStruct", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null);

            if (emplPositionReportingStructCount> 0) {
                recordsFiltered = (int) emplPositionReportingStructCount;
                recordsTotal = (int) emplPositionReportingStructCount;
                EntityFindOptions efo = new EntityFindOptions();
                efo.setDistinct(true);
                int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
                int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
                efo.setOffset(startInx);
                efo.setLimit(endInx);
                List<GenericValue> emplPositionReportingStruct = delegator.findList("EmplPositionReportingStruct", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
                if (UtilValidate.isNotEmpty(emplPositionReportingStruct)) {
                    for(GenericValue emplPositionReportingStructGV : emplPositionReportingStruct) {
                        JSONObject data = new JSONObject();
                        String emplPositionTypeId = "";
                        String name = "";
                        String partyId = "";
                        String positionDescription = "";
                        
                        data.put("emplTeamId", emplPositionReportingStructGV.getString("emplTeamId"));
                        data.put("emplPositionIdReportingTo", emplPositionReportingStructGV.getString("emplPositionIdReportingTo"));
                        data.put("emplPositionIdManagedBy", emplPositionReportingStructGV.getString("emplPositionIdManagedBy"));
                        if(UtilValidate.isNotEmpty(emplPositionReportingStructGV.getString("emplPositionIdReportingTo"))) {
                            GenericValue emplPosition = EntityQuery.use(delegator).from("EmplPosition")
                                .where("emplPositionId", emplPositionReportingStructGV.getString("emplPositionIdReportingTo")).queryOne();
                            if(emplPosition != null && emplPosition.size()> 0) {
                                emplPositionTypeId = emplPosition.getString("emplPositionTypeId");
                                if(UtilValidate.isNotEmpty(emplPositionTypeId)) {
                                    GenericValue emplPositionType = EntityQuery.use(delegator).from("EmplPositionType")
                                         .where("emplPositionTypeId", emplPositionTypeId).queryOne();
                                    if(emplPositionType != null && emplPositionType.size()> 0) {
                                        positionDescription = emplPositionType.getString("description");
                                    }
                                }
                            }
                            
                            GenericValue emplPositionFulfillment = EntityQuery.use(delegator).from("EmplPositionFulfillment")
                                    .where("emplPositionId", emplPositionReportingStructGV.getString("emplPositionIdReportingTo")).queryFirst();
                            if(emplPositionFulfillment != null && emplPositionFulfillment.size()> 0) {
                                partyId = emplPositionFulfillment.getString("partyId");
                                if(UtilValidate.isNotEmpty(partyId)) {
                                    GenericValue person = EntityQuery.use(delegator).from("Person").where("partyId", partyId).queryOne();
                                    if(person != null && person.size()> 0 ){
                                        name = person.getString("firstName");
                                        if (UtilValidate.isNotEmpty(person.getString("lastName"))) {
                                            name = name + " " + person.getString("lastName");
                                        }
                                    }
                                }
                            }
                        }
                        
                        data.put("partyId", partyId);
                        data.put("name", name);
                        data.put("emplPositionTypeId", emplPositionTypeId);
                        data.put("positionDescription", positionDescription);
                        datas.add(data);
                    }
                }

            }

        } catch (Exception e) {
            Debug.logInfo("Error-" + e.getMessage(), MODULE);
            resp.put("data", datas);
            resp.put("draw", draw);
            resp.put("recordsTotal", 0);
            resp.put("recordsFiltered", 0);
            return AjaxEvents.doJSONResponse(response, resp);
        }

        resp.put("data", datas);
        resp.put("draw", draw);
        resp.put("recordsTotal", recordsTotal);
        resp.put("recordsFiltered", recordsFiltered);
        return AjaxEvents.doJSONResponse(response, resp);
    }
    
    public static String getUserTeamMembers(HttpServletRequest request, HttpServletResponse response) {
    	
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		String draw = request.getParameter("draw");
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String rmPartyId = request.getParameter("rmPartyId");
		String roleTypeId = request.getParameter("roleTypeId");
		
		String email = request.getParameter("email");
		String phoneNumber = request.getParameter("phoneNumber");
		String city = request.getParameter("city");
		String postalCode = request.getParameter("postalCode");
		
		Map<String, Object> returnMap = FastMap.newInstance();
		List<Object> partyRoleList = FastList.newInstance();
		
		try {
			
			List<EntityCondition> conditions = new ArrayList<EntityCondition> ();

			// construct role conditions
			if (UtilValidate.isNotEmpty(roleTypeId)) {
				conditions.add(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, roleTypeId));
			} else {
				conditions.add(EntityCondition.makeCondition("roleTypeId", EntityOperator.LIKE, "DBS_%"));
			}
			
			// construct search conditions
			if (UtilValidate.isNotEmpty(lastName)) {
				conditions.add(EntityCondition.makeCondition("lastName", EntityOperator.LIKE, EntityFunction.UPPER("%" + lastName + "%")));
			}
			if (UtilValidate.isNotEmpty(firstName)) {
				conditions.add(EntityCondition.makeCondition("firstName", EntityOperator.LIKE, EntityFunction.UPPER("%" + firstName + "%")));
			}
			if (UtilValidate.isNotEmpty(rmPartyId)) {
				conditions.add(EntityCondition.makeCondition("partyId", EntityOperator.NOT_EQUAL, rmPartyId));
			}
			
			if (UtilValidate.isNotEmpty(email)) {
				conditions.add(EntityCondition.makeCondition("infoString", EntityOperator.EQUALS, email));
			}
			if (UtilValidate.isNotEmpty(phoneNumber)) {
				conditions.add(EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("tnContactNumber", EntityOperator.EQUALS, phoneNumber),
						EntityCondition.makeCondition("secondaryPhoneNumber", EntityOperator.EQUALS, phoneNumber)));

			}
			if (UtilValidate.isNotEmpty(city)) {
				conditions.add(EntityCondition.makeCondition("paCity", EntityOperator.EQUALS, city));
			}
			if (UtilValidate.isNotEmpty(postalCode)) {
				conditions.add(EntityCondition.makeCondition("paPostalCode", EntityOperator.EQUALS, postalCode));
			}
			// remove disabled parties
			conditions.add(EntityCondition.makeCondition(EntityOperator.OR,
					EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
					EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)));

			EntityCondition mainConditons = EntityCondition.makeCondition(conditions, EntityOperator.AND);
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);
			int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
			int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0;
			efo.setOffset(startInx);
			efo.setLimit(endInx);
			List<GenericValue> partyToSummartyByRoleCount = delegator.findList("PartyToSummaryAndContactByRole", mainConditons, null, null, null, false);
			int count = partyToSummartyByRoleCount.size();
			int recordsFiltered = count;
			int recordsTotal = count;
			
			List<GenericValue> partyToSummartyByRole = delegator.findList("PartyToSummaryAndContactByRole", mainConditons, null, null, efo, false);
			if (partyToSummartyByRole != null && partyToSummartyByRole.size()> 0) {
				
				int id = 1;
				for (GenericValue roles: partyToSummartyByRole) {
					Map<String, Object> partyToSummartyByRoleMap = FastMap.newInstance();
					
					id = id + 1;
					partyToSummartyByRoleMap.put("id", id + "");
					partyToSummartyByRoleMap.put("name", roles.getString("firstName") + " " + roles.getString("lastName"));
					partyToSummartyByRoleMap.put("partyId", roles.getString("partyId"));
					
					String userRoleTypeId = "";
					String userRoleTypeDesc = "";
					String primaryEmail = roles.getString("infoString"); 
					String primaryPhoneNumber = roles.getString("tnContactNumber"); 
					String paCity = roles.getString("paCity");
					String paPostalCode = roles.getString("paPostalCode"); 
					GenericValue geoCity = delegator.findOne("Geo", UtilMisc.toMap("geoId", roles.getString("paCity")), false);
		        	if (geoCity != null && geoCity.size() > 0) {
	        	    	paCity = geoCity.getString("geoName");
			    	}
					GenericValue dbsRole = DataUtil.getFirstDbsRole(delegator, roles.getString("partyId"));
					if (UtilValidate.isNotEmpty(dbsRole)) {
						GenericValue roleType = delegator.findOne("RoleType", UtilMisc.toMap("roleTypeId", dbsRole.getString("roleTypeId")), true);
						userRoleTypeId = dbsRole.getString("roleTypeId");
						userRoleTypeDesc = roleType.getString("description");
					}
					if(UtilValidate.isEmpty(primaryEmail)) {
					  GenericValue emailAddress = PartyUtil.getPartyEmailAddress(delegator, roles.getString("partyId"));
			          if (emailAddress != null && emailAddress.size() > 0) {
			        	  primaryEmail= emailAddress.getString("infoString");
			          }
					}
					GenericValue primaryTelecomNumber = PartyUtil.getPartyPhoneNumber(delegator, roles.getString("partyId"), "PRIMARY_PHONE");
			        if (primaryTelecomNumber != null && primaryTelecomNumber.size() > 0) {
			            if(UtilValidate.isEmpty(primaryPhoneNumber)) {
			            	primaryPhoneNumber = primaryTelecomNumber.getString("contactNumber");
			            }
			        }
			        GenericValue postalAddress = PartyUtil.getPartyPrimaryPostal(delegator, roles.getString("partyId"));
			        if (postalAddress != null && postalAddress.size() > 0) {
			        	GenericValue geoCitys = delegator.findOne("Geo", UtilMisc.toMap("geoId", postalAddress.getString("city")), false);
						if(UtilValidate.isEmpty(paCity)) {
			        	    if (geoCitys != null && geoCitys.size() > 0) {
			        	    	paCity = geoCitys.getString("geoName");
					    	}
						}
						if(UtilValidate.isEmpty(paPostalCode)) {
							paPostalCode = postalAddress.getString("postalCode");
						}
			        }
			        
					partyToSummartyByRoleMap.put("roleTypeId", userRoleTypeId);
					partyToSummartyByRoleMap.put("roleTypeDesc", userRoleTypeDesc);
					partyToSummartyByRoleMap.put("email", primaryEmail);
					partyToSummartyByRoleMap.put("phoneNumber", primaryPhoneNumber);
					partyToSummartyByRoleMap.put("city", paCity);
					partyToSummartyByRoleMap.put("postalCode", paPostalCode);
					
					partyRoleList.add(partyToSummartyByRoleMap);
				}
				returnMap.put("data", partyRoleList);
				returnMap.put("draw", draw);
				returnMap.put("recordsTotal", recordsTotal);
				returnMap.put("recordsFiltered", recordsFiltered);
				
			} else {
				
				returnMap.put("data", partyRoleList);
				returnMap.put("draw", draw);
				returnMap.put("recordsTotal", 0);
				returnMap.put("recordsFiltered", 0);
				return AjaxEvents.doJSONResponse(response, returnMap);
				
			}
		} catch (Exception e) {
			Debug.logError("Exception in Get Team Member" + e.getMessage(), MODULE);
		}
		return AjaxEvents.doJSONResponse(response, returnMap);
	}
    
    @SuppressWarnings("unchecked")
    public static String assignPinCode(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String partyId = request.getParameter("partyId");
        String positionTypeId = request.getParameter("positionTypeId");
        
        String countryGeoId = request.getParameter("countryGeoId");
        String stateProvinceGeoId = request.getParameter("stateProvinceGeoId");
        String city = request.getParameter("city");
        
        String selectedPinCodes = request.getParameter("selectedPinCodes");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	
        	List<GenericValue> toBeStored = FastList.newInstance();
        	
        	if (UtilValidate.isNotEmpty(selectedPinCodes)) {
        		
        		delegator.removeByAnd("UserLocationAssoc", UtilMisc.toMap("partyId", partyId));
        		
        		String pinCodes[] = selectedPinCodes.split(",");
        		for (String pinCode : pinCodes) {
        			
        			//GenericValue locationAssoc = delegator.findOne("UserLocationAssoc", UtilMisc.toMap("partyId", partyId, "postalCode", pinCode), false);
        			GenericValue locationAssoc = delegator.makeValue("UserLocationAssoc");
        			
        			locationAssoc.put("partyId", partyId);
        			locationAssoc.put("roleTypeId", positionTypeId);
        			locationAssoc.put("postalCode", pinCode);
        			locationAssoc.put("countryGeoId", countryGeoId);
        			locationAssoc.put("stateProvinceGeoId", stateProvinceGeoId);
        			locationAssoc.put("city", city);
        			
        			toBeStored.add(locationAssoc);
        			
        		}
        		
        	}
        	
        	delegator.storeAll(toBeStored);
        	
        	resp.put(UserMgmtConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(UserMgmtConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(UserMgmtConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
}
