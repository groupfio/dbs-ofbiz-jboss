<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<@sectionHeader title="${uiLabelMap.findUsers!}" />
<div class="card-header mt-2 mb-3">
   <#--<form method="post" action="findUser" id="findUser" class="form-horizontal" name="findUser" novalidate="novalidate" data-toggle="validator">-->
   <div class="row">
      <@generalInputForRow 
      id="firstName"
      placeholder=uiLabelMap.firstName
      label = uiLabelMap.firstName
      required=false
      />
      <@generalInputForRow 
      id="rmDisplayName"
      placeholder=uiLabelMap.rmDisplayName
      label = uiLabelMap.rmDisplayName
      required=false
      />
      <@generalInputForRow 
      id="userLoginId"
      placeholder=uiLabelMap.oneBankId
      label = uiLabelMap.oneBankId
      required=false
      />
      
      <@dropdownInputForRow 
      id="virtualTeamId"
      name="virtualTeamId"
      options=virtualTeamList
      allowEmpty=true
      tooltip = uiLabelMap.team
      label=uiLabelMap.team
      dataLiveSearch=true
      />
      <#-- 
      <@dropdownInputForRow 
      id="emplTeamId"
      name="emplTeamId"
      options=emplTeam
      allowEmpty=true
      tooltip = uiLabelMap.team
      label=uiLabelMap.team
      dataLiveSearch=true
      />
      -->
      
      <@dropdownInputForRow 
      id="roleTypeId"
      name="roleTypeId"
      options=emplPositionType
      allowEmpty=true
      tooltip = uiLabelMap.role
      label=uiLabelMap.role
      dataLiveSearch=true
      disabled=disabled
      />
      <div class="col-md-2 col-sm-2">
         <input type="submit" value="Find" id="find-user" class="btn btn-sm btn-primary tooltips" style="margin-top:28px;"/>
      </div>
   </div>
</div>
<div class="clearfix"> </div>
<div class="page-header">
   <h2 class="float-left">Users List</h2>
</div>
<div class="table-responsive">
   <table id="users-list" class="table table-striped">
      <thead>
         <tr>
            <th>${uiLabelMap.name!}</th>
            <th>${uiLabelMap.emailId!}</th>
            <th>${uiLabelMap.oneBankId!}</th>
            <th>${uiLabelMap.team!}</th>
            <th>${uiLabelMap.lineManager!}</th>
            <th>${uiLabelMap.role!}</th>
         </tr>
      </thead>
   </table>
</div>
<script >
   $(document).ready(function() {
    findUsers();
   });

   $('#find-user').on('click', function(){
       findUsers();
   });
   
   $('#firstName, #rmDisplayName, #userLoginId').on("keypress", function(e) {
        if (e.keyCode == 13) {
            findUsers();
        }
   });
   
   $("#emplTeamId, #roleTypeId").change(function () {
        findUsers();
    });
    
   function findUsers() {
    var firstName = $('#firstName').val();
    var rmDisplayName = $('#rmDisplayName').val();
    var userLoginId = $('#userLoginId').val();
    var virtualTeamId = $('#virtualTeamId').val();
    var roleTypeId = $('#roleTypeId').val();
    
    $('#users-list').DataTable({
     "processing": true,
     "serverSide": true,
     "destroy": true,
     "searching": false,
     "ajax": {
      "url": "getUsers",
      "type": "POST",
      data: {
       "firstName": firstName,
       "rmDisplayName": rmDisplayName,
       "userLoginId": userLoginId,
       "virtualTeamId": virtualTeamId,
       "roleTypeId": roleTypeId
      }
     },
     "pageLength": 10,
     "order": [],
     "columns": [{
       "data": "firstName",
       "render": function(data, type, row, meta) {
        	data = '<a href="/user-mgmt/control/viewUser?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.name + ' ('+row.partyId+')</a>';
        	return data;
       }
      },
      {
       "data": "emailAddress"
      },
      {
       "data": "userLoginId"
      },
      {
       "data": "virtualTeamName"
      },
      {
       "data": "emplPositionIdManagedBy"
      },
      {
       "data": "emplPositionTypeId"
      }
     ],
     "fnDrawCallback": function(oSettings) {
      resetDefaultEvents();
     }
    });
   }
</script>