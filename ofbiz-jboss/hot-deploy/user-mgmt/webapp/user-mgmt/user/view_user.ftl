<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<link href="/bootstrap/css/dualselectlist.css" rel="stylesheet"> 
<script type="text/javascript" src="/bootstrap/js/dualselectlist.jquery.js"> </script>

<#assign extra>
<a href="/user-mgmt/control/updateUserForm?partyId=${partyId?if_exists}" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.edit!}</a>
<a href="/user-mgmt/control/findUser" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.back!}</a> 
<#--<a href="#" class="btn btn-xs btn-danger ">${uiLabelMap.deactivateTeam!}</a>-->
</#assign>
<#assign screenTitle>
${uiLabelMap.viewUser} - ${userName!} (${partyId!})
</#assign>
<@sectionHeader title=screenTitle extra=extra/>

<ul class="nav nav-tabs">
   <li class="nav-item"><a data-toggle="tab" class="nav-link active show" href="#tab1">Details </a></li>
   
   <#if isUserRm?exists && isUserRm>
   <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab2">Assign PIN Code</a></li>
   </#if>
   
</ul>
<div class="tab-content">
   <div id="tab1" class="tab-pane fade active show">
      <div class="page-header">
         <h2 class="float-left">${uiLabelMap.contactInformation!}</h2>
      </div>
      <div class="row padding-r">
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="firstName"
                  label=uiLabelMap.firstName
                  value="${emplPositionAndPersonAndUserLogin?if_exists.firstName?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  <@readonlyInput 
                  id="mobileNo"
                  label=uiLabelMap.mobileNo
                  value="${primaryTelecomNumber?if_exists.countryCode?if_exists} ${primaryTelecomNumber?if_exists.contactNumber?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  <@readonlyInput 
                  id="address1"
                  label=uiLabelMap.address1
                  required=true
                  value="${postalAddress?if_exists.address1?if_exists}"
                  isHiddenInput=false
                  />
                  <@readonlyInput 
                  id="pinCode"
                  label=uiLabelMap.pinCode
                  required=true
                  value="${postalAddress?if_exists.postalCode?if_exists}"
                  isHiddenInput=false
                  />
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="lastName"
                  label=uiLabelMap.lastName
                  value="${emplPositionAndPersonAndUserLogin?if_exists.lastName?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  <@readonlyInput 
                  id="officeNo"
                  label=uiLabelMap.officeNo
                  value="${secondaryTelecomNumber?if_exists.countryCode?if_exists} ${secondaryTelecomNumber?if_exists.contactNumber?if_exists}"
                  required=true
                  isHiddenInput=false
                  />
                  <@readonlyInput 
                  id="address2"
                  label=uiLabelMap.address2
                  value="${postalAddress?if_exists.address2?if_exists}"
                  isHiddenInput=false
                  />
                  <@readonlyInput 
                  id="cityId"
                  label=uiLabelMap.city
                  value="${city?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="rmDisplayName"
                  label=uiLabelMap.rmDisplayName
                  value="${emplPositionAndPersonAndUserLogin?if_exists.rmDisplayName?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  <@readonlyInput 
                  id="emailAddress"
                  label=uiLabelMap.emailAddress
                  value="${emailAddress?if_exists.infoString?if_exists}"
                  required=true
                  isHiddenInput=false
                  />
                  <@readonlyInput 
                  id="country"
                  label=uiLabelMap.country
                  value="${country?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
                  <@readonlyInput 
                  id="state"
                  label=uiLabelMap.state
                  value="${state?if_exists}"
                  isHiddenInput=false
                  required=true
                  />
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
      <div class="page-header">
         <h2 class="float-left">User Information</h2>
      </div>
      <div class="row padding-r">
      <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="reporting1bankid"
                  label=uiLabelMap.userName
                  value="${emplPositionUserLoginId?if_exists}"
                  isHiddenInput=false
                  required=true
                  />	
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="team"
                  label=uiLabelMap.team
                  value="${virtualTeamName?if_exists}"
                  isHiddenInput=false
                  required=true
                  />	
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="role"
                  label=uiLabelMap.role
                  value="${positionDescription?if_exists}"
                  isHiddenInput=false
                  required=true
                  />	
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="portlet-body form">
               <div class="form-body">
                  <@readonlyInput 
                  id="reporting"
                  label=uiLabelMap.reporting
                  value="${managedByName?if_exists}"
                  isHiddenInput=false
                  required=true
                  />	
               </div>
            </div>
         </div>
         <div class="clearfix"> </div>
      </div>
   </div>
   
   <#if isUserRm?exists && isUserRm>
   <div id="tab2" class="tab-pane fade">
      <div class="page-header">
         <h2 class="float-left">${uiLabelMap.assignPinCode!}</h2>
      </div>
      
      <form method="post" action="assignPinCodeForm" id="assignPinCodeForm" class="form-horizontal" name="addUserZipCode" novalidate="true" data-toggle="validator">
         
         <input type="hidden" name="partyId" value="${partyId?if_exists}"/>
         <input type="hidden" id="selectedPinCodes" name="selectedPinCodes"/>
         <input type="hidden" id="positionTypeId" name="positionTypeId" value="${positionTypeId!}"/>
         
         <div class="card-header mb-3">
            <div class="row">
            
            <div class="col-md-2 col-sm-2">
              <@simpleDropdownInput 
              id="countryGeoId"
              options=countryMap
              required=false
              value="${postalAddress?if_exists.countryGeoId?if_exists}"
              allowEmpty=true
              emptyText = "Country"
              dataLiveSearch=true
              />
            </div>
            <div class="col-md-2 col-sm-2">
              <@simpleDropdownInput 
              id="stateProvinceGeoId"
              required=false
              value=""
              allowEmpty=true
              emptyText = "State"
              dataLiveSearch=true
              />
            </div>
            <div class="col-md-2 col-sm-2">
              <@simpleDropdownInput 
              id="city"
              required=false
              value=""
              allowEmpty=true
              emptyText = "City"
              dataLiveSearch=true
              />
            </div>
            </div>
         </div>
         
         
         <div class="clearfix"> </div>
         <div id="zipCode" style="height:300px;"></div>
			         
         <div class="clearfix"> </div>
         <div class="col-md-12 col-sm-12">
            <div class="form-group row">
               <div class="offset-sm-6 col-sm-6 mt-3" >
                  <button id="assign-pincode-button" type="button" class="btn btn-sm btn-primary" >Assign</button>
               </div>
            </div>
         </div>
         
      </form>
      
      <div class="clearfix"> </div>
   </div>
   </#if>
   
</div>

<script>

$(document).ready(function() {
    getPinCodes();
    resetCommonEvents();
    
    $( "#countryGeoId" ).trigger( "change" );
    
    $('#stateProvinceGeoId').val("${postalAddress?if_exists.stateProvinceGeoId?if_exists}");
    $( "#stateProvinceGeoId" ).trigger( "change" );
    
    $('#city').val("${postalAddress?if_exists.city?if_exists}");
    $( "#city" ).trigger( "change" );
});

function resetCommonEvents() {
	$('#countryGeoId').unbind( "change" );
	$('#countryGeoId').bind( "change", function( event ) {
		loadStateList($(this).val());																	
	});
	
	$('#stateProvinceGeoId').unbind( "change" );
	$('#stateProvinceGeoId').bind( "change", function( event ) {
		loadCityList($(this).val());																		
	});
	
	$('#city').unbind( "change" );
	$('#city').bind( "change", function( event ) {
		getPinCodes();																
	});
}

var dsl = '';

function getPinCodes() {

    var countryGeoId = $("#countryGeoId").val();
    var stateProvinceGeoId = $("#stateProvinceGeoId").val();
    var city = $("#city").val();
    
    $('#zipCode').empty();
    
    if(countryGeoId && stateProvinceGeoId && city) {
    
	    $.post("getPinCodes", {
	        "city": city,
	        "partyId": "${partyId?if_exists}"
	    }, function(data) {
	        dsl = $('#zipCode').DualSelectList({
	            'candidateItems': data.candidateItems,
	            'selectionItems': data.selectionItems
	        });
	    });
    } else {
    	dsl = $('#zipCode').DualSelectList();
    }
}

function loadStateList(geoId) {
    var list = $("#stateProvinceGeoId");
    $('#stateProvinceGeoId').dropdown('clear');
    if (geoId != null && geoId != "") {
        var urlString = "getStateDataJSON?countryGeoId=" + geoId;
        $.ajax({
            type: 'POST',
            async: false,
            url: urlString,
            success: function(states) {
                $("#stateProvinceGeoId").empty();
                list.append("<option value=''>Select State</option>");
                if (states.length == 0) {
                    list.append("<option value = ''>N/A</option>");
                } else {
                    for (var i = 0; i < states.length; i++) {
                        if (state != null && state != "" && states[i].geoId == state) {
                            list.append("<option  value =" + states[i].geoId + " selected>" + states[i].geoName + " </option>");
                        } else {
                            list.append("<option  value =" + states[i].geoId + ">" + states[i].geoName + "</option>");
                        }
                    }
                }
            }
        });

        $('#stateProvinceGeoId').append(list);
        $('#stateProvinceGeoId').dropdown('refresh');
    }
}

function loadCityList(geoId) {
    var cityOptions = $("#city");

    $('#city').dropdown('clear');
    $.ajax({

        type: "POST",
        url: "getGeoAssocList",
        data: {
            "geoId": geoId,
            "geoAssocTypeId": "COUNTY_CITY"
        },
        async: false,
        success: function(data) {

            if (data.code == 200) {
                $("#city").empty();
                cityOptions.append("<option value=''>Select City</option>");
                for (var i = 0; i < data.results.length; i++) {
                    var result = data.results[i];
                    cityOptions.append("<option  value =" + result.geoIdTo + ">" + result.geoName + " </option>");
                } 
            }

        }

    });
    $('#city').append(cityOptions);
    $('#city').dropdown('refresh');
}

$('#assign-pincode-button').on('click', function(){

	if (dsl.getSelection().length == 0) {
		showAlert ("error", "Please Select PIN Code");
		return;
	}
	
	$('#selectedPinCodes').val(dsl.getSelection());
	
	$.post('assignPinCode', $('#assignPinCodeForm').serialize(), function(returnedData) {

		if (returnedData.code == 200) {
			
			showAlert ("success", "Successfully assign pincodes");
			
		} else {
			showAlert ("error", returnedData.message);
		}
		
		resetCommonEvents();
	});
	
});

</script>