<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#if request.getRequestURI().contains("updateUserForm") && partyId?exists && partyId?has_content>
   <#assign extra>
    <a href="/user-mgmt/control/viewUser?partyId=${partyId?if_exists}" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.back!}</a> 
    </#assign>
    <@sectionHeader title="${uiLabelMap.updateUser!}" extra=extra/>
<#else>
   <@sectionHeader title="${uiLabelMap.createUser!}" />
</#if>
<#if request.getRequestURI().contains("updateUserForm") && partyId?exists && partyId?has_content>
   <form method="post" action="updateUserService" id="updateUserService" class="form-horizontal" name="updateUserService" novalidate="novalidate" data-toggle="validator" onsubmit="return userService(this);">
   <input type="hidden" value="${partyId?if_exists}" name="partyId">
<#else>
   <form method="post" action="createUserService" id="createUserService" class="form-horizontal" name="createUserService" novalidate="novalidate" data-toggle="validator" onsubmit="return userService(this);">
</#if>
   <@sectionTitle title="${uiLabelMap.contactInformation!}" />
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <@generalInput 
         id="firstName"
         name="firstName"
         label=uiLabelMap.firstName
         placeholder=uiLabelMap.firstName
         value="${emplPositionAndPersonAndUserLogin?if_exists.firstName?if_exists}"
         required=true
         maxlength="100"
         pattern="^[ A-Za-z'@.!&:*()+-]*$"
         dataError="Please enter valid name"
         />
         <@generalInput 
         id="rmDisplayName"
         name="rmDisplayName"
         value="${emplPositionAndPersonAndUserLogin?if_exists.rmDisplayName?if_exists}"
         label=uiLabelMap.rmDisplayName
         placeholder=uiLabelMap.rmDisplayName
         required=false
         maxlength="100"
         pattern="^[ A-Za-z'@.!&:*()+-]*$"
         dataError="Please enter valid name"
         />
         <@generalInputSplitCol
         colId1="officeNoCountryCode"
         colId2="officeNo"
         label=uiLabelMap.officeNo
         colPlaceholder1 = "+91"
         colPlaceholder2 = "4442147838"
         value1="${secondaryTelecomNumber?if_exists.countryCode?if_exists}"
         value2="${secondaryTelecomNumber?if_exists.contactNumber?if_exists}"
         required=true
         maxlength1=3
         maxlength2=10
         minlength2=10
         pattern1="^[+]?[0-9]{2}$"
         pattern2="^[0-9]{0,}$"
         dataError2="Enter valid phone number"
         dataError1="Enter valid country code"
         errorId = "secondaryPhone"
         onkeyup1 = "secondaryPhoneCountryCode();"
         onkeyup = "secondaryPhoneNumber();"
         />
         <@generalInput 
         id="addressLine1"
         label=uiLabelMap.address1
         placeholder=uiLabelMap.address1
         value="${postalAddress?if_exists.address1?if_exists}"
         required=true
         maxlength="255"
         />
         <@dropdownInput 
         id="countryGeoId"
         label=uiLabelMap.country
         options=countryMap
         required=true
         value="${countryGeoIdValue?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
         <@dropdownInput 
         id="city"
         label=uiLabelMap.city
         required=true
         value="${postalAddress?if_exists.city?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
      </div>
      <div class="col-md-6 col-sm-6">
         <@generalInput 
         id="lastName"
         name="lastName"
         label=uiLabelMap.lastName
         placeholder=uiLabelMap.lastName
         value="${emplPositionAndPersonAndUserLogin?if_exists.lastName?if_exists}"
         required=false
         maxlength="100"
         pattern="^[ A-Za-z'@.!&:*()+-]*$"
         dataError="Please enter valid name"
         />
         <@generalInputSplitCol
         colId1="mobileNoCountryCode"
         colId2="mobileNo"
         label=uiLabelMap.mobileNo
         colPlaceholder1 = "+91"
         colPlaceholder2 = "4442147838"
         value1="${primaryTelecomNumber?if_exists.countryCode?if_exists}"
         value2="${primaryTelecomNumber?if_exists.contactNumber?if_exists}"
         required=true
         maxlength1=3
         maxlength2=10
         minlength2=10
         pattern1="^[+]?[0-9]{2}$"
         pattern2="^[0-9]{0,}$"
         dataError2="Enter valid phone number"
         dataError1="Enter valid country code"
         errorId = "primaryPhone"
         onkeyup1 = "primaryPhoneCountryCode();"
         onkeyup = "primaryPhoneNumber();"
         />
         <@generalInput 
         id="emailAddress"
         label=uiLabelMap.emailAddress
         placeholder="example@company.com"
         pattern = "^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$"
         value="${emailAddress?if_exists.infoString?if_exists}"
         required=true
         maxlength=255
         inputType="email"
         dataError="Enter valid email address"
         />
         <@generalInput 
         id="addressLine2"
         label=uiLabelMap.address2
         placeholder=uiLabelMap.address2
         value="${postalAddress?if_exists.address2?if_exists}"
         required=false
         maxlength="255"
         />
         <@dropdownInput 
         id="stateProvinceGeoId"
         label=uiLabelMap.state
         required=true
         value="${postalAddress?if_exists.stateProvinceGeoId?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
         <@generalInput 
         id="postalCode"
         label=uiLabelMap.pinCode
         placeholder=uiLabelMap.pinCode
         value="${postalAddress?if_exists.postalCode?if_exists}"
         required=true
         dataError="Should accept 6 digits and numbers only"
         pattern="^\\d{6}$"
         />
      </div>
   </div>
   <@sectionTitle title="${uiLabelMap.userInformation!}" />
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <#assign userLoginDisable = false/>
         <#if request.getRequestURI().contains("updateUserForm") && partyId?exists && partyId?has_content>
           <#assign userLoginDisable = true/>
         </#if>
         <@generalInput 
         id="reporting1bankid"
         name="username"	
         label=uiLabelMap.userName
         placeholder=uiLabelMap.userName
         value="${emplPositionAndPersonAndUserLogin?if_exists.userLoginId?if_exists}"
         required=true
         disabled=userLoginDisable
         maxlength="250"
         dataError="Please enter valid username"
         pattern="^[A-Za-z0-9]*$"	
         />
         <@dropdownInput 
         id="roleTypeId"
         label=uiLabelMap.role
         required=true
         options=emplPositionType
         value="${emplPositionAndPersonAndUserLogin?if_exists.emplPositionTypeId?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
      </div>
      <div class="col-md-6 col-sm-6">
         
         <@dropdownInput 
         id="virtualTeamId"
         label=uiLabelMap.team
         required=true
         options=virtualTeamList
         value="${virtualTeamId?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
         
         <#-- 
         <@dropdownInput 
         id="emplTeamId"
         label=uiLabelMap.team
         required=true
         options=emplTeam
         value="${emplPositionAndPersonAndUserLogin?if_exists.emplTeamId?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
          -->
          
         <input type="hidden" id="reportingPartyId" name="reportingPartyId" />
         <@generalInputModal 
         id="managed1bankid"
         name="managed1bankid"
         label=uiLabelMap.reporting
         placeholder=uiLabelMap.reporting
         modalName="teamMemberModal"
         value="${personManagedByName?if_exists}"
         required=true
         dataError="Please select an item in the list"
         hint="Team will be change if reporting to change"
         />
      </div>
   </div>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <#if actionType?has_content && actionType == "UPDATE">
         <@fromCommonAction showCancelBtn=true showCancelBtn=true cancelUrl="viewUser?partyId=${partyId?if_exists}" showClearBtn=false submitLabel="Update"/>
         <#else>
         <@fromCommonAction clearId="resetForm" showCancelBtn=false showClearBtn=true submitLabel="Create"/>
         </#if>
      </div>
   </div>
</form>

<div id="teamMemberModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Find Team Members</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="card-header">
               <form method="post" action="#" id="FindTeamMembers" class="form-horizontal" name="FindTeamMembers" novalidate="novalidate" data-toggle="validator">
               	<input type="hidden" id="addTeamMembers" name="addTeamMembers" value="">
                  
                  <div class="row">
                  
                  	<div class="col-md-3 col-sm-3">
                           
                           <@simpleDropdownInput 
							id="filterRoleTypeId"
							options=roleTypeList
							required=false
							allowEmpty=true
							tooltip = uiLabelMap.role
							emptyText = uiLabelMap.role
							dataLiveSearch=true
							/>
                           
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="firstNameModal"
							placeholder=uiLabelMap.firstName
							required=false
							maxlength=100
							/>
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="lastNameModal"
							placeholder=uiLabelMap.lastName
							required=false
							maxlength=100
							/>
                     </div>
                     
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="emailModal"
							placeholder="Email"
							required=false
							/>
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="phoneNumberModal"
							placeholder="Phone Number"
							required=false
							maxlength=10
							/>
                     </div>
                     <div class="col-md-3 col-sm-3">
                           
                           <@simpleDropdownInput 
							id="cityModal"
							options=cityList
							required=false
							allowEmpty=true
							tooltip = uiLabelMap.city
							emptyText = uiLabelMap.city
							dataLiveSearch=true
							/>
                           
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="postalCodeModal"
							placeholder=uiLabelMap.pinCode
							required=false
							maxlength=100
							/>
                     </div>
                     
                     <div class="col-md-1 col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary navbar-dark" onclick="javascript:getTeamMembers();">Find Team Members</button>
                     </div>
                     
                  </div>
               </form>
               <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">Team Members</h2>
            </div>
            <div class="table-responsive">
               <table id="find-team-members" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>City</th>
                        <th>PIN Code</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>

<script>

var selectedCity;

$(document).ready(function() {
getTeamMembers();
loadCityList("${postalAddress?if_exists.stateProvinceGeoId?if_exists}", "${postalAddress?if_exists.city?if_exists}");
loadStateList("${countryGeoIdValue?if_exists}", "${postalAddress?if_exists.stateProvinceGeoId?if_exists}");
     $("#stateProvinceGeoId").change(function() {
         loadCityList($(this).val(), "");
         $("#stateProvinceGeoId_error").empty();
         if($(this).val() == null || $(this).val() == "") {
            $("#stateProvinceGeoId_error").css('display','block');
            $("#stateProvinceGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         }else {
            $("#stateProvinceGeoId_error").css('display','none');
         }
     });
     $("#countryGeoId").change(function() {
         loadStateList($(this).val(), "");
         $("#countryGeoId_error").empty();
         if($(this).val() == null || $(this).val() == "") {
            $("#countryGeoId_error").css('display','block');
            $("#countryGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         } else {
            $("#countryGeoId_error").css('display','none');
         }
     });
     
     $("#city").change(function() {
         $("#city_error").empty();
         if($(this).val() == null || $(this).val() == "") {
            $("#city_error").css('display','block');
            $("#city_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         } else {
            $("#city_error").css('display','none');
         }
     });
     $("#virtualTeamId").change(function() {
         $("#roleTypeId_error").empty();
         if($(this).val() == null || $(this).val() == "") {
            $("#virtualTeamId_error").css('display','block');
            $("#virtualTeamId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         } else {
            $("#virtualTeamId_error").css('display','none');
         }
     });
     $("#roleTypeId").change(function() {
         $("#roleTypeId_error").empty();
         if($(this).val() == null || $(this).val() == "") {
            $("#roleTypeId_error").css('display','block');
            $("#roleTypeId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         } else {
            $("#roleTypeId_error").css('display','none');
         }
     });
  
$('#teamMemberModal').on('click', '.rmPartyId', function() {
   var value = $(this).children("span").attr("value");
   $('#managed1bankid').val(value);
   $('#managed1bankid_error').empty();
   $('#teamMemberModal').modal('hide');
});

$("#city").change(function() {
	checkPostalCodeValidaty();
});   

$('#postalCode').bind( "blur keyup", function( event ) {
	
	if ($('#postalCode').val().length == 0) {
		$("#postalCode_error").html('');
	}
	
	if ($('#postalCode').val().length == 6) {
		$.ajax({
			      
			type: "POST",
	     	url: "getPostalCodeDetail",
	        data:  {"postalCode": $("#postalCode").val()},
	        async: false,
	        success: function (data) {   
	            if (data.code == 200) {
	            	if (data.postalCodeDetail.postalCode) {
	            		$("#postalCode_error").html('');
	            		selectedCity = data.postalCodeDetail.city;
	            		$("#stateProvinceGeoId").val(data.postalCodeDetail.stateProvinceGeoId).change();
	            	} else {
	            		//showAlert ("error", "Invalid PIN Code");
	            		$("#postalCode_error").html('<ul class="list-unstyled"><li>Invalid PIN Code</li></ul>'); 
	            		$("#postalCode_error").closest('.form-group').addClass("has-error has-danger");
							            		
						$('#stateProvinceGeoId').dropdown("clear");	 
						$("#city").html( "" );    
						$('#city').dropdown('clear');  
						selectedCity = null;	
											   
	            	}
	            }
	        }
	        
		});    
	}

});

$("#resetForm").click(function(){
   $('#updateUserService').trigger("reset");
   $("#countryGeoId_error").empty();
   $("#countryGeoId option[value='${countryGeoIdValue?if_exists}']").removeAttr('selected');
   $("#stateProvinceGeoId_error").empty();
   $("#city_error").empty();
   $("#roleTypeId_error").empty();
   $("#virtualTeamId_error").empty();
   $("#countryGeoId_error").css('display','none');
   $("#stateProvinceGeoId_error").css('display','none');
   $("#city_error").css('display','none');
   $("#roleTypeId_error").css('display','none');
   $("#virtualTeamId_error").css('display','none');
   
});
});

 function userService() {
     var countryGeoId = $("#countryGeoId").val();
     var stateProvinceGeoId = $("#stateProvinceGeoId").val();
     var city = $("#city").val();
     var virtualTeamId = $("#virtualTeamId").val();
     var roleTypeId = $("#roleTypeId").val();
     var validate = true;
     
     $("#countryGeoId_error").empty();
     $("#stateProvinceGeoId_error").empty();
     $("#city_error").empty();
     $("#virtualTeamId_error").empty();
     $("#roleTypeId_error").empty();
     
     if(countryGeoId == '' || countryGeoId == null) {
         $("#countryGeoId_error").css('display','block');
         $("#countryGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         
         validate = false;
     } else {
        $("#countryGeoId_error").css('display','none');
     }
     
     if(stateProvinceGeoId == '' || stateProvinceGeoId == null) {
         $("#stateProvinceGeoId_error").css('display','block');
         $("#stateProvinceGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         
         validate = false;
     } else {
        $("#stateProvinceGeoId_error").css('display','none');
     }
     
     if(city == '' || city == null) {
         $("#city_error").css('display','block');
         $("#city_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         validate = false;
     } else {
         $("#city_error").css('display','none');
     }
     
     if(virtualTeamId == '' || virtualTeamId == null) {
         $("#virtualTeamId_error").css('display','block');
         $("#virtualTeamId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         validate = false;
     } else {
         $("#virtualTeamId_error").css('display','none');
     }
     
     if(roleTypeId == '' || roleTypeId == null) {
         $("#roleTypeId_error").css('display','block');
         $("#roleTypeId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         validate = false;
     } else {
         $("#roleTypeId_error").css('display','none');
     }
     
     if(!validate) {
        return false;
     }
     var managed1bankid = $("#managed1bankid").val();

     if (managed1bankid != null && managed1bankid != "") {
         var res = managed1bankid.substring(managed1bankid.indexOf("(") + 1, managed1bankid.indexOf(")"));
         $("#reportingPartyId").val(res);
     }
 }

function setTeamMember(value) {
 $('#managed1bankid').val(value);
 $('#managed1bankid_error').empty();
 $('#teamMemberModal').modal('hide');
}

function getTeamMembers() {
 var firstName = $("#firstNameModal").val();
 var lastName = $("#lastNameModal").val();
 var roleTypeId = $("#filterRoleTypeId").val();
 var email = $("#emailModal").val();
 var phoneNumber = $("#phoneNumberModal").val();
 var postalCode = $("#postalCodeModal").val();
 var city = $("#cityModal").val();
 
 $('#find-team-members').DataTable({
  "processing": true,
  "serverSide": true,
  "searching": false,
  "destroy": true,
  "ajax": {
   "url": "getTeamMembers",
   "type": "POST",
   data: {
    "firstName": firstName,
    "lastName": lastName,
    "roleTypeId": roleTypeId,
    "email" : email,
    "phoneNumber" : phoneNumber,
    "postalCode" : postalCode,
    "city" : city
   }
  },
  "Paginate": true,
  "language": {
   "emptyTable": "No data available in table",
   "info": "Showing _START_ to _END_ of _TOTAL_ entries",
   "infoEmpty": "No entries found",
   "infoFiltered": "(filtered1 from _MAX_ total entries)",
   "lengthMenu": "Show _MENU_ entries",
   "zeroRecords": "No matching records found",
   "oPaginate": {
    "sNext": "Next",
    "sPrevious": "Previous"
   }
  },
  "pageLength": 10,
  "bAutoWidth": false,
  "stateSave": false,
  "columns": [{
    "data": "partyId",
    "render": function(data, type, row, meta) {
     data = '<a href="#" class="rmPartyId"><span value ="'+row.name+' ('+row.partyId+')"></span>' + row.name + '(' + data + ')</a>';
     return data;
    }
   },
   	{ "data": "roleTypeDesc" },
   	{ "data": "email" },
   	{ "data": "phoneNumber" },
   	{ "data": "city" },
   	{ "data": "postalCode" }
  ]
 });
}

 function loadStateList(countryGeoId, stateProvinceGeoId) {
     var stateOptions = $("#stateProvinceGeoId").empty();

     $('#stateProvinceGeoId').dropdown('clear');
     $.ajax({

         type: "POST",
         url: "getGeoAssocList",
         data: {
             "geoId": countryGeoId,
             "geoAssocTypeId": "REGIONS"
         },
         async: false,
         success: function(data) {

             if (data.code == 200) {
                 $("#stateProvinceGeoId").empty();
                 stateOptions.append("<option value=''>Select City</option>");
                 for (var i = 0; i < data.results.length; i++) {
                     var result = data.results[i];
                     if (stateProvinceGeoId != null && stateProvinceGeoId != "" && result.geoIdTo == stateProvinceGeoId) {
                         stateOptions.append("<option  value =" + result.geoIdTo + " selected>" + result.geoName + " </option>");
                     } else {
                         stateOptions.append("<option  value =" + result.geoIdTo + ">" + result.geoName + " </option>");
                     }
                 }
             }

         }

     });
     $('#stateProvinceGeoId').append(stateOptions);
     $('#stateProvinceGeoId').dropdown('refresh');
 }
 
 function loadCityList(stateProvinceGeoId, city) {
     var cityOptions = $("#city").empty();

     $('#city').dropdown('clear');
     $.ajax({

         type: "POST",
         url: "getGeoAssocList",
         data: {
             "geoId": stateProvinceGeoId,
             "geoAssocTypeId": "COUNTY_CITY"
         },
         async: false,
         success: function(data) {

             if (data.code == 200) {
                 $("#city").empty();
                 cityOptions.append("<option value=''>Select City</option>");
                 for (var i = 0; i < data.results.length; i++) {
                     var result = data.results[i];
                     if (city != null && city != "" && result.geoIdTo == city) {
                         cityOptions.append("<option  value =" + result.geoIdTo + " selected>" + result.geoName + " </option>");
                     } else {
                         cityOptions.append("<option  value =" + result.geoIdTo + ">" + result.geoName + " </option>");
                     }
                 }
             }

         }

     });
     $('#city').append(cityOptions);
     if (selectedCity) {
        $("#city").val( selectedCity ).change();
     }
     $('#city').dropdown('refresh');
     selectedCity = null;
 }
    
function checkPostalCodeValidaty() {
	if ($('#postalCode').val() && $('#city').val()) {
		$.ajax({
			      
			type: "POST",
	     	url: "getPostalCodeDetail",
	        data:  {"postalCode": $("#postalCode").val(), "geoId": $("#city").val()},
	        async: false,
	        success: function (data) {   
	            if (data.code == 200) {
	            	if (data.postalCodeDetail.postalCode) {
	            		
	            	} else {
						$('#postalCode').val("");
						$('#postalCode_error').html("");
	            	}
	            }
	        }
	        
		});    
	}
}    

 function primaryPhoneCountryCode() {
     var countryCode = $("#mobileNoCountryCode").val();
     var phoneNumber = $("#mobileNo").val();
     var re = /^[+]?[0-9]{2}$/;
     if (countryCode != '') {
         if (countryCode.match(re)) {
             $("#primaryPhone_error").empty();
             if (phoneNumber == '') {
                 $("#primaryPhone_error").empty();
                 $("#primaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter phone number</li></ul>');
                 return false;
             }
         }
     }
     if (countryCode == '') {
         $("#primaryPhone_error").empty();
         $("#primaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter country code</li></ul>');
         return false;
     }
     if (re.test(countryCode) && phoneNumber != '') {
         $("#primaryPhone_error").empty();
     }

     if (phoneNumber.length > 10 && countryCode != '') {
         $("#primaryPhone_error").empty();
     }
     return true;
 }

 function primaryPhoneNumber() {
     var countryCode = $("#mobileNoCountryCode").val();
     var phoneNumber = $("#mobileNo").val();
     if (phoneNumber != '') {
         var re = new RegExp("^[0-9]{0,}$");
         if (re.test(phoneNumber)) {
             $("#primaryPhone_error").empty();
             if (countryCode == '' && phoneNumber.length == 10) {
                 $("#primaryPhone_error").html("");
                 $("#primaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter country code.</li></ul>');
                 return false;
             } else {
                 $("#primaryPhone_error").html("");
             }
         } else {
             $("#primaryPhone_error").empty();
             $("#primaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Enter valid phone number.</li></ul>');
             return false;
         }
     }
     if (phoneNumber == '') {
         $("#primaryPhone_error").empty();
         $("#primaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter phone number</li></ul>');
         return false;
     }
     if (phoneNumber != '' && countryCode != '') {
         $("#primaryPhone_error").html("");
     }
     if (phoneNumber.length > 10) {
         $("#primaryPhone_error").html("");
     }
     return true;
 }

 function secondaryPhoneCountryCode() {
     var countryCode = $("#officeNoCountryCode").val();
     var phoneNumber = $("#officeNo").val();
     var re = /^[+]?[0-9]{2}$/;
     if (countryCode != '') {
         if (countryCode.match(re)) {
             $("#secondaryPhone_error").empty();
             if (phoneNumber == '') {
                 $("#secondaryPhone_error").empty();
                 $("#secondaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter phone number</li></ul>');
                 return false;
             } else {
                 $("#secondaryPhone_error").empty();
             }
         }
     }
     if (countryCode == '') {
         $("#secondaryPhone_error").empty();
         $("#secondaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter country code</li></ul>');
         return false;
     }
     if (countryCode.match(re) && phoneNumber != '') {
         $("#secondaryPhone_error").empty();
     }
     if (phoneNumber.length > 10 && countryCode != '') {
         $("#secondaryPhone_error").empty();
     }
     return true;
 }

 function secondaryPhoneNumber() {
     var countryCode = $("#officeNoCountryCode").val();
     var phoneNumber = $("#officeNo").val();

     if (phoneNumber != '') {
         var re = new RegExp("^[0-9]{0,}$");
         if (re.test(phoneNumber)) {
             $("#secondaryPhone_error").html("");
             if (countryCode == '' && phoneNumber.length == 10) {
                 $("#secondaryPhone_error").html("");
                 $("#secondaryPhone_error").append('<ul class="list-unstyled text-danger"><li id="error_Sources">Please enter country code.</li></ul>');
                 return false;
             } else {
                 $("#secondaryPhone_error").html("");
             }
         } else {
             $("#secondaryPhone_error").empty();
             $("#secondaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Enter valid phone number.</li></ul>');
             return false;
         }

     }
     if (phoneNumber == '') {
         $("#secondaryPhone_error").empty();
         $("#secondaryPhone_error").append('<ul class="list-unstyled text-danger"><li>Please enter phone number</li></ul>');
         return false;
     }
     if (phoneNumber != '' && countryCode != '') {
         $("#secondaryPhone_error").html("");
     }
     if (phoneNumber.length > 10) {
         $("#secondaryPhone_error").html("");
     }
     return true;
 }
</script>