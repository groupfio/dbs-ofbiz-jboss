import java.util.HashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.StringUtil
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.fio.usermgmt.util.DataHelper;
import org.groupfio.user.mgmt.util.PartyUtil;
import org.fio.crm.util.VirtualTeamUtil;
import org.groupfio.user.mgmt.util.DataUtil;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String partyId = request.getParameter("partyId");

conditionList = [];
conditionList.add(EntityCondition.makeCondition(EntityOperator.OR,
    EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, "Y"),
    EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, null),
    EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, ""),
));
List < GenericValue > emplTeam = EntityQuery.use(delegator).from("EmplTeam")
    .where(conditionList).queryList();
if (emplTeam != null && emplTeam.size() > 0) {
    context.put("emplTeam", DataHelper.getDropDownOptions(emplTeam, "emplTeamId", "teamName"));
}

List < GenericValue > emplPositionType = EntityQuery.use(delegator).from("EmplPositionType").queryList();
if (emplTeam != null && emplTeam.size() > 0) {
    context.put("emplPositionType", DataHelper.getDropDownOptions(emplPositionType, "emplPositionTypeId", "description"));
}

String actionType = "CREATE";
String countryGeoIdValue = UtilProperties.getPropertyValue("crm", "defaultCountryGeoId");
if (UtilValidate.isNotEmpty(partyId)) {
    context.put("partyId", partyId);
    GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
    if (party != null && party.size() > 0) {
        actionType = "UPDATE";
        GenericValue postalAddress = PartyUtil.getPartyPrimaryPostal(delegator, partyId);
        if (postalAddress != null && postalAddress.size() > 0) {
            context.put("postalAddress", postalAddress);
            if(UtilValidate.isNotEmpty(postalAddress.getString("countryGeoId"))) {
				countryGeoIdValue = postalAddress.getString("countryGeoId");
			}
        }
        GenericValue emailAddress = PartyUtil.getPartyEmailAddress(delegator, partyId);
        if (emailAddress != null && emailAddress.size() > 0) {
            context.put("emailAddress", emailAddress);
        }

        GenericValue primaryTelecomNumber = PartyUtil.getPartyPhoneNumber(delegator, partyId, "PRIMARY_PHONE");
        if (primaryTelecomNumber != null && primaryTelecomNumber.size() > 0) {
            context.put("primaryTelecomNumber", primaryTelecomNumber);
        }
        GenericValue secondaryTelecomNumber = PartyUtil.getPartyPhoneNumber(delegator, partyId, "PHONE_WORK_SEC");
        if (secondaryTelecomNumber != null && secondaryTelecomNumber.size() > 0) {
            context.put("secondaryTelecomNumber", secondaryTelecomNumber);
        }

        conditionList = [];
        conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
            EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
            EntityCondition.makeConditionDate("emplPositionFromDate", "emplPositionThruDate"),
            EntityCondition.makeConditionDate("reportingStructFromDate", "reportingStructThruDate")
        ));
        GenericValue emplPositionAndPersonAndUserLogin = EntityQuery.use(delegator).from("EmplPositionAndPersonAndUserLogin")
            .where(conditionList).queryFirst();

        if (emplPositionAndPersonAndUserLogin != null && emplPositionAndPersonAndUserLogin.size() > 0) {
            GenericValue emplPositionFulfillment = EntityQuery.use(delegator).from("EmplPositionFulfillment")
                .where("emplPositionId", emplPositionAndPersonAndUserLogin.getString("emplPositionIdManagedBy"))
                .filterByDate().queryFirst();
            if (emplPositionFulfillment != null && emplPositionFulfillment.size() > 0) {
                managedByPartyId = emplPositionFulfillment.getString("partyId");
                if (UtilValidate.isNotEmpty(managedByPartyId)) {
                    GenericValue personManagedBy = EntityQuery.use(delegator).from("Person")
                        .where("partyId", managedByPartyId).queryOne();
                    if (personManagedBy != null && personManagedBy.size() > 0) {
                        context.put("personManagedBy", personManagedBy);
                        personManagedByName = personManagedBy.getString("firstName");
                        if (UtilValidate.isNotEmpty(personManagedBy.getString("lastName"))) {
                            personManagedByName = personManagedByName + " " + personManagedBy.getString("lastName");
                        }
                        personManagedByName = personManagedByName + " (" + managedByPartyId + ")";
                        context.put("personManagedByName", personManagedByName);
                    }
                }
                context.put("managedByPartyId", managedByPartyId);

            }

            context.put("emplPositionAndPersonAndUserLogin", emplPositionAndPersonAndUserLogin);
        }
        
        Map<String, Object> virtualTeam = VirtualTeamUtil.getFirstVirtualTeamMember(delegator, null, partyId);
        context.put("virtualTeamId", virtualTeam.get("virtualTeamId"));
    }
}
countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("user-mgmt", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}
context.put("countryGeoIdValue", countryGeoIdValue);
context.put("countryMap", countryMap);
context.put("actionType", actionType);

context.put("virtualTeamList", DataHelper.getDropDownOptions(VirtualTeamUtil.getVirtualTeamList(delegator, null), "partyId", "groupName"));
//context.put("virtualTeamList", DataHelper.getDropDownOptions(VirtualTeamUtil.getVirtualTeamList(delegator, userLogin.getString("countryGeoId"), userLogin.getString("partyId")), "partyId", "groupName"));
context.put("roleTypeList", DataHelper.getDropDownOptions(DataUtil.getDbsRoleList(delegator), "roleTypeId", "description"));

cityList = new ArrayList();

indiaStateList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoId", userLogin.getString("countryGeoId"), "geoAssocTypeId", "REGIONS"), null, false);
indiaStateList.each{ state ->
	cityAssocList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoId", state.getString("geoIdTo"), "geoAssocTypeId", "COUNTY_CITY"), null, false);
	cityList.addAll(cityAssocList);
}
context.put("cityList", DataHelper.getDropDownOptions(cityList, "geoIdTo", "geoName"));
