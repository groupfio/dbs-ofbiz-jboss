import java.util.HashMap
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.StringUtil
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import javolution.util.FastList;

import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.groupfio.user.mgmt.util.PartyUtil;
import org.ofbiz.party.party.PartyHelper;
import org.fio.crm.util.VirtualTeamUtil;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String partyId = request.getParameter("partyId");
if (UtilValidate.isNotEmpty(partyId)) {
    context.put("partyId", partyId);
    
    GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
    
    userName = PartyHelper.getPartyName(delegator, partyId, false);
	context.put("userName", userName);
    
    if (party != null && party.size() > 0) {
    
    	context.put("isLoggedUserRm", PartyUtil.isLoggedUserRm(delegator, userLogin.getString("partyId")));
    
        GenericValue postalAddress = PartyUtil.getPartyPrimaryPostal(delegator, partyId);
        if (postalAddress != null && postalAddress.size() > 0) {
            context.put("postalAddress", postalAddress);
            GenericValue geoState = delegator.findOne("Geo", UtilMisc.toMap("geoId", postalAddress.getString("stateProvinceGeoId")), false);
            if (geoState != null && geoState.size() > 0) {
                state = geoState.getString("geoName");
                context.put("state", state);
            }
            GenericValue geoCountry = delegator.findOne("Geo", UtilMisc.toMap("geoId", postalAddress.getString("countryGeoId")), false);
            if (geoCountry != null && geoCountry.size() > 0) {
                country = geoCountry.getString("geoName");
                context.put("country", country);
            }
			city = "";
			GenericValue geoCity = delegator.findOne("Geo", UtilMisc.toMap("geoId", postalAddress.getString("city")), false);
			if (geoCity != null && geoCity.size() > 0) {
				city = geoCity.getString("geoName");
			} else {
				city = postalAddress.getString("city");
			}
			context.put("city", city);
        }
        GenericValue emailAddress = PartyUtil.getPartyEmailAddress(delegator, partyId);
        if (emailAddress != null && emailAddress.size() > 0) {
            context.put("emailAddress", emailAddress);
        }

        GenericValue primaryTelecomNumber = PartyUtil.getPartyPhoneNumber(delegator, partyId, "PRIMARY_PHONE");
        if (primaryTelecomNumber != null && primaryTelecomNumber.size() > 0) {
            context.put("primaryTelecomNumber", primaryTelecomNumber);
        }
        GenericValue secondaryTelecomNumber = PartyUtil.getPartyPhoneNumber(delegator, partyId, "PHONE_WORK_SEC");
        if (secondaryTelecomNumber != null && secondaryTelecomNumber.size() > 0) {
            context.put("secondaryTelecomNumber", secondaryTelecomNumber);
        }

        conditionList = [];
        conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
            EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
            EntityCondition.makeConditionDate("emplPositionFromDate", "emplPositionThruDate"),
            EntityCondition.makeConditionDate("reportingStructFromDate", "reportingStructThruDate")
        ));
        GenericValue emplPositionAndPersonAndUserLogin = EntityQuery.use(delegator).from("EmplPositionAndPersonAndUserLogin")
            .where(conditionList).queryFirst();

        if (emplPositionAndPersonAndUserLogin != null && emplPositionAndPersonAndUserLogin.size() > 0) {
            context.put("emplPositionAndPersonAndUserLogin", emplPositionAndPersonAndUserLogin);

            emplPositionTypeId = emplPositionAndPersonAndUserLogin.getString("emplPositionTypeId");
            emplTeamId = emplPositionAndPersonAndUserLogin.getString("emplTeamId");
            emplPositionIdManagedBy = emplPositionAndPersonAndUserLogin.getString("emplPositionIdManagedBy");
            reportingToPartyId = emplPositionAndPersonAndUserLogin.getString("partyId");

            if (UtilValidate.isNotEmpty(emplTeamId)) {
                GenericValue emplTeam = EntityQuery.use(delegator).from("EmplTeam")
                    .where("emplTeamId", emplTeamId).queryOne();
                if (emplTeam != null && emplTeam.size() > 0) {
                    context.put("teamDescription", emplTeam.getString("teamName"));
                }
            }

            if (UtilValidate.isNotEmpty(emplPositionTypeId)) {
                GenericValue emplPositionType = EntityQuery.use(delegator).from("EmplPositionType")
                    .where("emplPositionTypeId", emplPositionTypeId).queryOne();
                if (emplPositionType != null && emplPositionType.size() > 0) {
                    positionDescription = emplPositionType.getString("description");
                    context.put("positionDescription", positionDescription);
                    context.put("positionTypeId", emplPositionType.getString("emplPositionTypeId"));
                    
                    if (emplPositionType.getString("emplPositionTypeId").equals("DBS_RM")) {
                    	context.put("isUserRm", true);
                    } else {
                    	context.put("isUserRm", false);
                    }
                    
                    context.put("isLoggedUserRm", PartyUtil.isLoggedUserRm(delegator, userLogin.getString("partyId")));
                    
                }
            }
            GenericValue emplPositionFulfillment = EntityQuery.use(delegator).from("EmplPositionFulfillment")
                .where("emplPositionId", emplPositionIdManagedBy).filterByDate().queryFirst();
            if (emplPositionFulfillment != null && emplPositionFulfillment.size() > 0) {
                managedByPartyId = emplPositionFulfillment.getString("partyId");
                if (UtilValidate.isNotEmpty(managedByPartyId)) {
                    GenericValue managedByPerson = EntityQuery.use(delegator).from("Person").where("partyId", managedByPartyId).queryOne();
                    if (managedByPerson != null && managedByPerson.size() > 0) {
                        managedByName = managedByPerson.getString("firstName");
                        if (UtilValidate.isNotEmpty(managedByPerson.getString("lastName"))) {
                            managedByName = managedByName + " " + managedByPerson.getString("lastName");
                        }
                        context.put("managedByName", managedByName);
                    }
                }
            }
            context.put("emplPositionAndPersonAndUserLogin", emplPositionAndPersonAndUserLogin);
            context.put("emplPositionUserLoginId", emplPositionAndPersonAndUserLogin.getString("userLoginId"));
        }
        
        Map<String, Object> virtualTeam = VirtualTeamUtil.getFirstVirtualTeamMember(delegator, null, partyId);
        context.put("virtualTeamName", virtualTeam.get("virtualTeamName"));
        
    }

    countryMap = new LinkedHashMap();
    List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("user-mgmt", "country.geo.id.list"), ",");
    if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
        for (String countryGeoId: countryGeoIdList) {
            geo = EntityQuery.use(delegator).from("Geo")
                .where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
            if (geo != null && geo.size() > 0) {
                countryMap.put(countryGeoId, geo.getString("geoName"));
            }
        }
    }

    context.put("countryMap", countryMap);
}