import java.util.HashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.fio.usermgmt.util.DataHelper;
import org.fio.crm.util.VirtualTeamUtil;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String firstName = request.getParameter("firstName");
String rmDisplayName = request.getParameter("rmDisplayName");
String userLoginId = request.getParameter("userLoginId");
String emplTeamId = request.getParameter("emplTeamId");
String roleTypeId = request.getParameter("roleTypeId");

conditionList = [];
conditionList.add(EntityCondition.makeCondition(EntityOperator.OR,
	EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, "Y"),
	EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, null),
	EntityCondition.makeCondition("isActive", EntityOperator.EQUALS, ""),
));
List < GenericValue > emplTeam = EntityQuery.use(delegator).from("EmplTeam")
		  .where(conditionList).queryList();
if(emplTeam != null && emplTeam.size() > 0) {
context.put("emplTeam", DataHelper.getDropDownOptions(emplTeam, "emplTeamId", "teamName"));
}

List < GenericValue > emplPositionType = EntityQuery.use(delegator).from("EmplPositionType").queryList();
if(emplTeam != null && emplTeam.size() > 0) {
context.put("emplPositionType", DataHelper.getDropDownOptions(emplPositionType, "emplPositionTypeId", "description"));
}

context.put("firstName", firstName);
context.put("rmDisplayName", rmDisplayName);
context.put("userLoginId", userLoginId);
context.put("emplTeamId", emplTeamId);
context.put("roleTypeId", roleTypeId);

context.put("virtualTeamList", DataHelper.getDropDownOptions(VirtualTeamUtil.getVirtualTeamList(delegator, null), "partyId", "groupName"));