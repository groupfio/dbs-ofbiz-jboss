import java.util.HashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.StringUtil
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

import org.ofbiz.entity.util.EntityQuery;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String partyId = request.getParameter("partyId");
String actionType = "CREATE";
if(UtilValidate.isNotEmpty(partyId)) {
    GenericValue partyGroup = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId", partyId), false);
    if(partyGroup != null && partyGroup.size() > 0) {
        context.put("partyGroup", partyGroup);
        context.put("partyId", partyId);
        actionType = "UPDATE";
    }
}

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("user-mgmt", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}

context.put("countryMap", countryMap);
context.put("actionType", actionType);