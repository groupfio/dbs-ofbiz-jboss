import java.util.HashMap
import java.util.List;
import java.util.Map

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.base.util.StringUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.entity.util.EntityQuery;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String teamName = request.getParameter("teamName");
String countryGeoId = request.getParameter("countryGeoId");

context.put("teamName", teamName);
context.put("countryGeoId", countryGeoId);

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("user-mgmt", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoIdGV: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoIdGV, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoIdGV, geo.getString("geoName"));
		}
	}
}
context.put("countryMap", countryMap);