import java.util.HashMap
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import javolution.util.FastList;

import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.fio.usermgmt.util.DataHelper;
import org.groupfio.user.mgmt.util.EnumUtil;
import org.groupfio.user.mgmt.util.DataUtil;

delegator = request.getAttribute("delegator");
userLogin = request.getAttribute("userLogin");
String partyId = request.getParameter("partyId");

if (UtilValidate.isNotEmpty(partyId)) {
	GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
	if(party != null && party.size() > 0) {
		context.put("statusId", party.getString("statusId"));
	}
	GenericValue partyGroup = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId", partyId), false);
    if (partyGroup != null && partyGroup.size() > 0) {
        countryName = null;
        context.put("partyGroup", partyGroup);
        context.put("partyId", partyId);
        countryGeoId = partyGroup.getString("teamCountryGeoId");
        if (UtilValidate.isNotEmpty(countryGeoId)) {
            geo = EntityQuery.use(delegator).from("Geo")
                .where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
            if (geo != null && geo.size() > 0) {
                countryName = geo.getString("geoName");
            }
        }
        context.put("countryName", countryName);
    }
	teamMemberList = [];
	List < EntityCondition > conditionList = FastList.newInstance();
	conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
		EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, partyId),
		EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT_TEAM"),
		EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "ASSIGNED_TO"),
		EntityUtil.getFilterByDateExpr()
	));
    List < GenericValue > partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
    if(partyRelationship != null && partyRelationship.size() > 0) {
		for(GenericValue partyRelationshipGV: partyRelationship) {
		    data = [:];
		    name = "";
		    GenericValue person = EntityQuery.use(delegator).from("Person").where("partyId", partyRelationshipGV.getString("partyIdTo")).queryOne();
		    if (person != null && person.size() > 0) {
			    name = person.getString("firstName");
			    if (UtilValidate.isNotEmpty(person.getString("lastName"))) {
				    name = name + " " + person.getString("lastName");
			    }
		    }
			data.put("partyIdFrom", partyRelationshipGV.getString("partyIdFrom"));
			data.put("partyIdTo", partyRelationshipGV.getString("partyIdTo"));
			data.put("securityGroupId", partyRelationshipGV.getString("securityGroupId"));
			data.put("name", name);
			
			String userRoleTypeId = "";
			String userRoleTypeDesc = "";
			GenericValue dbsRole = DataUtil.getFirstDbsRole(delegator, partyRelationshipGV.getString("partyIdTo"));
			if (UtilValidate.isNotEmpty(dbsRole)) {
				GenericValue roleType = delegator.findOne("RoleType", UtilMisc.toMap("roleTypeId", dbsRole.getString("roleTypeId")), true);
				userRoleTypeId = dbsRole.getString("roleTypeId");
				userRoleTypeDesc = roleType.getString("description");
			}
			data.put("roleTypeId", userRoleTypeId);
			data.put("roleTypeDesc", userRoleTypeDesc);
			
			teamMemberList.add(data);
		}
	context.put("teamMemberList", teamMemberList);
    }
    
	salesTeamRoleSecurity = EntityQuery.use(delegator).from("SalesTeamRoleSecurity").orderBy("sequenceNum").queryList();
	context.put("salesTeamRoleSecurity", DataHelper.getDropDownOptions(salesTeamRoleSecurity, "securityGroupId", "roleDescription"));

context.put("roleTypeList", DataHelper.getDropDownOptions(DataUtil.getDbsRoleList(delegator), "roleTypeId", "description"));
		
/*
memberTypeList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "teamMemberType");
context.put("memberTypeList", DataHelper.getDropDownOptions(memberTypeList, "enumId", "description"));
*/	
    /*datas = [];
    List < EntityCondition > conditionList = FastList.newInstance();
    conditionList.add(EntityCondition.makeCondition(EntityOperator.AND,
        EntityCondition.makeCondition("emplTeamId", EntityOperator.EQUALS, emplTeamId),
        EntityCondition.makeConditionDate("emplPositionFromDate", "emplPositionThruDate"),
        EntityCondition.makeConditionDate("reportingStructFromDate", "reportingStructThruDate")
    ));
    List < GenericValue > emplPositionAndFulfillmentAndReportingStruct = delegator.findList("EmplPositionAndFulfillmentAndReportingStruct", EntityCondition.makeCondition(conditionList, EntityOperator.AND), null, null, null, false);
    if (UtilValidate.isNotEmpty(emplPositionAndFulfillmentAndReportingStruct)) {
        for (GenericValue emplPositionAndFulfillmentAndReportingStructGV: emplPositionAndFulfillmentAndReportingStruct) {
            Map data = new HashMap();
            String emplPositionTypeId = "";
            String name = "";
            String partyId = "";
            String positionDescription = "";

            data.put("emplTeamId", emplPositionAndFulfillmentAndReportingStructGV.getString("emplTeamId"));
            data.put("emplPositionIdReportingTo", emplPositionAndFulfillmentAndReportingStructGV.getString("emplPositionIdReportingTo"));
            data.put("emplPositionIdManagedBy", emplPositionAndFulfillmentAndReportingStructGV.getString("emplPositionIdManagedBy"));

            emplPositionTypeId = emplPositionAndFulfillmentAndReportingStructGV.getString("emplPositionTypeId");
            if (UtilValidate.isNotEmpty(emplPositionTypeId)) {
                GenericValue emplPositionType = EntityQuery.use(delegator).from("EmplPositionType")
                    .where("emplPositionTypeId", emplPositionTypeId).queryOne();
                if (emplPositionType != null && emplPositionType.size() > 0) {
                    positionDescription = emplPositionType.getString("description");
                }
            }
            reportingToPartyId = emplPositionAndFulfillmentAndReportingStructGV.getString("partyId");
            if (UtilValidate.isNotEmpty(reportingToPartyId)) {
                GenericValue reportingToPerson = EntityQuery.use(delegator).from("Person").where("partyId", reportingToPartyId).queryOne();
                if (reportingToPerson != null && reportingToPerson.size() > 0) {
                    name = reportingToPerson.getString("firstName");
                    if (UtilValidate.isNotEmpty(reportingToPerson.getString("lastName"))) {
                        name = name + " " + reportingToPerson.getString("lastName");
                    }
                }
            }

            data.put("partyId", partyId);
            data.put("name", name);
            data.put("emplPositionTypeId", emplPositionTypeId);
            data.put("positionDescription", positionDescription);
            datas.add(data);
        }
    }
    context.put("teamMemberList", datas);*/
}
cityList = new ArrayList();

indiaStateList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoId", userLogin.getString("countryGeoId"), "geoAssocTypeId", "REGIONS"), null, false);
indiaStateList.each{ state ->
	cityAssocList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoId", state.getString("geoIdTo"), "geoAssocTypeId", "COUNTY_CITY"), null, false);
	cityList.addAll(cityAssocList);
}
context.put("cityList", DataHelper.getDropDownOptions(cityList, "geoIdTo", "geoName"));