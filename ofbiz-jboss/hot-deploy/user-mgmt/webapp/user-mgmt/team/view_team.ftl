<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#assign extra>
<a href="/user-mgmt/control/updateTeamForm?partyId=${partyId?if_exists}" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.edit!}</a>
<a href="/user-mgmt/control/findTeam" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.back!}</a> 

<#if statusId?exists && statusId?has_content && statusId!="PARTY_DISABLED">
<#--<a class="btn btn-xs btn-danger confirm-message" href="javascript:document.deactivateTeamForm.submit()">${uiLabelMap.deactivateTeam!}</a>-->
<a class="btn btn-xs btn-secondary btn-danger" data-toggle="confirmation" href="javascript:document.deactivateTeamForm.submit()" alt="Remove" title="Are you sure? Do you want to Deactivate Team">${uiLabelMap.deactivateTeam!}</a>
<form method="post" action="deactivateTeam" name="deactivateTeamForm">
  <input type="hidden" name="partyId" value="${partyId?if_exists}">
</form>
</#if>
</#assign>
<@sectionHeader title="${uiLabelMap.viewTeam!}" extra=extra/>
<div class="col-md-4 col-sm-4">
   <div class="portlet-body form">
      <div class="form-body">
         <@readonlyInput 
         id="name"
         label=uiLabelMap.name
         value="${partyGroup?if_exists.groupName?if_exists}"
         isHiddenInput=false
         required=true
         />	
         <@readonlyInput 
         id="description"
         label=uiLabelMap.description
         value="${partyGroup?if_exists.comments?if_exists}"
         isHiddenInput=false
         />
         <@readonlyInput 
         id="countryName"
         label=uiLabelMap.country
         value="${countryName?if_exists}"
         isHiddenInput=false
         />
      </div>
   </div>
</div>
<div class="clearfix"></div>
<div class="page-header">
   <h2 class="float-left">${uiLabelMap.teamMembers!}</h2>
</div>
<div class="table-responsive">
   <table id="view-team-members" class="table table-striped">
      <thead>
         <tr>
            <th>${uiLabelMap.teamMember!}</th>
            <th>${uiLabelMap.role!}</th>
            <th>${uiLabelMap.memberType!}</th>
            <th>${uiLabelMap.Remove!}</th>
         </tr>
      </thead>
      <tbody>
         <#if teamMemberList?exists && teamMemberList?has_content>
         <#assign i = 1/>
         <#list teamMemberList as teamMember>
            <tr>
               <td>${teamMember.name!} (${teamMember.partyIdTo!})</td>
               <td>${teamMember.roleTypeDesc!}</td>
               <td>
               <form method="post" action="updateTeamMember" name="updateTeamMemberForm_${i}" id="updateTeamMemberForm_${i}" novalidate="novalidate" data-toggle="validator">
                  <input type="hidden" name="accountTeamPartyId" value="${partyId!}">
                  <input type="hidden" name="teamMemberPartyId" value="${teamMember.partyIdTo!}">
                  <input type="hidden" name="partyId" value="${partyId!}">
                  <div class="col-md-6 col-sm-6 pl-0">
                  <@simpleDropdownInput 
                  id="securityGroupId-${i}"
                  name="securityGroupId"
                  emptyText=uiLabelMap.memberType
                  options=salesTeamRoleSecurity
                  value="${teamMember.securityGroupId!}"
                  required=true
                  dataLiveSearch=true
                  allowEmpty=true
                  />
                  </div>
               </form>
               </td>
               <td>
               
               <#-- <a href="javascript:document.updateTeamMemberForm_${i}.submit()" class="btn btn-xs btn-primary tooltips" data-original-title="Update"><i class="fa fa-check info"></i></a> -->
               <a href="#" onclick="updateTeamMember('${i}')" class="btn btn-xs btn-primary tooltips" data-original-title="Update"><i class="fa fa-check info"></i></a>
               <a class="btn btn-xs btn-secondary btn-danger" id="remove_${i}" data-toggle="confirmation" href="javascript:document.removeTeamMemberForm_${i}.submit()" alt="Remove" title="Are you sure? Do you want to Remove"><i class="fa fa-times red"></i></a>
               </a>
               <form method="post" action="removeTeamMember" name="removeTeamMemberForm_${i}">
                  <input type="hidden" name="accountTeamPartyId" value="${partyId!}">
                  <input type="hidden" name="teamMemberPartyId" value="${teamMember.partyIdTo!}">
                  <input type="hidden" name="partyId" value="${partyId!}">
               </form>
               </td>
            </tr>
            <#assign i = i+1/>
         </#list>
         </#if>
      </tbody>
   </table>
</div>

<form method="post" action="removeTeamToReportingStruct" id="removeTeamMembers" class="form-horizontal" name="removeTeamMembers" novalidate="true" data-toggle="validator">
   <input type="hidden" name="emplTeamId" id="emplTeamId" value="${emplTeamId?if_exists}">
   <input type="hidden" name="reportingTo" id="reportingTo" value="">
</form>

<div class="clearfix"></div>
<div class="page-header">
   <h2 class="float-left">${uiLabelMap.addTeamMembers!}</h2>
</div>
<form method="post" action="addTeamMember" id="addTeamMember" class="form-horizontal" name="addTeamMember" novalidate="true" data-toggle="validator" onsubmit="return teamService(this);">
   <input type="hidden" name="partyId" id="partyId" value="${partyId?if_exists}">
   <input type="hidden" name="accountTeamPartyId" id="accountTeamPartyId" value="${partyId?if_exists}">
   <input type="hidden" name="teamMemberPartyId" id="teamMemberPartyId" >
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <@generalInputModal 
         id="teamMemberParty"
         name="teamMemberParty"
         label=uiLabelMap.newTeamMember
         placeholder=uiLabelMap.newTeamMember
         modalName="teamMemberModal"
         required=true
         />
         
         <#-- 
         <@dropdownInput 
         id="memberTypeId"
         label=uiLabelMap.memberType
         options=memberTypeList
         required=true
         allowEmpty=true
         dataLiveSearch=false
         />
          -->
         
         <@dropdownInput 
         id="securityGroupId"
         label=uiLabelMap.memberType
         options=salesTeamRoleSecurity
         required=true
         allowEmpty=true
         dataLiveSearch=true
         />
          
      </div>
      <div class="col-md-12 col-sm-12">
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-9">
                 <button  class="btn btn-sm btn-primary mt-2">Add</button>
              </div>
            </div>
      </div>
   </div>
</form>


<div id="teamMemberModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Find Team Members</h4>
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="card-header">
               <form method="post" action="#" id="FindTeamMembers" class="form-horizontal" name="FindTeamMembers" novalidate="novalidate" data-toggle="validator">
               	<input type="hidden" id="addTeamMembers" name="addTeamMembers" value="">
                  
                  <div class="row">
                  
                  	<div class="col-md-3 col-sm-3">
                           
                           <@simpleDropdownInput 
							id="roleTypeId"
							options=roleTypeList
							required=false
							allowEmpty=true
							tooltip = uiLabelMap.role
							emptyText = uiLabelMap.role
							dataLiveSearch=true
							/>
                           
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="firstName"
							placeholder=uiLabelMap.firstName
							required=false
							maxlength=100
							/>
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="lastName"
							placeholder=uiLabelMap.lastName
							required=false
							maxlength=100
							/>
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="email"
							placeholder="Email"
							required=false
							/>
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="phoneNumber"
							placeholder="Phone Number"
							required=false
							maxlength=10
							/>
                     </div>
                     <div class="col-md-3 col-sm-3">
                           
                           <@simpleDropdownInput 
							id="city"
							options=cityList
							required=false
							allowEmpty=true
							tooltip = uiLabelMap.city
							emptyText = uiLabelMap.city
							dataLiveSearch=true
							/>
                           
                     </div>
                     <div class="col-md-2 col-sm-2">
                     	<@simpleInput 
							id="postalCode"
							placeholder=uiLabelMap.pinCode
							required=false
							maxlength=100
							/>
                     </div>
                     
                     <div class="col-md-1 col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary navbar-dark" onclick="javascript:getTeamMembers();">Find Team Members</button>
                     </div>
                     
                  </div>
               </form>
               <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">Team Members</h2>
            </div>
            <div class="table-responsive">
               <table id="find-team-members" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>City</th>
                        <th>PIN Code</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>

<script>

function updateTeamMember(fromId) {

	var validate = true;

	var securityGroupId = $('#securityGroupId-'+fromId).val();
	var securityGroupHelpBlock = $('form[name="updateTeamMemberForm_'+fromId+'"] .help-block');
	if(!securityGroupId) {
         validate = false;
    }
    
    if (validate) {
    	$('form[name="updateTeamMemberForm_'+fromId+'"]').submit();
    }
	
}

function teamService() {
   var teamMemberParty = $("#teamMemberParty").val();
   
   if(teamMemberParty != null && teamMemberParty != "") {
      var res = teamMemberParty.substring(teamMemberParty.indexOf("(") + 1, teamMemberParty.indexOf(")"));
      $("#teamMemberPartyId").val(res);
   }
}

 $(document).ready(function() {
  getTeamMembers();
  $('#teamMemberModal').on('click', '.rmPartyId', function() {
       var value = $(this).children("span").attr("value");
       $('#teamMemberParty').val(value);
       $('#teamMemberParty_error').empty();
       $('#teamMemberModal').modal('hide');
   });
 });

function removeTeamMember(value) {
 if(value != null && value != "") {
    $('#reportingTo').val(value);
    var form = document.getElementById("removeTeamMembers");
    form.submit();
 }
}

function getTeamMembers() {
 
 var firstName = $("#firstName").val();
 var lastName = $("#lastName").val();
 var roleTypeId = $("#roleTypeId").val();
 var email = $("#email").val();
 var phoneNumber = $("#phoneNumber").val();
 var postalCode = $("#postalCode").val();
 var city = $("#city").val();
 
 $('#find-team-members').DataTable({
  "processing": true,
  "serverSide": true,
  "searching": false,
  "destroy": true,
  "ajax": {
   "url": "getTeamMembers",
   "type": "POST",
   data: {
    "firstName": firstName,
    "lastName": lastName,
    "roleTypeId": roleTypeId,
    "email" : email,
    "phoneNumber" : phoneNumber,
    "postalCode" : postalCode,
    "city" : city
    
   }
  },
  "Paginate": true,
  "language": {
   "emptyTable": "No data available in table",
   "info": "Showing _START_ to _END_ of _TOTAL_ entries",
   "infoEmpty": "No entries found",
   "infoFiltered": "(filtered1 from _MAX_ total entries)",
   "lengthMenu": "Show _MENU_ entries",
   "zeroRecords": "No matching records found",
   "oPaginate": {
    "sNext": "Next",
    "sPrevious": "Previous"
   }
  },
  "pageLength": 10,
  "bAutoWidth": false,
  "stateSave": false,
  "columns": [
  	{
    	"data": "partyId",
    	"render": function(data, type, row, meta) {
     		data = '<a href="#" class="rmPartyId"><span value ="'+row.name+' ('+row.partyId+')"></span>' + row.name + '(' + data + ')</a>';
     		return data;
    	}
   	},
   	{ "data": "roleTypeDesc" },
   	{ "data": "email" },
   	{ "data": "phoneNumber" },
   	{ "data": "city" },
   	{ "data": "postalCode" }

  ]
 });
}

</script>