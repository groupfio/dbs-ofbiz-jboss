<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<@sectionHeader title="${uiLabelMap.findTeams!}" />
<div class="card-header">
<#--<form method="post" action="findTeam" id="findTeam" class="form-horizontal" name="findTeam" novalidate="novalidate" data-toggle="validator">-->
      <div class="row">
         
         <div class="col-md-2 col-sm-2">
            <@simpleInput 
            id="teamName"
            placeholder=uiLabelMap.name
            tooltip = uiLabelMap.name
            required=false
            maxlength=255
            />
         </div>
         <div class="col-md-2 col-sm-2">
            <@simpleDropdownInput 
            id="countryGeoId"
            emptyText=uiLabelMap.country
            options=countryMap
            required=false
            value="${countryGeoId?if_exists}"
            allowEmpty=true
            dataLiveSearch=true
            />
         </div>
         <@fromSimpleAction id="find-team" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
      </div>
      <#--</form>-->
</div>
   <div class="clearfix"> </div>
<div class="clearfix"> </div>
<div class="page-header">
   <h2 class="float-left">Teams List</h2>
</div>
<div class="table-responsive">
   <table id="team-list" class="table table-striped">
      <thead>
         <tr>
            <th>${uiLabelMap.name!}</th>
            <th>${uiLabelMap.description!}</th>
            <th>${uiLabelMap.country!}</th>
         </tr>
      </thead>
   </table>
</div>
<script >
   $(document).ready(function() {
      findTeam();
   });
   $('#find-team').on('click', function(){
       findTeam();
   });
   
   $('#teamName').on("keypress", function(e) {
        if (e.keyCode == 13) {
            findTeam();
        }
   });
   
   $("#countryGeoId").change(function () {
        findTeam();
    });
   function findTeam(){
    var url = "getTeam";
    var teamName = $('#teamName').val();
    var countryGeoId = $('#countryGeoId').val();
    $('#team-list').DataTable( {
        "processing": true,
        "serverSide": true,
        "destroy": true,
        "searching": false,
        "ajax": {
               "url": url,
               "type": "POST",
               data:  {"teamName": teamName, "countryGeoId": countryGeoId}
           },
           "pageLength": 10,
           "order": [],
           "columns": [
               { 
               "data": "groupName",
               "render": function(data, type, row, meta) {
                var partyId = row.partyId;
                if (partyId != null && partyId != "" && partyId != undefined) {
                   data = '<a href="/user-mgmt/control/viewTeam?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.groupName + '</a>';
                }
                return data;
                }
               },
               { "data": "description"},
               { "data": "countryGeoId" }
            ],
           "fnDrawCallback": function( oSettings ) {
               resetDefaultEvents();
        }
    });
   }
</script>