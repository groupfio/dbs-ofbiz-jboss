<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<#if request.getRequestURI().contains("updateTeamForm") && partyId?exists && partyId?has_content>
    <#assign extra>
    <a href="/user-mgmt/control/viewTeam?partyId=${partyId?if_exists}" class="btn btn-xs btn-primary navbar-dark">${uiLabelMap.back!}</a> 
    </#assign>
    <@sectionHeader title="${uiLabelMap.updateTeam!}" extra=extra/>
<#else>
    <@sectionHeader title="${uiLabelMap.createTeam!}" />
</#if>
<#if request.getRequestURI().contains("updateTeamForm") && partyId?exists && partyId?has_content>
    <form method="post" action="updateTeamService" id="teamForm" class="form-horizontal" name="leadForm" novalidate="novalidate" data-toggle="validator" onsubmit="return teamService(this);">
    <input type="hidden" value="${partyId?if_exists}" name="partyId">
<#else>
    <form method="post" action="createTeamService" id="teamForm" class="form-horizontal" name="leadForm" novalidate="novalidate" data-toggle="validator" onsubmit="return teamService(this);">
</#if>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
               
         <@generalInput
         id="groupName"
         label=uiLabelMap.name
         placeholder=uiLabelMap.name
         value="${partyGroup?if_exists.groupName?if_exists}"
         maxlength="255"
         required=true
         />
         <@textareaInput
         id="comments"
         label=uiLabelMap.description
         rows="5"
         placeholder = uiLabelMap.description
         value = "${partyGroup?if_exists.comments?if_exists}"
         maxlength="255"
         required = false
         />
         <@dropdownInput 
         id="countryGeoId"
         label=uiLabelMap.country
         options=countryMap
         required=true
         value="${partyGroup?if_exists.teamCountryGeoId?if_exists}"
         allowEmpty=true
         dataLiveSearch=true
         />
      </div>
   </div>
   <div class="row padding-r">
      <div class="col-md-6 col-sm-6">
         <#if actionType?has_content && actionType == "UPDATE">
         <@fromCommonAction showCancelBtn=true showCancelBtn=true cancelUrl="viewTeam?partyId=${partyId?if_exists}" showClearBtn=false submitLabel="Update"/>
         <#else>
         <@fromCommonAction clearId="resetForm" showCancelBtn=false showClearBtn=true submitLabel="Create"/>
         </#if>
      </div>
   </div>
</form>

<script>
$(document).ready(function() {
  $("#countryGeoId").change(function() {
     $("#countryGeoId_error").empty();
     if($(this).val() == null || $(this).val() == "") {
        $("#countryGeoId_error").css('display','block');
        $("#countryGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
     } else {
        $("#countryGeoId_error").css('display','none');
     }
  });
  
  $("#resetForm").click(function(){
   $('#teamForm').trigger("reset");
   $("#countryGeoId_error").empty();
   $("#countryGeoId option[value='${countryGeoIdValue?if_exists}']").removeAttr('selected');
  });
});
  function teamService() {
    var countryGeoId = $("#countryGeoId").val();
    $("#countryGeoId_error").empty();
    if(countryGeoId == '' || countryGeoId == null) {
         $("#countryGeoId_error").css('display','block');
         $("#countryGeoId_error").append('<ul class="list-unstyled"><li>Please select an item in the list.</li></ul>');
         return false;
     } else {
        $("#countryGeoId_error").css('display','none');
     }
  }
</script>