package org.groupfio.ewallet.app.validator;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
public class GetTransactionListDataValidator implements Validator {

    private static String MODULE = GetTransactionListDataValidator.class.getName();
    private boolean validate;
    /* (non-Javadoc)
     * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
     */
    @Override
    public Map<String, Object> validate(Map<String, Object> context) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> data = (Map<String, Object>) context.get("data");
        String errorSummary = null;
        try {
            setValidate(true);
            Map<String, Object> txnEnquiry = (Map<String, Object>) data.get("txnEnquiry");
            if (UtilValidate.isNotEmpty(txnEnquiry)) {
                Delegator delegator = (Delegator) context.get("delegator");
                String enqAccountNo = UtilValidate.isNotEmpty(txnEnquiry.get("enqAccountNo")) ? (String) txnEnquiry.get("enqAccountNo") : null;
                if (UtilValidate.isEmpty(enqAccountNo)) {
                    setValidate(false);
                    errorSummary = "E304";
                } else {
                    GenericValue appRegistery = WalletUtil.getActiveWalletAccount(delegator, enqAccountNo);
                    if (UtilValidate.isEmpty(appRegistery)) {
                        setValidate(false);
                        errorSummary = "E313";
                    }
                }
                if (isValidate()) {
                    String fromDateStr = UtilValidate.isNotEmpty(txnEnquiry.get("fromDate")) ? (String) txnEnquiry.get("fromDate") : null;
                    if (UtilValidate.isEmpty(fromDateStr)) {
                        setValidate(false);
                        errorSummary = "E621";
                    } else {
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            dateFormat.setLenient(false);
                            Date parsedDate = dateFormat.parse(fromDateStr);
                            Timestamp fromDate = new java.sql.Timestamp(parsedDate.getTime());
                            if (UtilValidate.isEmpty(fromDate)) {
                                setValidate(false);
                                errorSummary = "E622";
                            }
                        } catch (Exception e) {
                            Debug.logError(e, MODULE);
                            setValidate(false);
                            errorSummary = "E622";
                        }
                    }
                    if (isValidate()) {
                        String toDateStr = UtilValidate.isNotEmpty(txnEnquiry.get("toDate")) ? (String) txnEnquiry.get("toDate") : null;
                        if (UtilValidate.isEmpty(toDateStr)) {
                            setValidate(false);
                            errorSummary = "E623";
                        } else {
                            try {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                dateFormat.setLenient(false);
                                Date parsedDate = dateFormat.parse(toDateStr);
                                Timestamp toDate = new java.sql.Timestamp(parsedDate.getTime());
                                if (UtilValidate.isEmpty(toDate)) {
                                    setValidate(false);
                                    errorSummary = "E624";
                                }
                            } catch (Exception e) {
                                Debug.logError(e, MODULE);
                                setValidate(false);
                                errorSummary = "E624";
                            }
                        }
                    }
                }
            } else {
                setValidate(false);
                errorSummary = "E104";
            }
            if (!isValidate()) {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
                response.put(EMConstants.RESPONSE_MESSAGE, errorSummary);
            } else {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
            }
        } catch (Exception e) {
            Debug.log(e.getMessage(), MODULE);
            response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            response.put(EMConstants.RESPONSE_MESSAGE, "E900");
            return response;
        }
        return response;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}