package org.groupfio.ewallet.app.validator;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class CreatePartyDataValidator implements Validator {

	private static String MODULE = CreatePartyDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			
			if (UtilValidate.isEmpty(data.get("accountType"))) {
				setValidate(false);
				message = "E204";
				validationMessage.put("accountType", message);
			} else {
				GenericValue roleType = EntityUtil.getFirst( delegator.findByAnd("RoleType", UtilMisc.toMap("roleTypeId", data.get("accountType")), null, false) ); 
				if (UtilValidate.isEmpty(roleType)) {
					setValidate(false);
					message = "E204";
					validationMessage.put("accountType", message);
				}
				
				if (data.get("accountType").equals("OPERATING_ACCT_OWNER")) {
					if (UtilValidate.isEmpty(data.get("masterPartyId"))) {
						setValidate(false);
						message = "E200";
						validationMessage.put("masterPartyId", message);
					} else {
						GenericValue masterParty = delegator.findOne("Party", UtilMisc.toMap("partyId", data.get("masterPartyId")), false);
						if (UtilValidate.isEmpty(masterParty) || UtilValidate.isEmpty(masterParty.getString("roleTypeId")) || !masterParty.getString("roleTypeId").equals("MASTER_ACCT_OWNER")) {
							setValidate(false);
							message = "E222";
							validationMessage.put("masterPartyId", message);
						}
					}
				}
				
				if (data.get("accountType").equals("MASTER_ACCT_OWNER") && UtilValidate.isNotEmpty(data.get("masterPartyId"))) {
					setValidate(false);
					message = "E223";
					validationMessage.put("masterPartyId", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("partyName"))) {
				setValidate(false);
				message = "E202";
				validationMessage.put("partyName", message);
			}

            if (UtilValidate.isEmpty(data.get("requestDatetime"))) {
                setValidate(false);
                message = "E129";
                validationMessage.put("requestDatetime", message);
            }

            if (UtilValidate.isEmpty(data.get("baseCurrency"))) {
				setValidate(false);
				message = "E201";
				validationMessage.put("baseCurrency", message);
			} else {
				GenericValue uom = EntityUtil.getFirst( delegator.findByAnd("Uom", UtilMisc.toMap("uomId", data.get("baseCurrency")), null, false) ); 
				if (UtilValidate.isEmpty(uom)) {
					setValidate(false);
					message = "E221";
					validationMessage.put("baseCurrency", message);
				}
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
