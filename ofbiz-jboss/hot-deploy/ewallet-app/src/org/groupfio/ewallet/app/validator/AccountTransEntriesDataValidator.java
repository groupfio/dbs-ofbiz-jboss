package org.groupfio.ewallet.app.validator;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class AccountTransEntriesDataValidator implements Validator {

	private static String MODULE = AccountTransEntriesDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			GenericValue walletAccount = null;
			
			if (UtilValidate.isEmpty(data.get("walletAcctId"))) {
				setValidate(false);
				message = "E601";
				validationMessage.put("walletAcctId", message);
			} else {
				walletAccount = WalletUtil.getActiveWalletAccount(delegator, data.get("walletAcctId").toString()); 
				if (UtilValidate.isEmpty(walletAccount)) {
					setValidate(false);
					message = "E601";
					validationMessage.put("walletAcctId", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("fromDate"))) {
				setValidate(false);
				message = "E621";
				validationMessage.put("fromDate", message);
			} else {
				try {
					Timestamp fromDate = UtilDateTime.toTimestamp(data.get("fromDate").toString());
					if (UtilValidate.isEmpty(fromDate)) {
						setValidate(false);
						message = "E622";
						validationMessage.put("fromDate", message);
					}
				} catch (Exception e) {
					Debug.logError(e, MODULE);
					setValidate(false);
					message = "E622";
					validationMessage.put("fromDate", message);
				}
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
