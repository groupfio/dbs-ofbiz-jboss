/**
 * 
 */
package org.groupfio.ewallet.app.model;

import java.math.BigDecimal;

import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class WalletAccount {

	private String walletAcctId;
	private String walletName;
	private String walletCurrency;
	private String businessDate;
	private Double totalAvailableBalance;
	private Double availableBalance;
	private Double earMarkAmount;
	private Double maxLimitAllowed;
	private String freezeType;
	private String operationTypeId;
	
	public WalletAccount() {}
	
	public WalletAccount(GenericValue billingAccount) {
		
		if (UtilValidate.isEmpty(billingAccount)) {
			return;
		}
		
		this.walletAcctId = billingAccount.getString("externalAccountId");
		this.walletName = billingAccount.getString("name");
		this.walletCurrency = billingAccount.getString("accountCurrencyUomId");
		this.businessDate = UtilValidate.isNotEmpty(billingAccount.getTimestamp("fromDate")) ? billingAccount.getTimestamp("fromDate").toString() : null;
		this.availableBalance = UtilValidate.isNotEmpty(billingAccount.getBigDecimal("accountBalanceAmount")) ? billingAccount.getBigDecimal("accountBalanceAmount").doubleValue() : null;
		this.earMarkAmount = UtilValidate.isNotEmpty(billingAccount.getBigDecimal("earmarkAmount")) ? billingAccount.getBigDecimal("earmarkAmount").doubleValue() : null;
		this.maxLimitAllowed = UtilValidate.isNotEmpty(billingAccount.getBigDecimal("accountLimit")) ? billingAccount.getBigDecimal("accountLimit").doubleValue() : null;
		this.freezeType = billingAccount.getString("freezeTypeId");
		this.operationTypeId = billingAccount.getString("operationTypeId");
		
		BigDecimal earmarkAmount = UtilValidate.isNotEmpty(billingAccount.getBigDecimal("earmarkAmount")) ? billingAccount.getBigDecimal("earmarkAmount") : new BigDecimal(0);
		this.totalAvailableBalance = earmarkAmount.add(billingAccount.getBigDecimal("accountBalanceAmount")).doubleValue();
		
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}

	public String getWalletName() {
		return walletName;
	}

	public void setWalletName(String walletName) {
		this.walletName = walletName;
	}

	public String getWalletCurrency() {
		return walletCurrency;
	}

	public void setWalletCurrency(String walletCurrency) {
		this.walletCurrency = walletCurrency;
	}

	public String getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(String businessDate) {
		this.businessDate = businessDate;
	}

	public Double getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	public void setTotalAvailableBalance(Double totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	public Double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Double getEarMarkAmount() {
		return earMarkAmount;
	}

	public void setEarMarkAmount(Double earMarkAmount) {
		this.earMarkAmount = earMarkAmount;
	}

	public Double getMaxLimitAllowed() {
		return maxLimitAllowed;
	}

	public void setMaxLimitAllowed(Double maxLimitAllowed) {
		this.maxLimitAllowed = maxLimitAllowed;
	}

	public String getFreezeType() {
		return freezeType;
	}

	public void setFreezeType(String freezeType) {
		this.freezeType = freezeType;
	}

	public String getOperationTypeId() {
		return operationTypeId;
	}

	public void setOperationTypeId(String operationTypeId) {
		this.operationTypeId = operationTypeId;
	}
	
}
