package org.groupfio.ewallet.app.rest.response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.party.party.PartyHelper;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
public class GetTransactionList {
    private List<TxnEnqResponse> txnEnqResponse = new ArrayList<TxnEnqResponse>();
    private Header header;

    /* (non-Javadoc)
     * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
     */
    public void doBuild(Map<String, Object> context) throws Exception {
        //Setting Header values
        String msgId = UtilValidate.isNotEmpty(context.get("msgId")) ? (String) context.get("msgId") : "";
        String orgId = UtilValidate.isNotEmpty(context.get("orgId")) ? (String) context.get("orgId") : "";
        String apiKey = UtilValidate.isNotEmpty(context.get("apiKey")) ? (String) context.get("apiKey") : "";
        String timeStamp = UtilValidate.isNotEmpty(context.get("timeStamp")) ? (String) context.get("timeStamp") : "";
        this.header = new Header(msgId, orgId, apiKey, timeStamp);

        //Setting TxnEnqResponse values
        String enqStatus = UtilValidate.isNotEmpty(context.get("enqStatus")) ? (String) context.get("enqStatus") : "";
        if ("ACSP".equals(enqStatus)) {
            List<Map<String, Object>> entries = (List<Map<String, Object>>) context.get("entries");
            Delegator delegator = (Delegator) context.get("delegator");
            for (Map<String, Object> entry : entries) {
                String txnCcy = UtilValidate.isNotEmpty(entry.get("walletCurrency")) ? (String) entry.get("walletCurrency") : "";
                String txnAmount = UtilValidate.isNotEmpty(entry.get("amount")) ? entry.get("amount").toString() : "";
                String txnDate = UtilValidate.isNotEmpty(entry.get("walletTransDate")) ? entry.get("walletTransDate").toString() : "";

                String senderPartyId = UtilValidate.isNotEmpty(entry.get("senderPartyId")) ? (String) entry.get("senderPartyId") : null;
                String senderPartyName = UtilValidate.isNotEmpty(PartyHelper.getPartyName(delegator, senderPartyId, false)) ? PartyHelper.getPartyName(delegator, senderPartyId, false) : senderPartyId;
                String walletAcctIdFrom = UtilValidate.isNotEmpty(entry.get("walletAcctIdFrom")) ? (String) entry.get("walletAcctIdFrom") : "";
                Party senderParty = new Party(senderPartyName, walletAcctIdFrom);

                String receivingPartyId = UtilValidate.isNotEmpty(entry.get("receivingPartyId")) ? (String) entry.get("receivingPartyId") : null;
                String receivingPartyName = UtilValidate.isNotEmpty(PartyHelper.getPartyName(delegator, receivingPartyId, false)) ? PartyHelper.getPartyName(delegator, receivingPartyId, false) : receivingPartyId;
                String walletAcctIdTo = UtilValidate.isNotEmpty(entry.get("walletAcctIdTo")) ? (String) entry.get("walletAcctIdTo") : "";
                Party receivingParty = new Party(receivingPartyName, walletAcctIdTo);

                this.txnEnqResponse.add(new TxnEnqSuccessResponse((String) context.get("enqStatus"), (String) entry.get("txnStatus"), "", (String) entry.get("txnStatusDescription"), txnCcy, txnAmount, txnDate, senderParty, receivingParty));
            }
        } else {
            //Setting AccountBalErrorResponse values
            String enqRejectCode = UtilValidate.isNotEmpty(context.get("enqRejectCode")) ? (String) context.get("enqRejectCode") : "";
            String enqStatusDescription = UtilValidate.isNotEmpty(context.get("enqStatusDescription")) ? (String) context.get("enqStatusDescription") : "";
            this.txnEnqResponse.add(new TxnEnqErrorResponse(enqStatus , enqRejectCode, enqStatusDescription));
        }
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public List<TxnEnqResponse> getTxnEnqResponse() {
        return txnEnqResponse;
    }

    public void setTxnEnqResponse(List<TxnEnqResponse> txnEnqResponse) {
        this.txnEnqResponse = txnEnqResponse;
    }
}