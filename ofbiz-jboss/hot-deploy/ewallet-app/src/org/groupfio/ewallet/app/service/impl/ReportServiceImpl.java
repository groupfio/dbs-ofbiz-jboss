/**
 * 
 */
package org.groupfio.ewallet.app.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
public class ReportServiceImpl {

	private static final String MODULE = ReportServiceImpl.class.getName();
    
    public static Map getWalletAPIAnalysisService(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String serviceName = (String) context.get("serviceName");

    	Timestamp fromDate = (Timestamp) context.get("fromDate");
    	Timestamp thruDate = (Timestamp) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		//List<Map<String, Object>> entries = new ArrayList<Map<String, Object>>();
    		Map<String, Map<String, Object>> entries = new HashMap<String, Map<String, Object>>();
    		
			EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("requestedTime", EntityOperator.NOT_EQUAL, null),
        			EntityCondition.makeCondition("requestedTime", EntityOperator.GREATER_THAN_EQUAL_TO, fromDate)
        			);
			
			condition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("requestedTime", EntityOperator.NOT_EQUAL, null),
        			EntityCondition.makeCondition("requestedTime", EntityOperator.LESS_THAN_EQUAL_TO, thruDate),
                    condition);
				
			if (UtilValidate.isNotEmpty(serviceName)) {
				condition = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("serviceName", EntityOperator.EQUALS, serviceName),
	                    condition);
			}
			
			List<GenericValue> apiLogs = delegator.findList("WalletApiLog", condition, null, null, null, false);
			
			for (GenericValue apiLog : apiLogs) {
				
				if (!entries.containsKey(apiLog.getString("serviceName"))) {
					Map<String, Object> entry = new HashMap<String, Object>();
					entry.put("successCount", new Long(0));
					entry.put("errorCount", new Long(0));
					if (apiLog.getString("responseStatus").equals("SUCCESS")) {
						entry.put("successCount", Long.parseLong(entry.get("successCount").toString()) + 1);
					} else {
						entry.put("errorCount", Long.parseLong(entry.get("errorCount").toString()) + 1);
					}
					entries.put(apiLog.getString("serviceName"), entry);
				} else {
					Map<String, Object> entry = entries.get(apiLog.getString("serviceName"));
					if (apiLog.getString("responseStatus").equals("SUCCESS")) {
						entry.put("successCount", Long.parseLong(entry.get("successCount").toString()) + 1);
					} else {
						entry.put("errorCount", Long.parseLong(entry.get("errorCount").toString()) + 1);
					}
					entries.put(apiLog.getString("serviceName"), entry);
				}
				
			}
			
			/*EntityCondition successCondition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("responseStatus", EntityOperator.EQUALS, "SUCCESS"),
                    condition);
			
			long successCount = delegator.findCountByCondition("WalletApiLog", successCondition, null, null);
			
			EntityCondition errorCondition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("responseStatus", EntityOperator.EQUALS, "FAILED"),
                    condition);
			
			long errorCount = delegator.findCountByCondition("WalletApiLog", errorCondition, null, null);
				
			result.put("successCount", new BigDecimal(successCount));
			result.put("errorCount", new BigDecimal(errorCount));*/
    			
    		result.put("entries", entries);
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully get Wallet API Analysis.."));
    	
    	return result;
    	
    }
    
}
