package org.groupfio.ewallet.app.validator;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class CreateWalletAccountDataValidator implements Validator {

	private static String MODULE = CreateWalletAccountDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			GenericValue party = null;
			
			try {
				if (UtilValidate.isEmpty(data.get("maxLimitAllowed"))) {
					setValidate(false);
					message = "E307";
					validationMessage.put("maxLimitAllowed", message);
				} else {
					BigDecimal amount = new BigDecimal(data.get("maxLimitAllowed").toString());
					
					if (UtilValidate.isNotEmpty(amount) && amount.doubleValue() < 0) {
						setValidate(false);
						message = "E307";
						validationMessage.put("maxLimitAllowed", message);
					}
				}
			} catch (Exception e) {
				//e.printStackTrace();
				Debug.log(e.getMessage(), MODULE);
				setValidate(false);
				message = "E307";
				validationMessage.put("maxLimitAllowed", message);
			}
			
			if (isValidate()) {
			
				if (UtilValidate.isEmpty(data.get("walletAcctId"))) {
					setValidate(false);
					message = "E304";
					validationMessage.put("walletAcctId", message);
				} else {
                    //Check for special characters in the walletAcctId
                    Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
                    Matcher matcher = pattern.matcher(data.get("walletAcctId").toString());
                    if (matcher.find()) {
                        Debug.logError("walletAcctId contains special characters", MODULE);
                        setValidate(false);
                        message = "E122";
                        validationMessage.put("walletAcctId", message);
                    }
					GenericValue walletAccount = EntityUtil.getFirst( delegator.findByAnd("BillingAccount", UtilMisc.toMap("externalAccountId", data.get("walletAcctId").toString()), null, false) );
					/*if (UtilValidate.isNotEmpty(walletAccount)) {
						setValidate(false);
						message = "E312";
						validationMessage.put("walletAcctId", message);
					}*/
				}
				
				if (UtilValidate.isEmpty(data.get("partyId"))) {
					setValidate(false);
					message = "E300";
					validationMessage.put("partyId", message);
				} else {
					party = EntityUtil.getFirst( delegator.findByAnd("Party", UtilMisc.toMap("partyId", data.get("partyId")), null, false) ); 
					if (UtilValidate.isEmpty(party)) {
						setValidate(false);
						message = "E306";
						validationMessage.put("partyId", message);
					} else if (UtilValidate.isEmpty(party.getString("roleTypeId"))) {
						setValidate(false);
						message = "E306";
						validationMessage.put("partyId", message);
					}
				}
				
				if (UtilValidate.isEmpty(data.get("walletName"))) {
					setValidate(false);
					message = "E302";
					validationMessage.put("walletName", message);
				}
				
				if (UtilValidate.isEmpty(data.get("walletCurrency"))) {
					setValidate(false);
					message = "E301";
					validationMessage.put("walletCurrency", message);
				} else {
                    String walletCurrency = (String) data.get("walletCurrency");
					GenericValue uom = EntityUtil.getFirst( delegator.findByAnd("Uom", UtilMisc.toMap("uomId", walletCurrency), null, false) ); 
					if (UtilValidate.isEmpty(uom)) {
						setValidate(false);
						message = "E221";
						validationMessage.put("walletCurrency", message);
                    } else if (UtilValidate.isNotEmpty(party) && UtilValidate.isNotEmpty(party.getString("roleTypeId"))) {
                        //EWALLET-60: Wallet Currency should match with the preferred currency of party
                        if (!walletCurrency.equals(party.getString("preferredCurrencyUomId"))) {
                            setValidate(false);
                            message = "E236";
                            validationMessage.put("walletCurrency", message);
                        }
                        List<GenericValue> activeWalletAccountRoles = WalletUtil.getActiveWalletAccountRoles(delegator, party.getString("partyId"), party.getString("roleTypeId"));
                        for (GenericValue activeWalletAccountRole : activeWalletAccountRoles) {
                            EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
                                    EntityCondition.makeCondition("billingAccountId", EntityOperator.EQUALS, activeWalletAccountRole.get("billingAccountId")),
                                    EntityCondition.makeCondition("accountCurrencyUomId", EntityOperator.EQUALS, walletCurrency),
                                    EntityCondition.makeConditionDate("fromDate", "thruDate"));
                            List<GenericValue> activeWalletAccounts = delegator.findList("BillingAccount", conditions, null, null, null, false);
                            if (UtilValidate.isNotEmpty(activeWalletAccounts)) {
                                setValidate(false);
                                message = "E337";
                                validationMessage.put("walletCurrency", message);
                            }
                        }
                        if (party.getString("roleTypeId").equals("OPERATING_ACCT_OWNER")) {
                            GenericValue parentWalletAccount = WalletUtil.getActiveParentWalletAccount(delegator, party.getString("partyId"), uom.getString("uomId"));
                            if (UtilValidate.isEmpty(parentWalletAccount)) {
                                setValidate(false);
                                message = "E311";
                                validationMessage.put("walletCurrency", message);
                            }
                        }
                    }
				}
				
				if (UtilValidate.isEmpty(data.get("description"))) {
					setValidate(false);
					message = "E310";
					validationMessage.put("description", message);
				}

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                dateFormat.setLenient(false);
                if (UtilValidate.isEmpty(data.get("fromDate"))) {
                    setValidate(false);
                    message = "E323";
                    validationMessage.put("fromDate", message);
                } else {
                    try {
                        Date parsedDate = dateFormat.parse(data.get("fromDate").toString());
                        Timestamp fromDate = new java.sql.Timestamp(parsedDate.getTime());
                        if (UtilValidate.isEmpty(fromDate)) {
                            setValidate(false);
                            message = "E321";
                            validationMessage.put("fromDate", message);
                        }else{
                            if (fromDate.before(UtilDateTime.getDayStart(UtilDateTime.nowTimestamp()))) {
                                setValidate(false);
                                message = "E336";
                                validationMessage.put("fromDate", message);
                            }
                            if(UtilValidate.isNotEmpty(data.get("thruDate"))){
                                parsedDate = dateFormat.parse(data.get("thruDate").toString());
                                Timestamp thruDate = new java.sql.Timestamp(parsedDate.getTime());
                                if(fromDate.after(thruDate)){
                                    setValidate(false);
                                    message = "E331";
                                    validationMessage.put("fromDate", message);
                                }else if(fromDate.equals(thruDate)){
                                    setValidate(false);
                                    message = "E332";
                                    validationMessage.put("fromDate", message);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Debug.logError(e, MODULE);
                        setValidate(false);
                        message = "E321";
                        validationMessage.put("fromDate", message);
                    }
                }

                if (UtilValidate.isNotEmpty(data.get("thruDate"))) {
                    try {
                        Date parsedDate = dateFormat.parse(data.get("thruDate").toString());
                        Timestamp thruDate = new java.sql.Timestamp(parsedDate.getTime());
                        if (UtilValidate.isEmpty(thruDate)) {
                            setValidate(false);
                            message = "E322";
                            validationMessage.put("thruDate", message);
                        }
                    } catch (Exception e) {
                        Debug.logError(e, MODULE);
                        setValidate(false);
                        message = "E322";
                        validationMessage.put("thruDate", message);
                    }
                }
            }

			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
