/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class Party {
    private String name;
    private String accountNo;

    public Party() {
    }

    public Party(String name, String accountNo) {
        this.name = name;
        this.accountNo = accountNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
}