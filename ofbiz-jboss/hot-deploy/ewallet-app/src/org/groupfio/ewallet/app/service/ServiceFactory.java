/**
 * 
 */
package org.groupfio.ewallet.app.service;

import org.groupfio.ewallet.app.service.impl.ApplicationServiceImpl;

/**
 * @author Group Fio
 *
 */
public class ServiceFactory {

	public static final ApplicationServiceImpl APPLICATION_SERVICE_IMPL = new ApplicationServiceImpl();
	
	public static ApplicationServiceImpl getApplicationService () {
		return APPLICATION_SERVICE_IMPL;
	}
	
}
