/**
 * 
 */
package org.groupfio.ewallet.app.writer;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.rest.response.Response;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class WriterUtil {

    private static String MODULE = WriterUtil.class.getName();

    public static String writeLog(Delegator delegator, String serviceName, String clientLogRefId, Object requestedData, Object response,
            String responseCode, String responseStatus, String clientRegistryId, Timestamp requestedTime, Timestamp responsedTime) {
        return writeLog(delegator, serviceName, clientLogRefId, requestedData, response,
                 responseCode, responseStatus, clientRegistryId, requestedTime, responsedTime, null, null);
    }

    public static String writeLog(Delegator delegator, String serviceName, String clientLogRefId, Object requestedData, Object response,
            String responseCode, String responseStatus, String clientRegistryId, Timestamp requestedTime, Timestamp responsedTime, String msgId, String orgId) {

        try {
            Writer writer = WriterFactory.getLogWriter();
            Map<String, Object> writerContext = new HashMap<String, Object>();
            writerContext.put("delegator", delegator);
            writerContext.put("serviceName", serviceName);
            writerContext.put("clientLogRefId", clientLogRefId);
            writerContext.put("requestedData", requestedData);
            writerContext.put("responseCode", responseCode);
            writerContext.put("responseStatus", responseStatus);
            writerContext.put("clientRegistryId", clientRegistryId);
            writerContext.put("requestedTime", requestedTime);
            writerContext.put("responsedTime", responsedTime);
            writerContext.put("msgId", msgId);
            writerContext.put("orgId", orgId);

            Map<String, Object> writerResponse = writer.write(writerContext);
            if (ResponseUtils.isError(writerResponse)) {
                return null;
            }
            if (response instanceof Response) {
                ((Response) response).setResponseRefId(writerResponse.get("walletApiLogId").toString());
            }
            JSON jsonFeed = JSON.from(response);
            String responsedData = jsonFeed.toString();
            GenericValue apiLog = (GenericValue) writerResponse.get("apiLog");
            apiLog.put("responsedData", responsedData);
            apiLog.store();
            return (String) writerResponse.get("walletApiLogId");
        } catch (Exception e) {
            Debug.logError("Error write log>>" + e.getMessage(), MODULE);
            return null;
        }
    }
}
