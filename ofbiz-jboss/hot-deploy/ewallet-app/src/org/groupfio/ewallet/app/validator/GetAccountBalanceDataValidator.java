package org.groupfio.ewallet.app.validator;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
public class GetAccountBalanceDataValidator implements Validator {

    private static String MODULE = GetAccountBalanceDataValidator.class.getName();
    private boolean validate;

    /* (non-Javadoc)
     * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
     */
    @Override
    public Map<String, Object> validate(Map<String, Object> context) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> data = (Map<String, Object>) context.get("data");
        String errorSummary = null;
        try {
            setValidate(true);
            Map<String, Object> accountBalInfo = (Map<String, Object>) data.get("accountBalInfo");
            if (UtilValidate.isNotEmpty(accountBalInfo)) {
                Delegator delegator = (Delegator) context.get("delegator");
                String accountNo = UtilValidate.isNotEmpty(accountBalInfo.get("accountNo")) ? (String) accountBalInfo.get("accountNo") : null;
                if (UtilValidate.isEmpty(accountNo)) {
                    setValidate(false);
                    errorSummary = "E304";
                } else {
                    GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, accountNo);
                    if (UtilValidate.isEmpty(walletAccount)) {
                        setValidate(false);
                        errorSummary = "E313";
                    }
                }

                if (isValidate()) {
                    String accountCcy = UtilValidate.isNotEmpty(accountBalInfo.get("accountCcy")) ? (String) accountBalInfo.get("accountCcy") : null;
                    if (UtilValidate.isNotEmpty(accountCcy)) {
                        /*Account currency* (If provided, this information will be checked against the currency linked to the account 
                        as set-up in the profile.)*/
                        GenericValue uom = EntityQuery.use(delegator).from("Uom").where("uomId", accountCcy).cache().queryOne();
                        if (uom == null) {
                            setValidate(false);
                            errorSummary = "E221";
                        } else {
                            GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, accountNo);
                            if (walletAccount != null && !walletAccount.getString("accountCurrencyUomId").equals(accountCcy)) {
                                setValidate(false);
                                errorSummary = "E231";
                            }
                        }
                    }
                }
            } else {
                setValidate(false);
                errorSummary = "E104";
            }
            if (!isValidate()) {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
                response.put(EMConstants.RESPONSE_MESSAGE, errorSummary);
            } else {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
            }
        } catch (Exception e) {
            Debug.log(e.getMessage(), MODULE);
            response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            response.put(EMConstants.RESPONSE_MESSAGE, "E900");
            return response;
        }
        return response;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}