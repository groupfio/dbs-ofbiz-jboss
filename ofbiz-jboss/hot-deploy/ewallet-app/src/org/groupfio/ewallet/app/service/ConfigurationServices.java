
package org.groupfio.ewallet.app.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;


/**
 * @author Group Fio
 */
public class ConfigurationServices {
	
    private static final String MODULE = ConfigurationServices.class.getName();
    
    public static Map createModelConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String modelId = (String) context.get("modelId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	BigDecimal toleranceFactor = (BigDecimal) context.get("toleranceFactor");
    	String serviceName = (String) context.get("serviceName");
    	Date fromDate = (Date) context.get("fromDate");
    	Date thruDate = (Date) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("modelId", modelId);
    	result.put("supplierPartyId", supplierPartyId);
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxModelPartyAssoc",UtilMisc.toMap("modelId", modelId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isNotEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Model And Party Assoc already exists!"));
    			return result;
    		}
    		
    		partyAssoc = delegator.makeValue("LockboxModelPartyAssoc");
    		
    		GenericValue etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelId", modelId), null, false) );
			if (UtilValidate.isNotEmpty(etlModel)) {
				partyAssoc.put("modelName", etlModel.getString("modelName"));
			}
    		
    		partyAssoc.put("modelId", modelId);
    		partyAssoc.put("supplierPartyId", supplierPartyId);
    		partyAssoc.put("toleranceFactor", toleranceFactor);
    		partyAssoc.put("serviceName", serviceName);
    		partyAssoc.put("fromDate", fromDate);
    		partyAssoc.put("thruDate", thruDate);
    		
    		partyAssoc.create();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
    public static Map updateModelConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String modelId = (String) context.get("modelId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	BigDecimal toleranceFactor = (BigDecimal) context.get("toleranceFactor");
    	String serviceName = (String) context.get("serviceName");
    	Date fromDate = (Date) context.get("fromDate");
    	Date thruDate = (Date) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("modelId", modelId);
    	result.put("supplierPartyId", supplierPartyId);
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxModelPartyAssoc",UtilMisc.toMap("modelId", modelId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Model And Party Assoc not found!"));
    			return result;
    		}
    		
    		partyAssoc.put("toleranceFactor", toleranceFactor);
    		partyAssoc.put("serviceName", serviceName);
    		partyAssoc.put("fromDate", fromDate);
    		partyAssoc.put("thruDate", thruDate);
    		
    		partyAssoc.store();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
    public static Map deleteModelConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String modelId = (String) context.get("modelId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxModelPartyAssoc",UtilMisc.toMap("modelId", modelId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Model And Party Assoc not found!"));
    			return result;
    		}
    		
    		partyAssoc.remove();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
    public static Map createGroupConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String groupId = (String) context.get("groupId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	BigDecimal toleranceFactor = (BigDecimal) context.get("toleranceFactor");
    	String serviceName = (String) context.get("serviceName");
    	Date fromDate = (Date) context.get("fromDate");
    	Date thruDate = (Date) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("groupId", groupId);
    	result.put("supplierPartyId", supplierPartyId);
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxGroupPartyAssoc",UtilMisc.toMap("groupId", groupId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isNotEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Group And Party Assoc already exists!"));
    			return result;
    		}
    		
    		partyAssoc = delegator.makeValue("LockboxGroupPartyAssoc");
    		
    		GenericValue etlGroup = EntityUtil.getFirst( delegator.findByAnd("EtlGrouping", UtilMisc.toMap("groupId", groupId), null, false) );
			if (UtilValidate.isNotEmpty(etlGroup)) {
				partyAssoc.put("groupName", etlGroup.getString("groupName"));
			}
			
			partyAssoc.put("supplierName", PartyHelper.getPartyName(delegator, supplierPartyId, false));
    		
    		partyAssoc.put("groupId", groupId);
    		partyAssoc.put("supplierPartyId", supplierPartyId);
    		partyAssoc.put("toleranceFactor", toleranceFactor);
    		partyAssoc.put("serviceName", serviceName);
    		partyAssoc.put("fromDate", fromDate);
    		partyAssoc.put("thruDate", thruDate);
    		
    		partyAssoc.create();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
    public static Map updateGroupConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String groupId = (String) context.get("groupId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	BigDecimal toleranceFactor = (BigDecimal) context.get("toleranceFactor");
    	String serviceName = (String) context.get("serviceName");
    	Date fromDate = (Date) context.get("fromDate");
    	Date thruDate = (Date) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put("groupId", groupId);
    	result.put("supplierPartyId", supplierPartyId);
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxGroupPartyAssoc",UtilMisc.toMap("groupId", groupId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Group And Party Assoc not found!"));
    			return result;
    		}
    		
    		partyAssoc.put("toleranceFactor", toleranceFactor);
    		partyAssoc.put("serviceName", serviceName);
    		partyAssoc.put("fromDate", fromDate);
    		partyAssoc.put("thruDate", thruDate);
    		
    		partyAssoc.store();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
    public static Map deleteGroupConfiguration(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	
    	String groupId = (String) context.get("groupId");
    	String supplierPartyId = (String) context.get("supplierPartyId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue partyAssoc = EntityUtil.getFirst( delegator.findByAnd("LockboxGroupPartyAssoc",UtilMisc.toMap("groupId", groupId, "supplierPartyId", supplierPartyId), null, false) );
    		
    		if (UtilValidate.isEmpty(partyAssoc)) {
    			result.putAll(ServiceUtil.returnError("Group And Party Assoc not found!"));
    			return result;
    		}
    		
    		partyAssoc.remove();
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	return result;
    	
    }
    
}
