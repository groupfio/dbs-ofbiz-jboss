/**
 * 
 */
package org.groupfio.ewallet.app.rest.resource;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.rest.response.ApplyWalletTransaction;
import org.groupfio.ewallet.app.rest.response.CreateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.CreateWalletAccount;
import org.groupfio.ewallet.app.rest.response.GetWalletAccount;
import org.groupfio.ewallet.app.rest.response.GetWalletAccountTransEntries;
import org.groupfio.ewallet.app.rest.response.PerformWalletFundTransfer;
import org.groupfio.ewallet.app.rest.response.UpdateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.UpdateWalletAccount;
import org.groupfio.ewallet.app.util.AppUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.groupfio.ewallet.app.validator.Validator;
import org.groupfio.ewallet.app.validator.ValidatorFactory;
import org.groupfio.ewallet.app.writer.WriterUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.ibgmlogging.IBGMLogVo;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
@Path("/wallet/v1/trans")
public class TransactionResource {

	private static final String MODULE = TransactionResource.class.getName();

	@Context
    HttpHeaders headers;
	
	@POST
	@Path("/performWalletFundTransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PerformWalletFundTransfer performWalletFundTransfer(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        PerformWalletFundTransfer response = new PerformWalletFundTransfer();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "performWalletFundTransfer")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getPerformWalletFundTransferDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("walletAcctIdFrom", request.get("walletAcctIdFrom"));
				context.put("walletAcctIdTo", request.get("walletAcctIdTo"));
				context.put("senderPartyId", request.get("senderPartyId"));
				context.put("receivingPartyId", request.get("receivingPartyId"));
				context.put("description", request.get("description"));
				context.put("amountToTransfer", ParamUtil.getBigDecimal(request, "amountToTransfer"));
				
				context.put("externalTxnId", ParamUtil.getString(request, "walletTxnExternalId"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.performWalletFundTransfer", context);
				res.put("delegator", delegator);
				res.put("walletTxnId", res.get("paymentId"));
				res.put("walletTxnExternalId", request.get("walletTxnExternalId"));
				res.put("transAmount", context.get("amountToTransfer"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			Debug.log("Error in performWalletFundTransfer "+e.getMessage());
			response.setResponseCode("E900");
		}
		
		}
		IBGMLogVo ibgmLogVo = new IBGMLogVo();
		ibgmLogVo.setAppName("ewallet");
		ibgmLogVo.setContextPath("/ewallet/wallet/v1/trans/performWalletFundTransfer");
		ibgmLogVo.setApiName("Perform Wallet Fund Transer");
		ibgmLogVo.setPartyId("Sender PartyId: "+(String) request.get("senderPartyId")+","+" Receiver PartyId: "+(String) request.get("receivingPartyId"));
		ibgmLogVo.setWalletAcctId("Wallet Account Id From:"+(String) request.get("walletAcctIdFrom")+","+" Wallet Account Id To:"+(String) request.get("walletAcctIdTo"));
		ibgmLogVo.setRequestDateTime(requestedTime.toString());
		ibgmLogVo.setClientRecordRefId((String) request.get("clientRecordRefId"));
		ibgmLogVo.setResponseCode(response.getResponseCode());
		ibgmLogVo.setResponseDesc(ResponseUtils.getResponseStatus(response.getResponseCode()));
		org.ofbiz.base.util.ibgmlogging.IBGMLogMonitor.printIBGMLogging(ibgmLogVo);
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "performWalletFundTransfer", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	//added by m.vijayakumar for master opeating active satus issue
	public static boolean isAccountValidWithMaster(String externalAccountId,Delegator delegator)
	{
		boolean condition = true;
		try
		{
			GenericValue billingAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
			
			if(UtilValidate.isNotEmpty(billingAccount))
			{
				String freezeAccountId = billingAccount.getString("freezeTypeId");
				String operationTypeId = billingAccount.getString("operationTypeId");
				String roleType = getBillingAccountRole(billingAccount);
				
				//checking whether the account is valid such 1. operating account 2. credit debit account id and 3. is active
				if("OPERATING_ACCT_OWNER".equalsIgnoreCase(roleType) && "CREDIT_DEBIT".equalsIgnoreCase(freezeAccountId) && "ACTIVE".equalsIgnoreCase(operationTypeId))
				{
					
					//once operating account is fine then need to check master account is valid or not
					String billingAccountId = getMasterAccountBillingAcId(billingAccount);
					if(UtilValidate.isNotEmpty(billingAccountId))
					{
						
						//get operating relatted master account note: it is master account so no need to check role type
						GenericValue masterBillingAccount = EntityUtil.getFirst( delegator.findList("BillingAccount", EntityCondition.makeCondition("billingAccountId", EntityOperator.EQUALS, billingAccountId), null, null, null, false) );
						String masterfreezeAccountId = masterBillingAccount.getString("freezeTypeId");
						String masteroperationTypeId = masterBillingAccount.getString("operationTypeId");
						String billingAccountStatusId = masterBillingAccount.getString("billingAccountStatusId");
						if(UtilValidate.isNotEmpty(masterBillingAccount))
						{
							if(!("CREDIT_DEBIT".equalsIgnoreCase(masterfreezeAccountId) && "ACTIVE".equalsIgnoreCase(masteroperationTypeId) && (!"ACTIVE".equalsIgnoreCase(billingAccountStatusId))))
							{
								condition = false;
							}
							
							//if master account is in otherthan any other status of active then need to show error message, As per requested
							if(!"WALLET_ACTIVE".equalsIgnoreCase(billingAccountStatusId))
							{
								condition = false;
							}
						}
							
					}
					
				}
			}
		}catch(Exception e)
		{
			Debug.logError("Exception occured at isMasterAccountActive "+e.getMessage()	, MODULE);
		}
		return condition;
	}
	

	public static String getBillingAccountRole(GenericValue billingAccount)
	{
		String externalAccountIdRole = null;
		try
		{
			Delegator delegator = billingAccount.getDelegator();
			String billingAccountId = billingAccount.getString("billingAccountId");
			if(UtilValidate.isNotEmpty(billingAccount))
			{
				List<GenericValue>	billingAccountRoles = delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId",billingAccountId), null, false);
				if(UtilValidate.isNotEmpty(billingAccountRoles))
				{
					GenericValue billingAccountRole = EntityUtil.getFirst(billingAccountRoles);
					if(UtilValidate.isNotEmpty(billingAccountRole))
					{
						externalAccountIdRole = billingAccountRole.getString("roleTypeId");
					}
				}
			}

		}catch(Exception e)
		{
			Debug.logError("Exception occured due to "+e.getMessage(), MODULE);
			return externalAccountIdRole;
		}
		return externalAccountIdRole;
	}

	public static String getMasterAccountBillingAcId(GenericValue billingAccount)
	{
		String parentBillingAccountId = null;
		try
		{
			Delegator delegator = billingAccount.getDelegator();
			String operatingPartyId = billingAccount.getString("billingAccountId");
			if(UtilValidate.isNotEmpty(billingAccount))
			{
				List<GenericValue>	masterAccounts = delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId",operatingPartyId), null, false);
				if(UtilValidate.isNotEmpty(masterAccounts))
				{
					GenericValue masterAccount = EntityUtil.getFirst(masterAccounts);
					if(UtilValidate.isNotEmpty(masterAccount))
					{
						parentBillingAccountId = masterAccount.getString("parentBillingAccountId");
					}
					
				}
			}

		}catch(Exception e)
		{
			Debug.logError("Exception occured due to "+e.getMessage(), MODULE);
			return parentBillingAccountId;
		}
		return parentBillingAccountId;
	}
	//end @vijayakumar
	
	
	@POST
	@Path("/applyWalletTransaction")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplyWalletTransaction applyWalletTransaction(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        ApplyWalletTransaction response = new ApplyWalletTransaction();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "applyWalletTransaction")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getApplyWalletTransactionDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			
			//added by m.vijayakumar date:15-03-2018 desc: to validate transaction for operating a/c such allow if master is active only
			String externalAccountId =(String) request.get("walletAcctId"); //getting wallet account id
			if(!ResponseUtils.isError(validatorResponse))
			{
				if(!isAccountValidWithMaster(externalAccountId, delegator))
				{
					Debug.logError("Invalid master and operating acount found ", MODULE);
					Map<String, Object> validationMessage = new HashMap<String, Object>();
					validationMessage.put("MasterWaalletAcctId", "E338");
					validatorResponse.put("data", request);
					validatorResponse.put("validationMessage", validationMessage);
					validatorResponse.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				}
			}
			
			
				
			
			//end @vijayakumar
			
			
			if (!ResponseUtils.isError(validatorResponse)) {
				
				
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("transactionTypeId", request.get("transactionTypeID"));
				context.put("description", request.get("description"));
				context.put("operationTypeId", request.get("operationType"));
				context.put("amount", ParamUtil.getBigDecimal(request, "amount"));
				
				context.put("externalTxnId", ParamUtil.getString(request, "walletTxnExternalId"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.applyWalletTransaction", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				res.put("walletTxnExternalId", request.get("walletTxnExternalId"));
				res.put("transAmount", context.get("amount"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in applyWalletTransaction "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		IBGMLogVo ibgmLogVo = new IBGMLogVo();
		ibgmLogVo.setAppName("ewallet");
		ibgmLogVo.setContextPath("/ewallet/wallet/v1/trans/applyWalletTransaction");
		ibgmLogVo.setApiName("Apply Wallet Transaction");
		ibgmLogVo.setWalletAcctId((String) request.get("walletAcctId"));
		ibgmLogVo.setRequestDateTime(requestedTime.toString());
		ibgmLogVo.setClientRecordRefId((String) request.get("clientRecordRefId"));
		ibgmLogVo.setResponseCode(response.getResponseCode());
		ibgmLogVo.setResponseDesc(ResponseUtils.getResponseStatus(response.getResponseCode()));
		org.ofbiz.base.util.ibgmlogging.IBGMLogMonitor.printIBGMLogging(ibgmLogVo);
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "applyWalletTransaction", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/getWalletAccountTransEntries")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetWalletAccountTransEntries getWalletAccountTransEntries(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        GetWalletAccountTransEntries response = new GetWalletAccountTransEntries();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "getWalletAccountTransEntries")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getAccountTransEntriesDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
	
				Timestamp fromDate = null;
				Timestamp thruDate = null;
				if (UtilValidate.isNotEmpty(request.get("fromDate"))) {
					fromDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "fromDate"));
				}
				if (UtilValidate.isNotEmpty(request.get("thruDate"))) {
					thruDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "thruDate"));
				}
				context.put("fromDate", fromDate);
				context.put("thruDate", thruDate);
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.getWalletAccountTransEntries", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in getWalletAccountTransEntries "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		IBGMLogVo ibgmLogVo = new IBGMLogVo();
		ibgmLogVo.setAppName("ewallet");
		ibgmLogVo.setContextPath("/ewallet/wallet/v1/trans/getWalletAccountTransEntries");
		ibgmLogVo.setApiName("Get wallet account trans entries");
		ibgmLogVo.setWalletAcctId((String) request.get("walletAcctId"));
		ibgmLogVo.setRequestDateTime(requestedTime.toString());
		ibgmLogVo.setResponseCode(response.getResponseCode());
		ibgmLogVo.setResponseDesc(ResponseUtils.getResponseStatus(response.getResponseCode()));
		org.ofbiz.base.util.ibgmlogging.IBGMLogMonitor.printIBGMLogging(ibgmLogVo);
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "getWalletAccountTransEntries", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
}
