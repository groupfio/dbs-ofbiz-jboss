/**
 * 
 */
package org.groupfio.ewallet.app.util;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.rest.response.Response;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;

/**
 * @author Group Fio
 *
 */
public class ResponseUtils {
	
	private static final String MODULE = ResponseUtils.class.getName();

	public static boolean isSuccess(Map<String, Object> response) {
		if (Integer.valueOf(String.valueOf(response.get(EMConstants.RESPONSE_CODE))) == (ResponseCodes.SUCCESS_CODE) ) {
			return true;
		}
		return false;
	}
	
	public static boolean isError(Map<String, Object> response) {
		if (Integer.valueOf(String.valueOf(response.get(EMConstants.RESPONSE_CODE))) != (ResponseCodes.SUCCESS_CODE) ) {
			return true;
		}
		return false;
	}
	
	public static String getResponseMessage(Map<String, Object> response) {
		return (String) response.get(EMConstants.RESPONSE_MESSAGE);
	}
	
	public static int getResponseCode(Map<String, Object> response) {
		if (UtilValidate.isNotEmpty(response.get(EMConstants.RESPONSE_CODE))) {
			return (Integer) response.get(EMConstants.RESPONSE_CODE);
		}
		return 0;
	}
	
	public static String getResponseStatus(String responseCode) {
    	if (isSuccessResponse(responseCode)) {
    		return "SUCCESS";
    	}
    	return "FAILED";
	}
	
	public static boolean isSuccessResponse(String responseCode) {
    	if (UtilValidate.isNotEmpty(responseCode) && responseCode.equals("S200")) {
    		return true;
    	}
    	return false;
    }
	
	public static void prepareResponse (Delegator delegator, Response response) {
		try {
			if (UtilValidate.isEmpty( response.getResponseCodeDesc() )) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				context.put("responseCode", response.getResponseCode());
				context.put("responseRefId", response.getResponseRefId());
				context.put("delegator", delegator);
				
				response.prepareContext(context);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
	}
	
}
