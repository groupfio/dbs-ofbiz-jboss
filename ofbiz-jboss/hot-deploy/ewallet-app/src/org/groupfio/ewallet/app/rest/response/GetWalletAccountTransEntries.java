/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.groupfio.ewallet.app.model.WalletAccount;
import org.groupfio.ewallet.app.model.WalletAccountTransaction;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class GetWalletAccountTransEntries extends Response {

	private String walletAcctId;
	
	private WalletAccount walletAccount;
	
	private List<WalletAccountTransaction> entries = new ArrayList<WalletAccountTransaction>();
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		if (UtilValidate.isNotEmpty(context.get("walletAccount"))) {
			
			GenericValue billingAccount = (GenericValue) context.get("walletAccount");
			
			this.walletAccount = new WalletAccount(billingAccount);
			this.walletAccount.setTotalAvailableBalance( new BigDecimal(context.get("totalAvailableBalance").toString()).doubleValue() );
		}
		
		if (UtilValidate.isNotEmpty(context.get("entries"))) {
			List<Map<String, Object>> entries = (List<Map<String, Object>>) context.get("entries");
			for (Map<String, Object> entry : entries) {
				this.entries.add(new WalletAccountTransaction(entry));
			}
			
		}
		
		setWalletAcctId(ParamUtil.getString(context, "walletAcctId"));
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}

	public WalletAccount getWalletAccount() {
		return walletAccount;
	}

	public void setWalletAccount(WalletAccount walletAccount) {
		this.walletAccount = walletAccount;
	}

	public List<WalletAccountTransaction> getEntries() {
		return entries;
	}

	public void setEntries(List<WalletAccountTransaction> entries) {
		this.entries = entries;
	}
	
}
