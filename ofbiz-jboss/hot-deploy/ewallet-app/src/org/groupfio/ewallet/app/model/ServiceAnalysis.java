/**
 * 
 */
package org.groupfio.ewallet.app.model;

import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Group Fio
 *
 */
public class ServiceAnalysis {

	private String serviceName;
	private Long successCount;
	private Long errorCount;
	
	public ServiceAnalysis() {}
	
	public ServiceAnalysis(Map<String, Object> entry) {
		
		if (UtilValidate.isEmpty(entry)) {
			return;
		}
				
		this.serviceName = ParamUtil.getString(entry, "serviceName");
		this.successCount = ParamUtil.getLong(entry, "successCount");
		this.errorCount = ParamUtil.getLong(entry, "errorCount");
		
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(Long successCount) {
		this.successCount = successCount;
	}

	public Long getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}
	
}
