/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class AccountBalResponse {
    protected String enqStatus;

    public AccountBalResponse() {
    }

    public AccountBalResponse(String enqStatus) {
        this.enqStatus = enqStatus;
    }

    public String getEnqStatus() {
        return enqStatus;
    }

    public void setEnqStatus(String enqStatus) {
        this.enqStatus = enqStatus;
    }
}