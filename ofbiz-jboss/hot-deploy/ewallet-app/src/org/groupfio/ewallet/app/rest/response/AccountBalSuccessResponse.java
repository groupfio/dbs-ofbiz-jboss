package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class AccountBalSuccessResponse extends AccountBalResponse {
    private String accountName;
    private String accountNo;
    private String accountCcy;
    private String clsAvailableBal;
    private String businessDate;

    public AccountBalSuccessResponse() {}

    public AccountBalSuccessResponse(String enqStatus, String accountName, String accountNo, String accountCcy, String businessDate, String clsAvailableBal) {
        this.enqStatus = enqStatus;
        this.accountName = accountName;
        this.accountNo = accountNo;
        this.accountCcy = accountCcy;
        this.businessDate = businessDate;
        this.clsAvailableBal = clsAvailableBal;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountCcy() {
        return accountCcy;
    }

    public void setAccountCcy(String accountCcy) {
        this.accountCcy = accountCcy;
    }

    public String getClsAvailableBal() {
        return clsAvailableBal;
    }

    public void setClsAvailableBal(String clsAvailableBal) {
        this.clsAvailableBal = clsAvailableBal;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }
}