package org.groupfio.ewallet.app.permissionChecking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CustomerPermissionChecking {

	/*public static Map validateCustomerPermission(DispatchContext dctx, Map context) {
		
		Map<String, Object> result = new HashMap<String, Object>();

		Delegator delegator = (Delegator) dctx.getDelegator();
		GenericValue userLogin1 = (GenericValue) context.get("userLogin");

		String partyId = (String) context.get("partyId");
		String screenName = (String) context.get("screenName");
		String status = "Y";

		if(UtilValidate.isNotEmpty(partyId)){

			try {

				String userLoginId = "";

				if(UtilValidate.isNotEmpty(userLogin1)){

					userLoginId = userLogin1.getString("userLoginId");

				}else{

					GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("partyId",userLoginId).queryFirst();
					if(UtilValidate.isNotEmpty(userLogin))

						userLoginId = userLogin.getString("userLoginId");

				}

				if(UtilValidate.isNotEmpty(userLoginId)){

					GenericValue userLoginAttribute = EntityQuery.use(delegator).from("UserLoginAttribute").where("userLoginId",userLoginId,"attrName","REGION").queryFirst();

					if(UtilValidate.isNotEmpty(userLoginAttribute)) {			

						String parentRoleTypeId = userLoginAttribute.getString("attrValue");

						if(UtilValidate.isNotEmpty(parentRoleTypeId)) {

							GenericValue parentRole = EntityQuery.use(delegator).from("RoleType").where("roleTypeId",parentRoleTypeId).queryFirst();

							if(UtilValidate.isNotEmpty(parentRole)) {

								List<GenericValue> roleType = EntityQuery.use(delegator).from("RoleType").where("parentTypeId",parentRoleTypeId).queryList();

								if(UtilValidate.isNotEmpty(roleType)) {

									List<String> roleTypeIdList = EntityUtil.getFieldListFromEntityList(roleType, "roleTypeId", true);

									if(UtilValidate.isNotEmpty(roleTypeIdList)) {

										EntityCondition partyRoleCondition = EntityCondition.makeCondition(UtilMisc.toList(
												EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, roleTypeIdList),
												EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)), EntityOperator.AND);

										List<GenericValue> partyRole = EntityQuery.use(delegator).from("PartyRole").where(partyRoleCondition).queryList();

										if(UtilValidate.isNotEmpty(partyRole)) {

											List<String> roleTypeIdList1 = EntityUtil.getFieldListFromEntityList(partyRole, "roleTypeId", true);
											List<GenericValue> securityGroupRoleTypeAssoc = EntityQuery.use(delegator).from("SecurityGroupRoleTypeAssoc").where(EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, roleTypeIdList1)).queryList();

											if(UtilValidate.isNotEmpty(securityGroupRoleTypeAssoc)) {

												List<String> groupIdList = EntityUtil.getFieldListFromEntityList(securityGroupRoleTypeAssoc, "groupId", true);

												EntityCondition userLoginSecurityGroupCondition = EntityCondition.makeCondition(UtilMisc.toList(
														EntityCondition.makeCondition("groupId", EntityOperator.IN, groupIdList),
														EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId)), EntityOperator.AND);

												List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup").where(userLoginSecurityGroupCondition).filterByDate().queryList();

												if(UtilValidate.isNotEmpty(userLoginSecurityGroup)){

													List<String> groupId = EntityUtil.getFieldListFromEntityList(userLoginSecurityGroup, "groupId", true);

													if(UtilValidate.isNotEmpty(groupId)) {

														List<GenericValue> securityGroupPermission = EntityQuery.use(delegator).from("SecurityGroupPermission").where(EntityCondition.makeCondition("groupId", EntityOperator.IN, groupId)).queryList();

														if(UtilValidate.isNotEmpty(securityGroupPermission)) {

															List<String> permissionId = EntityUtil.getFieldListFromEntityList(securityGroupPermission, "permissionId", true);

															if(UtilValidate.isNotEmpty(permissionId)) {

																EntityCondition ofbizPageSecurityCondition = EntityCondition.makeCondition(UtilMisc.toList(
																		EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionId),
																		EntityCondition.makeCondition("pageId", EntityOperator.EQUALS, screenName)), EntityOperator.AND);

																List<GenericValue> ofbizPageSecurity = EntityQuery.use(delegator).from("OfbizPageSecurity").where(ofbizPageSecurityCondition).queryList();

																if(UtilValidate.isNotEmpty(ofbizPageSecurity)) {

																	status = "Y";
																}
															}
														}
													}				
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}catch(Exception e) {
				Debug.log("Error Log"+e.toString());
			}
		}
		result.put("status", status);
		return result;

	}*/
	
public static Map validateCustomerPermission(DispatchContext dctx, Map context) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		HttpServletRequest request = (HttpServletRequest) context.get("request");
		Delegator delegator = (Delegator) dctx.getDelegator();
		
		HttpSession session = request.getSession();
		String roleTypeId = (String) session.getAttribute("_USER_PERMISSION_ROLE_");
		
		Debug.log("Permission----->"+roleTypeId);
		String screenName = (String) context.get("screenName");
		String status = "N";

		if(UtilValidate.isNotEmpty(roleTypeId)){
            try {
                GenericValue securityGroupRoleTypeAssoc = EntityQuery.use(delegator).from("SecurityGroupRoleTypeAssoc").where(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, roleTypeId)).queryOne();
                if(UtilValidate.isNotEmpty(securityGroupRoleTypeAssoc)) {
                    String groupId = securityGroupRoleTypeAssoc.getString("groupId");
					if(UtilValidate.isNotEmpty(groupId)){
                        List<GenericValue> securityGroupPermission = EntityQuery.use(delegator).from("SecurityGroupPermission").where(EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupId)).queryList();
                        if(UtilValidate.isNotEmpty(securityGroupPermission)) {
                        List<String> permissionId = EntityUtil.getFieldListFromEntityList(securityGroupPermission, "permissionId", true);
                            if(UtilValidate.isNotEmpty(permissionId)) {
                                EntityCondition ofbizPageSecurityCondition = EntityCondition.makeCondition(UtilMisc.toList(
			                    EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionId),
								EntityCondition.makeCondition("pageId", EntityOperator.EQUALS, screenName)), EntityOperator.AND);
                                List<GenericValue> ofbizPageSecurity = EntityQuery.use(delegator).from("OfbizPageSecurity").where(ofbizPageSecurityCondition).queryList();
                                if(UtilValidate.isNotEmpty(ofbizPageSecurity)) {
                                    status = "Y";
								}
							}
						}
					}
			}
			}catch(Exception e) {
				Debug.log("Error Log"+e.toString());
			}
		}
		result.put("status", status);
		return result;

	}

}
