/**
 * 
 */
package org.groupfio.ewallet.app.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.ewallet.app.util.TransactionUtil;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
public class TransactionServiceImpl {

	private static final String MODULE = TransactionServiceImpl.class.getName();
    
    public static Map performWalletFundTransfer(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String walletAcctIdFrom = (String) context.get("walletAcctIdFrom");
    	String walletAcctIdTo = (String) context.get("walletAcctIdTo");
    	String senderPartyId = (String) context.get("senderPartyId");
    	String receivingPartyId = (String) context.get("receivingPartyId");
    	String description = (String) context.get("description");
    	BigDecimal amountToTransfer = (BigDecimal) context.get("amountToTransfer");
    	
    	String externalTxnId = (String) context.get("externalTxnId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue walletAcctFrom = WalletUtil.getActiveWalletAccount(delegator, walletAcctIdFrom);
    		GenericValue walletAcctTo = WalletUtil.getActiveWalletAccount(delegator, walletAcctIdTo);
    		
    		if (walletAcctFrom != null && walletAcctFrom.size()>0 && walletAcctTo != null && walletAcctTo.size()>0) {
    			
    			Map<String, Object> pContext = new HashMap<String, Object>();
    			
    			pContext.put("amount", amountToTransfer);
    			pContext.put("partyIdFrom", senderPartyId);
    			pContext.put("partyIdTo", receivingPartyId);
    			pContext.put("comments", description);
    			
    			String currencyUomId = walletAcctFrom.getString("accountCurrencyUomId");
    			if(UtilValidate.isNotEmpty(currencyUomId))
    			pContext.put("currencyUomId", currencyUomId);
    			
    			pContext.put("paymentMethodTypeId", "EXT_BILLACT");
    			pContext.put("paymentTypeId", "CUSTOMER_PAYMENT");
    			pContext.put("statusId", "PMNT_SENT");
    			
    			pContext.put("userLogin", userLogin);
    			
    			Map<String, Object> pRes = dispatcher.runSync("createPayment", pContext);
    			
    			if (ServiceUtil.isSuccess(pRes)) {
    				
    				String paymentId = (String) pRes.get("paymentId");
    				
    				GenericValue walletAcctFromPayment = delegator.findOne("Payment", UtilMisc.toMap("paymentId", paymentId), false);
    				walletAcctFromPayment.put("transactionTypeId", "NORMAL");
    				walletAcctFromPayment.put("operationTypeId", "TOP-UP");
    				walletAcctFromPayment.put("externalTxnId", externalTxnId);
    				walletAcctFromPayment.put("walletAcctIdFrom", walletAcctFrom.getString("externalAccountId"));
    				
    				String externalAccountId = walletAcctTo.getString("externalAccountId");
    				if(UtilValidate.isNotEmpty(externalAccountId))
    				walletAcctFromPayment.put("walletAcctIdTo", externalAccountId);
    				
    				walletAcctFrom.put("accountBalanceAmount", walletAcctFrom.getBigDecimal("accountBalanceAmount").subtract(amountToTransfer));
    				walletAcctFrom.store();
    				
    				walletAcctTo.put("accountBalanceAmount", walletAcctTo.getBigDecimal("accountBalanceAmount").add(amountToTransfer));
    				walletAcctTo.store();
    				
    				result.put("paymentId", paymentId);
    				
    				walletAcctFromPayment.put("availableBalance", walletAcctFrom.getBigDecimal("accountBalanceAmount"));
    				walletAcctFromPayment.store();
    				
    				// another payment
    				pContext = new HashMap<String, Object>();
        			
        			pContext.put("amount", amountToTransfer);
        			pContext.put("partyIdFrom", receivingPartyId);
        			pContext.put("partyIdTo", senderPartyId);
        			pContext.put("comments", description);
        			pContext.put("currencyUomId", walletAcctFrom.getString("accountCurrencyUomId"));
        			pContext.put("paymentMethodTypeId", "EXT_BILLACT");
        			pContext.put("paymentTypeId", "CUSTOMER_PAYMENT");
        			pContext.put("statusId", "PMNT_SENT");
        			
        			pContext.put("userLogin", userLogin);
        			
        			pRes = dispatcher.runSync("createPayment", pContext);
        			
        			if (ServiceUtil.isSuccess(pRes)) {
        				
        				GenericValue walletAcctToPayment = delegator.findOne("Payment", UtilMisc.toMap("paymentId", pRes.get("paymentId")), false);
        				walletAcctToPayment.put("transactionTypeId", "NORMAL");
        				walletAcctToPayment.put("operationTypeId", "WITHDRAWAL");
        				walletAcctToPayment.put("externalTxnId", externalTxnId);
        				walletAcctToPayment.put("walletAcctIdFrom", walletAcctTo.getString("externalAccountId"));
        				walletAcctToPayment.put("walletAcctIdTo", walletAcctFrom.getString("externalAccountId"));
        				
        				walletAcctToPayment.put("availableBalance", walletAcctTo.getBigDecimal("accountBalanceAmount"));
        				walletAcctToPayment.store();
        			}
    				
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully Perform Wallet Fund Transfer.."));
    	
    	return result;
    	
    }

    public static Map<String, Object> applyWalletTransaction(DispatchContext dctx, Map context) {
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String externalAccountId = (String) context.get("externalAccountId");
        String transactionTypeId = (String) context.get("transactionTypeId");
        String operationTypeId = (String) context.get("operationTypeId");
        String description = (String) context.get("description");
        BigDecimal amount = (BigDecimal) context.get("amount");
        String externalTxnId = (String) context.get("externalTxnId");
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
            if (walletAccount != null && walletAccount.size()>0) {
                GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccount.getString("billingAccountId"));
                String walletAccountPartyId = null;
                if (walletAccountRole != null && walletAccountRole.size()>0) {
                    walletAccountPartyId = walletAccountRole.getString("partyId");
                    if ("OPERATING_ACCT_OWNER".equals(walletAccountRole.getString("roleTypeId")) && UtilValidate.isNotEmpty(walletAccountRole.getString("parentBillingAccountId"))) {
                        String masterWalletAcctId = WalletUtil.getExternalAccountId(delegator, walletAccountRole.getString("parentPartyId"));
                        result.put("masterWalletAcctId", masterWalletAcctId);
                    } else {
                        result.put("masterWalletAcctId", externalAccountId);
                    }
                }

                Map<String, Object> pContext = new HashMap<String, Object>();
                pContext.put("amount", amount);
                pContext.put("partyIdFrom", "Company");
                pContext.put("partyIdTo", walletAccountPartyId);
                pContext.put("comments", description);
                pContext.put("currencyUomId", walletAccount.getString("accountCurrencyUomId"));
                pContext.put("paymentMethodTypeId", "EXT_BILLACT");
                pContext.put("paymentTypeId", "CUSTOMER_PAYMENT");
                pContext.put("statusId", "PMNT_SENT");
                pContext.put("userLogin", userLogin);
                Map<String, Object> pRes = dispatcher.runSync("createPayment", pContext);
                if (ServiceUtil.isSuccess(pRes)) {
                    String paymentId = (String) pRes.get("paymentId");
                    GenericValue payment = delegator.findOne("Payment", UtilMisc.toMap("paymentId", paymentId), false);
                    payment.put("transactionTypeId", transactionTypeId);
                    payment.put("operationTypeId", operationTypeId);
                    payment.put("externalTxnId", externalTxnId);
                    payment.put("walletAcctIdFrom", walletAccount.getString("externalAccountId"));
                    if (operationTypeId.equals("TOP-UP")) {
                        walletAccount.put("accountBalanceAmount", walletAccount.getBigDecimal("accountBalanceAmount").add(amount));
                    } else if (operationTypeId.equals("WITHDRAWAL")) {
                        walletAccount.put("earmarkAmount", walletAccount.getBigDecimal("earmarkAmount").subtract(amount));
                    } else if (operationTypeId.equals("EM_ADD")) {
                        walletAccount.put("earmarkAmount", UtilValidate.isNotEmpty(walletAccount.getBigDecimal("earmarkAmount")) ? walletAccount.getBigDecimal("earmarkAmount").add(amount) : amount);
                        walletAccount.put("accountBalanceAmount", walletAccount.getBigDecimal("accountBalanceAmount").subtract(amount));
                    } else if (operationTypeId.equals("EM_RELEASE")) {
                        walletAccount.put("earmarkAmount", UtilValidate.isNotEmpty(walletAccount.getBigDecimal("earmarkAmount")) ? walletAccount.getBigDecimal("earmarkAmount").subtract(amount) : amount);
                        walletAccount.put("accountBalanceAmount", walletAccount.getBigDecimal("accountBalanceAmount").add(amount));
                    }
                    walletAccount.store();
                    result.put("paymentId", paymentId);
                    result.put("accountBalanceAmount", walletAccount.getBigDecimal("accountBalanceAmount"));
                    payment.put("availableBalance", walletAccount.getBigDecimal("accountBalanceAmount"));
                    payment.store();
                }
            }
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            result.putAll(ServiceUtil.returnError(e.getMessage()));
            return result;
        }
        result.putAll(ServiceUtil.returnSuccess("Successfully Perform Wallet Fund Transfer.."));
        return result;
    }

    public static Map<String, Object> getWalletAccountTransEntries(DispatchContext dctx, Map context) {
        Delegator delegator = (Delegator) dctx.getDelegator();
        String externalAccountId = (String) context.get("externalAccountId");
        Timestamp fromDate = (Timestamp) context.get("fromDate");
        Timestamp thruDate = (Timestamp) context.get("thruDate");
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            List<Map<String, Object>> entries = new ArrayList<Map<String, Object>>();
            GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
            BigDecimal totalAvailableBalance = new BigDecimal(0);
            if (walletAccount != null && walletAccount.size()>0) {
                totalAvailableBalance = TransactionUtil.getTotalAvailableBalance(delegator, walletAccount);
                GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccount.getString("billingAccountId"));
                String walletAccountPartyId = null;
                if (walletAccountRole != null && walletAccountRole.size()>0) {
                    walletAccountPartyId = walletAccountRole.getString("partyId");
                }
                if (UtilValidate.isNotEmpty(walletAccountPartyId)) {
                    List<String> processedExternalTxnIds = new ArrayList<String>();

                    EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
                            EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, walletAccountPartyId)
                    );
                    if (UtilValidate.isNotEmpty(fromDate)) {
                        condition = EntityCondition.makeCondition(EntityOperator.AND,
                                EntityCondition.makeCondition("effectiveDate", EntityOperator.NOT_EQUAL, null),
                                EntityCondition.makeCondition("effectiveDate", EntityOperator.GREATER_THAN_EQUAL_TO, fromDate),
                                condition);
                    }
                    if (UtilValidate.isNotEmpty(thruDate)) {
                        condition = EntityCondition.makeCondition(EntityOperator.AND,
                                EntityCondition.makeCondition("effectiveDate", EntityOperator.NOT_EQUAL, null),
                                EntityCondition.makeCondition("effectiveDate", EntityOperator.LESS_THAN_EQUAL_TO, thruDate),
                                condition);
                    }
                    List<GenericValue> payments = EntityQuery.use(delegator).from("Payment").where(condition).orderBy("createdTxStamp").queryList();
                    //Get all the processed withdrawals/released transactions
                    condition = EntityCondition.makeCondition(EntityOperator.OR,
                            EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS, "WITHDRAWAL"),
                            EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS, "EM_RELEASE")
                    );
                    List<GenericValue> processedTransactions = EntityUtil.filterByCondition(payments, condition);
                    for (GenericValue processedTransaction : processedTransactions) {
                        Map<String, Object> entry = new HashMap<String, Object>();
                        entry.put("walletAcctIdFrom", WalletUtil.getExternalAccountId(delegator, processedTransaction.getString("partyIdFrom")));
                        entry.put("walletAcctIdTo", WalletUtil.getExternalAccountId(delegator, processedTransaction.getString("partyIdTo")));
                        entry.put("walletTxnId", processedTransaction.getString("paymentId"));
                        entry.put("walletTransDate", processedTransaction.get("effectiveDate"));
                        entry.put("senderPartyId", processedTransaction.get("partyIdFrom"));
                        entry.put("receivingPartyId", processedTransaction.get("partyIdTo"));
                        entry.put("walletCurrency", walletAccount.getString("accountCurrencyUomId"));
                        entry.put("transactionTypeId", processedTransaction.get("transactionTypeId"));
                        entry.put("operationType", processedTransaction.get("operationTypeId"));
                        entry.put("amount", processedTransaction.get("amount"));
                        entry.put("availableBalance", processedTransaction.get("availableBalance"));
                        entry.put("externalTxnId", processedTransaction.get("externalTxnId"));
                        if ("WITHDRAWAL".equals(processedTransaction.get("operationTypeId"))) {
                            entry.put("txnStatus", "ACSP");
                        } else {
                            entry.put("txnStatus", "RJCT");
                        }
                        entry.put("txnStatusDescription", "WITHDRAWL");
                        entries.add(entry);
                        processedExternalTxnIds.add(processedTransaction.getString("externalTxnId"));
                    }

                    //Get all the unprocessed EM_ADD transactions which are neither withdrawal nor released yet
                    condition = EntityCondition.makeCondition(EntityOperator.AND,
                            EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS, "EM_ADD"),
                            EntityCondition.makeCondition("externalTxnId", EntityOperator.NOT_IN, processedExternalTxnIds)
                    );
                    List<GenericValue> unProcessedTransactions = EntityUtil.filterByCondition(payments, condition);
                    for (GenericValue unProcessedTransaction : unProcessedTransactions) {
                        Map<String, Object> entry = new HashMap<String, Object>();
                        entry.put("walletAcctIdFrom", WalletUtil.getExternalAccountId(delegator, unProcessedTransaction.getString("partyIdFrom")));
                        entry.put("walletAcctIdTo", WalletUtil.getExternalAccountId(delegator, unProcessedTransaction.getString("partyIdTo")));
                        entry.put("walletTxnId", unProcessedTransaction.getString("paymentId"));
                        entry.put("walletTransDate", unProcessedTransaction.get("effectiveDate"));
                        entry.put("senderPartyId", unProcessedTransaction.get("partyIdFrom"));
                        entry.put("receivingPartyId", unProcessedTransaction.get("partyIdTo"));
                        entry.put("walletCurrency", walletAccount.getString("accountCurrencyUomId"));
                        entry.put("transactionTypeId", unProcessedTransaction.get("transactionTypeId"));
                        entry.put("operationType", unProcessedTransaction.get("operationTypeId"));
                        entry.put("amount", unProcessedTransaction.get("amount"));
                        entry.put("availableBalance", unProcessedTransaction.get("availableBalance"));
                        entry.put("externalTxnId", unProcessedTransaction.get("externalTxnId"));
                        entry.put("txnStatus", "ACTC");
                        entry.put("txnStatusDescription", "WITHDRAWL");
                        entries.add(entry);
                    }

                    //Get all the TOP-UP transactions which are neither withdrawal nor released yet
                    condition = EntityCondition.makeCondition(EntityOperator.AND,
                            EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS, "TOP-UP"),
                            EntityCondition.makeCondition("transactionTypeId", EntityOperator.EQUALS, "NORMAL")
                    );
                    List<GenericValue> topupTransactions = EntityUtil.filterByCondition(payments, condition);
                    for (GenericValue topupTransaction : topupTransactions) {
                        Map<String, Object> entry = new HashMap<String, Object>();
                        entry.put("walletAcctIdFrom", WalletUtil.getExternalAccountId(delegator, topupTransaction.getString("partyIdFrom")));
                        entry.put("walletAcctIdTo", WalletUtil.getExternalAccountId(delegator, topupTransaction.getString("partyIdTo")));
                        entry.put("walletTxnId", topupTransaction.getString("paymentId"));
                        entry.put("walletTransDate", topupTransaction.get("effectiveDate"));
                        entry.put("senderPartyId", topupTransaction.get("partyIdFrom"));
                        entry.put("receivingPartyId", topupTransaction.get("partyIdTo"));
                        entry.put("walletCurrency", walletAccount.getString("accountCurrencyUomId"));
                        entry.put("transactionTypeId", topupTransaction.get("transactionTypeId"));
                        entry.put("operationType", topupTransaction.get("operationTypeId"));
                        entry.put("amount", topupTransaction.get("amount"));
                        entry.put("availableBalance", topupTransaction.get("availableBalance"));
                        entry.put("externalTxnId", topupTransaction.get("externalTxnId"));
                        entry.put("txnStatus", "ACSP");
                        entry.put("txnStatusDescription", "TOPUP");
                        entries.add(entry);
                    }
                }
                if (entries.size() > 0) {
                    Collections.sort(entries, new PaymentDateComparator());
                }
                Debug.log("==========entries=========="+entries);
                result.put("walletAccount", walletAccount);
                result.put("totalAvailableBalance", totalAvailableBalance);
                result.put("entries", entries);
            }
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            result.putAll(ServiceUtil.returnError(e.getMessage()));
            return result;
        }
        result.putAll(ServiceUtil.returnSuccess("Successfully get Wallet Account transaction Entries.."));
        return result;
    }

    protected static class PaymentDateComparator implements Comparator<Map<String, Object>> {
        public int compare(Map<String, Object> payment1, Map<String, Object> payment2) {
            Timestamp transactionDate1 = (Timestamp) payment1.get("walletTransDate");
            Timestamp transactionDate2 = (Timestamp) payment2.get("walletTransDate");
            if (transactionDate1 != null && transactionDate2 != null) {
                return transactionDate1.compareTo(transactionDate2);
            }
            return 0;
        }
    }
}
