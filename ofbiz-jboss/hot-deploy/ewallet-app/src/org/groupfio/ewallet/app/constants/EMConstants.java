/**
 * 
 */
package org.groupfio.ewallet.app.constants;

/**
 * @author Group Fio
 * @since June 16, 2015
 *
 */
public class EMConstants {
	
	// Resource bundles	
    public static final String configResource = "ewallet-app";
    public static final String uiLabelMap = "EwalletManagerUiLabels";
    
    public static final int DEFAULT_BUFFER_SIZE = 102400;
    public static final int LOCKBOX_ITEM_SEQUENCE_ID_DIGITS = 5;
    
    public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	
	public static final class AppStatus {
        private AppStatus() { }
        public static final String ACTIVATED = "ACTIVATED";
        public static final String DEACTIVATED = "DEACTIVATED";
    }
	
	public static final class SourceInvoked {
        private SourceInvoked() { }
        public static final String API = "API";
        public static final String PORTAL = "PORTAL";
        public static final String UNKNOWN = "UNKNOWN";
    }
	
}
