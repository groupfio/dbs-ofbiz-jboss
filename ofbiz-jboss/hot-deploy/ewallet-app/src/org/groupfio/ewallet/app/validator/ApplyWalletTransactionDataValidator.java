package org.groupfio.ewallet.app.validator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class ApplyWalletTransactionDataValidator implements Validator {

	private static String MODULE = ApplyWalletTransactionDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			GenericValue walletAccount = null;
			String operationType = UtilValidate.isNotEmpty(data.get("operationType")) ? data.get("operationType").toString() : null;
			boolean isWalletAccountTransactionable = false;
			
			try {
				if (UtilValidate.isEmpty(data.get("amount"))) {
					setValidate(false);
					message = "E503";
					validationMessage.put("amount", message);
				} else {
					BigDecimal amount = new BigDecimal(data.get("amount").toString());
					
					if (UtilValidate.isNotEmpty(amount) && !(amount.doubleValue() > 0)) {
						setValidate(false);
						message = "E503";
						validationMessage.put("amount", message);
					}
				}
			} catch (Exception e) {
				//e.printStackTrace();
				Debug.log(e.getMessage(), MODULE);
				setValidate(false);
				message = "E503";
				validationMessage.put("amount", message);
			}
			
			if (isValidate()) {
			
				if (UtilValidate.isEmpty(data.get("walletAcctId"))) {
					setValidate(false);
					message = "E501";
					validationMessage.put("walletAcctId", message);
				} else {
					walletAccount = WalletUtil.getActiveWalletAccount(delegator, data.get("walletAcctId").toString()); 
					if (UtilValidate.isEmpty(walletAccount)) {
						setValidate(false);
						message = "E501";
						validationMessage.put("walletAcctId", message);
					} else if (!UtilValidate.isEmpty(data.get("operationType"))) {
						isWalletAccountTransactionable = WalletUtil.isWalletAccountTransactionable(walletAccount, operationType);
						if (!isWalletAccountTransactionable) {
							setValidate(false);
							message = "E523";
							validationMessage.put("walletAcctId", message);
						}
					}
				}
				
				if (UtilValidate.isEmpty(data.get("transactionTypeID"))) {
					setValidate(false);
					message = "E502";
					validationMessage.put("transactionTypeID", message);
				} else {
					GenericValue transactionType = EntityUtil.getFirst( delegator.findByAnd("TransactionType", UtilMisc.toMap("transactionTypeId", data.get("transactionTypeID")), null, false) ); 
					if (UtilValidate.isEmpty(transactionType)) {
						setValidate(false);
						message = "E502";
						validationMessage.put("transactionTypeID", message);
					} else if (UtilValidate.isNotEmpty(operationType)) {
						
						if (data.get("transactionTypeID").equals("EARMARK") && (!operationType.equals("EM_RELEASE") && !operationType.equals("EM_ADD"))) {
							setValidate(false);
							message = "E524";
							validationMessage.put("transactionTypeID", message);
						} else if (data.get("transactionTypeID").equals("NORMAL") && (!operationType.equals("TOP-UP") && !operationType.equals("WITHDRAWAL"))) {
							setValidate(false);
							message = "E524";
							validationMessage.put("transactionTypeID", message);
						}
						
					}
					
					if (UtilValidate.isNotEmpty(transactionType) && (!data.get("transactionTypeID").equals("NORMAL") && !data.get("transactionTypeID").equals("EARMARK"))) {
						setValidate(false);
						message = "E525";
						validationMessage.put("transactionTypeID", message);
					}
				}
				
				if (UtilValidate.isEmpty(data.get("operationType"))) {
					setValidate(false);
					message = "E521";
					validationMessage.put("operationType", message);
				} else {
					GenericValue ot = EntityUtil.getFirst( delegator.findByAnd("OperationType", UtilMisc.toMap("operationTypeId", data.get("operationType")), null, false) ); 
					if (UtilValidate.isEmpty(ot)) {
						setValidate(false);
						message = "E521";
						validationMessage.put("operationType", message);
					} else if (UtilValidate.isNotEmpty(data.get("amount")) && isWalletAccountTransactionable) {
						if (data.get("operationType").equals("TOP-UP")) {
							BigDecimal amount = new BigDecimal(data.get("amount").toString());
							BigDecimal currentBalance = walletAccount.getBigDecimal("accountBalanceAmount").add(amount);
							if (currentBalance.doubleValue() > walletAccount.getBigDecimal("accountLimit").doubleValue()) {
								setValidate(false);
								message = "E526";
								validationMessage.put("amount", message);
							}
							
						} else if (data.get("operationType").equals("WITHDRAWAL")) {
							BigDecimal earmarkAmount = UtilValidate.isNotEmpty(walletAccount.getBigDecimal("earmarkAmount")) ? walletAccount.getBigDecimal("earmarkAmount") : new BigDecimal(0);
							BigDecimal amountToWithdraw = new BigDecimal(data.get("amount").toString());
							if (amountToWithdraw.compareTo(earmarkAmount) >= 1) {
								setValidate(false);
								message = "E527";
								validationMessage.put("amount", message);
							}
							
						} else if (data.get("operationType").equals("EM_RELEASE")) {
							BigDecimal earmarkAmount = UtilValidate.isNotEmpty(walletAccount.getBigDecimal("earmarkAmount")) ? walletAccount.getBigDecimal("earmarkAmount") : new BigDecimal(0);
							BigDecimal amountToRelease = new BigDecimal(data.get("amount").toString());
							if (amountToRelease.compareTo(earmarkAmount) >= 1) {
								setValidate(false);
								message = "E528";
								validationMessage.put("amount", message);
							}
							
						} else if (data.get("operationType").equals("EM_ADD")) {
							BigDecimal earmarkAmountToAdd = new BigDecimal(data.get("amount").toString());
							BigDecimal accountBalanceAmount = walletAccount.getBigDecimal("accountBalanceAmount");
							if (earmarkAmountToAdd.compareTo(accountBalanceAmount) >= 1) {
								setValidate(false);
								message = "E529";
								validationMessage.put("amount", message);
							}
						}
					}
				}
				
				if (UtilValidate.isEmpty(data.get("description"))) {
					setValidate(false);
					message = "E522";
					validationMessage.put("description", message);
				}

                if (UtilValidate.isEmpty(data.get("walletTxnExternalId"))) {
                    setValidate(false);
                    message = "E127";
                    validationMessage.put("walletTxnExternalId", message);
                }
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
