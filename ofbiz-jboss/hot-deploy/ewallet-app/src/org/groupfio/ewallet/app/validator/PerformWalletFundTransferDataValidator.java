package org.groupfio.ewallet.app.validator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class PerformWalletFundTransferDataValidator implements Validator {

	private static String MODULE = PerformWalletFundTransferDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			GenericValue walletAccountFrom = null;
			GenericValue walletAccountTo = null;
			
			try {
				if (UtilValidate.isEmpty(data.get("amountToTransfer"))) {
					setValidate(false);
					message = "E403";
					validationMessage.put("amountToTransfer", message);
				} else {
					BigDecimal amountToTransfer = new BigDecimal(data.get("amountToTransfer").toString());
					
					if (UtilValidate.isNotEmpty(amountToTransfer) && amountToTransfer.doubleValue() < 0) {
						setValidate(false);
						message = "E403";
						validationMessage.put("amountToTransfer", message);
					}
				}
			} catch (Exception e) {
				//e.printStackTrace();
				Debug.log(e.getMessage(), MODULE);
				setValidate(false);
				message = "E403";
				validationMessage.put("amountToTransfer", message);
			}
			
			if (isValidate()) {
			
				if (UtilValidate.isEmpty(data.get("walletAcctIdFrom"))) {
					setValidate(false);
					message = "E401";
					validationMessage.put("walletAcctIdFrom", message);
				} else {
					walletAccountFrom = WalletUtil.getActiveWalletAccount(delegator, data.get("walletAcctIdFrom").toString()); 
					if (walletAccountFrom == null) {
						setValidate(false);
						message = "E401";
						validationMessage.put("walletAcctIdFrom", message);
					} else if (!WalletUtil.isWalletAccountTransactionable(walletAccountFrom, "WITHDRAWAL")) {
						setValidate(false);
						message = "E401";
						validationMessage.put("walletAcctIdFrom", message);
					} else if (UtilValidate.isNotEmpty(data.get("amountToTransfer"))) {
						if(walletAccountFrom.size()>0) {
                            BigDecimal totalTransferableAmount = UtilValidate.isNotEmpty(walletAccountFrom.getBigDecimal("accountBalanceAmount")) ? walletAccountFrom.getBigDecimal("accountBalanceAmount") : new BigDecimal(0);
                            BigDecimal txnAmount = new BigDecimal(data.get("amountToTransfer").toString());
                            if (txnAmount.compareTo(totalTransferableAmount) > 0) {
                                setValidate(false);
                                message = "E403";
                                validationMessage.put("amountToTransfer", message);
                            }
						}
					}
				}
				
				if (UtilValidate.isEmpty(data.get("walletAcctIdTo"))) {
					setValidate(false);
					message = "E402";
					validationMessage.put("walletAcctIdTo", message);
				} else {
					walletAccountTo = WalletUtil.getActiveWalletAccount(delegator, data.get("walletAcctIdTo").toString()); 
					if (walletAccountTo == null) {
						setValidate(false);
						message = "E402";
						validationMessage.put("walletAcctIdTo", message);
					} else if (!WalletUtil.isWalletAccountTransactionable(walletAccountTo, "TOP-UP")) {
						setValidate(false);
						message = "E402";
						validationMessage.put("walletAcctIdTo", message);
                    } else if (UtilValidate.isNotEmpty(data.get("amountToTransfer"))) {
                        if (walletAccountTo.size()>0) {
                            BigDecimal earmarkAmount = UtilValidate.isNotEmpty(walletAccountTo.getBigDecimal("earmarkAmount")) ? walletAccountTo.getBigDecimal("earmarkAmount") : new BigDecimal(0);
                            BigDecimal accountBalanceAmount = UtilValidate.isNotEmpty(walletAccountTo.getBigDecimal("accountBalanceAmount")) ? walletAccountTo.getBigDecimal("accountBalanceAmount") : new BigDecimal(0);
                            BigDecimal txnAmount = new BigDecimal(data.get("amountToTransfer").toString());
                            BigDecimal newBalance = accountBalanceAmount.add(earmarkAmount).add(txnAmount);
                            BigDecimal accountLimit = walletAccountTo.getBigDecimal("accountLimit");
                            if (newBalance.compareTo(accountLimit) > 0) {
                                setValidate(false);
                                validationMessage.put("amountToTransfer", message);
                            }
                        }
                    }
                }

				if (UtilValidate.isNotEmpty(walletAccountFrom) && UtilValidate.isNotEmpty(walletAccountTo)) {
					if (UtilValidate.isNotEmpty(walletAccountFrom.getString("accountCurrencyUomId")) 
							&& UtilValidate.isNotEmpty(walletAccountTo.getString("accountCurrencyUomId"))
							&& !walletAccountFrom.getString("accountCurrencyUomId").equals(walletAccountTo.getString("accountCurrencyUomId"))
							) {
						setValidate(false);
						message = "E421";
						validationMessage.put("amountToTransfer", message);
					}
				}
				
				if (UtilValidate.isEmpty(data.get("senderPartyId"))) {
					setValidate(false);
					message = "E404";
					validationMessage.put("senderPartyId", message);
				} else {
					GenericValue party = EntityUtil.getFirst( delegator.findByAnd("PartyGroup", UtilMisc.toMap("partyId", data.get("senderPartyId")), null, false) ); 
					if (UtilValidate.isEmpty(party)) {
						setValidate(false);
						message = "E404";
						validationMessage.put("senderPartyId", message);
					}
				}
				
				if (UtilValidate.isEmpty(data.get("receivingPartyId"))) {
					setValidate(false);
					message = "E405";
					validationMessage.put("receivingPartyId", message);
				} else {
					GenericValue party = EntityUtil.getFirst( delegator.findByAnd("PartyGroup", UtilMisc.toMap("partyId", data.get("receivingPartyId")), null, false) ); 
					if (UtilValidate.isEmpty(party)) {
						setValidate(false);
						message = "E405";
						validationMessage.put("receivingPartyId", message);
					}
				}
				
				if (UtilValidate.isEmpty(data.get("description"))) {
					setValidate(false);
					message = "E406";
					validationMessage.put("description", message);
				}
				
				if (UtilValidate.isEmpty(data.get("amountToTransfer"))) {
					setValidate(false);
					message = "E403";
					validationMessage.put("amountToTransfer", message);
				} 
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
