/**
 * 
 */
package org.groupfio.ewallet.app.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;

/**
 * @author Group Fio
 *
 */
public class DataHelper {
	
	private static String MODULE = DataHelper.class.getName();

	public static Map<String, Object> getLockboxStores(Delegator delegator) {
		
		Map<String, Object> lockboxStores = new HashMap<String, Object>();
		try {
			String defaultSalesChannelEnumId = "SPY_SALES_CHANNEL";
			List<GenericValue> asList = delegator.findAll("ProductStore", false);
			for(GenericValue productStore : asList){
				String storeName = "";
				if(UtilValidate.isNotEmpty(productStore)){
					//storeName = productStore.getString("storeName")+" ["+as.getString("productStoreId")+"]";
					storeName = productStore.getString("storeName");
				}
				
				lockboxStores.put(productStore.getString("productStoreId"), storeName);
				
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return lockboxStores;
	}
	
	public static Map getDropDownOptions(List<GenericValue> entityList, String keyField, String desField){
		Map<String, Object> options = new HashMap<String, Object>();
		for (GenericValue entity : entityList) {
			options.put(entity.getString(keyField), entity.getString(desField));
		}
		return options;
	}
	
	public static Map getSupplierList(Delegator delegator){
		
		Map<String, Object> suppliers = new HashMap<String, Object>();
		try {
			List<GenericValue> supplierList = delegator.findByAnd("PartyRole", UtilMisc.toMap("roleTypeId", "SUPPLIER"), null, false);
			for (GenericValue supplier : supplierList) {
				suppliers.put(supplier.getString("partyId"), PartyHelper.getPartyName(supplier) + " [" + supplier.getString("partyId") + "]");
			}
		} catch (GenericEntityException e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return suppliers;
		
	}
	
	public static Map getServiceList(Delegator delegator){
		
		Map<String, Object> services = new HashMap<String, Object>();
		try {
			List<GenericValue> serviceList = delegator.findByAnd("ServiceName", UtilMisc.toMap("componentName", "ewallet-app"), null, false);
			for (GenericValue service : serviceList) {
				services.put(service.getString("serviceName"), " [ " + service.getString("serviceName") + " ] - " + service.getString("description"));
			}
		} catch (GenericEntityException e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return services;
		
	}
	
	public static String getModelName(Delegator delegator, String modelId){
		try {
			
			GenericValue etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelId", modelId), null, false) );
			if (UtilValidate.isNotEmpty(etlModel)) {
				return etlModel.getString("modelName");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static String getGroupName(Delegator delegator, String groupId){
		try {
			
			GenericValue etlGroup = EntityUtil.getFirst( delegator.findByAnd("EtlGrouping", UtilMisc.toMap("groupId", groupId), null, false) );
			if (UtilValidate.isNotEmpty(etlGroup)) {
				return etlGroup.getString("groupName");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static String getServiceDescription(Delegator delegator, String serviceName, String componentName){
		try {
			
			if (UtilValidate.isEmpty(serviceName) || UtilValidate.isEmpty(componentName)) {
				return null;
			}
			
			GenericValue service = EntityUtil.getFirst( delegator.findByAnd("ServiceName", UtilMisc.toMap("serviceName", serviceName, "componentName", componentName), null, false) );
			if (UtilValidate.isNotEmpty(service)) {
				return service.getString("description");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static Map getWalletModelList(Delegator delegator, String modelType){
		
		Map<String, Object> modelList = new HashMap<String, Object>();
		try {
			
			EntityCondition conditions = null;
			
			if (UtilValidate.isNotEmpty(modelType)) {
				conditions = EntityCondition.makeCondition("tableName", EntityOperator.EQUALS, modelType);
			} else {
				conditions = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("tableName", EntityOperator.EQUALS, "DataImportWallet")
				), EntityOperator.OR);
			}
			
			List<GenericValue> etlSourceTable = delegator.findList("EtlSourceTable", conditions, null, null, null, false);
			if(UtilValidate.isNotEmpty(etlSourceTable)){
				List<String> listNameList = EntityUtil.getFieldListFromEntityList(etlSourceTable, "listName", true);
				
				if (UtilValidate.isNotEmpty(listNameList)) {
					for (String modelName : listNameList) {
						GenericValue etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelName", modelName), null, false) );
						if (UtilValidate.isNotEmpty(etlModel) && UtilValidate.isEmpty(etlModel.getString("isExport"))) {
							modelList.put(etlModel.getString("modelId"), modelName);
						}
					}
				}
				
			}
		} catch (GenericEntityException e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return modelList;
		
	}
	
	public static Map<String, Object> getActiveMasterWalletAccounts (Delegator delegator) {
		Map<String, Object> wallets = new HashMap<String, Object>();
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, "MASTER_ACCT_OWNER"),
					
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			
			List<GenericValue> billingAccountRoles = delegator.findList("BillingAccountRole", conditions, null, null, null, false);
			
			for (GenericValue role : billingAccountRoles) {
				GenericValue walletAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", role.getString("billingAccountId")), false);
				wallets.put(role.getString("billingAccountId"), walletAccount.getString("name") + " [ " + walletAccount.getString("externalAccountId") + " ]");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return wallets;
	}
	
	public static Map<String, Object> getActiveOperatorWalletAccounts (Delegator delegator, String masterBillingAccountId) {
		Map<String, Object> wallets = new HashMap<String, Object>();
		try {
			if (UtilValidate.isNotEmpty(masterBillingAccountId)) {
				List<GenericValue> operatorWalletList = WalletUtil.getAllActiveOperatorWalletAccounts(delegator, masterBillingAccountId);
				for (GenericValue operatorWallet : operatorWalletList) {
					wallets.put(operatorWallet.getString("billingAccountId"), operatorWallet.getString("name") + " [ " + operatorWallet.getString("externalAccountId") + " ]");
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return wallets;
	}
	
}
