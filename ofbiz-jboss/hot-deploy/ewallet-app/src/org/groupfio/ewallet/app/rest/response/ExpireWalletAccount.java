/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;

import org.groupfio.ewallet.app.model.WalletAccount;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class ExpireWalletAccount extends Response {

	private String billingAccountId;
	private String walletAcctId;
	
	private WalletAccount walletAccount;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		if (UtilValidate.isNotEmpty(context.get("walletAccount"))) {
			
			GenericValue billingAccount = (GenericValue) context.get("walletAccount");
			
			this.walletAccount = new WalletAccount(billingAccount);
			
			setBillingAccountId( billingAccount.getString("billingAccountId") );
			setWalletAcctId( billingAccount.getString("externalAccountId") );
		}
		
	}

	public WalletAccount getWalletAccount() {
		return walletAccount;
	}

	public void setWalletAccount(WalletAccount walletAccount) {
		this.walletAccount = walletAccount;
	}

	public String getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(String billingAccountId) {
		this.billingAccountId = billingAccountId;
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}
	
}
