package org.groupfio.ewallet.app.validator;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class GetWalletAPIAnalysisServiceDataValidator implements Validator {

	private static String MODULE = GetWalletAPIAnalysisServiceDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			GenericValue party = null;
			
			if (UtilValidate.isNotEmpty(data.get("serviceName"))) {
				GenericValue service = EntityUtil.getFirst( delegator.findByAnd("ServiceName", UtilMisc.toMap("serviceName", data.get("serviceName")), null, false) );
				if (UtilValidate.isEmpty(service)) {
					setValidate(false);
					message = "E901";
					validationMessage.put("serviceName", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("fromDate"))) {
				setValidate(false);
				message = "E921";
				validationMessage.put("fromDate", message);
			} else {
				try {
					Timestamp fromDate = UtilDateTime.toTimestamp(data.get("fromDate").toString());
					if (UtilValidate.isEmpty(fromDate)) {
						setValidate(false);
						message = "E921";
						validationMessage.put("fromDate", message);
					}
				} catch (Exception e) {
					Debug.logError(e, MODULE);
					setValidate(false);
					message = "E921";
					validationMessage.put("fromDate", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("thruDate"))) {
				setValidate(false);
				message = "E922";
				validationMessage.put("thruDate", message);
			} else {
				try {
					Timestamp fromDate = UtilDateTime.toTimestamp(data.get("thruDate").toString());
					if (UtilValidate.isEmpty(fromDate)) {
						setValidate(false);
						message = "E922";
						validationMessage.put("thruDate", message);
					}
				} catch (Exception e) {
					Debug.logError(e, MODULE);
					setValidate(false);
					message = "E922";
					validationMessage.put("thruDate", message);
				}
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "GetWalletAPIAnalysisService Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "GetWalletAPIAnalysisService Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
