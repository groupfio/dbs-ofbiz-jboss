/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.groupfio.ewallet.app.model.WalletAccount;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class GetWalletAccount extends Response {

	private BigDecimal totalAvailableBalance;
	
	private WalletAccount walletAccount;
	
	private List<WalletAccount> operators = new ArrayList<WalletAccount>();
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		if (UtilValidate.isNotEmpty(context.get("walletAccount"))) {
			
			GenericValue billingAccount = (GenericValue) context.get("walletAccount");
			
			this.walletAccount = new WalletAccount(billingAccount);
			this.walletAccount.setTotalAvailableBalance( new BigDecimal(context.get("totalAvailableBalance").toString()).doubleValue() );
		}
		BigDecimal totalAvailableBalanceAllOperators = BigDecimal.ZERO;
		if (UtilValidate.isNotEmpty(context.get("operatorWalletAccounts"))) {
			List<GenericValue> billingAccounts = (List<GenericValue>) context.get("operatorWalletAccounts");
			for (GenericValue billingAccount : billingAccounts) {
				operators.add(new WalletAccount(billingAccount));
				BigDecimal operatorEarmarkAmount = UtilValidate.isNotEmpty(billingAccount.getBigDecimal("earmarkAmount")) ? billingAccount.getBigDecimal("earmarkAmount") : new BigDecimal(0);
				totalAvailableBalanceAllOperators = totalAvailableBalanceAllOperators.add( billingAccount.getBigDecimal("accountBalanceAmount") ).add(operatorEarmarkAmount);
			}
			setTotalAvailableBalance(totalAvailableBalanceAllOperators.add((BigDecimal) context.get("totalAvailableBalance")));
		} else {
			setTotalAvailableBalance((BigDecimal) context.get("totalAvailableBalance"));
		}
	}

	public WalletAccount getWalletAccount() {
		return walletAccount;
	}

	public void setWalletAccount(WalletAccount walletAccount) {
		this.walletAccount = walletAccount;
	}

	public List<WalletAccount> getOperators() {
		return operators;
	}

	public void setOperators(List<WalletAccount> operators) {
		this.operators = operators;
	}

	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}
	
}
