package org.groupfio.ewallet.app.validator;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class RegisterWalletApplicationDataValidator implements Validator {

	private static String MODULE = RegisterWalletApplicationDataValidator.class.getName();
	
	private boolean validate;
	
	/* (non-Javadoc)
	 * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
	 */
	@Override
	public Map<String, Object> validate(Map<String, Object> context) {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> data = (Map<String, Object>) context.get("data");
		Map<String, Object> validationMessage = new HashMap<String, Object>();
		
		try {
			
			setValidate(true);
			
			Delegator delegator = (Delegator) context.get("delegator");
			response.put("delegator", delegator);
			
			//String modelName = ParamUtil.getString(context, "modelName");
			
			String message = null;
			
			if (UtilValidate.isEmpty(data.get("clientName"))) {
				setValidate(false);
				message = "E803";
				validationMessage.put("clientName", message);
			} else {
				GenericValue appRegistery = EntityUtil.getFirst( delegator.findByAnd("ClientApplicationRegistry", UtilMisc.toMap("clientName", data.get("clientName")), null, false) );
				if (UtilValidate.isNotEmpty(appRegistery)) {
					setValidate(false);
					message = "E821";
					validationMessage.put("clientName", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("clientRequestDomainName"))) {
				setValidate(false);
				message = "E801";
				validationMessage.put("clientRequestDomainName", message);
			} else {
				GenericValue appRegistery = EntityUtil.getFirst( delegator.findByAnd("ClientApplicationRegistry", UtilMisc.toMap("clientDomain", data.get("clientRequestDomainName")), null, false) );
				if (UtilValidate.isNotEmpty(appRegistery)) {
					setValidate(false);
					message = "E822";
					validationMessage.put("clientRequestDomainName", message);
				}
			}
			
			if (UtilValidate.isEmpty(data.get("externalReferenceId"))) {
				setValidate(false);
				message = "E823";
				validationMessage.put("externalReferenceId", message);
			} else {
				GenericValue appRegistery = EntityUtil.getFirst( delegator.findByAnd("ClientApplicationRegistry", UtilMisc.toMap("externalReferenceId", data.get("externalReferenceId")), null, false) );
				if (UtilValidate.isNotEmpty(appRegistery)) {
					setValidate(false);
					message = "E824";
					validationMessage.put("externalReferenceId", message);
				}
			}
			
			if (!isValidate()) {
				
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
				response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
				
			} else {
				response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Customer Data Validation Failed...!");
			
			return response;
		}
		
		response.put("data", data);
		response.put("validationMessage", validationMessage);
		
		return response;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
