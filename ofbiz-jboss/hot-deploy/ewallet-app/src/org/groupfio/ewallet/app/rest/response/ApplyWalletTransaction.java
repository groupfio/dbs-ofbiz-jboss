/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.math.BigDecimal;
import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class ApplyWalletTransaction extends Response {

	private String walletAcctId;
	private String masterWalletAcctId;
	private String walletTxnId;
	private String walletTxnExternalId;
	private BigDecimal availableBalance;
	private BigDecimal transAmount; 

	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setWalletAcctId( ParamUtil.getString(context, "walletAcctId") );
		setMasterWalletAcctId( ParamUtil.getString(context, "masterWalletAcctId") );
		setWalletTxnId( ParamUtil.getString(context, "paymentId") );
		setWalletTxnExternalId( ParamUtil.getString(context, "walletTxnExternalId") );
		setAvailableBalance( ParamUtil.getBigDecimal(context, "accountBalanceAmount") );
		setTransAmount( ParamUtil.getBigDecimal(context, "transAmount") );
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}
	
	public String getMasterWalletAcctId() {
		return masterWalletAcctId;
	}

	public void setMasterWalletAcctId(String masterWalletAcctId) {
		this.masterWalletAcctId = masterWalletAcctId;
	}

	public String getWalletTxnId() {
		return walletTxnId;
	}

	public void setWalletTxnId(String walletTxnId) {
		this.walletTxnId = walletTxnId;
	}

	public String getWalletTxnExternalId() {
		return walletTxnExternalId;
	}

	public void setWalletTxnExternalId(String walletTxnExternalId) {
		this.walletTxnExternalId = walletTxnExternalId;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}
	
}
