/**
 * 
 */
package org.groupfio.ewallet.app.writer;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class LogWriter implements Writer {

	private static String MODULE = LogWriter.class.getName();
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.writer.Writer#write(java.util.Map)
	 */
	@Override
	public Map<String, Object> write(Map<String, Object> context) {
		
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			
			Delegator delegator = (Delegator) context.get("delegator");
			
			String serviceName = ParamUtil.getString(context, "serviceName");
			String clientLogRefId = ParamUtil.getString(context, "clientLogRefId");
			String requestedData = ParamUtil.getString(context, "requestedData");
			String responsedData = ParamUtil.getString(context, "responsedData");
			String responseCode = ParamUtil.getString(context, "responseCode");
			String clientRegistryId = ParamUtil.getString(context, "clientRegistryId");
            String msgId = ParamUtil.getString(context, "msgId");
            String orgId = ParamUtil.getString(context, "orgId");
			String responseStatus = ParamUtil.getString(context, "responseStatus");
			
			Timestamp requestedTime = (Timestamp) context.get("requestedTime");
			Timestamp responsedTime = (Timestamp) context.get("responsedTime");
			
			if (UtilValidate.isEmpty(responseStatus)) {
				responseStatus = "ERROR";
			}
			
			GenericValue apiLog = delegator.makeValue("WalletApiLog");
			
			String walletApiLogId = delegator.getNextSeqId("WalletApiLog");
			
			apiLog.put("walletApiLogId", walletApiLogId);
			
			apiLog.put("serviceName", serviceName);
			apiLog.put("clientLogRefId", clientLogRefId);
			apiLog.put("requestedData", requestedData);
			apiLog.put("responsedData", responsedData);
			apiLog.put("responseCode", responseCode);
			apiLog.put("responseStatus", responseStatus);
			apiLog.put("clientRegistryId", clientRegistryId);
			apiLog.put("requestedTime", requestedTime);
			apiLog.put("responsedTime", responsedTime);
            apiLog.put("msgId", msgId);
            apiLog.put("orgId", orgId);
			
			apiLog.create();
			
			response.put("apiLog", apiLog);
			response.put("walletApiLogId", walletApiLogId);
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(EMConstants.RESPONSE_MESSAGE, "Log write Failed...!");
			
			return response;
		}
		
		response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		
		return response;
	}

}
