package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class TxnEnqSuccessResponse extends TxnEnqResponse {
    private String txnStatus;
    private String txnRejectCode;
    private String txnStatusDescription;
    private String txnCcy;
    private String txnAmount;
    private String txnDate;
    private Party senderParty;
    private Party receivingParty;

    public TxnEnqSuccessResponse() {}

    public TxnEnqSuccessResponse(String enqStatus, String txnStatus, String txnRejectCode, String txnStatusDescription, String txnCcy,
            String txnAmount, String txnDate, Party senderParty, Party receivingParty) {
        this.enqStatus = enqStatus;
        this.txnStatus = txnStatus;
        this.txnRejectCode = txnRejectCode;
        this.txnStatusDescription = txnStatusDescription;
        this.txnCcy = txnCcy;
        this.txnAmount = txnAmount;
        this.txnDate = txnDate;
        this.senderParty = senderParty;
        this.receivingParty = receivingParty;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnRejectCode() {
        return txnRejectCode;
    }

    public void setTxnRejectCode(String txnRejectCode) {
        this.txnRejectCode = txnRejectCode;
    }

    public String getTxnStatusDescription() {
        return txnStatusDescription;
    }

    public void setTxnStatusDescription(String txnStatusDescription) {
        this.txnStatusDescription = txnStatusDescription;
    }

    public String getTxnCcy() {
        return txnCcy;
    }

    public void setTxnCcy(String txnCcy) {
        this.txnCcy = txnCcy;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public Party getSenderParty() {
        return senderParty;
    }

    public void setSenderParty(Party senderParty) {
        this.senderParty = senderParty;
    }

    public Party getReceivingParty() {
        return receivingParty;
    }

    public void setReceivingParty(Party receivingParty) {
        this.receivingParty = receivingParty;
    }
}