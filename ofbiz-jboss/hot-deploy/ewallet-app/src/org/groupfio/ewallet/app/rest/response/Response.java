/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;
import java.util.StringTokenizer;

import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public abstract class Response {

	protected String responseCode;
	protected String responseCodeDesc;
	private String responseRefId;
	
	protected abstract void doBuild(Map<String, Object> context) throws Exception;
	
	public void build(Map<String, Object> context) throws Exception {
		doBuild(context);
	}
	
	public void prepareContext(Map<String, Object> context) throws Exception {
		setResponseCode( ParamUtil.getString(context, "responseCode") );
		setResponseRefId( ParamUtil.getString(context, "responseRefId") );
		
		Map<String, Object> validationMessage = (Map<String, Object>) context.get("validationMessage");
		if (UtilValidate.isNotEmpty(validationMessage)) {
			responseCode = "";
			for (String prop : validationMessage.keySet()) {
				responseCode += validationMessage.get(prop) + "| ";
			}
			responseCode = responseCode.substring(0, responseCode.length() - 2);
		}
		
		if (UtilValidate.isNotEmpty(getResponseCode()) && UtilValidate.isNotEmpty(context.get("delegator"))) {
			Delegator delegator = (Delegator) context.get("delegator");
			responseCodeDesc = "";
			
		if(responseCode != null){
			StringTokenizer st = new StringTokenizer(responseCode, "|");
			while(st.hasMoreTokens()) {
				String code = st.nextToken().trim();
				GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", code), null, false) );
				if (UtilValidate.isNotEmpty(errorCode) && UtilValidate.isNotEmpty(errorCode.getString("solutionDescription"))) {
					responseCodeDesc += errorCode.getString("solutionDescription") + "| ";
				} else {
					responseCodeDesc += "| ";
				}
			}
			if (responseCodeDesc.length() > 2) {
				responseCodeDesc = responseCodeDesc.substring(0, responseCodeDesc.length() - 2);
			}
		}
		}
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseRefId() {
		return responseRefId;
	}

	public void setResponseRefId(String responseRefId) {
		this.responseRefId = responseRefId;
	}

	public String getResponseCodeDesc() {
		return responseCodeDesc;
	}

	public void setResponseCodeDesc(String responseCodeDesc) {
		this.responseCodeDesc = responseCodeDesc;
	}
	
}
