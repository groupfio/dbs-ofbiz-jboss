package org.groupfio.ewallet.app.rest.response;
import java.util.Map;

import org.ofbiz.base.util.UtilValidate;

/**
 * @author Group Fio
 *
 */
public class FundTransfer {
    private TxnResponse txnResponse;
    private Header header;

    public void doBuild(Map<String, Object> context) throws Exception {
        //Setting Header class values
        String msgId = UtilValidate.isNotEmpty(context.get("msgId")) ? (String) context.get("msgId") : "";
        String orgId = UtilValidate.isNotEmpty(context.get("orgId")) ? (String) context.get("orgId") : "";
        String apiKey = UtilValidate.isNotEmpty(context.get("apiKey")) ? (String) context.get("apiKey") : "";
        String timestamp = UtilValidate.isNotEmpty(context.get("timeStamp")) ? (String) context.get("timeStamp") : "";
        this.header = new Header(msgId, orgId, apiKey, timestamp);

        //Setting TxnResponse class values
        String walletTxnId = UtilValidate.isNotEmpty(context.get("walletTxnId")) ? (String) context.get("walletTxnId") : "";
        String txnRefId = UtilValidate.isNotEmpty(context.get("txnRefId")) ? (String) context.get("txnRefId") : "";
        String txnStatus = UtilValidate.isNotEmpty(context.get("txnStatus")) ? (String) context.get("txnStatus") : "";
        String txnRejectCode = UtilValidate.isNotEmpty(context.get("txnRejectCode")) ? (String) context.get("txnRejectCode") : "";
        String txnStatusDescription = UtilValidate.isNotEmpty(context.get("txnStatusDescription")) ? (String) context.get("txnStatusDescription") : "";
        String txnSettlementAmt = UtilValidate.isNotEmpty(context.get("txnSettlementAmt")) ? context.get("txnSettlementAmt").toString() : "";
        String txnSettlementDt = UtilValidate.isNotEmpty(context.get("txnSettlementAmt")) ? context.get("txnSettlementDt").toString() : "";
        this.txnResponse = new TxnResponse(walletTxnId, txnRefId, txnStatus, txnRejectCode, txnStatusDescription, txnSettlementAmt , txnSettlementDt);
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public TxnResponse getTxnResponse() {
        return txnResponse;
    }

    public void setTxnResponse(TxnResponse txnEnqResponse) {
        this.txnResponse = txnEnqResponse;
    }
}