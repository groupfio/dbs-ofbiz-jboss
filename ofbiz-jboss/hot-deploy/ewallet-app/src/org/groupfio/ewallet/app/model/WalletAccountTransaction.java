/**
 * 
 */
package org.groupfio.ewallet.app.model;

import java.sql.Timestamp;
import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue;

import com.ibm.icu.math.BigDecimal;

/**
 * @author Group Fio
 *
 */
public class WalletAccountTransaction {

	private String walletAcctIdFrom;
	private String walletAcctIdTo;
	private String walletTxnId;
	private String walletTransDate;
	private String senderPartyId;
	private String receivingPartyId;
	private String walletCurrency;
	private String transactionTypeId;
	private String operationType;
	private Double amount;
	private Double availableBalance;
	
	public WalletAccountTransaction() {}
	
	public WalletAccountTransaction(Map<String, Object> entry) {
		
		if (UtilValidate.isEmpty(entry)) {
			return;
		}
				
		this.walletAcctIdFrom = ParamUtil.getString(entry, "walletAcctIdFrom");
		this.walletAcctIdTo = ParamUtil.getString(entry, "walletAcctIdTo");
		this.walletTxnId = ParamUtil.getString(entry, "walletTxnId");
		this.walletTransDate = ParamUtil.getString(entry, "walletTransDate");
		this.senderPartyId = ParamUtil.getString(entry, "senderPartyId");
		this.receivingPartyId = ParamUtil.getString(entry, "receivingPartyId");
		this.walletCurrency = ParamUtil.getString(entry, "walletCurrency");
		this.transactionTypeId = ParamUtil.getString(entry, "transactionTypeId");
		this.operationType = ParamUtil.getString(entry, "operationType");
		this.amount = UtilValidate.isNotEmpty(entry.get("amount")) ? new BigDecimal( entry.get("amount").toString() ).doubleValue() : null;
		this.availableBalance = UtilValidate.isNotEmpty(entry.get("availableBalance")) ? new BigDecimal( entry.get("availableBalance").toString() ).doubleValue() : null;
		
	}

	public String getWalletAcctIdFrom() {
		return walletAcctIdFrom;
	}

	public void setWalletAcctIdFrom(String walletAcctIdFrom) {
		this.walletAcctIdFrom = walletAcctIdFrom;
	}

	public String getWalletAcctIdTo() {
		return walletAcctIdTo;
	}

	public void setWalletAcctIdTo(String walletAcctIdTo) {
		this.walletAcctIdTo = walletAcctIdTo;
	}

	public String getWalletTxnId() {
		return walletTxnId;
	}

	public void setWalletTxnId(String walletTxnId) {
		this.walletTxnId = walletTxnId;
	}

	public String getWalletTransDate() {
		return walletTransDate;
	}

	public void setWalletTransDate(String walletTransDate) {
		this.walletTransDate = walletTransDate;
	}

	public String getSenderPartyId() {
		return senderPartyId;
	}

	public void setSenderPartyId(String senderPartyId) {
		this.senderPartyId = senderPartyId;
	}

	public String getReceivingPartyId() {
		return receivingPartyId;
	}

	public void setReceivingPartyId(String receivingPartyId) {
		this.receivingPartyId = receivingPartyId;
	}

	public String getWalletCurrency() {
		return walletCurrency;
	}

	public void setWalletCurrency(String walletCurrency) {
		this.walletCurrency = walletCurrency;
	}

	public String getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(String transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}
	
}
