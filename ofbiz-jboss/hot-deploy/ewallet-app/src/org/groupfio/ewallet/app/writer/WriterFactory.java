/**
 * 
 */
package org.groupfio.ewallet.app.writer;

/**
 * @author Group Fio
 *
 */
public final class WriterFactory {

	private static final LogWriter LOG_WRITER = new LogWriter();
	
	public static LogWriter getLogWriter () {
		return LOG_WRITER;
	}
	
}
