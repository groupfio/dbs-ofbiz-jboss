/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.sql.Timestamp;
import java.util.Map;
import org.ofbiz.base.util.Debug;

/**
 * @author Group Fio
 *
 */
public class Header {
    private String msgId;
    private String orgId;
    private String apiKey;
    private String timeStamp;

    public Header() {}

    public Header(String msgId, String orgId, String apiKey, String timeStamp) {
        Debug.log("==========ResponseHeader.java 28===========");
        this.msgId = msgId;
        this.orgId = orgId;
        this.apiKey = apiKey;
        this.timeStamp = timeStamp;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}