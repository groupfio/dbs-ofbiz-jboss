/**
 * 
 */
package org.groupfio.ewallet.app.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.etl.process.EtlConstants;
import org.groupfio.etl.process.service.CommonImportService;
import org.groupfio.etl.process.service.ServiceFactory;
import org.groupfio.etl.process.util.CommonUtil;
import org.groupfio.ewallet.app.util.DataUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.TransactionUtil;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.util.EntityUtilProperties;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Group Fio
 *
 */
public class WalletServiceImpl {

    private static final String MODULE = WalletServiceImpl.class.getName();

    public static Map createWalletAccount(DispatchContext dctx, Map context) {
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String externalAccountId = (String) context.get("externalAccountId");
        String partyId = (String) context.get("partyId");
        String name = (String) context.get("name");
        String accountCurrencyUomId = (String) context.get("accountCurrencyUomId");
        String description = (String) context.get("description");
        BigDecimal accountLimit = (BigDecimal) context.get("accountLimit");
        Timestamp fromDate = (Timestamp) context.get("fromDate");
        Timestamp thruDate = (Timestamp) context.get("thruDate");
        String orgId = (String) context.get("orgId");
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
            Map<String, Object> baContext = new HashMap<String, Object>();
            baContext.put("externalAccountId", externalAccountId);
            baContext.put("partyId", partyId);
            baContext.put("accountCurrencyUomId", accountCurrencyUomId);
            baContext.put("accountLimit", accountLimit);
            baContext.put("description", description);
            baContext.put("fromDate", fromDate);
            baContext.put("thruDate", thruDate);
            baContext.put("roleTypeId", party.getString("roleTypeId"));
            baContext.put("userLogin", userLogin);


            //M.Vijayakumar @05-03-2018 desc: billing account updation process
            Map<String, Object> baRes = null;
            List<GenericValue> billingAccountList = delegator.findByAnd("BillingAccount", UtilMisc.toMap("externalAccountId",externalAccountId), null, false);
            if(UtilValidate.isNotEmpty(billingAccountList))
            {
            	billingAccountList = EntityUtil.filterByDate(billingAccountList);
            	if(UtilValidate.isNotEmpty(billingAccountList))
            	{
            		GenericValue billingAccount = EntityUtil.getFirst(billingAccountList);
            		baContext.put("billingAccountId",billingAccount.getString("billingAccountId"));
            		baRes = dispatcher.runSync("updateBillingAccount", baContext);
            		baRes.put("billingAccountId",billingAccount.getString("billingAccountId"));
            	}
            }else
            {
            	  	baRes = dispatcher.runSync("createBillingAccountAndRole", baContext);
            }
            
            
            
            if (ServiceUtil.isSuccess(baRes)) {
                String billingAccountId = (String) baRes.get("billingAccountId");
                GenericValue billingAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", baRes.get("billingAccountId")), false);
                billingAccount.put("name", name);
                billingAccount.put("accountBalanceAmount", new BigDecimal(0));
                billingAccount.put("billingAccountTypeId", "WALLET");
                billingAccount.put("earmarkAmount", new BigDecimal(0));
                billingAccount.put("billingAccountStatusId", "WALLET_NEW");
                billingAccount.put("lastModifiedByUserLoginId", userLogin.getString("userLoginId"));
                billingAccount.store();

                
                //@vijayakumar desc: we need to update party group information also because account name is relatted with partyName
                if(UtilValidate.isNotEmpty(billingAccount))
                {
                	List<GenericValue> billingAccountRole = delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId", baRes.get("billingAccountId")), null, false);
                	if(UtilValidate.isNotEmpty(billingAccountRole))
                	{
                		GenericValue billingAccountRle = EntityUtil.getFirst(billingAccountRole);
                		if(UtilValidate.isNotEmpty(billingAccountRle))
                		{
                			GenericValue partyGroup = delegator.findOne("PartyGroup", false, UtilMisc.toMap("partyId",billingAccountRle.getString("partyId")));
                			if(UtilValidate.isNotEmpty(partyGroup))
                			{
                				partyGroup.set("groupName", name);
                				partyGroup.store();
                			}
                		}
                	}
                }
                	
                //end @vijayakumar
                GenericValue billingAccountStatusHistory = delegator.makeValue("BillingAccountStatusHistory");
                billingAccountStatusHistory.put("billingAccountStatusHistoryId", delegator.getNextSeqId("BillingAccountStatusHistory"));
                billingAccountStatusHistory.put("billingAccountStatusId", "WALLET_NEW");
                billingAccountStatusHistory.put("billingAccountId", billingAccountId);
                billingAccountStatusHistory.put("statusDatetime", UtilDateTime.nowTimestamp());
                if (userLogin != null) {
                    billingAccountStatusHistory.put("statusUserLogin", userLogin.getString("userLoginId"));
                }
                billingAccountStatusHistory.create();

                if ("OPERATING_ACCT_OWNER".equals(party.getString("roleTypeId"))) {
                    GenericValue billingAccountRole = EntityQuery.use(delegator).from("BillingAccountRole").where("billingAccountId", baRes.get("billingAccountId"), "partyId", partyId, "roleTypeId", "OPERATING_ACCT_OWNER").filterByDate().queryFirst();
                    if (UtilValidate.isNotEmpty(billingAccountRole)) {
                        GenericValue partyRelationship = EntityQuery.use(delegator).from("PartyRelationship").where("partyIdTo", party.getString("partyId"), "roleTypeIdTo", "OPERATING_ACCT_OWNER", "partyRelationshipTypeId", "WALLET_RELATION").filterByDate().queryFirst();
                        if (UtilValidate.isNotEmpty(partyRelationship)) {
                            List<GenericValue> parentBillingAccountRoles = EntityQuery.use(delegator).from("BillingAccountRole").where("partyId", partyRelationship.getString("partyIdFrom"), "roleTypeId", "MASTER_ACCT_OWNER").filterByDate().queryList();
                            for (GenericValue parentBillingAccountRole : parentBillingAccountRoles) {
                                GenericValue parentBillingAccount = EntityQuery.use(delegator).from("BillingAccount").where("billingAccountId", parentBillingAccountRole.getString("billingAccountId"), "accountCurrencyUomId", accountCurrencyUomId).filterByDate().queryOne();
                                if (parentBillingAccount != null) {
                                    billingAccountRole.put("parentPartyId", partyRelationship.getString("partyIdFrom"));
                                    billingAccountRole.put("parentBillingAccountId", parentBillingAccountRole.getString("billingAccountId"));
                                    break;
                                }
                            }
                        }
                        billingAccountRole.store();
                    }
                }
                if (orgId != null && UtilValidate.isNotEmpty(orgId)) {
                    GenericValue partyAttribute = delegator.makeValue("PartyAttribute");
                    partyAttribute.put("partyId", partyId);
                    partyAttribute.put("attrName", "ORG_ID");
                    partyAttribute.put("attrValue", orgId);
                    partyAttribute.create();
                }
                result.put("billingAccountId", baRes.get("billingAccountId"));
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            result.putAll(ServiceUtil.returnError(e.getMessage()));
            return result;
        }
        result.putAll(ServiceUtil.returnSuccess("Successfully created wallet account.."));
        return result;
    }

    public static Map updateWalletAccount(DispatchContext dctx, Map context) {
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Delegator delegator = (Delegator) dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String externalAccountId = (String) context.get("externalAccountId");
        String partyId = (String) context.get("partyId");
        String name = (String) context.get("name");
        String accountCurrencyUomId = (String) context.get("accountCurrencyUomId");
        String description = (String) context.get("description");
        BigDecimal accountLimit = (BigDecimal) context.get("accountLimit");
        String freezeTypeId = (String) context.get("freezeTypeId");
        String operationTypeId = (String) context.get("operationTypeId");
        Timestamp fromDate = (Timestamp) context.get("fromDate");
        Timestamp thruDate = (Timestamp) context.get("thruDate");
        String billingAccountStatusId = (String) context.get("billingAccountStatusId");
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
            GenericValue billingAccount = EntityUtil.getFirst( delegator.findByAnd("BillingAccount", UtilMisc.toMap("externalAccountId", externalAccountId), null, false) );
            if (billingAccount != null) {
                if (billingAccountStatusId == null) {
                    billingAccountStatusId = "WALLET_PENDING";
                }

                BigDecimal balanceAmount = billingAccount.getBigDecimal("accountBalanceAmount");
                BigDecimal earmarkAmount = billingAccount.getBigDecimal("earmarkAmount");
                if (accountLimit == null) {
                    return ServiceUtil.returnError("Max Limit Allowed cannot be null.");
                } else {
                    if (balanceAmount == null) {
                        balanceAmount = BigDecimal.ZERO;
                    }
                    if (earmarkAmount == null) {
                        earmarkAmount = BigDecimal.ZERO;
                    }
                    BigDecimal totalAvailableBalance = balanceAmount.add(earmarkAmount);
                    if (accountLimit.compareTo(totalAvailableBalance) < 0) {
                        return ServiceUtil.returnError("Max Limit Allowed cannot be less than total available balance.");
                    }
                }
                billingAccount.put("accountCurrencyUomId", accountCurrencyUomId);
                billingAccount.put("accountLimit", accountLimit);
                billingAccount.put("description", description);
                billingAccount.put("name", name);
                billingAccount.put("freezeTypeId", freezeTypeId);
                billingAccount.put("operationTypeId", operationTypeId);
                billingAccount.put("fromDate", fromDate);
                billingAccount.put("thruDate", thruDate);
                billingAccount.put("billingAccountStatusId", billingAccountStatusId);
                billingAccount.put("lastModifiedByUserLoginId", userLogin.getString("userLoginId"));
                billingAccount.store();

                /*Get the latest status of the wallet.
                 * If it is 'Pending for Approval' and it is updated by same user login then no need to create new record just update its statusDateTime
                */
                boolean isExistingRecord = false;
                GenericValue billingAccountStatusHistory = EntityQuery.use(delegator).from("BillingAccountStatusHistory").where("billingAccountId", billingAccount.getString("billingAccountId")).orderBy("-statusDatetime").queryFirst();
                if (billingAccountStatusHistory != null) {
                    String statusHistoryUserLogin = billingAccountStatusHistory.getString("statusUserLogin");
                    if("WALLET_PENDING".equals(billingAccountStatusHistory.getString("billingAccountStatusId")) && userLogin.getString("userLoginId").equals(statusHistoryUserLogin)) {
                        billingAccountStatusHistory.set("statusDatetime", UtilDateTime.nowTimestamp());
                        billingAccountStatusHistory.store();
                        isExistingRecord = true;
                    }
                }
                if (!isExistingRecord) {
                    //Create the BillingAccountStatusHistory
                    billingAccountStatusHistory = delegator.makeValue("BillingAccountStatusHistory");
                    billingAccountStatusHistory.put("billingAccountStatusHistoryId", delegator.getNextSeqId("BillingAccountStatusHistory"));
                    billingAccountStatusHistory.put("billingAccountStatusId", billingAccountStatusId);
                    billingAccountStatusHistory.put("billingAccountId", billingAccount.getString("billingAccountId"));
                    billingAccountStatusHistory.put("statusDatetime", UtilDateTime.nowTimestamp());
                    if (userLogin != null) {
                        billingAccountStatusHistory.put("statusUserLogin", userLogin.getString("userLoginId"));
                    }
                    billingAccountStatusHistory.create();
                }

                GenericValue billingAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, billingAccount.getString("billingAccountId"));
                if(billingAccountRole != null && billingAccountRole.size()>0){
                    if(UtilValidate.isNotEmpty(billingAccountRole.getString("partyId"))){
                        GenericValue partyRoleGv = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId",partyId),false);
                        if(UtilValidate.isNotEmpty(partyRoleGv)){
                            partyRoleGv.put("groupName", name);
                            partyRoleGv.store();
                        }
                        // expire/freeze all the associate operating wallet for master wallet
                        if ("MASTER_ACCT_OWNER".equals(billingAccountRole.getString("roleTypeId")) && !"ACTIVE".equalsIgnoreCase(operationTypeId)) {
                            List<GenericValue> operators = WalletUtil.getAllActiveOperatorWalletAccounts(delegator, billingAccount.getString("billingAccountId"));
                            for (GenericValue operator : operators) {
                                if ("EXPIRED".equalsIgnoreCase(operationTypeId)) {
                                    operator.put("freezeTypeId", "NO_OPERATION");
                                    operator.put("thruDate", UtilDateTime.nowTimestamp());
                                    GenericValue operatorAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, operator.getString("billingAccountId"));
                                    if (UtilValidate.isNotEmpty(operatorAccountRole)) {
                                        operatorAccountRole.put("thruDate", UtilDateTime.nowTimestamp());
                                        operatorAccountRole.store();
                                    }
                                }
                                operator.put("operationTypeId", operationTypeId);
                                operator.store();
                            }
                        }
                    }
                }
                result.put("billingAccountId", billingAccount.get("billingAccountId"));
            }
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            result.putAll(ServiceUtil.returnError(e.getMessage()));
            return result;
        }
        result.putAll(ServiceUtil.returnSuccess("Successfully updated wallet account.."));
        return result;
    }

    public static Map getWalletAccount(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String externalAccountId = (String) context.get("externalAccountId");
    	String nodeType = (String) context.get("nodeType");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		BigDecimal totalAvailableBalance = new BigDecimal(0);
    		
    		GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
    		
    		if (walletAccount != null && walletAccount.size()>0) {
    			
    			result.put("walletAccount", walletAccount);
    			
    			totalAvailableBalance = TransactionUtil.getTotalAvailableBalance(delegator, walletAccount);
    			
    			if (UtilValidate.isNotEmpty(nodeType) && nodeType.equals("ALL_NODES")) {
        			List<GenericValue> operatorWalletAccounts = WalletUtil.getAllActiveOperatorWalletAccounts(delegator, walletAccount.getString("billingAccountId"));
        			result.put("operatorWalletAccounts", operatorWalletAccounts);
        		}
    			
    		}
    		
    		result.put("totalAvailableBalance", totalAvailableBalance);
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully get wallet account.."));
    	
    	return result;
    	
    }
    
    public static Map createOperatingWalletUser(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String externalAccountId = (String) context.get("externalAccountId");
    	String partyName = (String) context.get("partyName");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
    		
    		if (walletAccount != null && walletAccount.size()>0) {
    			
    			GenericValue billingAccountRole = EntityUtil.getFirst( delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId", walletAccount.get("billingAccountId"), "roleTypeId", "OPERATING_ACCT_OWNER"), null, false) );
    			if (UtilValidate.isEmpty(billingAccountRole)) {
    				
    				result.put("responseCode", "E701");
    				result.putAll( ServiceUtil.returnError("This not Operating Account Owner....") );
    				
    				return result;
    			}
    			
    			Map<String, Object> pgContext = new HashMap<String, Object>();
        		
        		pgContext.put("groupName", partyName);
        		pgContext.put("comments", walletAccount.getString("description"));
        		pgContext.put("preferredCurrencyUomId", walletAccount.getString("accountCurrencyUomId"));
        		pgContext.put("statusId", "WALLET_ACTIVE");
        		
        		pgContext.put("userLogin", userLogin);
        		
        		Map<String, Object> pgRes = dispatcher.runSync("createPartyGroup", pgContext);
        		
        		if (ServiceUtil.isSuccess(pgRes)) {
        			
        			String partyId = ParamUtil.getString(pgRes, "partyId");
        			
        			GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
        			party.put("roleTypeId", "WALLET_USER_ROLE");
        			party.store();
        			
        			//GenericValue billingAccountRole = EntityUtil.getFirst( delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId", walletAccount.get("billingAccountId"), "roleTypeId", "OPERATING_ACCT_OWNER"), null, false) );
    				if (UtilValidate.isNotEmpty(billingAccountRole)) {
    					
    					String operatorPartyId = billingAccountRole.getString("partyId");
    					if (UtilValidate.isNotEmpty(operatorPartyId)) {
    	    				Map<String, Object> prContext = new HashMap<String, Object>();
    	        			
    	        			prContext.put("partyIdFrom", operatorPartyId);
    	        			prContext.put("partyIdTo", partyId);
    	        			prContext.put("partyRelationshipTypeId", "WALLET_RELATION");
    	        			
    	        			prContext.put("roleTypeIdFrom", "OPERATING_ACCT_OWNER");
    	        			prContext.put("roleTypeIdTo", "WALLET_USER_ROLE");
    	        			
    	        			prContext.put("userLogin", userLogin);
    	        			
    	        			Map<String, Object> prRes = dispatcher.runSync("createPartyRelationshipAndRole", prContext);
    	        			
    	        			if (ServiceUtil.isSuccess(prRes)) {
    	        				if(UtilValidate.isNotEmpty(partyId))
    	        				Debug.logInfo("Create party relationship for partyIdFrom#"+operatorPartyId+", partyIdTo#"+partyId, MODULE);
    	        			}
    	        			
    	    			}
    					
    				}
        			
    				result.put("userPartyId", partyId);
    				result.put("partyStatus", "ACTIVE");
        		}
    			
    		}
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
    		result.put("responseCode", "E900");
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created operating wallet user.."));
    	
    	return result;
    	
    }
    
    public static Map updateOperatingWalletUser(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String externalAccountId = (String) context.get("externalAccountId");
    	String partyName = (String) context.get("partyName");
    	String userPartyId = (String) context.get("userPartyId");
    	String partyStatus = (String) context.get("partyStatus");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
    		
    		if (walletAccount != null && walletAccount.size()>0) {
    			
    			Map<String, Object> pgContext = new HashMap<String, Object>();
        		
    			pgContext.put("partyId", userPartyId);
        		pgContext.put("groupName", partyName);
        		
        		String description = walletAccount.getString("description");
        		if(UtilValidate.isNotEmpty(description))
        		pgContext.put("comments", description);
        		
        		String accountCurrencyUomId = walletAccount.getString("accountCurrencyUomId");
        		if(UtilValidate.isNotEmpty(accountCurrencyUomId))
        		pgContext.put("preferredCurrencyUomId", accountCurrencyUomId);
        		//pgContext.put("statusId", DataUtil.getStatusId(delegator, partyStatus));
        		
        		pgContext.put("userLogin", userLogin);
        		
        		Map<String, Object> pgRes = dispatcher.runSync("updatePartyGroup", pgContext);
        		
        		if (ServiceUtil.isSuccess(pgRes)) {
        			
        			String partyId = userPartyId;
        			
        			GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", partyId), false);
        			if (UtilValidate.isNotEmpty(partyStatus)) {
        				party.put("statusId", DataUtil.getStatusId(delegator, partyStatus));
        			}
        			party.store();
        			
    				result.put("userPartyId", partyId);
    				result.put("partyStatus", partyStatus);
        		}
    			
    		}
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated operating wallet user.."));
    	
    	return result;
    	
    }
    
    public static Map expireWalletAccount(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String externalAccountId = (String) context.get("externalAccountId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue walletAccount = WalletUtil.getActiveWalletAccount(delegator, externalAccountId);
    		
    		if (walletAccount != null && walletAccount.size()>0) {
    			
    			walletAccount.put("thruDate", UtilDateTime.nowTimestamp());
    			walletAccount.put("freezeTypeId", "NO_OPERATION");
    			walletAccount.put("operationTypeId", "EXPIRED");
    			walletAccount.store();
    			
    			result.put("walletAccount", walletAccount);
    			
    			GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccount.getString("billingAccountId"));
    			if (walletAccountRole != null && walletAccountRole.size()>0) {
    				walletAccountRole.put("thruDate", UtilDateTime.nowTimestamp());
    				walletAccountRole.store();
    				
    				// expire all the associate operating wallet for master wallet
    				if ("MASTER_ACCT_OWNER".equals(walletAccountRole.getString("roleTypeId"))) {
    					
    					List<GenericValue> operators = WalletUtil.getAllActiveOperatorWalletAccounts(delegator, walletAccount.getString("billingAccountId"));
    					for (GenericValue operator : operators) {
    						operator.put("thruDate", UtilDateTime.nowTimestamp());
    						operator.put("freezeTypeId", "NO_OPERATION");
    						operator.put("operationTypeId", "EXPIRED");
    						operator.store();
    						
    						GenericValue operatorAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, operator.getString("billingAccountId"));
    		    			if (UtilValidate.isNotEmpty(operatorAccountRole)) {
    		    				operatorAccountRole.put("thruDate", UtilDateTime.nowTimestamp());
    		    				operatorAccountRole.store();
    		    			}
    					}
    				}
    				
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		//e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully expire wallet account.."));
    	
    	return result;
    	
    }

    //Added by arshiya
    public static Map<String, Object> updateEwallet(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = (Delegator) dctx.getDelegator();
        Map<String, Object> result = new HashMap<String, Object>();
        String billingAccountId= (String) context.get("billingAccountId");
        String description= (String) context.get("description");
        String operationTypeId =(String) context.get("operationTypeId");
        String freezeTypeId =(String) context.get("freezeTypeId");
        String walletName= (String) context.get("walletName");
        String  externalAccountId =(String) context.get("externalAccountId");
        BigDecimal accountLimit = (BigDecimal) context.get("accountLimit");
        String accountName =(String) context.get("accountName");
        String externalPartyId =(String) context.get("externalPartyId");
        String orgId =(String) context.get("orgId");
        String accountCurrencyUomId =(String) context.get("accountCurrencyUomId");
        String countryGeoId =(String) context.get("countryGeoId");
        String billingAccountStatusId = (String) context.get("billingAccountStatusId");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        try {
            GenericValue walletAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", billingAccountId), false);
            if (UtilValidate.isNotEmpty(walletAccount)) {
                BigDecimal balanceAmount = walletAccount.getBigDecimal("accountBalanceAmount");
                BigDecimal earmarkAmount = walletAccount.getBigDecimal("earmarkAmount");
                if (accountLimit == null) {
                    return ServiceUtil.returnError("Max Limit Allowed cannot be null.");
                } else {
                    if (balanceAmount == null) {
                        balanceAmount = BigDecimal.ZERO;
                    }
                    if (earmarkAmount == null) {
                        earmarkAmount = BigDecimal.ZERO;
                    }
                    BigDecimal totalAvailableBalance = balanceAmount.add(earmarkAmount);
                    if (accountLimit.compareTo(totalAvailableBalance) < 0) {
                        return ServiceUtil.returnError("Max Limit Allowed cannot be less than total available balance.");
                    }
                }

                if (billingAccountStatusId == null) {
                    billingAccountStatusId = "WALLET_PENDING";
                }

                //Add or update country id
                String contactMechId = walletAccount.getString("contactMechId");
                if (UtilValidate.isNotEmpty(contactMechId) && UtilValidate.isNotEmpty(countryGeoId)) {
                    GenericValue postalAddressGv = delegator.findOne("PostalAddress", UtilMisc.toMap("contactMechId",contactMechId),false);
                    if (postalAddressGv != null && postalAddressGv.size()>0) {
                        postalAddressGv.put("countryGeoId", countryGeoId);
                        postalAddressGv.put("toName", accountName);
                        postalAddressGv.store();
                    }
                } else {
                    GenericValue contactMech = delegator.makeValue("ContactMech");
                    String contactMechIdNew = delegator.getNextSeqId("ContactMech");
                    contactMech.put("contactMechId", contactMechIdNew);
                    contactMech.put("contactMechTypeId", "POSTAL_ADDRESS");
                    contactMech.create();

                    GenericValue postalAddress = delegator.makeValue("PostalAddress");
                    postalAddress.put("contactMechId", contactMechIdNew);
                    postalAddress.put("countryGeoId", countryGeoId);
                    postalAddress.put("toName", accountName);
                    postalAddress.create();
                    walletAccount.put("contactMechId", contactMechIdNew);
                }

                walletAccount.put("freezeTypeId", freezeTypeId);
                walletAccount.put("operationTypeId", operationTypeId);
                walletAccount.put("description", description);
                walletAccount.put("accountCurrencyUomId", accountCurrencyUomId);

                if (UtilValidate.isNotEmpty(walletName)) {
                    walletName = walletName.trim();
                    if (UtilValidate.isNotEmpty(walletName)) {
                        walletAccount.put("name", walletName);
                    }
                }
                walletAccount.put("accountLimit", accountLimit);
                walletAccount.put("billingAccountStatusId", billingAccountStatusId);
                walletAccount.put("lastModifiedByUserLoginId", userLogin.getString("userLoginId"));
                walletAccount.store();
                
                
                /*Get the latest status of the wallet.
                 * If it is 'Pending for Approval' and it is updated by same user login then no need to create new record just update its statusDateTime
                */
                boolean isExistingRecord = false;
                GenericValue billingAccountStatusHistory = EntityQuery.use(delegator).from("BillingAccountStatusHistory").where("billingAccountId", walletAccount.getString("billingAccountId")).orderBy("-statusDatetime").queryFirst();
                if (billingAccountStatusHistory != null) {
                    String statusHistoryUserLogin = billingAccountStatusHistory.getString("statusUserLogin");
                    if("WALLET_PENDING".equals(billingAccountStatusHistory.getString("billingAccountStatusId")) && userLogin.getString("userLoginId").equals(statusHistoryUserLogin)) {
                        billingAccountStatusHistory.set("statusDatetime", UtilDateTime.nowTimestamp());
                        billingAccountStatusHistory.store();
                        isExistingRecord = true;
                    }
                }
                if (!isExistingRecord) {
                    //Create the BillingAccountStatusHistory
                    billingAccountStatusHistory = delegator.makeValue("BillingAccountStatusHistory");
                    billingAccountStatusHistory.put("billingAccountStatusHistoryId", delegator.getNextSeqId("BillingAccountStatusHistory"));
                    billingAccountStatusHistory.put("billingAccountStatusId", billingAccountStatusId);
                    billingAccountStatusHistory.put("billingAccountId", walletAccount.getString("billingAccountId"));
                    billingAccountStatusHistory.put("statusDatetime", UtilDateTime.nowTimestamp());
                    if (userLogin != null) {
                        billingAccountStatusHistory.put("statusUserLogin", userLogin.getString("userLoginId"));
                    }
                    billingAccountStatusHistory.create();
                }

                GenericValue billingAccountRoleGv = WalletUtil.getActiveWalletAccountRole(delegator, billingAccountId);
                if (billingAccountRoleGv != null && billingAccountRoleGv.size()>0) {
                    String partyId = billingAccountRoleGv.getString("partyId");
                    if (UtilValidate.isNotEmpty(partyId)) {
                        GenericValue partyRoleGv = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId",partyId),false);
                        if (partyRoleGv != null && partyRoleGv.size()>0) {
                            partyRoleGv.put("groupName", accountName);
                            partyRoleGv.store();
                        }

                        GenericValue partyIdentification = delegator.findOne("PartyIdentification", UtilMisc.toMap("partyId", partyId, "partyIdentificationTypeId", "ABC_EXT_REF"), false);
                        if (UtilValidate.isNotEmpty(partyIdentification)) {
                            partyIdentification.put("idValue", externalPartyId);
                            partyIdentification.store();
                        } else {
                            partyIdentification = delegator.makeValue("PartyIdentification");
                            partyIdentification.put("partyId", partyId);
                            partyIdentification.put("partyIdentificationTypeId", "ABC_EXT_REF");
                            partyIdentification.put("idValue", externalPartyId);
                            partyIdentification.create();
                        }

                        if (UtilValidate.isNotEmpty(orgId)) {
                            GenericValue PartyAttributeGv = delegator.findOne("PartyAttribute", UtilMisc.toMap("partyId",partyId,"attrName","ORG_ID"),false);
                            if (UtilValidate.isEmpty(PartyAttributeGv)) {
                                GenericValue PartyAttribute = delegator.makeValue("PartyAttribute");
                                if(UtilValidate.isEmpty(PartyAttribute)){
                                    PartyAttribute.put("partyId",partyId);
                                    PartyAttribute.put("attrName", "ORG_ID");
                                    PartyAttribute.put("attrValue", orgId);
                                    PartyAttribute.create();
                                }
                            }
                        }

                        // expire/freeze all the associate operating wallet for master wallet
                        if (billingAccountRoleGv.getString("roleTypeId").equals("MASTER_ACCT_OWNER") && !"ACTIVE".equalsIgnoreCase(operationTypeId)) {
                            List<GenericValue> operators = WalletUtil.getAllActiveOperatorWalletAccounts(delegator, walletAccount.getString("billingAccountId"));
                            for (GenericValue operator : operators) {
                                if ("EXPIRED".equalsIgnoreCase(operationTypeId)) {
                                    operator.put("freezeTypeId", "NO_OPERATION");
                                    operator.put("thruDate", UtilDateTime.nowTimestamp());
                                    GenericValue operatorAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, operator.getString("billingAccountId"));
                                    if (UtilValidate.isNotEmpty(operatorAccountRole)) {
                                        operatorAccountRole.put("thruDate", UtilDateTime.nowTimestamp());
                                        operatorAccountRole.store();
                                    }
                                }
                                operator.put("operationTypeId", operationTypeId);
                                operator.store();
                            }
                        }
                    }
                }
             }
        } catch (Exception e) {
            //e.printStackTrace();
            result.put("externalAccountId", externalAccountId);
            result.putAll(ServiceUtil.returnError(e.getMessage()));
            return result;
        }
        result.put("externalAccountId", externalAccountId);
        result.putAll(ServiceUtil.returnSuccess("Successfully updated"));
        return result;
    }

    public static Map<String, Object> autoImportWallets(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String modelName = (String) context.get("modelName");
        String filePath= (String) context.get("filePath");
        String processId = null;
        if (modelName == null) {
            return ServiceUtil.returnError("Model name is missing.");
        } else {
            processId = modelName+"_Process";
        }
        String fileName = null;
        if (filePath == null) {
            return ServiceUtil.returnError("File path is missing");
        } else {
            fileName = filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
        }
        if (fileName == null) {
            return ServiceUtil.returnError("Invalid fild path");
        }
        File serverFile = new File(filePath);
        if (serverFile.length() == 0) {
            return ServiceUtil.returnError("Please load XML file without empty values");
        }
        String nowTimeString = UtilDateTime.nowDateString();
        //Create the target location for the file which will be used internally by the wallet import service
        File targetLocation = new File(serverFile.getParent() +"/"+ nowTimeString +"_"+ CommonUtil.getFileExtension(fileName));
        Debug.log("==========targetLocation========="+targetLocation);
        if (!targetLocation.exists()) {
            targetLocation.mkdir();
        }
        //Create the backup location for the file
        File backupLocation = new File(serverFile.getParent() +"/"+ nowTimeString +"_"+ CommonUtil.getFileExtension(fileName)+"_bkp");
        Debug.log("==========backupLocation========="+backupLocation);
        if (!backupLocation.exists()) {
            backupLocation.mkdir();
        }
        try {
            UtilMisc.copyFile(serverFile, new File(targetLocation.getAbsolutePath() + File.separator + fileName));
            UtilMisc.copyFile(serverFile, new File(backupLocation.getAbsolutePath() + File.separator + fileName));
        } catch (IOException e) {
            Debug.logError(e.getMessage(), MODULE);
            return ServiceUtil.returnError(e.getMessage());
        }
        CommonImportService commonImportService = ServiceFactory.getCommonImportService();
        Map<String, Object> reqContext = new HashMap<String, Object>();
        reqContext.put("dispatcher", dispatcher);
        reqContext.put("delegator", delegator);
        reqContext.put("userLogin", userLogin);
        reqContext.put("processId", processId);
        reqContext.put("modelName", modelName);
        reqContext.put("fileName", fileName);
        reqContext.put("isExecuteModelProcess", false);
        reqContext.put("filePath", serverFile.getAbsolutePath());
        reqContext.put("targetLocation", targetLocation);
        Map<String, Object> result = commonImportService.importWallet(reqContext);
        Debug.log("==========result from autoImportWallet========="+result);
        Boolean fileFailed = false;
        if (result == null) {
            fileFailed = true;
        } else {
            if (result.get(EtlConstants.RESPONSE_CODE) != null) {
                Integer resultCode = (Integer) result.get(EtlConstants.RESPONSE_CODE);
                if (resultCode.compareTo(200) != 0) {
                    fileFailed = true;
                }
            } else {
                fileFailed = true;
            }
        }
        //If file got failed then move it to "failed" location
        if (fileFailed) {
            File failedLocation = new File(serverFile.getParent() +"/"+ nowTimeString +"_"+ CommonUtil.getFileExtension(fileName)+"_failed");
            if (!failedLocation.exists()) {
                failedLocation.mkdir();
            }
            try {
                Debug.log("==========failedLocation========="+failedLocation);
                UtilMisc.copyFile(serverFile, new File(failedLocation.getAbsolutePath() + File.separator + fileName));
            } catch (IOException e) {
                Debug.logError(e.getMessage(), MODULE);
                return ServiceUtil.returnError(e.getMessage());
            }
        }
        return result;
    }

    public static Map<String, Object> updateBillingAccountStatus(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String billingAccountId = (String) context.get("billingAccountId");
        String billingAccountStatusId= (String) context.get("billingAccountStatusId");
        String billingAccountOldStatusId = null;
        String externalAccountId = (String) context.get("externalAccountId");
        String orgId = (String) context.get("orgId");
        String externalPartyId = (String) context.get("externalPartyId");
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            GenericValue validWalletStatus = EntityQuery.use(delegator).from("StatusItem").where("statusId", billingAccountStatusId, "statusTypeId", "WALLET_STATUS").queryOne();
            if (validWalletStatus == null) {
                result = ServiceUtil.returnError("Please amend the wallet and select valid operationType.");
                result.put("externalAccountId", externalAccountId);
                return result;
            }

            GenericValue billingAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", billingAccountId), false);
            if (UtilValidate.isNotEmpty(billingAccount)) {
                billingAccountOldStatusId = billingAccount.getString("billingAccountStatusId");
                externalAccountId = billingAccount.getString("externalAccountId");

                if ("WALLET_REJECTED".equals(billingAccountStatusId)) {
                    //Check whether the wallet is an active master wallet
                    GenericValue billingAccountRole = EntityQuery.use(delegator)
                                                                 .from("BillingAccountRole")
                                                                 .where("billingAccountId", billingAccount.getString("billingAccountId"), "roleTypeId", "MASTER_ACCT_OWNER")
                                                                 .filterByDate()
                                                                 .queryFirst();
                    if (billingAccountRole != null) {
                        List<GenericValue>billingAccountOldStatusHistoryList = EntityQuery.use(delegator)
                                                                                          .from("BillingAccountStatusHistory")
                                                                                          .where("billingAccountId", billingAccountId)
                                                                                          .orderBy("-statusDatetime")
                                                                                          .queryList();

                        boolean rejectOperatingWallets = false;
                        for (int i = 0; i < billingAccountOldStatusHistoryList.size(); i++) {
                            if (i == 1) {
                                GenericValue billingAccountOldStatusHistory = billingAccountOldStatusHistoryList.get(i);
                                if (billingAccountOldStatusHistory != null && "WALLET_NEW".equals(billingAccountOldStatusHistory.getString("billingAccountStatusId"))) {
                                    rejectOperatingWallets = true;
                                }
                            } else {
                                continue;
                            }
                        }
                        Debug.log("===========rejectOperatingWallets==========="+rejectOperatingWallets);
                        // Reject all the associated operating wallet for master wallet
                        if (rejectOperatingWallets) {
                            List<GenericValue> operators = EntityQuery.use(delegator).from("BillingAccountRole")
                                                                                     .where("parentBillingAccountId", billingAccount.getString("billingAccountId"))
                                                                                     .queryList();
                            Debug.log("=====operators to be rejected===="+operators);
                            for (GenericValue operator : operators) {
                                GenericValue operatingWallet = EntityQuery.use(delegator)
                                                                          .from("BillingAccount")
                                                                          .where("billingAccountId", operator.getString("billingAccountId"))
                                                                          .queryOne();
                                if (operatingWallet != null) {
                                    operatingWallet.put("billingAccountStatusId", billingAccountStatusId);
                                    operatingWallet.store();

                                    //Create the BillingAccountStatusHistory of rejected operating wallets
                                    GenericValue billingAccountStatusHistory = delegator.makeValue("BillingAccountStatusHistory");
                                    billingAccountStatusHistory.put("billingAccountStatusHistoryId", delegator.getNextSeqId("BillingAccountStatusHistory"));
                                    billingAccountStatusHistory.put("billingAccountStatusId", billingAccountStatusId);
                                    billingAccountStatusHistory.put("billingAccountId", operator.getString("billingAccountId"));
                                    billingAccountStatusHistory.put("statusDatetime", UtilDateTime.nowTimestamp());
                                    if (userLogin != null) {
                                        billingAccountStatusHistory.put("statusUserLogin", userLogin.getString("userLoginId"));
                                    }
                                    billingAccountStatusHistory.create();
                                }
                            }
                        }
                    }
                } else {
                    if (orgId == null) {
                        result = ServiceUtil.returnError("OrgId is missing.");
                        result.put("externalAccountId", externalAccountId);
                        return result;
                    }
                    if (externalPartyId == null) {
                        result = ServiceUtil.returnError("Accound Id is missing.");
                        result.put("externalAccountId", externalAccountId);
                        return result;
                    }
                }

                //Update the BillingAccount
                billingAccount.put("billingAccountStatusId", billingAccountStatusId);
                billingAccount.store();

                //Create the BillingAccountStatusHistory
                GenericValue billingAccountStatusHistory = delegator.makeValue("BillingAccountStatusHistory");
                billingAccountStatusHistory.put("billingAccountStatusHistoryId", delegator.getNextSeqId("BillingAccountStatusHistory"));
                billingAccountStatusHistory.put("billingAccountStatusId", billingAccountStatusId);
                billingAccountStatusHistory.put("billingAccountId", billingAccountId);
                billingAccountStatusHistory.put("statusDatetime", UtilDateTime.nowTimestamp());
                if (userLogin != null) {
                    billingAccountStatusHistory.put("statusUserLogin", userLogin.getString("userLoginId"));
                }
                billingAccountStatusHistory.create();
            }
        } catch (GenericEntityException gee) {
            Debug.logError(gee.getMessage(), MODULE);
            return ServiceUtil.returnError(gee.getMessage());
        }
        result.put("billingAccountOldStatusId", billingAccountOldStatusId);
        result.put("externalAccountId", externalAccountId);
        if ("WALLET_REJECTED".equals(billingAccountStatusId)) {
            result.put("successMessage", "Wallet account has been rejected successfully.");
        } else {
            result.put("successMessage", "Wallet account has been approved successfully.");
        }
        return result;
    }

    public static Map<String, Object> getOrgIdFromCountryCodeAndAccountNo(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        String countryCode = (String) context.get("countryCode");
        String accountNo = (String) context.get("accountNo");
        String orgId = null;
        try {
            String apiUrl = EntityUtilProperties.getPropertyValue("Etl-Process", "etl.orgId.api.url", delegator);
            if (apiUrl != null && UtilValidate.isNotEmpty(apiUrl)) {
                Client client = Client.create();
                WebResource webResource = client.resource(apiUrl+countryCode+"/"+accountNo);
                ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
                if (response.getStatus() != 200) {
                   Debug.logError("Failed : HTTP error code : "+ response.getStatus(), MODULE);
                   return ServiceUtil.returnError("Failed : HTTP error code : "+ response.getStatus());
                }
                orgId = response.getEntity(String.class);
            } else {
                return ServiceUtil.returnError("OrgId API URL is missing, please configure it in database.");
            }
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            return ServiceUtil.returnError(e.getMessage());
        }
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("orgId", orgId);
        return result;
    }
}