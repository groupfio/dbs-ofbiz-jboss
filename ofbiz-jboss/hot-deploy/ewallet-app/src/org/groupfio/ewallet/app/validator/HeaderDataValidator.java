package org.groupfio.ewallet.app.validator;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.groupfio.etl.process.util.WalletUtil;
import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.AppUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
public class HeaderDataValidator implements Validator {

    private static String MODULE = HeaderDataValidator.class.getName();
    private boolean validate;

    /* (non-Javadoc)
     * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
     */
    @Override
    public Map<String, Object> validate(Map<String, Object> context) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> data = (Map<String, Object>) context.get("data");
        String errorSummary = null;
        String transactionCurrency = null;
        String billingAccountCurrency = null;
        String receiverAccountCurrency = null;
        String sendAccountCtry = null;
        String receiverAccountCtry = null;
        try {
            setValidate(true);
            if (UtilValidate.isNotEmpty(data.get("header"))) {
                Map<String, Object> header = (Map<String, Object>) data.get("header");
                Delegator delegator = (Delegator) context.get("delegator");

                //msgId validation
                String msgId = UtilValidate.isNotEmpty(header.get("msgId")) ? (String) header.get("msgId") : null;
                Debug.log("==========HeaderDataValidator msgId=========="+msgId);
                if (StringUtils.isNotEmpty(msgId)) {
                    //Check for length of the msgId
                    if (msgId.length() > 35) {
                        Debug.logError("msgId contains more than 35 characters", MODULE);
                        setValidate(false);
                        errorSummary = "E118";
                    }

                    if (isValidate()) {
                        //Check for special characters in the msgId
                        Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
                        Matcher matcher = pattern.matcher(msgId);
                        if (matcher.find()) {
                           Debug.logError("msgId contains special characters", MODULE);
                           setValidate(false);
                           errorSummary = "E117";
                        }

                        if (isValidate()) {
                            //90 Days date validation for msgId
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(new Date());
                            cal.add(Calendar.DAY_OF_MONTH, -90);
                            Timestamp ninetyDaysAgo = UtilDateTime.getTimestamp(cal.getTimeInMillis());
                            Debug.log("==========HeaderDataValidator ninetyDaysAgo=========="+ninetyDaysAgo);
                            List<GenericValue> walletApiLogs = EntityQuery.use(delegator).from("WalletApiLog")
                                    .where(EntityCondition.makeCondition("msgId", EntityOperator.EQUALS, msgId), EntityCondition.makeCondition("requestedTime", EntityOperator.GREATER_THAN_EQUAL_TO, ninetyDaysAgo))
                                    .queryList();
                            Debug.log("==========HeaderDataValidator walletApiLogs=========="+walletApiLogs);
                            if (UtilValidate.isNotEmpty(walletApiLogs)) {
                                setValidate(false);
                                errorSummary = "E110";
                            }
                        }
                    }
                } else {
                    setValidate(false);
                    errorSummary = "E109";
                }

                String serviceName = UtilValidate.isNotEmpty(context.get("serviceName")) ? (String) context.get("serviceName") : null;
                if (isValidate()) {
                    //orgId validation
                    String orgId = UtilValidate.isNotEmpty(header.get("orgId")) ? (String) header.get("orgId") : null;
                    if (UtilValidate.isNotEmpty(orgId)) {
                        //Check for length of the orgId
                        if (orgId.length() > 12) {
                            Debug.logError("orgId contains more than 12 characters", MODULE);
                            setValidate(false);
                            errorSummary = "E121";
                        }

                        if (isValidate()) {
                            //Check for special characters in the orgId
                            Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
                            Matcher matcher = pattern.matcher(orgId);
                            if (matcher.find()) {
                               Debug.logError("orgId contains special characters", MODULE);
                               setValidate(false);
                               errorSummary = "E120";
                            }

                            //Validating orgId against the account number
                            if (isValidate()) {
                                boolean isValidJSONStructure = true;
                                boolean isValidOrgId = false;
                                String accountNo = null;
                                String receiverNo = null;
                                if ("getAccountBalance".equals(serviceName)) {
                                    if (data.get("accountBalInfo") != null) {
                                        Map<String, Object> accountBalInfo = (Map<String, Object>) data.get("accountBalInfo");
                                        if (accountBalInfo != null && accountBalInfo.get("accountNo") != null) {
                                            accountNo = (String) accountBalInfo.get("accountNo");
                                        } else {
                                            isValidJSONStructure = false;
                                        }
                                    } else {
                                        isValidJSONStructure = false;
                                    }
                                } else if ("getTransactionList".equals(serviceName)) {
                                    if (data.get("txnEnquiry") != null) {
                                        Map<String, Object> txnEnquiry = (Map<String, Object>) data.get("txnEnquiry");
                                        if (txnEnquiry != null && txnEnquiry.get("enqAccountNo") != null) {
                                            accountNo = (String) txnEnquiry.get("enqAccountNo");
                                        } else {
                                            isValidJSONStructure = false;
                                        }
                                    } else {
                                        isValidJSONStructure = false;
                                    }
                                } else if ("fundTransfer".equals(serviceName)) {
                                    if (data.get("txnInfo") != null) {
                                        Map<String, Object> txnInfo = (Map<String, Object>) data.get("txnInfo");
                                        //get sender currency information 
                                        transactionCurrency = (String)txnInfo.get("txnCcy");
                                        
                                        if (txnInfo != null && txnInfo.get("receivingParty") != null) {
                                        	 Map<String, Object> receiverParty = (Map<String, Object>) txnInfo.get("receivingParty");
                                        	 if (receiverParty != null && receiverParty.get("accountNo") != null) {
                                        		 receiverNo = (String) receiverParty.get("accountNo");
                                             }
                                        }
                                        if (txnInfo != null && txnInfo.get("senderParty") != null) {
                                            Map<String, Object> senderParty = (Map<String, Object>) txnInfo.get("senderParty");
                                            if (senderParty != null && senderParty.get("accountNo") != null) {
                                                accountNo = (String) senderParty.get("accountNo");
                                            } else {
                                                isValidJSONStructure = false;
                                            }
                                        } else {
                                            isValidJSONStructure = false;
                                        }
                                    } else {
                                        isValidJSONStructure = false;
                                    }
                                }
                                if (!isValidJSONStructure) {
                                    Debug.logError("Invalid JSON Structure", MODULE);
                                    setValidate(false);
                                    errorSummary = "E104";
                                }
                                if (isValidate()) {
                                    if (accountNo != null) {
                                        GenericValue billingAccount = EntityQuery.use(delegator).from("BillingAccount").where("externalAccountId", accountNo).filterByDate().queryFirst();
                                        if (billingAccount != null) {
                                            String billingAccountId = billingAccount.getString("billingAccountId");
                                         
                                             billingAccountCurrency = billingAccount.getString("accountCurrencyUomId");
                                             
                                             String contactMechId = billingAccount.getString("contactMechId");
                                             if(UtilValidate.isNotEmpty(contactMechId))
                                             {
                                            	 GenericValue postalAddress = delegator.findOne("PostalAddress", false, UtilMisc.toMap("contactMechId",contactMechId));
                                            	 if(UtilValidate.isNotEmpty(postalAddress))
                                            	 {
                                            		 sendAccountCtry = postalAddress.getString("countryGeoId");
                                            	 }
                                            	  
                                             }
                                             
                                             
                                           
                                            GenericValue billingAccountRole = EntityQuery.use(delegator).from("BillingAccountRole").where("billingAccountId", billingAccountId).filterByDate().queryFirst();
                                            if (billingAccountRole != null) {
                                                GenericValue partyAttribute = EntityQuery.use(delegator).from("PartyAttribute").where("partyId", billingAccountRole.getString("partyId"), "attrName", "ORG_ID", "attrValue", orgId).queryOne();
                                                if (partyAttribute != null) {
                                                    isValidOrgId = true;
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(receiverNo!=null)
                                    {
                                    	GenericValue billingAccount = EntityQuery.use(delegator).from("BillingAccount").where("externalAccountId", receiverNo).filterByDate().queryFirst();
                                        if (billingAccount != null) {
                                        	  receiverAccountCurrency = billingAccount.getString("accountCurrencyUomId");
                                        	  
                                        	  String contactMechId = billingAccount.getString("contactMechId");
                                              if(UtilValidate.isNotEmpty(contactMechId))
                                              {
                                             	 GenericValue postalAddress = delegator.findOne("PostalAddress", false, UtilMisc.toMap("contactMechId",contactMechId));
                                             	 if(UtilValidate.isNotEmpty(postalAddress))
                                             	 {
                                             		 receiverAccountCtry = postalAddress.getString("countryGeoId");
                                             	 }
                                             	  
                                              }
                                              
                                              
                                              
                                        }
                                        
                                    }
                                    if (!isValidOrgId) {
                                        Debug.logError("orgId is not associated with account no.", MODULE);
                                        setValidate(false);
                                        errorSummary = "E126";
                                    }
                                    

                                	//@vijayakumar desc : restrict fund transfer from M1 -> O2  and O1 -> O2
                                	if(!isValidTransactionBtwAcc(delegator,accountNo,receiverNo))
                                	{
                                          setValidate(false);
                                          errorSummary = "E313";
                                          Debug.logError("Transaction not going to process", MODULE);
                                	}
                                }
                            }
                        }
                    } else {
                        setValidate(false);
                        errorSummary = "E111";
                    }

                    if (isValidate()) {
                        Debug.log("==========HeaderDataValidator 81==========");
                        //timeStamp validation
                        String timeStampStr = UtilValidate.isNotEmpty(header.get("timeStamp")) ? (String) header.get("timeStamp") : null;
                        if (UtilValidate.isNotEmpty(timeStampStr)) {
                            //Check for special characters in the timeStamp
                            Pattern pattern = Pattern.compile("[^A-Za-z0-9:.-]");
                            Matcher matcher = pattern.matcher(timeStampStr);
                            if (matcher.find()) {
                               Debug.logError("timeStamp contains special characters", MODULE);
                               setValidate(false);
                               errorSummary = "E119";
                            }

                            //Date format validation
                            try {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                                dateFormat.setLenient(false);
                                Date parsedDate = dateFormat.parse(timeStampStr);
                                Timestamp timeStamp = new java.sql.Timestamp(parsedDate.getTime());
                                if (UtilValidate.isEmpty(timeStamp)) {
                                    setValidate(false);
                                    errorSummary = "E114";
                                }
                            } catch (ParseException px) {
                                Debug.logError(px.getMessage(), MODULE);
                                setValidate(false);
                                errorSummary = "E114";
                            }
                        } else {
                            setValidate(false);
                            errorSummary = "E113";
                        }
                    }
                    Debug.log("==========HeaderDataValidator 107==========");

                    if (isValidate()) {
                        //API key validation
                        if (AppUtil.isServiceRestricted(delegator, serviceName)) {
                            String apiKey = UtilValidate.isNotEmpty(header.get("apiKey")) ? (String) header.get("apiKey") : null;
                            if (UtilValidate.isNotEmpty(apiKey)) {
                                Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
                                Map<String, Object> validatorContext = new HashMap<String, Object>();
                                List<String> authorization = Arrays.asList(apiKey);
                                validatorContext.put("delegator", delegator);
                                validatorContext.put("authorization", authorization);
                                Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
                                Debug.log("==========validatorResponse=========="+validatorResponse);
                                if (ResponseUtils.isError(validatorResponse)) {
                                    setValidate(false);
                                    errorSummary = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                                } else {
                                    response.put("clientRegistryId", (String) validatorResponse.get("clientRegistryId"));
                                }
                            } else {
                                setValidate(false);
                                errorSummary = "E108";
                            }
                        }
                    }
                    if (isValidate()) {
                        //Country validation only for Fund Transfer API
                        if ("fundTransfer".equals(serviceName)) {
                        	//added by m.vijayakumar for validation billing account currency and specified currency is valid or not
                        	//1. check for jso currency and from party currency is valid or not  
                        	if(UtilValidate.isNotEmpty(billingAccountCurrency))
                        	{
                        		if(!billingAccountCurrency.equals(transactionCurrency))
                        		{
                        			setValidate(false);
                                    errorSummary = "E231";
                        		}else if(!receiverAccountCurrency.equals(transactionCurrency))
                        		{
                        			setValidate(false);
                                    errorSummary = "E421";
                        		}
                        	}
                        	
                        
                        	
                        	//end @vijayakumar
                        	
                            String ctry = UtilValidate.isNotEmpty(header.get("ctry")) ? (String) header.get("ctry") : null;
                            if (UtilValidate.isNotEmpty(ctry)) {
                                GenericValue geo = EntityQuery.use(delegator).from("Geo").where("geoCode", ctry, "geoTypeId","COUNTRY").cache().queryOne();
                                if (geo == null) {
                                    setValidate(false);
                                    errorSummary = "E116";
                                }else
                                {
                                	
                                	String sendGeoCode = null,receiverGeocode = null;
                                	//check for country of three is valid or not
                                	GenericValue sendGeo = delegator.findOne("Geo", false, UtilMisc.toMap("geoId",sendAccountCtry));
                                	if(UtilValidate.isNotEmpty(sendGeo))
                                	{
                                		sendGeoCode =  sendGeo.getString("geoCode");                               	
                                	}
                                	
                                	GenericValue receiverGeo = delegator.findOne("Geo", false, UtilMisc.toMap("geoId",receiverAccountCtry));
                                	if(UtilValidate.isNotEmpty(receiverGeo))
                                	{
                                		receiverGeocode = receiverGeo.getString("geoCode");
                                	}
                                	
                                	if(!(ctry.equals(sendGeoCode) && ctry.equals(receiverGeocode)))
                                	{
                                		 setValidate(false);
                                         errorSummary = "E116";
                                	}
                                }
                            } else {
                                setValidate(false);
                                errorSummary = "E115";
                            }
                        }
                    }
                }
            } else {
                setValidate(false);
                errorSummary = "E104";
            }
            if (!isValidate()) {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
                response.put(EMConstants.RESPONSE_MESSAGE, errorSummary);
            } else {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
            }
        } catch (Exception e) {
            Debug.log(e.getMessage(), MODULE);
            response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            response.put(EMConstants.RESPONSE_MESSAGE, "E900");
            return response;
        }
        Debug.log("===========response from HeaderDataValidation===========");
        return response;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
    
    //@vijayakumar
    public static boolean isValidTransactionBtwAcc(Delegator delegator,String senderAccountNo, String receiverAccountNo)
    {
    	try{
    		if(UtilValidate.isNotEmpty(senderAccountNo) && UtilValidate.isNotEmpty(receiverAccountNo))
    		{
    			//At first get the information about sender a. get its master or operating account type
    			String senderRoleType = getWalletAccountType(delegator, senderAccountNo,false,null);
    			String receiverRoleType = getWalletAccountType(delegator, receiverAccountNo,false,null);
    			
    			//if master tring to transfer the fund to operating
    			if("MASTER_ACCT_OWNER".equalsIgnoreCase(senderRoleType) && "OPERATING_ACCT_OWNER".equalsIgnoreCase(receiverRoleType))
    			{
    				//get all operating account before that need master partyId
    				String parentPartyId = getWalletAccountType(delegator, senderAccountNo,true,null);
    				
    				//get list of operating party information
    				List<GenericValue> operatingAccountList = delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("parentPartyId",parentPartyId), null, false);
    				if(UtilValidate.isNotEmpty(operatingAccountList))
    				{
    					List<String> operatingAccounts = EntityUtil.getFieldListFromEntityList(operatingAccountList, "partyId", true);
    					if(UtilValidate.isNotEmpty(operatingAccounts))
    					{
    						String receiverPartyId = getWalletAccountType(delegator, receiverAccountNo,true,null);
    						
    						//if none of the operating account party id is not available then we need to process.
    						if(!operatingAccounts.contains(receiverPartyId))
    						{
    							return false;
    						}
    					}
    				}
    			}//if operating tries to send fund to master
    			else if("OPERATING_ACCT_OWNER".equalsIgnoreCase(senderRoleType) && "MASTER_ACCT_OWNER".equalsIgnoreCase(receiverRoleType))
    			{
    				//get operating account party id
    				String masterPartyId = getWalletAccountType(delegator, senderAccountNo,false,"parentPartyId");
    				if(UtilValidate.isNotEmpty(masterPartyId))
    				{
    					String receiverPartyId = getWalletAccountType(delegator, receiverAccountNo,true,null);
    					if(!masterPartyId.equalsIgnoreCase(receiverPartyId))
    					{
    						return false;
    					}
    				}
    			}//if both are same then FIXME: M -> M transaction we need to analyse
    			else if(senderRoleType.equalsIgnoreCase(receiverRoleType))
    			{
    				if("MASTER_ACCT_OWNER".equalsIgnoreCase(senderRoleType))
    				{
    					//because there is only one M--1-------*--->O
    					//return false; // if master to master transaction need to success then enable it
    					
    				}else
    				{
    					String masterPartyId = getWalletAccountType(delegator, senderAccountNo,false,"parentPartyId");
    					String receiverPartyId = getWalletAccountType(delegator, receiverAccountNo,false,"parentPartyId");
    					if(!masterPartyId.equalsIgnoreCase(receiverPartyId))
    					{
    						return false;
    					}
    				}
    			}
    				
    			
    			
    			
    		}
    	}catch(Exception e)
    	{
    		Debug.logError("Error occured @isValidTransactionBtwAcc", MODULE);
    	}
    	return true;
    }
    
    public static String getWalletAccountType(Delegator delegator,String accountNo,boolean returnParty,String column)
    {
    	String accountInfo = null;
    	try
    	{
			GenericValue billAcc = WalletUtil.getActiveWalletAccount(delegator, accountNo);
			if(UtilValidate.isNotEmpty(billAcc))
			{
				String billingAccountId = billAcc.getString("billingAccountId");
				List<GenericValue> billingAccountRole = delegator.findByAnd("BillingAccountRole", UtilMisc.toMap("billingAccountId",billingAccountId), null, false);
				if(UtilValidate.isNotEmpty(billingAccountRole))
	    		{
					GenericValue billingAccountRle = EntityUtil.getFirst(billingAccountRole);
					if(UtilValidate.isNotEmpty(billingAccountRle))
					{
						accountInfo = returnParty?billingAccountRle.getString("partyId"):billingAccountRle.getString("roleTypeId");
						if(UtilValidate.isNotEmpty(column))
						{
							accountInfo = billingAccountRle.getString(column);
						}
					}
	    		}
			}
		
    	}catch(Exception e)
    	{
    		Debug.logError("Error occured @getWalletAccountType", MODULE);
    		return accountInfo;
    	}
    	return accountInfo;
    	
    }
    
}