/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class UpdateParty extends Response {

	private String partyID;
	private String externalAppPartyRef;
	private String partyStatus;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setPartyID( ParamUtil.getString(context, "partyId") );
		setExternalAppPartyRef( ParamUtil.getString(context, "externalAppPartyRef") );
		setPartyStatus( ParamUtil.getString(context, "partyStatus") );
		
	}

	public String getPartyID() {
		return partyID;
	}

	public void setPartyID(String partyID) {
		this.partyID = partyID;
	}

	public String getExternalAppPartyRef() {
		return externalAppPartyRef;
	}

	public void setExternalAppPartyRef(String externalAppPartyRef) {
		this.externalAppPartyRef = externalAppPartyRef;
	}

	public String getPartyStatus() {
		return partyStatus;
	}

	public void setPartyStatus(String partyStatus) {
		this.partyStatus = partyStatus;
	}

}
