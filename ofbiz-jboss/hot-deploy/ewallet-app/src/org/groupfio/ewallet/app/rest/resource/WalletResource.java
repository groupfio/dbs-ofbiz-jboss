/**
 * 
 */
package org.groupfio.ewallet.app.rest.resource;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.rest.response.CreateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.CreateWalletAccount;
import org.groupfio.ewallet.app.rest.response.ExpireWalletAccount;
import org.groupfio.ewallet.app.rest.response.GetWalletAccount;
import org.groupfio.ewallet.app.rest.response.UpdateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.UpdateWalletAccount;
import org.groupfio.ewallet.app.util.AppUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.groupfio.ewallet.app.validator.Validator;
import org.groupfio.ewallet.app.validator.ValidatorFactory;
import org.groupfio.ewallet.app.writer.WriterUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
@Path("/wallet/v1/wallet")
public class WalletResource {

	private static final String MODULE = WalletResource.class.getName();

	@Context
    HttpHeaders headers;
	
	@POST
	@Path("/createWalletAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CreateWalletAccount createWalletAccount(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        CreateWalletAccount response = new CreateWalletAccount();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "createWalletAccount")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
				
			Validator validator = ValidatorFactory.getCreateWalletAccountDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
			
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("partyId", request.get("partyId"));
				context.put("name", request.get("walletName"));
				context.put("accountCurrencyUomId", request.get("walletCurrency"));
				context.put("description", request.get("description"));
				context.put("accountLimit", ParamUtil.getBigDecimal(request, "maxLimitAllowed"));
				
				Timestamp fromDate = null;
				Timestamp thruDate = null;
				if (UtilValidate.isNotEmpty(request.get("fromDate"))) {
					fromDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "fromDate"));
				}
				if (UtilValidate.isNotEmpty(request.get("thruDate"))) {
					thruDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "thruDate"));
				}
				context.put("fromDate", fromDate);
				context.put("thruDate", thruDate);
				
				context.put("userLogin", userLogin);
				
                String orgId = null;
                //Call the service to getOrgId based on countryCode and accountNo
                Map<String, Object> orgIdContext = new HashMap<String, Object>();
                //As discussed with DBS, default wallet country for phase-1 is SG, so hardcoded it here, later we may have to make it configurable.
                orgIdContext.put("countryCode", "SG");
                orgIdContext.put("accountNo", request.get("walletAcctId"));
                orgIdContext.put("userLogin", userLogin);
                Map<String, Object> orgIdResult = dispatcher.runSync("getOrgIdFromCountryCodeAndAccountNo", orgIdContext);
                if (ServiceUtil.isError(orgIdResult)) {
                    Debug.logError("Unable to get orgId from the API", MODULE);
                } else {
                    orgId = (String) orgIdResult.get("orgId");
                }
                context.put("orgId", orgId);

				Map<String, Object> res = dispatcher.runSync("ewallet.createWalletAccount", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in createWalletAccount "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "createWalletAccount", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/updateWalletAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UpdateWalletAccount updateWalletAccount(Object jsonReq) {
		
		Timestamp requestedTime = UtilDateTime.nowTimestamp();

		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        UpdateWalletAccount response = new UpdateWalletAccount();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "updateWalletAccount")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getUpdateWalletAccountDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("partyId", request.get("partyId"));
				context.put("name", request.get("walletName"));
				context.put("accountCurrencyUomId", request.get("walletCurrency"));
				context.put("description", request.get("description"));
				context.put("freezeTypeId", request.get("freezeType"));
				context.put("operationTypeId", request.get("operationType"));
				context.put("accountLimit", ParamUtil.getBigDecimal(request, "maxLimitAllowed"));
				
				Timestamp fromDate = null;
				Timestamp thruDate = null;
				if (UtilValidate.isNotEmpty(request.get("fromDate"))) {
					fromDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "fromDate"));
				}
				if (UtilValidate.isNotEmpty(request.get("thruDate"))) {
					thruDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "thruDate"));
				}
				context.put("fromDate", fromDate);
				context.put("thruDate", thruDate);
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.updateWalletAccount", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in createWalletAccount "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "updateWalletAccount", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/getWalletAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetWalletAccount getWalletAccount(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        GetWalletAccount response = new GetWalletAccount();
		
		boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "getWalletAccount")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getGetWalletAccountDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("nodeType", request.get("nodeType"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.getWalletAccount", context);
				res.put("delegator", delegator);
				//res.put("walletAcctId", request.get("walletAcctId"));
				
				if (UtilValidate.isEmpty(res.get("walletAccount"))) {
					res.put("responseCode", "E304");
				} else if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 	
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in getWalletAccount "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "getWalletAccount", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/createOperatingWalletUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CreateOperatingWalletUser createOperatingWalletUser(Object jsonReq) {
		
		Timestamp requestedTime = UtilDateTime.nowTimestamp();

		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        CreateOperatingWalletUser response = new CreateOperatingWalletUser();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "createOperatingWalletUser")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getCreateOperatingWalletUserDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("partyName", request.get("partyName"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.createOperatingWalletUser", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", res.get("responseCode"));
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in createOperatingWalletUser "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "createOperatingWalletUser", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/updateOperatingWalletUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UpdateOperatingWalletUser updateOperatingWalletUser(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        UpdateOperatingWalletUser response = new UpdateOperatingWalletUser();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "updateOperatingWalletUser")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getUpdateOperatingWalletUserDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("userPartyId", request.get("userPartyId"));
				context.put("externalAccountId", request.get("walletAcctId"));
				context.put("partyName", request.get("partyName"));
				context.put("partyStatus", request.get("userStatus"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.updateOperatingWalletUser", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in updateOperatingWalletUser "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "updateOperatingWalletUser", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/expireWalletAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExpireWalletAccount expireWalletAccount(Object jsonReq) {
		
		Timestamp requestedTime = UtilDateTime.nowTimestamp();

		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        ExpireWalletAccount response = new ExpireWalletAccount();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "expireWalletAccount")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getExpireWalletAccountDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("externalAccountId", request.get("walletAcctId"));
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.expireWalletAccount", context);
				res.put("delegator", delegator);
				res.put("walletAcctId", request.get("walletAcctId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in expireWalletAccount "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "expireWalletAccount", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
}
