package org.groupfio.ewallet.app.rest.response;
import java.util.Map;
import org.groupfio.ewallet.app.rest.response.AccountBalResponse;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Group Fio
 *
 */
public class GetAccountBalance {
    private AccountBalResponse accountBalResponse;
    private Header header;

    public void doBuild(Map<String, Object> context) throws Exception {
        //Setting Header values
        String msgId = UtilValidate.isNotEmpty(context.get("msgId")) ? (String) context.get("msgId") : "";
        String orgId = UtilValidate.isNotEmpty(context.get("orgId")) ? (String) context.get("orgId") : "";
        String apiKey = UtilValidate.isNotEmpty(context.get("apiKey")) ? (String) context.get("apiKey") : "";
        String timeStamp = UtilValidate.isNotEmpty(context.get("timeStamp")) ? (String) context.get("timeStamp") : "";
        this.header = new Header(msgId, orgId, apiKey, timeStamp);

        //Setting AccountBalResponse values
        String enqStatus = UtilValidate.isNotEmpty(context.get("enqStatus")) ? (String) context.get("enqStatus") : "";
        if ("ACSP".equals(enqStatus)) {
            //Setting AccountBalSuccessResponse values
            String accountName = UtilValidate.isNotEmpty(context.get("accountName")) ? (String) context.get("accountName") : "";
            String accountNo = UtilValidate.isNotEmpty(context.get("accountNo")) ? (String) context.get("accountNo") : "";
            String accountCcy = UtilValidate.isNotEmpty(context.get("accountCcy")) ? (String) context.get("accountCcy") : "";
            String businessDate = UtilValidate.isNotEmpty(context.get("businessDate")) ? (String) context.get("businessDate") : "";
            String clsAvailableBal = UtilValidate.isNotEmpty(context.get("clsAvailableBal")) ? context.get("clsAvailableBal").toString() : "";
            this.accountBalResponse = new AccountBalSuccessResponse(enqStatus, accountName, accountNo, accountCcy, businessDate, clsAvailableBal);
        } else {
            //Setting AccountBalErrorResponse values
            String enqRejectCode = UtilValidate.isNotEmpty(context.get("enqRejectCode")) ? (String) context.get("enqRejectCode") : "";
            String enqStatusDescription = UtilValidate.isNotEmpty(context.get("enqStatusDescription")) ? (String) context.get("enqStatusDescription") : "";
            this.accountBalResponse = new AccountBalErrorResponse(enqStatus, enqRejectCode, enqStatusDescription);
        }
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public AccountBalResponse getAccountBalResponse() {
        return accountBalResponse;
    }

    public void setAccountBalResponse(AccountBalResponse accountBalResponse) {
        this.accountBalResponse = accountBalResponse;
    }
}