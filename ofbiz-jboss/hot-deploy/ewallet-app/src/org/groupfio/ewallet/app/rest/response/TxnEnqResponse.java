/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class TxnEnqResponse {
    protected String enqStatus;

    public TxnEnqResponse() {
    }

    public TxnEnqResponse(String enqStatus) {
        this.enqStatus = enqStatus;
    }

    public String getEnqStatus() {
        return enqStatus;
    }

    public void setEnqStatus(String enqStatus) {
        this.enqStatus = enqStatus;
    }
}