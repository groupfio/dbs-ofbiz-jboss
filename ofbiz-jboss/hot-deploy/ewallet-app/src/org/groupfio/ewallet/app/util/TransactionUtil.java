/**
 * 
 */
package org.groupfio.ewallet.app.util;

import java.math.BigDecimal;
import java.util.List;

import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;

/**
 * @author Group Fio
 *
 */
public class TransactionUtil {

	public static BigDecimal getTotalAvailableBalance (Delegator delegator, GenericValue walletAccount) {
		
		BigDecimal totalAvailableBalance = new BigDecimal(0);
		
		if (walletAccount!=null && walletAccount.size()>0) {
			
			BigDecimal earmarkAmount = UtilValidate.isNotEmpty(walletAccount.getBigDecimal("earmarkAmount")) ? walletAccount.getBigDecimal("earmarkAmount") : new BigDecimal(0);
			totalAvailableBalance = earmarkAmount.add(walletAccount.getBigDecimal("accountBalanceAmount"));
		}
		
		return totalAvailableBalance;
		
	}
	
}
