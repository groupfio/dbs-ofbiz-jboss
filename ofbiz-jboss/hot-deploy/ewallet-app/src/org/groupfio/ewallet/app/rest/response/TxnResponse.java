package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class TxnResponse {
    private String walletTxnId;
    private String txnRefId;
    private String txnStatus;
    private String txnRejectCode;
    private String txnStatusDescription;
    private String txnSettlementAmt;
    private String txnSettlementDt;

    public TxnResponse() {}

    public TxnResponse(String walletTxnId, String txnRefId, String txnStatus, String txnRejectCode,
            String txnStatusDescription, String txnSettlementAmt, String txnSettlementDt) {
        this.walletTxnId = walletTxnId;
        this.txnRefId = txnRefId;
        this.txnStatus = txnStatus;
        this.txnRejectCode = txnRejectCode;
        this.txnStatusDescription = txnStatusDescription;
        this.txnSettlementAmt = txnSettlementAmt;
        this.txnSettlementDt = txnSettlementDt;
    }

    public String getWalletTxnId() {
        return walletTxnId;
    }

    public void setWalletTxnId(String walletTxnId) {
        this.walletTxnId = walletTxnId;
    }

    public String getTxnRefId() {
        return txnRefId;
    }

    public void setTxnRefId(String txnRefId) {
        this.txnRefId = txnRefId;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnRejectCode() {
        return txnRejectCode;
    }

    public void setTxnRejectCode(String txnRejectCode) {
        this.txnRejectCode = txnRejectCode;
    }

    public String getTxnStatusDescription() {
        return txnStatusDescription;
    }

    public void setTxnStatusDescription(String txnStatusDescription) {
        this.txnStatusDescription = txnStatusDescription;
    }

    public String getTxnSettlementAmt() {
        return txnSettlementAmt;
    }

    public void setTxnSettlementAmt(String txnSettlementAmt) {
        this.txnSettlementAmt = txnSettlementAmt;
    }

    public String getTxnSettlementDt() {
        return txnSettlementDt;
    }

    public void setTxnSettlementDt(String txnSettlementDt) {
        this.txnSettlementDt = txnSettlementDt;
    }
}