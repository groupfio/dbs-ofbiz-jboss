/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.math.BigDecimal;
import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class PerformWalletFundTransfer extends Response {
	
	private String walletTxnId;
	private String walletTxnExternalId;
	private BigDecimal transAmount; 

	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setWalletTxnId( ParamUtil.getString(context, "walletTxnId") );
		setWalletTxnExternalId( ParamUtil.getString(context, "walletTxnExternalId") );
		setTransAmount( ParamUtil.getBigDecimal(context, "transAmount") );
	}

	public String getWalletTxnId() {
		return walletTxnId;
	}

	public void setWalletTxnId(String walletTxnId) {
		this.walletTxnId = walletTxnId;
	}

	public String getWalletTxnExternalId() {
		return walletTxnExternalId;
	}

	public void setWalletTxnExternalId(String walletTxnExternalId) {
		this.walletTxnExternalId = walletTxnExternalId;
	}

	public BigDecimal getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}
	
}
