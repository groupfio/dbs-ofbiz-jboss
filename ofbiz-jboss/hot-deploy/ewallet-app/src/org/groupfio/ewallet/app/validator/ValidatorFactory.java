/**
 * 
 */
package org.groupfio.ewallet.app.validator;

/**
 * @author Group Fio
 *
 */
public final class ValidatorFactory {

	private static final AccessTokenValidator ACCESS_TOKEN_VALIDATOR = new AccessTokenValidator();
	
	private static final RegisterWalletApplicationDataValidator REGISTER_WALLET_APPLICATION_VALIDATOR = new RegisterWalletApplicationDataValidator();
	private static final UpdateWalletApplicationDataValidator UPDATE_WALLET_APPLICATION_VALIDATOR = new UpdateWalletApplicationDataValidator();
	
	private static final CreatePartyDataValidator CREATE_PARTY_VALIDATOR = new CreatePartyDataValidator();
	private static final UpdatePartyDataValidator UPDATE_PARTY_VALIDATOR = new UpdatePartyDataValidator();
	
	private static final CreateWalletAccountDataValidator CREATE_WALLET_ACCOUNT_VALIDATOR = new CreateWalletAccountDataValidator();
	private static final UpdateWalletAccountDataValidator UPDATE_WALLET_ACCOUNT_VALIDATOR = new UpdateWalletAccountDataValidator();
	private static final GetWalletAccountDataValidator GET_WALLET_ACCOUNT_VALIDATOR = new GetWalletAccountDataValidator();
    private static final HeaderDataValidator HEADER_VALIDATOR = new HeaderDataValidator();
    private static final GetAccountBalanceDataValidator GET_ACCOUNT_BALANCE_VALIDATOR = new GetAccountBalanceDataValidator();
    private static final GetTransactionListDataValidator GET_TRANSACTION_LIST_VALIDATOR = new GetTransactionListDataValidator();
    private static final FundTransferDataValidator FUND_TRANSFER_VALIDATOR = new FundTransferDataValidator();
	private static final CreateOperatingWalletUserDataValidator CREATE_OPERATING_WALLET_USER_VALIDATOR = new CreateOperatingWalletUserDataValidator();
	private static final UpdateOperatingWalletUserDataValidator UPDATE_OPERATING_WALLET_USER_VALIDATOR = new UpdateOperatingWalletUserDataValidator();
	private static final PerformWalletFundTransferDataValidator PERFORM_WALLET_FUND_TRANSFER_VALIDATOR = new PerformWalletFundTransferDataValidator();
	private static final ApplyWalletTransactionDataValidator APPLY_WALLET_TRANSACTION_VALIDATOR = new ApplyWalletTransactionDataValidator();
	private static final AccountTransEntriesDataValidator ACCOUNT_TRANS_ENTRIES_VALIDATOR = new AccountTransEntriesDataValidator();
	private static final GetWalletAPIAnalysisServiceDataValidator GET_WALLET_API_ANALYSIS_VALIDATOR = new GetWalletAPIAnalysisServiceDataValidator();
	
	private static final ExpireWalletAccountDataValidator EXPIRE_WALLET_ACCOUNT_VALIDATOR = new ExpireWalletAccountDataValidator();
	
	public static AccessTokenValidator getAccessTokenValidator () {
		return ACCESS_TOKEN_VALIDATOR;
	}
	
	public static RegisterWalletApplicationDataValidator getRegisterWalletApplicationDataValidator () {
		return REGISTER_WALLET_APPLICATION_VALIDATOR;
	}
	
	public static UpdateWalletApplicationDataValidator getUpdateWalletApplicationDataValidator () {
		return UPDATE_WALLET_APPLICATION_VALIDATOR;
	}
	
	public static CreatePartyDataValidator getCreatePartyDataValidator () {
		return CREATE_PARTY_VALIDATOR;
	}
	
	public static UpdatePartyDataValidator getUpdatePartyDataValidator () {
		return UPDATE_PARTY_VALIDATOR;
	}
	
	public static GetWalletAccountDataValidator getGetWalletAccountDataValidator () {
		return GET_WALLET_ACCOUNT_VALIDATOR;
	}
	
    public static HeaderDataValidator getHeaderDataValidator () {
        return HEADER_VALIDATOR;
    }

    public static GetAccountBalanceDataValidator getGetAccountBalanceDataValidator () {
        return GET_ACCOUNT_BALANCE_VALIDATOR;
    }

    public static GetTransactionListDataValidator getGetTransactionListDataValidator () {
        return GET_TRANSACTION_LIST_VALIDATOR;
    }

    public static FundTransferDataValidator getFundTransferDataValidator () {
        return FUND_TRANSFER_VALIDATOR;
    }

	public static CreateWalletAccountDataValidator getCreateWalletAccountDataValidator () {
		return CREATE_WALLET_ACCOUNT_VALIDATOR;
	}
	
	public static UpdateWalletAccountDataValidator getUpdateWalletAccountDataValidator () {
		return UPDATE_WALLET_ACCOUNT_VALIDATOR;
	}
	
	public static CreateOperatingWalletUserDataValidator getCreateOperatingWalletUserDataValidator () {
		return CREATE_OPERATING_WALLET_USER_VALIDATOR;
	}
	
	public static UpdateOperatingWalletUserDataValidator getUpdateOperatingWalletUserDataValidator () {
		return UPDATE_OPERATING_WALLET_USER_VALIDATOR;
	}
	
	public static PerformWalletFundTransferDataValidator getPerformWalletFundTransferDataValidator () {
		return PERFORM_WALLET_FUND_TRANSFER_VALIDATOR;
	}
	
	public static ApplyWalletTransactionDataValidator getApplyWalletTransactionDataValidator () {
		return APPLY_WALLET_TRANSACTION_VALIDATOR;
	}
	
	public static AccountTransEntriesDataValidator getAccountTransEntriesDataValidator () {
		return ACCOUNT_TRANS_ENTRIES_VALIDATOR;
	}
	
	public static GetWalletAPIAnalysisServiceDataValidator getGetWalletAPIAnalysisServiceDataValidator () {
		return GET_WALLET_API_ANALYSIS_VALIDATOR;
	}
	
	public static ExpireWalletAccountDataValidator getExpireWalletAccountDataValidator () {
		return EXPIRE_WALLET_ACCOUNT_VALIDATOR;
	}
	
}
