/**
 * 
 */
package org.groupfio.ewallet.app.rest.resource;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.rest.response.CreateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.CreateWalletAccount;
import org.groupfio.ewallet.app.rest.response.GetWalletAPIAnalysisService;
import org.groupfio.ewallet.app.rest.response.GetWalletAccount;
import org.groupfio.ewallet.app.rest.response.UpdateOperatingWalletUser;
import org.groupfio.ewallet.app.rest.response.UpdateWalletAccount;
import org.groupfio.ewallet.app.util.AppUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.groupfio.ewallet.app.validator.Validator;
import org.groupfio.ewallet.app.validator.ValidatorFactory;
import org.groupfio.ewallet.app.writer.WriterUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
@Path("/wallet/v1/report")
public class ReportResource {

	private static final String MODULE = ReportResource.class.getName();

	@Context
    HttpHeaders headers;
	
	@POST
	@Path("/getWalletAPIAnalysisService")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetWalletAPIAnalysisService getWalletAPIAnalysisService(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        GetWalletAPIAnalysisService response = new GetWalletAPIAnalysisService();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "getWalletAPIAnalysisService")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
				
			Validator validator = ValidatorFactory.getGetWalletAPIAnalysisServiceDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
			
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("serviceName", request.get("serviceName"));
				
				Timestamp fromDate = null;
				Timestamp thruDate = null;
				if (UtilValidate.isNotEmpty(request.get("fromDate"))) {
					fromDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "fromDate"));
				}
				if (UtilValidate.isNotEmpty(request.get("thruDate"))) {
					thruDate = UtilDateTime.toTimestamp(ParamUtil.getString(request, "thruDate"));
				}
				context.put("fromDate", fromDate);
				context.put("thruDate", thruDate);
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.getWalletAPIAnalysisService", context);
				res.put("delegator", delegator);
				res.put("serviceName", request.get("serviceName"));
				res.put("fromDate", request.get("fromDate"));
				res.put("thruDate", request.get("thruDate"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in getWalletAPIAnalysisService "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "getWalletAPIAnalysisService", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
}
