package org.groupfio.ewallet.app.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Group Fio
 *
 */
@Path("/api/CustomerProfile/")

public class CustomerProfileResource {
    private static final String MODULE = CustomerProfileResource.class.getName();
    @GET
    @Path("getOrgId/{countryCode}/{accountNo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrgId(@PathParam("countryCode") String countryCode, @PathParam("accountNo") String accountNo) {
       return Response.status(200).entity(countryCode+accountNo).build();
    }
}