/**
 * 
 */
package org.groupfio.ewallet.app.rest.resource;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.constants.EMConstants.AppStatus;
import org.groupfio.ewallet.app.constants.EMConstants.SourceInvoked;
import org.groupfio.ewallet.app.rest.response.RegisterWalletApplication;
import org.groupfio.ewallet.app.util.AppUtil;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.groupfio.ewallet.app.validator.Validator;
import org.groupfio.ewallet.app.validator.ValidatorFactory;
import org.groupfio.ewallet.app.writer.WriterUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
@Path("/wallet/v1/app")
public class ApplicationResource {

	private static final String MODULE = ApplicationResource.class.getName();

	@Context
    HttpHeaders headers;
	
	@POST
	@Path("/registerWalletApplication")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterWalletApplication registerWalletApplication(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        RegisterWalletApplication response = new RegisterWalletApplication();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "registerWalletApplication")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getRegisterWalletApplicationDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("clientName", request.get("clientName"));
				context.put("clientDomain", request.get("clientRequestDomainName"));
				context.put("clientAppPassword", request.get("clientAuthenticationKey"));
				context.put("externalReferenceId", request.get("externalReferenceId"));
				context.put("comments", request.get("comments"));
				context.put("appStatusId", AppStatus.ACTIVATED);
				
				context.put("sourceInvoked", SourceInvoked.API);
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.createAppRegistry", context);
				res.put("delegator", delegator);
				res.put("appAuthCode", res.get("secretCode"));
				res.put("clientAppStatus", AppStatus.ACTIVATED);
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in registerWalletApplication "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "registerWalletApplication", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), response.getClientRegistryId(), requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}
	
	@POST
	@Path("/updateWalletApplication")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterWalletApplication updateWalletApplication(Object jsonReq) {

		Timestamp requestedTime = UtilDateTime.nowTimestamp();
		
		Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
		
        RegisterWalletApplication response = new RegisterWalletApplication();
		
        boolean isError = false;
		String clientRegistryId = null;
		
		if (AppUtil.isServiceRestricted(delegator, "updateWalletApplication")) {
		
			Validator tokenValidator = ValidatorFactory.getAccessTokenValidator();
			
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			
			validatorContext.put("delegator", delegator);
			validatorContext.put("authorization", headers.getRequestHeader("authorization"));
			
			Map<String, Object> validatorResponse = tokenValidator.validate(validatorContext);
			
			if (ResponseUtils.isError(validatorResponse)) {
				response.setResponseCode(ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE));
				
				isError = true;
			}
			clientRegistryId = (String) validatorResponse.get("clientRegistryId");
		}
		
		if (UtilValidate.isEmpty(jsonReq)) {
			
			response.setResponseCode("E104");
			
			isError = true;
		}
		
		Map<String, Object> request = new HashMap<String, Object>();
		
		if (!isError) {
		
		try {
			
			GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			request = jsonMap.convert(jsonFeed);
			
			Validator validator = ValidatorFactory.getUpdateWalletApplicationDataValidator();
			Map<String, Object> validatorContext = new HashMap<String, Object>();
			validatorContext.put("delegator", delegator);
			validatorContext.put("data", request);
			
			Map<String, Object> validatorResponse = validator.validate(validatorContext);
			if (!ResponseUtils.isError(validatorResponse)) {
				
				Map<String, Object> context = new HashMap<String, Object>();
				
				context.put("clientName", request.get("clientName"));
				context.put("clientDomain", request.get("clientRequestDomainName"));
				context.put("clientAppPassword", request.get("clientAuthenticationKey"));
				context.put("externalReferenceId", request.get("externalReferenceId"));
				context.put("comments", request.get("comments"));
				//context.put("appStatusId", AppStatus.ACTIVATED);
				
				//context.put("sourceInvoked", SourceInvoked.API);
				
				context.put("userLogin", userLogin);
				
				Map<String, Object> res = dispatcher.runSync("ewallet.updateAppRegistry", context);
				res.put("delegator", delegator);
				res.put("appAuthCode", res.get("secretCode"));
				res.put("clientAppStatus", res.get("appStatusId"));
				
				if (ServiceUtil.isSuccess(res)) {
					res.put("responseCode", "S200");
				} else {
					res.put("responseCode", "E900");
				}
			
				response.build(res);
			} else {
				response.build(validatorResponse);
			} 		
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.log("Error in updateWalletApplication "+e);
			response.setResponseCode("E900");
		}
		
		}
		
		Timestamp responsedTime = UtilDateTime.nowTimestamp();
		String walletApiLogId = WriterUtil.writeLog(delegator, "updateWalletApplication", (String) request.get("clientRecordRefId"), jsonReq, response, response.getResponseCode(), ResponseUtils.getResponseStatus(response.getResponseCode()), clientRegistryId, requestedTime, responsedTime);
		response.setResponseRefId(walletApiLogId);
		
		ResponseUtils.prepareResponse(delegator, response);
		
		return response;

	}

}
