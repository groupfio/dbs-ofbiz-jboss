/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class RegisterWalletApplication extends Response {
	
	private String appAuthCode;
	private String clientAppStatus;
	private String clientRegistryId;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setAppAuthCode( ParamUtil.getString(context, "appAuthCode") );
		setClientAppStatus( ParamUtil.getString(context, "clientAppStatus") );
		setClientRegistryId( ParamUtil.getString(context, "clientRegistryId") );
	}

	public String getAppAuthCode() {
		return appAuthCode;
	}

	public void setAppAuthCode(String appAuthCode) {
		this.appAuthCode = appAuthCode;
	}

	public String getClientAppStatus() {
		return clientAppStatus;
	}

	public void setClientAppStatus(String clientAppStatus) {
		this.clientAppStatus = clientAppStatus;
	}

	public String getClientRegistryId() {
		return clientRegistryId;
	}

	public void setClientRegistryId(String clientRegistryId) {
		this.clientRegistryId = clientRegistryId;
	}
	
}
