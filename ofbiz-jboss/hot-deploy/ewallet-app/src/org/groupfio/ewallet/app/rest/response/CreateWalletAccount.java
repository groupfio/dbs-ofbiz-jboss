/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class CreateWalletAccount extends Response {

	private String walletAcctId;
	private String billingAccountId;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		prepareContext(context);
		
		setWalletAcctId( ParamUtil.getString(context, "walletAcctId") );
		setBillingAccountId( ParamUtil.getString(context, "billingAccountId") );
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}

	public String getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(String billingAccountId) {
		this.billingAccountId = billingAccountId;
	}
	
}
