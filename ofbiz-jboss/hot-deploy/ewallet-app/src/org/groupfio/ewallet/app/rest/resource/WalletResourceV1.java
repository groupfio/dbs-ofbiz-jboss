package org.groupfio.ewallet.app.rest.resource;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.rest.response.FundTransfer;
import org.groupfio.ewallet.app.rest.response.GetAccountBalance;
import org.groupfio.ewallet.app.rest.response.GetTransactionList;
import org.groupfio.ewallet.app.util.ParamUtil;
import org.groupfio.ewallet.app.util.ResponseUtils;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.groupfio.ewallet.app.validator.Validator;
import org.groupfio.ewallet.app.validator.ValidatorFactory;
import org.groupfio.ewallet.app.writer.WriterUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceContainer;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
@Path("/rapid/wallet/v1/")
public class WalletResourceV1 {
    private static final String MODULE = WalletResourceV1.class.getName();
    @Context
    HttpHeaders headers;

    @POST
    @Path("/account/balance")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GetAccountBalance getAccountBalance(Object jsonReq) {
        Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
        Timestamp requestedTime = UtilDateTime.nowTimestamp();
        GetAccountBalance response = new GetAccountBalance();
        Map<String, Object> request = new HashMap<String, Object>();
        Map<String, Object> apiResult = new HashMap<String, Object>();
        String msgId = null;
        String orgId = null;
        String timeStampStr = null;
        Timestamp timeStamp = null;
        String apiKey = null;
        String clientRegistryId = null;
        boolean isError = false;
        try {
            if (UtilValidate.isEmpty(jsonReq)) {
                apiResult.put("enqStatus", "RJCT");
                String enqRejectCode = "E104";
                apiResult.put("enqRejectCode", enqRejectCode);
                GenericValue errorCode = EntityUtil.getFirst(delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                if (UtilValidate.isNotEmpty(errorCode)) {
                    apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                }
                isError = true;
            }
            if (!isError) {
                JSON jsonFeed = JSON.from(jsonReq);
                JSONToMap jsonMap = new JSONToMap();
                request = jsonMap.convert(jsonFeed);
                if (UtilValidate.isNotEmpty(request.get("header"))) {
                    Map<String, Object> header = (Map<String, Object>) request.get("header");
                    msgId = UtilValidate.isNotEmpty(header.get("msgId")) ? (String) header.get("msgId") : null;
                    orgId = UtilValidate.isNotEmpty(header.get("orgId")) ? (String) header.get("orgId") : null;
                    timeStampStr = UtilValidate.isNotEmpty(header.get("timeStampStr")) ? (String) header.get("timeStampStr") : null;
                    apiKey = UtilValidate.isNotEmpty(header.get("apiKey")) ? (String) header.get("apiKey") : null;
                }
    
                Validator headerValidator = ValidatorFactory.getHeaderDataValidator();
                Map<String, Object> validatorContext = new HashMap<String, Object>();
                validatorContext.put("delegator", delegator);
                validatorContext.put("data", request);
                validatorContext.put("serviceName", "getAccountBalance");
                Map<String, Object> validatorResponse = headerValidator.validate(validatorContext);
                Debug.log("==========header Validator response getAccountBalance=========="+validatorResponse);
                if (ResponseUtils.isError(validatorResponse)) {
                    apiResult.put("enqStatus", "RJCT");
                    String enqRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                    apiResult.put("enqRejectCode", enqRejectCode);
                    GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                    if (UtilValidate.isNotEmpty(errorCode)) {
                        apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                    }
                    isError = true;
                } else {
                    clientRegistryId = (String) validatorResponse.get("clientRegistryId");
                    timeStamp = (Timestamp) validatorResponse.get("timeStamp");
                }
                validatorContext.clear();
    
                if (!isError) {
                    GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
                    Validator validator = ValidatorFactory.getGetAccountBalanceDataValidator();
                    validatorContext.put("delegator", delegator);
                    validatorContext.put("data", request);
                    validatorResponse = validator.validate(validatorContext);
                    Debug.log("==========body Validator response getAccountBalance=========="+validatorResponse);
                    if (!ResponseUtils.isError(validatorResponse)) {
                        Map<String, Object> context = new HashMap<String, Object>();
                        Map<String, Object> accountBalInfo = (Map<String, Object>) request.get("accountBalInfo");
                        String accountNo = null;
                        if (UtilValidate.isNotEmpty(accountBalInfo)) {
                            accountNo = (String) accountBalInfo.get("accountNo");
                        }
                        context.put("externalAccountId", accountNo);
                        context.put("userLogin", userLogin);
                        Map<String, Object> result = dispatcher.runSync("ewallet.getWalletAccount", context);
                        if (ServiceUtil.isSuccess(result)) {
                            if (UtilValidate.isNotEmpty(result.get("walletAccount"))) {
                                GenericValue billingAccount = (GenericValue) result.get("walletAccount");
                                apiResult.put("enqStatus", "ACSP");
                                apiResult.put("accountName", billingAccount.getString("name"));
                                apiResult.put("accountNo", accountNo);
                                apiResult.put("accountCcy", billingAccount.getString("accountCurrencyUomId"));
                                Timestamp businessDate = billingAccount.getTimestamp("fromDate");
                                if (UtilValidate.isNotEmpty(businessDate)) {
                                    apiResult.put("businessDate", UtilDateTime.timeStampToString(UtilDateTime.nowTimestamp(), "yyyy-MM-dd HH:mm:ss", TimeZone.getDefault(), null));
                                }
                                apiResult.put("clsAvailableBal", UtilValidate.isNotEmpty(billingAccount.getBigDecimal("accountBalanceAmount")) ? billingAccount.getBigDecimal("accountBalanceAmount") : BigDecimal.ZERO);
                            } else {
                                apiResult.put("enqStatus", "RJCT");
                                String enqRejectCode = "E104";
                                apiResult.put("enqRejectCode", enqRejectCode);
                                GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false));
                                if (UtilValidate.isNotEmpty(errorCode)) {
                                    apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                                }
                            }
                        } else {
                            apiResult.put("enqStatus", "RJCT");
                            String enqRejectCode = "E900";
                            apiResult.put("enqRejectCode", enqRejectCode);
                            GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                            if (UtilValidate.isNotEmpty(errorCode)) {
                                apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                            }
                        }
                    } else {
                        apiResult.put("enqStatus", "RJCT");
                        String enqRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                        apiResult.put("enqRejectCode", enqRejectCode);
                        GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                        if (UtilValidate.isNotEmpty(errorCode)) {
                            apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                        }
                    }
                }
            }
            apiResult.put("msgId", msgId);
            apiResult.put("orgId", orgId);
            apiResult.put("apiKey", apiKey);
            apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
            Debug.log("==========apiResult getAccountBalance=========="+apiResult);
            response.doBuild(apiResult);
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            try {
                apiResult.put("enqStatus", "RJCT");
                String enqRejectCode = null;
                if (e.getMessage() != null && e.getMessage().contains("JsonParseException")) {
                    enqRejectCode = "E104";
                } else {
                    enqRejectCode = "E900";
                }
                apiResult.put("enqRejectCode", enqRejectCode);
                GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false));
                apiResult.put("enqStatusDescription", UtilValidate.isNotEmpty(errorCode) ? errorCode.getString("solutionDescription") : e.getMessage());
                apiResult.put("msgId", msgId);
                apiResult.put("orgId", orgId);
                apiResult.put("apiKey", apiKey);
                apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
                response.doBuild(apiResult);
            } catch (Exception ex) {
                Debug.logError(ex.getMessage(), MODULE);
            }
        }
        String enqStatus = null;
        if(response != null  && response.getAccountBalResponse() != null) {
            enqStatus = response.getAccountBalResponse().getEnqStatus();
        }
        WriterUtil.writeLog(delegator, "getAccountBalance", null, jsonReq, response, enqStatus, enqStatus, clientRegistryId, requestedTime, UtilDateTime.nowTimestamp(), msgId, orgId);
        return response;
    }

    @POST
    @Path("/transaction/list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GetTransactionList getTransactionList(Object jsonReq) {
        GetTransactionList response = new GetTransactionList();
        Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
        Timestamp requestedTime = UtilDateTime.nowTimestamp();
        Map<String, Object> request = new HashMap<String, Object>();
        Map<String, Object> apiResult = new HashMap<String, Object>();
        String msgId = null;
        String orgId = null;
        String timeStampStr = null;
        Timestamp timeStamp = null;
        String apiKey = null;
        String clientRegistryId = null;
        boolean isError = false;
        try {
            if (UtilValidate.isEmpty(jsonReq)) {
                apiResult.put("enqStatus", "RJCT");
                String enqRejectCode = "E104";
                apiResult.put("enqRejectCode", enqRejectCode);
                GenericValue errorCode = EntityUtil.getFirst(delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                if (UtilValidate.isNotEmpty(errorCode)) {
                    apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                }
                isError = true;
            }
            if (!isError) {
                JSON jsonFeed = JSON.from(jsonReq);
                JSONToMap jsonMap = new JSONToMap();
                request = jsonMap.convert(jsonFeed);
                if (UtilValidate.isNotEmpty(request.get("header"))) {
                    Map<String, Object> header = (Map<String, Object>) request.get("header");
                    msgId = UtilValidate.isNotEmpty(header.get("msgId")) ? (String) header.get("msgId") : null;
                    orgId = UtilValidate.isNotEmpty(header.get("orgId")) ? (String) header.get("orgId") : null;
                    timeStampStr = UtilValidate.isNotEmpty(header.get("timeStampStr")) ? (String) header.get("timeStampStr") : null;
                    apiKey = UtilValidate.isNotEmpty(header.get("apiKey")) ? (String) header.get("apiKey") : null;
                }

                Validator headerValidator = ValidatorFactory.getHeaderDataValidator();
                Map<String, Object> validatorContext = new HashMap<String, Object>();
                validatorContext.put("delegator", delegator);
                validatorContext.put("data", request);
                validatorContext.put("serviceName", "getTransactionList");
                Map<String, Object> validatorResponse = headerValidator.validate(validatorContext);
                Debug.log("==========header Validator response getTransactionList=========="+validatorResponse);
                if (ResponseUtils.isError(validatorResponse)) {
                    apiResult.put("enqStatus", "RJCT");
                    String enqRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                    apiResult.put("enqRejectCode", enqRejectCode);
                    GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                    if (UtilValidate.isNotEmpty(errorCode)) {
                        apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                    }
                    isError = true;
                } else {
                    clientRegistryId = (String) validatorResponse.get("clientRegistryId");
                }
                validatorContext.clear();

                if (!isError) {
                    GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
                    Validator validator = ValidatorFactory.getGetTransactionListDataValidator();
                    validatorContext.put("delegator", delegator);
                    validatorContext.put("data", request);
                    validatorResponse = validator.validate(validatorContext);
                    Debug.log("==========body Validator response getTransactionList=========="+validatorResponse);
                    if (!ResponseUtils.isError(validatorResponse)) {
                        Map<String, Object> context = new HashMap<String, Object>();
                        Map<String, Object> txnEnquiry = (Map<String, Object>) request.get("txnEnquiry");
                        String accountCcy = null;
                        String enqAccountNo = null;
                        Timestamp fromDate = null;
                        Timestamp thruDate = null;
                        if (UtilValidate.isNotEmpty(txnEnquiry)) {
                            accountCcy = (String) txnEnquiry.get("accountCcy");
                            enqAccountNo = (String) txnEnquiry.get("enqAccountNo");
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            if (UtilValidate.isNotEmpty(txnEnquiry.get("fromDate"))) {
                                Date parsedDate = dateFormat.parse(txnEnquiry.get("fromDate").toString());
                                fromDate = new java.sql.Timestamp(parsedDate.getTime());
                            }
                            if (UtilValidate.isNotEmpty(txnEnquiry.get("toDate"))) {
                                Date parsedDate = dateFormat.parse(txnEnquiry.get("toDate").toString());
                                //We need to get data upto end of the day 23:59:59
                                thruDate = UtilDateTime.getDayEnd(new java.sql.Timestamp(parsedDate.getTime()));
                            }
                        }
                        context.put("externalAccountId", enqAccountNo);
                        context.put("fromDate", fromDate);
                        context.put("thruDate", thruDate);
                        context.put("userLogin", userLogin);
                        Map<String, Object> result = dispatcher.runSync("ewallet.getWalletAccountTransEntries", context);
                        if (ServiceUtil.isSuccess(result)) {
                            if (UtilValidate.isNotEmpty(result.get("walletAccount"))) {
                                List<Map<String, Object>> entries = (List<Map<String, Object>>) result.get("entries");
                                apiResult.put("enqStatus", "ACSP");
                                apiResult.put("delegator", delegator);
                                apiResult.put("entries", entries);
                            } else {
                                apiResult.put("enqStatus", "RJCT");
                                String enqRejectCode = "E104";
                                apiResult.put("enqRejectCode", enqRejectCode);
                                GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false));
                                if (UtilValidate.isNotEmpty(errorCode)) {
                                    apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                                }
                            }
                        } else {
                            apiResult.put("enqStatus", "RJCT");
                            String enqRejectCode = "E900";
                            apiResult.put("enqRejectCode", enqRejectCode);
                            GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                            if (UtilValidate.isNotEmpty(errorCode)) {
                                apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                            }
                        }
                    } else {
                        apiResult.put("enqStatus", "RJCT");
                        String enqRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                        apiResult.put("enqRejectCode", enqRejectCode);
                        GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false) );
                        if (UtilValidate.isNotEmpty(errorCode)) {
                            apiResult.put("enqStatusDescription", errorCode.getString("solutionDescription"));
                        }
                    }
                }
            }
            apiResult.put("msgId", msgId);
            apiResult.put("orgId", orgId);
            apiResult.put("apiKey", apiKey);
            apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
            Debug.log("==========apiResult getTransactionList=========="+apiResult);
            response.doBuild(apiResult);
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            try {
                apiResult.put("enqStatus", "RJCT");
                String enqRejectCode = null;
                if (e.getMessage() != null && e.getMessage().contains("JsonParseException")) {
                    enqRejectCode = "E104";
                } else {
                    enqRejectCode = "E900";
                }
                apiResult.put("enqRejectCode", enqRejectCode);
                GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", enqRejectCode), null, false));
                apiResult.put("enqStatusDescription", UtilValidate.isNotEmpty(errorCode) ? errorCode.getString("solutionDescription") : e.getMessage());
                apiResult.put("msgId", msgId);
                apiResult.put("orgId", orgId);
                apiResult.put("apiKey", apiKey);
                apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
                response.doBuild(apiResult);
            } catch (Exception ex) {
                Debug.logError(ex.getMessage(), MODULE);
            }
        }
        String enqStatus = UtilValidate.isNotEmpty(response.getTxnEnqResponse()) ? response.getTxnEnqResponse().get(0).getEnqStatus() : null;
        WriterUtil.writeLog(delegator, "getTransactionList", msgId, jsonReq, response, enqStatus, enqStatus, clientRegistryId, requestedTime, UtilDateTime.nowTimestamp(), msgId, orgId);
        return response;
    }

    @POST
    @Path("/fund/transfer")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public FundTransfer fundTransfer(Object jsonReq) {
        FundTransfer response = new FundTransfer();
        Delegator delegator = (Delegator) DelegatorFactory.getDelegator("default");
        LocalDispatcher dispatcher = ServiceContainer.getLocalDispatcher(delegator.getDelegatorName(), delegator);
        Timestamp requestedTime = UtilDateTime.nowTimestamp();
        Map<String, Object> request = new HashMap<String, Object>();
        Map<String, Object> apiResult = new HashMap<String, Object>();
        //header Fields
        String msgId = null;
        String orgId = null;
        String timeStampStr = null;
        Timestamp timeStamp = null;
        String apiKey = null;
        String ctry = null;
        //txnResponse Fields
        String walletTxnId = null;
        String txnRefId = null;
        String txnStatus = null;
        String txnRejectCode = null;
        String txnStatusDescription = null;
        String txnSettlementAmt = null;
        String txnSettlementDt = null;

        String clientRegistryId = null;
        boolean isError = false;
        try {
            if (UtilValidate.isEmpty(jsonReq)) {
                txnStatus = "RJCT";
                txnRejectCode = "E104";
                isError = true;
            }
            if (!isError) {
                JSON jsonFeed = JSON.from(jsonReq);
                JSONToMap jsonMap = new JSONToMap();
                request = jsonMap.convert(jsonFeed);
                Map<String, Object> header = (Map<String, Object>) request.get("header");
                if (UtilValidate.isNotEmpty(header)) {
                    msgId = UtilValidate.isNotEmpty(header.get("msgId")) ? (String) header.get("msgId") : null;
                    orgId = UtilValidate.isNotEmpty(header.get("orgId")) ? (String) header.get("orgId") : null;
                    timeStampStr = UtilValidate.isNotEmpty(header.get("timeStampStr")) ? (String) header.get("timeStampStr") : null;
                    apiKey = UtilValidate.isNotEmpty(header.get("apiKey")) ? (String) header.get("apiKey") : null;
                    ctry = UtilValidate.isNotEmpty(header.get("ctry")) ? (String) header.get("ctry") : null;
                }

                Map<String, Object> txnInfo = (Map<String, Object>) request.get("txnInfo");
                if (UtilValidate.isNotEmpty(txnInfo)) {
                    walletTxnId = UtilValidate.isNotEmpty(txnInfo.get("walletTxnId")) ? (String) txnInfo.get("walletTxnId") : null;
                    txnRefId = UtilValidate.isNotEmpty(txnInfo.get("txnRefId")) ? (String) txnInfo.get("txnRefId") : null;
                    txnSettlementAmt = UtilValidate.isNotEmpty(txnInfo.get("txnAmount")) ? (String) txnInfo.get("txnAmount") : null;
                    txnSettlementDt = UtilValidate.isNotEmpty(txnInfo.get("txnDate")) ? (String) txnInfo.get("txnDate") : null;
                }

                Validator headerValidator = ValidatorFactory.getHeaderDataValidator();
                Map<String, Object> validatorContext = new HashMap<String, Object>();
                validatorContext.put("delegator", delegator);
                validatorContext.put("data", request);
                validatorContext.put("serviceName", "fundTransfer");
                Map<String, Object> validatorResponse = headerValidator.validate(validatorContext);
                Debug.log("==========header Validator response fundTransfer=========="+validatorResponse);
                if (ResponseUtils.isError(validatorResponse)) {
                    txnStatus = "RJCT";
                    txnRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                    isError = true;
                } else {
                    clientRegistryId = (String) validatorResponse.get("clientRegistryId");
                }
                validatorContext.clear();

                if (!isError) {
                    GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", "system").queryOne();
                    Validator validator = ValidatorFactory.getFundTransferDataValidator();
                    validatorContext.put("delegator", delegator);
                    validatorContext.put("data", request);
                    validatorResponse = validator.validate(validatorContext);
                    Debug.log("==========validatorResponse fundTransfer=========="+validatorResponse);
                    if (!ResponseUtils.isError(validatorResponse)) {
                        String externalTxnId = (String) txnInfo.get("walletTxnId");
                        Map<String, Object> senderParty = (Map<String, Object>) txnInfo.get("senderParty");
                        String walletAcctIdFrom = (String) senderParty.get("accountNo");
                        String senderPartyId = null;
                        GenericValue walletAccountFrom = WalletUtil.getActiveWalletAccount(delegator, walletAcctIdFrom);
                        if (walletAccountFrom != null) {
                            GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccountFrom.getString("billingAccountId"));
                            if (walletAccountRole != null) {
                                senderPartyId = walletAccountRole.getString("partyId");
                            }
                        }
                        Map<String, Object> receivingParty = (Map<String, Object>) txnInfo.get("receivingParty");
                        String walletAcctIdTo = (String) receivingParty.get("accountNo");
                        String receivingPartyId = null;
                        GenericValue walletAccountTo = WalletUtil.getActiveWalletAccount(delegator, walletAcctIdTo);
                        if (walletAccountTo != null) {
                            GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccountTo.getString("billingAccountId"));
                            if (walletAccountRole != null) {
                                receivingPartyId = walletAccountRole.getString("partyId");
                            }
                        }
                        String description = UtilValidate.isNotEmpty(txnInfo.get("paymentDescription")) ? (String) txnInfo.get("paymentDescription") : null;
                        BigDecimal txnAmount = UtilValidate.isNotEmpty(txnSettlementAmt) ? new BigDecimal(txnSettlementAmt) : null;

                        Map<String, Object> context = new HashMap<String, Object>();
                        context.put("externalTxnId", externalTxnId);
                        context.put("walletAcctIdFrom", walletAcctIdFrom);
                        context.put("senderPartyId", senderPartyId);
                        context.put("walletAcctIdTo", walletAcctIdTo);
                        context.put("receivingPartyId", receivingPartyId);
                        context.put("description", description);
                        context.put("amountToTransfer", txnAmount);
                        context.put("userLogin", userLogin);
                        Map<String, Object> result = dispatcher.runSync("ewallet.performWalletFundTransfer", context);
                        if (ServiceUtil.isSuccess(result)) {
                            if (UtilValidate.isNotEmpty(result.get("paymentId"))) {
                                txnStatus = "ACSP";
                                txnRefId =  (String) result.get("paymentId");
                            } else {
                                txnStatus = "RJCT";
                                txnRejectCode = "E104";
                            }
                        } else {
                            txnStatus = "RJCT";
                            txnRejectCode = "E900";
                        }
                    } else {
                        txnStatus = "RJCT";
                        txnRejectCode = ParamUtil.getString(validatorResponse, EMConstants.RESPONSE_MESSAGE);
                    }
                }
            }
            apiResult.put("msgId", msgId);
            apiResult.put("orgId", orgId);
            apiResult.put("apiKey", apiKey);
            apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
            apiResult.put("ctry", ctry);
            apiResult.put("walletTxnId", walletTxnId);
            apiResult.put("txnRefId", txnRefId);
            apiResult.put("txnStatus", txnStatus);
            if ("RJCT".equals(txnStatus)) {
                apiResult.put("txnRejectCode", txnRejectCode);
                GenericValue errorCode = EntityUtil.getFirst(delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", txnRejectCode), null, false));
                if (UtilValidate.isNotEmpty(errorCode)) {
                    txnStatusDescription = errorCode.getString("solutionDescription");
                }
            } else {
                txnStatusDescription = "Success";
            }
            apiResult.put("txnStatusDescription", txnStatusDescription);
            apiResult.put("txnSettlementAmt", txnSettlementAmt);
            apiResult.put("txnSettlementDt", UtilDateTime.nowDateString("yyyy-MM-dd"));
            Debug.log("==========apiResult fundTransfer=========="+apiResult);
            response.doBuild(apiResult);
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
            try {
                if (e.getMessage() != null && e.getMessage().contains("JsonParseException")) {
                    txnRejectCode = "E104";
                } else {
                    txnRejectCode = "E900";
                }
                apiResult.put("msgId", msgId);
                apiResult.put("orgId", orgId);
                apiResult.put("apiKey", apiKey);
                apiResult.put("timeStamp", UtilDateTime.nowDateString("yyyy-MM-dd'T'HH:mm:ss.sss"));
                apiResult.put("ctry", ctry);
                apiResult.put("walletTxnId", walletTxnId);
                apiResult.put("txnRefId", txnRefId);
                apiResult.put("txnStatus", "RJCT");
                apiResult.put("txnRejectCode", txnRejectCode);
                GenericValue errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("code", txnRejectCode), null, false));
                apiResult.put("txnStatusDescription", UtilValidate.isNotEmpty(errorCode) ? errorCode.getString("solutionDescription") : e.getMessage());
                apiResult.put("txnSettlementDt", UtilDateTime.nowDateString("yyyy-MM-dd"));
                response.doBuild(apiResult);
            } catch (Exception ex) {
                Debug.logError(ex.getMessage(), MODULE);
            }
        }
        WriterUtil.writeLog(delegator, "fundTransfer", msgId, jsonReq, response, txnStatus, txnStatusDescription, clientRegistryId, requestedTime, UtilDateTime.nowTimestamp(), msgId, orgId);
        return response;
    }
}