/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

/**
 * @author Group Fio
 *
 */
public class TxnEnqErrorResponse extends TxnEnqResponse {
    private String enqRejectCode;
    private String enqStatusDescription;

    public TxnEnqErrorResponse() {}

    public TxnEnqErrorResponse(String enqStatus, String enqRejectCode, String enqStatusDescription) {
        this.enqStatus = enqStatus;
        this.enqRejectCode = enqRejectCode;
        this.enqStatusDescription = enqStatusDescription;
    }

    public String getEnqStatus() {
        return enqStatus;
    }

    public void setEnqStatus(String enqStatus) {
        this.enqStatus = enqStatus;
    }
    
    public String getEnqRejectCode() {
        return enqRejectCode;
    }

    public void setEnqRejectCode(String enqRejectCode) {
        this.enqRejectCode = enqRejectCode;
    }

    public String getEnqStatusDescription() {
        return enqStatusDescription;
    }

    public void setEnqStatusDescription(String enqStatusDescription) {
        this.enqStatusDescription = enqStatusDescription;
    }
}