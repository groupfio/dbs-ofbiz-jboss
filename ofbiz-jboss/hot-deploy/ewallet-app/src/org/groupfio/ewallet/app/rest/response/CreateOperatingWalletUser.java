/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.util.Map;

import org.groupfio.ewallet.app.util.ParamUtil;

/**
 * @author Group Fio
 *
 */
public class CreateOperatingWalletUser extends Response {

	private String walletAcctId;
	private String userPartyId;
	private String statusCode;
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		setWalletAcctId( ParamUtil.getString(context, "walletAcctId") );
		setUserPartyId( ParamUtil.getString(context, "userPartyId") );
		setStatusCode( ParamUtil.getString(context, "partyStatus") );
		
	}

	public String getWalletAcctId() {
		return walletAcctId;
	}

	public void setWalletAcctId(String walletAcctId) {
		this.walletAcctId = walletAcctId;
	}

	public String getUserPartyId() {
		return userPartyId;
	}

	public void setUserPartyId(String userPartyId) {
		this.userPartyId = userPartyId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
}
