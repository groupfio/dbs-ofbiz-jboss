/**
 * 
 */
package org.groupfio.ewallet.app.rest.response;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.groupfio.ewallet.app.model.ServiceAnalysis;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Group Fio
 *
 */
public class GetWalletAPIAnalysisService extends Response {

	private String fromDate;
	private String thruDate;
	
	private List<ServiceAnalysis> entries = new ArrayList<ServiceAnalysis>();
	
	/* (non-Javadoc)
	 * @see org.groupfio.ewallet.app.rest.response.Response#doBuild(java.util.Map)
	 */
	@Override
	protected void doBuild(Map<String, Object> context) throws Exception {
		
		prepareContext(context);
		
		if (UtilValidate.isNotEmpty(context.get("entries"))) {
			Map<String, Map<String, Object>> entries = (Map<String, Map<String, Object>>) context.get("entries");
			for (String key : entries.keySet()) {
				Map<String, Object> entry = entries.get(key);
				entry.put("serviceName", key);
				this.entries.add( new ServiceAnalysis( entries.get(key) ) );
			}
		}
		
		setFromDate((String) context.get("fromDate"));
		setThruDate((String) context.get("thruDate"));
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getThruDate() {
		return thruDate;
	}

	public void setThruDate(String thruDate) {
		this.thruDate = thruDate;
	}

	public List<ServiceAnalysis> getEntries() {
		return entries;
	}

	public void setEntries(List<ServiceAnalysis> entries) {
		this.entries = entries;
	}
	
}
