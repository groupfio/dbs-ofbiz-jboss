package org.groupfio.ewallet.app.validator;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.groupfio.ewallet.app.ResponseCodes;
import org.groupfio.ewallet.app.constants.EMConstants;
import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
@SuppressWarnings("unchecked")
public class FundTransferDataValidator implements Validator {

    private static String MODULE = FundTransferDataValidator.class.getName();
    private boolean validate;

    /* (non-Javadoc)
     * @see org.groupfio.etl.process.validator.Validator#validate(java.util.Map)
     */
    @Override
    public Map<String, Object> validate(Map<String, Object> context) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> data = (Map<String, Object>) context.get("data");
        String errorSummary = null;
        try {
            setValidate(true);
            if (UtilValidate.isNotEmpty(data.get("txnInfo"))) {
                Map<String, Object> txnInfo = (Map<String, Object>) data.get("txnInfo");
                Delegator delegator = (Delegator) context.get("delegator");

                if (UtilValidate.isEmpty(txnInfo.get("walletTxnId"))) {
                    setValidate(false);
                    errorSummary = "E431";
                } else {
                    String walletTxnId = (String) txnInfo.get("walletTxnId");
                    //Check for length of the walletTxnId
                    if (walletTxnId.length() > 35) {
                        Debug.logError("walletTxnId contains more than 35 characters", MODULE);
                        setValidate(false);
                        errorSummary = "E123";
                    }
                }

                if (isValidate()) {
                    if (UtilValidate.isEmpty(txnInfo.get("txnType"))) {
                        setValidate(false);
                        errorSummary = "E232";
                    } else {
                        //TODO: Validate the txnType
                    }

                    if (isValidate()) {
                        if (UtilValidate.isEmpty(txnInfo.get("txnAmount"))) {
                            setValidate(false);
                            errorSummary = "E235";
                        } else {
                            BigDecimal txnAmount = new BigDecimal((String) txnInfo.get("txnAmount"));
                            if (UtilValidate.isNotEmpty(txnAmount)) {
                                String txnAmountStr = txnAmount.toString();
                                if ((txnAmount.compareTo(BigDecimal.ZERO) <= 0) || !txnAmountStr.matches("\\d+(\\.\\d{2})?")) {
                                    setValidate(false);
                                    errorSummary = "E403";
                                }
                            }
                        }

                        if (isValidate()) {
                            String txnDateStr = (String) txnInfo.get("txnDate");
                            if (UtilValidate.isEmpty(txnDateStr)) {
                                setValidate(false);
                                errorSummary = "E625";
                            } else {
                                try {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    dateFormat.setLenient(false);
                                    Date parsedDate = dateFormat.parse(txnDateStr);
                                    Timestamp txnDate = new java.sql.Timestamp(parsedDate.getTime());
                                    if (UtilValidate.isEmpty(txnDate)) {
                                        setValidate(false);
                                        errorSummary = "E626";
                                    }
                                } catch (ParseException px) {
                                    Debug.logError(px.getMessage(), MODULE);
                                    setValidate(false);
                                    errorSummary = "E626";
                                }
                            }

                            if (isValidate()) {
                                String txnCcy = (String) txnInfo.get("txnCcy");
                                if (UtilValidate.isEmpty(txnCcy)) {
                                    setValidate(false);
                                    errorSummary = "E233";
                                } else {
                                    GenericValue uom = EntityQuery.use(delegator).from("Uom").where("uomId", txnCcy).cache().queryOne();
                                    if (uom == null) {
                                        setValidate(false);
                                        errorSummary = "E234";
                                    }
                                }

                                if (isValidate()) {
                                    GenericValue walletAccountFrom = null;
                                    if (UtilValidate.isNotEmpty(txnInfo.get("senderParty"))) {
                                        Map<String, Object> senderParty = (Map<String, Object>) txnInfo.get("senderParty");
                                        String accountNo = (String) senderParty.get("accountNo");
                                        if (UtilValidate.isNotEmpty(accountNo)) {
                                            walletAccountFrom = WalletUtil.getActiveWalletAccount(delegator, accountNo); 
                                            if (UtilValidate.isEmpty(walletAccountFrom)) {
                                                setValidate(false);
                                                errorSummary = "E401";
                                            } else if (!WalletUtil.isWalletAccountTransactionable(walletAccountFrom, "WITHDRAWAL")) {
                                                setValidate(false);
                                                errorSummary = "E401";
                                            } else if (UtilValidate.isNotEmpty(txnInfo.get("txnAmount"))) {
                                                BigDecimal totalTransferableAmount = UtilValidate.isNotEmpty(walletAccountFrom.getBigDecimal("accountBalanceAmount")) ? walletAccountFrom.getBigDecimal("accountBalanceAmount") : new BigDecimal(0);
                                                BigDecimal txnAmount = new BigDecimal(txnInfo.get("txnAmount").toString());
                                                if (txnAmount.compareTo(totalTransferableAmount) > 0) {
                                                    setValidate(false);
                                                    errorSummary = "E403";
                                                }
                                            }
                                        } else {
                                            setValidate(false);
                                            errorSummary = "E436";
                                        }
                                        String senderPartyName = (String) senderParty.get("name");
                                        if (UtilValidate.isEmpty(senderPartyName)) {
                                            setValidate(false);
                                            errorSummary = "E432";
                                        } else {
                                            GenericValue party = EntityUtil.getFirst( delegator.findByAnd("PartyGroup", UtilMisc.toMap("groupName", senderPartyName), null, false));
                                            if (UtilValidate.isEmpty(party)) {
                                                setValidate(false);
                                                errorSummary = "E433";
                                            }
                                        }
                                    } else {
                                        setValidate(false);
                                        errorSummary = "E401";
                                    }

                                    if (isValidate()) {
                                        GenericValue walletAccountTo = null;
                                        if (UtilValidate.isNotEmpty(txnInfo.get("receivingParty"))) {
                                            Map<String, Object> receivingParty = (Map<String, Object>) txnInfo.get("receivingParty");
                                            String accountNo = (String) receivingParty.get("accountNo");
                                            if (UtilValidate.isNotEmpty(accountNo)) {
                                                walletAccountTo = WalletUtil.getActiveWalletAccount(delegator, accountNo); 
                                                if (UtilValidate.isEmpty(walletAccountTo)) {
                                                    setValidate(false);
                                                    errorSummary = "E402";
                                                } else if (!WalletUtil.isWalletAccountTransactionable(walletAccountTo, "TOP-UP")) {
                                                    setValidate(false);
                                                    errorSummary = "E402";
                                                } else if (UtilValidate.isNotEmpty(txnInfo.get("txnAmount"))) {
                                                    BigDecimal earmarkAmount = UtilValidate.isNotEmpty(walletAccountTo.getBigDecimal("earmarkAmount")) ? walletAccountTo.getBigDecimal("earmarkAmount") : new BigDecimal(0);
                                                    BigDecimal accountBalanceAmount = UtilValidate.isNotEmpty(walletAccountTo.getBigDecimal("accountBalanceAmount")) ? walletAccountTo.getBigDecimal("accountBalanceAmount") : new BigDecimal(0);
                                                    BigDecimal txnAmount = new BigDecimal(txnInfo.get("txnAmount").toString());
                                                    BigDecimal newBalance = accountBalanceAmount.add(earmarkAmount).add(txnAmount);
                                                    BigDecimal accountLimit = walletAccountTo.getBigDecimal("accountLimit");
                                                    if (newBalance.compareTo(accountLimit) > 0) {
                                                        setValidate(false);
                                                        errorSummary = "E403";
                                                    }
                                                }
                                            } else {
                                                setValidate(false);
                                                errorSummary = "E437";
                                            }
                                            String receivingPartyName = (String) receivingParty.get("name");
                                            if (UtilValidate.isEmpty(receivingPartyName)) {
                                                setValidate(false);
                                                errorSummary = "E434";
                                            } else {
                                                GenericValue party = EntityUtil.getFirst( delegator.findByAnd("PartyGroup", UtilMisc.toMap("groupName", receivingPartyName), null, false));
                                                if (UtilValidate.isEmpty(party)) {
                                                    setValidate(false);
                                                    errorSummary = "E435";
                                                }
                                            }
                                        } else {
                                            setValidate(false);
                                            errorSummary = "E402";
                                        }

                                        if (isValidate()) {
                                            if (UtilValidate.isNotEmpty(walletAccountFrom) && UtilValidate.isNotEmpty(walletAccountTo)) {
                                                if (UtilValidate.isNotEmpty(walletAccountFrom.getString("accountCurrencyUomId")) && UtilValidate.isNotEmpty(walletAccountTo.getString("accountCurrencyUomId"))) {
                                                    if (!walletAccountFrom.getString("accountCurrencyUomId").equals(walletAccountTo.getString("accountCurrencyUomId"))) {
                                                        setValidate(false);
                                                        errorSummary = "E421";
                                                    }
                                                    if (isValidate()) {
                                                        if (!walletAccountFrom.getString("accountCurrencyUomId").equals(txnCcy) || !walletAccountTo.getString("accountCurrencyUomId").equals(txnCcy)) {
                                                            setValidate(false);
                                                            errorSummary = "E422";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                setValidate(false);
                errorSummary = "E104";
            }
            if (!isValidate()) {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.BAD_REQUEST);
                response.put(EMConstants.RESPONSE_MESSAGE, errorSummary);
            } else {
                response.put(EMConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
            }
        } catch (Exception e) {
            Debug.log(e.getMessage(), MODULE);
            response.put(EMConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            response.put(EMConstants.RESPONSE_MESSAGE, "E900");
            return response;
        }
        return response;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}