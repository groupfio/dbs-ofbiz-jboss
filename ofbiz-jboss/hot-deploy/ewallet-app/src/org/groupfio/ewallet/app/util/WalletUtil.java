/**
 * 
 */
package org.groupfio.ewallet.app.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;

/**
 * @author Group Fio
 *
 */
public class WalletUtil {
	
	private static final String MODULE = WalletUtil.class.getName();
	
	public static GenericValue getActiveWalletAccount (Delegator delegator, String externalAccountId) {
		
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("externalAccountId", EntityOperator.EQUALS, externalAccountId),
					
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			
			GenericValue billingAccount = EntityUtil.getFirst( delegator.findList("BillingAccount", conditions, null, null, null, false) );
			
			if (UtilValidate.isNotEmpty(billingAccount)) {
				GenericValue billingAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, billingAccount.getString("billingAccountId"));
				if (UtilValidate.isNotEmpty(billingAccountRole)) {
					return billingAccount;
				}
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static GenericValue getActiveWalletAccountRole (Delegator delegator, String billingAccountId) {
		
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("billingAccountId", EntityOperator.EQUALS, billingAccountId),
					
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			
			GenericValue billingAccountRole = EntityUtil.getFirst( delegator.findList("BillingAccountRole", conditions, null, null, null, false) );
			return billingAccountRole;
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static List<GenericValue> getActiveWalletAccountRoles (Delegator delegator, String partyId, String roleTypeId) {
		
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
					EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, roleTypeId),
					
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			return delegator.findList("BillingAccountRole", conditions, null, null, null, false);
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
   	public static GenericValue getActiveWalletAccountRole (String billingAccountId, String roleTypeId, Delegator delegator) {
        try {
            EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
                    EntityCondition.makeCondition("billingAccountId", EntityOperator.EQUALS, billingAccountId),
                    EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, roleTypeId),
                    EntityCondition.makeConditionDate("fromDate", "thruDate")
            );
            GenericValue billingAccountRole = EntityUtil.getFirst( delegator.findList("BillingAccountRole", conditions, null, null, null, false) );
            return billingAccountRole;
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
        }
        return null;
    }

	public static String getExternalAccountId (Delegator delegator, String partyId) {
		
		try {
			if (UtilValidate.isNotEmpty(partyId)) {
				EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId),
						EntityCondition.makeConditionDate("fromDate", "thruDate")
						);
				
				GenericValue billingAccountRole = EntityUtil.getFirst( delegator.findList("BillingAccountRole", conditions, null, null, null, false) );
				
				if (UtilValidate.isNotEmpty(billingAccountRole)) {
					GenericValue billingAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", billingAccountRole.getString("billingAccountId")), false);
					return billingAccount.getString("externalAccountId");
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}
	
	public static List<GenericValue> getAllActiveOperatorWalletAccounts (Delegator delegator, String billingAccountId) {
		
		List<GenericValue> walletAccounts = new ArrayList<GenericValue>();
		
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("parentBillingAccountId", EntityOperator.EQUALS, billingAccountId),
					
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			
			List<GenericValue> roles = delegator.findList("BillingAccountRole", conditions, null, null, null, false);
			if (UtilValidate.isNotEmpty(roles)) {
				for (GenericValue role : roles) {
					
					GenericValue walletAccount = delegator.findOne("BillingAccount", UtilMisc.toMap("billingAccountId", role.getString("billingAccountId")), false);
					walletAccounts.add(walletAccount);
					
				}
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return walletAccounts;
	}

    public static GenericValue getActiveParentWalletAccount (Delegator delegator, String operatorPartyId, String currenyUomId) {
        try {
            GenericValue partyRelationship = EntityQuery.use(delegator).from("PartyRelationship").where("partyIdTo", operatorPartyId, "roleTypeIdTo", "OPERATING_ACCT_OWNER", "partyRelationshipTypeId", "WALLET_RELATION").filterByDate().queryFirst();
            if (UtilValidate.isNotEmpty(partyRelationship)) {
                List<GenericValue> parentBillingAccountRoles = EntityQuery.use(delegator).from("BillingAccountRole").where("partyId", partyRelationship.getString("partyIdFrom"), "roleTypeId", "MASTER_ACCT_OWNER").filterByDate().queryList();
                for (GenericValue parentBillingAccountRole : parentBillingAccountRoles) {
                    GenericValue parentBillingAccount = EntityQuery.use(delegator).from("BillingAccount").where("billingAccountId", parentBillingAccountRole.getString("billingAccountId"), "accountCurrencyUomId", currenyUomId).filterByDate().queryOne();
                    if (parentBillingAccount != null) {
                        return parentBillingAccount;
                    }
                }
            }
        } catch (Exception e) {
            Debug.logError(e.getMessage(), MODULE);
        }
        return null;
    }

    public static boolean isWalletAccountTransactionable (GenericValue walletAccount, String operationType) {
        if (walletAccount != null && walletAccount.size()>0 && UtilValidate.isNotEmpty(operationType)) {
            String statusId = walletAccount.getString("billingAccountStatusId");
            if ("WALLET_NEW".equals(statusId) || "WALLET_PENDING".equals(statusId) || "WALLET_REJECTED".equals(statusId)) {
                return false;
            }
            if (("EM_RELEASE".equals(operationType) || "EM_ADD".equals(operationType)) && "ACTIVE".equals(walletAccount.getString("operationTypeId"))) {
                return true;
            }
            if (UtilValidate.isNotEmpty(walletAccount.getString("freezeTypeId")) && UtilValidate.isNotEmpty(walletAccount.getString("operationTypeId"))) {
                if ("FREEZE".equals(walletAccount.getString("operationTypeId")) && "ONLY_DEBIT".equals(walletAccount.getString("freezeTypeId")) && ("WITHDRAWAL".equals(operationType) || "EM_ADD".equals(operationType) || "EM_RELEASE".equals(operationType))) {
                    return true;
                }
                if ("ACTIVE".equals(walletAccount.getString("operationTypeId")) && "CREDIT_DEBIT".equals(walletAccount.getString("freezeTypeId")) && ("TOP-UP".equals(operationType) || "WITHDRAWAL".equals(operationType) || "EM_RELEASE".equals(operationType) || "EM_ADD".equals(operationType))) {
                    return true;
                }
            }
        }
        return false;
    }

	public static GenericValue getActiveBillingAccount (Delegator delegator, String externalAccountId, Timestamp fromDate) {
		
		try {
			EntityCondition conditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("externalAccountId", EntityOperator.EQUALS, externalAccountId),
					EntityCondition.makeCondition("fromDate", EntityOperator.EQUALS, fromDate),
					EntityCondition.makeConditionDate("fromDate", "thruDate")
					);
			
			GenericValue billingAccount = EntityUtil.getFirst( delegator.findList("BillingAccount", conditions, null, null, null, false) );
			
			if (UtilValidate.isNotEmpty(billingAccount)) {
				return billingAccount;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			Debug.logError(e.getMessage(), MODULE);
		}
		return null;
	}

    public static List<String> getWalletContactMechListFromUserLogin (Delegator delegator, String userLoginId) {
        List<String> contactMechList = new ArrayList<String>();
        try {
            if (userLoginId != null) {
                GenericValue userRegion = EntityQuery.use(delegator).from("UserLoginAttribute").where("userLoginId", userLoginId, "attrName", "REGION").queryOne();
                if (userRegion != null) {
                    String attrValue = userRegion.getString("attrValue");
                    if (attrValue != null) {
                        String [] parts = attrValue.split("-");
                        if (parts != null) {
                            String userRegionId = parts[0];
                            //userRegionId is actually a Geo code of a country, we need to get geoId from it
                            GenericValue geo = EntityQuery.use(delegator).from("Geo").where("geoTypeId", "COUNTRY", "geoCode", userRegionId).queryFirst();
                            if (geo != null) {
                                //Keeping other values null because we are storing only geoId as the wallet country reference
                                List<GenericValue> postalAddresses = EntityQuery.use(delegator).from("PostalAddress").where("countryGeoId", geo.getString("geoId"), "address1", null, "address2", null, "city", null, "stateProvinceGeoId", null, "postalCode", null).queryList();
                                contactMechList = EntityUtil.getFieldListFromEntityList(postalAddresses, "contactMechId", true);
                            }
                        }
                    }
                }
            }
        } catch (GenericEntityException gee) {
            Debug.logError(gee.getMessage(), MODULE);
            return null;
        }
        return contactMechList;
    }
}
