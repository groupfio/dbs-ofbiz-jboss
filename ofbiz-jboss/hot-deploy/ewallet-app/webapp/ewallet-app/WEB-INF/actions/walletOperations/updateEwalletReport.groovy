import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastMap;
import javolution.util.FastList;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import com.jasper.reports.ReportData;
import com.jasper.reports.ReportGateway;

billingAccountList = [];
String externalAccountId = request.getParameter("externalAccountId");
String orgId = null;
if(UtilValidate.isNotEmpty(externalAccountId)){
    GenericValue billingAccount = EntityQuery.use(delegator).from("BillingAccount").where("externalAccountId", externalAccountId).queryOne();
    if(UtilValidate.isNotEmpty(billingAccount)){
        //EWALLET-5: Wallet Details should be displayed based on logged in LDAP user Country Access
        userLogin = context.get("userLogin");
        hasPermission = false;
        if (billingAccount.contactMechId != null) {
            if (userLogin != null) {
                contactMechList = WalletUtil.getWalletContactMechListFromUserLogin(delegator, userLogin.userLoginId);
                if (contactMechList != null && contactMechList.size > 0 && contactMechList.contains(billingAccount.contactMechId)) {
                    hasPermission = true;
                }
            }
        } else {
            hasPermission = true;
        }
        //EWALLET-5: End

        if (hasPermission) {
            if ("NA".equals(billingAccount.name)) {
                billingAccount.name = null;
            }
            context.put("billingAccount", billingAccount);
            billingAccountId = billingAccount.getString("billingAccountId");
            contactMechId = billingAccount.getString("contactMechId");
            if(UtilValidate.isNotEmpty(billingAccountId)){
                GenericValue billingAccountRoleAN = EntityQuery.use(delegator).from("BillingAccountRole").where("billingAccountId", billingAccountId).queryOne();
                if(UtilValidate.isNotEmpty(billingAccountRoleAN)){
                    partyId = billingAccountRoleAN.getString("partyId");
                    if(UtilValidate.isNotEmpty(partyId)){
                        partyGroupGv = delegator.findOne("PartyGroup",UtilMisc.toMap("partyId",partyId),false);
                        if(UtilValidate.isNotEmpty(partyGroupGv) && !"NA".equals(partyGroupGv.getString("groupName"))){
                            context.put("groupName", partyGroupGv.getString("groupName"));
                        }
                        partyIdentificationGv = delegator.findOne("PartyIdentification",UtilMisc.toMap("partyId",partyId,"partyIdentificationTypeId","ABC_EXT_REF"),false);
                        if(UtilValidate.isNotEmpty(partyIdentificationGv)){
                            context.put("idValue", partyIdentificationGv.getString("idValue"));
                        }
                        
                        partyAttributeGv = delegator.findOne("PartyAttribute",UtilMisc.toMap("partyId",partyId,"attrName","ORG_ID"),false);
                        if(UtilValidate.isNotEmpty(partyAttributeGv)){
                            context.put("orgId", partyAttributeGv.getString("attrValue"));
							orgId = partyAttributeGv.getString("attrValue");
                        }
                    }
                }

                GenericValue billingAccountRole = EntityQuery.use(delegator).from("BillingAccountRole").where("billingAccountId", billingAccountId,"roleTypeId","MASTER_ACCT_OWNER").queryOne();
                if(UtilValidate.isNotEmpty(billingAccountRole)){
                    Set<String> billingAccountIdSet = new HashSet<String>();
                    billingAccountRoleList = EntityQuery.use(delegator).from("BillingAccountRole").where("parentBillingAccountId",billingAccountId).queryList();
                    if(UtilValidate.isNotEmpty(billingAccountRoleList)){
                        for(GenericValue billingAccountRoleGv : billingAccountRoleList){
                            billingAccountIdSet.add(billingAccountRoleGv.getString("billingAccountId"));
                        }
                        EntityCondition condition = EntityCondition.makeCondition("billingAccountId", EntityOperator.IN,billingAccountIdSet);
                        billingAccountList = delegator.findList("BillingAccount", condition, null, UtilMisc.toList("createdStamp DESC"), null, false);
                        if(UtilValidate.isNotEmpty(billingAccountList))
                        context.put("billingAccountList", billingAccountList);
                    }
                    
                }

                if(UtilValidate.isNotEmpty(contactMechId)){
                    GenericValue contactMech = EntityQuery.use(delegator).from("ContactMech").where("contactMechId", contactMechId,"contactMechTypeId","POSTAL_ADDRESS").queryOne();
                    if(UtilValidate.isNotEmpty(contactMech)){
                        GenericValue postalAddress = EntityQuery.use(delegator).from("PostalAddress").where("contactMechId", contactMechId).queryOne();
                        if(UtilValidate.isNotEmpty(postalAddress)){
                            countryGeoId = postalAddress.getString("countryGeoId");
                            if(UtilValidate.isNotEmpty(countryGeoId)){
                                context.put("countryGeoId", countryGeoId);
                            }
                        }
                    }
                }
            }
        }
        context.hasPermission = hasPermission;
    }
}

/*operationTypeId = parameters.get("operationTypeId");
freezeTypeId = parameters.get("freezeTypeId");

String name="";
String description="";
String freezeTypeId ="";
String operationTypeId="";
String accountCreditLimit="";
String accountBalanceAmount ="";
String earmarkAmount ="";
String accountName ="";
String billingAccountId="";
String roleTypeId="";
String partyId="";
String groupName ="";

if(UtilValidate.isNotEmpty(operationTypeId)){
    context.put("operationTypeId", operationTypeId);
}
if(UtilValidate.isNotEmpty(freezeTypeId)){
    context.put("freezeTypeId", freezeTypeId);
}


List<Map<String, Object>> walletData = new FastList<Map<String, Object>>();
//if(UtilValidate.isNotEmpty(externalAccountId)){
    billingAccountLi = delegator.findByAnd("BillingAccount", UtilMisc.toMap("externalAccountId", externalAccountId), null, false);
    if(UtilValidate.isNotEmpty(billingAccountLi)){
        for(GenericValue billingAccountGV : billingAccountLi){
            Map<String, Object> walletMap = new FastMap<String, Object>();
            Map<String, Object> walletMasterMap = new FastMap<String, Object>();
            name = billingAccountGV.getString("name");
            description = billingAccountGV.getString("description");
            billingAccountId = billingAccountGV.getString("billingAccountId");
            freezeTypeId = billingAccountGV.getString("freezeTypeId");
            operationTypeId = billingAccountGV.getString("operationTypeId");
            accountBalanceAmount = billingAccountGV.getString("accountBalanceAmount");
            accountCreditLimit = billingAccountGV.getString("accountCreditLimit");
            earmarkAmount = billingAccountGV.getString("earmarkAmount");
            
            billingAccountRoleLi = delegator.findByAnd("BillingAccountRole",UtilMisc.toMap("billingAccountId",billingAccountId),null,false)
            if(UtilValidate.isNotEmpty(billingAccountRoleLi)){
                for(GenericValue billingAccountRoleGv : billingAccountRoleLi){
                    roleTypeId =billingAccountRoleGv.getString("roleTypeId");
                    partyId = billingAccountRoleGv.getString("partyId");
                }
            }
            partyGroupGv = delegator.findOne("PartyGroup",UtilMisc.toMap("partyId",partyId),false);
            if(UtilValidate.isNotEmpty(partyGroupGv)){
                groupName = partyGroupGv.getString("groupName");
            }
            walletMap.put("name",name);
            walletMap.put("billingAccountId",billingAccountId)
            walletMap.put("externalAccountId",externalAccountId);
            walletMap.put("description",description);
            walletMap.put("freezeTypeId",freezeTypeId);
            walletMap.put("operationTypeId",operationTypeId);
            walletMap.put("accountCreditLimit",accountCreditLimit);
            walletMap.put("accountBalanceAmount",accountBalanceAmount);
            walletMap.put("earmarkAmount",earmarkAmount);
            walletMap.put("groupName", groupName)
            walletMap.put("roleTypeId",roleTypeId);
            if(roleTypeId.equals("OPERATING_ACCT_OWNER")){
                walletData.add(walletMap);
            }else{
                
                walletData.add(walletMasterMap);
            }
            System.out.println("=====walletMap=="+walletMap);
            context.put("walletMap",walletMap)
            context.put("walletData",walletData);
        }
        
    }
    */
    
//}




//added bt m.vijayakumar for billing account list information
walletEnquiryList = [];
if(UtilValidate.isNotEmpty(billingAccountList)) {
	billingAccountList.each { billingAccount ->
		walletEnquiryMap = [:];
		//To get the orgId information
		billingAccountRole = from("BillingAccountRole").where("billingAccountId", billingAccount.billingAccountId).filterByDate().queryFirst();
		if (billingAccountRole) {
			partyAttribute = from("PartyAttribute").where("partyId", billingAccountRole.partyId, "attrName", "ORG_ID").queryOne();
			if (partyAttribute) {
				walletEnquiryMap.put("orgId", partyAttribute.getString("attrValue"));
			}
		}
		
		walletEnquiryMap.put("billingAccountStatusId", billingAccount.operationTypeId);
		walletEnquiryMap.put("walletNo", billingAccount.getString("externalAccountId"));
		walletEnquiryMap.put("walletAccountType", "Operating Account Owner");
		walletEnquiryMap.put("accountCurrencyUomId", billingAccount.getString("accountCurrencyUomId"));
		walletEnquiryMap.put("accountBalanceAmount", billingAccount.getBigDecimal("accountBalanceAmount")+"");
		walletEnquiryMap.put("earmarkAmount", billingAccount.getBigDecimal("earmarkAmount")+"");
		walletEnquiryList.add(walletEnquiryMap);
	}
}

serviceCtx = [:];
reportRegistryId = "WALLET_ENQUIRY";
downloadFormat = "PDF";

//println("The got pdf information is =======================-------------------- "+isPdf);
if (UtilValidate.isNotEmpty(reportRegistryId) && UtilValidate.isNotEmpty(downloadFormat)) {
	ReportData rData = new ReportData();
	Map jrParameters = [:];
	jrParameters.put("orgId", null);
	jrParameters.put("wallet", externalAccountId);
		
	
	
	GenericValue masterBillingAccount = EntityQuery.use(delegator).from("BillingAccount").where("externalAccountId", externalAccountId).queryOne();
	
	
	jrParameters.put("status", masterBillingAccount?masterBillingAccount.operationTypeId:null);
	rData.setReportRegistryId(reportRegistryId);
	rData.setDownloadFormat(downloadFormat)
	rData.setJrDataSource(walletEnquiryList);
	rData.setJrParmeters(jrParameters);
	String repotId = ReportGateway.generateReport(request, response,rData);
	context.reportId = repotId;
	request.setAttribute("reportId",repotId);
}
return "success";
//end @vijayakumar

