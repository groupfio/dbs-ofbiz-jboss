import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.GenericValue;
import com.jasper.reports.ReportData;
import com.jasper.reports.ReportGateway;

walletId = parameters.get("walletId");
walletName = parameters.get("walletName");
operationTypeId = parameters.get("operationTypeId");
freezeTypeId = parameters.get("freezeTypeId");
walletType = parameters.get("walletType");
billingAccountStatusId = parameters.get("billingAccountStatusId");
accountOwnerName = parameters.get("accountOwnerName");
accountOwnerId = parameters.get("accountOwnerId");

delegator = request.getAttribute("delegator");

conditionsList = [];

if(UtilValidate.isNotEmpty(walletId)){
    context.put("walletId", walletId);
    EntityCondition cond1=EntityCondition.makeCondition("externalAccountId",EntityOperator.EQUALS,walletId);
    conditionsList.add(cond1);
}
if(UtilValidate.isNotEmpty(walletName)){
    context.put("walletName", walletName);
    EntityCondition cond2 = EntityCondition.makeCondition("name", EntityOperator.LIKE, walletName+"%");
    conditionsList.add(cond2);
}
if(UtilValidate.isNotEmpty(operationTypeId)){
    context.put("operationTypeId", operationTypeId);
    EntityCondition cond3 = EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS,operationTypeId);
    conditionsList.add(cond3);
}
if(UtilValidate.isNotEmpty(freezeTypeId)){
    context.put("freezeTypeId", freezeTypeId);
    EntityCondition cond4 = EntityCondition.makeCondition("freezeTypeId", EntityOperator.EQUALS,freezeTypeId);
    conditionsList.add(cond4);
}

if(UtilValidate.isNotEmpty(walletType)){
    context.put("walletType", walletType);
    billingAccountRoleList1 =  delegator.findByAnd("BillingAccountRole",UtilMisc.toMap("roleTypeId",walletType),null,false);
    Set<String> billingAccountIdSet1 = new HashSet<String>();
    if(UtilValidate.isNotEmpty(billingAccountRoleList1)){
        for(GenericValue billingAccountRole1 : billingAccountRoleList1){
            billingAccountIdSet1.add(billingAccountRole1.getString("billingAccountId"));
        }
    }
    EntityCondition cond5 = EntityCondition.makeCondition("billingAccountId", EntityOperator.IN,billingAccountIdSet1);
    conditionsList.add(cond5);
}

if(UtilValidate.isNotEmpty(billingAccountStatusId)) {
    context.put("billingAccountStatusId", billingAccountStatusId);
    conditionsList.add( EntityCondition.makeCondition("billingAccountStatusId", EntityOperator.EQUALS, billingAccountStatusId));
}

if(UtilValidate.isNotEmpty(accountOwnerName)){
    context.put("accountOwnerName", accountOwnerName);
    partyGroupList =  delegator.findByAnd("PartyGroup",,UtilMisc.toMap("groupName",accountOwnerName),null,false);
    Set<String> billingAccountIdSet2 = new HashSet<String>();
    Set<String> partyIdSet = new HashSet<String>();
    if(UtilValidate.isNotEmpty(partyGroupList)){
        for(GenericValue partyGroup : partyGroupList){
            partyIdSet.add(partyGroup.getString("partyId"));
        }
        EntityCondition partyCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("partyId", EntityOperator.IN,partyIdSet),
                                         EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, ["MASTER_ACCT_OWNER","OPERATING_ACCT_OWNER"])],EntityOperator.AND);
        billingAccountRoleList2 = delegator.findList("BillingAccountRole", partyCondition, null, UtilMisc.toList("createdStamp DESC"), null, false);
        if(UtilValidate.isNotEmpty(billingAccountRoleList2)){
            for(GenericValue billingAccountRole2 : billingAccountRoleList2){
                billingAccountIdSet2.add(billingAccountRole2.getString("billingAccountId"));
            }
        }
    }
    EntityCondition cond6 = EntityCondition.makeCondition("billingAccountId", EntityOperator.IN,billingAccountIdSet2);
    conditionsList.add(cond6);
}

if(UtilValidate.isNotEmpty(accountOwnerId)){
    context.put("accountOwnerId", accountOwnerId);
    partyIdentificationList =  delegator.findByAnd("PartyIdentification",,UtilMisc.toMap("partyIdentificationTypeId","ABC_EXT_REF","idValue",accountOwnerId),null,false);
    Set<String> billingAccountIdSet3 = new HashSet<String>();
    Set<String> partyIdSet1 = new HashSet<String>();
    if(UtilValidate.isNotEmpty(partyIdentificationList)){
        for(GenericValue partyIdentification : partyIdentificationList){
            partyIdSet1.add(partyIdentification.getString("partyId"));
        }
        
        EntityCondition partyCondition1 = EntityCondition.makeCondition([EntityCondition.makeCondition("partyId", EntityOperator.IN,partyIdSet1),
            EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, ["MASTER_ACCT_OWNER","OPERATING_ACCT_OWNER"])],EntityOperator.AND);
        billingAccountRoleList3 = delegator.findList("BillingAccountRole", partyCondition1, null, UtilMisc.toList("createdStamp DESC"), null, false);
        if(UtilValidate.isNotEmpty(billingAccountRoleList3)){
            for(GenericValue billingAccountRole3 : billingAccountRoleList3){
                billingAccountIdSet3.add(billingAccountRole3.getString("billingAccountId"));
            }
        }
    }
    EntityCondition cond7 = EntityCondition.makeCondition("billingAccountId", EntityOperator.IN,billingAccountIdSet3);
    conditionsList.add(cond7);
}

//EWALLET-5: Wallet Details should be displayed based on logged in LDAP user Country Access
contactMechConditionList = []
userLogin = context.get("userLogin");
if (userLogin != null) {
    contactMechList = WalletUtil.getWalletContactMechListFromUserLogin(delegator, userLogin.userLoginId);
    if (contactMechList != null && contactMechList.size > 0) {
        contactMechConditionList.add(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, contactMechList));
    }
}
//This condition is required to display fresh wallets, as they do not have associated country yet
contactMechConditionList.add(EntityCondition.makeCondition("contactMechId", EntityOperator.EQUALS, null));
contactMechCondition = EntityCondition.makeCondition(contactMechConditionList,EntityOperator.OR);
//EWALLET-5: End

conditionsList.add(contactMechCondition);
conditionsList.add(EntityCondition.makeCondition("billingAccountTypeId", EntityOperator.EQUALS,"WALLET"));
EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList,EntityOperator.AND);
//println("=========mainConditons============"+mainConditons);
billingAccountList = delegator.findList("BillingAccount", mainConditons, null, UtilMisc.toList("createdStamp DESC"), null, false);
context.put("billingAccountList", billingAccountList);

//added bt m.vijayakumar for billing account list information
walletEnquiryList = [];
if(UtilValidate.isNotEmpty(billingAccountList)) {
    billingAccountList.each { billingAccount ->
        walletEnquiryMap = [:];
        //To get the orgId information
        billingAccountRole = from("BillingAccountRole").where("billingAccountId", billingAccount.billingAccountId).filterByDate().queryFirst();
        if (billingAccountRole) {
            partyAttribute = from("PartyAttribute").where("partyId", billingAccountRole.partyId, "attrName", "ORG_ID").queryOne();
            if (partyAttribute) {
                walletEnquiryMap.put("orgId", partyAttribute.attrValue);
            }
        }
        
        walletEnquiryMap.put("billingAccountStatusId", billingAccount.operationTypeId);
        walletEnquiryMap.put("walletNo", billingAccount.getString("externalAccountId"));
        walletEnquiryMap.put("walletAccountType", billingAccountRole?"Master Account Owner":"Operating Account Owner");
        walletEnquiryMap.put("accountCurrencyUomId", billingAccount.getString("accountCurrencyUomId"));
        walletEnquiryMap.put("accountBalanceAmount", billingAccount.getBigDecimal("accountBalanceAmount")+"");
        walletEnquiryMap.put("earmarkAmount", billingAccount.getBigDecimal("earmarkAmount")+"");
        walletEnquiryList.add(walletEnquiryMap);
    }
}

serviceCtx = [:];
reportRegistryId = request.getParameter("reportRegistryId");
downloadFormat = request.getParameter("downloadFormat");
isPdf = request.getParameter("isPdf");

//println("The got pdf information is =======================-------------------- "+isPdf);
if (UtilValidate.isNotEmpty(reportRegistryId) && UtilValidate.isNotEmpty(downloadFormat) && "Y".equals(isPdf)) {
    ReportData rData = new ReportData();
    Map jrParameters = [:];
    jrParameters.put("orgId", null);
    jrParameters.put("wallet", walletId);
    
    if (UtilValidate.isNotEmpty(billingAccountStatusId)) {
        billingAccountStatus = from("StatusItem").where("statusId", billingAccountStatusId).queryOne();
        if (billingAccountStatus) {
            billingAccountStatusId = billingAccountStatus.description;
        }
    }
    
    
    jrParameters.put("status", billingAccountStatusId);
    rData.setReportRegistryId(reportRegistryId);
    rData.setDownloadFormat(downloadFormat)
    rData.setJrDataSource(walletEnquiryList);
    rData.setJrParmeters(jrParameters);
    String repotId = ReportGateway.generateReport(request, response,rData);
    context.reportId = repotId;
    request.setAttribute("reportId",repotId);
}
return "success";
//end @vijayakumar