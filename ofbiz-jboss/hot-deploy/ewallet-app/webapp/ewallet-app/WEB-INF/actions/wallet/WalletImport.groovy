/**
 * @author Group Fio
 *
 */
import java.sql.BatchUpdateException;

import javolution.util.FastList

import org.ofbiz.entity.condition.*
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.groupfio.ewallet.app.util.DataHelper;
	
walletsError = 0;
//walletsNotProcessed = 0;
walletsImported = 0;

 /*
  GET ERROR BATCHES
  */
  errorBatchConditions = FastList.newInstance();
  errorBatchConditions.add(new EntityExpr("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED"));
  
  conditions = new EntityConditionList(errorBatchConditions, EntityOperator.OR);
  
  walletsError = delegator.findCountByCondition("DataImportWallet", conditions, null, null);
  
  errorBatchConditions = FastList.newInstance();
  errorBatchConditions.add(new EntityExpr("importStatusId", EntityOperator.EQUALS, "DATAIMP_NOT_PROC"));
  errorBatchConditions.add(new EntityExpr("importStatusId", EntityOperator.EQUALS, null));
  
  conditions = new EntityConditionList(errorBatchConditions, EntityOperator.OR);
  
  walletsNotProcessed = delegator.findCountByCondition("DataImportWallet", conditions, null, null);

walletsImported = 0;
importedWallets = from("DataImportWallet").where("importStatusId", "DATAIMP_IMPORTED").queryList();
if (importedWallets) {
    masterWallets = EntityUtil.getFieldListFromEntityList(importedWallets, "masterWalletNumber", true);
    if (masterWallets) {
        masterWalletsSet = new HashSet<>(masterWallets);
        walletsImported += masterWalletsSet.size();
    }
    operatingWallets = EntityUtil.getFieldListFromEntityList(importedWallets, "operatingWalletNumber", true);
    if (operatingWallets) {
        operatingWalletsSet = new HashSet<>(operatingWallets);
        walletsImported += operatingWalletsSet.size();
    }
}
context.put("walletsNotProcessed", walletsNotProcessed);
context.put("walletsError", walletsError);
context.put("walletsImported", walletsImported);

// Upload section [start]

modelTypes = UtilMisc.toMap("DataImportWallet", "Wallet Model");
context.put("modelTypes", modelTypes);

modelList = new HashMap();
uploadFilter = new HashMap();

modelType = request.getParameter("modelType");
if (UtilValidate.isNotEmpty(modelType)) {
    modelList = DataHelper.getWalletModelList(delegator, modelType);
}
uploadFilter.put("modelType", modelType);

modelId = request.getParameter("modelId");
if (UtilValidate.isNotEmpty(modelId)) {
}
uploadFilter.put("modelId", modelId);

context.put("modelList", modelList);
context.put("uploadFilter", uploadFilter);

// Upload section [end]


