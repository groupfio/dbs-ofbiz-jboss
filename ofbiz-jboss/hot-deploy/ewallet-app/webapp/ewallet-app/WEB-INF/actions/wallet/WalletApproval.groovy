import org.groupfio.ewallet.app.util.WalletUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;

//Wallet Details should be displayed based on logged in LDAP user Country Access
conditionsList = []
contactMechConditionList = [];
userLogin = context.get("userLogin");
if (userLogin != null) {
    conditionsList.add(EntityCondition.makeCondition("lastModifiedByUserLoginId", EntityOperator.NOT_EQUAL, userLogin.userLoginId));
    contactMechList = WalletUtil.getWalletContactMechListFromUserLogin(delegator, userLogin.userLoginId);
    if (contactMechList != null && contactMechList.size > 0) {
        contactMechConditionList.add(EntityCondition.makeCondition("contactMechId", EntityOperator.IN, contactMechList));
    }
}

//This condition is required to display fresh wallets, as they do not have associated country yet
contactMechConditionList.add(EntityCondition.makeCondition("contactMechId", EntityOperator.EQUALS, null));
contactMechCondition = EntityCondition.makeCondition(contactMechConditionList,EntityOperator.OR);

//Operation Type Condition List
operationTypeConditionList = [];
operationTypeConditionList.add(EntityCondition.makeCondition("operationTypeId", EntityOperator.NOT_EQUAL,"EXPIRED"));
operationTypeConditionList.add(EntityCondition.makeCondition("operationTypeId", EntityOperator.EQUALS, null));
operationTypeCondition = EntityCondition.makeCondition(operationTypeConditionList,EntityOperator.OR);

conditionsList.add(contactMechCondition);
conditionsList.add(operationTypeCondition);
conditionsList.add(EntityCondition.makeCondition("billingAccountTypeId", EntityOperator.EQUALS,"WALLET"));
conditionsList.add(EntityCondition.makeCondition("billingAccountStatusId", EntityOperator.EQUALS,"WALLET_PENDING"));
EntityCondition mainConditions = EntityCondition.makeCondition(conditionsList,EntityOperator.AND);
billingAccountList = delegator.findList("BillingAccount", mainConditions, null, UtilMisc.toList("createdStamp DESC"), null, false);
context.put("billingAccountList", billingAccountList);