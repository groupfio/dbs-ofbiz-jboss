import org.ofbiz.base.util.ObjectType;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.service.ServiceUtil;

import com.jasper.reports.ReportData
import com.jasper.reports.ReportGateway;
import javolution.util.FastMap

walletId = request.getParameter("walletId");
fromDate = request.getParameter("fromDate");
thruDate = request.getParameter("thruDate");

walletTxnList = [];
condition = null;
orgId = null;
if (walletId) {
    billingAccount = from("BillingAccount").where("externalAccountId", walletId).queryOne();
    if (billingAccount) {
        //Get Org Id
        billingAccountRole = from("BillingAccountRole").where("billingAccountId", billingAccount.billingAccountId).filterByDate().queryFirst();
        if (billingAccountRole != null) {
            if (billingAccountRole.partyId) {
                partyAttribute = from("PartyAttribute").where("partyId", billingAccountRole.partyId, "attrName", "ORG_ID").queryOne();
                if (partyAttribute) {
                    context.orgId = partyAttribute.attrValue;
                    orgId = partyAttribute.attrValue;
                }
            }
        }
        //Get Currency
        accountCurrencyUomId = billingAccount.accountCurrencyUomId;
        //Hardcoding opening balance to 0, as currently no information is gettig stored regarding this
        openingBalance = 0;

        serviceCtx = [:];
        serviceCtx.put("userLogin",context.userLogin);
        if (fromDate) {
            serviceCtx.put("fromDate", ObjectType.simpleTypeConvert(fromDate, "Timestamp", null, null));
        }
        if (thruDate) {
            serviceCtx.put("thruDate", ObjectType.simpleTypeConvert(thruDate, "Timestamp", null, null));
        }
        serviceCtx.put("externalAccountId", billingAccount.externalAccountId);
        serviceResult = dispatcher.runSync("ewallet.getWalletAccountTransEntries", serviceCtx);
        if (!ServiceUtil.isError(serviceResult)) {
            currentBalance = openingBalance;
            totalDebitAmount = 0;
            totalCreditAmount = 0;
            entries = serviceResult.entries;
            entries.each { entry ->
                if (UtilValidate.isEmpty(accountCurrencyUomId) || accountCurrencyUomId == entry.walletCurrency) {
                    walletTxn = [:];
                    walletTxn.customerRefId = entry.externalTxnId;
                    walletTxn.dateTime = entry.walletTransDate;
                    fromParty = null;
                    if (entry.walletAcctIdFrom != null) {
                        fromParty = entry.walletAcctIdFrom;
                    } else {
                        fromParty = entry.senderPartyId;
                    }
                    if ("TOPUP".equals(entry.txnStatusDescription)) {
                        walletTxn.description = "Received from "+fromParty;
                        walletTxn.creditAmount = entry.amount;
                        currentBalance = currentBalance + entry.amount
                        totalCreditAmount = totalCreditAmount + entry.amount;
                    } else if ("WITHDRAWL".equals(entry.txnStatusDescription)) {
                        walletTxn.description = "Transfer to "+fromParty;
                        walletTxn.debitAmount = entry.amount;
                        currentBalance = currentBalance - entry.amount;
                        totalDebitAmount = totalDebitAmount + entry.amount;
                    }
                    walletTxn.balance = currentBalance;
                    walletTxn.status = entry.txnStatus;
                    walletTxnList.add(walletTxn);
                }
            }
            context.totalDebitAmount = totalDebitAmount;
            context.totalCreditAmount = totalCreditAmount;
            context.currentBalance = currentBalance;
        }
        context.openingBalance = openingBalance;
        context.accountCurrencyUomId = accountCurrencyUomId;
    } else {
        context.errorMessage = "Invalid Wallet Id";
    }
} else {
    context.errorMessage = "Empty Wallet Id";
}
context.walletId = walletId;
context.fromDate = fromDate;
context.thruDate = thruDate;
context.walletTxnList = walletTxnList;

//println(walletTxnList+"--------------------------");

//vijayakumar:for transaction enquiry pdf report section date:16-02-2018
serviceCtx = FastMap.newInstance();
reportRegistryId = request.getParameter("reportRegistryId");
downloadFormat = request.getParameter("downloadFormat");
if (UtilValidate.isNotEmpty(reportRegistryId) && UtilValidate.isNotEmpty(downloadFormat) && "Y".equals(request.getParameter("isPdf"))) {
    ReportData rData = new ReportData();
    Map jrParameters = FastMap.newInstance();
    jrParameters.put("orgId", orgId);
    jrParameters.put("wallet", walletId);
    jrParameters.put("currency", accountCurrencyUomId);
    jrParameters.put("openingBalance", openingBalance);
    jrParameters.put("totalDebit", totalDebitAmount);
    jrParameters.put("totalCredit", totalCreditAmount);
    jrParameters.put("totalBalance", currentBalance);

    rData.setReportRegistryId(reportRegistryId);
    rData.setDownloadFormat(downloadFormat)
    rData.setJrDataSource(walletTxnList);
    rData.setJrParmeters(jrParameters);
    String repotId = ReportGateway.generateReport(request, response,rData);
    context.reportId = repotId;
    request.setAttribute("reportId",repotId);
}
return "success";
//end @vijayakumar