import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import org.groupfio.ewallet.app.util.DataHelper;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("EwalletAppUiLabels", locale);

serviceConfigId = request.getParameter("serviceConfigId");
if (UtilValidate.isNotEmpty(serviceConfigId)) {
	serviceConfig = EntityUtil.getFirst( delegator.findByAnd("ServiceConfig", UtilMisc.toMap("serviceConfigId", serviceConfigId), null, false) );
} else {
	serviceConfig = new HashMap();
}
context.put("serviceConfig", serviceConfig);

context.put("serviceNames", DataHelper.getServiceList(delegator));

yesnoOptions = UtilMisc.toMap("N", uiLabelMap.get("CommonNo"), "Y", uiLabelMap.get("CommonYes"));
context.put("yesnoOptions", yesnoOptions);