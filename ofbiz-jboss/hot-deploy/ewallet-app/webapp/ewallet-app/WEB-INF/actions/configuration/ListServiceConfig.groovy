import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("EwalletAppUiLabels", locale);

serviceConfigList = delegator.findByAnd("ServiceConfig", UtilMisc.toMap("componentName", "ewallet-app"), null, false);
context.put("serviceConfigList", serviceConfigList);