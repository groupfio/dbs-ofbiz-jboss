import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;

import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.util.EntityUtil;

delegator = request.getAttribute("delegator");

/*
mainCond = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLogin.getString("userLoginId"))
        			//EntityCondition.makeCondition("channelAccessType", EntityOperator.EQUALS, "DASHBOARD")
				);
*/
        	
apiLogList = delegator.findList("WalletApiLog", null, null, UtilMisc.toList("createdStamp DESC"), null, false);
context.put("apiLogList", apiLogList);
