import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;

import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.GenericValue;

import org.groupfio.ewallet.app.util.WalletUtil;
import java.util.ArrayList;

delegator = request.getAttribute("delegator");

entries = new ArrayList<Map<String, Object>>();

masterWalletNumbr = request.getParameter("masterWalletNumbr");
operatingWalletNumbr = request.getParameter("operatingWalletNumbr");
fromDateString = request.getParameter("fromDate");
thruDateString = request.getParameter("thruDate");

GenericValue walletAccount = null;

if (UtilValidate.isNotEmpty(operatingWalletNumbr)) {    		
	walletAccount = EntityUtil.getFirst( delegator.findByAnd("BillingAccount", UtilMisc.toMap("billingAccountId", operatingWalletNumbr), null, false) );
} else {
	walletAccount = EntityUtil.getFirst( delegator.findByAnd("BillingAccount", UtilMisc.toMap("billingAccountId", masterWalletNumbr), null, false) );
}

if (UtilValidate.isNotEmpty(walletAccount)) {
	
	GenericValue walletAccountRole = WalletUtil.getActiveWalletAccountRole(delegator, walletAccount.getString("billingAccountId"));
	String walletAccountPartyId = null;
	if (UtilValidate.isNotEmpty(walletAccountRole)) {
		walletAccountPartyId = walletAccountRole.getString("partyId");
	}
	
	if (UtilValidate.isNotEmpty(walletAccountPartyId)) {
		
		EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
				EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, walletAccountPartyId)
				//EntityUtil.getFilterByDateExpr("createdStamp", "createdStamp")
				);
		
		if (UtilValidate.isNotEmpty(fromDateString)) {
            fromDate = ObjectType.simpleTypeConvert(fromDateString, "Timestamp", null, null);
			condition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("effectiveDate", EntityOperator.NOT_EQUAL, null),
        			EntityCondition.makeCondition("effectiveDate", EntityOperator.GREATER_THAN_EQUAL_TO, fromDate),
                    condition);
		}
		if (UtilValidate.isNotEmpty(thruDateString)) {
            thruDate = ObjectType.simpleTypeConvert(thruDateString, "Timestamp", null, null);
			condition = EntityCondition.makeCondition(EntityOperator.AND,
        			EntityCondition.makeCondition("effectiveDate", EntityOperator.NOT_EQUAL, null),
        			EntityCondition.makeCondition("effectiveDate", EntityOperator.LESS_THAN_EQUAL_TO, thruDate),
                    condition);
		}
		
		List<GenericValue> payments = delegator.findList("Payment", condition, null, null, null, false);
		
		for (GenericValue payment : payments) {
			
			Map<String, Object> entry = new HashMap<String, Object>();
			
			entry.put("walletAcctIdFrom", WalletUtil.getExternalAccountId(delegator, payment.getString("partyIdFrom")));
			entry.put("walletAcctIdTo", WalletUtil.getExternalAccountId(delegator, payment.getString("partyIdTo")));
			entry.put("walletTxnId", payment.getString("paymentId"));
			entry.put("walletTransDate", payment.get("effectiveDate"));
			entry.put("senderPartyId", payment.get("partyIdFrom"));
			entry.put("receivingPartyId", payment.get("partyIdTo"));
			entry.put("walletCurrency", walletAccount.getString("accountCurrencyUomId"));
			entry.put("transactionTypeId", payment.get("transactionTypeId"));
			entry.put("operationType", payment.get("operationTypeId"));
			entry.put("amount", payment.get("amount"));
			entry.put("availableBalance", payment.get("availableBalance"));
			
			entries.add(entry);
		}
		
	}
	System.out.println("entries>>>>>> "+entries);
	context.put("entries", entries);
	
}

