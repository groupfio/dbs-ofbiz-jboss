import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;

import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.util.EntityUtil;

import org.groupfio.ewallet.app.util.WalletUtil;
import org.groupfio.ewallet.app.util.DataHelper;
import java.util.ArrayList;

delegator = request.getAttribute("delegator");

masterWallets = DataHelper.getActiveMasterWalletAccounts(delegator);
context.put("masterWallets", masterWallets);

operatorWallets = new HashMap();
transFilter = new HashMap();

masterWalletNumbr = request.getParameter("masterWalletNumbr");
if (UtilValidate.isNotEmpty(masterWalletNumbr)) {
    operatorWallets = DataHelper.getActiveOperatorWalletAccounts(delegator, masterWalletNumbr);
}
transFilter.put("masterWalletNumbr", masterWalletNumbr);

operatingWalletNumbr = request.getParameter("operatingWalletNumbr");
if (UtilValidate.isNotEmpty(operatingWalletNumbr)) {
}
transFilter.put("operatingWalletNumbr", operatingWalletNumbr);

fromDateString = request.getParameter("fromDate");
thruDateString = request.getParameter("thruDate");

transFilter.put("fromDate", fromDateString);
transFilter.put("thruDate", thruDateString);

context.put("operatorWallets", operatorWallets);
context.put("transFilter", transFilter);

