import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.entity.condition.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityConditionList;
import org.groupfio.ewallet.app.util.DataHelper;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("EwalletAppUiLabels", locale);

modelTypes = UtilMisc.toMap("DataImportWallet", "Wallet Model");
context.put("modelTypes", modelTypes);

modelList = new HashMap();


// search [start]

errorLogFilter = new HashMap();
conditions = FastList.newInstance();

modelType = request.getParameter("modelType");
if (UtilValidate.isNotEmpty(modelType)) {
    conditions.add(new EntityExpr("tableName", EntityOperator.EQUALS, modelType));
    
    modelList = DataHelper.getWalletModelList(delegator, modelType);
}
errorLogFilter.put("modelType", modelType);

modelId = request.getParameter("modelId");
if (UtilValidate.isNotEmpty(modelId)) {
	etlModel = EntityUtil.getFirst( delegator.findByAnd("EtlModel", UtilMisc.toMap("modelId", modelId), null, true) );
    conditions.add(new EntityExpr("listId", EntityOperator.EQUALS, etlModel.getString("modelName")));
}
errorLogFilter.put("modelId", modelId);

if (UtilValidate.isNotEmpty(conditions)) {

	conditionList = new EntityConditionList( conditions, EntityOperator.AND);
	errorLogs = delegator.findList("EtlLogProcError", conditionList, null, UtilMisc.toList("timeStamp DESC"), null, false);
	
	context.put("errorLogs", errorLogs);

} else {

	conditions = EntityCondition.makeCondition(UtilMisc.toList(
			EntityCondition.makeCondition("tableName", EntityOperator.EQUALS, "DataImportWallet")
   	), EntityOperator.OR);
	
	errorLogs = delegator.findList("EtlLogProcError", conditions, null, UtilMisc.toList("timeStamp DESC"), null, false);

	context.put("errorLogs", errorLogs);
	
}

// search [end]

context.put("modelList", modelList);
context.put("errorLogFilter", errorLogFilter);
