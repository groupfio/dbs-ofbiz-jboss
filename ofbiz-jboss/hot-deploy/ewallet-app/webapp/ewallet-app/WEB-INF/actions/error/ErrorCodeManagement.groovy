import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("EwalletAppUiLabels", locale);

errorCodeId = request.getParameter("errorCodeId");
if (UtilValidate.isNotEmpty(errorCodeId)) {
	errorCode = EntityUtil.getFirst( delegator.findByAnd("EwalletErrorCode", UtilMisc.toMap("errorCodeId", errorCodeId), null, false) );
} else {
	errorCode = new HashMap();
}
context.put("errorCode", errorCode);
/*
productTypeList = delegator.findAll("ProductType");
context.put("productTypeList", DataHelper.getDropDownOptions(productTypeList, "productTypeId", "description"));
*/
errorCodes = delegator.findAll("EwalletErrorCode", false);
context.put("errorCodes", errorCodes);

yesnoOptions = UtilMisc.toMap("N", uiLabelMap.get("CommonNo"), "Y", uiLabelMap.get("CommonYes"));
context.put("yesnoOptions", yesnoOptions);