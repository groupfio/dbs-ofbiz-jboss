/*Added by Rajasekar*/

import java.util.HashSet
import java.util.List
import java.util.Set

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.GenericValue
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;

userLogin = session.getAttribute("userLogin");
String requestURI = request.getRequestURI();
uiLabelMap = UtilProperties.getResourceBundleMap("EwalletAppUiLabels", locale);
List rows = new ArrayList();

/*if(UtilValidate.isNotEmpty(userLogin)){

	userLoginId = userLogin.getString("userLoginId");

	if(UtilValidate.isNotEmpty(userLoginId)){

		//Validate full admin permission

		String fullAdmin = "N";
		userLoginSecurityGrpCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, "FULLADMIN"),
			EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId)], EntityOperator.AND);
		userLoginSecurityGrp = from("UserLoginSecurityGroup").where(userLoginSecurityGrpCondition).filterByDate().queryFirst();

		if(UtilValidate.isNotEmpty(userLoginSecurityGrp)){
			fullAdmin = "Y";
		}
		context.put("fullAdmin",fullAdmin);

		userLogin = from("UserLogin").where("userLoginId",userLoginId).queryFirst();

		if(UtilValidate.isNotEmpty(userLogin))
			partyId = userLogin.getString("partyId");

		userLoginAttribute = from("UserLoginAttribute").where("userLoginId",userLoginId,"attrName","REGION").queryFirst();

		if(UtilValidate.isNotEmpty(userLoginAttribute)) {

			parentRoleTypeId = userLoginAttribute.getString("attrValue");

			if(UtilValidate.isNotEmpty(parentRoleTypeId)) {

				parentRole = from("RoleType").where("roleTypeId",parentRoleTypeId).queryFirst();

				if(UtilValidate.isNotEmpty(parentRole)) {

					roleType = from("RoleType").where("parentTypeId",parentRoleTypeId).queryList();

					if(UtilValidate.isNotEmpty(roleType)) {

						roleTypeIdList = EntityUtil.getFieldListFromEntityList(roleType, "roleTypeId", true);

						if(UtilValidate.isNotEmpty(roleTypeIdList) && UtilValidate.isNotEmpty(partyId)) {

							partyRoleCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, roleTypeIdList),
								EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId)], EntityOperator.AND);

							partyRole = from("PartyRole").where(partyRoleCondition).queryList();

							if(UtilValidate.isNotEmpty(partyRole)) {

								roleTypeIdList1 = EntityUtil.getFieldListFromEntityList(partyRole, "roleTypeId", true);

								securityGroupRoleTypeAssoc = from("SecurityGroupRoleTypeAssoc").where(EntityCondition.makeCondition("roleTypeId", EntityOperator.IN, roleTypeIdList1)).queryList();

								if(UtilValidate.isNotEmpty(securityGroupRoleTypeAssoc)) {

									groupIdList = EntityUtil.getFieldListFromEntityList(securityGroupRoleTypeAssoc, "groupId", true);

									userLoginSecurityGroupCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("groupId", EntityOperator.IN, groupIdList),
										EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId)], EntityOperator.AND);

									userLoginSecurityGroup = from("UserLoginSecurityGroup").where(userLoginSecurityGroupCondition).filterByDate().queryList();

									if(UtilValidate.isNotEmpty(userLoginSecurityGroup)){

										groupId = EntityUtil.getFieldListFromEntityList(userLoginSecurityGroup, "groupId", true);

										if(UtilValidate.isNotEmpty(groupId)) {

											securityGroupPermission = from("SecurityGroupPermission").where(EntityCondition.makeCondition("groupId", EntityOperator.IN, groupId)).queryList();

											if(UtilValidate.isNotEmpty(securityGroupPermission)) {

												permissionId = EntityUtil.getFieldListFromEntityList(securityGroupPermission, "permissionId", true);

												if(UtilValidate.isNotEmpty(permissionId)) {

													ofbizPageSecurityCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionId),
														EntityCondition.makeCondition("isExternal", EntityOperator.NOT_EQUAL, "Y")], EntityOperator.AND);

													ofbizPageSecurity = from("OfbizPageSecurity").where(ofbizPageSecurityCondition).orderBy("seqId").queryList();

													if(UtilValidate.isNotEmpty(permissionId)) {

														for(GenericValue ofbizPageSecurityGV: ofbizPageSecurity) {
															
															Map mp = new HashMap();
															uiLabelName = ofbizPageSecurityGV.getString("uiLabels");
															requestUri = ofbizPageSecurityGV.getString("requestUri");
															mp.put("uiLabelName",uiLabelMap.get(uiLabelName));
															mp.put("requestUri",requestUri);
															rows.add(mp);

														}

														context.put("ofbizPageSecurity", rows);

													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}*/

userLoginPermission = session.getAttribute("_USER_PERMISSION_ROLE_");
if(UtilValidate.isNotEmpty(userLogin)){
	
	userLoginId = userLogin.getString("userLoginId");
		
	if(UtilValidate.isNotEmpty(userLoginId)){
	    String fullAdmin = "N";
	    userLoginSecurityGrpCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")),
		EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId)], EntityOperator.AND);
	    userLoginSecurityGrp = from("UserLoginSecurityGroup").where(userLoginSecurityGrpCondition).filterByDate().queryFirst();

	    if(UtilValidate.isNotEmpty(userLoginSecurityGrp)){
		    fullAdmin = "Y";
	    }
	    context.put("fullAdmin",fullAdmin);
	    if(UtilValidate.isNotEmpty(userLoginPermission)) {
		
		    securityGroupRoleTypeAssoc = from("SecurityGroupRoleTypeAssoc").where(EntityCondition.makeCondition("roleTypeId", EntityOperator.EQUALS, userLoginPermission)).queryOne();
		    if(UtilValidate.isNotEmpty(securityGroupRoleTypeAssoc)) {
			    groupId = securityGroupRoleTypeAssoc.getString("groupId");
			    /*userLoginSecurityGroupCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupIdList),
											        EntityCondition.makeCondition("userLoginId", EntityOperator.EQUALS, userLoginId)], EntityOperator.AND);
			     userLoginSecurityGroup = from("UserLoginSecurityGroup").where(userLoginSecurityGroupCondition).filterByDate().queryList();
		
			     if(UtilValidate.isNotEmpty(userLoginSecurityGroup)){
		
				 groupId = EntityUtil.getFieldListFromEntityList(userLoginSecurityGroup, "groupId", true);*/
				 
				 if(UtilValidate.isNotEmpty(groupId)) {
					 securityGroupPermission = from("SecurityGroupPermission").where(EntityCondition.makeCondition("groupId", EntityOperator.EQUALS, groupId)).queryList();
					 if(UtilValidate.isNotEmpty(securityGroupPermission)) {
						   permissionId = EntityUtil.getFieldListFromEntityList(securityGroupPermission, "permissionId", true);
						   if(UtilValidate.isNotEmpty(permissionId)) {
								ofbizPageSecurityCondition = EntityCondition.makeCondition([EntityCondition.makeCondition("permissionId", EntityOperator.IN, permissionId),
															 EntityCondition.makeCondition("isExternal", EntityOperator.NOT_EQUAL, "Y")], EntityOperator.AND);
								ofbizPageSecurity = from("OfbizPageSecurity").where(ofbizPageSecurityCondition).orderBy("seqId").queryList();
								if(UtilValidate.isNotEmpty(permissionId)) {
									for(GenericValue ofbizPageSecurityGV: ofbizPageSecurity) {
										Map mp = new HashMap();
										uiLabelName = ofbizPageSecurityGV.getString("uiLabels");
										requestUri = ofbizPageSecurityGV.getString("requestUri");
										mp.put("uiLabelName",uiLabelMap.get(uiLabelName));
										mp.put("requestUri",requestUri);
										rows.add(mp);
									 }
									context.put("ofbizPageSecurity", rows);
							     }
							}
					  }
				  }
			/*}*/
		  }
	  }
	}
}
	
