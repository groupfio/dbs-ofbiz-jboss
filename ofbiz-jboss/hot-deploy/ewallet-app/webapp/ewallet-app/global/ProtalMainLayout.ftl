<#-- <@import location="component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl" /> -->
<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<!--<meta content="width=device-width" name="viewport">-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<title>eWallet: <#if title?has_content>${title}<#elseif titleProperty?has_content>${uiLabelMap.get(titleProperty)}</#if></title>
	
	<meta name="description" content="${pageDescriptionLabel!}" />
	<meta name="keywords" content="${keywordsLabel!}" />
	
	<#if layoutSettings.VT_SHORTCUT_ICON?has_content>
    	<#assign shortcutIcon = layoutSettings.VT_SHORTCUT_ICON.get(0)/>
  	<#elseif layoutSettings.shortcutIcon?has_content>
    	<#assign shortcutIcon = layoutSettings.shortcutIcon/>
  	</#if>
  	<#if shortcutIcon?has_content>
    	<link rel="shortcut icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />	
		<link rel="icon" type="image/x-icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />
  	</#if>

	<#if layoutSettings.styleSheets?has_content>
    	<#--layoutSettings.styleSheets is a list of style sheets. So, you can have a user-specified "main" style sheet, AND a component style sheet.-->
    	<#list layoutSettings.styleSheets as styleSheet>
      		<link rel="stylesheet" href="<@ofbizContentUrl>${StringUtil.wrapString(styleSheet)}</@ofbizContentUrl>" type="text/css"/>
    	</#list>
  	</#if>
  	<#if layoutSettings.VT_STYLESHEET?has_content>
    	<#list layoutSettings.VT_STYLESHEET as styleSheet>
      		<link rel="stylesheet" href="<@ofbizContentUrl>${StringUtil.wrapString(styleSheet)}</@ofbizContentUrl>" type="text/css"/>
    	</#list>
  	</#if>
  	<#if layoutSettings.rtlStyleSheets?has_content && langDir == "rtl">
    	<#--layoutSettings.rtlStyleSheets is a list of rtl style sheets.-->
    	<#list layoutSettings.rtlStyleSheets as styleSheet>
      		<link rel="stylesheet" href="<@ofbizContentUrl>${StringUtil.wrapString(styleSheet)}</@ofbizContentUrl>" type="text/css"/>
    	</#list>
  	</#if>
  	<#if layoutSettings.VT_RTL_STYLESHEET?has_content && langDir == "rtl">
    	<#list layoutSettings.VT_RTL_STYLESHEET as styleSheet>
      		<link rel="stylesheet" href="<@ofbizContentUrl>${StringUtil.wrapString(styleSheet)}</@ofbizContentUrl>" type="text/css"/>
    	</#list>
  	</#if>

	${layoutSettings.extraHead?if_exists}
  	<#if layoutSettings.VT_EXTRA_HEAD?has_content>
    	<#list layoutSettings.VT_EXTRA_HEAD as extraHead>
      		${extraHead}
    	</#list>
  	</#if>

	<!--<link type="text/css" href="/floral/css/bootstrap-combined.css" 	rel="stylesheet" />-->	

	<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">-->
	
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="/metronic/css/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/jqvmap.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/morris.css" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" type="text/css" href="/metronic/css/bootstrap-select.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/dataTables.scroller.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/dataTables.colReorder.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/multi-select.css"/>
	
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	
	<!-- BEGIN PAGE STYLES -->
	<!--<link href="/ea-resource/css/tasks.css" rel="stylesheet" type="text/css"/>-->
	<link rel="stylesheet" type="text/css" href="/metronic/css/bootstrap-datepicker3.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/bootstrap-timepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/jquery.notific8.min.css"/>
	<!-- END PAGE STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
	<link href="/metronic/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/plugins-md.css" rel="stylesheet" type="text/css"/>
	
	<#-- <link href="/metronic/css/layout.css" rel="stylesheet" type="text/css"/> 
	<link href="/metronic/css/light.css" rel="stylesheet" type="text/css" id="style_color"/>-->
	
	<link href="/metronic/css/error.css" rel="stylesheet" type="text/css"/>
	<link href="/metronic/css/custom.css" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	
	<link rel="stylesheet" type="text/css" href="/metronic/css/fancybox/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="/metronic/css/bootstrap-fileinput.css"/>
	
	<link href="/ea-resource/css/portfolio.css" rel="stylesheet" type="text/css"/>
	<!--<link href="/ea-resource/css/style.min.css" rel="stylesheet" type="text/css"/>-->
		
	<!--<script language="JavaScript" type="text/javascript" src="/images/prototypejs/prototype.js"></script>-->
	
	<#--layoutSettings.javaScripts is a list of java scripts. -->
    <#-- use a Set to make sure each javascript is declared only once, but iterate the list to maintain the correct order -->
	<#if layoutSettings.javaScripts?has_content>
    	<#assign javaScriptsSet = Static["org.ofbiz.base.util.UtilMisc"].toSet(layoutSettings.javaScripts)/>
    	<#list layoutSettings.javaScripts as javaScript>
      		<#if javaScriptsSet.contains(javaScript)>
        		<#assign nothing = javaScriptsSet.remove(javaScript)/>
        		<script type="text/javascript" src="<@ofbizContentUrl>${StringUtil.wrapString(javaScript)}</@ofbizContentUrl>"></script>
      		</#if>
    	</#list>
  	</#if>
	<#if layoutSettings.VT_HDR_JAVASCRIPT?has_content>
    	<#list layoutSettings.VT_HDR_JAVASCRIPT as javaScript>
      		<script type="text/javascript" src="<@ofbizContentUrl>${StringUtil.wrapString(javaScript)}</@ofbizContentUrl>"></script>
    	</#list>
  	</#if>
  	
  	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<script src="../../assets/global/plugins/respond.min.js"></script>
	<script src="../../assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<script src="/metronic/js/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="/metronic/js/jquery/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
	<script src="/metronic/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
	<script src="/metronic/js/bootstrap/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.cokie.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="/metronic/js/bootstrap/bootstrap-switch.min.js" type="text/javascript"></script>
	<script src="/metronic/js/bootstrap/validator.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="/metronic/js/bootstrap/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="/metronic/js/bootstrap/bootstrap-timepicker.min.js"></script>
	<script type="text/javascript" src="/metronic/js/clockface/clockface.js"></script>
	<script type="text/javascript" src="/metronic/js/bootstrap/moment.min.js"></script>
	<script type="text/javascript" src="/metronic/js/bootstrap/daterangepicker.js"></script>
	<script type="text/javascript" src="/metronic/js/bootstrap/bootstrap-datetimepicker.min.js"></script>	
	
	<script src="/metronic/js/jquery/jqvmap/jquery.vmap.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
	<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
	<script src="/metronic/js/morris/raphael-min.js" type="text/javascript"></script>
	<script src="/metronic/js/morris/morris.min.js" type="text/javascript"></script>
	<script src="/metronic/js/jquery/jquery.sparkline.min.js" type="text/javascript"></script>
		
	<script src="/metronic/js/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/serial.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/pie.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/radar.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/themes/light.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/themes/patterns.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/themes/chalk.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/ammap/worldLow.js" type="text/javascript"></script>
	
	<script src="/metronic/js/amcharts/ammap/ammap.js" type="text/javascript"></script>
	<script src="/metronic/js/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="/metronic/js/bootstrap/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="/metronic/js/select2/select2.min.js"></script>
	<script type="text/javascript" src="/metronic/js/jquery/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/metronic/js/dataTables.tableTools.min.js"></script>
	<script type="text/javascript" src="/metronic/js/dataTables.colReorder.min.js"></script>
	<script type="text/javascript" src="/metronic/js/dataTables.scroller.min.js"></script>
	<script type="text/javascript" src="/metronic/js/bootstrap/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="/metronic/js/jquery/jquery.multi-select.js"></script>
	<script type="text/javascript" src="/metronic/js/jquery/jquery.notific8.min.js"></script>
	<script type="text/javascript" src="/metronic/js/jquery/jquery.pulsate.min.js"></script>
	
	<script type="text/javascript" src="/metronic/js/jquery/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="/metronic/js/jquery/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/metronic/js/pace.min.js"></script>
	<script type="text/javascript" src="/metronic/js/bootbox/bootbox.min.js"></script>
	
	<#--
	-->
	
	<!-- END PAGE LEVEL PLUGINS -->
	
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="/metronic/js/metronic/datatable.js" type="text/javascript"></script>
	<script src="/metronic/js/metronic/metronic.js" type="text/javascript"></script>
	<script src="/metronic/js/layout.js" type="text/javascript"></script>
	<script src="/metronic/js/quick-sidebar.js" type="text/javascript"></script>
	<script src="/metronic/js/demo.js" type="text/javascript"></script>
	<script src="/metronic/js/bootstrap-fileinput.js" type="text/javascript"></script>
	
	<script src="/ea-resource/js/components-pickers.js"></script>
	<script src="/ea-resource/js/tasks.js" type="text/javascript"></script>
	<!--<script src="/ea-resource/js/charts-amcharts.js"></script>-->
	<script src="/ea-resource/js/bootstrap-confirmation.js" type="text/javascript"></script>
	
	<!-- END PAGE LEVEL SCRIPTS -->
	
	<script src="/metronic/js/custom.js" type="text/javascript"></script>
	
	<meta property="og:description" content="${pageDescriptionLabel!}">
	<meta property="og:title" content="<#if title?has_content>${title}<#elseif titleProperty?has_content>${uiLabelMap.get(titleProperty)}</#if>: ">
	<meta property="og:site_name" content="">
	<meta property="og:type" content="website">
	<#if og_image??>
		<meta property="og:image" content="<@fullUrlPath url=og_image />">
	</#if>

	<#--<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ttt_portal_images/apple-touch-icon.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ttt_portal_images/apple-touch-icon-144x144.png" />-->
	
	<#--><@includeAnalytics/>-->

</head>

<#assign uri=request.getRequestURI() />

<body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">

${sections.render("theme-header-content")}

${sections.render("header-common-content")}
	
${sections.render("header-main-content")}	
	
<!-- BEGIN CONTAINER -->
<div class="page-container">

	${sections.render("left-sidebar-content")}
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
	
		<div class="page-content" style="margin-left: 0px !important">
		
			${sections.render("top-content")}

			${sections.render("bottom-content")}
		
			${sections.render("extra-bottom-content")}
		
			${sections.render("body-end")}
		
		</div>
		
		${sections.render("quick-sidebar-content")}
	
	</div>
	<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->

${sections.render("footer-content")}

${sections.render("theme-footer-content")}

<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
jQuery(document).ready(function() {    
	
	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	   	
   	QuickSidebar.init(); // init quick sidebar
   	ComponentsPickers.init();
		
});
</script>
<!-- END JAVASCRIPTS -->

<#include "message.ftl"/>

<script src="/metronic/js/custom.js" type="text/javascript"></script>

</body>
</html>
