

<script type="text/javascript">
$(document).ready(function(){
	$("#TransactionEnquiry").attr('action','<@ofbizUrl>TransactionEnquiry</@ofbizUrl>');
});
function downloadReport()
{
	$("#isPdf").val("Y");
	$("#TransactionEnquiry").attr('action','<@ofbizUrl>TransactionEnquiryReport</@ofbizUrl>');
	$("#TransactionEnquiry").submit();
	$("#TransactionEnquiry").attr('action','<@ofbizUrl>TransactionEnquiry</@ofbizUrl>');
}

</script>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-building font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> ${uiLabelMap.TransactionDetails!}</span>
                </div>
                <div class="tools">
                	<#if walletTxnList?has_content>
                 	<button class="btn btn-xs btn-default" onclick='downloadReport()'>PDF</button>
                 	</#if>
                 	
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="fullscreen"></a>
                    
                </div>
            </div>
            <#if !errorMessage?exists>
                <div class="portlet-body form">
                    <div class="table-scrollable">
                        <table class="table table-hover" id="ca">
                            <thead>
                                <tr>
                                    <th>${uiLabelMap.SerialId}</th>
                                    <th>${uiLabelMap.CustomerRefId}</th>
                                    <th>${uiLabelMap.DateAndTime}</th>
                                    <th>${uiLabelMap.description}</th>
                                    <th class="text-right">${uiLabelMap.Debit}</th>
                                    <th class="text-right">${uiLabelMap.Credit}</th>
                                    <th class="text-center">${uiLabelMap.status}</th>
                                    <th class="text-right">${uiLabelMap.Balance}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <#list walletTxnList as walletTxn>
                                    <tr>
                                        <td>${walletTxn_index+1}</td>
                                        <td>${walletTxn.customerRefId!}</td>
                                        <td>${walletTxn.dateTime!}</td>
                                        <td>${walletTxn.description!}</td>
                                        <td class="text-right">${walletTxn.debitAmount!}</td>
                                        <td class="text-right">${walletTxn.creditAmount!}</td>
                                        <td class="text-center">${walletTxn.status!}</td>
                                        <td class="text-right">${walletTxn.balance!}</td>
                                    </tr>
                                </#list>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4"><strong>${uiLabelMap.TransactionSummary}</strong></td>
                                    <td class="text-right"><strong>${totalDebitAmount!}</strong></td>
                                    <td class="text-right"><strong>${totalCreditAmount!}</strong></td>
                                    <td></td>
                                    <td class="text-right"><strong>${currentBalance!}</strong></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            <#else>
                <div class="portlet-body">
                    <#if "Invalid Wallet Id" == errorMessage!>
                        ${uiLabelMap.WalletErrorInvalidWalletNo}
                    <#elseif "Empty Wallet Id" == errorMessage!>
                        ${uiLabelMap.WalletErrorEmptyWalletNo}
                    </#if>
                </div>
            </#if>
        </div>
    </div>
</div>

