<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<div class="page-head">
	<div class="page-title">
		<h1>Wallet <small>Import</small></h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<#-- <div class="caption font-green-haze">
					<i class="fa fa-building font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Lockbox Import</span>
				</div> -->
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<#--<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>-->
					<a href="javascript:;" class="fullscreen">
					</a>
					<#--<a href="javascript:;" class="remove">
					</a>-->
				</div>
			</div>
			<div class="portlet-body">
				
				<div class="row">
					<div class="col-md-6">
                    <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-bolt"></i> Importing </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-briefcase"></i> Imported </th>
                                <th>
                                    <i class="fa fa-bug"></i> Error Records </th>
                                <th>
                                    <i class="fa fa-bug"></i> ${uiLabelMap.notProcessed} </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">${uiLabelMap.Wallet!}</a>
                                </td>
                                <td> ${walletsImported} </td>
                                <td> ${walletsError} </td>
                                <td> ${walletsNotProcessed} </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                
				<div class="clearfix margin-bottom-20"></div>
				
				<div class="portlet light">
				<div class="portlet-title">
					<div class="caption font-green-haze">
						<i class="fa fa-building font-green-haze"></i>
						<span class="caption-subject bold uppercase"> ${uiLabelMap.uploadFile}</span>
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse">
						</a>
						<a href="javascript:;" class="fullscreen">
						</a>
					</div>
				</div>
				
				<div class="portlet-body">
					
					<#-- <form role="form" class="form-horizontal" action="<@ofbizUrl>uploadLockBoxFile</@ofbizUrl>" method="post" enctype="multipart/form-data" data-toggle="validator"> -->
					<form id="uploadFileForm" role="form" class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">
					
					<input type="hidden" name="modelName" id="modelName" value=""/>
					<input type="hidden" name="processId" id="processId" value=""/>
					
					<div class="form-body">
						
						<@dropdownInput 
							id="modelType"
							label=uiLabelMap.EtlModelType
							options=modelTypes
							value=uploadFilter.modelType
							allowEmpty=true
							required=true
							tooltip = uiLabelMap.EtlModelType
							/>
					
						<@dropdownInput 
							id="modelId"
							label=uiLabelMap.EtlModelName
							options=modelList
							value=uploadFilter.modelId
							allowEmpty=true
							required=true
							tooltip = uiLabelMap.EtlModelName
							/>
					
						<div class="row form-group form-md-line-input">
							<label class="col-md-2 control-label">${uiLabelMap.fileToImport}</label>
							<div class="col-md-10">
								
								<div class="row">
								
									<div class="col-sm-4">
					                  <div class="fileinput fileinput-new pull-left" data-provides="fileinput" >
					                    <div class="input-group input-group-sm">
					                      <div class="form-control uneditable-input input-fixed input-group-sm" data-trigger="fileinput">
					                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
					                        <span class="fileinput-filename" name="csvFile"> </span>
					                      </div>
					                      <span class="input-group-addon btn default btn-file">
					                      <span class="fileinput-new"> ${uiLabelMap.selectFile} </span>
					                      <span class="fileinput-exists"> ${uiLabelMap.change} </span>
					                      <input type="hidden"><input type="file" name="csv_fileName" id="csv_fileName_lst" required> </span>
					                      <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">  ${uiLabelMap.removeButton} </a>
					                    </div>
					                    <span class="error" id="file_name_error_csv"></span> 
					                  </div>
					                </div>
								
								</div>
								
								<div class="row">
									<div class="col-sm-4 control-label" style="text-align: left;">
										<label class="red">		
					                 		${uiLabelMap.pickRightFile}
					                  	</label>
									</div>
								</div>
								
							</div>
						</div>
					
					</div>
					
					<@fromCommonAction iconClass="fa fa-upload" showCancelBtn=false submitLabel=uiLabelMap.upload/>
					
					</form>
					
				</div>
				</div>
				
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
 	$( "#modelType" ).change(function() {
  		
  		$.ajax({
			      
			type: "POST",
	     	url: "getEtlModels",
	        data:  {"modelType": $("#modelType").val()},
	        success: function (returnedData) {   
	            
	            $('#modelId').html("");
	            
	            if (returnedData.code == 200 && returnedData.modelList.length !== 0) {
	            	
	            	var row = "<option value=''>Please Select</option>";
	            	
	            	for (var modelId in returnedData.modelList) {
	            		
	            		row += "<option value='"+modelId+"'>"+returnedData.modelList[modelId]+"</option>"
	            		
	            	}
	            	
	            }
	            
	            $('#modelId').html(row);
				    	
	        }
	        
		});
  		
	});
	  
	$("#modelId").change(function() {
  		
  		$("#modelName").val( $("#modelId option:selected").text() );
  		$("#processId").val( $("#modelId option:selected").text() + "_Process" );
  		
  		$("#uploadFileForm").attr("action", "<@ofbizUrl>uploadWalletFile</@ofbizUrl>?"+"modelName="+$("#modelName").val());
  		
	});
	
</script>

<script type="text/javascript">
	
	
 	
</script>