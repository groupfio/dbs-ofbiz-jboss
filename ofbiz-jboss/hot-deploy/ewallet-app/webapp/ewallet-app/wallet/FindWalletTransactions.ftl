<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-building font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> ${uiLabelMap.TransactionEnquiryReport}</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="fullscreen"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" class="form-horizontal" action="<@ofbizUrl>TransactionEnquiry</@ofbizUrl>" method="post" id="TransactionEnquiry">
                    <input type="hidden" name="reportRegistryId" value="TRANSACTION_ENQUIRY"/>
					<input type="hidden" name="downloadFormat" value="PDF"/>
					<input type="hidden" name="isPdf" id="isPdf" value="N"/>
					
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="walletId">${uiLabelMap.walletId}<span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-icon">
                                            <input type="text" name="walletId" id="walletId" class="form-control" value="${walletId!}" placeholder = "${uiLabelMap.walletId}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="orgId">${uiLabelMap.OrgIdOrCorpCode}</label>
                                    <div class="col-md-9">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" name="orgId" id="orgId" class="form-control" value="${orgId!}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="accountCurrencyUomId">${uiLabelMap.CommonCurrency}</label>
                                    <div class="col-md-9">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" name="accountCurrencyUomId" id="accountCurrencyUomId" class="form-control" value="${accountCurrencyUomId!}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transactionType">${uiLabelMap.TransactionType}</label>
                                    <div class="col-md-9">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" name="transactionType" id="transactionType" class="form-control" value="CREDIT/DEBIT" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="openingBalance">${uiLabelMap.OpeningBalance}</label>
                                    <div class="col-md-9">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" name="openingBalance" id="openingBalance" class="form-control" value="${openingBalance?default(0)}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <@dateInput
                                    id="fromDate"
                                    label=uiLabelMap.CommonFromDate
                                    value=fromDate
                                    />
                                <@dateInput
                                    id="thruDate"
                                    label=uiLabelMap.ToDate
                                    value=thruDate
                                    />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" id="submitBtn" disabled=disabled class="btn blue pull-right">${uiLabelMap.CommonFind}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .readOnlyField {
        background-color:#e2e1e1;
    }
</style>
<script>
    $("#walletId").keyup(function() {
        var walletId = $('#walletId').val();
        if (walletId == null || walletId == '') {
            $("#submitBtn").attr('disabled', 'disabled');
        } else {
            $("#submitBtn").removeAttr("disabled");
        }
    });
    $("#walletId").blur(function() {
        var walletId = $('#walletId').val();
        if (walletId == null || walletId == '') {
            $("#submitBtn").attr('disabled', 'disabled');
        } else {
            $("#submitBtn").removeAttr("disabled");
        }
    });
</script>