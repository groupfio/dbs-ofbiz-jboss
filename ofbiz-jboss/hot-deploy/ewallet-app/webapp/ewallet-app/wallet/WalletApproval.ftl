<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<style type="text/css">
    .dt-right {
        text-align : right;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-building font-green-haze"></i>
                    <span class="caption-subject bold uppercase">${uiLabelMap.ApprovalWalletsList}</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="fullscreen"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-scrollable">
                    <table class="table table-hover" id="ca">
                        <thead>
                            <tr>
                                <th>${uiLabelMap.SerialNo}</th>
                                <th>${uiLabelMap.walletId}</th>
                                <th>${uiLabelMap.walletName}</th>
                                <th>${uiLabelMap.Country}</th>
                                <th>${uiLabelMap.Currency}</th>
                                <th>${uiLabelMap.orgId}</th>
                                <th>${uiLabelMap.CreatedModifiedBy}</th>
                                <th>${uiLabelMap.CreatedModifiedAt}</th>
                                <th>${uiLabelMap.Action}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <#if billingAccountList?has_content>
                                <#list billingAccountList as billingAccount>
                                    <tr>
                                        <td>${billingAccount_index+1}</td>
                                        <td><a href="editEWallet?externalAccountId=${billingAccount.externalAccountId!}" class="btn btn-xs default tooltips" data-original-title="${uiLabelMap.edit!}" target="_blank">${billingAccount.externalAccountId!}</a></td>
                                        <td>${billingAccount.name!}</td>
                                        <#assign postalAddress = delegator.findOne("PostalAddress", Static["org.ofbiz.base.util.UtilMisc"].toMap("contactMechId", billingAccount.contactMechId!), true)?if_exists />
                                        <#if postalAddress?exists && postalAddress?has_content>
                                            <#assign country = delegator.findOne("Geo", Static["org.ofbiz.base.util.UtilMisc"].toMap("geoId", postalAddress.countryGeoId!), true) />
                                            <#if country?exists && country?has_content>
                                              <td>${country.geoName!}</td>
                                            <#else>
                                              <td>${postalAddress.countryGeoId!}</td>
                                            </#if>
                                        <#else>
                                            <td></td>
                                        </#if>
                                        <td>${billingAccount.accountCurrencyUomId!}</td>
                                        <#if billingAccount.billingAccountId?has_content>
                                            <#assign billingAccountRoleList = delegator.findByAnd("BillingAccountRole", Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId), Static["org.ofbiz.base.util.UtilMisc"].toList("-lastUpdatedStamp"),false)?if_exists/>
                                            <#if billingAccountRoleList?exists && billingAccountRoleList?has_content>
                                                <#assign partyId = billingAccountRoleList.get(0).get("partyId")?if_exists/>
                                                <#assign partyAttribute  = delegator.findOne("PartyAttribute",Static["org.ofbiz.base.util.UtilMisc"].toMap("partyId",partyId,"attrName","ORG_ID"), false)?if_exists/>
                                                <#if partyAttribute?exists && partyAttribute?has_content>
                                                    <td>${partyAttribute.attrValue?if_exists}</td>
                                                <#else>
                                                    <td></td>
                                                </#if>
                                            </#if>
                                        </#if>
                                        <#assign orderByList = Static["org.ofbiz.base.util.UtilMisc"].toList("-statusDatetime")/>
                                        <#assign billingAccountStatusHistoryList = delegator.findByAnd("BillingAccountStatusHistory",Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountStatusId", billingAccount.billingAccountStatusId!, "billingAccountId", billingAccount.billingAccountId!), orderByList, false)?if_exists/>
                                        <#if billingAccountStatusHistoryList?has_content>
                                            <#assign billingAccountStatusHistory = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(billingAccountStatusHistoryList) />
                                            <td>${billingAccountStatusHistory.statusUserLogin!}</td>
                                            <td>${billingAccountStatusHistory.statusDatetime!}</td>
                                        <#else>
                                            <td></td>
                                            <td></td>
                                        </#if>
                                        <#-- To Get the last status of the wallet before "Pending for Approval" -->
                                        <#assign billingAccountOldStatusId = null>
                                        <#assign billingAccountOldStatusHistoryList = delegator.findByAnd("BillingAccountStatusHistory",Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId!), orderByList, false)?if_exists/>
                                        <#list billingAccountOldStatusHistoryList as billingAccountOldStatusHistory>
                                            <#assign oldStatusUserLogin = billingAccountOldStatusHistory.statusUserLogin!>
                                            <#if billingAccountOldStatusHistory_index != 0 && billingAccountOldStatusId == null>
                                                <#--If the last status of wallet is not 'Pending for Approval' get the old status directly-->
                                                <#if "WALLET_PENDING" != billingAccountOldStatusHistory.billingAccountStatusId!>
                                                    <#assign billingAccountOldStatusId = billingAccountOldStatusHistory.billingAccountStatusId!>
                                                <#elseif userLogin.userLoginId! != oldStatusUserLogin>
                                                    <#assign billingAccountOldStatusId = billingAccountOldStatusHistory.billingAccountStatusId!>
                                                </#if>
                                            </#if>
                                        </#list>
                                        <td>
                                            <#if "WALLET_NEW" == billingAccountOldStatusId!>
                                                ${uiLabelMap.Add}
                                            <#elseif "WALLET_ACTIVE" == billingAccountOldStatusId! || "WALLET_FREEZE" == billingAccountOldStatusId!>
                                                ${uiLabelMap.Modify}
                                            <#elseif "WALLET_PENDING" == billingAccountOldStatusId!>
                                                ${uiLabelMap.Amend}
                                            </#if>
                                        </td>
                                    </tr>
                                </#list>
                            </#if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
     var TableAdvanced_ca = function () {
        var initTable2 = function () {
            var table = $('#ca');
            /* Formatting function for row details */
            function fnFormatDetails(oTable, nTr) {
                var aData = oTable.fnGetData(nTr);
                var sOut = '<table>';
                sOut += '<tr><td>${uiLabelMap.SerialNo}:</td><td>' + aData[1] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.walletId}:</td><td>' + aData[2] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.walletName}:</td><td>' + aData[3] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.Country}:</td><td>' + aData[4] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.Currency}:</td><td>' + aData[5] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.orgId}:</td><td>' + aData[6] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.CreatedModifiedBy}:</td><td>' + aData[7] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.CreatedModifiedAt}:</td><td>' + aData[8] + '</td></tr>';
                sOut += '<tr><td>${uiLabelMap.Action}:</td><td>' + aData[9] + '</td></tr>';
                sOut += '</table>';
                return sOut;
            }
            /*
             * Insert a 'details' column to the table
             */
            var nCloneTh = document.createElement('th');
            nCloneTh.className = "table-checkbox";
            var nCloneTd = document.createElement('td');
            nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';
            table.find('thead tr').each(function () {
                this.insertBefore(nCloneTh, this.childNodes[0]);
            });
            table.find('tbody tr').each(function () {
                this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
            });
            /* Set tabletools buttons and button container */
            $.extend(true, $.fn.DataTable.TableTools.classes, {
                "container": "btn-group tabletools-btn-group pull-right",
                "buttons": {
                    "normal": "btn btn-sm default",
                    "disabled": "btn btn-sm default disabled"
                }
            });
            var oTable = table.dataTable({
                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "Show _MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },
                "columnDefs": [
                    {
                        "orderable": false,
                        "targets": [0]
                    },
                ],
                "order": [
                    [0, 'asc']
                ],
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "pageLength": 10,
                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                "tableTools": {
                    "sSwfPath": "/metronic/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [{
                        "sExtends": "pdf",
                        "sButtonText": "PDF"
                    }, {
                        "sExtends": "csv",
                        "sButtonText": "CSV"
                    }, /*{
                        "sExtends": "xls",
                        "sButtonText": "Excel"
                    },*/ {
                        "sExtends": "print",
                        "sButtonText": "Print",
                        "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
                        "sMessage": "Generated by DataTables"
                    }, {
                        "sExtends": "copy",
                        "sButtonText": "Copy"
                    }]
                }
            });
            var tableWrapper = $('#ca_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
            var tableColumnToggler = $('#ca_column_toggler');
            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
            /* Add event listener for opening and closing details
             * Note that the indicator for showing which row is open is not controlled by DataTables,
             * rather it is done here
             */
            table.on('click', ' tbody td .row-details', function () {
                var nTr = $(this).parents('tr')[0];
                if (oTable.fnIsOpen(nTr)) {
                    /* This row is already open - close it */
                    $(this).addClass("row-details-close").removeClass("row-details-open");
                    oTable.fnClose(nTr);
                } else {
                    /* Open this row */
                    $(this).addClass("row-details-open").removeClass("row-details-close");
                    oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
                }
            });
            /* handle show/hide columns*/
            $('input[type="checkbox"]', tableColumnToggler).change(function () {
                /* Get the DataTables object again - this is not a recreation, just a get of the object */
                var iCol = parseInt($(this).attr("data-column"));
                iCol = iCol +1;
                var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
                oTable.fnSetColumnVis(iCol, (bVis ? false : true));
            });
        }
        return {
            //main function to initiate the module
            init: function () {
                if (!jQuery().dataTable) {
                    return;
                }
                initTable2();
            }
        };
    }();
    TableAdvanced_ca.init();
</script>