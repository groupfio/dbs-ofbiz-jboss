<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<#assign editMode = false>
<#assign submitLabel = "${uiLabelMap.Create}">
<#assign disabledKey = false>
<#if errorCode?exists && errorCode.errorCodeId?exists>
	<#assign editMode = true>
	<#assign submitLabel = "${uiLabelMap.Update}">
	<#assign disabledKey = true>
</#if>

<div class="page-head">
	<div class="page-title">
		<h1>${uiLabelMap.Error!} <small>${uiLabelMap.CodeManagement!}</small></h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-building font-green-haze"></i>
					<span class="caption-subject bold uppercase"> ${uiLabelMap.CreateUpdateCode!}</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<#--<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>-->
					<a href="javascript:;" class="fullscreen">
					</a>
					<#--<a href="javascript:;" class="remove">
					</a>-->
				</div>
			</div>
			<div class="portlet-body form">
				
				<form role="form" class="form-horizontal" action="<#if editMode><@ofbizUrl>updateErrorCode</@ofbizUrl><#else><@ofbizUrl>createErrorCode</@ofbizUrl></#if>" encType="multipart/form-data" method="post" data-toggle="validator">
					
					<input type="hidden" name="errorCodeId" value="${errorCode.errorCodeId!}"/>
					
					<div class="form-body">
					
						<@generalInput 
							id="code"
							label=uiLabelMap.code
							placeholder=uiLabelMap.code
							required=true
							value=errorCode.code
							tooltip = uiLabelMap.code
							/>
					
						<@textareaInput 
							id="codeDescription"
							label=uiLabelMap.codeDescription
							placeholder=uiLabelMap.codeDescription
							rows="3"
							value=errorCode.codeDescription
							required=true
							/>
							
						<@textareaInput 
							id="solutionDescription"
							label=uiLabelMap.solutionDescription
							placeholder=uiLabelMap.solutionDescription
							rows="3"
							value=errorCode.solutionDescription
							required=true
							/>	
					
						<@textareaInput 
							id="userRequirement"
							label=uiLabelMap.userRequirement
							placeholder=uiLabelMap.userRequirement
							rows="3"
							value=errorCode.userRequirement
							/>	
					
						<@dateInput 
							id="fromDate"
							label=uiLabelMap.CommonFromDate
							value=errorCode.fromDate
							/>
							
						<@dateInput 
							id="thruDate"
							label=uiLabelMap.CommonThruDate
							value=errorCode.thruDate
							/>			
					
						<@dropdownInput 
							id="inUse"
							label=uiLabelMap.inUse
							options=yesnoOptions
							required=true
							value=errorCode.inUse
							disabled = disabledKey
							allowEmpty=false
							tooltip = uiLabelMap.inUse
							/>
					
					</div>
					
					<@fromCommonAction iconClass="fa fa-check" showCancelBtn=false submitLabel=submitLabel/>
					
				</form>
				
				<div class="table-scrollable">
					<table class="table table-hover" id="error-code-grid">
					<thead>
					<tr>
						<th>#</th>
						<th>${uiLabelMap.errorCodeId!}</th>
						<th>${uiLabelMap.code!}</th>
						<th>${uiLabelMap.codeDescription!}</th>
						<th>${uiLabelMap.solutionDescription!}</th>
						<th>${uiLabelMap.userRequirement!}</th>
						<th>${uiLabelMap.fromDate!}</th>
						<th>${uiLabelMap.thruDate!}</th>
						<th>${uiLabelMap.inUse!}</th>
						<th class="text-center">${uiLabelMap.Action}</th>
					</tr>
					</thead>
					<tbody>
					
					<#if errorCodes?has_content>
						
					<#list errorCodes as ec>
					<tr>
						<td>${ec_index+1}</td>
						<td>${ec.errorCodeId!}</td>
						<td>${ec.code!}</td>
						<td>${ec.codeDescription!}</td>
						<td>${ec.solutionDescription!}</td>
						<td>${ec.userRequirement!}</td>
						<td>${ec.fromDate!}</td>
						<td>${ec.thruDate!}</td>
						<td>${ec.inUse!}</td>
						<td class="text-center">
							<div class="btn-group btn-group-solid">
								<a href="EditErrorCode?errorCodeId=${ec.errorCodeId}" class="btn btn-xs default tooltips" data-original-title="Edit"><i class="fa fa-pencil info"></i></a>
								<a class="btn btn-xs default tooltips confirm-message" href="deleteErrorCode?errorCodeId=${ec.errorCodeId}" data-original-title="Remove"><i class="fa fa-times red"></i></a>
							</div>
						</td>
					</tr>
					
					</#list>
						
					</#if>
					
					</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
	var securityReport = function () {
        var table = $('#error-code-grid');
		
        /* Set tabletools buttons and button container */
        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-btn-group pull-right",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            }
        });
		
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

			"columnDefs": [{
                "orderable": false,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],

            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "/metronic/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, /*{
                    "sExtends": "xls",
                    "sButtonText": "Excel"
                },*/ {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }, {
                    "sExtends": "copy",
                    "sButtonText": "Copy"
                }]
            }
        });

        var tableWrapper = $('#security_report_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = $('#security_report_column_toggler');
        
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        
        /* handle show/hide columns*/
        $('input[type="checkbox"]', tableColumnToggler).change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            iCol = iCol;
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
        
    }
    
    securityReport(); 	
	
</script>