<#-- Added by Rajasekar-->

<#-- <div class="page-head">
	<div class="page-title">
		<h1>${uiLabelMap.findWallets}</small></h1>
	</div>
</div>-->

<div class="row">
	<div class="col-md-12">
		
		<div class="portlet light">
		<div class="portlet-title">
			<div class="caption font-green-haze">
				<i class="fa fa-building font-green-haze"></i>
				<span class="caption-subject bold uppercase"> ${uiLabelMap.findWallets}</span>
			</div>
			<div class="tools">
				
				<#-- 
				<#if !lockboxBatch.importStatusId?exists || lockboxBatch.importStatusId != "LBIMP_IMPORTED">
				<button class="btn red-pink" data-toggle="confirmation" id="btn_discard_batch" data-popout="true" data-original-title="" title="Are you sure to Discard Batch ?">
				<i class="fa fa-times"></i>&nbsp;
				Discard Batch
				</button>
				</#if>
				 -->
				 
				<a href="javascript:;" class="collapse">
				</a>
				<a href="javascript:;" class="fullscreen">
				</a>
			</div>
		</div>
		
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>walletOperations</@ofbizUrl>" data-toggle="validator" method="POST" id="walletOperations">
				<input type="hidden" name="reportRegistryId" value="WALLET_ENQUIRY"/>
				<input type="hidden" name="downloadFormat" value="PDF"/>
				<input type="hidden" name="isPdf" id="isPdf" value="N"/>
																
			<div class="form-body">
			

			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="walletId">${uiLabelMap.walletId}</label>
				<div class="col-md-10">
					<div class="input-icon ">
						<input type="text" class="form-control input-md tooltips " value="${walletId?if_exists}" id="walletId" name="walletId" placeholder="${uiLabelMap.walletId}" data-original-title="${uiLabelMap.walletId}" autocomplete="off">
						<div class="form-control-focus">
						</div>
						<div class="help-block with-errors"></div>
							<i class=""></i>
					</div>
				</div>
			</div>
			
						
			
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="walletName">${uiLabelMap.walletName}</label>
				<div class="col-md-10">
					<div class="input-icon ">
						<input type="text" class="form-control input-md tooltips " value="${walletName?if_exists}" id="walletName" name="walletName" placeholder="${uiLabelMap.walletName}" data-original-title="${uiLabelMap.walletName}" autocomplete="off">
						<div class="form-control-focus">
						</div>
						<div class="help-block with-errors"></div>
							<i class=""></i>
					</div>
				</div>
			</div>
			
							
			
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="operationTypeId">${uiLabelMap.operationTypeId}</label>
				<div class="col-md-10">
					<div class="input-icon ">
					<select class="form-control tooltips" id="operationTypeId" name="operationTypeId" data-original-title="${uiLabelMap.operationTypeId}">
			  			    <option value="">Please Select</option>
			  				<option value="ACTIVE" <#if operationTypeId?exists && operationTypeId?has_content && operationTypeId=="ACTIVE">selected</#if>>ACTIVE</option>
			  				<option value="FREEZE" <#if operationTypeId?exists && operationTypeId?has_content && operationTypeId=="FREEZE">selected</#if>>FREEZE</option>
			  				<option value="EXPIRED" <#if operationTypeId?exists && operationTypeId?has_content && operationTypeId=="EXPIRED">selected</#if>>EXPIRED</option>
					</select>
					<div class="form-control-focus">
					</div>
					<span class="help-block with-errors"></span>
					</div>
				</div>
			</div>
			
						
			
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="freezeTypeId">${uiLabelMap.freezeTypeId}</label>
				<div class="col-md-10">
				<div class="input-icon ">
					<select class="form-control tooltips" id="freezeTypeId" name="freezeTypeId" data-original-title="${uiLabelMap.freezeTypeId}">
							<option value="">Please Select</option>
							<#if operationTypeId?exists && operationTypeId?has_content && operationTypeId=="FREEZE">
							 <option value="ONLY_DEBIT" <#if freezeTypeId?exists && freezeTypeId?has_content && freezeTypeId=="ONLY_DEBIT">selected</#if>>ONLY_DEBIT</option>
							 <option value="NO_OPERATION" <#if freezeTypeId?exists && freezeTypeId?has_content && freezeTypeId=="NO_OPERATION">selected</#if>>NO_OPERATION</option>
							<#elseif operationTypeId?exists && operationTypeId?has_content && operationTypeId=="EXPIRED">
			  				 <option value="NO_OPERATION" <#if freezeTypeId?exists && freezeTypeId?has_content && freezeTypeId=="NO_OPERATION">selected</#if>>NO_OPERATION</option>
			  				<#elseif operationTypeId?exists && operationTypeId?has_content && operationTypeId=="ACTIVE">
			  				<option value="CREDIT_DEBIT" <#if freezeTypeId?exists && freezeTypeId?has_content && freezeTypeId=="CREDIT_DEBIT">selected</#if>>CREDIT_DEBIT</option>
			  				</#if>
					</select>
					<div class="form-control-focus">
					</div>
					<span class="help-block with-errors"></span>
					</div>
				</div>
			</div>
			
									
			
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="walletType">${uiLabelMap.walletType}</label>
				<div class="col-md-10">
				<div class="input-icon ">
					<select class="form-control tooltips" id="walletType" name="walletType" data-original-title="${uiLabelMap.walletType}">
							<option value="">Please Select</option>
							<option value="MASTER_ACCT_OWNER" <#if walletType?exists && walletType?has_content && walletType=="MASTER_ACCT_OWNER">selected</#if>>Master Account Owner</option>
			  				<option value="OPERATING_ACCT_OWNER" <#if walletType?exists && walletType?has_content && walletType=="OPERATING_ACCT_OWNER">selected</#if>>Operating Account Owner</option>
					</select>
					<div class="form-control-focus">
					</div>
					<span class="help-block with-errors"></span>
					</div>
				</div>
			</div>

            <#assign walletStatusList = delegator.findByAnd("StatusItem", Static["org.ofbiz.base.util.UtilMisc"].toMap("statusTypeId", "WALLET_STATUS"), null, false)>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="billingAccountStatusId">${uiLabelMap.WalletStatus}</label>
                <div class="col-md-10">
                    <div class="input-icon ">
                        <select class="form-control tooltips" id="billingAccountStatusId" name="billingAccountStatusId" data-original-title="${uiLabelMap.WalletStatus}">
                            <option value="">${uiLabelMap.PleaseSelect}</option>
                            <#list walletStatusList as walletStatus>
                                <option value="${walletStatus.statusId!}" <#if billingAccountStatusId?exists && billingAccountStatusId?has_content && billingAccountStatusId==walletStatus.statusId!>selected</#if>>${walletStatus.description!}</option>
                            </#list>
                        </select>
                        <div class="form-control-focus">
                        </div>
                        <span class="help-block with-errors"></span>
                    </div>
                </div>
            </div>

			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="accountOwnerName">${uiLabelMap.accountName}</label>
				<div class="col-md-10">
					<div class="input-icon ">
						<input type="text" class="form-control input-md tooltips" value="${accountOwnerName?if_exists}" id="accountOwnerName" name="accountOwnerName" placeholder="${uiLabelMap.accountName}" data-original-title="${uiLabelMap.accountName}" autocomplete="off">
						<div class="form-control-focus">
						</div>
						<div class="help-block with-errors"></div>
							<i class=""></i>
					</div>
				</div>
			</div>
			
							
			
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label" for="accountOwnerId">${uiLabelMap.accountId}</label>
				<div class="col-md-10">
					<div class="input-icon ">
						<input type="text" class="form-control input-md tooltips " value="${accountOwnerId?if_exists}" id="accountOwnerId" name="accountOwnerId" placeholder="${uiLabelMap.accountId}" data-original-title="${uiLabelMap.accountId}" autocomplete="off">
						<div class="form-control-focus">
						</div>
						<div class="help-block with-errors"></div>
							<i class=""></i>
					</div>
				</div>
			</div>
			
																																																																																							
			</div>
			
			
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> ${uiLabelMap.find}</button>
					</div>
				</div>
			</div>
			
			<#-- <@fromCommonAction iconClass="fa fa-check" showCancelBtn=false submitLabel=uiLabelMap.CommonFind/> -->
			
		</form>			
		</div>
		</div>
			
	</div>
	
</div>

<script>
    $('#operationTypeId').change(function() {
        var operationTypeId = $(this).val();
        $("#freezeTypeId").empty();
        if(operationTypeId=="FREEZE"){
            $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="ONLY_DEBIT">ONLY_DEBIT</option><option value="NO_OPERATION">NO_OPERATION</option>');
        }
        if(operationTypeId=="ACTIVE"){
            $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="CREDIT_DEBIT">CREDIT_DEBIT</option>');
        }
        if(operationTypeId=="EXPIRED"){
            $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="NO_OPERATION">NO_OPERATION</option>');
        }
        if(operationTypeId=="" || operationTypeId==null){
            $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option>');
        }
    });
</script>
