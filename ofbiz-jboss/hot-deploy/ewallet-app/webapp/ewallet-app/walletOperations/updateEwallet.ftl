<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>
<#if billingAccount?has_content && billingAccount?exists>
    <style>
        .readOnlyField{
            background-color:#e2e1e1;
        }
    </style>
    <#assign amend = parameters.amend?default("N")>
    <#assign roleTypeId = session.getAttribute("_USER_PERMISSION_ROLE_")?default(null)>
    <#assign roleTypeHasEditPermission = Static["org.groupfio.ewallet.app.control.LoginWorker"].roleTypeHasPermission(roleTypeId, "EDIT-WALLET", delegator)/>
    <#assign roleTypeHasApprovePermission = Static["org.groupfio.ewallet.app.control.LoginWorker"].roleTypeHasPermission(roleTypeId, "APRV-RJCT-WALLET", delegator)/>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="fa fa-building font-green-haze"></i>
                        <span class="caption-subject bold uppercase">
                            <#if (amend != "Y" && roleTypeHasApprovePermission) || (!roleTypeHasApprovePermission && roleTypeHasEditPermission && "WALLET_PENDING"== billingAccount.billingAccountStatusId!) || "WALLET_REJECTED"== billingAccount.billingAccountStatusId!>
                                ${uiLabelMap.ViewWallet}
                            <#elseif roleTypeHasEditPermission && ("WALLET_NEW" == billingAccount.billingAccountStatusId! || "WALLET_ACTIVE" == billingAccount.billingAccountStatusId! || "WALLET_FREEZE" == billingAccount.billingAccountStatusId! || "WALLET_PENDING" == billingAccount.billingAccountStatusId!)>
                                ${uiLabelMap.editWallet}
                            </#if>
                        </span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="fullscreen"></a>
                   </div>
                </div>
                <div class="portlet-body form">
                    <#--amend != "Y" && roleTypeHasApprovePermission Logged in user is a makerchecker but not amending the wallet-->
                    <#--!roleTypeHasApprovePermission && roleTypeHasEditPermission && "WALLET_PENDING"== billingAccount.billingAccountStatusId! Logged in user is a maker but not the wallet already in 'Pending for Approval' status-->
                    <#--Wallet is rejected-->
                    <#if (amend != "Y" && roleTypeHasApprovePermission) || (!roleTypeHasApprovePermission && roleTypeHasEditPermission && "WALLET_PENDING"== billingAccount.billingAccountStatusId!) || "WALLET_REJECTED"== billingAccount.billingAccountStatusId!>
                        <#--View/Approve/Reject Wallet Section start-->
                        <form role="form" class="form-horizontal" action="" method="post">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletId">${uiLabelMap.walletId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips"  value="${billingAccount.externalAccountId?if_exists}" id="externalAccountId" name="externalAccountId" data-original-title="${uiLabelMap.walletId}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletName">${uiLabelMap.walletName}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " value="${billingAccount.name?if_exists}" id="walletName" name="walletName" data-original-title="${uiLabelMap.walletName}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletType">${uiLabelMap.walletType}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <#if billingAccount.billingAccountId?exists && billingAccount.billingAccountId?has_content>
                                                <#assign billingAccountRoleList = delegator.findByAnd("BillingAccountRole", Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId,"roleTypeId","MASTER_ACCT_OWNER"), Static["org.ofbiz.base.util.UtilMisc"].toList("-lastUpdatedStamp"),false)/>
                                                <#if billingAccountRoleList?exists && billingAccountRoleList?has_content>
                                                    <input type="text" class="form-control input-md tooltips " value="Master Account Owner" id="walletType" name="walletType" data-original-title="${uiLabelMap.walletType}" readonly>
                                                <#else>
                                                    <input type="text" class="form-control input-md tooltips " value="Operating Account Owner" id="walletType" name="walletType" data-original-title="${uiLabelMap.walletType}" readonly>
                                                </#if>
                                            </#if>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="operationTypeId">${uiLabelMap.operationTypeId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " value="${billingAccount.operationTypeId?if_exists}" id="operationTypeId" name="operationTypeId" data-original-title="${uiLabelMap.operationTypeId}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="freezeTypeId">${uiLabelMap.freezeTypeId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " value="${billingAccount.freezeTypeId?if_exists}" id="freezeTypeId" name="freezeTypeId" data-original-title="${uiLabelMap.freezeTypeId}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountName">${uiLabelMap.accountName}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " value="${groupName!}" id="accountName" name="accountName" data-original-title="${uiLabelMap.accountName}" readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="externalPartyId">${uiLabelMap.accountId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " value="${idValue!}" id="externalPartyId" name="externalPartyId" data-original-title="${uiLabelMap.accountId}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="orgId">${uiLabelMap.orgId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="orgId" value="${orgId!}" data-original-title="${uiLabelMap.orgId}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input has-">
                                    <label class="col-md-2 control-label" for="description">${uiLabelMap.description}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <textarea class="form-control " rows="3" id="description" name="description" value="${billingAccount.description?if_exists}" data-original-title="${uiLabelMap.description}" readonly>${billingAccount.description?if_exists}</textarea>
                                        </div>
                                    </div>
                                </div>
                                 <#assign currency = delegator.findOne("Uom", {"uomId" : billingAccount.accountCurrencyUomId!}, false)?if_exists>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountCurrencyUomId">${uiLabelMap.CommonCurrency}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <#if currency?has_content>
                                                <input type="text" class="form-control input-md tooltips" value="${currency.description!}" id="accountCurrencyUomId" name="accountCurrencyUomId" data-original-title="${uiLabelMap.CommonCurrency}" readonly>
                                            <#else>
                                                <input type="text" class="form-control input-md tooltips" value="" id="accountCurrencyUomId" name="accountCurrencyUomId" data-original-title="${uiLabelMap.CommonCurrency}" readonly>
                                            </#if>
                                        </div>
                                    </div>
                                </div>
                                <#assign country = delegator.findOne("Geo", {"geoId" : countryGeoId!}, false)?if_exists>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="countryGeoId">${uiLabelMap.Country}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <#if country?has_content>
                                                <input type="text" class="form-control input-md tooltips " value="${country.geoName!}" id="countryGeoId" name="countryGeoId" data-original-title="${uiLabelMap.Country}" readonly>
                                            <#else>
                                                <input type="text" class="form-control input-md tooltips " value="" id="countryGeoId" name="countryGeoId" data-original-title="${uiLabelMap.Country}" readonly>
                                            </#if>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountLimit">${uiLabelMap.maxLimitAllowed}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="accountLimit" name="accountLimit" value="${billingAccount.accountLimit?if_exists}" data-original-title="${uiLabelMap.maxLimitAllowed}" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountBalanceAmount">${uiLabelMap.accountBalanceAmount}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="accountBalanceAmount" name="accountBalanceAmount" value="${billingAccount.accountBalanceAmount?if_exists}" data-original-title="${uiLabelMap.accountBalanceAmount}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="earmarkAmount">${uiLabelMap.earmarkAmount}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="earmarkAmount" name="earmarkAmount" value="${billingAccount.earmarkAmount?if_exists}" data-original-title="${uiLabelMap.earmarkAmount}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="billingAccountStatusId">${uiLabelMap.WalletStatus}</label>
                                    <div class="col-md-10">
                                        <#assign walletStatusDesc = delegator.findOne("StatusItem", {"statusId" : billingAccount.billingAccountStatusId!}, true)?if_exists>
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips" id="billingAccountStatusId" value="${walletStatusDesc.description!}" data-original-title="${uiLabelMap.WalletStatus}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form role="form" name="approveWalletForm" id="approveWalletForm" class="form-horizontal" action="<@ofbizUrl>updateBillingAccountStatus</@ofbizUrl>" method="post">
                            <input type="hidden" name="billingAccountId" id="billingAccountId" value="${billingAccount.billingAccountId!}"/>
                            <input type="hidden" name="billingAccountStatusId" id="billingAccountStatusId" value="WALLET_${billingAccount.operationTypeId!}"/>
                            <input type="hidden" name="externalAccountId" id="billingAccountId" value="${billingAccount.externalAccountId!}"/>
                            <input type="hidden" name="orgId" id="orgId" value="${orgId!}"/>
                            <input type="hidden" name="externalPartyId" id="externalPartyId" value="${idValue!}"/>
                        </form>
                        <form role="form" name="rejectWalletForm" id="rejectWalletForm" class="form-horizontal" action="<@ofbizUrl>updateBillingAccountStatus</@ofbizUrl>" method="post">
                            <input type="hidden" name="billingAccountId" id="billingAccountId" value="${billingAccount.billingAccountId!}"/>
                            <input type="hidden" name="billingAccountStatusId" id="billingAccountStatusId" value="WALLET_REJECTED"/>
                            <input type="hidden" name="externalAccountId" id="billingAccountId" value="${billingAccount.externalAccountId!}"/>
                        </form>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <#if roleTypeHasApprovePermission && billingAccount.billingAccountStatusId?exists && "WALLET_REJECTED" != billingAccount.billingAccountStatusId!>
                                        <#assign orderByList = Static["org.ofbiz.base.util.UtilMisc"].toList("-statusDatetime")/>
                                        <#assign billingAccountStatusHistoryList = delegator.findByAnd("BillingAccountStatusHistory",Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountStatusId", billingAccount.billingAccountStatusId!, "billingAccountId", billingAccount.billingAccountId!), orderByList, false)?if_exists/>
                                        <#if billingAccountStatusHistoryList?has_content>
                                            <#assign billingAccountStatusHistory = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(billingAccountStatusHistoryList) />
                                            <#--If Logged user is MAKERCHECKER, it should be approved by another MAKERCHECKER. Since same logged in user can't approve his changes -->
                                            <#if billingAccountStatusHistory?has_content && userLogin?has_content && billingAccountStatusHistory.statusUserLogin! != userLogin.userLoginId!>
                                                <button type="submit" class="btn blue" form="approveWalletForm">${uiLabelMap.Approve}</button>
                                                <button type="submit" class="btn blue" form="rejectWalletForm">${uiLabelMap.Reject}</button>
                                                <a href="<@ofbizUrl>editEWallet?externalAccountId=${billingAccount.externalAccountId!}&amend=Y</@ofbizUrl>" class="btn blue">${uiLabelMap.Amend}</a>
                                            </#if>
                                        </#if>
                                    </#if>
                                    <#if roleTypeHasApprovePermission>
                                        <#--Redirecting MakerChecker to WalletApproval page-->
                                        <a href="<@ofbizUrl>WalletApproval</@ofbizUrl>" class="btn blue">${uiLabelMap.BackToApproval}</a>
                                    <#else>
                                        <#--Redirecting MakerChecker to walletOperations page-->
                                        <a href="<@ofbizUrl>walletOperations</@ofbizUrl>" class="btn blue">${uiLabelMap.Back}</a>
                                    </#if>
                                </div>
                            </div>
                        </div>
                        <#--View/Approve/Reject Wallet Section end-->
                    <#elseif roleTypeHasEditPermission && ("WALLET_NEW" == billingAccount.billingAccountStatusId! || "WALLET_ACTIVE" == billingAccount.billingAccountStatusId! || "WALLET_FREEZE" == billingAccount.billingAccountStatusId! || "WALLET_PENDING" == billingAccount.billingAccountStatusId!)>
                        <#--Edit Wallet Start-->
                        <form role="form" name="updateEwalletForm" id="updateEwalletForm" class="form-horizontal" action="<@ofbizUrl><#if amend == "Y" && roleTypeHasApprovePermission>updateWalletByMakerChecker<#else>updateWalletByMaker</#if></@ofbizUrl>" method="post">
                            <input type="hidden" name="billingAccountId" id="billingAccountId" value="${billingAccount.billingAccountId?if_exists}"/>
                            <input type="hidden" name="operationTypeIds" id="operationTypeIds" value="${billingAccount.operationTypeId?if_exists}"/>
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletId">${uiLabelMap.walletId}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips"  value="${billingAccount.externalAccountId?if_exists}" id="externalAccountId" name="externalAccountId" data-original-title="${uiLabelMap.walletId}" readonly>
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletName">${uiLabelMap.walletName}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <input type="text" class="form-control input-md tooltips " value="${billingAccount.name?if_exists}" id="walletName" name="walletName" data-original-title="${uiLabelMap.walletName}" autocomplete="off">
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="walletType">${uiLabelMap.walletType}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <#if billingAccount.billingAccountId?exists && billingAccount.billingAccountId?has_content>
                                                <#assign billingAccountRoleList = delegator.findByAnd("BillingAccountRole", Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId,"roleTypeId","MASTER_ACCT_OWNER"), Static["org.ofbiz.base.util.UtilMisc"].toList("-lastUpdatedStamp"),false)/>
                                                <#if billingAccountRoleList?exists && billingAccountRoleList?has_content>
                                                    <input type="text" class="form-control input-md tooltips " value="Master Account Owner" id="walletType" name="walletType" data-original-title="${uiLabelMap.walletType}" readonly>
                                                <#else>
                                                    <input type="text" class="form-control input-md tooltips " value="Operating Account Owner" id="walletType" name="walletType" data-original-title="${uiLabelMap.walletType}" readonly>
                                                </#if>
                                            </#if>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="operationTypeId">${uiLabelMap.operationTypeId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <select class="form-control tooltips" id="operationTypeId" name="operationTypeId" data-original-title="${uiLabelMap.operationTypeId}">
                                                <option value="">${uiLabelMap.PleaseSelect}</option>
                                                <option value="ACTIVE" <#if "ACTIVE" == billingAccount.operationTypeId!>selected</#if>>ACTIVE</option>
                                                <option value="FREEZE" <#if "FREEZE" == billingAccount.operationTypeId!>selected</#if>>FREEZE</option>
                                                <option value="EXPIRED" <#if "EXPIRED" == billingAccount.operationTypeId!>selected</#if>>EXPIRED</option>
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="freezeTypeId">${uiLabelMap.freezeTypeId}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <select class="form-control tooltips" id="freezeTypeId" name="freezeTypeId" data-original-title="${uiLabelMap.freezeTypeId}">
                                                <option value="">${uiLabelMap.PleaseSelect}</option>
                                                <#if "FREEZE" == billingAccount.operationTypeId!>
                                                    <option value="ONLY_DEBIT" <#if "ONLY_DEBIT" == billingAccount.freezeTypeId!>selected</#if>>ONLY_DEBIT</option>
                                                    <option value="NO_OPERATION" <#if "NO_OPERATION" == billingAccount.freezeTypeId!>selected</#if>>NO_OPERATION</option>
                                                <#elseif "EXPIRED" == billingAccount.operationTypeId!>
                                                    <option value="NO_OPERATION" <#if "NO_OPERATION" == billingAccount.freezeTypeId!>selected</#if>>NO_OPERATION</option>
                                                <#elseif "ACTIVE" == billingAccount.operationTypeId!>
                                                    <option value="CREDIT_DEBIT" <#if "CREDIT_DEBIT" == billingAccount.freezeTypeId!>selected</#if>>CREDIT_DEBIT</option>
                                                </#if> 
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountName">${uiLabelMap.accountName}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <input type="text" class="form-control input-md tooltips " value="${groupName?if_exists}" id="accountName" name="accountName" data-original-title="${uiLabelMap.accountName}" autocomplete="off">
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="externalPartyId">${uiLabelMap.accountId}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div <#if idValue?has_content>class="input-icon readOnlyField"<#else>class="input-icon"</#if>>
                                            <input type="text" class="form-control input-md tooltips " value="${idValue?if_exists}" id="externalPartyId" name="externalPartyId" data-original-title="${uiLabelMap.accountId}" <#if idValue?has_content>readonly</#if>>
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="orgId">${uiLabelMap.orgId}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <#if orgId?exists && orgId?has_content>
                                            <div class="input-icon readOnlyField">
                                                <input type="text" class="form-control input-md tooltips " id="orgId" value="${orgId?if_exists}" data-original-title="${uiLabelMap.orgId}" readonly>
                                            </div>
                                        <#else>
                                            <div class="input-icon">
                                                <input type="text" class="form-control input-md tooltips " maxlength="12" id="orgId" name="orgId" data-original-title="${uiLabelMap.orgId}">
                                            </div>
                                        </#if>
                                        <div class="form-control-focus"></div>
                                        <div class="help-block with-errors"></div>
                                        <i class=""></i>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input has-">
                                    <label class="col-md-2 control-label" for="description">${uiLabelMap.description}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <textarea class="form-control " rows="3" id="description" name="description" value="${billingAccount.description?if_exists}">${billingAccount.description?if_exists}</textarea>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <#assign orderByList = Static["org.ofbiz.base.util.UtilMisc"].toList("uomId")/>
                                <#assign currencies = delegator.findByAnd("Uom",Static["org.ofbiz.base.util.UtilMisc"].toMap("uomTypeId", "CURRENCY_MEASURE"), orderByList, false)/>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountCurrencyUomId">${uiLabelMap.CommonCurrency}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <select class="form-control tooltips" id="accountCurrencyUomId" name="accountCurrencyUomId" data-original-title="${uiLabelMap.CommonCurrency}">
                                                <option value="">${uiLabelMap.PleaseSelect}</option>
                                                <#list currencies as currency>
                                                    <option value="${currency.uomId!}" <#if billingAccount?has_content && billingAccount?exists && currency.uomId! == billingAccount.accountCurrencyUomId!>selected</#if>>${currency.description!}</option>
                                                </#list>
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <#assign orderByList = Static["org.ofbiz.base.util.UtilMisc"].toList("geoName")/>
                                <#assign countryList = delegator.findByAnd("Geo",Static["org.ofbiz.base.util.UtilMisc"].toMap("geoTypeId", "COUNTRY"), orderByList, false)/>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="countryGeoId">${uiLabelMap.Country}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <select class="form-control tooltips" id="countryGeoId" name="countryGeoId" data-original-title="${uiLabelMap.Country}">
                                                <option value="">${uiLabelMap.PleaseSelect}</option>
                                                <#list countryList as country>
                                                    <option value="${country.geoId!}" <#if countryGeoId?has_content && countryGeoId?exists && country.geoId! == countryGeoId!>selected</#if>>${country.geoName!}</option>
                                                </#list>
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountLimit">${uiLabelMap.maxLimitAllowed}<span class="text-danger">*</span></label>
                                    <div class="col-md-10">
                                        <div class="input-icon ">
                                            <input type="text" class="form-control input-md tooltips " id="accountLimit" name="accountLimit" value="${billingAccount.accountLimit?if_exists}" data-original-title="${uiLabelMap.maxLimitAllowed}" autocomplete="off">
                                            <div class="form-control-focus">
                                            </div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="accountBalanceAmount">${uiLabelMap.accountBalanceAmount}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="accountBalanceAmount" name="accountBalanceAmount" value="${billingAccount.accountBalanceAmount?if_exists}" data-original-title="${uiLabelMap.accountBalanceAmount}"  readonly>
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="earmarkAmount">${uiLabelMap.earmarkAmount}</label>
                                    <div class="col-md-10">
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips " id="earmarkAmount" name="earmarkAmount" value="${billingAccount.earmarkAmount?if_exists}" data-original-title="${uiLabelMap.earmarkAmount}" readonly>
                                            <div class="form-control-focus"></div>
                                            <div class="help-block with-errors"></div>
                                            <i class=""></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="billingAccountStatusIdDisplay">${uiLabelMap.WalletStatus}</label>
                                    <div class="col-md-10">
                                        <#assign walletStatusDesc = delegator.findOne("StatusItem", {"statusId" : billingAccount.billingAccountStatusId!}, true)?if_exists>
                                        <div class="input-icon readOnlyField">
                                            <input type="text" class="form-control input-md tooltips" id="billingAccountStatusIdDisplay" value="${walletStatusDesc.description!}" data-original-title="${uiLabelMap.WalletStatus}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <#if "EXPIRED" == billingAccount.operationTypeId!>
                                        <button type="submit" class="btn blue" form="updateEwalletForm" id="submit" disabled><i class="fa fa-check"></i>${uiLabelMap.Submit}</button>
                                    <#elseif billingAccount.name?has_content && billingAccount.accountCurrencyUomId?has_content && groupName?has_content && countryGeoId?has_content && orgId?has_content && billingAccount.accountLimit?has_content>
                                        <button type="submit" class="btn blue" form="updateEwalletForm" id="submit"><i class="fa fa-check"></i>${uiLabelMap.Submit}</button>
                                    <#else>
                                        <button type="submit" class="btn blue" form="updateEwalletForm" id="submit" disabled><i class="fa fa-check"></i>${uiLabelMap.Submit}</button>
                                    </#if>
                                    <#--<a href="<@ofbizUrl>editEWallet?externalAccountId=${billingAccount.externalAccountId!}</@ofbizUrl>" class="btn blue">${uiLabelMap.Cancel}</a>-->
                                    
                                    <a href="<@ofbizUrl>walletOperations</@ofbizUrl>" class="btn blue">${uiLabelMap.Cancel}</a>
                                    
                                </div>
                            </div>
                        </div>
                        <#--Edit Wallet End-->
                    </#if>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#operationTypeId').change(function() {
            var operationTypeId = $(this).val();
            $("#freezeTypeId").empty();
            if(operationTypeId=="FREEZE"){
                $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="ONLY_DEBIT">ONLY_DEBIT</option><option value="NO_OPERATION">NO_OPERATION</option>');
            }
            if(operationTypeId=="ACTIVE"){
                $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="CREDIT_DEBIT">CREDIT_DEBIT</option>');
            }
            if(operationTypeId=="EXPIRED"){
                $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option><option value="NO_OPERATION">NO_OPERATION</option>');
            }
            if(operationTypeId=="" || operationTypeId==null){
                $("#freezeTypeId").append('<option value="">${uiLabelMap.PleaseSelect}</option>');
            }
        });
        function ewalletSubmit(){
            var operationTypeId = $("#operationTypeIds").val();
            if(operationTypeId == "EXPIRED"){
                return false;
            }
        }
        $('#walletName, #accountLimit, #accountName, #orgId, #accountLimit').on("keyup", action);
        $('#accountCurrencyUomId, #countryGeoId').on("change", action);
        function action() {
            if( $('#walletName').val().length > 0 && $('#accountLimit').val().length > 0 && $('#accountName').val().length > 0 && $('#orgId').val().length > 0 && $('#accountLimit').val().length > 0 && $('#accountCurrencyUomId').val() != '' && $('#countryGeoId').val() != '') {
                $('#submit').prop("disabled", false);
            } else {
                $('#submit').prop("disabled", true);
            }
        }
    </script>
</#if>