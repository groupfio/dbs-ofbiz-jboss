<#-- Added by Rajasekar-->

<style type="text/css">
.dt-right{
text-align : right;
}
.walletAcct{
width: 110px;
}
</style>
<script>
$(document).ready(function(){
	$("#walletOperations").attr('action','<@ofbizUrl>walletOperations</@ofbizUrl>');
});
function downloadReport()
{
	$("#isPdf").val("Y");
	$("#walletOperations").attr('action','<@ofbizUrl>walletOperationsReport</@ofbizUrl>');
	$("#walletOperations").submit();
	$("#walletOperations").attr('action','<@ofbizUrl>walletOperations</@ofbizUrl>');
}
</script>

<#assign roleTypeId = session.getAttribute("_USER_PERMISSION_ROLE_")?default(null)>
<#assign roleTypeHasApprovePermission = Static["org.groupfio.ewallet.app.control.LoginWorker"].roleTypeHasPermission(roleTypeId, "APRV-RJCT-WALLET", delegator)/>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-building font-green-haze"></i>
					<span class="caption-subject bold uppercase"> ${uiLabelMap.listWallets!}</span>
				</div>
				<div class="tools">
					<#if billingAccountList?has_content>
						<button class="btn btn-xs btn-default" onclick='downloadReport()' id="">PDF</button>
					</#if>
					<a href="javascript:;" class="collapse">
					</a>
					<#--<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>-->
					
					<a href="javascript:;" class="fullscreen">
					</a>
					<#--<a href="javascript:;" class="remove">
					</a>-->
				</div>
			</div>
			<div class="portlet-body form">
				
				<div class="table-scrollable">
					<table class="table table-hover" id="ca">
					<thead>
					<tr>
						<th>${uiLabelMap.walletId!}</th>
						<th>${uiLabelMap.walletName!}</th>
						<th>${uiLabelMap.accountName!}</th>
						<th>${uiLabelMap.accountId!}</th>
						<th>${uiLabelMap.orgId!}</th>
						<th>${uiLabelMap.WalletStatus}</th>
						<th>${uiLabelMap.Country}</th>
						<th>${uiLabelMap.walletCurrencyId!}</th>
						<th>${uiLabelMap.accountBalanceAmount!}</th>
						<th>${uiLabelMap.earmarkAmount!}</th>
						<th>${uiLabelMap.maxLimitAllowed!}</th>
						<th>${uiLabelMap.operationTypeId!}</th>
						<th>${uiLabelMap.freezeTypeId!}</th>
						<th class="walletAcct">${uiLabelMap.walletAccountType!}</th>
						<th>${uiLabelMap.description!}</th>
						<th>${uiLabelMap.createdTxStamp!}</th>
						<th>${uiLabelMap.lastUpdatedTxStamp!}</th>
						<th>${uiLabelMap.fromDate!}</th>
						<th>${uiLabelMap.thruDate!}</th>
					</tr>
					</thead>
					<tbody>
					
					<#if billingAccountList?has_content>
						
					<#list billingAccountList as billingAccount>
					<tr>
                        <td>
                            <#if roleTypeHasApprovePermission>
                                <#if "WALLET_PENDING"== billingAccount.billingAccountStatusId!>
                                    ${billingAccount.externalAccountId!}
                                <#else>
                                    <a href="editEWallet?externalAccountId=${billingAccount.externalAccountId?if_exists}&amend=Y" class="btn btn-xs default tooltips" data-original-title="${uiLabelMap.edit!}" target="_blank">${billingAccount.externalAccountId?if_exists}</a>
                                </#if>
                            <#else>
                                <a href="editEWallet?externalAccountId=${billingAccount.externalAccountId?if_exists}" class="btn btn-xs default tooltips" data-original-title="${uiLabelMap.edit!}" target="_blank">${billingAccount.externalAccountId?if_exists}</a>
                            </#if>
                        </td>
						<td>
						  <#if billingAccount?exists && billingAccount?has_content && ("NA" != billingAccount.name!)>
						    ${billingAccount.name!}
						  </#if>
						</td>

						<#if billingAccount.billingAccountId?exists && billingAccount.billingAccountId?has_content>
							<#assign billingAccountRoleList = delegator.findByAnd("BillingAccountRole", Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId), Static["org.ofbiz.base.util.UtilMisc"].toList("-lastUpdatedStamp"),false)?if_exists/>
							<#if billingAccountRoleList?exists && billingAccountRoleList?has_content>
								<#assign partyId = billingAccountRoleList.get(0).get("partyId")?if_exists/>
								<#assign roleTypeId = billingAccountRoleList.get(0).get("roleTypeId")?if_exists/>
									<#if roleTypeId="MASTER_ACCT_OWNER" || roleTypeId="OPERATING_ACCT_OWNER">
									<#assign partyGroup  = delegator.findOne("PartyGroup",Static["org.ofbiz.base.util.UtilMisc"].toMap("partyId",partyId), false)?if_exists/>
									<#assign partyIdentification  = delegator.findOne("PartyIdentification",Static["org.ofbiz.base.util.UtilMisc"].toMap("partyId",partyId,"partyIdentificationTypeId","ABC_EXT_REF"), false)?if_exists/>
										<#if partyGroup?exists && partyGroup?has_content && ("NA" != partyGroup.groupName?if_exists)>
											<td>${partyGroup.groupName?if_exists}</td>
										<#else>
											<td></td>
										</#if>
											<#if partyIdentification?exists && partyIdentification?has_content>
												<td>${partyIdentification.idValue?if_exists}</td>
											<#else>
												<td></td>
											</#if>
										<#else>
											<td></td>
											<td></td>
									</#if>
									
									<#assign partyAttribute  = delegator.findOne("PartyAttribute",Static["org.ofbiz.base.util.UtilMisc"].toMap("partyId",partyId,"attrName","ORG_ID"), false)?if_exists/>
									<#if partyAttribute?exists && partyAttribute?has_content>
										<td>${partyAttribute.attrValue?if_exists}</td>
									<#else>
										<td></td>
									</#if>
							</#if>
						</#if>

                        <#assign walletStatusDesc = delegator.findOne("StatusItem", {"statusId" : billingAccount.billingAccountStatusId!}, true)?if_exists>
                        <#if walletStatusDesc?has_content>
                              <td>${walletStatusDesc.description!}</td>
                        <#else>
                            <td></td>
                        </#if>

                        <#assign postalAddress = delegator.findOne("PostalAddress", Static["org.ofbiz.base.util.UtilMisc"].toMap("contactMechId", billingAccount.contactMechId!), true)?if_exists />
                        <#if postalAddress?exists && postalAddress?has_content>
                            <#assign country = delegator.findOne("Geo", Static["org.ofbiz.base.util.UtilMisc"].toMap("geoId", postalAddress.countryGeoId!), true) />
                            <#if country?exists && country?has_content>
                              <td>${country.geoName!}</td>
                            <#else>
                              <td>${postalAddress.countryGeoId!}</td>
                            </#if>
                        <#else>
                            <td></td>
                        </#if>

						<td>${billingAccount.accountCurrencyUomId?if_exists}</td>
						<td>$${billingAccount.accountBalanceAmount?default(0)?if_exists}</td>
						<td>$${billingAccount.earmarkAmount?default(0)?if_exists}</td>
						<td>$${billingAccount.accountLimit?default(0)?if_exists}</td>
						<td>${billingAccount.operationTypeId?if_exists}</td>
						<td>${billingAccount.freezeTypeId?if_exists}</td>
						<#if billingAccount.billingAccountId?exists && billingAccount.billingAccountId?has_content>
						<#assign billingAccountRoleList = delegator.findByAnd("BillingAccountRole", Static["org.ofbiz.base.util.UtilMisc"].toMap("billingAccountId", billingAccount.billingAccountId,"roleTypeId","MASTER_ACCT_OWNER"), Static["org.ofbiz.base.util.UtilMisc"].toList("-lastUpdatedStamp"),false)?if_exists/>
						<#if billingAccountRoleList?exists && billingAccountRoleList?has_content>
						<td class="walletAcct">Master Account Owner</td>
						<#else>
						<td class="walletAcct">Operating Account Owner</td>
						</#if>
						</#if>
						<td>${billingAccount.description?if_exists}</td>
						<td>${billingAccount.createdTxStamp?if_exists}</td>
						<td>${billingAccount.lastUpdatedTxStamp?if_exists}</td>
						<td>${billingAccount.fromDate?if_exists}</td>
						<td>${billingAccount.thruDate?if_exists}</td>
					</tr>
					
					</#list>
						
					</#if>
					
					</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
 	var TableAdvanced_ca = function () {
		
		var initTable2 = function () {
	        var table = $('#ca');
	
			/* Formatting function for row details */
	        function fnFormatDetails(oTable, nTr) {
	            var aData = oTable.fnGetData(nTr);
	            var sOut = '<table>';
	            sOut += '<tr><td>${uiLabelMap.walletId!}:</td><td>' + aData[1] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.walletName!}:</td><td>' + aData[2] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.accountName!}:</td><td>' + aData[3] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.accountId!}:</td><td>' + aData[4] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.orgId!}:</td><td>' + aData[5] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.WalletStatus}:</td><td>' + aData[6] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.Country!}:</td><td>' + aData[7] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.walletCurrencyId!}:</td><td>' + aData[8] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.accountBalanceAmount!}:</td><td>' + aData[9] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.earmarkAmount!}:</td><td>' + aData[10] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.maxLimitAllowed!}:</td><td>' + aData[11] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.operationTypeId!}:</td><td>' + aData[12] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.freezeTypeId!}:</td><td>' + aData[13] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.walletAccountType!}:</td><td>' + aData[14] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.description!}:</td><td>' + aData[15] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.createdTxStamp!}:</td><td>' + aData[16] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.lastUpdatedTxStamp!}:</td><td>' + aData[17] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.fromDate!}:</td><td>' + aData[18] + '</td></tr>';
	            sOut += '<tr><td>${uiLabelMap.thruDate!}:</td><td>' + aData[19] + '</td></tr>';
	           	sOut += '</table>';
				
	            return sOut;
	        }
	
	        /*
	         * Insert a 'details' column to the table
	         */
	        var nCloneTh = document.createElement('th');
	        nCloneTh.className = "table-checkbox";
	
	        var nCloneTd = document.createElement('td');
	        nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';
	
	        table.find('thead tr').each(function () {
	            this.insertBefore(nCloneTh, this.childNodes[0]);
	        });
	
	        table.find('tbody tr').each(function () {
	            this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
	        });
	
	        /* Set tabletools buttons and button container */
	        $.extend(true, $.fn.DataTable.TableTools.classes, {
	            "container": "btn-group tabletools-btn-group pull-right",
	            "buttons": {
	                "normal": "btn btn-sm default",
	                "disabled": "btn btn-sm default disabled"
	            }
	        });
	
	        var oTable = table.dataTable({
	
	            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
	            "language": {
	                "aria": {
	                    "sortAscending": ": activate to sort column ascending",
	                    "sortDescending": ": activate to sort column descending"
	                },
	                "emptyTable": "No data available in table",
	                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
	                "infoEmpty": "No entries found",
	                "infoFiltered": "(filtered1 from _MAX_ total entries)",
	                "lengthMenu": "Show _MENU_ entries",
	                "search": "Search:",
	                "zeroRecords": "No matching records found"
	            },
	
				"columnDefs": [
					{
		                "orderable": false,
		                "targets": [0]
		            },
		            {
		                "orderable": false,
		                "visible": false,
		                "targets": [7,8,9,10,11,16,17,18,19]
		            },
		            {
		                "className": "dt-right",
		                "targets": [9,10,11]
		            },
		            
	            ],
	            "order": [
	                [0, 'asc']
	            ],
	            "lengthMenu": [
	                [5, 15, 20, -1],
	                [5, 15, 20, "All"] // change per page values here
	            ],
	
	            // set the initial value
	            "pageLength": 10,
	            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
	
	            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
	            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
	            // So when dropdowns used the scrollable div should be removed. 
	            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
	
	            "tableTools": {
	                "sSwfPath": "/metronic/swf/copy_csv_xls_pdf.swf",
	                "aButtons": [{
	                    "sExtends": "pdf",
	                    "sButtonText": "PDF"
	                }, {
	                    "sExtends": "csv",
	                    "sButtonText": "CSV"
	                }, /*{
	                    "sExtends": "xls",
	                    "sButtonText": "Excel"
	                },*/ {
	                    "sExtends": "print",
	                    "sButtonText": "Print",
	                    "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
	                    "sMessage": "Generated by DataTables"
	                }, {
	                    "sExtends": "copy",
	                    "sButtonText": "Copy"
	                }]
	            }
	        });
	
	        var tableWrapper = $('#ca_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
	        var tableColumnToggler = $('#ca_column_toggler');
	        
	        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
	        
	        /* Add event listener for opening and closing details
	         * Note that the indicator for showing which row is open is not controlled by DataTables,
	         * rather it is done here
	         */
	        table.on('click', ' tbody td .row-details', function () {
	            var nTr = $(this).parents('tr')[0];
	            if (oTable.fnIsOpen(nTr)) {
	                /* This row is already open - close it */
	                $(this).addClass("row-details-close").removeClass("row-details-open");
	                oTable.fnClose(nTr);
	            } else {
	                /* Open this row */
	                $(this).addClass("row-details-open").removeClass("row-details-close");
	                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
	            }
	        });
	        
	        /* handle show/hide columns*/
	        $('input[type="checkbox"]', tableColumnToggler).change(function () {
	            /* Get the DataTables object again - this is not a recreation, just a get of the object */
	            var iCol = parseInt($(this).attr("data-column"));
	            iCol = iCol +1;
	            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
	        });
	        
	    }
	    
		return {

	        //main function to initiate the module
	        init: function () {
	
	            if (!jQuery().dataTable) {
	                return;
	            }
	
	            //console.log('me 1');
	
	            initTable2();
	            
	            //console.log('me 2');
	        }
	
	    };
	
	}();
	
	TableAdvanced_ca.init();
	
</script>