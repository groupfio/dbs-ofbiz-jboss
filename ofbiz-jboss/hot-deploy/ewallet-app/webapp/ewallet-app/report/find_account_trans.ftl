<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-building font-green-haze"></i>
					<span class="caption-subject bold uppercase"> ${uiLabelMap.FindAccountTrans!}</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<#--<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>-->
					<a href="javascript:;" class="fullscreen">
					</a>
					<#--<a href="javascript:;" class="remove">
					</a>-->
				</div>
			</div>
			<div class="portlet-body form">
				
				<form role="form" class="form-horizontal" action="<@ofbizUrl>findAccountTrans</@ofbizUrl>" method="post">
					
					<div class="form-body">
					
						<@dropdownInput 
							id="masterWalletNumbr"
							label=uiLabelMap.masterWallet
							options=masterWallets
							value=transFilter.masterWalletNumbr
							allowEmpty=true
							required=true
							tooltip = uiLabelMap.masterWallet
							/>
					
						<@dropdownInput 
							id="operatingWalletNumbr"
							label=uiLabelMap.operatingWallet
							options=operatorWallets
							value=transFilter.operatingWalletNumbr
							allowEmpty=true
							required=false
							tooltip = uiLabelMap.operatingWallet
							/>
						<@dateInput
							id="fromDate"
							label=uiLabelMap.CommonFromDate
							value=transFilter.fromDate
							/>		
						<@dateInput
							id="thruDate"
							label=uiLabelMap.CommonThruDate
							value=transFilter.thruDate
							/>				
									
					</div>
					
					<@fromCommonAction iconClass="fa fa-check" showCancelBtn=false submitLabel=uiLabelMap.CommonFind/>
					
				</form>
				
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
 	$( "#masterWalletNumbr" ).change(function() {
  		
  		$.ajax({
			      
			type: "POST",
	     	url: "getOperatingWallets",
	        data:  {"masterWalletNumbr": $("#masterWalletNumbr").val()},
	        success: function (returnedData) {   
	            
	            $('#operatingWalletNumbr').html("");
	            
	            if (returnedData.code == 200 && returnedData.operatorList.length !== 0) {
	            	
	            	var row = "<option value=''>Please Select</option>";
	            	
	            	for (var operatorId in returnedData.operatorList) {
	            		
	            		row += "<option value='"+operatorId+"'>"+returnedData.operatorList[operatorId]+"</option>"
	            		
	            	}
	            	
	            }
	            
	            $('#operatingWalletNumbr').html(row);
				    	
	        }
	        
		});
  		
	});
	
</script>