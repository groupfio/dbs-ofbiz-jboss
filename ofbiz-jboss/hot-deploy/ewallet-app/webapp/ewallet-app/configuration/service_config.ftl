<#include "component://ewallet-app/webapp/ewallet-app/lib/portalContentMacros.ftl"/>

<#assign editMode = false>
<#assign submitLabel = "Create">
<#assign disabledKey = false>
<#if serviceConfig?exists && serviceConfig.serviceName?exists>
	<#assign editMode = true>
	<#assign submitLabel = "Update">
	<#assign disabledKey = true>
</#if>

<div class="page-head">
	<div class="page-title">
		<h1>${uiLabelMap.Service!} <small>${uiLabelMap.Configuration!}</small></h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-building font-green-haze"></i>
					<span class="caption-subject bold uppercase"> ${uiLabelMap.CreateUpdateServiceConfig!}</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<#--<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>-->
					<a href="javascript:;" class="fullscreen">
					</a>
					<#--<a href="javascript:;" class="remove">
					</a>-->
				</div>
			</div>
			<div class="portlet-body form">
				
				<form role="form" class="form-horizontal" action="<#if editMode><@ofbizUrl>updateServiceConfig</@ofbizUrl><#else><@ofbizUrl>createServiceConfig</@ofbizUrl></#if>" encType="multipart/form-data" method="post" data-toggle="validator">
					
					<input type="hidden" name="serviceConfigId" value="${serviceConfig.serviceConfigId!}"/>
					<input type="hidden" name="componentName" value="ewallet-app"/>
					
					<div class="form-body">
					
						<@dropdownInput 
							id="serviceName"
							label=uiLabelMap.serviceName
							options=serviceNames
							required=true
							value=serviceConfig.serviceName
							disabled = disabledKey
							allowEmpty=false
							tooltip = uiLabelMap.serviceName
							/>
					
						<@dropdownInput 
							id="isPublic"
							label=uiLabelMap.isPublic
							options=yesnoOptions
							required=true
							value=serviceConfig.isPublic
							allowEmpty=false
							tooltip = uiLabelMap.isPublic
							/>
							
					</div>
					
					<@fromCommonAction iconClass="fa fa-check" showCancelBtn=false submitLabel=uiLabelMap.Submit/>
					
				</form>
				
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
	
	
</script>