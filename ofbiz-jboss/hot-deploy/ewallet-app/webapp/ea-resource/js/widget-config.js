function updateSortablePortlets(data) {
	
	var areaDefault = "<div class='row mix-grid'>";
	areaDefault += "<div class='col-lg-6 col-md-6 col-sm-6'><div class='portlet portlet-sortable-empty'></div></div>" +
	"<div class='col-lg-6 col-md-6 col-sm-6'><div class='portlet portlet-sortable-empty'></div></div>";
	areaDefault += "</div>";
	
	$('#DropAreaDefault').html( areaDefault );
	$('#DropAreaLeft').html( "<div class='portlet portlet-sortable-empty'></div>" );
	
	$('#leftAreaIdlist').val( data.leftAreaIdlist );
	
	if (data.defaultContentBoxList.length > 0) {
		areaDefault = "<div class='row mix-grid'>";
		for (var i=0;i<data.defaultContentBoxList.length;i++) {
			var contentBox = data.defaultContentBoxList[i];
			
			var randomVal = Math.floor((Math.random() * 100) + 1);
			
			areaDefault 	+= "<div class='col-lg-6 col-md-6 col-sm-6 mix' data-order='"+randomVal+"'>"
							+	"<div class='portlet portlet-sortable light bg-inverse config-widget-portlet' id='wdg_"+contentBox.contentBoxId+"'>"
							+		"<div class='portlet-title'>" 	
							+			"<div class='caption'>"
							+				"<i class='icon-puzzle font-red-flamingo'></i>"
							+				"<span class='caption-subject bold font-red-flamingo uppercase'>"
							+				""+contentBox.header+" </span>"
							+			"</div>"
							+			"<div class='tools'>"
							+				"<a href='' class='collapse'></a>"
							//+				"<a href='' class='fullscreen'></a>"
							+				"<a class='fancybox-button tooltips' href='"+contentBox.previewImageUrl+"' data-original-title='Preview' data-rel='fancybox-button'>"
							+					"<i class='fa fa-search'></i>"	
							+				"</a>"
							+			"</div>"
							+		"</div>"	
							+		"<div class='portlet-body'>"
							+			"<div class='thumbnail'>"
							+				"<img alt='' src='"+contentBox.previewImageUrl+"'>";
			
			if(contentBox.description != "") {
				areaDefault += 				"<div class='caption'>"
							+				"<p>"+contentBox.description+"</p>"
							+				"</div>";
			}	
											
			areaDefault +=				"</div>"	
							+		"</div>"	
							+	"</div>"	
							+ "</div>";		
						
		}
		
		areaDefault += "<div class='col-lg-6 col-md-6 col-sm-6'><div class='portlet portlet-sortable-empty'></div></div>" +
				"<div class='col-lg-6 col-md-6 col-sm-6'><div class='portlet portlet-sortable-empty'></div></div>";
		
		areaDefault += "</div>";
		
		//areaDefault += "<div class='portlet portlet-sortable-empty'></div>";
		
		$('#DropAreaDefault').html( areaDefault );
		
		$('.mix-grid').mixItUp({
		    animation: {
		        effects: 'fade rotateZ(-180deg)',
		        duration: 700
		    }
		});
		$('.mix-grid').mixItUp('sort', 'order : asc', true);
		
	}
	
	if (data.boxConfigList.length > 0) {
		var areaLeft = "";
		for (var i=0;i<data.boxConfigList.length;i++) {
			var contentBox = data.boxConfigList[i].contentBox;
			
			var randomVal = Math.floor((Math.random() * 100) + 1);
			
			areaLeft 		+= "<div class='portlet portlet-sortable light bg-inverse config-widget-portlet mix' data-order='"+randomVal+"' id='wdg_"+contentBox.contentBoxId+"'>"
							+		"<div class='portlet-title'>" 	
							+			"<div class='caption'>"
							+				"<i class='icon-puzzle font-red-flamingo'></i>"
							+				"<span class='caption-subject bold font-red-flamingo uppercase'>"
							+				""+contentBox.header+" </span>"
							+			"</div>"
							+			"<div class='tools'>"
							+				"<a href='' class='collapse'></a>"
							//+				"<a href='' class='fullscreen'></a>"
							+				"<a class='fancybox-button tooltips' href='"+contentBox.previewImageUrl+"' data-original-title='Preview' data-rel='fancybox-button'>"
							+					"<i class='fa fa-search'></i>"	
							+				"</a>"
							+			"</div>"
							+		"</div>"	
							+		"<div class='portlet-body'>"
							+			"<div class='thumbnail'>"
							+				"<img alt='' src='"+contentBox.previewImageUrl+"'>";
			
			if(contentBox.description != "") {
				areaLeft += 				"<div class='caption'>"
							+				"<p>"+contentBox.description+"</p>"
							+				"</div>";
			}	
											
			areaLeft 	+=				"</div>"	
							+		"</div>"	
							+	"</div>";	
											
		}
		
		areaLeft += "<div class='portlet portlet-sortable-empty'></div>";
		
		$('#DropAreaLeft').html( areaLeft );
		
		$('.mix-grid-left').mixItUp({
		    animation: {
		        effects: 'fade rotateZ(-180deg)',
		        duration: 700
		    }
		});
	}
	
}

function parseSerial(hash){
	//var arrN = hash.split(',');
	var arrN = hash;
	//alert(arrN);
	var idlist="";
	for(var i=0; (i<arrN.length); i++)
	{
		var id = arrN[i].split('_')[1];
		if(id) {
			idlist +=  "_" + id;	
		}
	}
	if (idlist!="")
	{
		idlist = idlist.substring(1);
	}
	//alert(idlist);
	return idlist;

}

$( ".list-widget" ).change(function() {
		
	rearrangeSortablePortlets();
		
});