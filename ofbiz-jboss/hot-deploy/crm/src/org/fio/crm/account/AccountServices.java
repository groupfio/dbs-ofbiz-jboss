package org.fio.crm.account;
import org.ofbiz.security.Security;

import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.sql.Timestamp;
import java.util.List;

import org.fio.crm.constants.CrmConstants;
import org.fio.crm.party.PartyHelper;
import org.fio.crm.util.LoginFilterUtil;
import org.fio.crm.util.UtilMessage;
import org.fio.crm.leads.LeadsServices;
import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;

public class AccountServices {
	private AccountServices(){ }

	private static final String MODULE = AccountServices.class.getName();
	public static final String resource = "crmUiLabels";
	// create account
	public static Map<String, Object> createAccount(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		LocalDispatcher dispatcher = dctx.getDispatcher();
		Security security = dctx.getSecurity();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		Locale locale = (Locale) context.get("locale");
		String accountName = (String) context.get("accountName");
		String accountOrLead = (String) context.get("accountOrLead");
		// the field that flag if force complete to create contact even existing same name already
		String forceComplete = context.get("forceComplete") == null ? "N" : (String) context.get("forceComplete");
		if (!security.hasPermission(CrmConstants.SecurityPermissions.CRMSFA_ACCOUNT_CREATE, userLogin)) {
			return UtilMessage.createAndLogServiceError("CrmErrorPermissionDenied", locale, MODULE);
		}

		// the net result of creating an account is the generation of an Account partyId
		String accountPartyId = (String) context.get("partyId");
		try {
			// make sure user has the right crmsfa roles defined.  otherwise the account will be created as deactivated.
			if (UtilValidate.isEmpty(PartyHelper.getFirstValidTeamMemberRoleTypeId(userLogin.getString("partyId"), delegator))) {
				return UtilMessage.createAndLogServiceError("CrmError_NoRoleForCreateParty", UtilMisc.toMap("userPartyName", PartyHelper.getPartyName(delegator, userLogin.getString("partyId"), false), "requiredRoleTypes", PartyHelper.TEAM_MEMBER_ROLES), locale, MODULE);
			}
            // Check Valid account or not
            if (UtilValidate.isNotEmpty(context.get("parentPartyId"))) {
                Boolean accountValidation = LeadsServices.accountValidation(delegator, (String) context.get("parentPartyId"));
                if (!accountValidation) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resource, "invalidAccount", locale));
                }
            }
			// if we're given the partyId to create, then verify it is free to use
			if (accountPartyId != null) {
				Map<String, Object> findMap =  UtilMisc.<String, Object>toMap("partyId", accountPartyId);
				GenericValue party = delegator.findOne("Party", UtilMisc.toMap("partyId", accountPartyId),false);
				if (party != null) {
					// TODO maybe a more specific message such as "Account already exists"
					return UtilMessage.createAndLogServiceError("person.create.person_exists", findMap, locale, MODULE);
				}
			}

			// verify account name is use already
			if (!"Y".equals(forceComplete)) {
				Set<GenericValue> duplicateAccountsWithName;
				try {
					duplicateAccountsWithName = PartyHelper.getPartyGroupByGroupNameAndRoleType(delegator,accountName, "ACCOUNT");
					// if existing the account which have same account name, then return the conflict account and error message
					if (duplicateAccountsWithName.size() > 0 && !"Y".equals(forceComplete)) {
						GenericValue partyGroup = duplicateAccountsWithName.iterator().next();
						Map results = ServiceUtil.returnError(UtilMessage.expandLabel("CrmCreateAccountDuplicateCheckFail", UtilMisc.toMap("partyId", partyGroup.getString("partyId")), locale));
						results.put("duplicateAccountsWithName", duplicateAccountsWithName);
						return results;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// create the Party and PartyGroup, which results in a partyId
			Map<String, Object> input = UtilMisc.toMap("groupName", context.get("accountName"), "groupNameLocal", context.get("groupNameLocal"),
					"officeSiteName", context.get("officeSiteName"), "description", context.get("description"), "partyId", accountPartyId,"gstnNo",context.get("gstnNum"));
			Map<String, Object> serviceResults = dispatcher.runSync("createPartyGroup", input);
			if (ServiceUtil.isError(serviceResults)) {
				return serviceResults;
			}
			accountPartyId = (String) serviceResults.get("partyId");

			// create a PartyRole for the resulting Account partyId with roleTypeId = ACCOUNT
			if(UtilValidate.isNotEmpty(accountOrLead) && "Y".equals(accountOrLead))
			{
				serviceResults = dispatcher.runSync("createPartyRole", UtilMisc.toMap("partyId", accountPartyId, "roleTypeId", "ACCOUNT_LEAD", "userLogin", userLogin));
			}else{
				serviceResults = dispatcher.runSync("createPartyRole", UtilMisc.toMap("partyId", accountPartyId, "roleTypeId", "ACCOUNT", "userLogin", userLogin));
			}


			if (ServiceUtil.isError(serviceResults)) {
				return serviceResults;
			}

			// create PartySupplementalData
			GenericValue partyData = delegator.makeValue("PartySupplementalData", UtilMisc.toMap("partyId", accountPartyId));
			partyData.setNonPKFields(context);
			partyData.create();

			// create a unique party relationship between the userLogin and the Account with partyRelationshipTypeId RESPONSIBLE_FOR
			if(UtilValidate.isNotEmpty(accountOrLead) && "Y".equals(accountOrLead))
			{
				createResponsibleAccountLeadRelationshipForParty(userLogin.getString("partyId"), accountPartyId, userLogin, delegator, dispatcher);
			}else
			{
				createResponsibleAccountRelationshipForParty(userLogin.getString("partyId"), accountPartyId, userLogin, delegator, dispatcher);
			}



			// if initial data source is provided, add it
			String dataSourceId = (String) context.get("dataSourceId");
			if (dataSourceId != null) {
				serviceResults = dispatcher.runSync("crmsfa.addAccountDataSource",
						UtilMisc.toMap("partyId", accountPartyId, "dataSourceId", dataSourceId, "userLogin", userLogin));
				if (ServiceUtil.isError(serviceResults)) {
					return UtilMessage.createAndLogServiceError(serviceResults, "CrmErrorCreateAccountFail", locale, MODULE);
				}
			}

			// if initial marketing campaign is provided, add it
			String marketingCampaignId = (String) context.get("marketingCampaignId");
			if (marketingCampaignId != null) {
				serviceResults = dispatcher.runSync("crmsfa.addAccountMarketingCampaign",
						UtilMisc.toMap("partyId", accountPartyId, "marketingCampaignId", marketingCampaignId, "userLogin", userLogin));
				if (ServiceUtil.isError(serviceResults)) {
					return UtilMessage.createAndLogServiceError(serviceResults, "CrmErrorCreateAccountFail", locale, MODULE);
				}
			}

			// if there's an initialTeamPartyId, assign the team to the account
			String initialTeamPartyId = (String) context.get("initialTeamPartyId");
			if (initialTeamPartyId != null) {
				serviceResults = dispatcher.runSync("crmsfa.assignTeamToAccount", UtilMisc.toMap("accountPartyId", accountPartyId,
						"teamPartyId", initialTeamPartyId, "userLogin", userLogin));
				if (ServiceUtil.isError(serviceResults)) {
					return serviceResults;
				}
			}

			// create contact info
			ModelService service = dctx.getModelService("crmsfa.createBasicContactInfoForParty");
			input = service.makeValid(context, "IN");
			input.put("partyId", accountPartyId);
			serviceResults = dispatcher.runSync(service.name, input);

			Debug.logInfo("---create contact info service ------"+serviceResults,MODULE);

			String partyClassificationGroupId = (String) context.get("partyClassificationGroupId"); 
			if(UtilValidate.isNotEmpty(partyClassificationGroupId)) {
				createPartyClassification(delegator, accountPartyId, partyClassificationGroupId);
			}

			String contactPartyId = (String) context.get("contactPartyId");
			if (contactPartyId != null) {
				dispatcher.runSync("crmsfa.assignContactToAccount", UtilMisc.toMap("contactPartyId", contactPartyId, "accountPartyId", accountPartyId, "userLogin", userLogin));
			}

			String isExempt = (String) context.get("isExempt");
			String partyTaxId = (String) context.get("partyTaxId");
			if(UtilValidate.isNotEmpty(isExempt) && UtilValidate.isNotEmpty(partyTaxId)) {
				Map inputMap = UtilMisc.toMap("partyTaxId",partyTaxId,"isExempt",isExempt,"partyId",accountPartyId,"userLogin",userLogin);
				dispatcher.runSync("createPartyTaxExcemption",inputMap);
			}

			if(UtilValidate.isNotEmpty(accountOrLead) && "Y".equals(accountOrLead))
			{
				createPartyClassification(delegator,accountPartyId,"ACCT_LEAD_CLASS");
			}
		} catch (GenericServiceException e) {
			return UtilMessage.createAndLogServiceError(e, "CrmErrorCreateAccountFail", locale, MODULE);
		} catch (GenericEntityException e) {
			return UtilMessage.createAndLogServiceError(e, "CrmErrorCreateAccountFail", locale, MODULE);
		}
		// return the partyId of the newly created Account
		Map<String, Object> results = ServiceUtil.returnSuccess();
		results.put("partyId", accountPartyId);
		return results;
	}
	public static boolean createResponsibleAccountLeadRelationshipForParty(String partyId, String accountPartyId,
			GenericValue userLogin, Delegator delegator, LocalDispatcher dispatcher)
					throws GenericServiceException, GenericEntityException {
		return PartyHelper.createNewPartyToRelationship(partyId, accountPartyId, "ACCOUNT_LEAD", "RESPONSIBLE_FOR",
				"ACCOUNT_OWNER", PartyHelper.TEAM_MEMBER_ROLES, true, userLogin, delegator, dispatcher);
	}
	public static boolean createResponsibleAccountRelationshipForParty(String partyId, String accountPartyId,
			GenericValue userLogin, Delegator delegator, LocalDispatcher dispatcher)
					throws GenericServiceException, GenericEntityException {
		return PartyHelper.createNewPartyToRelationship(partyId, accountPartyId, "ACCOUNT", "RESPONSIBLE_FOR",
				"ACCOUNT_OWNER", PartyHelper.TEAM_MEMBER_ROLES, true, userLogin, delegator, dispatcher);
	}
	public static boolean createPartyClassification(Delegator delegator,String partyId,String partyClassifcationGroupId)
	{
		try{
			List<GenericValue> partyClassifications = EntityQuery.use(delegator).from("PartyClassification").where("partyId", partyId,"partyClassificationGroupId",partyClassifcationGroupId).queryList();
			//List<GenericValue> partyClassifications = delegator.findByAnd("PartyClassification", UtilMisc.toMap("partyId",partyId,"partyClassificationGroupId",partyClassifcationGroupId));
			if(partyClassifications == null) {
				GenericValue partyClassification = delegator.makeValue("PartyClassification");
				partyClassification.put("partyId", partyId);
				partyClassification.put("partyClassificationGroupId", partyClassifcationGroupId);
				partyClassification.put("fromDate", UtilDateTime.nowTimestamp());
				partyClassification.create();
			}else {
				return false;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static Map<String, Object> updateAccount(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		LocalDispatcher dispatcher = dctx.getDispatcher();
		Security security = dctx.getSecurity();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		Locale locale = (Locale)context.get("locale");
		String accountPartyId = (String) context.get("partyId");
		String currencyUomId = (String) context.get("currencyUomId");

		// make sure userLogin has CRMSFA_ACCOUNT_UPDATE permission for this account
		String userLoginId = userLogin.getString("partyId");
		if(LoginFilterUtil.checkEmployeePosition(delegator, userLoginId) && !security.hasPermission(CrmConstants.SecurityPermissions.CRMSFA_ACCOUNT_UPDATE, userLogin)) {
			return UtilMessage.createAndLogServiceError("CrmErrorPermissionDenied", locale, MODULE);
		}
		try {
            // Check Valid account or not
            if (UtilValidate.isNotEmpty(context.get("parentPartyId"))) {
                Boolean accountValidation = LeadsServices.accountValidation(delegator, (String) context.get("parentPartyId"));
                if (!accountValidation) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resource, "invalidAccount", locale));
                }
            }
			// update the Party and PartyGroup
			Map<String, Object> input = UtilMisc.toMap("groupName", context.get("accountName"), "groupNameLocal", context.get("groupNameLocal"),
					"officeSiteName", context.get("officeSiteName"), "description", context.get("description"));
			input.put("partyId", accountPartyId);
			input.put("preferredCurrencyUomId", currencyUomId);
			input.put("userLogin", userLogin);
			input.put("gstnNo",context.get("gstnNum"));
			Map<String, Object> serviceResults = dispatcher.runSync("updatePartyGroup", input);
			if (ServiceUtil.isError(serviceResults)) {
				return serviceResults;
			}

			// update PartySupplementalData
			GenericValue partyData = delegator.findOne("PartySupplementalData", UtilMisc.toMap("partyId", accountPartyId),false);
			if (partyData == null) {
				// create a new one
				partyData = delegator.makeValue("PartySupplementalData", UtilMisc.toMap("partyId", accountPartyId));
				partyData.create();
			}
			partyData.setNonPKFields(context);
			partyData.store();


			String partyClassificationGroupId = (String) context.get("partyClassificationGroupId"); 

			if(UtilValidate.isNotEmpty(partyClassificationGroupId)) {
				GenericValue partyClassification = null;   
				GenericValue removePartyClassification = null;        
				Map<String, Object> partyClassificationMap = UtilMisc.<String, Object>toMap("partyId", accountPartyId,"partyClassificationGroupId", partyClassificationGroupId);
				partyClassification = EntityUtil.getFirst(delegator.findByAnd("PartyClassification", partyClassificationMap,null,false));        

				if(UtilValidate.isEmpty(partyClassification)){
					removePartyClassification=EntityUtil.getFirst(delegator.findByAnd("PartyClassification",UtilMisc.toMap("partyId", accountPartyId),null,false));
					if(UtilValidate.isNotEmpty(removePartyClassification))
					{
						removePartyClassification.remove(); 
					}
					partyClassificationMap = UtilMisc.<String, Object>toMap("partyId", accountPartyId,"partyClassificationGroupId", partyClassificationGroupId,"fromDate", UtilDateTime.nowTimestamp());
					partyClassification = delegator.makeValue("PartyClassification", partyClassificationMap);            
					Debug.logInfo("---partyClassificationMap------"+partyClassificationMap,MODULE);
					delegator.create(partyClassification);
					Debug.logInfo("---partyClassificationMap---create---"+partyClassificationMap,MODULE);                   
				}
			}
			String isExempt = (String) context.get("isExempt");
			String partyTaxId = (String) context.get("partyTaxId");
			if(UtilValidate.isNotEmpty(isExempt) && UtilValidate.isNotEmpty(partyTaxId)) {
				Map inputMap = UtilMisc.toMap("partyTaxId",partyTaxId,"isExempt",isExempt,"partyId",accountPartyId,"userLogin",userLogin);
				dispatcher.runSync("updatePartyTaxExcemption",inputMap);
			}

		} catch (GenericServiceException e) {
			return UtilMessage.createAndLogServiceError(e, "CrmErrorUpdateAccountFail", locale, MODULE);
		} catch (GenericEntityException e) {
			return UtilMessage.createAndLogServiceError(e, "CrmErrorUpdateAccountFail", locale, MODULE);
		}
		return ServiceUtil.returnSuccess();
	}
	
	public static Map<String, Object> deactivateAccount(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale)context.get("locale");

        // what account we're expiring
        String accountPartyId = (String) context.get("partyId");

        // check that userLogin has CRMSFA_ACCOUNT_DEACTIVATE permission for this account
        String userLoginId = userLogin.getString("partyId");
        if (LoginFilterUtil.checkEmployeePosition(delegator, userLoginId) && !security.hasPermission(CrmConstants.SecurityPermissions.CRMSFA_ACCOUNT_DEACTIVATE, userLogin)){
            return UtilMessage.createAndLogServiceError("CrmErrorPermissionDenied", locale, MODULE);
        }

        // when to expire the account
        Timestamp expireDate = (Timestamp) context.get("expireDate");
        if (expireDate == null) {
            expireDate = UtilDateTime.nowTimestamp();
        }

        // in order to deactivate an account, we expire all party relationships on the expire date
        try {
            List<GenericValue> partyRelationships = delegator.findByAnd("PartyRelationship", UtilMisc.toMap("partyIdFrom", accountPartyId, "roleTypeIdFrom", "ACCOUNT"),null,false);
            PartyHelper.expirePartyRelationships(partyRelationships, expireDate, dispatcher, userLogin);
        } catch (GenericEntityException e) {
            return UtilMessage.createAndLogServiceError(e, "CrmErrorDeactivateAccountFail", locale, MODULE);
        } catch (GenericServiceException e) {
            return UtilMessage.createAndLogServiceError(e, "CrmErrorDeactivateAccountFail", locale, MODULE);
        }

        // set the account party statusId to PARTY_DISABLED and register PartyDeactivation
        // TODO: improve this to support disabling on a future expireDate
        try {
            GenericValue accountParty = delegator.findOne("Party", UtilMisc.toMap("partyId", accountPartyId),false);
            accountParty.put("statusId", "PARTY_DISABLED");
            accountParty.store();

            delegator.create("PartyDeactivation", UtilMisc.toMap("partyId", accountPartyId, "deactivationTimestamp", expireDate));
        } catch (GenericEntityException e) {
            return UtilMessage.createAndLogServiceError(e, "CrmErrorDeactivateAccountFail", locale, MODULE);
        }
        return ServiceUtil.returnSuccess();
    }

    public static Map<String, Object> reassignAccountResponsibleParty(DispatchContext dctx, Map<String, Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Security security = dctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");

        String accountPartyId = (String) context.get("accountPartyId");
        String newPartyId = (String) context.get("newPartyId");

        // ensure reassign permission on this account
        String userLoginId = userLogin.getString("partyId");
        if (LoginFilterUtil.checkEmployeePosition(delegator, userLoginId) && !security.hasPermission(CrmConstants.SecurityPermissions.CRMSFA_ACCOUNT_REASSIGN, userLogin)) {
            return UtilMessage.createAndLogServiceError("CrmErrorPermissionDenied", locale, MODULE);
        }
        try {
            // reassign relationship using a helper method
            boolean result = createResponsibleAccountRelationshipForParty(newPartyId, accountPartyId, userLogin, delegator, dispatcher);
            if (!result) {
                return UtilMessage.createAndLogServiceError("CrmErrorReassignFail", locale, MODULE);
            }
        } catch (GenericServiceException e) {
            return UtilMessage.createAndLogServiceError(e, "CrmErrorReassignFail", locale, MODULE);
        } catch (GenericEntityException e) {
            return UtilMessage.createAndLogServiceError(e, "CrmErrorReassignFail", locale, MODULE);
        }
        return ServiceUtil.returnSuccess();
    }

}
