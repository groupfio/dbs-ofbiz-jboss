package org.fio.crm.leads;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fio.crm.export.ExportUtil;
import org.fio.crm.export.ExportWrapper;
import org.fio.crm.export.ExporterFacade;
import org.fio.crm.party.PartyHelper;
import org.fio.crm.util.DataHelper;
import org.fio.crm.util.DataUtil;
import org.fio.crm.util.ResponseUtils;
import org.fio.crm.util.UtilCommon;
import org.fio.crm.util.VirtualTeamUtil;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.StringUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntity;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javolution.util.FastList;
import javolution.util.FastMap;

public class LeadEvents {
	private static final String module = LeadEvents.class.getName();
	
	@SuppressWarnings("unchecked")
	public static String exportLead(HttpServletRequest request,HttpServletResponse response) throws GenericEntityException {

		HttpSession session = request.getSession();
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		String exportType = request.getParameter("exportType");
		
		String partyId = request.getParameter("partyId");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String companyName = request.getParameter("companyName");
		String emailAddress = request.getParameter("emailAddress");
		String contactNumber = request.getParameter("contactNumber");
		String location = request.getParameter("location");
		String fromCallBackDate = request.getParameter("fromCallBackDate");
		String toCallBackDate = request.getParameter("toCallBackDate");
		String source = request.getParameter("source");
		String salesTurnoverFrom = request.getParameter("salesTurnoverFrom");
		String salesTurnoverTo = request.getParameter("salesTurnoverTo");
		String tallyUserType = request.getParameter("tallyUserType");
		String leadStatus = request.getParameter("leadStatus");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		String noOfDaysSinceLastCall = request.getParameter("noOfDaysSinceLastCall");
		List<String> RMRoleList = UtilCommon.getArrayToList(request.getParameter("RMRoleList"));
		List<String> selectedFields = UtilCommon.getArrayToList(request.getParameter("selectedFields"));
		String virtualTeamId = request.getParameter("virtualTeamId");
		
		Map < String, Object > returnMap = FastMap.newInstance();
		List < Object > findList = FastList.newInstance();
		String returnMessage = "success";
		
		try {
			List < String > partyIdsTo = new LinkedList<String>();
			if(UtilValidate.isNotEmpty(RMRoleList)) {
				for(String partyIdTo : RMRoleList) {
					Debug.log("==partyIdTo===="+partyIdTo);
					EntityCondition searchConditions = EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, partyIdTo),
							EntityUtil.getFilterByDateExpr());
					List<GenericValue> existingRelationship = delegator.findList("PartyRelationship", searchConditions,null, null, null, false);
					if(UtilValidate.isNotEmpty(existingRelationship)) {
					   for(GenericValue partyRelation : existingRelationship) {
					     String partyIds = partyRelation.getString("partyIdFrom");
					     Debug.log("==partyIds===="+partyIds);
						 partyIdsTo.add(partyIds);
					   }
					}
					
				}
			}
			Debug.log("==partyIdsTo===="+partyIdsTo);
			List < EntityCondition > conditions = new ArrayList < EntityCondition > ();
	
			// construct role conditions
			EntityCondition roleTypeCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "LEAD"),
					EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "LEAD_CONVERTED")), EntityOperator.AND);
			conditions.add(roleTypeCondition);
	
			EntityCondition partyStatusCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
					EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)), EntityOperator.OR);
	
			conditions.add(partyStatusCondition);
			conditions.add(EntityUtil.getFilterByDateExpr());
			if(UtilValidate.isNotEmpty(RMRoleList)) {
				EntityCondition teamMemberCondition = EntityCondition.makeCondition("partyId", EntityOperator.IN, partyIdsTo);
				conditions.add(teamMemberCondition);
			}
	
			if (UtilValidate.isNotEmpty(partyId)) {
				EntityCondition partyCondition = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId);
				conditions.add(partyCondition);
			}
			if (UtilValidate.isNotEmpty(firstName)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("firstName")), EntityOperator.LIKE, "%"+firstName.toUpperCase()+"%"),
						EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("partyFirstName")), EntityOperator.LIKE, "%"+firstName.toUpperCase()+"%")
						), EntityOperator.OR);
				//EntityCondition firstNameCondition = EntityCondition.makeCondition("firstName", EntityOperator.LIKE, firstName + "%");
				conditions.add(condition);
			}
			if (UtilValidate.isNotEmpty(lastName)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("lastName")), EntityOperator.LIKE, "%"+lastName.toUpperCase()+"%"),
						EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("partyLastName")), EntityOperator.LIKE, "%"+lastName.toUpperCase()+"%")
						), EntityOperator.OR);
				//EntityCondition lastNameCondition = EntityCondition.makeCondition("lastName", EntityOperator.LIKE, lastName + "%");
				conditions.add(condition);
			}
			if (UtilValidate.isNotEmpty(companyName)) {
				EntityCondition companyNameCondition = EntityCondition.makeCondition("companyName", EntityOperator.LIKE, "%"+companyName + "%");
				conditions.add(companyNameCondition);
			}
	
			if (UtilValidate.isNotEmpty(source)) {
				EntityCondition condition = EntityCondition.makeCondition("source", EntityOperator.EQUALS, source);
				conditions.add(condition);
			}
			
			if (UtilValidate.isNotEmpty(leadStatus)) {
				EntityCondition condition = EntityCondition.makeCondition("leadStatus", EntityOperator.EQUALS, leadStatus);
				conditions.add(condition);
			}
	
			if (UtilValidate.isNotEmpty(salesTurnoverFrom)) {
				EntityCondition condition = EntityCondition.makeCondition("salesTurnover", EntityOperator.GREATER_THAN_EQUAL_TO, new BigDecimal(salesTurnoverFrom));
				conditions.add(condition);
			}
			if (UtilValidate.isNotEmpty(salesTurnoverTo)) {
				EntityCondition condition = EntityCondition.makeCondition("salesTurnover", EntityOperator.LESS_THAN_EQUAL_TO, new BigDecimal(salesTurnoverTo));
				conditions.add(condition);
			}
	
			if (UtilValidate.isNotEmpty(tallyUserType)) {
				EntityCondition condition = EntityCondition.makeCondition("tallyUserType", EntityOperator.EQUALS, tallyUserType);
				conditions.add(condition);
			}
	
			List < EntityCondition > eventExprs = new LinkedList < EntityCondition > ();
			if (UtilValidate.isNotEmpty(emailAddress) || UtilValidate.isNotEmpty(contactNumber) || UtilValidate.isNotEmpty(location)) {
	
				if (UtilValidate.isNotEmpty(emailAddress)) {
					EntityCondition emailCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("infoString", EntityOperator.EQUALS, emailAddress),
							EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "EMAIL_ADDRESS")), EntityOperator.AND);
					eventExprs.add(emailCondition);
				}
	
				if (UtilValidate.isNotEmpty(contactNumber)) {
					EntityCondition phoneCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("contactNumber", EntityOperator.EQUALS, contactNumber),
							EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "TELECOM_NUMBER")), EntityOperator.AND);
					eventExprs.add(phoneCondition);
				}
	
				if (UtilValidate.isNotEmpty(location)) {
					
					EntityCondition locationCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("city", EntityOperator.EQUALS, location)
							//EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "POSTAL_ADDRESS")
							), EntityOperator.AND);
					eventExprs.add(locationCondition);
					
					/*EntityCondition locationCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("city", EntityOperator.EQUALS, location),
							EntityCondition.makeCondition("contactMechTypeId", EntityOperator.EQUALS, "POSTAL_ADDRESS")), EntityOperator.AND);
					eventExprs.add(locationCondition);*/
				}
				conditions.add(EntityCondition.makeCondition(eventExprs, EntityOperator.OR));
			}
	
			if (UtilValidate.isNotEmpty(fromCallBackDate) || UtilValidate.isNotEmpty(toCallBackDate)) {
				if(UtilValidate.isEmpty(fromCallBackDate)) {
					conditions.add(EntityCondition.makeCondition("lastCallBackDate", EntityOperator.LESS_THAN_EQUAL_TO, java.sql.Date.valueOf(toCallBackDate)));
				} else if(UtilValidate.isEmpty(toCallBackDate)) {
					conditions.add(EntityCondition.makeCondition("lastCallBackDate", EntityOperator.GREATER_THAN_EQUAL_TO, java.sql.Date.valueOf(fromCallBackDate)));
				} else if(UtilValidate.isNotEmpty(fromCallBackDate) && UtilValidate.isNotEmpty(toCallBackDate)) {
					conditions.add(EntityCondition.makeCondition("lastCallBackDate", EntityOperator.BETWEEN, UtilMisc.toList(java.sql.Date.valueOf(fromCallBackDate),java.sql.Date.valueOf(toCallBackDate))));
				}
			}
			if (UtilValidate.isNotEmpty(noOfDaysSinceLastCall)) {
                Integer i = Integer.valueOf(noOfDaysSinceLastCall);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, -i);
                String sinceCallDate = formatter1.format(cal.getTime());
                conditions.add(EntityCondition.makeCondition("lastContactDate", EntityOperator.EQUALS, java.sql.Date.valueOf(sinceCallDate)));
            }
			//Login Based lead Filter
            String userLoginId = userLogin.getString("userLoginId");
            List<GenericValue> userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup")
            		.where("userLoginId", userLoginId, "groupId", org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")).filterByDate().queryList();
            if(userLoginSecurityGroup == null || userLoginSecurityGroup.size() < 1){
			Map<String, Object> dataSecurityMetaInfo = (Map<String, Object>) session.getAttribute("dataSecurityMetaInfo");
			if (ResponseUtils.isSuccess(dataSecurityMetaInfo)) {
	
				List<String> lowerPositionPartyIds = (List<String>) dataSecurityMetaInfo.get("lowerPositionPartyIds");
				if (UtilValidate.isNotEmpty(lowerPositionPartyIds)) {
					
					List < EntityCondition > securityConditions = new ArrayList < EntityCondition > ();
					
					Map<String, Object> virtualTeam = VirtualTeamUtil.getFirstVirtualTeamMember(delegator, virtualTeamId, userLogin.getString("partyId"));
					
					if (UtilValidate.isEmpty(virtualTeam.get("virtualTeamId"))) {
						securityConditions.add(EntityCondition.makeCondition(EntityOperator.AND,
								EntityCondition.makeCondition("partyIdTo", EntityOperator.IN, lowerPositionPartyIds),
								EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
								EntityUtil.getFilterByDateExpr()
								));
					}
					
					if (UtilValidate.isNotEmpty(userLogin)) {
						securityConditions.add(EntityCondition.makeCondition(UtilMisc.toList(
								EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, userLogin.getString("userLoginId"))
								//securityConditions
							), EntityOperator.OR));
					}
					
					// virtual team [start]
					
					virtualTeamId = UtilValidate.isNotEmpty(virtualTeamId) ? virtualTeamId : (String) virtualTeam.get("virtualTeamId");
					//String loggedUserVirtualTeamId = (String) dataSecurityMetaInfo.get("loggedUserVirtualTeamId");
					//virtualTeamId = UtilValidate.isNotEmpty(virtualTeamId) ? virtualTeamId : loggedUserVirtualTeamId;
					List<Map<String, Object>> virtualTeamMemberList = VirtualTeamUtil.getVirtualTeamMemberList(delegator, null, userLogin.getString("partyId"));
					if (UtilValidate.isNotEmpty(virtualTeamMemberList)) {
						
						Set<String> virtualTeamIdAsLeadList = VirtualTeamUtil.getVirtualTeamIds(virtualTeamMemberList, "VT_SG_TL", true);
						if (UtilValidate.isNotEmpty(virtualTeamIdAsLeadList)) {
							securityConditions.add(EntityCondition.makeCondition("virtualTeamId", EntityOperator.IN, virtualTeamIdAsLeadList));
							Set<String> virtualTeamMemberPartyIdList = new HashSet<String>();
							for (String vtId : virtualTeamIdAsLeadList) {
								List<Map<String, Object>> teamMemberList = VirtualTeamUtil.getVirtualTeamMemberList(delegator, vtId, null);
								virtualTeamMemberPartyIdList.addAll( DataUtil.getFieldListFromMapList(teamMemberList, "virtualTeamMemberId", true) );
							}
							
							if (UtilValidate.isNotEmpty(virtualTeamMemberPartyIdList)) {
								securityConditions.add(EntityCondition.makeCondition(EntityOperator.AND,
										EntityCondition.makeCondition("partyIdTo", EntityOperator.IN, virtualTeamMemberPartyIdList),
										EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
										EntityUtil.getFilterByDateExpr()
										));
							}
							
						}
						
						Set<String> virtualTeamIdAsMemberList = VirtualTeamUtil.getVirtualTeamIds(virtualTeamMemberList, "VT_SG_TL", false);
						if (UtilValidate.isNotEmpty(virtualTeamIdAsMemberList)) {
							securityConditions.add(EntityCondition.makeCondition(UtilMisc.toList(
									EntityCondition.makeCondition("partyIdTo", EntityOperator.EQUALS, userLogin.getString("partyId")),
									EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
									//EntityCondition.makeCondition("virtualTeamId", EntityOperator.IN, virtualTeamIdAsMemberList),
									EntityUtil.getFilterByDateExpr()
								), EntityOperator.AND));
							
						}
						
					}
					
					// virtual team [end]
	
					EntityCondition securityCondition = EntityCondition.makeCondition(UtilMisc.toList(
							securityConditions
							), EntityOperator.OR);
					
					conditions.add(securityCondition);
				}
	
				Debug.log("lowerPositionPartyIds> "+lowerPositionPartyIds);
	
			}
            }
			EntityCondition orderFieldConditions = EntityCondition.makeCondition(EntityOperator.AND,
					EntityUtil.getFilterByDateExpr()
					);
			conditions.add(orderFieldConditions);
			EntityCondition mainConditons = EntityCondition.makeCondition(conditions, EntityOperator.AND);
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);

			List <GenericValue> parties = delegator.findList("PartyFromByRelnAndContactInfoAndPartySupplemantalData", mainConditons, UtilMisc.toSet("partyId"), null, efo, false);
			
			if (parties != null && parties.size() > 0) {
				List <String> partyIdList = EntityUtil.getFieldListFromEntityList(parties, "partyId", true);
				List<Map<String, Object>> extractList = FastList.newInstance();
				if(partyIdList.size() > 0) {
                    for(String leadId : partyIdList) {
						Map<String, Object> exportRow = new LinkedHashMap<String,Object>();
						
						GenericValue person = delegator.findOne("Person", UtilMisc.toMap("partyId",leadId), false);
						String first_name = "";
						String last_name = "";
						if(person != null && person.size() > 0) {
							first_name = person.getString("firstName");
							last_name = person.getString("lastName");
						}
						GenericValue partySupplementalData = EntityUtil.getFirst( delegator.findByAnd("PartySupplementalData", UtilMisc.toMap("partyId", leadId), null, false) );
						if (UtilValidate.isNotEmpty(partySupplementalData)) {
						    exportRow.put("lead_id",leadId);
						    
							ExportWrapper.getData(selectedFields, "first_name", first_name != "" ? first_name : partySupplementalData.getString("partyFirstName"), exportRow);
							ExportWrapper.getData(selectedFields, "last_name", last_name != "" ? last_name : partySupplementalData.getString("partyLastName"), exportRow);
							ExportWrapper.getData(selectedFields, "company_name", partySupplementalData.getString("companyName") != null ? partySupplementalData.getString("companyName") : "", exportRow);
							ExportWrapper.getData(selectedFields, "parent_co_details", partySupplementalData.getString("parentCoDetails") != null ? partySupplementalData.getString("parentCoDetails") : "", exportRow);
							ExportWrapper.getData(selectedFields, "Salutation", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("title"), "SALUTATION"), exportRow);
							ExportWrapper.getData(selectedFields, "sales_turnover", partySupplementalData.getString("salesTurnover") != null ? partySupplementalData.getString("salesTurnover") : "", exportRow);
							ExportWrapper.getData(selectedFields, "date_of_incorporation", partySupplementalData.getString("dateOfIncorporation") != null ? partySupplementalData.getString("dateOfIncorporation") : "", exportRow);
							ExportWrapper.getData(selectedFields, "constitution", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("constitution"), "DBS_CONSTITUTION"), exportRow);
							ExportWrapper.getData(selectedFields, "industry_cat", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("industryCat"), "DBS_INDUSTRY_CAT"), exportRow);
							ExportWrapper.getData(selectedFields, "industry", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("industry"), "DBS_INDUSTRY"), exportRow);
							ExportWrapper.getData(selectedFields, "Import / Export Customer", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("customerTradingType"), "DBS_CUST_TRD_TYPE"), exportRow);
							ExportWrapper.getData(selectedFields, "tally_user_type", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("tallyUserType"), "DBS_TALLY_USR_TYPE"), exportRow);
							ExportWrapper.getData(selectedFields, "source", DataHelper.getPartyIdentificationDescription(delegator, partySupplementalData.getString("source")), exportRow);
							ExportWrapper.getData(selectedFields, "tcp_name", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("tcpName"), "DBS_TCP_NAME"), exportRow);
							ExportWrapper.getData(selectedFields, "key_contact_person1", partySupplementalData.getString("keyContactPerson1") != null ? partySupplementalData.getString("keyContactPerson1") : "", exportRow);
							ExportWrapper.getData(selectedFields, "key_contact_person2", partySupplementalData.getString("keyContactPerson2") != null ? partySupplementalData.getString("keyContactPerson2") : "", exportRow);
							
							String primary_phone_country_code = "";
							String phone_number1 = "";
							String phone_number2_country_code = "";
							String phone_number2 = "";
							String address1 = "";
							String address2 = "";
							String cityId = "";
							String stateProvinceGeoId = "";
							String postalCode = "";
							String email_address = "";
							
							String phoneNumberOneDndStatus = "";
							String phoneNumberTwoDndStatus = "";
							
							List < GenericValue > partyContactMechs = delegator.findByAnd("PartyContactMech", UtilMisc.toMap("partyId", leadId), null, false);
							if (partyContactMechs != null && partyContactMechs.size() > 0) {
								partyContactMechs = EntityUtil.filterByDate(partyContactMechs);
								if (partyContactMechs != null && partyContactMechs.size() > 0) {
									partyContactMechs = EntityUtil.getFieldListFromEntityList(partyContactMechs, "contactMechId", true);
								}
								if (partyContactMechs != null && partyContactMechs.size() > 0) {
									Set < String > findOptions = UtilMisc.toSet("contactMechId");
									List < String > orderBy = UtilMisc.toList("createdStamp DESC");

									EntityCondition condition1 = EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, leadId);
									EntityCondition condition2 = EntityCondition.makeCondition("contactMechId", EntityOperator.IN, partyContactMechs);

									EntityCondition primaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_PHONE")));
									List < GenericValue > primaryPhones = delegator.findList("PartyContactMechPurpose", primaryPhoneConditions, findOptions, orderBy, null, false);
									if (primaryPhones != null && primaryPhones.size() > 0) {
										GenericValue primaryPhone = EntityUtil.getFirst(EntityUtil.filterByDate(primaryPhones));
										if (UtilValidate.isNotEmpty(primaryPhone)) {
											GenericValue primaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", primaryPhone.getString("contactMechId")), false);
											if (UtilValidate.isNotEmpty(primaryPhoneNumber)) {
												phone_number1 = primaryPhoneNumber.getString("contactNumber");
												primary_phone_country_code = primaryPhoneNumber.getString("countryCode"); 
												phoneNumberOneDndStatus = primaryPhoneNumber.getString("dndStatus"); 
											}
										}
									}

									EntityCondition secondaryPhoneConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PHONE_WORK_SEC")));
									List <GenericValue> secondaryPhones = delegator.findList("PartyContactMechPurpose", secondaryPhoneConditions, findOptions, orderBy, null, false);
									if (secondaryPhones != null && secondaryPhones.size() > 0) {
										GenericValue secondaryPhone = EntityUtil.getFirst(EntityUtil.filterByDate(secondaryPhones));
										if (UtilValidate.isNotEmpty(secondaryPhone)) {
											GenericValue secondaryPhoneNumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactMechId", secondaryPhone.getString("contactMechId")), false);
											if (UtilValidate.isNotEmpty(secondaryPhoneNumber)) {
												phone_number2 = secondaryPhoneNumber.getString("contactNumber");
												phone_number2_country_code = secondaryPhoneNumber.getString("countryCode"); 
												phoneNumberTwoDndStatus = secondaryPhoneNumber.getString("dndStatus"); 
											}
										}
									}

									EntityCondition primaryEmailConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_EMAIL")));
									List < GenericValue > primaryEmails = delegator.findList("PartyContactMechPurpose", primaryEmailConditions, findOptions, orderBy, null, false);
									if (primaryEmails != null && primaryEmails.size() > 0) {
										GenericValue primaryEmail = EntityUtil.getFirst(EntityUtil.filterByDate(primaryEmails));
										if (UtilValidate.isNotEmpty(primaryEmail)) {
											GenericValue primaryInfoString = delegator.findOne("ContactMech", UtilMisc.toMap("contactMechId", primaryEmail.getString("contactMechId")), false);
											if (UtilValidate.isNotEmpty(primaryInfoString)) {
												email_address = primaryInfoString.getString("infoString");
											}
										}
									}

									EntityCondition postalAddressConditions = EntityCondition.makeCondition(UtilMisc.toList(condition1, condition2, EntityCondition.makeCondition("contactMechPurposeTypeId", EntityOperator.EQUALS, "PRIMARY_LOCATION")));
									List < GenericValue > primaryAddressList = delegator.findList("PartyContactMechPurpose", postalAddressConditions, findOptions, orderBy, null, false);
									if (primaryAddressList != null && primaryAddressList.size() > 0) {
										GenericValue primaryAddress = EntityUtil.getFirst(EntityUtil.filterByDate(primaryAddressList));
										if (UtilValidate.isNotEmpty(primaryAddress)) {
											GenericValue postalAddress = delegator.findOne("PostalAddress", UtilMisc.toMap("contactMechId", primaryAddress.getString("contactMechId")), false);
											if (UtilValidate.isNotEmpty(postalAddress)) {
												cityId = postalAddress.getString("city");
												stateProvinceGeoId = postalAddress.getString("stateProvinceGeoId");
												address1 = postalAddress.getString("address1");
												address2 = postalAddress.getString("address2");
												postalCode = postalAddress.getString("postalCode");
											}
										}
									}
								}
							}
							
							String personResponsible = "";
							String personResponsibleAssignBy = "";

							EntityCondition conditionPR = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("partyIdFrom", EntityOperator.EQUALS, leadId),
						            EntityCondition.makeCondition("roleTypeIdTo", EntityOperator.EQUALS, "ACCOUNT_MANAGER"),
						        EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.IN, UtilMisc.toList("ACCOUNT", "LEAD", "CONTACT")),
						        EntityCondition.makeCondition("partyRelationshipTypeId", EntityOperator.EQUALS, "RESPONSIBLE_FOR"),
						        EntityUtil.getFilterByDateExpr()), EntityOperator.AND);

							GenericValue responsibleFor = EntityQuery.use(delegator).from("PartyRelationship").where(conditionPR).orderBy("fromDate DESC").queryFirst();
							
							if (UtilValidate.isNotEmpty(responsibleFor)) {
								personResponsible = responsibleFor.getString("partyIdTo");
							    
							    if (UtilValidate.isNotEmpty(responsibleFor.getString("createdByUserLoginId"))) {
							    	GenericValue createdByUserLogin =  EntityQuery.use(delegator).from("UserLogin").where("userLoginId", responsibleFor.getString("createdByUserLoginId")).queryFirst();
							    	if (UtilValidate.isNotEmpty(createdByUserLogin)) {
							    		personResponsibleAssignBy = createdByUserLogin.getString("partyId");
							    	}
							    }
							    
							}
							
							ExportWrapper.getData(selectedFields, "phone_number1_country_code", primary_phone_country_code != null ? primary_phone_country_code : "", exportRow);
							ExportWrapper.getData(selectedFields, "phone_number1", phone_number1 != null ? phone_number1 : "", exportRow);
							ExportWrapper.getData(selectedFields, "phone_number2_country_code", phone_number2_country_code != null ? phone_number2_country_code : "", exportRow);
							ExportWrapper.getData(selectedFields, "phone_number2", phone_number2 != null ? phone_number2 : "", exportRow);
							ExportWrapper.getData(selectedFields, "Company Address", address1 != null ? address1 : "", exportRow);
							ExportWrapper.getData(selectedFields, "city", DataHelper.getGeoName(delegator, partySupplementalData.getString("city"), "CITY"), exportRow);
							ExportWrapper.getData(selectedFields, "state", DataHelper.getGeoName(delegator, partySupplementalData.getString("stateProvinceGeoId"), "STATE"), exportRow);
							ExportWrapper.getData(selectedFields, "email_address",email_address != null ? email_address : "", exportRow);
							ExportWrapper.getData(selectedFields, "permanent_acccount_number", partySupplementalData.getString("permanentAcccountNumber") != null ? partySupplementalData.getString("permanentAcccountNumber") : "", exportRow);
							ExportWrapper.getData(selectedFields, "business_reg_no", partySupplementalData.getString("businessRegNo") != null ? partySupplementalData.getString("businessRegNo") : "", exportRow);
							ExportWrapper.getData(selectedFields, "Other Banks", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("otherBankName"), "DBS_EXISTING_BN"), exportRow);
							ExportWrapper.getData(selectedFields, "Total Cash Balances Maintained With Other Banks", partySupplementalData.getString("otherBankBalance") != null ? partySupplementalData.getString("otherBankBalance") : "", exportRow);
							ExportWrapper.getData(selectedFields, "Current Product(s) Held With Other Bank", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("productsHeldInOthBank"), "DBS_PROD_HIOB"), exportRow);
							ExportWrapper.getData(selectedFields, "Product Value with other banks", partySupplementalData.getString("productsValueInOthBank") != null ? partySupplementalData.getString("productsValueInOthBank") : "", exportRow);
							ExportWrapper.getData(selectedFields, "paidup_capital", partySupplementalData.getString("paidupCapital") != null ? partySupplementalData.getString("paidupCapital") : "", exportRow);
							ExportWrapper.getData(selectedFields, "authorised_cap", partySupplementalData.getString("authorisedCap") != null ? partySupplementalData.getString("authorisedCap") : "", exportRow);
							ExportWrapper.getData(selectedFields, "Lead Assigned To", personResponsible, exportRow);
							ExportWrapper.getData(selectedFields, "Lead Assigned By", personResponsibleAssignBy, exportRow);
							ExportWrapper.getData(selectedFields, "segment", partySupplementalData.getString("segment") != null ? partySupplementalData.getString("segment") : "", exportRow);
							ExportWrapper.getData(selectedFields, "Asset / Liability Lead", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("liabOrAsset"), "DBS_LA_TYPE"), exportRow);
							ExportWrapper.getData(selectedFields, "Telecaller Status", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("teleCallingStatus"), "DBS_TELE_CALL_STATUS"), exportRow);
							ExportWrapper.getData(selectedFields, "Telecaller sub status", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("teleCallingSubStatus"), "DBS_TELE_SUB_STATUS"), exportRow);
							ExportWrapper.getData(selectedFields, "Telecaller Remarks", partySupplementalData.getString("teleCallingRemarks") != null ? partySupplementalData.getString("teleCallingRemarks") : "", exportRow);
							ExportWrapper.getData(selectedFields, "RM Call Status", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("rmCallingStatus"), "DBS_RM_CALL_STATUS"), exportRow);
							ExportWrapper.getData(selectedFields, "RM Call Sub Status", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("rmCallingSubStatus"), "DBS_RM_SUB_STATUS"), exportRow);
							ExportWrapper.getData(selectedFields, "RM Remarks", partySupplementalData.getString("rmCallingRemarks") != null ? partySupplementalData.getString("rmCallingRemarks") : "", exportRow);
							ExportWrapper.getData(selectedFields, "no_of_attempt", partySupplementalData.getString("noOfAttempt") != null ? partySupplementalData.getString("noOfAttempt") : "", exportRow);
							ExportWrapper.getData(selectedFields, "finacle_id", partySupplementalData.getString("finacleId") != null ? partySupplementalData.getString("finacleId") : "", exportRow);
							ExportWrapper.getData(selectedFields, "place_of_incorporation", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("placeOfIncorporation"), "DBS_PLACE_INCORP"), exportRow);
							ExportWrapper.getData(selectedFields, "no_of_employees", partySupplementalData.getString("noOfEmployees") != null ? partySupplementalData.getString("noOfEmployees") : "", exportRow);
							ExportWrapper.getData(selectedFields, "job_family", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("jobFamily"), "DBS_JOB_FAMILY"), exportRow);
							ExportWrapper.getData(selectedFields, "designation", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("designation"), "DBS_LD_DESIGNATION"), exportRow);
							ExportWrapper.getData(selectedFields, "PIN Code", postalCode, exportRow);
							ExportWrapper.getData(selectedFields, "lead_status", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("leadStatus"), "LEAD_STATUS_HISTORY"), exportRow);

							ExportWrapper.getData(selectedFields, "phone_number1_dnd_status", phoneNumberOneDndStatus, exportRow);
							ExportWrapper.getData(selectedFields, "phone_number2_dnd_status", phoneNumberTwoDndStatus, exportRow);
							ExportWrapper.getData(selectedFields, "lead_score", DataHelper.getEnumDescription(delegator, partySupplementalData.getString("leadScore"), "LEAD_SCORE"), exportRow);
							
							extractList.add(exportRow);
							
						}
					}
				}
				
				if(extractList.size() > 0) {
					
					String fileName = "LeadExport_"+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+ExportUtil.getFileExtension(exportType);
					String templateLocation = ComponentConfig.getRootLocation("crm")+"/webapp/crm-resource/template";
					String delimiter = ",";
					
					Map<String, Object> exportContext = new HashMap<String, Object>();
					
					exportContext.put("delegator", delegator);
					exportContext.put("rows", extractList);
					exportContext.put("headers", null);
					exportContext.put("fileName", fileName);
					exportContext.put("location", templateLocation);
					exportContext.put("delimiter", delimiter);
					exportContext.put("isHeaderRequird", true);
					
					exportContext.put("exportType", exportType);
					
					ExporterFacade.exportReport(exportContext);
					
					Thread.sleep(1000);
					
					String filePath = templateLocation+File.separatorChar+fileName;
					
					ExporterFacade.downloadReport(request, response, filePath, exportType);
					
					boolean isdelete = true;
					if(isdelete) {
						File file = new File(filePath);
						file.delete();
					}
				}
			}else {
				request.setAttribute("_ERROR_MESSAGE_", "No Records found to Export");
				returnMessage = "error";
			}

		} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), module);
			return "error";
		}
		
		return returnMessage;
	}
	
	@SuppressWarnings("unchecked")
	public static String exportLeadErrorLog(HttpServletRequest request,HttpServletResponse response) throws GenericEntityException {

		HttpSession session = request.getSession();
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		String exportType = request.getParameter("exportType");
		
	
		List<String> selectedFields = UtilCommon.getArrayToList(request.getParameter("selectedFields"));
		Debug.log("==selectedFields  ==="+selectedFields);
		try {
			List < EntityCondition > conditions = new ArrayList < EntityCondition > ();
			List < String > partyIdsTo = new LinkedList<String>();


			String importStatusId = request.getParameter("importStatusId");
			String partyId = request.getParameter("leadId");
			String batchId = request.getParameter("batchId");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String uploadedByUserLoginId = request.getParameter("uploadedByUserLoginId");
			String errorCodeId = request.getParameter("errorCodeId");

			String draw = request.getParameter("draw");
			String start = request.getParameter("start");
			String length = request.getParameter("length");

			String sortDir = "desc";
			String orderField = "";
			String orderColumnId = request.getParameter("order[0][column]");
			if(UtilValidate.isNotEmpty(orderColumnId)) {
				int sortColumnId = Integer.parseInt(orderColumnId);
				String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
				sortDir = request.getParameter("order[0][dir]").toUpperCase();
				orderField = sortColumnName;
			} else {
				orderField = "createdStamp";
			}


			boolean isApprover = false;
			/*
			security = request.getAttribute("security");
			if (security.hasPermission("ETL-IMPDAT-APPROVER", userLogin)) {
			    isApprover = true;
			}
			*/

			if (UtilValidate.isNotEmpty(importStatusId)) {
				if (importStatusId.equals("DISABLED")) {
					conditions.add(EntityCondition.makeCondition("partyStatusId", EntityOperator.EQUALS, "PARTY_DISABLED"));	
				} else if (importStatusId.equals("DATAIMP_ERROR")) {
					
					
					EntityCondition statusCondition = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_ERROR"),
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_FAILED"))
					, EntityOperator.OR);
					conditions.add(statusCondition);		
				} else {
					EntityCondition statusCondition = EntityCondition.makeCondition(UtilMisc.toList(
						EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, importStatusId)
					), EntityOperator.AND);
					conditions.add(statusCondition);	
				}
			} else {
				/*EntityCondition statusCondition = EntityCondition.makeCondition([
					EntityCondition.makeCondition("importStatusId", EntityOperator.EQUALS, "DATAIMP_NOT_APPROVED")
				], EntityOperator.AND);
				conditionsList.add(statusCondition);
				*/
			}

			String fromDate = request.getParameter("fromDate");
			String thruDate = request.getParameter("thruDate");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
			if (UtilValidate.isNotEmpty(fromDate)) {
				String fromdateVal=fromDate+" "+"00:00:00.0";
				Date parsedDateFrom = dateFormat.parse(fromdateVal);
				Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());	
				conditions.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
			}
			if (UtilValidate.isNotEmpty(thruDate)) { 
				String thruDateVal=thruDate+" "+"23:23:59.0";
				Date parsedDateThru = dateFormat.parse(thruDateVal);
				Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
				conditions.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
			}

			if (UtilValidate.isNotEmpty(partyId)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("leadId")), EntityOperator.LIKE, "%"+partyId.toUpperCase()+"%"),
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("primaryPartyId")), EntityOperator.LIKE, "%"+partyId.toUpperCase()+"%")
				), EntityOperator.OR);
				conditions.add(condition);
			}

			if (UtilValidate.isNotEmpty(batchId)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("batchId")), EntityOperator.LIKE, "%"+batchId.toUpperCase()+"%")
				), EntityOperator.OR);
				conditions.add(condition);
			}

			if (UtilValidate.isNotEmpty(firstName)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("firstName")), EntityOperator.LIKE, "%"+firstName.toUpperCase()+"%")
				), EntityOperator.OR);
				conditions.add(condition);
			}
			if (UtilValidate.isNotEmpty(lastName)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("lastName")), EntityOperator.LIKE, "%"+lastName.toUpperCase()+"%")
				), EntityOperator.OR);
				conditions.add(condition);
			}

			if (UtilValidate.isNotEmpty(errorCodeId)) {
				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
					EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("errorCodes")), EntityOperator.LIKE, "%"+errorCodeId.toUpperCase()+"%")
				), EntityOperator.OR);
				conditions.add(condition);
			}

			if (UtilValidate.isNotEmpty(uploadedByUserLoginId)) {
				conditions.add(EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, uploadedByUserLoginId));
			}

			if (!isApprover) {
				conditions.add(EntityCondition.makeCondition("uploadedByUserLoginId", EntityOperator.EQUALS, userLogin.getString("userLoginId")));
			}

			/*EntityCondition orderFieldConditions = EntityCondition.makeCondition(
					EntityUtil.getFilterByDateExpr()
					);
			conditions.add(orderFieldConditions);*/
			EntityCondition mainConditons = EntityCondition.makeCondition(conditions, EntityOperator.AND);
			EntityFindOptions efo = new EntityFindOptions();
			efo.setDistinct(true);

			
			System.out.println(mainConditons+ " mainConditonsmainConditons");
			List <GenericValue> parties = delegator.findList("DataImportLeadSummary", mainConditons, UtilMisc.toSet("leadId"), null, null, false);
			
			System.out.println(parties+ " partiesparties");
			if (parties != null && parties.size() > 0) {
				List <String> partyIdList = EntityUtil.getFieldListFromEntityList(parties, "leadId", true);
				List<Map<String, Object>> extractList = FastList.newInstance();
				if(partyIdList.size() > 0) {
                    for(String leadId : partyIdList) {
						Map<String, Object> exportRow = new LinkedHashMap<String,Object>();
						
						GenericValue dataImportLeadSummary = EntityUtil.getFirst( delegator.findByAnd("DataImportLeadSummary", UtilMisc.toMap("leadId", leadId), null, false) );
						if (UtilValidate.isNotEmpty(dataImportLeadSummary)) {
						    exportRow.put("lead_id",leadId);
						    
						    ExportWrapper.getData(selectedFields, "batch_id", dataImportLeadSummary.getString("batchId"), exportRow);
							ExportWrapper.getData(selectedFields, "lead_id",  dataImportLeadSummary.getString("leadId"), exportRow);
							ExportWrapper.getData(selectedFields, "first_name", dataImportLeadSummary.getString("firstName"), exportRow);
							ExportWrapper.getData(selectedFields, "last_name", dataImportLeadSummary.getString("lastName"), exportRow);
							//ExportWrapper.getData(selectedFields, "attn_name", dataImportLeadSummary.getString("attnName"), exportRow);
							ExportWrapper.getData(selectedFields, "address1", dataImportLeadSummary.getString("address1"), exportRow);
							ExportWrapper.getData(selectedFields, "address2",  dataImportLeadSummary.getString("address2"), exportRow);
							ExportWrapper.getData(selectedFields, "city", dataImportLeadSummary.getString("city"), exportRow);
							ExportWrapper.getData(selectedFields, "state_province_geoid", dataImportLeadSummary.getString("stateProvinceGeoId"), exportRow);
							ExportWrapper.getData(selectedFields, "postal_code", dataImportLeadSummary.getString("postalCode"), exportRow);
							ExportWrapper.getData(selectedFields, "postal_code_ext", dataImportLeadSummary.getString("postalCodeExt"), exportRow);
							ExportWrapper.getData(selectedFields, "state_province_geo_name", dataImportLeadSummary.getString("stateProvinceGeoName"), exportRow);
							ExportWrapper.getData(selectedFields, "country_geo_id",dataImportLeadSummary.getString("countryGeoId"), exportRow);
							ExportWrapper.getData(selectedFields, "primary_phone_country_code", dataImportLeadSummary.getString("primaryPhoneCountryCode"), exportRow);
							ExportWrapper.getData(selectedFields, "primary_phone_area_code", dataImportLeadSummary.getString("primaryPhoneAreaCode"), exportRow);
							ExportWrapper.getData(selectedFields, "primary_phone_number", dataImportLeadSummary.getString("primaryPhoneNumber"), exportRow);
							ExportWrapper.getData(selectedFields, "primary_phone_extension", dataImportLeadSummary.getString("primaryPhoneExtension"), exportRow);
							ExportWrapper.getData(selectedFields, "secondary_phone_country_code", dataImportLeadSummary.getString("secondaryPhoneCountryCode"), exportRow);
							ExportWrapper.getData(selectedFields, "secondary_phone_area_code", dataImportLeadSummary.getString("secondaryPhoneAreaCode"), exportRow);
							ExportWrapper.getData(selectedFields, "secondary_phone_number", dataImportLeadSummary.getString("secondaryPhoneNumber"), exportRow);
							ExportWrapper.getData(selectedFields, "secondary_phone_extension", dataImportLeadSummary.getString("secondaryPhoneExtension"), exportRow);
							/*ExportWrapper.getData(selectedFields, "fax_country_code", dataImportLeadSummary.getString("faxCountryCode"), exportRow);
							ExportWrapper.getData(selectedFields, "fax_area_code", dataImportLeadSummary.getString("faxAreaCode"), exportRow);
							ExportWrapper.getData(selectedFields, "fax_number", dataImportLeadSummary.getString("faxNumber"), exportRow);
							ExportWrapper.getData(selectedFields, "did_country_code", dataImportLeadSummary.getString("didCountryCode"), exportRow);
							ExportWrapper.getData(selectedFields, "did_area_code", dataImportLeadSummary.getString("didAreaCode"), exportRow);
							ExportWrapper.getData(selectedFields, "did_number", dataImportLeadSummary.getString("didNumber"), exportRow);
							ExportWrapper.getData(selectedFields, "did_extension", dataImportLeadSummary.getString("didExtension"), exportRow);*/
							ExportWrapper.getData(selectedFields, "email_address", dataImportLeadSummary.getString("emailAddress"), exportRow);
							/*ExportWrapper.getData(selectedFields, "web_address", dataImportLeadSummary.getString("webAddress"), exportRow);
							ExportWrapper.getData(selectedFields, "note", dataImportLeadSummary.getString("note"), exportRow);
							ExportWrapper.getData(selectedFields, "source", dataImportLeadSummary.getString("source"), exportRow);*/
							ExportWrapper.getData(selectedFields, "import_status_id", dataImportLeadSummary.getString("importStatusId"), exportRow);
							ExportWrapper.getData(selectedFields, "import_error", dataImportLeadSummary.getString("importError"), exportRow);
							ExportWrapper.getData(selectedFields, "primary_party_id", dataImportLeadSummary.getString("primaryPartyId"), exportRow);
							ExportWrapper.getData(selectedFields, "company_name", dataImportLeadSummary.getString("companyName"), exportRow);
							ExportWrapper.getData(selectedFields, "parent_co_details", dataImportLeadSummary.getString("parentCoDetails"), exportRow);
							ExportWrapper.getData(selectedFields, "sales_turn_over", dataImportLeadSummary.getString("salesTurnover"), exportRow);
							ExportWrapper.getData(selectedFields, "date_of_incorporation", dataImportLeadSummary.getString("dateOfIncorporation"), exportRow);
							ExportWrapper.getData(selectedFields, "constitution", dataImportLeadSummary.getString("constitution"), exportRow);
							ExportWrapper.getData(selectedFields, "industry_cat", dataImportLeadSummary.getString("industryCat"), exportRow);
							ExportWrapper.getData(selectedFields, "industry", dataImportLeadSummary.getString("industry"), exportRow);
							ExportWrapper.getData(selectedFields, "customer_trading_type", dataImportLeadSummary.getString("customerTradingType"), exportRow);
							ExportWrapper.getData(selectedFields, "tally_user_type", dataImportLeadSummary.getString("tallyUserType"), exportRow);
							ExportWrapper.getData(selectedFields, "tcp_name", dataImportLeadSummary.getString("tcpName"), exportRow);
							ExportWrapper.getData(selectedFields, "key_contact_person1", dataImportLeadSummary.getString("keyContactPerson1"), exportRow);
							ExportWrapper.getData(selectedFields, "key_contact_person2", dataImportLeadSummary.getString("keyContactPerson2"), exportRow);
							ExportWrapper.getData(selectedFields, "permanent_acccount_number", dataImportLeadSummary.getString("permanentAcccountNumber"), exportRow);
							ExportWrapper.getData(selectedFields, "business_reg_no", dataImportLeadSummary.getString("businessRegNo"), exportRow);
							ExportWrapper.getData(selectedFields, "other_bank_name", dataImportLeadSummary.getString("otherBankName"), exportRow);
							ExportWrapper.getData(selectedFields, "other_bank_balance", dataImportLeadSummary.getString("otherBankBalance"), exportRow);
							
							ExportWrapper.getData(selectedFields, "products_held_in_oth_bank", dataImportLeadSummary.getString("productsHeldInOthBank"), exportRow);
							ExportWrapper.getData(selectedFields, "products_value_in_oth_bank", dataImportLeadSummary.getString("productsValueInOthBank"), exportRow);
							ExportWrapper.getData(selectedFields, "paidup_capital", dataImportLeadSummary.getString("paidupCapital"), exportRow);
							ExportWrapper.getData(selectedFields, "authorised_cap", dataImportLeadSummary.getString("authorisedCap"), exportRow);
							ExportWrapper.getData(selectedFields, "lead_assign_to", dataImportLeadSummary.getString("leadAssignTo"), exportRow);
							ExportWrapper.getData(selectedFields, "lead_assign_by", dataImportLeadSummary.getString("leadAssignBy"), exportRow);
							ExportWrapper.getData(selectedFields, "segment", dataImportLeadSummary.getString("segment"), exportRow);
							ExportWrapper.getData(selectedFields, "liab_or_asset", dataImportLeadSummary.getString("liabOrAsset"), exportRow);
							ExportWrapper.getData(selectedFields, "tele_calling_status", dataImportLeadSummary.getString("teleCallingStatus"), exportRow);
							ExportWrapper.getData(selectedFields, "tele_calling_sub_status", dataImportLeadSummary.getString("teleCallingSubStatus"), exportRow);
							ExportWrapper.getData(selectedFields, "tele_calling_remarks", dataImportLeadSummary.getString("teleCallingRemarks"), exportRow);
							ExportWrapper.getData(selectedFields, "rm_calling_status", dataImportLeadSummary.getString("rmCallingStatus"), exportRow);
							ExportWrapper.getData(selectedFields, "rm_calling_sub_status", dataImportLeadSummary.getString("rmCallingSubStatus"), exportRow);
							ExportWrapper.getData(selectedFields, "rm_calling_remarks", dataImportLeadSummary.getString("rmCallingRemarks"), exportRow);
							ExportWrapper.getData(selectedFields, "title", dataImportLeadSummary.getString("title"), exportRow);
							ExportWrapper.getData(selectedFields, "no_of_attempt", dataImportLeadSummary.getString("noOfAttempt"), exportRow);
							ExportWrapper.getData(selectedFields, "finacle_id", dataImportLeadSummary.getString("finacleId"), exportRow);
							ExportWrapper.getData(selectedFields, "place_of_incorporation", dataImportLeadSummary.getString("placeOfIncorporation"), exportRow);
							ExportWrapper.getData(selectedFields, "no_of_employees", dataImportLeadSummary.getString("noOfEmployees"), exportRow);
							
							ExportWrapper.getData(selectedFields, "job_family", dataImportLeadSummary.getString("jobFamily"), exportRow);
							ExportWrapper.getData(selectedFields, "designation", dataImportLeadSummary.getString("designation"), exportRow);
							ExportWrapper.getData(selectedFields, "lead_status", dataImportLeadSummary.getString("leadStatus"), exportRow);
							ExportWrapper.getData(selectedFields, "error_codes", dataImportLeadSummary.getString("errorCodes"), exportRow);
							/*ExportWrapper.getData(selectedFields, "approved_by_user_login_id", dataImportLeadSummary.getString("approvedByUserLoginId"), exportRow);
							ExportWrapper.getData(selectedFields, "rejected_by_user_login_id", dataImportLeadSummary.getString("rejectedByUserLoginId"), exportRow);
							ExportWrapper.getData(selectedFields, "uploaded_by_user_login_id", dataImportLeadSummary.getString("uploadedByUserLoginId"), exportRow);*/
							ExportWrapper.getData(selectedFields, "is_dedup_auto_segmentation", dataImportLeadSummary.getString("isDedupAutoSegmentation"), exportRow);
							

							extractList.add(exportRow);
							System.out.println(exportRow+ " exportRowexportRowexportRow");
							System.out.println(extractList+ " extractListextractList");
						}
					}
				}
				
				if(extractList.size() > 0) {
					
					String fileName = "LeadErrorExport_"+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+ExportUtil.getFileExtension(exportType);
					String location = ComponentConfig.getRootLocation("crm")+"/webapp/crm-resource/template";
					String delimiter = ",";
					
					Map<String, Object> exportContext = new HashMap<String, Object>();
					
					exportContext.put("delegator", delegator);
					exportContext.put("rows", extractList);
					exportContext.put("headers", null);
					exportContext.put("fileName", fileName);
					exportContext.put("location", location);
					exportContext.put("delimiter", delimiter);
					exportContext.put("isHeaderRequird", true);
					
					exportContext.put("exportType", exportType);
					
					ExporterFacade.exportReport(exportContext);
					
					Thread.sleep(1000);
					
					String filePath = location+File.separatorChar+fileName;
					
					ExporterFacade.downloadReport(request, response, filePath, exportType);
					
					boolean isdelete = true;
					if(isdelete) {
						File file = new File(filePath);
						file.delete();
					}
				}
			}


		} catch (Exception e) {
			Debug.logError("Error : "+e.getMessage(), module);
			return "error";
		}

		return "success";
	}
}
