package org.fio.crm.common;

import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.GeneralException;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

public class CustomFieldProcess {
	public static Map<String, Object> createUpdateCustomValueMulti(DispatchContext dctx, Map<String, Object> context) {
		
		LocalDispatcher dispatcher = dctx.getDispatcher();
		String CustomFieldId = (String) context.get("customFieldId");
        String CustomFieldValue = (String) context.get("customFieldValue");
        String partyId = (String) context.get("partyId");
		String action = (String)context.get("action");
		try{
		 String serviceName = null;
		  if("CREATE".equals(action)){
		     serviceName = "createFieldValue";
		  }else if("UPDATE".equals(action)){
		    serviceName = "updateFieldValue";
		  }
		     if(UtilValidate.isNotEmpty(CustomFieldId) && UtilValidate.isNotEmpty(CustomFieldValue) && UtilValidate.isNotEmpty(serviceName)){
			  ModelService createUpdateField = dctx.getModelService(serviceName);
			  Map<String,Object> input = createUpdateField.makeValid(context,ModelService.IN_PARAM);
			  
			  Map<String, Object>  serviceResults = dispatcher.runSync(serviceName, input);
		   }
		  //}
		}catch(Exception ex){
			Debug.logInfo("==========================ERROR======================"+ex.toString(), "");
		}
		
		Map<String,Object> result = ServiceUtil.returnSuccess();
		//result.put("partyId",partyId);
	
	    return result;
	}
	
	public static Map<String, Object> createFieldValue(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		String CustomFieldId = (String) context.get("customFieldId");
        String CustomFieldValue = (String) context.get("customFieldValue");
        String partyId = (String) context.get("partyId");
		
	    Debug.logInfo("contextgottttttcreateFioFieldValue"+context, "");
        try {
			GenericValue CustomFieldTemplate=delegator.makeValue("CustomFieldValue",UtilMisc.toMap("customFieldId",CustomFieldId, "partyId", partyId));
			CustomFieldTemplate.set("fieldValue", CustomFieldValue);
			CustomFieldTemplate.create();
        } catch (GeneralException e) {
            Debug.logError(e, "");
            return ServiceUtil.returnError("ErrorwithService: " + e.toString());
        }

        return ServiceUtil.returnSuccess();
    }
	
	public static Map<String, Object> updateFieldValue(DispatchContext dctx, Map<String, Object> context) {
		Delegator delegator = dctx.getDelegator();
		String CustomFieldId = (String) context.get("customFieldId");
        String CustomFieldValue = (String) context.get("customFieldValue");
        String partyId = (String) context.get("partyId");
		
	    Debug.logInfo("contextgottttttupdateFioFieldValue"+context, "");

        try {
			GenericValue updateCustomField = delegator.findOne("CustomFieldValue",UtilMisc.toMap("customFieldId",CustomFieldId,"partyId",partyId), false);

			//GenericValue FioCustomField=delegator.findByPrimaryKey("CustomField",UtilMisc.toMap("CustomFieldId",fioCustomFieldId, "partyId", partyId));		
			updateCustomField.put("fieldValue", CustomFieldValue);
			updateCustomField.store();		
        } catch (GeneralException e) {
             Debug.logError(e, "");
            //return ServiceUtil.returnError("ErrorwithService: " + e.toString());
        }

        Map returnMap = ServiceUtil.returnSuccess();
        //returnMap.put("partyId",partyId);
        
        return returnMap;
	}	

}
