<div class="page-header">
  <h2 class="float-left">Opportunities</h2>
</div>

<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<ul class="nav nav-tabs">
	<#-- <li class="nav-item" id="assigned-customer-tab"><a data-toggle="tab" href="#tab1"
		class="nav-link active">${uiLabelMap.AssignedCustomer!}</a></li> -->
		<li class="nav-item"><a data-toggle="tab" href="#notes" class="nav-link active">Notes</a></li>
		<li class="nav-item"><a data-toggle="tab" href="#logCall" class="nav-link">${uiLabelMap.logCall}</a></li>
	<#--<li class="nav-item"><a data-toggle="tab" href="#hadoop-tab1"
		class="nav-link active">CaAccount Data</a></li> -->
						
</ul>

<div class="tab-content">
	
	<div id="notes" class="tab-pane fade show active">		
		  ${screens.render("component://crm/webapp/widget/crm/screens/account/AccountScreens.xml#note")}		
	</div>
	
	<div id="logCall" class="tab-pane fade">
         ${screens.render("component://crm/webapp/widget/crm/screens/common/CommonScreens.xml#callDetails")}
      </div>
	
	
</div>

