<div class="page-header">
   <h2 class="float-left">Campaign Details</h2>
</div>
<#assign requestURII = request.getRequestURI()/>
<#assign requestt = requestURII+"?" />

<div class="table-responsive">
   <table id="crmCampaignDetailsTable" class="table table-striped">
      <thead>
         <tr>
            <th>Campaign Name</th>
            <th>Campaign Type </th>
            <#if requestt?contains("viewContact?")>
            <th>Account Name</th>
            </#if>
            <th>${uiLabelMap.opened!}</th>
            <th>${uiLabelMap.notOpened!}</th>
            <th>${uiLabelMap.clicked!}</th>
            <th>${uiLabelMap.converted!}</th>
            <th>${uiLabelMap.subscribed!}</th>
            <th>${uiLabelMap.unsubscribed!}</th>
            <th>${uiLabelMap.bounced!}</th>
            <th></th>
         </tr>
      </thead>

   </table>
</div>


<script type="text/javascript">



	function viewNote(campaignNoteId){
		//$('#noteCreateUpdate').modal("show");
		//var campaignNoteId = campaignNoteId;
		$('#campaignNoteId').val( campaignNoteId );
	}
	
jQuery(document).ready(function() {
$("a[href='#campaignDetails']").on('shown.bs.tab', function(e) {
	 var currPartyId = $("#partyId").val();  
     <#if requestt?contains("viewAccount?")>
     var URL = 'getAcctCampaignDetails';
     findAcctCampaignDetails(currPartyId,URL);
     <#elseif requestt?contains("viewContact?")>
      var URL = 'getPartyCampaignDetails';
     findPartyCampaignDetails(currPartyId,URL);
     </#if>    
});
 
	 
});



    function findPartyCampaignDetails(currPartyId,URL) {
    //alert("hia");
        $('#crmCampaignDetailsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": {
                "url": URL,
                "type": "POST",
                data: {"partyId":currPartyId},
            },
            "paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                   "data": "contactListId",
                   "render": function(data, type, row, meta) {
                        data = row.contactListId + ' (' + row.campaignId + ')';
                        return data;
                    }
                },
                {  "orderable": false,
                   "data": "campaignTypeDesc"
                },
                {  "orderable": false,
                   "data": "accountName",
                   "render": function(data, type, row, meta) {
                       var actId = row.accountId;
                       if (actId != "null" && actId != ""  && actId != undefined){
                        data = row.accountName + ' (' + row.accountId + ')';
                        }else{
                        data = "N/A";
                        }
                        return data;
                    }
                 
                },
                {   "orderable": false,
                   "data": "opened"
                },
                {   "orderable": false,
                   "data": "notOpen"
                },
                {  "orderable": false,
                   "data": "clickCount"
                },
                {   "orderable": false,
                   "data": "converted"
                },
                {   "orderable": false,
                   "data": "subscribe"
                },
                {   "orderable": false,
                   "data": "unSubscribe"
                },
                {   "orderable": false,
                   "data": "bounced"
                },
                {
                    "orderable": false,
                    "data": "campaignId",
                    "render": function(data, type, row, meta) {
                        data = '<a onclick=viewNote("' + row.campaignId + '")>' + '<span class="fa fa-sticky-note btn btn-xs btn-primary tooltips create-campaignNote" data-toggle="modal" href="#noteCreateUpdate" alt="Note" title="Note"></span></a>';
                        return data;
                    }
                }
            ]
        });
    }  

    function findAcctCampaignDetails(currPartyId,URL) {
     //alert("hiacc");
        $('#crmCampaignDetailsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": {
                "url": URL,
                "type": "POST",
                data: {"partyId":currPartyId},
            },
            "paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                   "data": "contactListId",
                    "render": function(data, type, row, meta) {
                        data = row.contactListId + ' (' + row.campaignId + ')';
                        return data;
                    }
                },
                {  "orderable": false,
                   "data": "campaignTypeDesc"
                },
                {   "orderable": false,
                   "data": "opened"
                },
                {   "orderable": false,
                   "data": "notOpen"
                },
                {  "orderable": false,
                   "data": "clickCount"
                },
                {   "orderable": false,
                   "data": "converted"
                },
                {   "orderable": false,
                   "data": "subscribe"
                },
                {   "orderable": false,
                   "data": "unSubscribe"
                },
                {   "orderable": false,
                   "data": "bounced"
                },
                {
                    "orderable": false,
                    "data": "campaignId",
                    "render": function(data, type, row, meta) {
                        data = '<a onclick=viewNote("' + row.campaignId + '")>' + '<span class="fa fa-sticky-note btn btn-xs btn-primary tooltips create-campaignNote" data-toggle="modal" href="#noteCreateUpdate" alt="Note" title="Note"></span></a>';
                        return data;
                    }
                }
            ]
        });
    }    
  




</script>

		
