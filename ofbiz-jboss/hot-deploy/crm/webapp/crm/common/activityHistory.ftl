<div class="page-header">
   <h2 class="float-left">Call Log</h2>
   <div class="float-right">
     <#assign requestURI = "viewContact"/> 
     <#if request.getRequestURI().contains("viewLead")>
     <#assign requestURI = "viewLead"/>
     <#elseif request.getRequestURI().contains("viewAccount")>
     <#assign requestURI = "viewAccount"/>
     </#if>
      <span class="btn btn-xs btn-primary m5" data-toggle="modal" data-target="#callLogModal" data-original-title="Log Call">Call Log</span>
      <#-- <span class="btn btn-xs btn-primary m5" data-toggle="modal" data-target="#emailLogModal" data-original-title="Log Call">Log Email</span> -->
      <#--<a class="btn btn-xs btn-primary m5" href="logCall?partyId=${partyId?if_exists}&amp;workEffortPurposeTypeId=WEPT_TASK_PHONE_CALL&amp;donePageCallLog=${requestURI?if_exists}">Log Call</a>
      <a class="btn btn-xs btn-primary m5" href="logCall?partyId=${partyId?if_exists}&amp;workEffortPurposeTypeId=WEPT_TASK_EMAIL&amp;donePageCallLog=${requestURI?if_exists}">Log Email</a> -->
   </div>
</div>
<div class="table-responsive">
   <table class="table table-striped" id="callDetailsTable">
      <thead>
         <tr>
            <th>Subject</th>
            <th>Purpose</th>
            <th>Message</th>
            <th>Created Date</th>
            <th>Created By</th>
            <th>Remove</th>
         </tr>
      </thead>
      <tbody>
         <#if activityHistoryList?exists && activityHistoryList?has_content>
         <#list activityHistoryList as activityHistory>
         <tr>
            <td>${activityHistory.workEffortName?if_exists}</td>
            <td>${activityHistory.workEffortPurposeTypeId?if_exists}</td>
            <td>${activityHistory.content?if_exists}</td>
            <td>${activityHistory.createdDate?if_exists}</td>
            <td>${activityHistory.createdByUserLogin?if_exists}</td>
            <td>
               <form method="post" action="deleteCallLog" name="deleteCallLogs_${activityHistory.workEffortId?if_exists}">
                  <input name="workEffortId" value="${activityHistory.workEffortId?if_exists}" type="hidden">
                  <input name="partyId" value="${activityHistory.partyId?if_exists}" type="hidden">
                  <input name="donePage" value="${requestURI}" type="hidden">
                  <input type="hidden" name="activeTab" value="opportunites" />
               </form>
               <a class="btn btn-xs btn-secondary btn-danger tooltips confirm-message" href="javascript:document.deleteCallLogs_${activityHistory.workEffortId?if_exists}.submit();" data-original-title="${uiLabelMap.delete}"><i class="fa fa-times red"></i></a>
            </td>
         </tr>
         </#list>
         </#if>
      </tbody>
   </table>
</div>
<script type="text/javascript" >
   $(document).ready(function() {
       $('#callDetailsTable').DataTable({
       destroy: true,
          "order": [[ 0, "desc" ]]
       });
   } );
</script>