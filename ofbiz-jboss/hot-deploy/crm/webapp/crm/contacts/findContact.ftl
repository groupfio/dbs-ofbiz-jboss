<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header border-b">
   <h1>Find Contacts</h1>
</div>
<div class="card-header">
   <#-- 
   <form method="post" action="findContact" id="findContact" class="form-horizontal" name="findContact" novalidate="novalidate" data-toggle="validator">
      -->
      <div class="row">
         <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <input type="text" class="form-control input-sm" name="partyId" id="partyId" placeholder="Contact ID" value="${partyId?if_exists}">
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <input type="text" class="form-control input-sm" id="firstName" name="firstName" placeholder="First Name" value="${firstName?if_exists}">
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <input type="text" class="form-control input-sm" id="lastName" name="lastName" placeholder="${uiLabelMap.lastName}" value="${lastName?if_exists}">
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <input type="text" class="form-control input-sm" name="emailAddress" id="emailAddress" placeholder="Email Address" value="${emailAddress?if_exists}">
            </div>
         </div>
         <div class="col-md-2 col-sm-2">
            <div class="form-group row mr">
               <input type="text" class="form-control input-sm" name="contactNumber" id="contactNumber" placeholder="Phone Number" value="${contactNumber?if_exists}">
            </div>
         </div>
         <@fromSimpleAction id="findContacts" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
         <#--
         <div class="col-md-1 col-sm-1 pl-0">
            <input type="submit" class="btn btn-sm btn-primary" value="Find"/>
         </div>
         -->
      </div>
      <#-- 
   </form>
   -->
   <div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>
<div class="page-header">
   <h2 class="float-left">Contacts List </h2>
</div>
<div class="table-responsive">
   <table id="findContactsTable" class="table table-striped">
      <thead>
         <tr>
            <th>${uiLabelMap.title}</th>
            <th>${uiLabelMap.crmContact} ${uiLabelMap.name}</th>
            <th>${uiLabelMap.accountName!}</th>
            <#-- <#if groupList?has_content>
            <#list groupList as groupList>
            <th>
               <div class="text-right mr-4">${groupList.customFieldName}</div>
            </th>
            </#list>
            </#if>-->
            <th>LCIN</th>
            <th>CIN</th>
            <th>${uiLabelMap.status}</th>
            <th>${uiLabelMap.phoneNumber}</th>
            <th>${uiLabelMap.email}</th>
         </tr>
      </thead>
      <#-- >
      <tbody>
         <#if findList?has_content>
         <#list findList as contactList>
         <tr>
            <td>${contactList?if_exists.generalProfTitle?if_exists}</td>
            <td><a href="viewContact?partyId=${contactList?if_exists.partyId?if_exists}">
               ${contactList?if_exists.name?if_exists}(${contactList?if_exists.partyId?if_exists})</a>
            </td>
            <td>${contactList?if_exists.companyName?if_exists}</td>
            <#if groupList?has_content>
            <#list groupList as groupList>
            <#assign fieldValue = delegator.findOne("CustomFieldValue",{"customFieldId":groupList.customFieldId?if_exists,"partyId":contactList?if_exists.partyId?if_exists},true)?if_exists />
            <#if fieldValue?has_content>
            <td>
               <div class="text-right mr-4">${fieldValue?if_exists.fieldValue?if_exists}</div>
            </td>
            <#else>
            <td></td>
            </#if> 
            </#list>
            </#if>
            <td>${contactList?if_exists.statusItemDesc?if_exists}</td>
            <td>${contactList?if_exists.contactNumber?if_exists}</td>
            <td>${contactList?if_exists.emailAddress?if_exists}</td>
         </tr>
         </#list>
         </#if>
      </tbody>
      -->
   </table>
</div>
</div>
</div>
<script>
   $(document).ready(function() {
       loadFindContacts();
   });
   $('#findContacts').on('click',function(){
   	loadFindContacts();
   });
   function loadFindContacts(findContactsUrl) {
   	var partyId = $("#partyId").val();
       var firstName = $("#firstName").val();
       var lastName = $("#lastName").val();
       var emailAddress = $("#emailAddress").val();
       var contactNumber = $("#contactNumber").val();
       var findContactsUrl = "getContactDetails?partyId=" + partyId + "&firstName=" + firstName + "&lastName=" + lastName + "&emailAddress=" + emailAddress+ "&contactNumber=" + contactNumber;
       oTable = $('#findContactsTable').DataTable({
           "processing": true,
           "serverSide": true,
           "searching": false,
           "destroy": true,
           "ordering": true,
           "ajax": {
               "url": findContactsUrl,
               "type": "POST"
           },
           "Paginate": true,
           "columnDefs": [
				{
	                "orderable": false,
	                "targets": [2]
	            },
	            {
	                "orderable": false,
	                "targets": [3]
	            },
            ],
           "order": [[ 1, "ASC" ]],
           "language": {
               "emptyTable": "No data available in table",
               "info": "Showing _START_ to _END_ of _TOTAL_ entries",
               "infoEmpty": "No entries found",
               "infoFiltered": "(filtered1 from _MAX_ total entries)",
               "lengthMenu": "Show _MENU_ entries",
               "zeroRecords": "No matching records found",
               "oPaginate": {
                   "sNext": "Next",
                   "sPrevious": "Previous"
               }
           },
   
           "pageLength": 10,
           "bAutoWidth": false,
           "stateSave": false,
           "columns": [
               {
                   "data": "generalProfTitle"
               },
               {
                   "data": "partyId",
                   "render": function(data, type, row, meta) {
                       var partyId = row.partyId;
                       if (partyId != null && partyId != "" && partyId != undefined) {
                           data = '<a href="/crm/control/viewContact?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.name + '(' +  row.partyId + ')</a>';
                       }
                       return data;
                   }
               },
               {
                   "data": "groupName"
               },
               {
                   "data": "partyIdTo"
               },
               {
                   "data": "cin",
                   "orderable": false
                   
               },
               {
                   "data": "statusDescription"
               },
               {
                   "data": "contactNumber"
               },
               {
                   "data": "infoString"
               }
           ]
       });
   }
</script>