<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

        <div class="page-header border-b">
          <h1 class="float-left">Update Contact</h1>
        </div>
        <form method="post" action="updateContact" id="updateContactForm" class="form-horizontal" name="updateContactForm" novalidate="novalidate" data-toggle="validator">
          <input type="hidden" value="${partySummary?if_exists.partyId?if_exists}" name ="partyId">
		  <div class="row padding-r">
          <div class="col-md-6 col-sm-6">
            <@generalInput 
			         id="firstName"
		             label=uiLabelMap.firstName
			         placeholder=uiLabelMap.firstName
			         value="${partySummary?if_exists.firstName?if_exists}"
			         required=true
			         maxlength="100"
			         />
            
            <#-- <div class="form-group row row">
              <label class="col-sm-4 col-form-label">${uiLabelMap.salutation}</label>
              <div class="col-sm-7">
               <input type="text" class="form-control input-sm" id="personalTitle" name="personalTitle" value="${partySummary?if_exists.personalTitle?if_exists}" placeholder="Salutation"> 
              </div>
            </div> -->
            <@dropDown 
				id = "personalTitle"
				label = uiLabelMap.salutation
				options = salutationList
				value = "${partySummary?if_exists.personalTitle?if_exists}"
				allowEmpty=true
				dataLiveSearch = true
				/>
            <#--<div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.title}</label>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name ="generalProfTitle" id="generalProfTitle" value="${partySummary?if_exists.generalProfTitle?if_exists}" placeholder="Title">
              </div>
            </div>-->
            <div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.department}</label>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="departmentName" id="departmentName" value="${partySummary?if_exists.departmentName?if_exists}" placeholder="Department">
              </div>
            </div>
            <#--
            <div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.classification}</label>
               <#assign classifications = delegator.findByAnd("PartyClassificationGroup",{"partyClassificationTypeId","CUST_CLASSIFICATION"},Static["org.ofbiz.base.util.UtilMisc"].toList("description"), false)?if_exists/>
               <div class="col-sm-7">
              <select name="partyClassificationGroupId" id="partyClassificationGroupId" class="ui dropdown form-control input-sm" data-live-search="true" >
                <option value="">---Select---</option>
               <#if classifications?has_content>
			   	<#list classifications as classification>
			   		<option value="${classification.partyClassificationGroupId}" <#if requestParameters.partyClassificationGroupId?if_exists = classification.partyClassificationGroupId>selected<#elseif partyClassificationGroupId?if_exists = classification.partyClassificationGroupId>selected</#if>>${classification.description}</option>   
				</#list>
				</#if>
              </select>
              </div>
            </div>
             -->
             <#assign classifications = delegator.findByAnd("CustomFieldPartyClassification",{"groupId","GENDER","partyId","${partySummary?if_exists.partyId?if_exists}"},[], false)?if_exists/>
             <#if classifications?exists && classifications?has_content>
             	<#assign gender = classifications.get(0).customFieldId?if_exists />
             </#if>
             <@dropDown 
				id = "gender"
				label = uiLabelMap.gender
				options = genderList
				value = gender?if_exists
				allowEmpty=true
				dataLiveSearch = true
				/>
            <div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.description}</label>
              <div class="col-sm-7">
			   <textarea name ="description" id="description" rows="3" placeholder="Description" class="form-control" >${partySummary?if_exists.description?if_exists}</textarea>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
           <@generalInput 
			         id="lastName"
		             label=uiLabelMap.lastName
			         placeholder=uiLabelMap.lastName
			         value="${partySummary?if_exists.lastName?if_exists}"
			         required=false
			         maxlength="100"
			         />
            <div class="form-group row row">
              <label for="inputEmail3" class="col-sm-4 col-form-label">${uiLabelMap.birthDate}</label>
              <div class="col-sm-7">
                <div class="input-group date" id="datetimepicker7">
                <input type='text' class="form-control input-sm" placeholder="YYYY-MM-DD" value="<#if partySummary.birthDate?has_content>${partySummary.birthDate?if_exists?string["yyyy-MM-dd"]}</#if>" data-date-format="YYYY-MM-DD" id="birthDate" name="birthDate"/>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <#--
            <div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.preferredCurrency}</label>
              <div class="col-sm-7">
	            <#assign currencies = delegator.findByAnd("Uom",{"uomTypeId","CURRENCY_MEASURE"},Static["org.ofbiz.base.util.UtilMisc"].toList("abbreviation"), false)?if_exists/>
	              <select class="ui dropdown form-control input-sm" data-live-search="true" id="preferredCurrencyUomId" name="preferredCurrencyUomId">
	                <option value="">---Select---</option>
	                <#if currencies?has_content>
	                <#list currencies as currency>
	                    <option value="${currency.uomId}" <#if currencyUomId?if_exists == currency.uomId>selected<#elseif partySummary.preferredCurrencyUomId?if_exists == currency.uomId>selected</#if>>${currency.description}</option>
	                </#list>
	                </#if>
	              </select>
	            </div>
	        </div> -->
           
            <div class="form-group row row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.note}</label>
              <div class="col-sm-7">
                <textarea name="importantNote" id="importantNote"  rows="3" placeholder="Note" class="form-control" >${partySummary?if_exists.importantNote?if_exists}</textarea>
              </div>
            </div>
          </div>
          </div>		  
            <div class="col-md-12 col-sm-12">
            <div class="form-group row row">
              <div class="offset-sm-2 col-sm-9">
              <button type="submit" class="btn btn-sm btn-primary mt" href="<@ofbizUrl>viewContact?partyId=${partySummary?if_exists.partyId?if_exists}</@ofbizUrl>">Back</button>
              <input type="submit" class="btn btn-sm btn-primary mt" value="Update"/>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- Marketing Campaign -->
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#mktCampaigns" aria-expanded="false" aria-controls="headingTwo">
            Marketing Campaigns
            </a>
          </h4>
        </div>
        <div id="mktCampaigns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
          <div class="panel-body">
            <#if marketingCampaigns?exists && marketingCampaigns?has_content>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Marketing Campaign</th>
                    <th>Remove</th>
                  </tr>
                </thead>
                <tbody>
                  <#assign j=0/>
                  <#list marketingCampaigns as campaign>
                  <tr>
                    <td><a href="viewMarketingCampaign?marketingCampaignId=${campaign.marketingCampaignId?if_exists}">${campaign.campaignName?if_exists}</a></td>
                    <td>
                      <form method="post" action="removeContactMarketingCampaign" name="removeMktCampaign_${j}" id="removeMktCampaign_${j}" >
                        <input type="hidden" name="partyId" id="partyId" value="${parameters.partyId?if_exists}"/>
                        <input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${campaign.marketingCampaignId?if_exists}"/>
                      </form>
                      <a href="javascript:document.removeMktCampaign_${j}.submit();" ><span class="glyphicon glyphicon-remove btn btn-xs btn-danger"></span></a>
                    </td>
                  </tr>
                  <#assign j = j+1>
                  </#list>
                </tbody>
              </table>
            </div>
            </#if>
            <form name="addContactMarketingCampaign" method="post" action="addContactMarketingCampaign">
              <div class="row padding-r">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group row has-error">
                    <label class="col-sm-4 col-form-label">New Marketing Campaign*</label>
                    <div class="col-sm-7">
                      <input type="hidden" name="partyId" id="partyId" value="${parameters.partyId!}"/>
                     <#assign marketingCampainList = delegator.findByAnd("MarketingCampaign", Static["org.ofbiz.base.util.UtilMisc"].toMap("statusId", "MKTG_CAMP_INPROGRESS"), Static["org.ofbiz.base.util.UtilMisc"].toList("campaignName ASC"),false) />
                      <select class="ui dropdown search form-control input-sm" name="marketingCampaignId" id="marketingCampaignId" >
                        <option value="" disabled selected>Select Campaign</option>
                        <#if marketingCampainList?has_content>
                        <#list marketingCampainList as marketingCampain>
                        <option value="${marketingCampain.marketingCampaignId}">${marketingCampain.campaignName}</option>
                        </#list>
                        </#if>
                      </select>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-4 col-sm-9">
                      <input type="submit" class="btn btn-sm btn-primary mt" value="${uiLabelMap.CommonAdd}"/>
                    </div>
                  </div>
                </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>

   