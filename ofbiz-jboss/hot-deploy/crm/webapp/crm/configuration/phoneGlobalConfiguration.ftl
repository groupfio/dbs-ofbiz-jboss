<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<@sectionHeader title="${uiLabelMap.globalParamSettings!}" />
<div class="row padding-r">
   <div class="col-md-6 col-sm-6">
      <div class="portlet-body form">
         <form method="post" action="<@ofbizUrl>updatePhoneGlobalParameter</@ofbizUrl>" id="updatePhoneGlobalParameter" class="form-horizontal" name="updatePhoneGlobalParameter" novalidate="novalidate" data-toggle="validator">
            <div class="form-body">
               <@radioButtonInput 
               name="outBoundCall"
               label="Enable Out Bound Call"
               options=optionMap
               value="Y"
               />
               <#if outBoundCall?has_content && outBoundCall == "Y">
               <@generalInput 
               id="maxCallNum"
               name="maxCallNum"
               label="Max Call To Customer"
               placeholder="Max Call To Customer"
               value="${maxCallNum?if_exists}"
               inputType="number"
               required=false
               />
               <@generalInput 
               id="hhMaxDays"
               name="hhMaxDays"
               label="Max Call To House Hold Id"
               placeholder="Max Call To House Hold Id"
               value="${hhMaxnum?if_exists}"
               inputType="number"
               required=false
               />
               <@generalInput 
               id="callDurationDays"
               name="callDurationDays"
               label="Next Call Interval"
               placeholder="Next Call Interval"
               value="${callDurationDays?if_exists}"
               inputType="number"
               required=false
               />
				<@generalInput 
				id="emailDurationDays"
				label=uiLabelMap.nextEmailInterval
				placeholder=uiLabelMap.nextEmailInterval
				value="${emailDuration?if_exists}"
				inputType="number"
				required=false
				min=1
				/>
               <@generalInput 
				id="smsDurationDays"
				label=uiLabelMap.nextSmsInterval
				placeholder=uiLabelMap.nextSmsInterval
				value="${smsDuration?if_exists}"
				inputType="number"
				required=false
				min=1
				/>
               </#if>
            </div>
            <@fromCommonAction showCancelBtn=false showClearBtn=true/>
         </form>
      </div>
   </div>
</div>