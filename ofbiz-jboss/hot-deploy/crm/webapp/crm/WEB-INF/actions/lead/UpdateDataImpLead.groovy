import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.fio.crm.util.DataHelper;
import org.fio.crm.util.EnumUtil;
import org.fio.crm.util.PermissionUtil;
import org.fio.crm.party.PartyHelper;
import org.fio.crm.constants.CrmConstants;
import org.fio.crm.util.VirtualTeamUtil;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("crmUiLabels", locale);

actionType = context.get("actionType");
context.put("actionType", actionType);

dataImportLead = new HashMap();

haveDataPermission = "Y";

leadId = UtilValidate.isNotEmpty(request.getParameter("leadId")) ? request.getParameter("leadId") : request.getParameter("partyId");
if (UtilValidate.isNotEmpty(leadId)) {

	cond = EntityCondition.makeCondition([
		EntityCondition.makeCondition("leadId", EntityOperator.EQUALS, leadId),
		EntityCondition.makeCondition("primaryPartyId", EntityOperator.EQUALS, leadId)
	], EntityOperator.OR);

	dataImportLead = EntityUtil.getFirst( delegator.findList("DataImportLead", cond, null, null, null, false) );
	
	//Login Based lead Filter
	String userLoginId = userLogin.getString("userLoginId");
	 userLoginSecurityGroup = EntityQuery.use(delegator).from("UserLoginSecurityGroup")
			.where("userLoginId", userLoginId, "groupId", org.ofbiz.base.util.UtilProperties.getPropertyValue("homeapps-config", "default.admin.group")).filterByDate().queryList();
	if ((userLoginSecurityGroup == null || userLoginSecurityGroup.size() < 1) && !PermissionUtil.havePartyViewPermission(delegator, session, leadId)) {
		haveDataPermission = "N";
	}
	
}
context.put("leadId", leadId);

context.put("dataImportLead", dataImportLead);
println("dataImportLead>>> "+dataImportLead);
context.put("haveDataPermission", haveDataPermission);

String leadName = PartyHelper.getPartyName(delegator, leadId, false);
if (UtilValidate.isEmpty(leadName)) {
	if (UtilValidate.isNotEmpty(dataImportLead.get("firstName"))) {
		leadName = dataImportLead.getString("firstName").concat( UtilValidate.isNotEmpty(dataImportLead.get("lastName")) ? " " + dataImportLead.get("lastName") : "" );
	} else if (UtilValidate.isNotEmpty(dataImportLead.get("firstName"))) {
		leadName = dataImportLead.get("lastName");
	}
}
context.put("leadName", leadName);
	
appStatus = new HashMap();
context.put("appStatus", appStatus);

appStatusList = UtilMisc.toMap("ACTIVATED", uiLabelMap.get("activated"), "DEACTIVATED", uiLabelMap.get("deActivated"));
context.put("appStatusList", appStatusList);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

customerTradingTypeList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "customerTradingType");
context.put("customerTradingTypeList", DataHelper.getDropDownOptions(customerTradingTypeList, "enumId", "description"));

tallyUserTypeList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "tallyUserType");
context.put("tallyUserTypeList", DataHelper.getDropDownOptions(tallyUserTypeList, "enumId", "description"));

liabOrAssetList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "liabOrAsset");
context.put("liabOrAssetList", DataHelper.getDropDownOptions(liabOrAssetList, "enumId", "description"));

tcpNameList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "tcpName");
context.put("tcpNameList", DataHelper.getDropDownOptions(tcpNameList, "enumId", "description"));

codeList = new LinkedHashMap<String, Object>();
if (dataImportLead != null && UtilValidate.isNotEmpty(dataImportLead.get("errorCodes"))) {
	
	errorCodeList = StringUtil.split(dataImportLead.getString("errorCodes"), ",");
	errorCodeList.each{ errorCode ->
		
		code = EntityUtil.getFirst( delegator.findByAnd("ErrorCode", UtilMisc.toMap("errorCodeId", errorCode, "ErrorCodeType", "LEAD_IMPORT"), null, false) );
		if (UtilValidate.isNotEmpty(code)) {
			
			toolTip = code.get("codeDescription");
			
			codeList.put(errorCode, toolTip);
		}
	}
	
}
context.put("codeList", codeList);

industryCatList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "industryCat");
context.put("industryCatList", DataHelper.getDropDownOptions(industryCatList, "enumId", "description"));

industryList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "industry");
industryOptions = new LinkedHashMap<String, Object>();
industryList.each{ industry ->
	industryOptions.put(industry.getString("enumId"), "("+industry.getString("enumCode")+") "+industry.getString("description"));
}
context.put("industryList", industryOptions);

indiaStateList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoId", userLogin.getString("countryGeoId"), "geoAssocTypeId", "REGIONS"), null, false);
context.put("indiaStateList", DataHelper.getDropDownOptions(indiaStateList, "geoIdTo", "geoName"));


callingStatusList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "callingStatus");
context.put("callingStatusList", DataHelper.getDropDownOptions(callingStatusList, "enumId", "description"));

rmCallStatusList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "rmCallStatus");
context.put("rmCallStatusList", DataHelper.getDropDownOptions(rmCallStatusList, "enumId", "description"));

teleCallStatusList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "teleCallStatus");
context.put("teleCallStatusList", DataHelper.getDropDownOptions(teleCallStatusList, "enumId", "description"));


constitutionList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "constitution");
context.put("constitutionList", DataHelper.getDropDownOptions(constitutionList, "enumId", "description"));

incorporationPlaceList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "incorporationPlace");
context.put("incorporationPlaceList", DataHelper.getDropDownOptions(incorporationPlaceList, "enumId", "description"));

prodPhobList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "prodPhob");
context.put("prodPhobList", DataHelper.getDropDownOptions(prodPhobList, "enumId", "description"));

existingBankList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "existingBank");
context.put("existingBankList", DataHelper.getDropDownOptions(existingBankList, "enumId", "description"));

designationList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "leadDesignation");
context.put("designationList", DataHelper.getDropDownOptions(designationList, "enumId", "description"));

leadScoreList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "leadScore");
context.put("leadScoreList", DataHelper.getDropDownOptions(leadScoreList, "enumId", "description"));

backUrl = request.getParameter("backUrl");
if (UtilValidate.isEmpty(backUrl) && (UtilValidate.isNotEmpty(actionType) && actionType.equals("UPDATE"))) {
	backUrl = "viewLead?partyId="+leadId;
}
context.put("backUrl", backUrl);

leadSourceList = new LinkedHashMap<String, Object>();

sourceList = delegator.findByAnd("PartyIdentificationType", UtilMisc.toMap("parentTypeId", "LEAD_SOURCE"), UtilMisc.toList("partyIdentificationTypeId"), false);
sourceList.each{ source ->
	leadSourceList.put(source.getString("partyIdentificationTypeId"), "("+source.getString("partyIdentificationTypeId")+") "+source.getString("description"));
}
context.put("leadSourceList", leadSourceList);

segmentList = new LinkedHashMap<String, Object>();

segmentList.put("IBG3", "IBG3");
segmentList.put("IBG4", "IBG4");

context.put("segmentList", segmentList);

jobFamilyList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "jobFamily");
context.put("jobFamilyList", DataHelper.getDropDownOptions(jobFamilyList, "enumId", "description"));

sourceList = delegator.findByAnd("PartyIdentificationType", UtilMisc.toMap("parentTypeId", "LEAD_SOURCE"), UtilMisc.toList("partyIdentificationTypeId"), false);
sourceList.each{ source ->
	leadSourceList.put(source.getString("partyIdentificationTypeId"), "("+source.getString("partyIdentificationTypeId")+") "+source.getString("description"));
}
context.put("leadSourceList", leadSourceList);
/*
cityList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "city");
context.put("cityList", DataHelper.getDropDownOptions(cityList, "enumId", "description"));
*/
/*
condition = UtilMisc.toMap("groupType", "SEGMENTATION");
condition.put("roleTypeId", "LEAD");
cond = EntityCondition.makeCondition(condition);
println("cond>>>> "+cond);
segmentValueList = delegator.findList("CustomFieldGroupSummary", cond, null, ["sequenceNumber"], null, false);
context.put("segmentValueList", DataHelper.getDropDownOptions(segmentValueList, "customFieldId", "customFieldName"));
*/

titles = delegator.findByAnd("Enumeration", UtilMisc.toMap("enumTypeId", "SALUTATION"), UtilMisc.toList("sequenceId"), false);
context.put("titleList", DataHelper.getDropDownOptions(titles,"enumId", "description"));

context.put("otherUserPositionType", true);
loggedUserPositionType = DataHelper.getEmployeePositionType(delegator, userLogin.getString("partyId"), userLogin.getString("countryGeoId"));
context.put("loggedUserPositionType", loggedUserPositionType);
if (UtilValidate.isNotEmpty(loggedUserPositionType) && (loggedUserPositionType.equals("DBS_TC") || DataHelper.getFirstValidRoleTypeId(loggedUserPositionType, CrmConstants.RM_ROLES))) {
	context.put("otherUserPositionType", false);
}

//city
cityList = delegator.findByAnd("GeoAssocSummary", UtilMisc.toMap("geoTypeId", "CITY"), null, false);
context.put("cityList", DataHelper.getDropDownOptions(cityList, "geoIdTo", "geoName"));
//Constitution
constitutionList = EnumUtil.getEnums(delegator, userLogin.getString("countryGeoId"), "constitution");
context.put("constitutionList", DataHelper.getDropDownOptions(constitutionList, "enumId", "description"));

if (UtilValidate.isNotEmpty(dataImportLead)){

	cond = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("pkCombinedValueText")), EntityOperator.LIKE, "%"+dataImportLead.get("leadId").toUpperCase()+"%"),
		EntityCondition.makeCondition("validationAuditType", EntityOperator.EQUALS, "VAT_LEAD_IMPORT")
	], EntityOperator.AND);
	importAuditCount = delegator.findCountByCondition("ValidationAuditLog", cond, null, null);
	context.put("importAuditCount", importAuditCount);
	importAuditLogTitle = "View Import Audit Messages";
	if (importAuditCount == 0) {
		importAuditLogTitle = "No Import Audit Log";
	}
	context.put("importAuditLogTitle", importAuditLogTitle);
	
	cond = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("pkCombinedValueText")), EntityOperator.LIKE, "%"+dataImportLead.get("leadId").toUpperCase()+"%"),
		EntityCondition.makeCondition("validationAuditType", EntityOperator.EQUALS, "VAT_LEAD_DEDUP")
	], EntityOperator.AND);
	dedupAuditCount = delegator.findCountByCondition("ValidationAuditLog", cond, null, null);
	context.put("dedupAuditCount", dedupAuditCount);
	dedupAuditLogTitle = "View Dedup Audit Messages";
	if (dedupAuditCount == 0) {
		dedupAuditLogTitle = "No Dedup Audit Log";
	}
	context.put("dedupAuditLogTitle", dedupAuditLogTitle);
	
}

context.put("virtualTeamList", DataHelper.getDropDownOptions(VirtualTeamUtil.getVirtualTeamList(delegator, userLogin.getString("countryGeoId"), userLogin.getString("partyId")), "partyId", "groupName"));

context.put("loggedUserVirtualTeamId", VirtualTeamUtil.getVirtualTeamId(delegator, userLogin.getString("partyId")));


