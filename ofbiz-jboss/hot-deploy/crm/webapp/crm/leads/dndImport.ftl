<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<link rel="stylesheet" href="/metronic/css/bootstrap-fileinput.css" type="text/css"/>
<script src="/metronic/js/bootstrap-fileinput.js" type="text/javascript"></script>

<div class="page-header">
	<h1 class="float-left">${uiLabelMap.dndImport!}</h1>

</div>


<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
			<form id="uploadFileDnd"  role="form" class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator" onsubmit="return fileUpload();">
				
			<input type="hidden" name="modelName" id="modelName" value="${defaultModelName!}"/>
			<input type="hidden" name="extension" id="extension" value="csv"/>
			
			<input type="hidden" name="customSuccessMessage" value="File Uploaded, Kindly review the error logs section for the Errors records and Find lead screen for the successfully imported records"/>	
				
			<div class="form-body">

			<div class="form-group row">
			   <label class="col-sm-4 col-form-label text-danger">${uiLabelMap.importFile}*</label>
			   <div class="col-sm-3">
			      
			   		<@simpleDropdownInput 
					id="importFileOptionId"
					options=importFileOptions
					required=false
					allowEmpty=false
					dataLiveSearch=true
					/>	   
			      
			   </div>
			   <div class="col-sm-4">
			      
			   		<a id="dnd-file-download-btn" href="/crm-resource/template/dnd_import.csv" class="btn btn-xs btn-primary m5 tooltips ml-3" title="" data-original-title="Download Import File Template" download>
			   			<i class="fa fa-download"></i>
			   			${uiLabelMap.download!}
			   		</a>   
			      
			   </div>
			</div> 
			 
			 <div class="form-group row has-error">
                  <label  class="col-sm-4 col-form-label">${uiLabelMap.fileToImport!}*</label>
                  <div class="col-sm-5">
                     <input name="csv_fileName" accept=".csv" id="csv_fileName_lst" data-error="Please select Text file to Upload" type="file" size="30" maxlength="" class="form-control" required>
                  </div>
                  
               </div>
			</div>
			<@fromCommonAction iconClass="fa fa-upload" showCancelBtn=false showClearBtn=false submitLabel=uiLabelMap.upload/>
			
		</form>	
							
		</div>
						
	</div>
	
</div>


<div class="clearfix"> </div>
<div class="page-header">
   <h2 class="float-left">Results</h2>
</div>



<div class="table-responsive">
   <table id="dndImportDetailsTable" class="table table-striped">
      <thead>
         <tr>
            <th>Process Id</th>
            <th >File Name</th>
            <th >${uiLabelMap.status!}</th>
            <th>Total Count</th>
            <th>${uiLabelMap.errorCount!}</th>
            <th>Error List</th>
         </tr>
      </thead>

   </table>
</div>


<div id="dndErrorLogsModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="reset" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="clearfix"> </div>
            <div class="page-header">
               <h2 class="float-left">DND Error Log List</h2>
               
            </div>
            <div class="table-responsive">
               <table id="dndErrorLogsModaldatatable" class="table table-striped">
                  <thead>
                     <tr>
                        <th>${uiLabelMap.processId!}</th>
                        <th>${uiLabelMap.logId!}</th>
                        <th>${uiLabelMap.dndNumber!}</th>
                        <th>${uiLabelMap.dndIndicator}</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
         <div class="modal-footer">
                <input id="importId" type="hidden" name="importId" value="">
            <a href="#"  class="btn btn-sm btn-primary navbar-dark dndErrorLogExport"   id="dndErrorLogExport">${uiLabelMap.download!}</a>
            <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">${uiLabelMap.close!}</button>
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">

$( "#dndErrorLogExport" ).click(function() {
 // var importId = $('#importId').val();
  $("#dndErrorLogExport").attr("href", "<@ofbizUrl>dndErrorLogExport</@ofbizUrl>?"+"importId="+$("#importId").val());
});

function fileUpload() {
   var fileSelected = $("#csv_fileName_lst").val();
   if(fileSelected ==""){
       $("#csv_fileName_lst").prop('required',true);
       return false;
    }else{
      $("#uploadFileDnd").attr("action", "<@ofbizUrl>uploadDndFile</@ofbizUrl>");
      return true;
    }
}

jQuery(document).ready(function() {
	$("#importFileOptionId").change(function() {
  		if ($(this).val() == "CSV") {
          
  			$("#dnd-file-download-btn").attr("href", "/crm-resource/template/dnd_import.csv");
  		}
	});

});

function viewImportError(importId){
	$('#importId').val(importId);
 dndErrorLogsModaldata(importId);
}

   $('#findDndImport').on('click',function(){
   	dndErrorLogsModaldata();
   });
      function dndErrorLogsModaldata(importId) {
        $('#dndErrorLogsModaldatatable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": {
                "url": "dndErrorLogsDetails",
                "type": "POST",
                data: {
                "importId" : importId,
                "search" : function(){
                var value = $('#dndErrorLogsModaldatatable_filter input').val();
                return value;
                }
               },
                
            },
            "Paginate": true,
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                   "data": "importId"
                },
                {
                     "data": "codeDescription",
                   /* "render": function(data, type, row, meta) {
                        var codeDescription = row.codeDescription;
                        var codeId = row.errorId;
                        if (codeDescription!= null && codeDescription != "" && codeDescription != undefined) {
                        data = codeId+' - '+codeDescription;
                        }
                        return data;
                    }*/
                },
                {
                   "data": "dndNumber"
                },
                {
                   "data": "dndIndicator"
                }
            ]
        });
    }    

$(document).ready(function() {
        dndImportDetails('');
    });

    function dndImportDetails() {
       
        $('#dndImportDetailsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": {
                "url": "dndImportDetails",
                "type": "POST",
                data: {
                 "searchParam" : function(){
                   var searchVal = $('#dndImportDetailsTable_filter input').val();
		           	return searchVal;
		           }
                },
            },
            "Paginate": true,
            "order": [[ 0, "DESC" ]],
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },
            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [{
                    "data": "importId"
                },
                {
                   "data": "actualFileName"
                },
                {
                   "data": "processed",
                   "render": function(data, type, row, meta) {
                        var process = row.processed;
                        if (process != null && process != "" && process != undefined) {
                           if (process == "N"){ 
                            data = "InProcess";
                            }else if (process == "Y"){
                            data = "Processed";
                            }
                        } else {
                            data = '';
                       }
                        return data;
                    }
                },
                {
                   "data": "totalCount",
                    "render": function(data, type, row, meta) {
                        var totalCount = row.totalCount;
                        if (totalCount!= null && totalCount != "" && totalCount != undefined) {
                        data = totalCount;
                        }
                        return data;
                    }
                },
                 {
                   "data": "totalError",
                    "render": function(data, type, row, meta) {
                        var totalErro = row.totalError;
                        var proc = row.processed;
                        if (proc == "N") {
                        data = "";
                        }
                        return data;
                    }
                },
               {    "orderable": false,
                    "data": "csrFirstName",
                     
                    "render": function(data, type, row, meta) {
                        data = '<a onclick=viewImportError("' + row.importId + '")>' + '<span class="btn btn-xs btn-primary ml-4" data-toggle="modal" href="#dndErrorLogsModal" alt="View" title="View">View</span></a>';
                        return data;
                    }
                }
            ]
        });
    }
    
</script>


