<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
  <div class="page-header border-b">
     <h1 class="float-left">${uiLabelMap.UpdateAccount!}</h1>
  </div>
  <form method="post" action="<@ofbizUrl>updateAccount</@ofbizUrl>" id="updateAccountForm" class="form-horizontal" name="updateAccountForm" novalidate="novalidate" data-toggle="validator">
     <input type="hidden" name="partyId" id="partyId" value="${parameters.partyId?if_exists}">
     <input type="hidden" name="accountName" id="accountName" value="${partySummary?if_exists.groupName?if_exists}">
     <div class="row padding-r">
        <div class="col-md-6 col-sm-6">
           <div class="form-group row has-error">
              <label class="col-sm-4 col-form-label">${uiLabelMap.companyName!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" value="${partySummary?if_exists.groupName?if_exists}" placeholder="${uiLabelMap.companyName!}"  required readonly>
                 <div class="help-block with-errors"></div>
              </div>
           </div>
           <div class="form-group row">
              <label class="col-sm-4 col-form-label">${uiLabelMap.localName!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm"  id="groupNameLocal" name="groupNameLocal" value="${partySummary?if_exists.groupNameLocal?if_exists}" placeholder="Local Name" > 
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.annualRevenue!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" id="annualRevenue" name="annualRevenue" value="${partySummary?if_exists.annualRevenue?if_exists}" placeholder="Annual Revenue">
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.DBSIC!}</label>
              <div class="col-sm-7">
                 <#assign enumerations = Static["org.fio.crm.util.UtilCommon"].getEnumerations("PARTY_INDUSTRY", delegator)/>
                 <select class="ui dropdown search form-control input-sm" name="industryEnumId">
                    <option value="" disabled selected>Select DBSIC</option>
                    <#list enumerations as enum>
                    <#if enum.disabled?has_content && enum.disabled="Y">
                    <#else>
                    <option value="${enum.enumId}"<#if partySummary?if_exists.industryEnumId?if_exists?if_exists==enum.enumId?if_exists>Selected</#if>>${enum.description}</option>   
                    </#if>
                    </#list>
                 </select>
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.constitution!}</label>
              <div class="col-sm-7">
                 <#assign ownershipList = delegator.findByAnd("Enumeration",Static["org.ofbiz.base.util.UtilMisc"].toMap("enumTypeId","PARTY_OWNERSHIP"),[],false)?if_exists/>
                 <select class="ui dropdown search form-control input-sm" name="ownershipEnumId" >
                    <option value="" disabled selected>Select Constitution</option>
                    <#list ownershipList as owner>
                    <option value="${owner.enumId?if_exists}"  <#if partySummary?if_exists.ownershipEnumId?if_exists==owner.enumId?if_exists>selected</#if> >${owner.description?if_exists}</option>
                    </#list>
                 </select>
              </div>
           </div>
           <#--<div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.sicCode!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" id="sicCode" name="sicCode" value="${partySummary?if_exists.sicCode?if_exists}" placeholder="SIC Code">
              </div>
           </div>-->
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.description!}</label>
              <div class="col-sm-7">
                 <textarea name="description" rows="3" placeholder="Description" class="form-control" >${partySummary?if_exists.description?if_exists}</textarea>
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.taxExemptNumber!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" name = "partyTaxId" value="${partyTaxId?if_exists}" placeholder="Tax Exempt Number">
              </div>
           </div>
        </div>
        <div class="col-md-6 col-sm-6">
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.parentAccount!}</label>
              <div class="col-sm-7">
                 <div class="input-group">
                    <input type="text" name="parentPartyId" id="parentPartyId" class="form-control input-sm" value="${parentParty?if_exists.partyId?if_exists}" placeholder="Parent Account">
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-list-alt" data-toggle="modal" data-target="#parentAccountModal" id="findAccount">
                    </span>
                    </span>
                 </div>
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.siteName!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" id="officeSiteName" name="officeSiteName" value="${partySummary?if_exists.officeSiteName?if_exists}" placeholder="Site Name">
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.preferredCurrency!}</label>
              <div class="col-sm-7">
                 <#assign currencies = delegator.findByAnd("Uom",{"uomTypeId","CURRENCY_MEASURE"},Static["org.ofbiz.base.util.UtilMisc"].toList("abbreviation"), false)?if_exists/>
                 <#assign defaultCourrencyUomId = (Static["org.ofbiz.base.util.UtilProperties"].getPropertyValue("crm", "defaultCurrencyUomId"))?default("USD")/>
                 <select class="ui dropdown search form-control input-sm"  id="currencyUomId" name="currencyUomId">
                    <option value="" disabled selected>Select Currency</option>
                    <#list currencies as currency>
                    	<option value="${currency.uomId}" <#if partySummary?if_exists.currencyUomId?if_exists==currency.uomId?if_exists>selected</#if>>${currency.description}</option>
                    </#list>
                 </select>
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.numberOfEmployees!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" id="numberEmployees" name="numberEmployees" value="${partySummary?if_exists.numberEmployees?if_exists}" placeholder="Number Of Employees">
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.tickerSymbol!}</label>
              <div class="col-sm-7">
                 <input type="text" class="form-control input-sm" id="tickerSymbol" name="tickerSymbol" value="${partySummary?if_exists.tickerSymbol?if_exists}" placeholder="Ticker Symbol">
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.note!}</label>
              <div class="col-sm-7">
                 <textarea id="importantNote" name="importantNote" rows="3" placeholder="Note" class="form-control" >${partySummary?if_exists.importantNote?if_exists}</textarea>
              </div>
           </div>
           <div class="form-group row">
              <label  class="col-sm-4 col-form-label">${uiLabelMap.isTaxExempt!}</label>
              <div class="col-sm-7">
              <#if PartyTaxAuthInfoList?has_content>
			    <#list PartyTaxAuthInfoList as PartyTaxAuthInfo>
			    <#if PartyTaxAuthInfo?has_content>
			    	<#assign partyTaxId = PartyTaxAuthInfo.partyTaxId?if_exists />
			    	<#assign isExempt = PartyTaxAuthInfo.isExempt?if_exists />
			    	<#break/>
			    </#if>
			    </#list>
			    </#if>
                 <select name="isExempt" class="ui dropdown search form-control input-sm">
                 	<option value="">Please Select</option>
                    <option value="N">No</option>
                    <option value="Y" <#if isExempt?has_content && isExempt == "Y"> selected = selected</#if>>Yes</option>
                 </select>
              </div>
           </div>
           
        </div>
     </div>
     <div class="col-md-12 col-sm-12">
        <div class="form-group row">
           <div class="offset-sm-2 col-sm-9 mb-3">
              <button type="submit" class="btn btn-sm btn-primary mt">${uiLabelMap.UpdateAccount!}</button>
           </div>
        </div>
     </div>
  </form>
  <div class="clearfix"> </div>
  <div class="panel-group" id="accordionMenu" role="tablist" aria-multiselectable="true">
     <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
           <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#sources" aria-expanded="false" aria-controls="headingTwo">
              Sources
              </a>
           </h4>
        </div>
        <div id="sources" class="panel-collapse collapse show" data-parent="#accordionMenu" aria-labelledby="">
          <div class="panel-body">               
           <#if dataSources?exists && dataSources?has_content>
              <div class="table-responsive">
                 <table class="table table-striped">
                    <thead>
                       <tr>
                          <th>Source</th>
                          <th>From Date</th>
                          <th>Remove</th>
                       </tr>
                    </thead>
                    <tbody>
                    <#list dataSources as dataSource>
                       <tr>
                          <td>
                          		<#assign dataSourceGv = dataSource.getRelatedOne("DataSource", false)! />
                          		<#if dataSourceGv?has_content>
                          			${dataSourceGv.description?if_exists}
                          		</#if>
                          </td>
                          <td>${dataSource.fromDate}</td>
                          <td>
                          	<form method="post" action="removeAccountDataSource" name="removeDataSource" id="removeDataSource" >
                          		<input type="hidden" name="partyId" id="partyId" value="${parameters.partyId?if_exists}"/>
                          		<input type="hidden" name="dataSourceId" id="dataSourceId" value="${dataSource.dataSourceId?if_exists}"/>
                          		<input type="hidden" name="fromDate" id="fromDate" value="${dataSource.fromDate?if_exists}"/>
                          	</form>
                          <a href="javascript:document.removeDataSource.submit();" ><span class="glyphicon glyphicon-remove btn btn-xs btn-danger"></span></a></td>
                       </tr>
                    </#list>
                    </tbody>
                 </table>
              </div>
           </#if>
           <div class="row padding-r">
           <div class="col-md-6 col-sm-6">
             <div class="form-group row has-error">
                <label class="col-sm-4 col-form-label">New Data Source</label>
                <div class="col-sm-7">
                <form name="addAccountDataSource" method="post" action="addAccountDataSource">
                	<input type="hidden" name="partyId" id="partyId" value="${parameters.partyId!}"/>
                   <select class="ui dropdown search form-control input-sm"  name="dataSourceId" id="dataSourceId" >
                      <option value="" disabled selected>Select Data Source</option>
                      <#if dataSourceList?has_content>
                      <#list dataSourceList as dataSource>
                        <option value="${dataSource.dataSourceId}">${dataSource.description}</option>
                      </#list>
                      </#if>
                   </select>
                   <div class="help-block with-errors"></div>
                </form>
                </div>
             </div>
             <div class="form-group row">
                <div class="offset-sm-2 col-sm-9">
                   <a href="javascript:document.addAccountDataSource.submit();"><span class="btn btn-sm btn-primary mb-2">${uiLabelMap.CommonAdd}</span></a>
                </div>
             </div>
           </div>
           </div>
		  </div> 
        </div>
     </div> <!-- data source end-->
     
     <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
           <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#mktCampaigns" aria-expanded="false" aria-controls="headingTwo">
              Marketing Campaigns
              </a>
           </h4>
        </div>
        <div id="mktCampaigns" class="panel-collapse collapse" data-parent="#accordionMenu" aria-labelledby="">
          <div class="panel-body">
           <#if marketingCampaigns?exists && marketingCampaigns?has_content>
              <div class="table-responsive">
                 <table class="table table-striped">
                    <thead>
                       <tr>
                          <th>Marketing Campaign</th>
                          <th>Remove</th>
                       </tr>
                    </thead>
                    <tbody>
                       <#list marketingCampaigns as campaign>
                       <tr>
                          <td><a href="viewMarketingCampaign?marketingCampaignId="+ ${campaign.marketingCampaignId?if_exists} >${campaign.campaignName?if_exists}</a></td>
                          <td>
                          <form method="post" action="removeAccountMarketingCampaign" name="removeMktCampaign" id="removeMktCampaign" >
                          		<input type="hidden" name="partyId" id="partyId" value="${parameters.partyId?if_exists}"/>
                          		<input type="hidden" name="marketingCampaignId" id="marketingCampaignId" value="${campaign.marketingCampaignId?if_exists}"/>
                          	</form>
                          <a href="javascript:document.removeMktCampaign.submit();" ><span class="glyphicon glyphicon-remove btn btn-xs btn-danger"></span></a>
                          </td>
                          
                          
                       </tr>
                       </#list>
                    </tbody>
                 </table>
              </div>
           </#if>
         <div class="row padding-r">
           <div class="col-md-6 col-sm-6">
             <div class="form-group row has-error">
                <label class="col-sm-4 col-form-label">New Marketing Campaign</label>
                <div class="col-sm-7">
                <form name="addAccountMarketingCampaign" method="post" action="addAccountMarketingCampaign" class="row" novalidate="true" data-toggle="validator">
                	<input type="hidden" name="partyId" id="partyId" value="${parameters.partyId!}"/>
                 <#assign marketingCampainList = delegator.findByAnd("MarketingCampaign", Static["org.ofbiz.base.util.UtilMisc"].toMap("statusId", "MKTG_CAMP_INPROGRESS"), Static["org.ofbiz.base.util.UtilMisc"].toList("campaignName ASC"),false) />
                   <select class="ui dropdown search form-control input-sm"  name="marketingCampaignId" id="marketingCampaignId" required>
                      <option value="" disabled selected>Select Campaign</option>
                      <#if marketingCampainList?has_content>
                      <#list marketingCampainList as marketingCampain>
                        <option value="${marketingCampain.marketingCampaignId}">${marketingCampain.campaignName}</option>
                      </#list>
                      </#if>
                   </select>
                   <div class="help-block with-errors"></div>
                </form>
                </div>
             </div>
             <div class="form-group row">
                <div class="offset-sm-2 col-sm-9">
                   <a href="javascript:document.addAccountMarketingCampaign.submit();"><span class="btn btn-sm btn-primary mt">${uiLabelMap.CommonAdd}</span></a>
                </div>
             </div>
           </div>
          </div>
        </div>
        </div>
     </div>
     <div class="clearfix"> </div>
  </div>
<#include "component://crm/webapp/crm/account/accountModels.ftl">
