<div class="page-header">
   <h2 class="float-left">Subscription Details</h2>
</div>
<div class="table-responsive">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Product Info</th>
            <th>Invoice ID</th>
            <th>Order ID</th>
            <th>Renewal Cost</th>
            <th>Renewed Date</th>
            <th>Next Renewal Date</th>
            <th>Subscription Info</th>
            <th>Contract Expiry Date</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td colspan="8">No records Available</td>
         </tr>
      </tbody>
   </table>
</div>