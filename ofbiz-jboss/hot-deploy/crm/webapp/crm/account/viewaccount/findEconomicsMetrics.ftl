<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h1 class="float-left">${uiLabelMap.EconomicsMetrics}</h1>
	<div class="float-right">
		
	</div>
</div>

<#-- 
<div class="panel-group" id="subaccordionMenu1" role="tablist" aria-multiselectable="true">
   <div class="row">
   
   	<#if groupingCodeList?has_content>
		
	<#list groupingCodeList.entrySet() as groupingCode>	
	   
      <div class="col-md-6 col-sm-6 mt-2">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="subaccordion">
               <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#subaccordionMenu1" href="#accordionGroup${groupingCode_index+1}" aria-expanded="false" aria-controls="collapseTwo">
                  ${groupingCode.value!}
                  </a>
               </h4>
            </div>
            <div id="accordionGroup${groupingCode_index+1}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head1">
               <div class="panel-body">
                  
               		<#assign metricIndicatorList = delegator.findByAnd("PartyMetricIndicator", {"partyId", parameters.partyId,"groupingCode" : groupingCode.key}, Static["org.ofbiz.base.util.UtilMisc"].toList("sequenceNumber ASC"), false)>  	 
                  
                  	<div class="table-responsive">
						<table class="table table-hover" id="list-metric-indicator-${groupingCode.key}">
						<thead>
						<tr>
							<th>${uiLabelMap.propertyName!}</th>
							<th>${uiLabelMap.propertyValue!}</th>
						</tr>
						</thead>
						<tbody>
						
						<#if metricIndicatorList?has_content>
							
						<#list metricIndicatorList as ec>
						<tr>
							<td>${ec.propertyName!}</td>
							<td>${ec.propertyValue!}</td>
						</tr>
						
						</#list>
							
						</#if>
						
						</tbody>
						</table>
					</div>
					
					<script type="text/javascript">

					jQuery(document).ready(function() {	
					
						$('#list-metric-indicator-${groupingCode.key}').DataTable({
					  		"order": [],
					  		
						});
					
					});
							
					</script>
                  
               </div>
            </div>
         </div>
      </div>
      
      </#list>
		
	</#if>
      
	</div>
</div>
 -->


<div class="card-header mt-2 mb-3">
   <form method="post" class="form-horizontal" data-toggle="validator">
   		
   		<input type="hidden" name="activeTab" value="economicsMetrics" />	
   		
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="groupingCode"
				options=groupingCodeList
				required=false
				value=metricIndicator.groupingCode
				allowEmpty=true
				tooltip = uiLabelMap.groupingCode
				emptyText = uiLabelMap.groupingCode
				dataLiveSearch=true
				/>
         </div>
         
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="economicCodeId"
				required=false
				value=metricIndicator.economicCodeId
				allowEmpty=true
				tooltip = uiLabelMap.economicCode
				emptyText = uiLabelMap.economicCode
				dataLiveSearch=true
				/>
         </div>
         
         <@fromSimpleAction id="" showCancelBtn=false isSubmitAction=true submitLabel="Find"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<div class="table-responsive">
	<table class="table table-hover" id="list-metric-indicator">
	<thead>
	<tr>
		<th>${uiLabelMap.groupingCode!}</th>
		<th>${uiLabelMap.economicCode!}</th>
		<th>${uiLabelMap.economicMetric!}</th>
		<th>${uiLabelMap.economicValue!}</th>
		<th>${uiLabelMap.CommonLastUpdated!}</th>
	</tr>
	</thead>
	<tbody>
	
	<#if metricIndicatorList?has_content>
		
	<#list metricIndicatorList as ec>
	
	<#assign economicCode = ec.getRelatedOne("CustomFieldGroup", false)! />
	<#assign economicMetric = ec.getRelatedOne("CustomField", false)! />
	
	<tr>
		<td>${ec.groupingCode!}</td>
		<td>${economicCode.groupName!}</td>
		<td>${economicMetric.customFieldName!}</td>
		<td>${ec.propertyValue!}</td>
		<td>${ec.lastUpdatedStamp!?string["dd-MM-yyyy, HH:mm"]}</td>
	</tr>
	
	</#list>
		
	</#if>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {	

	$('#list-metric-indicator').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	
	loadEconomicCodeList();
	$("#groupingCode").change(function() {
		loadEconomicCodeList()
	});

});

function loadEconomicCodeList() {
	var nonSelectContent = "<span class='nonselect'>Select ${uiLabelMap.economicCode!}</span>";
	var groupNameOptions = '<option value="" data-content="'+nonSelectContent+'" selected="">Select ${uiLabelMap.economicCode!}</option>';		
		
	if ( $("#groupingCode").val() ) {
		
		$.ajax({
			      
			type: "POST",
	     	url: "getCustomFieldGroups",
	        data:  {"groupingCode": $("#groupingCode").val()},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.groups.length; i++) {
	            		var group = data.groups[i];
	            		groupNameOptions += '<option value="'+group.groupId+'">'+group.groupName+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
		$("#economicCodeId").html( groupNameOptions );
		
		<#if metricIndicator.economicCodeId?has_content>
		$("#economicCodeId").val( "${metricIndicator.economicCodeId}" );
		</#if>
		
		$('#economicCodeId').dropdown('refresh');
	}
		
}
		
</script>

