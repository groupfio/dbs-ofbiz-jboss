<div class="page-header">
   <h2 class="float-left">Search</h2>
</div>
<div class="card-header">
     <form method="post" action="findAccounts" id="findAccountForm" class="form-horizontal" name="findAccountForm" novalidate="novalidate" data-toggle="validator">
        <div class="row">
           <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr input-group date" id="datetimepicker7">
                     <input type="text" class="form-control input-sm" id="" name="" value="" placeholder="From Date">
                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </div>
           </div>
           <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr input-group date" id="datetimepicker6">
                     <input type="text" class="form-control input-sm" id="" name="" value="" placeholder="Thru Date">
                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </div>
           </div>
           <div class="col-md-2 col-sm-2">
              <div class="form-group row mr">
                 <input type="text" class="form-control input-sm" id="" name="" value="" placeholder="New Team Member">
              </div>
           </div>
               
           <div class="col-md-1 col-sm-1 pl-0">
              <button type="reset" class="btn btn-sm btn-primary">Search</button>
           </div>
        </div>
     </form>
     <div class="clearfix"> </div>
  </div>

<div class="page-header">
   <h2 class="float-left">Calls</h2>
</div>
<div class="table-responsive">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Call Id </th>
            <th>Call Name </th>
            <th>Started Date </th>
            <th>Closed Date </th>
            <th>Created Date</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td> 29561</td>
            <td> Test Log call </td>
            <td> 2018-01-12 04:22:00.000 </td>
            <td> 2018-01-12 05:22:00.000 </td>
            <td> 2018-01-12 04:25:01.000
            </td>
         </tr>
      </tbody>
   </table>
</div>
<div class="clearfix"></div>
<div class="page-header">
   <h2 class="float-left">Notes</h2>
</div>
<div class="table-responsive">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Note Info </th>
            <th>Created By </th>
            <th>Note Date Time </th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td colspan="3"> No Records Found.</td>
         </tr>
      </tbody>
   </table>
</div>
<div class="clearfix"></div>