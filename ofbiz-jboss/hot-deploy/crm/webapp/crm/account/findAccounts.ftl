<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>
<div class="page-header border-b">
     <h1>Find Accounts</h1>
  </div>
  <div class="card-header">
     <#-- <form method="post" action="findAccounts" id="findAccountForm" class="form-horizontal" name="findAccountForm" novalidate="novalidate" data-toggle="validator"> -->
        <div class="row">
           <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr">
                     <input type="text" class="form-control input-sm" id="accountSearchPartyId" name="accountSearchPartyId" value="${accountSearchPartyId?if_exists}" placeholder="${uiLabelMap.companyId!}">
                  </div>
               </div>
               <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr">
                     <input type="text" class="form-control input-sm" id="searchGroupName" name="searchGroupName" value="${searchGroupName?if_exists}" placeholder="${uiLabelMap.companyName!}">
                  </div>
               </div>
               <#--<div class="col-md-2 col-sm-2">
                  <div class="form-group row mr">
                     <input type="text" class="form-control input-sm" id="searchCompanyName" name="searchCompanyName" value="${searchCompanyName?if_exists}" placeholder="Company Name">
                  </div>
               </div>-->
               <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr">
                     <input type="text" class="form-control input-sm" id="searchEmailId" name="searchEmailId" value="${searchEmailId?if_exists}" placeholder="Email Address">
                  </div>
               </div>
               <div class="col-md-2 col-sm-2">
                  <div class="form-group row mr">
                     <input type="text" class="form-control input-sm" id="searchPhoneNum" name="searchPhoneNum" value="${searchPhoneNum?if_exists}" placeholder="Phone Number">
                  </div>
               </div>
           <#-- <div class="col-md-1 col-sm-1 pl-0">
              <button type="submit" class="btn btn-sm btn-primary ">Find</button>
           </div> -->
           	  <@fromSimpleAction id="findAccounts" showCancelBtn=false isSubmitAction=false submitLabel="Find"/>
        </div>
     <#-- </form> -->
     <div class="clearfix"> </div>
  </div>
  <div class="clearfix"> </div>
  <div class="page-header">
     <h2 class="float-left">Accounts List </h2>
  </div>
  <div class="table-responsive">
     <table id="findAccountsTable" class="table table-striped">
        <thead>
           <tr>
              <th>${uiLabelMap.companyName!}</th>
               <#-- <#if groupList?has_content>
              <#list groupList as groupList>
              <th><div class="text-right mr-4">${groupList.customFieldName}</div></th>
              </#list>
              </#if>-->
              <th>LCIN</th>
              <th>CIN</th>
              <th>Status</th>
              <th>Phone Number</th>
              <th>E-Mail Address</th>
             
           </tr>
        </thead>
        <#-- <tbody>
           <#if findList?has_content>
            <#list findList as list>
              <tr>
                <td><a href="viewAccount?partyId=${list?if_exists.partyId?if_exists}">${list?if_exists.groupName?if_exists}(${list?if_exists.partyId?if_exists})</a></td>
			     <#if groupList?has_content>
              <#list groupList as groupList>
              <#assign fieldValue = delegator.findOne("CustomFieldValue",{"customFieldId":groupList.customFieldId?if_exists,"partyId":list?if_exists.partyId?if_exists},true)?if_exists />
                <#if fieldValue?has_content>
                 <td><div class="text-right mr-4">${fieldValue?if_exists.fieldValue?if_exists}</div></td>
                 <#else>
                 <td></td>
                 </#if> 
                 </#list>
              </#if>
                <td>${list?if_exists.statusItemDesc?if_exists}</td>
                <td>${list?if_exists.contactNumber?if_exists}</td>
                <td>${list?if_exists.emailAddress?if_exists}</td>
             
              </tr>
              </#list>
             </#if>
        </tbody>-->
     </table>
</div>

<script>
    $(document).ready(function() {
        loadFindAccounts();
    });
    
    $('#findAccounts').on('click',function(){
    	loadFindAccounts();
    });
    
    function loadFindAccounts() {
    	var accountSearchPartyId = $("#accountSearchPartyId").val();
        var searchGroupName = $("#searchGroupName").val();
        var searchEmailId = $("#searchEmailId").val();
        var searchPhoneNum = $("#searchPhoneNum").val();
        var findAccountsUrl = "getAccountDetails?accountSearchPartyId=" + accountSearchPartyId + "&searchGroupName=" + searchGroupName + "&searchEmailId=" + searchEmailId + "&searchPhoneNum=" + searchPhoneNum;
      
        oTable = $('#findAccountsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "ajax": {
                "url": findAccountsUrl,
                "type": "POST"
            },
			"deferRender": true,
            "Paginate": true,
            "order": [[ 0, "DESC" ]],
            "language": {
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "oPaginate": {
                    "sNext": "Next",
                    "sPrevious": "Previous"
                }
            },

            "pageLength": 10,
            "bAutoWidth": false,
            "stateSave": false,
            "columns": [
                {
                    "data": "partyId",
                    "render": function(data, type, row, meta) {
                        var partyId = row.partyId;
                        if (partyId != null && partyId != "" && partyId != undefined) {
                            data = '<a href="/crm/control/viewAccount?partyId=' + row.partyId + '&externalLoginKey=${requestAttributes.externalLoginKey!}">' + row.groupName + '(' + data + ')</a>';
                        }
                        return data;
                    }
                },
                {
                    "data": "partyId"
                },
                {	
                    "data": "cin",
                    "orderable": false
                },
                {
                    "data": "statusDescription"
                },
                {
                    "data": "contactNumber"
                },
                {
                    "data": "infoString"
                }
            ]
        });
    }
</script>
