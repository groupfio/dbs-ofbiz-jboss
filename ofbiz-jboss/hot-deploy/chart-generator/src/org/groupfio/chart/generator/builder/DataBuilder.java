/**
 * 
 */
package org.groupfio.chart.generator.builder;

import java.util.HashMap;
import java.util.Map;

import org.ofbiz.base.util.Debug;

/**
 * @author Sharif
 *
 */
public abstract class DataBuilder {

	private static String MODULE = DataBuilder.class.getName();
	
	private Map<String, Object> context;

	protected abstract Map<String, Object> doBuild(Map<String, Object> context) throws Exception;

	public Map<String, Object> build(Map<String, Object> context){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = doBuild(context);
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		return result;
	}
	
}
