/**
 * 
 */
package org.groupfio.chart.generator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.groupfio.chart.generator.constants.ChartGeneratorConstants.FlowchartDirection;
import org.groupfio.chart.generator.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Sharif
 *
 */
public class FlowchartData {

	private String flowchartDirection = FlowchartDirection.LEFT_RIGHT;
	
	private String title;
	private String titleLink;
	private String titleClass;
	
	private List<DataNode> nodes = new ArrayList<DataNode>();
	
	public FlowchartData() {}
	
	public FlowchartData(Map<String, Object> data) {
		this.title = ParamUtil.getString(data, "title");
		this.titleLink = ParamUtil.getString(data, "titleLink");
		this.titleClass = ParamUtil.getString(data, "titleClass");
		
		if (UtilValidate.isNotEmpty(data.get("nodes"))) {
			List<Map<String, Object>> nodeList = (List<Map<String, Object>>) data.get("nodes");
			for (Map<String, Object> node : nodeList) {
				nodes.add( new DataNode(node) );
			}
		}
		
	}

	public String getFlowchartDirection() {
		return flowchartDirection;
	}

	public void setFlowchartDirection(String flowchartDirection) {
		this.flowchartDirection = flowchartDirection;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleLink() {
		return titleLink;
	}

	public void setTitleLink(String titleLink) {
		this.titleLink = titleLink;
	}

	public String getTitleClass() {
		return titleClass;
	}

	public void setTitleClass(String titleClass) {
		this.titleClass = titleClass;
	}

	public List<DataNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<DataNode> nodes) {
		this.nodes = nodes;
	}
	
}
