/**
 * 
 */
package org.groupfio.chart.generator.util;

import org.groupfio.chart.generator.constants.ChartGeneratorConstants.NodeConnectorType;
import org.groupfio.chart.generator.constants.ChartGeneratorConstants.NodeType;
import org.groupfio.chart.generator.model.DataNode;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Sharif
 *
 */
public class FlowchartDataUtil {

	public static String getNodeText (DataNode node) {
		if (UtilValidate.isNotEmpty(node.getNodeType()) && UtilValidate.isNotEmpty(node.getNodeText())) {
			
			String nodeText = node.getNodeText();
			String iconClass = UtilValidate.isNotEmpty(node.getIconClass()) ? node.getIconClass() + " " : "";
			
			switch (node.getNodeType()) {
			case NodeType.TEXT:
				nodeText = "[" + iconClass + nodeText + "]"; 
				break;
			case NodeType.ROUND_EDGE:
				nodeText = "(" + iconClass + nodeText + ")";
				break;
			case NodeType.CIRCLE:
				nodeText = "((" + iconClass + nodeText + "))";
				break;
			case NodeType.ASYMETRIC_SHAPE:
				nodeText = ">" + iconClass + nodeText + "]";
				break;
			case NodeType.RHOMBUS:
				nodeText = "{" + iconClass + nodeText + "}";
				break;
			default:
				break;
			}
			
			return nodeText;
		}
		return "";
	}
	
	public static String getNodeConnector (String nodeConnectorType) {
		if (UtilValidate.isNotEmpty(nodeConnectorType)) {
			switch (nodeConnectorType) {
			case NodeConnectorType.ARROW_HEAD:
				nodeConnectorType = "-->"; 
				break;
			case NodeConnectorType.OPEN_LINK:
				nodeConnectorType = "--";
				break;
			case NodeConnectorType.DOTTED_LINK:
				nodeConnectorType = "-.->";
				break;
			case NodeConnectorType.THICK_LINK:
				nodeConnectorType = "==>";
				break;
			default:
				break;
			}
		}
		return nodeConnectorType;
	}
	
	public static String getLinkText (String linkText) {
		if (UtilValidate.isNotEmpty(linkText)) {
			return "|" + linkText + "|";
		}
		return "";
	}
	
}
