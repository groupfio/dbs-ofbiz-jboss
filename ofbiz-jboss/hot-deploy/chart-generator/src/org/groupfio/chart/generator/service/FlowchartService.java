/**
 * 
 */
package org.groupfio.chart.generator.service;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.chart.generator.builder.BuilderFactory;
import org.groupfio.chart.generator.builder.DataBuilder;
import org.groupfio.chart.generator.constants.ChartGeneratorConstants.DataBuilderType;
import org.groupfio.chart.generator.util.ParamUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

/**
 * @author Sharif
 *
 */
public class FlowchartService {

	private static final String MODULE = FlowchartService.class.getName();
	
    public static Map buildFlowchartData(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String data = (String) context.get("data");
    	String direction = (String) context.get("direction");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		if (UtilValidate.isNotEmpty(data)) {
    			
    			DataBuilder dataBuilder = BuilderFactory.getBuilder(DataBuilderType.FLOW_CHART);
    			
    			Map<String, Object> dataContext = new HashMap<String, Object>();
    			
    			dataContext.put("data", data);
    			dataContext.put("direction", direction);
    			
    			Map<String, Object> dataResponse = dataBuilder.build(dataContext);
    			
    			result.put("generatedChartData", ParamUtil.getString(dataResponse, "generatedChartData"));
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully build flow chart data.."));
    	
    	return result;
    	
    }
    
}
