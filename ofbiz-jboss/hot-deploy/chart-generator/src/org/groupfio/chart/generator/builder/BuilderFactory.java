/**
 * 
 */
package org.groupfio.chart.generator.builder;

import org.groupfio.chart.generator.constants.ChartGeneratorConstants.DataBuilderType;

/**
 * @author Sharif
 *
 */
public class BuilderFactory {

	public static DataBuilder getBuilder(DataBuilderType type) {
		
		DataBuilder writer = null;
		
		switch (type) {
			case FLOW_CHART:
				writer = new FlowchartDataBuilder();
				break;
			default:
				break;
		}
		
		return writer;
	}
	
}
