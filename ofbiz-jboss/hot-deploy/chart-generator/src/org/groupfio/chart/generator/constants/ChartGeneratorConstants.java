/**
 * 
 */
package org.groupfio.chart.generator.constants;

/**
 * @author Sharif Ul Islam
 * @since June 14, 2018
 *
 */
public class ChartGeneratorConstants {
	
	// Resource bundles	
    public static final String configResource = "chart-generator";
    public static final String uiLabelMap = "ChartGeneratorUiLabels";
    
    public static final int DEFAULT_BUFFER_SIZE = 102400;
    public static final int LOCKBOX_ITEM_SEQUENCE_ID_DIGITS = 5;
    
    public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	
	public static final class AppStatus {
        private AppStatus() { }
        public static final String ACTIVATED = "ACTIVATED";
        public static final String DEACTIVATED = "DEACTIVATED";
    }
	
	public enum DataBuilderType {
		FLOW_CHART("FLOW_CHART"), 
		;

		public String value;

		private DataBuilderType(String value) {
			this.value = value;
		}
		
	}
	
	public static final class SourceInvoked {
        private SourceInvoked() { }
        public static final String API = "API";
        public static final String PORTAL = "PORTAL";
        public static final String UNKNOWN = "UNKNOWN";
    }
	
	public static final class NodeType {
        private NodeType() { }
        public static final String TEXT = "TEXT";
        public static final String ROUND_EDGE = "ROUND_EDGE";
        public static final String CIRCLE = "CIRCLE";
        public static final String ASYMETRIC_SHAPE = "ASYMETRIC_SHAPE";
        public static final String RHOMBUS = "RHOMBUS";
    }
	
	public static final class NodeConnectorType {
        private NodeConnectorType() { }
        public static final String ARROW_HEAD = "ARROW_HEAD";
        public static final String OPEN_LINK = "OPEN_LINK";
        public static final String DOTTED_LINK = "CIRCLE";
        public static final String THICK_LINK = "ASYMETRIC_SHAPE";
    }
	
	public static final class FlowchartDirection {
        private FlowchartDirection() { }
        public static final String TOP_BOTTOM = "TB";
        public static final String BOTTOM_TOP = "BT";
        public static final String RIGHT_LEFT = "RL";
        public static final String LEFT_RIGHT = "LR";
    }
	
}
