/**
 * 
 */
package org.groupfio.chart.generator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.groupfio.chart.generator.constants.ChartGeneratorConstants.NodeConnectorType;
import org.groupfio.chart.generator.constants.ChartGeneratorConstants.NodeType;
import org.groupfio.chart.generator.util.ParamUtil;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Sharif
 *
 */
public class DataNode {

	private String nodeId;
	private String nodeText;
	private String linkText;
	private String url;
	private String tooltip;
	private String nodeClass;
	private String iconClass;
	
	private boolean isLink;
	private boolean isHighlighted;
	
	private String nodeType;
	private String connectorType;
	
	private List<DataNode> nodeLinks = new ArrayList<DataNode>();
	
	public DataNode() {}
	
	public DataNode(Map<String, Object> data) {
		this.nodeId = ParamUtil.getString(data, "nodeId");
		this.nodeText = ParamUtil.getString(data, "nodeText");
		this.linkText = ParamUtil.getString(data, "linkText");
		this.url = ParamUtil.getString(data, "url");
		this.tooltip = ParamUtil.getString(data, "tooltip");
		this.nodeClass = ParamUtil.getString(data, "nodeClass");
		this.iconClass = ParamUtil.getString(data, "iconClass");
		this.isLink = ParamUtil.getBoolean(data, "isLink");
		this.isHighlighted = ParamUtil.getBoolean(data, "isHighlighted");
		
		this.nodeType = ParamUtil.getString(data, "nodeType");
		this.connectorType = ParamUtil.getString(data, "connectorType");
		
		if (UtilValidate.isNotEmpty( data.get("nodeLinks") )) {
			List<Map<String, Object>> nodeList = (List<Map<String, Object>>) data.get("nodeLinks");
			for (Map<String, Object> node : nodeList) {
				nodeLinks.add( new DataNode(node) );
			}
		}
		
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeText() {
		return nodeText;
	}

	public void setNodeText(String nodeText) {
		this.nodeText = nodeText;
	}

	public String getLinkText() {
		return linkText;
	}

	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getNodeClass() {
		return nodeClass;
	}

	public void setNodeClass(String nodeClass) {
		this.nodeClass = nodeClass;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public boolean isLink() {
		return isLink;
	}

	public void setLink(boolean isLink) {
		this.isLink = isLink;
	}

	public boolean isHighlighted() {
		return isHighlighted;
	}

	public void setHighlighted(boolean isHighlighted) {
		this.isHighlighted = isHighlighted;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public List<DataNode> getNodeLinks() {
		return nodeLinks;
	}

	public void setNodeLinks(List<DataNode> nodeLinks) {
		this.nodeLinks = nodeLinks;
	}
	
}
