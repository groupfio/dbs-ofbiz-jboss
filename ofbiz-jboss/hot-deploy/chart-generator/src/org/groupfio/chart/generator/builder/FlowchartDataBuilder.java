/**
 * 
 */
package org.groupfio.chart.generator.builder;

import java.util.HashMap;
import java.util.Map;

import org.groupfio.chart.generator.ResponseCodes;
import org.groupfio.chart.generator.constants.ChartGeneratorConstants;
import org.groupfio.chart.generator.model.DataNode;
import org.groupfio.chart.generator.model.FlowchartData;
import org.groupfio.chart.generator.util.FlowchartDataUtil;
import org.groupfio.chart.generator.util.ParamUtil;
import org.ofbiz.base.conversion.JSONConverters.JSONToMap;
import org.ofbiz.base.lang.JSON;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;

/**
 * @author Sharif
 *
 */
public class FlowchartDataBuilder extends DataBuilder {
	
	private static String MODULE = DataBuilder.class.getName();
	
	private static FlowchartDataBuilder instance;
	
	public static synchronized FlowchartDataBuilder getInstance(){
        if(instance == null) {
            instance = new FlowchartDataBuilder();
        }
        return instance;
    }

	@Override
	protected Map<String, Object> doBuild(Map<String, Object> context) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			
			String jsonReq = (String) context.get("data");
			
			JSON jsonFeed = JSON.from(jsonReq);
			
			JSONToMap jsonMap = new JSONToMap();
			Map<String, Object> data = jsonMap.convert(jsonFeed);
			
			FlowchartData chartData = new FlowchartData(data);
			
			if (UtilValidate.isNotEmpty(context.get("direction"))) {
				chartData.setFlowchartDirection( ParamUtil.getString(context, "direction") );
			}
			
			String generatedChartData = "";
			String newLine = ";";
			
			generatedChartData += "graph " + chartData.getFlowchartDirection() + newLine;
			
			for (DataNode node : chartData.getNodes()) {
				
				String nodeId = node.getNodeId();
				
				for (DataNode nodeLink : node.getNodeLinks()) {
					
					generatedChartData += node.getNodeId();
					generatedChartData += FlowchartDataUtil.getNodeText(node);
					
					generatedChartData += FlowchartDataUtil.getNodeConnector(node.getConnectorType()) + FlowchartDataUtil.getLinkText( node.getLinkText() ) + nodeLink.getNodeId() + FlowchartDataUtil.getNodeText(nodeLink);
					generatedChartData += newLine;
					
					prepareNode(nodeLink, generatedChartData, newLine);
					
				}
				
				prepareNode(node, generatedChartData, newLine);
				
			}
			
			Debug.log("generatedChartData>>>>> "+generatedChartData, MODULE);
			
			response.put("generatedChartData", generatedChartData);
			
		} catch (Exception e) {
			e.printStackTrace();
			Debug.log(e.getMessage(), MODULE);
			
			response.put(ChartGeneratorConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			response.put(ChartGeneratorConstants.RESPONSE_MESSAGE, e.getMessage());
			
			return response;
			
		}
		
		response.put(ChartGeneratorConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		
		return response;

	}
	
	private void prepareNode(DataNode node, String generatedChartData, String newLine) throws Exception {
		if (node.isHighlighted()) {
			//generatedChartData += "class " + node.getNodeId() + " " + UtilProperties.getPropertyValue(ChartGeneratorConstants.configResource, "flowchart.highlighted.class");
			generatedChartData += "style " + node.getNodeId() + " " + UtilProperties.getPropertyValue(ChartGeneratorConstants.configResource, "flowchart.highlighted.style");
			generatedChartData += newLine;
		}
		
		if (node.isLink() && UtilValidate.isNotEmpty(node.getUrl())) {
			generatedChartData += "click " + node.getNodeId() + " \"" + node.getUrl() + "\"";
			generatedChartData += newLine;
		}
	}
	
}
