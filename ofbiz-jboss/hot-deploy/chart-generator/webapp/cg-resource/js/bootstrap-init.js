var BootStrapInit = function () {
	
	// IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;
    
    // initializes main settings
    var handleInit = function() {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            $('html').addClass('ie10'); // detect IE10 version
        }

        if (isIE10 || isIE9 || isIE8) {
            $('html').addClass('ie'); // detect IE10 version
        }
    };

    return {
        //main function to initiate the module
        init: function () {
        	
        	handleInit(); // initialize core variables
        	
        },
    
	    // check IE8 mode
	    isIE8: function() {
	        return isIE8;
	    },
	
	    // check IE9 mode
	    isIE9: function() {
	        return isIE9;
	    },
	
	    //check RTL mode
	    isRTL: function() {
	        return isRTL;
	    },
    
    };

}();