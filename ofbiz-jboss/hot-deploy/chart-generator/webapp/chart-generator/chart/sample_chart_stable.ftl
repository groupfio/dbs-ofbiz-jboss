<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Sample} <small>${uiLabelMap.FlowChart}</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-12 col-sm-12">
				
		<div class="portlet-body form">
		
			<div class="mermaid">
				
				graph LR
				
				title[<u>Campaign Diagram</u>]
				title-->A
				style title fill:#FFF,stroke:#FFF
				linkStyle 0 stroke:#FFF,stroke-width:0;
				
				A[Create campaign <br> workflow Camp1] --> B(Setup list with<br>Referral Segment)
				B --> C(Create List)
				C --> D(Apply Exlusion<br>Filters)
				D --> E(fa:fa-car <br>Configure<br>Template ID)
				E --> F(Generate<br>Untagged Data File<br>Based on List)
				F --> G(Configure Template<br>Parameters)
				G --> |TO|H(Publish Campaign<br>Camp1)
				G --> |T+7|I{Check Campaign Cond<br>Service on 7th Day}
				
				style B fill:#f9f,stroke:#333,stroke-width:4px
				
				click A "http://www.google.com" "link test"
				
			</div>
			
			<#-- <h1 style="margin-top:150px; text-align:center">jQuery flowSVG Plugin Demo</h1>
			<div id="drawing" style="margin:30px auto; width:900px;"></div> -->					
																	
		</div>
						
	</div>
	
</div>

<script>

mermaid.initialize({ theme: 'neutral' });


/*
flowSVG.draw(SVG('drawing').size(900, 1100));
    flowSVG.config({
    	interactive: false,
    	showButtons: false,
    	connectorLength: 60
    });
    flowSVG.shapes(
    	[{
    			label: 'tops',
    			type: 'decision',
    			moveRight: 500,
    			text: ['Boss'],
    			orient: {
    				yes: 'b',
    				no: 'r'
    					// If there is a next...
    					//next: 'r'
    			},
    			yes: 'siblingA',
    			no: 'siblingB'
    		}, {
    			label: 'siblingA',
    			type: 'process',
    			text: ['Brother'],
    			next: 'end'
    		}, {
    			label: 'siblingB',
    			type: 'process',
    			text: ['Sister'],
    			next: 'end'
    		}, {
    			label: 'end',
    			type: 'finish',
    			text: ['done']
    		}

    	]);
*/

///////////////////// start flow chart ////////////////////////////////////////////////////////////

/*
    flowSVG.draw(SVG('drawing').size(9000, 11000));
    
    flowSVG.config({
        interactive: false,
        showButtons: false,
        connectorLength: 60,
        scrollto: true
    });
    
    flowSVG.shapes(
		[
        
            {
				label: 'createCampaingWorkFlow',
				type: 'process',
				text: [
	                'Create',
	                'Campaign',
	                'WorkFlow',
	                'Camp1'
	            ],
	            next: 'referrelSegment'
	        },
	        
	        {
				label: 'referrelSegment',
				type: 'process',
				text: [
	                'Setup List with',
	                'Referral Segment',
	            ],
	            next: 'createList'
	        },
	        
	        {
				label: 'createList',
				type: 'process',
				text: [
	                'Create List',
	            ],
	            next: 'applyExclusionFilter'
	        },
	        
	        {
				label: 'applyExclusionFilter',
				type: 'process',
				text: [
	                'Apply Exclusion',
	                'Filters'
	            ],
	            next: 'configureTemplateId'
	        },
	        
	        {
				label: 'configureTemplateId',
				type: 'process',
				text: [
	                'Configure',
	                'Template ID'
	            ],
	            next: 'generateUntaggedDataFile'
	        },
	        
	        {
				label: 'generateUntaggedDataFile',
				type: 'process',
				text: [
	                'Generate',
	                'Untagged data file',
	                'Base on List'
	            ],
	            next: 'configureTemplateParameters'
	        },
	        
	        {
				label: 'configureTemplateParameters',
				type: 'process',
				text: [
	                'Configure Template',
	                'Parameters',
	            ],
	            next: 'checkCampaignCondition'
	        },
	        
	        {
				label: 'checkCampaignCondition',
				type: 'decision',
				text: [
	                'Check',
	                'Campaign Condition',
	                'Campaign Condition',
	                'Service on 7th',
	                'Day'
	            ],
	            next: ''
	        },
	        
	        {
				label: 'publishCampaign',
				type: 'process',
				text: [
	                'Check',
	                'Campaign Condition',
	                'Campaign Condition',
	                'Service on 7th',
	                'Day'
	            ],
	            next: ''
	        },
                    
    	]
    );

    */
</script>
