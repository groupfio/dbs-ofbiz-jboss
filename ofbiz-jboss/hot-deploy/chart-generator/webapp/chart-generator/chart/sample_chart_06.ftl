<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${marketingCampaign.campaignName}[${marketingCampaign.marketingCampaignId?if_exists}] <i class="fa fa-arrow-right" aria-hidden="true"></i> Flow Chart</h1>
	<div class="float-right">
		<a href="viewMarketingCampaign?marketingCampaignId=${marketingCampaign.marketingCampaignId}&activeTab=drip" class="btn btn-xs btn-primary m5 tooltips" title="Back to ${marketingCampaign.campaignName!}" >Back</a>
	</div>
</div>

<div class="row padding-r">
	<div class="col-md-12 col-sm-12">
				
		<div class="portlet-body form">
			<#-- 
			<strong>Dynamically Generated flow chart: [ Usecase 2 ]</strong> <br><br> ${generatedChartDataVCase2!}
			<br><br>
			 -->
			<#--  
			<div class="mermaid">
			
				${generatedChartDataVCase2!}
			
			</div>
			 -->
			
			<div class="mermaid">
			
				${generatedChartDataHCase2!}
			
			</div>
																																																
		</div>
						
	</div>
	
</div>

<script>

</script>

<script src="/cg-resource/js/mermaid.min.js" type="text/javascript"></script>
<!--<script>mermaid.initialize({startOnLoad: false});</script>-->

<script>

mermaid.initialize({
  mermaid: {
     startOnLoad: true
  }
});

//mermaid.initialize();

</script>

<link href="/cg-resource/css/custom.css" rel="stylesheet" type="text/css"/>
