<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Sample} <small>${uiLabelMap.FlowChart}</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-12 col-sm-12">
				
		<div class="portlet-body form">
			
			<script>
			    var callback = function(param){
			        //alert('A callback was triggered');
			        alert('parameter: '+param);
			    }
			</script>
			
			<strong>Static flow chart:</strong> <br><br> 
			   
			<div class="mermaid">
				
				graph TB
				
				title[<u>Campaign Diagram</u>]
				title-->A
				style title fill:#FFF,stroke:#FFF
				linkStyle 0 stroke:#FFF,stroke-width:0;
				
				A[Create campaign <br> workflow Camp1] --> B(Setup list with<br>Referral Segment)
				B --> C(Create List)
				C --> D(Apply Exlusion<br>Filters)
				D --> E(fa:fa-car <br>Configure<br>Template ID)
				E --> F(Generate<br>Untagged Data File<br>Based on List)
				F --> G(Configure Template<br>Parameters)
				G --> |TO|H(Publish Campaign<br>Camp1)
				G --> |T+7|I{Check Campaign Cond<br>Service on 7th Day}
				I --> J(Remind to follow up<br>Camp2)
				I --> K(You seem interested..<br>Camp3)
				H --> |Not Clicked|L(Refer now and get $500!<br>Camp4)
				J --> M{Check<br>Campaign Condition}
				K --> M
				L --> N{Check<br>Campaign Condition}
				M --> |T14|O(Congrats! your<br>referral has applied<br>Camp5)
				N --> |T14|O
				
				style B fill:#f9f,stroke:#333,stroke-width:4px
				
				click A "http://www.google.com" "link test"
				
			</div>
			
			<strong>Customized design</strong> <br><br>
			<br><br>
			
			<div class="mermaid">
				
				graph TB
				title(Campaign Diagram  )
				title-->A
				style title fill:#337ab7,stroke:#84adec;
				A(Create campaign <br> workflow Camp1) --> B(Setup list with <br>Referral Segment  ) 
				B --> C(fa:fa-plus <br> Create<br> List)
				C --> D(fa:fa-wrench <br>Apply Exlusion<br>Filters)
				D --> E(fa:fa-cogs <br>Configure<br>Template ID)
				E --> F(fa:fa-filter Generate<br>Untagged Data File <br>Based on List)
				F --> G(fa:fa-cog<br> Configure Template  <br>Parameters)
				G --> |TO|H(Publish Campaign<br>Camp1)
				G --> |T+7|I{Check Campaign<br> Cond Service <br>on 7th Day}
				I --> J(Remind to follow up<br>Camp2)
				I --> K(You seem interested..<br>Camp3)
				H --> |Not Clicked|L(Refer now and get $500!<br>Camp4)
				J --> M{Check<br>Campaign Condition}
				K --> M
				L --> N{Check<br>Campaign Condition}
				M --> |T14|O(Congrats! your<br>referral has applied<br>Camp5)
				N --> |T14|O		
				click A "http://www.google.com" "link test"			
			</div>	
			
													
		</div>
						
	</div>
	
</div>

<script src="/cg-resource/js/mermaid.min.js" type="text/javascript"></script>
<link href="/cg-resource/css/custom.css" rel="stylesheet" type="text/css"/>
