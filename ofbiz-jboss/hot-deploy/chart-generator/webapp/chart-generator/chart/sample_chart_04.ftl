<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.FlowChart}</h1>
</div>

<div class="row padding-r">
	<div class="col-md-12 col-sm-12">
				
		<div class="portlet-body form">
			
			<strong>Dynamically Generated flow chart: [ Usecase 1 ]</strong> <br><br> ${generatedChartData!}
			<br><br>
			
			<div class="mermaid">
			
				${generatedChartData!}
			
			</div>
			
			<div class="mermaid">
			
				${generatedChartData2!}
			
			</div>
			
			<br><br><br>
			<strong>Dynamically Generated flow chart: [ Usecase 2 ]</strong> <br><br> ${generatedChartDataVCase2!}
			<br><br>
			
			<div class="mermaid">
			
				${generatedChartDataVCase2!}
			
			</div>
			
			<div class="mermaid">
			
				${generatedChartDataHCase2!}
			
			</div>
																																																
		</div>
						
	</div>
	
</div>

<script>

</script>

<script src="/cg-resource/js/mermaid.min.js" type="text/javascript"></script>
<!--<script>mermaid.initialize({startOnLoad: false});</script>-->

<script>

mermaid.initialize({
  mermaid: {
     startOnLoad: true
  }
});

//mermaid.initialize();

</script>
