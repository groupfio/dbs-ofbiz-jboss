<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Update} <small>${uiLabelMap.CustomFieldGroup}</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
		
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>updateCustomFieldGroup</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
																					
			<div class="form-body">
			
			<@readonlyInput 
				id="groupId"
				label=uiLabelMap.groupId
				value=customFieldGroup.groupId
				isHiddenInput=true
				/>
			
			<@generalInput 
				id="groupName"
				label=uiLabelMap.groupName
				placeholder=uiLabelMap.groupName
				value=customFieldGroup.groupName
				tooltip = uiLabelMap.groupName
				required=true
				/>
			
			<@generalInput 
				id="sequence"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customFieldGroup.sequence
				tooltip = uiLabelMap.sequence
				inputType="number"
				required=true
				min=1
				/>	
																																																																																																																																																																																																																																																					
			</div>
			
			<@fromCommonAction showCancelBtn=true cancelUrl="customFieldGroup"/>
			
		</form>			
							
		</div>
			
	</div>
	
</div>
