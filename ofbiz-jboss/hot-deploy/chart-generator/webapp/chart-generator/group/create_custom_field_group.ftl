<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.Create} <small>${uiLabelMap.CustomFieldGroup}</small></h1>
</div>

<div class="row padding-r">
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
			<form role="form" class="form-horizontal" action="<@ofbizUrl>createCustomFieldGroup</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">
				
			<div class="form-body">
			
			<@generalInput 
				id="groupId"
				label=uiLabelMap.groupId
				placeholder=uiLabelMap.groupId
				value=customFieldGroup.groupId
				tooltip = uiLabelMap.groupId
				required=true
				/>
			
			<@generalInput 
				id="groupName"
				label=uiLabelMap.groupName
				placeholder=uiLabelMap.groupName
				value=customFieldGroup.groupName
				tooltip = uiLabelMap.groupName
				required=true
				/>
			
			<@generalInput 
				id="sequence"
				label=uiLabelMap.sequence
				placeholder=uiLabelMap.sequence
				value=customFieldGroup.sequence
				tooltip = uiLabelMap.sequence
				inputType="number"
				required=true
				min=1
				/>				
															
			</div>
			
			<@fromCommonAction showCancelBtn=false/>
			
		</form>			
							
		</div>
						
	</div>
	
</div>
