import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

//currencyCode = parameters.get("currencyCode");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "lastUpdatedStamp";
}

Debug.logInfo("orderField>>> "+orderField,"SearchCurrencyConversion");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

//conditionsList.add(EntityCondition.makeCondition("uomTypeId", EntityOperator.EQUALS, "CURRENCY_MEASURE"));
/*
if (UtilValidate.isNotEmpty(currencyCode)) {
	conditionsList.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("uomId")), EntityOperator.EQUALS, currencyCode.toUpperCase()));
}

if (UtilValidate.isNotEmpty(isEnabled)) {
	conditionsList.add(EntityCondition.makeCondition("isEnabled", EntityOperator.EQUALS, isEnabled));
}
*/

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

Debug.logInfo("mainConditons>>> "+mainConditons,"ListCurrencyConversion");

Debug.logInfo("ProductSummary start count>>> "+UtilDateTime.nowTimestamp(),"ListCurrencyConversion");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("UomConversionDated", mainConditons, null, UtilMisc.toSet("uomId"), efoNum);
//count = delegator.findCountByCondition("ProductSummary", mainConditons, null, efoNum);

Debug.logInfo("UomConversionDated end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"ListCurrencyConversion");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

entryList = delegator.findList("UomConversionDated", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
int recordsFiltered = count;
int recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
entryList.each{entry ->
	JSONObject result = new JSONObject();
	result.putAll(entry);
	
	result.put("conversionMethodDesc", org.groupfio.homeapps.util.DataUtil.getSystemConfigDesc(delegator, entry.getString("conversionMethod")));
	
	result.put("fromDate", UtilValidate.isNotEmpty(entry.get("fromDate")) ? UtilDateTime.timeStampToString(entry.getTimestamp("fromDate"), "dd/MM/yyyy", TimeZone.getDefault(), null) : "");
	result.put("thruDate", UtilValidate.isNotEmpty(entry.get("thruDate")) ? UtilDateTime.timeStampToString(entry.getTimestamp("thruDate"), "dd/MM/yyyy", TimeZone.getDefault(), null) : "");
	result.put("lastUpdatedStamp", UtilValidate.isNotEmpty(entry.get("lastUpdatedStamp")) ? UtilDateTime.timeStampToString(entry.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);