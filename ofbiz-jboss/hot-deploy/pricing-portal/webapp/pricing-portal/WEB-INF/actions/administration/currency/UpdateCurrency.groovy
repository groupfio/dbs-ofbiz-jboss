import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

currency = new HashMap();

currencyCode = request.getParameter("currencyCode");
if (UtilValidate.isNotEmpty(currencyCode)) {
	currency = EntityUtil.getFirst( delegator.findByAnd("Uom",UtilMisc.toMap("uomId", currencyCode, "uomTypeId", "CURRENCY_MEASURE"), null, false) );
}

context.put("currency", currency);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);