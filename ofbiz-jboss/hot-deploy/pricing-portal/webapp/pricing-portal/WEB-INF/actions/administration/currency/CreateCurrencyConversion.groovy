import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

currency = new HashMap();
context.put("currency", currency);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("uomTypeId", EntityOperator.EQUALS, "CURRENCY_MEASURE"));
conditionsList.add(EntityCondition.makeCondition([
			EntityCondition.makeCondition("isEnabled", EntityOperator.EQUALS, null),
			EntityCondition.makeCondition("isEnabled", EntityOperator.EQUALS, "Y")
		], EntityOperator.OR));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
currencyList = delegator.findList("Uom", mainConditons, null, UtilMisc.toList("uomId"), null, false);
context.put("currencyList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyList, "uomId", "uomId", "description"));

conversionMethodList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CONV_METHD"), java.util.Arrays.asList("sequence ASC"), false);
context.put("conversionMethodList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(conversionMethodList, "profileConfigurationId", "profileDescription"));

