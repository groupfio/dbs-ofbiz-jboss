import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

filterProfileConfig = new HashMap();
context.put("filterProfileConfig", filterProfileConfig);

profileTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "PROFILE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("profileTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(profileTypeList, "profileCode", "profileDescription"));
