import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

profileConfigurationId = parameters.get("profileConfigurationId");
profileType = parameters.get("profileType");
profileCode = parameters.get("profileCode");
profileDescription = parameters.get("profileDescription");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "createdStamp";
}

Debug.logInfo("orderField>>> "+orderField,"SearchPhm");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

conditionsList.add(EntityCondition.makeCondition("profileTypeId", EntityOperator.NOT_EQUAL, "PROFILE_TYPE"));

if (UtilValidate.isNotEmpty(profileConfigurationId)) {
	conditionsList.add(EntityCondition.makeCondition("profileConfigurationId", EntityOperator.EQUALS, profileConfigurationId));
}

if (UtilValidate.isNotEmpty(profileType)) {
	conditionsList.add(EntityCondition.makeCondition("profileTypeId", EntityOperator.EQUALS, profileType));
}

if (UtilValidate.isNotEmpty(profileCode)) {
	EntityCondition nameCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("profileCode")), EntityOperator.LIKE, "%"+profileCode.toUpperCase()+"%")
		//EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("groupName")), EntityOperator.LIKE, "%"+searchFirstName.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(nameCondition);
}

if (UtilValidate.isNotEmpty(profileDescription)) {
	EntityCondition nameCondition = EntityCondition.makeCondition([
		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("profileDescription")), EntityOperator.LIKE, "%"+profileDescription.toUpperCase()+"%")
		//EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("groupName")), EntityOperator.LIKE, "%"+searchFirstName.toUpperCase()+"%")
	], EntityOperator.OR);
	conditionsList.add(nameCondition);
}

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

Debug.logInfo("mainConditons>>> "+mainConditons,"ProfileConfiguration");

Debug.logInfo("ProfileConfiguration start count>>> "+UtilDateTime.nowTimestamp(),"ProfileConfiguration");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("ProfileConfiguration", mainConditons, null, UtilMisc.toSet("profileConfigurationId"), efoNum);
//count = delegator.findCountByCondition("ProfileConfiguration", mainConditons, null, efoNum);

Debug.logInfo("ProfileConfiguration end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"ProfileConfiguration");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

Debug.logInfo("ProfileConfiguration start list>>> "+UtilDateTime.nowTimestamp(),"ProfileConfiguration");
entryList = delegator.findList("ProfileConfiguration", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
Debug.logInfo("entryList.size(): "+entryList.size(),"ProfileConfiguration");
Debug.logInfo("ProfileConfiguration end list>>> "+UtilDateTime.nowTimestamp(),"ProfileConfiguration");
int recordsFiltered = count;
int recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
entryList.each{entry ->
	JSONObject result = new JSONObject();
	result.putAll(entry);
	
	String profileTypeDesc = "";
	profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileCode", entry.getString("profileTypeId")), null, false) );
	if (UtilValidate.isNotEmpty(profileConfiguration)) {
		profileTypeDesc = profileConfiguration.getString("profileDescription");
	}
	result.put("profileTypeDesc", profileTypeDesc);
	
	result.put("lastUpdatedStamp", UtilValidate.isNotEmpty(entry.get("lastUpdatedStamp")) ? UtilDateTime.timeStampToString(entry.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);