import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

transactionProductAssocId = request.getParameter("transactionProductAssocId");

transChargeMapping = new HashMap();

transChargeMapping.put("transactionProductAssocId", transactionProductAssocId);

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "SERVICE"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
serviceCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("serviceCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceCodeList, "productId", "productIdAlt"));

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "DIGITAL_GOOD"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
chargeCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("chargeCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeCodeList, "productId", "productIdAlt"));

transAttrLeftList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "1", "hide", "N", "attrType", "TA_CC_MAP"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrLeftList", transAttrLeftList);

transAttrRightList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "2", "hide", "N", "attrType", "TA_CC_MAP"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrRightList", transAttrRightList);

if (UtilValidate.isNotEmpty(transactionProductAssocId)) {

	transProdAssoc = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssoc", UtilMisc.toMap("transactionProductAssocId", transactionProductAssocId), null, false) );
	if (UtilValidate.isNotEmpty(transProdAssoc)) {
		//transChargeMapping.put("productId", productId);
		productId = transProdAssoc.getString("productId");
		product = EntityUtil.getFirst( delegator.findByAnd("Product",UtilMisc.toMap("productId", transProdAssoc.getString("productId")), null, false) );
		if (UtilValidate.isNotEmpty(product)) {
			
			String serviceProductId = "";
			String chargeProductId = "";
			String serviceCode = "";
			String chargeCode = "";
			
			if (transProdAssoc.getString("productTypeId").equals("DIGITAL_GOOD")) {
				chargeCode = product.getString("productIdAlt");
				chargeProductId = product.getString("productId");
				attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "CHARGE_CODE", "attrName", "SERVICE_CODE"), null, false) );
				if (UtilValidate.isNotEmpty(attrInstance)) {
					product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productIdAlt", attrInstance.getString("attrValue")), null, false) );
					serviceCode = product.getString("productIdAlt");
					serviceProductId = product.getString("productId");
				}
			} else if (transProdAssoc.getString("productTypeId").equals("SERVICE")) {
				serviceCode = product.getString("productIdAlt");
				serviceProductId = product.getString("productId");
			}
			
			transChargeMapping.put("serviceCode", serviceProductId);
			transChargeMapping.put("chargeCode", chargeProductId);
			
			transChargeMapping.put("fromDate", UtilValidate.isNotEmpty(transProdAssoc.get("fromDate")) ? UtilDateTime.timeStampToString(transProdAssoc.getTimestamp("fromDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
			transChargeMapping.put("thruDate", UtilValidate.isNotEmpty(transProdAssoc.get("thruDate")) ? UtilDateTime.timeStampToString(transProdAssoc.getTimestamp("thruDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");	
		
			transChargeMapping.put("description", transProdAssoc.get("description"));
		}
		
		transAttrLeftList.each{transAttr ->
			String attrName = transAttr.getString("attrName");
			String attrValue = "";
			codeValue = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssocSummary", UtilMisc.toMap("productId", productId, "attrName", attrName), null, false) );
			if (UtilValidate.isNotEmpty(codeValue)) {
				attrValue = codeValue.getString("attrValue");
			}
			transChargeMapping.put(attrName, attrValue);				
		}
		
		transAttrRightList.each{transAttr ->
			String attrName = transAttr.getString("attrName");
			String attrValue = "";
			codeValue = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssocSummary", UtilMisc.toMap("productId", productId, "attrName", attrName), null, false) );
			if (UtilValidate.isNotEmpty(codeValue)) {
				attrValue = codeValue.getString("attrValue");
			}
			transChargeMapping.put(attrName, attrValue);				
		}
		
	}
		
}

context.put("transChargeMapping", transChargeMapping);

/*
appStatusList = UtilMisc.toMap("ACTIVATED", uiLabelMap.get("activated"), "DEACTIVATED", uiLabelMap.get("deActivated"));
context.put("appStatusList", appStatusList);

serviceTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SERVICE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("serviceTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceTypeList, "profileConfigurationId", "profileDescription"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));
*/

