import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

serviceCode = parameters.get("serviceCode");
serviceType = parameters.get("serviceType");
currencyType = parameters.get("currencyType");
bundleCurrency = parameters.get("bundleCurrency");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "lastUpdatedStamp";
}

Debug.logInfo("orderField>>> "+orderField,"SearchPhm");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "SERVICE"));

if (UtilValidate.isNotEmpty(serviceCode)) {
	conditionsList.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("productIdAlt")), EntityOperator.LIKE, "%"+serviceCode.toUpperCase()+"%"));
}

if (UtilValidate.isNotEmpty(serviceType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "PRODUCT_HIERARCHY_MAINTENANCE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "SERVICE_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, serviceType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(currencyType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "PRODUCT_HIERARCHY_MAINTENANCE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, currencyType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(bundleCurrency)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "PRODUCT_HIERARCHY_MAINTENANCE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "BUNDLE_CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, bundleCurrency)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

/*
fromDate = parameters.get("fromDate");
thruDate = parameters.get("thruDate");
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
if (UtilValidate.isNotEmpty(fromDate)) {
	fromdateVal=fromDate+" "+"00:00:00.0";
	Date parsedDateFrom = dateFormat.parse(fromdateVal);
	Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "lastUpdated" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "lastUpdated" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
}
if (UtilValidate.isNotEmpty(thruDate)) { 
	thruDateVal=thruDate+" "+"23:23:59.0";
	Date parsedDateThru = dateFormat.parse(thruDateVal);
	Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "lastUpdated" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "lastUpdated" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
}
*/
EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

Debug.logInfo("mainConditons>>> "+mainConditons,"ProductSummary");

Debug.logInfo("ProductSummary start count>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("ProductSummary", mainConditons, null, UtilMisc.toSet("productIdAlt"), efoNum);
//count = delegator.findCountByCondition("ProductSummary", mainConditons, null, efoNum);

Debug.logInfo("ProductSummary end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"ProductSummary");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

Debug.logInfo("ProductSummary start list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
entryList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
Debug.logInfo("entryList.size(): "+entryList.size(),"ProductSummary");
Debug.logInfo("ProductSummary end list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
int recordsFiltered = count;
int recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
entryList.each{entry ->
	JSONObject result = new JSONObject();
	//result.putAll(entry);
	
	result.put("productId", entry.getString("productId"));
	result.put("productIdAlt", entry.getString("productIdAlt"));
	result.put("description", entry.getString("internalName"));
	result.put("serviceCode", entry.getString("productIdAlt"));
	
	String serviceType = "";
	attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", entry.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "SERVICE_TYPE"), null, false) );
	if (UtilValidate.isNotEmpty(attrInstance)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", attrInstance.getString("attrValue")), null, false) );
		serviceType = profileConfiguration.getString("profileDescription");
		
	}
	result.put("serviceType", serviceType);
	
	String currencyType = "";
	attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", entry.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "CURRENCY_TYPE"), null, false) );
	if (UtilValidate.isNotEmpty(attrInstance)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", attrInstance.getString("attrValue")), null, false) );
		currencyType = profileConfiguration.getString("profileDescription");
	}
	result.put("currencyType", currencyType);
	
	String bundleCurrency = "";
	attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", entry.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "BUNDLE_CURRENCY_TYPE"), null, false) );
	if (UtilValidate.isNotEmpty(attrInstance)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", attrInstance.getString("attrValue")), null, false) );
		bundleCurrency = profileConfiguration.getString("profileDescription");
	}
	result.put("bundleCurrency", bundleCurrency);
		
	result.put("lastUpdatedStamp", UtilValidate.isNotEmpty(entry.get("lastUpdatedStamp")) ? UtilDateTime.timeStampToString(entry.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);