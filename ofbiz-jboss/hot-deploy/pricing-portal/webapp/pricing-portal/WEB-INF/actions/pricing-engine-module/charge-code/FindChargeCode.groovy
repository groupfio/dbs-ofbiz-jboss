import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

filterChargeCode = new HashMap();
context.put("filterChargeCode", filterChargeCode);

searchConditionList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CC_SRC_COND"), java.util.Arrays.asList("sequence ASC"), false);
context.put("searchConditionList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(searchConditionList, "profileConfigurationId", "profileDescription"));

searchConditionOpertorList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CC_SRC_COND_OPR"), java.util.Arrays.asList("sequence ASC"), false);
context.put("searchConditionOpertorList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(searchConditionOpertorList, "profileConfigurationId", "profileDescription"));
