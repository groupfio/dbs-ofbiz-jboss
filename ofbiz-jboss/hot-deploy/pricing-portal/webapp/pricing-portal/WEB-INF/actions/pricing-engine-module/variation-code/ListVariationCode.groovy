import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

searchCondition = parameters.get("searchCondition");
searchConditionOpertor = parameters.get("searchConditionOpertor");
searchValue = parameters.get("searchValue");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "createdStamp";
}

sortDir = "";
orderField = "createdStamp";

Debug.logInfo("orderField>>> "+orderField, "ListVariationCode");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

conditionsList.add(EntityUtil.getFilterByDateExpr());

if (UtilValidate.isNotEmpty(searchCondition) && UtilValidate.isNotEmpty(searchConditionOpertor) && UtilValidate.isNotEmpty(searchValue)) {
	if (searchConditionOpertor.equals("SC_OPR_1000")) {
		EntityCondition condition = EntityCondition.makeCondition([
			EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, searchCondition),
			EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("attrValue")), EntityOperator.EQUALS, searchValue)
		], EntityOperator.AND);
		conditionsList.add(condition);
	}
}

/*
if (UtilValidate.isNotEmpty(serviceCode)) {
	conditionsList.add(EntityCondition.makeCondition("productIdAlt", EntityOperator.EQUALS, serviceCode));
}

if (UtilValidate.isNotEmpty(serviceType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "SERVICE_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, serviceType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(currencyType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, currencyType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(bundleCurrency)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "BUNDLE_CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, bundleCurrency)
	], EntityOperator.AND);
	conditionsList.add(condition);
}
*/

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

//Debug.logInfo("mainConditons>>> "+mainConditons,"ProductSummary");

//Debug.logInfo("ProductSummary start count>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("VariationCodeSummary", mainConditons, null, UtilMisc.toSet("variationCodeId"), efoNum);
//count = delegator.findCountByCondition("ProductSummary", mainConditons, null, efoNum);

//Debug.logInfo("ProductSummary end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"ProductSummary");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

//Debug.logInfo("ProductSummary start list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
entryList = delegator.findList("VariationCodeSummary", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
//Debug.logInfo("entryList.size(): "+entryList.size(),"ProductSummary");
//Debug.logInfo("ProductSummary end list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
int recordsFiltered = count;
int recordsTotal = count;

transAttrList = delegator.findByAnd("TransactionAttribute", null, java.util.Arrays.asList("sequence ASC"), false);

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
entryList.each{entry ->
	JSONObject result = new JSONObject();
	//result.putAll(entry);
	
	String variationCodeId = entry.getString("variationCodeId");
	
	result.put("variationCodeId", variationCodeId);
	result.put("description", entry.getString("description"));
	
	transAttrList.each{transAttr ->
		String attrName = transAttr.getString("attrName");
		String attrValue = "";
		codeValue = EntityUtil.getFirst( delegator.findByAnd("VariationCodeValue", UtilMisc.toMap("variationCodeId", variationCodeId, "attrName", attrName), null, false) );
		if (UtilValidate.isNotEmpty(codeValue)) {
			attrValue = codeValue.getString("attrValue");
		}
		result.put(attrName, attrValue);				
	}
	
	result.put("createdStamp", UtilValidate.isNotEmpty(entry.get("createdStamp")) ? UtilDateTime.timeStampToString(entry.getTimestamp("createdStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);