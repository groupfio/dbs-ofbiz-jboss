import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

chargeCode = new HashMap();

productId = request.getParameter("productId");
if (UtilValidate.isNotEmpty(productId)) {
	product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productId, "productTypeId", "DIGITAL_GOOD"), null, false) );
	if (UtilValidate.isNotEmpty(product)) {
		chargeCode.put("productId", productId);
		chargeCode.put("chargeCode", product.getString("productIdAlt"));
		chargeCode.put("description", product.getString("internalName"));
		
		String chargeType = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "CHARGE_TYPE");
		chargeCode.put("chargeType", chargeType);
		
		String debitSystemType = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "DEBIT_SYSTEM_TYPE");
		chargeCode.put("debitSystemType", debitSystemType);
		
		String serviceCode = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "SERVICE_CODE");
		if (UtilValidate.isNotEmpty(serviceCode)) {
			serviceProduct = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productIdAlt", serviceCode), null, false) );
			if (UtilValidate.isNotEmpty(serviceProduct)) {
				chargeCode.put("serviceCode", serviceProduct.get("productId"));
			}
		}
		
		String invoiceRequired = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "INVOICE_REQUIRED");
		chargeCode.put("invoiceRequired", invoiceRequired);
		
		String printFlag = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "PRINT_FLAG");
		chargeCode.put("printFlag", printFlag);
		
		String priceFreq = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "PRICE_FREQ");
		chargeCode.put("priceFreq", priceFreq);
		
		String pricingDay = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "PRICING_DAY");
		chargeCode.put("pricingDay", pricingDay);
		
		String billAddressLevel = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "BILL_ADDRESS_LEVEL");
		chargeCode.put("billAddressLevel", billAddressLevel);
		
		String invFreq = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "INV_FREQ");
		chargeCode.put("invFreq", invFreq);
		
		String invDay = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "INV_DAY");
		chargeCode.put("invDay", invDay);
		
		String currencyType = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "CURRENCY_TYPE");
		chargeCode.put("currencyType", currencyType);
		
		String bundleCurrency = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, productId, "CHARGE_CODE", "BUNDLE_CURRENCY_TYPE");
		chargeCode.put("bundleCurrency", bundleCurrency);
		
		EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
				EntityCondition.makeCondition("productId", EntityOperator.EQUALS, productId),
				//EntityCondition.makeCondition("glAccountId", EntityOperator.EQUALS, glAccountId),
				EntityUtil.getFilterByDateExpr()
			);
		GenericValue productGlAssoc = EntityUtil.getFirst(delegator.findList("ProductGlAccountAssoc", condition, null, null, null, false));
	    if (UtilValidate.isNotEmpty(productGlAssoc)) {
	    	chargeCode.put("glAccountId", productGlAssoc.getString("glAccountId"));
	    	chargeCode.put("glCurrency", productGlAssoc.getString("glCurrency"));
	    	chargeCode.put("pcCode", productGlAssoc.getString("pcCode"));
	    }			
		
	}
}

context.put("chargeCode", chargeCode);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

chargeTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CHARGE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("chargeTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeTypeList, "profileConfigurationId", "profileDescription"));

debitSystemTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "DEBIT_SYSTEM_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("debitSystemTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(debitSystemTypeList, "profileConfigurationId", "profileDescription"));

serviceCodeList = delegator.findByAnd("Product", UtilMisc.toMap("productTypeId", "SERVICE"), java.util.Arrays.asList("productIdAlt ASC"), false);
context.put("serviceCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceCodeList, "productId", "internalName"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

printFlagList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "PRINT_ZERO"), java.util.Arrays.asList("sequence ASC"), false);
context.put("printFlagList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(printFlagList, "profileConfigurationId", "profileDescription"));

priceFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "PRICE_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("priceFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(priceFreqList, "profileConfigurationId", "profileDescription"));

billAddressLevelList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BILL_ADDRESS_LEVEL"), java.util.Arrays.asList("sequence ASC"), false);
context.put("billAddressLevelList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(billAddressLevelList, "profileConfigurationId", "profileDescription"));

invFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "INV_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("invFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(invFreqList, "profileConfigurationId", "profileDescription"));

context.put("dayList", org.groupfio.homeapps.util.DataHelper.getDayDropDownOptions());

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("uomTypeId", EntityOperator.EQUALS, "CURRENCY_MEASURE"));
conditionsList.add(EntityCondition.makeCondition([
			EntityCondition.makeCondition("isEnabled", EntityOperator.EQUALS, null),
			EntityCondition.makeCondition("isEnabled", EntityOperator.EQUALS, "Y")
		], EntityOperator.OR));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
currencyList = delegator.findList("Uom", mainConditons, null, UtilMisc.toList("uomId"), null, false);
context.put("currencyList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyList, "uomId", "uomId", "description"));
