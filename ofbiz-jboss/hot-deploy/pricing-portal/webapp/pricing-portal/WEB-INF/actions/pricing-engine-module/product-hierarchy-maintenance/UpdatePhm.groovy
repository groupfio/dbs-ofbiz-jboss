import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

prodHierMain = new HashMap();

productId = request.getParameter("productId");
if (UtilValidate.isNotEmpty(productId)) {
	product = EntityUtil.getFirst( delegator.findByAnd("Product",UtilMisc.toMap("productId", productId, "productTypeId", "SERVICE"), null, false) );
	if (UtilValidate.isNotEmpty(product)) {
		prodHierMain.put("productId", productId);
		prodHierMain.put("serviceCode", product.getString("productIdAlt"));
		prodHierMain.put("description", product.getString("internalName"));
		
		String serviceType = "";
		attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "SERVICE_TYPE"), null, false) );
		if (UtilValidate.isNotEmpty(attrInstance)) {
			serviceType = attrInstance.getString("attrValue");
		}
		prodHierMain.put("serviceType", serviceType);
		
		String currencyType = "";
		attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "CURRENCY_TYPE"), null, false) );
		if (UtilValidate.isNotEmpty(attrInstance)) {
			currencyType = attrInstance.getString("attrValue");
		}
		prodHierMain.put("currencyType", currencyType);
		
		String bundleCurrency = "";
		attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "BUNDLE_CURRENCY_TYPE"), null, false) );
		if (UtilValidate.isNotEmpty(attrInstance)) {
			bundleCurrency = attrInstance.getString("attrValue");
		}
		prodHierMain.put("bundleCurrency", bundleCurrency);
	}
}

context.put("prodHierMain", prodHierMain);

serviceTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SERVICE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("serviceTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceTypeList, "profileConfigurationId", "profileDescription"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));