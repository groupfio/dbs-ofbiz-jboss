import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

pricelistMain = new HashMap();
context.put("pricelistMain", pricelistMain);

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "DIGITAL_GOOD"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
chargeCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("chargeCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeCodeList, "productId", "productIdAlt", "internalName"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

conditionsList = FastList.newInstance();
conditionsList.add(EntityUtil.getFilterByDateExpr());
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
variationCodeList = delegator.findList("VariationCodeSummary", mainConditons, null, UtilMisc.toList("variationCodeId"), null, false);
context.put("variationCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(variationCodeList, "variationCodeId", "variationCodeId", "description"));

conditionsList = FastList.newInstance();
conditionsList.add(EntityUtil.getFilterByDateExpr());
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
pricingTemplateList = delegator.findList("PricingMethodSummary", mainConditons, null, UtilMisc.toList("pricingMethodId"), null, false);
context.put("pricingTemplateList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(pricingTemplateList, "pricingMethodId", "pricingMethodId", "description"));




rateAppliedOnList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "RATE_APPLIED_ON"), java.util.Arrays.asList("sequence ASC"), false);
context.put("rateAppliedOnList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(rateAppliedOnList, "profileConfigurationId", "profileDescription"));

discountRateTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "DCNT_RATE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("discountRateTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(discountRateTypeList, "profileConfigurationId", "profileDescription"));

appStatusList = UtilMisc.toMap("ACTIVATED", uiLabelMap.get("activated"), "DEACTIVATED", uiLabelMap.get("deActivated"));
context.put("appStatusList", appStatusList);

serviceTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SERVICE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("serviceTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceTypeList, "profileConfigurationId", "profileDescription"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

