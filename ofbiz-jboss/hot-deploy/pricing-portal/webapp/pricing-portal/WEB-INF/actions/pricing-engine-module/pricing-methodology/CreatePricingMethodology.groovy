import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

pricingMethod = new HashMap();
context.put("pricingMethod", pricingMethod);

appStatusList = UtilMisc.toMap("ACTIVATED", uiLabelMap.get("activated"), "DEACTIVATED", uiLabelMap.get("deActivated"));
context.put("appStatusList", appStatusList);

methodAttrLeftList = delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("binaryFlag", "1", "hide", "N"), java.util.Arrays.asList("pricingMethodAttributeId ASC"), false);
context.put("methodAttrLeftList", methodAttrLeftList);

methodAttrRightList = delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("binaryFlag", "2", "hide", "N"), java.util.Arrays.asList("pricingMethodAttributeId ASC"), false);
context.put("methodAttrRightList", methodAttrRightList);