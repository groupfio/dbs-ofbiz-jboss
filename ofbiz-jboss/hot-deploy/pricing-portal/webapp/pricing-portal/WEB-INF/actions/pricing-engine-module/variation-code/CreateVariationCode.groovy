import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

variationCode = new HashMap();
context.put("variationCode", variationCode);

transAttrLeftList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "1", "hide", "N", "attrType", "VARIATION_CODE"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrLeftList", transAttrLeftList);

transAttrRightList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "2", "hide", "N", "attrType", "VARIATION_CODE"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrRightList", transAttrRightList);

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "DIGITAL_GOOD"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
chargeCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("chargeCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeCodeList, "productIdAlt", "productIdAlt", "internalName"));