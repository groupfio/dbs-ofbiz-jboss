import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

searchCondition = parameters.get("searchCondition");
searchConditionOpertor = parameters.get("searchConditionOpertor");
searchValue = parameters.get("searchValue");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "lastUpdatedStamp";
}

Debug.logInfo("orderField>>> "+orderField,"SearchPhm");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "DIGITAL_GOOD"));

if (UtilValidate.isNotEmpty(searchCondition) && UtilValidate.isNotEmpty(searchConditionOpertor) && UtilValidate.isNotEmpty(searchValue)) {
	if (searchConditionOpertor.equals("CCSC_OPR_1000")) {
		if (searchCondition.equals("CCSC_1000")) {
			conditionsList.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("productIdAlt")), EntityOperator.EQUALS, searchValue.toUpperCase()));
		}
		
		if (searchCondition.equals("CCSC_1001")) {
			conditionsList.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("internalName")), EntityOperator.EQUALS, searchValue.toUpperCase()));
		}
		
		if (searchCondition.equals("CCSC_1002")) {
			EntityCondition condition = EntityCondition.makeCondition([
				EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
				EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "CHARGE_TYPE"),
				//EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, searchValue)
				EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("attrValue")), EntityOperator.EQUALS, searchValue.toUpperCase())
			], EntityOperator.AND);
			conditionsList.add(condition);
		}
		
		if (searchCondition.equals("CCSC_1003")) {
			EntityCondition condition = EntityCondition.makeCondition([
				EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
				EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "SERVICE_CODE"),
				//EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, searchValue)
				EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("attrValue")), EntityOperator.EQUALS, searchValue.toUpperCase())
			], EntityOperator.AND);
			conditionsList.add(condition);
		}
	}
}

/*
if (UtilValidate.isNotEmpty(serviceCode)) {
	conditionsList.add(EntityCondition.makeCondition("productIdAlt", EntityOperator.EQUALS, serviceCode));
}

if (UtilValidate.isNotEmpty(serviceType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "SERVICE_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, serviceType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(currencyType)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, currencyType)
	], EntityOperator.AND);
	conditionsList.add(condition);
}

if (UtilValidate.isNotEmpty(bundleCurrency)) {
	EntityCondition condition = EntityCondition.makeCondition([
		EntityCondition.makeCondition("attrType", EntityOperator.EQUALS, "CHARGE_CODE"),
		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "BUNDLE_CURRENCY_TYPE"),
		EntityCondition.makeCondition("attrValue", EntityOperator.EQUALS, bundleCurrency)
	], EntityOperator.AND);
	conditionsList.add(condition);
}
*/

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

Debug.logInfo("mainConditons>>> "+mainConditons,"ProductSummary");

Debug.logInfo("ProductSummary start count>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("ProductSummary", mainConditons, null, UtilMisc.toSet("productIdAlt"), efoNum);
//count = delegator.findCountByCondition("ProductSummary", mainConditons, null, efoNum);

Debug.logInfo("ProductSummary end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"ProductSummary");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

Debug.logInfo("ProductSummary start list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
entryList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
Debug.logInfo("entryList.size(): "+entryList.size(),"ProductSummary");
Debug.logInfo("ProductSummary end list>>> "+UtilDateTime.nowTimestamp(),"ProductSummary");
int recordsFiltered = count;
int recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
entryList.each{entry ->
	JSONObject result = new JSONObject();
	//result.putAll(entry);
	
	result.put("productId", entry.getString("productId"));
	result.put("productIdAlt", entry.getString("productIdAlt"));
	result.put("description", entry.getString("internalName"));
	result.put("serviceCode", entry.getString("productIdAlt"));
	result.put("chargeCode", entry.getString("productIdAlt"));
	
	String chargeType = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, entry.getString("productId"), "CHARGE_CODE", "CHARGE_TYPE");
	if (UtilValidate.isNotEmpty(chargeType)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", chargeType), null, false) );
		chargeType = profileConfiguration.getString("profileDescription");
		
	}
	result.put("chargeType", chargeType);
	/*
	String currencyType = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, entry.getString("productId"), "CHARGE_CODE", "CURRENCY_TYPE");
	if (UtilValidate.isNotEmpty(currencyType)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", currencyType), null, false) );
		currencyType = profileConfiguration.getString("profileDescription");
	}
	result.put("currencyType", currencyType);
	*/result.put("currencyType", "");
	String bundleCurrency = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, entry.getString("productId"), "CHARGE_CODE", "BUNDLE_CURRENCY_TYPE");
	if (UtilValidate.isNotEmpty(bundleCurrency)) {
		//profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", bundleCurrency), null, false) );
		//bundleCurrency = profileConfiguration.getString("profileDescription");
		uom = EntityUtil.getFirst( delegator.findByAnd("Uom", UtilMisc.toMap("uomId", bundleCurrency, "uomTypeId", "CURRENCY_MEASURE"), null, false) );
		println("uom>> "+uom);
		if (UtilValidate.isNotEmpty(uom)) {
			bundleCurrency = "("+bundleCurrency+") "+uom.getString("description");	
		}
		println("bundleCurrency>> "+bundleCurrency);
	}
	result.put("bundleCurrency", bundleCurrency);
	
	String printZero = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, entry.getString("productId"), "CHARGE_CODE", "PRINT_FLAG");
	if (UtilValidate.isNotEmpty(bundleCurrency)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", printZero), null, false) );
		printZero = profileConfiguration.getString("profileDescription");
	}
	result.put("printZero", printZero);
	
	String pricingFrequency = org.groupfio.homeapps.util.ProductUtil.getProductAttrValue(delegator, entry.getString("productId"), "CHARGE_CODE", "PRICE_FREQ");
	if (UtilValidate.isNotEmpty(pricingFrequency)) {
		profileConfiguration = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", pricingFrequency), null, false) );
		pricingFrequency = profileConfiguration.getString("profileDescription");
	}
	result.put("pricingFrequency", pricingFrequency);
	
	result.put("lastUpdatedStamp", UtilValidate.isNotEmpty(entry.get("lastUpdatedStamp")) ? UtilDateTime.timeStampToString(entry.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);