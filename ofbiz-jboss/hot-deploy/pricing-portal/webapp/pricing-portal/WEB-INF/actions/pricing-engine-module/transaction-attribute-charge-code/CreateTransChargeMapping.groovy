import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

transChargeMapping = new HashMap();
context.put("transChargeMapping", transChargeMapping);

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "SERVICE"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
serviceCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("serviceCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceCodeList, "productId", "productIdAlt"));

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("productTypeId", EntityOperator.EQUALS, "DIGITAL_GOOD"));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
chargeCodeList = delegator.findList("ProductSummary", mainConditons, null, UtilMisc.toList("productIdAlt"), null, false);
context.put("chargeCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeCodeList, "productId", "productIdAlt"));

transAttrLeftList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "1", "hide", "N", "attrType", "TA_CC_MAP"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrLeftList", transAttrLeftList);

transAttrRightList = delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("binaryFlag", "2", "hide", "N", "attrType", "TA_CC_MAP"), java.util.Arrays.asList("transactionAttributeId ASC"), false);
context.put("transAttrRightList", transAttrRightList);

/*
appStatusList = UtilMisc.toMap("ACTIVATED", uiLabelMap.get("activated"), "DEACTIVATED", uiLabelMap.get("deActivated"));
context.put("appStatusList", appStatusList);

serviceTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SERVICE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("serviceTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceTypeList, "profileConfigurationId", "profileDescription"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));
*/

