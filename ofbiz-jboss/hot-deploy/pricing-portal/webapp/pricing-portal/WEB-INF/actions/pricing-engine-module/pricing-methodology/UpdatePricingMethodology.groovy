import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

methodAttrLeftList = delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("binaryFlag", "1", "hide", "N"), java.util.Arrays.asList("pricingMethodAttributeId ASC"), false);
context.put("methodAttrLeftList", methodAttrLeftList);

methodAttrRightList = delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("binaryFlag", "2", "hide", "N"), java.util.Arrays.asList("pricingMethodAttributeId ASC"), false);
context.put("methodAttrRightList", methodAttrRightList);

pricingMethod = new HashMap();

pricingMethodId = request.getParameter("pricingMethodId");

pricingMethod.put("pricingMethodId", pricingMethodId);		

if (UtilValidate.isNotEmpty(pricingMethodId)) {

	methodAttrLeftList.each{transAttr ->
		String attrName = transAttr.getString("attrName");
		String attrValue = "";
		codeValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("pricingMethodId", pricingMethodId, "attrName", attrName), null, false) );
		if (UtilValidate.isNotEmpty(codeValue)) {
			attrValue = codeValue.getString("attrValue");
		}
		pricingMethod.put(attrName, attrValue);				
	}
	
	methodAttrRightList.each{transAttr ->
		String attrName = transAttr.getString("attrName");
		String attrValue = "";
		codeValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("pricingMethodId", pricingMethodId, "attrName", attrName), null, false) );
		if (UtilValidate.isNotEmpty(codeValue)) {
			attrValue = codeValue.getString("attrValue");
		}
		pricingMethod.put(attrName, attrValue);				
	}
	
}

context.put("pricingMethod", pricingMethod);
