import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

//String currentDate = UtilDateTime.timeStampToString(entry.getTimestamp("lastUpdatedStamp"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null)

chargeCode = new HashMap();
context.put("chargeCode", chargeCode);

chargeTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CHARGE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("chargeTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(chargeTypeList, "profileConfigurationId", "profileDescription"));

debitSystemTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "DEBIT_SYSTEM_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("debitSystemTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(debitSystemTypeList, "profileConfigurationId", "profileDescription"));

serviceCodeList = delegator.findByAnd("Product", UtilMisc.toMap("productTypeId", "SERVICE"), java.util.Arrays.asList("productIdAlt ASC"), false);
context.put("serviceCodeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(serviceCodeList, "productId", "internalName"));

currencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("currencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(currencyTypeList, "profileConfigurationId", "profileDescription"));

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));