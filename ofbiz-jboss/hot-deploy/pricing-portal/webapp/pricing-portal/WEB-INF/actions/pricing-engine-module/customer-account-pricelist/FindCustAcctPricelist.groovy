import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

filterCustAcctPricelist = new HashMap();
context.put("filterCustAcctPricelist", filterCustAcctPricelist);

searchConditionOpertorList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SRC_COND_OPR"), java.util.Arrays.asList("sequence ASC"), false);
context.put("searchConditionOpertorList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(searchConditionOpertorList, "profileConfigurationId", "profileDescription"));

discountRateTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "DCNT_RATE_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("discountRateTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(discountRateTypeList, "profileConfigurationId", "profileDescription"));

rateAppliedOnList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "RATE_APPLIED_ON"), java.util.Arrays.asList("sequence ASC"), false);
context.put("rateAppliedOnList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(rateAppliedOnList, "profileConfigurationId", "profileDescription"));
