import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

filterPricingMethodology = new HashMap();
context.put("filterPricingMethodology", filterPricingMethodology);

searchConditionOpertorList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "SRC_COND_OPR"), java.util.Arrays.asList("sequence ASC"), false);
context.put("searchConditionOpertorList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(searchConditionOpertorList, "profileConfigurationId", "profileDescription"));

methodAttrList = delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("isHideInList", "N"), java.util.Arrays.asList("sequence ASC"), false);
context.put("methodAttrList", methodAttrList);

context.put("searchConditionList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(methodAttrList, "attrName", "description"));
