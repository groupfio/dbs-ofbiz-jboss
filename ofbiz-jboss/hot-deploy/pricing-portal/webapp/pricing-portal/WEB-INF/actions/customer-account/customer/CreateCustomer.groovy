import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

customer = new HashMap();
context.put("customer", customer);

postalAddress = new HashMap();

context.put("postalAddress", postalAddress);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

billingFrequencyList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BILLING_FREQUENCY"), java.util.Arrays.asList("sequence ASC"), false);
context.put("billingFrequencyList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(billingFrequencyList, "profileConfigurationId", "profileDescription"));

giroConsFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "GIRO_CONS_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("giroConsFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(giroConsFreqList, "profileConfigurationId", "profileDescription"));

giroDbtFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "GIRO_DBT_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("giroDbtFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(giroDbtFreqList, "profileConfigurationId", "profileDescription"));

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("pricing-portal", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}
context.put("countryMap", countryMap);


