import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.party.party.PartyHelper;
import org.groupfio.pricing.portal.event.AjaxEvents;

import java.util.HashMap;
import java.util.List;

import org.ofbiz.entity.condition.EntityExpr;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.ofbiz.base.util.UtilDateTime;
import java.util.TimeZone;

import javolution.util.FastList;

searchPartyId = parameters.get("searchPartyId");
statusType = parameters.get("statusType");

String draw = request.getParameter("draw");
String start = request.getParameter("start");
String length = request.getParameter("length");

String sortDir = "desc";
String orderField = "";
String orderColumnId = request.getParameter("order[0][column]");
if(UtilValidate.isNotEmpty(orderColumnId)) {
	int sortColumnId = Integer.parseInt(orderColumnId);
	String sortColumnName = request.getParameter("columns["+sortColumnId+"][data]");
	sortDir = request.getParameter("order[0][dir]").toUpperCase();
	orderField = sortColumnName;
} else {
	orderField = "lastUpdated";
}

Debug.logInfo("orderField>>> "+orderField,"CustomerSummary");

delegator = request.getAttribute("delegator");

conditionsList = FastList.newInstance();

EntityCondition roleTypeCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"));
conditionsList.add(roleTypeCondition);

EntityCondition partyStatusCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)), EntityOperator.OR);

conditionsList.add(partyStatusCondition);
conditionsList.add(EntityUtil.getFilterByDateExpr());

fromDate = parameters.get("fromDate");
thruDate = parameters.get("thruDate");
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
if (UtilValidate.isNotEmpty(fromDate)) {
	fromdateVal=fromDate+" "+"00:00:00.0";
	Date parsedDateFrom = dateFormat.parse(fromdateVal);
	Timestamp fromTimestamp = new java.sql.Timestamp(parsedDateFrom.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.GREATER_THAN_EQUAL_TO , fromTimestamp ) ) );
}
if (UtilValidate.isNotEmpty(thruDate)) { 
	thruDateVal=thruDate+" "+"23:23:59.0";
	Date parsedDateThru = dateFormat.parse(thruDateVal);
	Timestamp thruDateTimestamp = new java.sql.Timestamp(parsedDateThru.getTime());	
	conditionsList.addAll( UtilMisc.toList( new EntityExpr( "createdStamp" , EntityOperator.NOT_EQUAL , null ) , new EntityExpr( "createdStamp" , EntityOperator.LESS_THAN_EQUAL_TO , thruDateTimestamp ) ) );
}

EntityCondition mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);

EntityFindOptions efo = new EntityFindOptions();
efo.setDistinct(true);

Debug.logInfo("mainConditons>>> "+mainConditons,"CustomerSummary");

Debug.logInfo("CustomerSummary start count>>> "+UtilDateTime.nowTimestamp(),"CustomerSummary");

long count = 0;
EntityFindOptions efoNum= new EntityFindOptions();
efoNum.setDistinct(true);
efoNum.getDistinct();
efoNum.setFetchSize(1000);

count = delegator.findCountByCondition("PartyCommonView", mainConditons, null, UtilMisc.toSet("partyId"), efoNum);

Debug.logInfo("CustomerSummary end count>>> "+UtilDateTime.nowTimestamp()+", count: "+count,"CustomerSummary");

int startInx = UtilValidate.isNotEmpty(start) ? Integer.parseInt(start) : 0;
int endInx = UtilValidate.isNotEmpty(length) ? Integer.parseInt(length) : 0
efo.setOffset(startInx);
efo.setLimit(endInx);

Debug.logInfo("CustomerSummary start list>>> "+UtilDateTime.nowTimestamp(),"CustomerSummary");
jobList = delegator.findList("PartyCommonView", mainConditons, null, UtilMisc.toList(orderField+ " " + sortDir), efo, false);
Debug.logInfo("jobList.size(): "+jobList.size(),"CustomerSummary");
Debug.logInfo("CustomerSummary end list>>> "+UtilDateTime.nowTimestamp(),"CustomerSummary");
int recordsFiltered = count;
int recordsTotal = count;

JSONObject grid = new JSONObject();

JSONArray results = new JSONArray();
jobList.each{job ->
	JSONObject result = new JSONObject();
	result.putAll(job);
	
	//result.put("lastUpdated", UtilValidate.isNotEmpty(job.get("lastUpdated")) ? UtilDateTime.timeStampToString(job.getTimestamp("lastUpdated"), "dd/MM/yyyy HH:mm:ss", TimeZone.getDefault(), null) : "");
	
	results.add(result);
}

grid.put("data", results);
grid.put("draw", draw);
grid.put("recordsTotal", recordsTotal);
grid.put("recordsFiltered", recordsFiltered);
	
return AjaxEvents.doJSONResponse(response, grid);