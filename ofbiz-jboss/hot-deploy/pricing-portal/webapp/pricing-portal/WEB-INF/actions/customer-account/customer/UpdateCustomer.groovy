import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

partyId = request.getParameter("partyId");

customer = new HashMap();

customer.put("partyId", partyId);

partySummary = EntityUtil.getFirst( delegator.findByAnd("PartyCommonView",UtilMisc.toMap("partyId", partyId), null, false) );
if (UtilValidate.isNotEmpty(partySummary)) {
	customer.put("customerId", partySummary.get("partyIdentificationIdValue"));
	customer.put("companyName", partySummary.get("groupName"));
	customer.put("emailAddress", partySummary.get("emailAddress"));
	customer.put("source", partySummary.get("source"));
	
	postalAddress = new HashMap();
	postalAddress.put("attnName", partySummary.get("attnTo"));
	postalAddress.put("address1", partySummary.get("address1"));
	postalAddress.put("address2", partySummary.get("address2"));
	postalAddress.put("address3", partySummary.get("address3"));
	postalAddress.put("address4", partySummary.get("address4"));
	postalAddress.put("address5", partySummary.get("address5"));
	postalAddress.put("city", partySummary.get("city"));
	postalAddress.put("stateProvinceGeoId", partySummary.get("stateProvinceGeoId"));
	postalAddress.put("countryGeoId", partySummary.get("countryGeoId"));
	postalAddress.put("postalCode", partySummary.get("postalCode"));
	context.put("postalAddress", postalAddress);
	
	customer.put("giroConsolidationFrequency", org.groupfio.homeapps.util.DataUtil.getPartyAttrValue(delegator, partyId, "GIRO_CONS_FREQ"));
	customer.put("giroDebitingFrequency", org.groupfio.homeapps.util.DataUtil.getPartyAttrValue(delegator, partyId, "GIRO_DBT_FREQ"));
	customer.put("billingFrequency", org.groupfio.homeapps.util.DataUtil.getPartyAttrValue(delegator, partyId, "BILLING_FREQUENCY"));
	customer.put("giroDebiting", org.groupfio.homeapps.util.DataUtil.getPartyAttrValue(delegator, partyId, "GIRO_DEBITING"));
	
}

context.put("customer", customer);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

billingFrequencyList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BILLING_FREQUENCY"), java.util.Arrays.asList("sequence ASC"), false);
context.put("billingFrequencyList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(billingFrequencyList, "profileConfigurationId", "profileDescription"));

giroConsFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "GIRO_CONS_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("giroConsFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(giroConsFreqList, "profileConfigurationId", "profileDescription"));

giroDbtFreqList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "GIRO_DBT_FREQ"), java.util.Arrays.asList("sequence ASC"), false);
context.put("giroDbtFreqList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(giroDbtFreqList, "profileConfigurationId", "profileDescription"));

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("pricing-portal", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}
context.put("countryMap", countryMap);


