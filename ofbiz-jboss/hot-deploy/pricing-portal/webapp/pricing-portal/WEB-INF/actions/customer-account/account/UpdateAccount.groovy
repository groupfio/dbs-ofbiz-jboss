import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;
import javolution.util.FastList;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

partyId = request.getParameter("partyId");
accountId = request.getParameter("accountId");

account = new HashMap();

account.put("partyId", partyId);
account.put("accountId", accountId);

accountAssoc = EntityUtil.getFirst( delegator.findByAnd("CustomerAccountAssoc",UtilMisc.toMap("custPartyId", partyId, "accountId", accountId), null, false) );
if (UtilValidate.isNotEmpty(accountAssoc)) {
	account.putAll(accountAssoc);
	
	postalAddress = new HashMap();
	if (UtilValidate.isNotEmpty(account.get("billingContactMechId"))) {
		postalAddress = EntityUtil.getFirst(delegator.findByAnd("PostalAddress", UtilMisc.toMap("contactMechId", account.get("billingContactMechId")), null, false));
	}
	context.put("postalAddress", postalAddress);
	
	account.put("openDate", UtilValidate.isNotEmpty(accountAssoc.get("openDate")) ? UtilDateTime.timeStampToString(accountAssoc.getTimestamp("openDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
	account.put("applicationDate", UtilValidate.isNotEmpty(accountAssoc.get("applicationDate")) ? UtilDateTime.timeStampToString(accountAssoc.getTimestamp("applicationDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
	account.put("bundleFromDate", UtilValidate.isNotEmpty(accountAssoc.get("bundleFromDate")) ? UtilDateTime.timeStampToString(accountAssoc.getTimestamp("bundleFromDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
	account.put("bundleToDate", UtilValidate.isNotEmpty(accountAssoc.get("bundleToDate")) ? UtilDateTime.timeStampToString(accountAssoc.getTimestamp("bundleToDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
	
	nominatedAccount = new HashMap();
	nominatedAccountAssoc = EntityUtil.getFirst(delegator.findByAnd("NominatedAccountAssoc", UtilMisc.toMap("partyId", partyId, "accountId", accountId), null, false));
	if (UtilValidate.isNotEmpty(nominatedAccountAssoc)) {
		nominatedAccount.putAll(nominatedAccountAssoc);
		nominatedAccount.put("fromDate", UtilValidate.isNotEmpty(nominatedAccountAssoc.get("fromDate")) ? UtilDateTime.timeStampToString(nominatedAccountAssoc.getTimestamp("fromDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
		nominatedAccount.put("thruDate", UtilValidate.isNotEmpty(nominatedAccountAssoc.get("thruDate")) ? UtilDateTime.timeStampToString(nominatedAccountAssoc.getTimestamp("thruDate"), "dd-MM-yyyy", TimeZone.getDefault(), null) : "");
	}
	context.put("nominatedAccount", nominatedAccount);
	
}

context.put("account", account);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

feeFloatBasedList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "FEE_FLOAT_BASED"), java.util.Arrays.asList("sequence ASC"), false);
context.put("feeFloatBasedList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(feeFloatBasedList, "profileConfigurationId", "profileDescription"));

accountTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "ACCT_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("accountTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(accountTypeList, "profileConfigurationId", "profileDescription"));

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("pricing-portal", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}
context.put("countryMap", countryMap);

customerList = org.groupfio.homeapps.util.DataHelper.getAccountList(delegator, userLogin);
context.put("customerList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(customerList, "partyId", "groupName"));

conditionsList = FastList.newInstance();
conditionsList.add(EntityCondition.makeCondition("custPartyId", EntityOperator.EQUALS, partyId));
conditionsList.add(EntityCondition.makeCondition("accountId", EntityOperator.NOT_EQUAL, accountId));
mainConditons = EntityCondition.makeCondition(conditionsList, EntityOperator.AND);
accountList = delegator.findList("CustomerAccountAssoc", mainConditons, null, UtilMisc.toList("accountId"), null, false);
context.put("accountList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(accountList, "accountId", "accountId", "accountName"));
