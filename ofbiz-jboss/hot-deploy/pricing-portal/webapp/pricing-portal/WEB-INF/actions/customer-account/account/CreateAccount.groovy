import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.*;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.base.util.UtilMisc;
import java.util.HashMap;

delegator = request.getAttribute("delegator");
uiLabelMap = UtilProperties.getResourceBundleMap("PricingPortalUiLabels", locale);

account = new HashMap();
context.put("account", account);

postalAddress = new HashMap();
context.put("postalAddress", postalAddress);

nominatedAccount = new HashMap();
context.put("nominatedAccount", nominatedAccount);

yesNoOptions = UtilMisc.toMap("Y", uiLabelMap.get("yes"), "N", uiLabelMap.get("no"));
context.put("yesNoOptions", yesNoOptions);

bundleCurrencyTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "BUNDLE_CURRENCY_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("bundleCurrencyTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(bundleCurrencyTypeList, "profileConfigurationId", "profileDescription"));

feeFloatBasedList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "FEE_FLOAT_BASED"), java.util.Arrays.asList("sequence ASC"), false);
context.put("feeFloatBasedList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(feeFloatBasedList, "profileConfigurationId", "profileDescription"));

accountTypeList = delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileTypeId", "ACCT_TYPE"), java.util.Arrays.asList("sequence ASC"), false);
context.put("accountTypeList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(accountTypeList, "profileConfigurationId", "profileDescription"));

countryMap = new LinkedHashMap();
List < String > countryGeoIdList = StringUtil.split(UtilProperties.getPropertyValue("pricing-portal", "country.geo.id.list"), ",");
if (countryGeoIdList != null && countryGeoIdList.size() > 0) {
	for (String countryGeoId: countryGeoIdList) {
		geo = EntityQuery.use(delegator).from("Geo")
			.where("geoId", countryGeoId, "geoTypeId", "COUNTRY").queryFirst();
		if (geo != null && geo.size() > 0) {
			countryMap.put(countryGeoId, geo.getString("geoName"));
		}
	}
}
context.put("countryMap", countryMap);

customerList = org.groupfio.homeapps.util.DataHelper.getAccountList(delegator, userLogin);
context.put("customerList", org.groupfio.homeapps.util.DataHelper.getDropDownOptions(customerList, "partyId", "groupName"));
	