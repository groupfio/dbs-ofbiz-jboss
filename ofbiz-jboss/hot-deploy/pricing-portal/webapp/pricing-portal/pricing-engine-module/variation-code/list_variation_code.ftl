<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.variationCode}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_variation_code">
	<thead>
	<tr>
		
		<#if transAttrList?has_content>
            <#list transAttrList as transAttr>
        		<th>
        		${uiLabelMap.get(transAttr.description)}
        		</th>
            </#list>
       	</#if>
		
		<th>Created Date</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		

$('#find-variationCode-button').on('click', function(){
	findVariationCodes();
});
			
findVariationCodes();
function findVariationCodes() {
	
	var searchCondition = $("#searchCondition").val();
	var searchConditionOpertor = $("#searchConditionOpertor").val();
	var searchValue = $("#searchValue").val();
	   	
   	var url = "searchVariationCodes?searchCondition="+searchCondition+"&searchConditionOpertor="+searchConditionOpertor+"&searchValue="+searchValue;
   
	$('#list_variation_code').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        /*
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
			*/
					      
	        "columns": [
					        	
	            //{ "data": "chargeCode" },
	            
	            <#if transAttrList?has_content>
                <#list transAttrList as transAttr>
                	{ "data": "${transAttr.attrName!}", 
                	  "orderable": false	
                	},
                </#list>
                </#if>
	            
	            { "data": "createdStamp" },
	            
	            { "data": "variationCodeId",
	              "orderable": false,
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="variationCodeUpdate?variationCodeId='+row.variationCodeId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
});	
	
</script>