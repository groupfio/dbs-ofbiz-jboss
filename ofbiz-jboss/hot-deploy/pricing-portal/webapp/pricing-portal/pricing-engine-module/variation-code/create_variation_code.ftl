<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.variationCodeCreate}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>variationCodeCreateAction</@ofbizUrl>" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<#if transAttrLeftList?has_content>
		
			<#list transAttrLeftList as transAttr>
			
				<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
					<#assign isRequired = true>
				<#else>
					<#assign isRequired = false>
				</#if>
				<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
				
				<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
					<@dateInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					disablePastDate=false
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
					
					<#assign ddOptions = Static["org.groupfio.homeapps.util.DataHelper"].getLovDropDownOptions(delegator, "TransactionAttributeLov", "transactionAttributeId", transAttr.transactionAttributeId, "attrLovName", "attrLovValue")/>
					
					<#if transAttr.attrName == "CHARGE_CODE">
					<#assign ddOptions = chargeCodeList/>
					</#if>
					
					<@dropdownInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					options=ddOptions
					required=isRequired
					allowEmpty=true
					/>	
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					inputType="number"
					/>
				<#else>
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				</#if>
				
			</#list>
		
			</#if>																																																														
																																																																																							
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
				
			<#if transAttrRightList?has_content>
		
			<#list transAttrRightList as transAttr>
			
				<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
					<#assign isRequired = true>
				<#else>
					<#assign isRequired = false>
				</#if>
				<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
				
				<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
					<@dateInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					disablePastDate=false
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
				
					<#assign ddOptions = Static["org.groupfio.homeapps.util.DataHelper"].getLovDropDownOptions(delegator, "TransactionAttributeLov", "transactionAttributeId", transAttr.transactionAttributeId, "attrLovName", "attrLovValue")/>
					
					<#if transAttr.attrName == "CHARGE_CODE">
					<#assign ddOptions = chargeCodeList/>
					</#if>
				
					<@dropdownInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					options=ddOptions
					required=isRequired
					allowEmpty=true
					/>	
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					inputType="number"
					/>
				<#else>
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				</#if>
			
			</#list>
		
			</#if>			 																																										
																															
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
