<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.pricingMethodology}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_variation_code">
	<thead>
	<tr>
		<th>${uiLabelMap.uniqueId!}</th>
		<#if methodAttrList?has_content>
            <#list methodAttrList as methodAttr>
        		<th>
        		${uiLabelMap.get(methodAttr.description)}
        		</th>
            </#list>
       	</#if>
		
		<th>Created Date</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		

$('#find-pricingMethodology-button').on('click', function(){
	findPricingMethodologys();
});
			
findPricingMethodologys();
function findPricingMethodologys() {
	
	var searchCondition = $("#searchCondition").val();
	var searchConditionOpertor = $("#searchConditionOpertor").val();
	var searchValue = $("#searchValue").val();
	   	
   	var url = "searchPricingMethodologys?searchCondition="+searchCondition+"&searchConditionOpertor="+searchConditionOpertor+"&searchValue="+searchValue;
   
	$('#list_variation_code').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        /*
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
			*/
					      
	        "columns": [
					        	
	            { "data": "pricingMethodId",
	              "orderable": false,
		          "render": function(data, type, row, meta){
		          	var data = row.pricingMethodId;
		            return data;
		          }
		         },
	            
	            <#if methodAttrList?has_content>
                <#list methodAttrList as methodAttr>
                	{ "data": "${methodAttr.attrName!}", 
                	  "orderable": false	
                	},
                </#list>
                </#if>
	            
	            { "data": "createdStamp" },
	            
	            { "data": "pricingMethodAttributeId",
	              "orderable": false,
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="pricingMethodologyUpdate?pricingMethodId='+row.pricingMethodId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
});	
	
</script>