<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.pricingMethodologys}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findPricingMethodologyForm" class="form-horizontal" name="findPricingMethodologyForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchCondition"
				options=searchConditionList
				required=false
				value=filterPricingMethodology.searchCondition
				allowEmpty=true
				emptyText = "---Select---"
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchConditionOpertor"
				options=searchConditionOpertorList
				required=false
				value=filterPricingMethodology.searchConditionOpertor
				allowEmpty=true
				emptyText = "---Select---"
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="searchValue"
				placeholder=uiLabelMap.searchValue
				value=filterPricingMethodology.searchValue
				required=false
				/>
         </div>
         
         <@fromSimpleAction id="find-pricingMethodology-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
