<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.pricingMethodologyCreate}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>pricingMethodologyCreateAction</@ofbizUrl>" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<#if methodAttrLeftList?has_content>
		
			<#list methodAttrLeftList as transAttr>
			
				<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
					<#assign isRequired = true>
				<#else>
					<#assign isRequired = false>
				</#if>
				<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
				
				<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
					<@dateInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					disablePastDate=false
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
					<#assign ddOptions = Static["org.groupfio.homeapps.util.DataHelper"].getLovDropDownOptions(delegator, "PricingMethodAttributeLov", "pricingMethodAttributeId", transAttr.pricingMethodAttributeId, "attrLovName", "attrLovValue")/>
					<@dropdownInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					options=ddOptions
					required=isRequired
					allowEmpty=true
					/>	
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					inputType="number"
					step=".0001"
					/>
				<#else>
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				</#if>
							
			</#list>
		
			</#if>																																																														
																																																																																							
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
				
			<#if methodAttrRightList?has_content>
		
			<#list methodAttrRightList as transAttr>
			
				<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
					<#assign isRequired = true>
				<#else>
					<#assign isRequired = false>
				</#if>
				<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
				
				<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
					<@dateInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					disablePastDate=false
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
					<#assign ddOptions = Static["org.groupfio.homeapps.util.DataHelper"].getLovDropDownOptions(delegator, "PricingMethodAttributeLov", "pricingMethodAttributeId", transAttr.pricingMethodAttributeId, "attrLovName", "attrLovValue")/>
					<@dropdownInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					options=ddOptions
					required=isRequired
					allowEmpty=true
					/>	
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					inputType="number"
					step=".0001"
					/>
				<#else>
					<@generalInput 
					id="${inputId}"
					label="${uiLabelMap.get(transAttr.description)}"
					placeholder="${uiLabelMap.get(transAttr.description)}"
					required=isRequired
					/>
				</#if>
			
			</#list>
		
			</#if>			 																																										
																															
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
