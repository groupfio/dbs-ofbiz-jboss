<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>
<#include "component://pricing-portal/webapp/pricing-portal/pricing-engine-module/modal.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.transChargeMappingUpdate!} - ${transChargeMapping.transactionProductAssocId!}</h1>
</div>

<form id="transChargeMappingUpdateForm" role="form" class="form-horizontal" action="<@ofbizUrl>transAttrChargeCodeUpdateAction</@ofbizUrl>" method="post" data-toggle="validator">

<input type="hidden" name="transactionProductAssocId" value="${transChargeMapping.transactionProductAssocId!}">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@dropdownInput 
				id="serviceCode"
				label=uiLabelMap.serviceCode
				options=serviceCodeList
				required=true
				value=transChargeMapping.serviceCode
				allowEmpty=true
				/>	
				
			<@dateInput 
				id="fromDate"
				label=uiLabelMap.CommonFromDate
				value=transChargeMapping.fromDate
				disablePastDate=false
				required=true
				/>
			<#-- 	
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=transChargeMapping.description
				required=false
				maxlength=100
				/>																																								
			 -->
			
		   	<div class="form-group row">
			  	<label class="col-sm-4 col-form-label">${uiLabelMap.description!} </label>
			   	<div class="col-sm-6">
			      <div class="input-icon ">
			         <input type="text" class="form-control input-sm  " autocomplete="off" value="${transChargeMapping.description!}" id="description" name="description" placeholder="${uiLabelMap.description!}" minlength="" maxlength="100">
			         <div class="help-block with-errors" id="description_error"></div>
			      </div>
			   	</div>
			  	<div class="col-sm-1">
					<button type="button" id="tacc-modal-btn" class="btn btn-xs btn-secondary tooltips mt-1" data-original-title="Details" data-toggle="modal" data-target="#prodAssocValueModalView"> <i class="fa fa-file-text-o" aria-hidden="true"></i></button>
				</div>
			</div>
																																																																																																
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@dropdownInput 
				id="chargeCode"
				label=uiLabelMap.chargeCode
				options=chargeCodeList
				required=false
				value=transChargeMapping.chargeCode
				allowEmpty=true
				/>			
				
			<@dateInput 
				id="thruDate"
				label=uiLabelMap.CommonThruDate
				value=transChargeMapping.thruDate
				disablePastDate=true
				/>																					
																															
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<#if transAttrLeftList?has_content>
<#list transAttrLeftList as transAttr>

<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
<input type="hidden" name="${inputId}" value="${transChargeMapping.get(transAttr.attrName)!}">

</#list>
</#if>	

<#if transAttrRightList?has_content>
<#list transAttrRightList as transAttr>

<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
<input type="hidden" name="${inputId}" value="${transChargeMapping.get(transAttr.attrName)!}">

</#list>
</#if>

<@fromCommonAction showCancelBtn=false showClearBtn=false/>

</form>

<script>

$("#tacc-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#prodAssocValueModalView').attr("data-parentForm", "transChargeMappingUpdateForm");    
        
    $('#prodAssocValueModalView').modal("show");
		
});

</script>
