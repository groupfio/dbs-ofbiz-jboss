<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.transactionAttribute}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_prod_hier_main">
	<thead>
	<tr>
		<th>${uiLabelMap.serviceCode!}</th>
		<th>${uiLabelMap.serviceType!}</th>
		<th>${uiLabelMap.fromDate!}</th>
		<th>${uiLabelMap.thruDate!}</th>
		<th>${uiLabelMap.chargeCode!}</th>
		<th>${uiLabelMap.description!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	/*
	$('#list-custom-field-group').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	*/

$('#find-transChargeMapping-button').on('click', function(){
	findTransChargeMappings();
});
			
findTransChargeMappings();
function findTransChargeMappings() {
	
	var serviceCode = $("#serviceCode").val();
	var serviceType = $("#serviceType").val();
	   	
   	var url = "searchTransChargeMappings?serviceCode="+serviceCode+"&serviceType="+serviceType;
   
	$('#list_prod_hier_main').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				},
				{
					"targets": 2,
					"orderable": false
				},
				{
					"targets": 3,
					"orderable": false
				},
				{
					"targets": 4,
					"orderable": false
				} 
			],
					      
	        "columns": [
				
				{ "data": "productId",
		          "render": function(data, type, row, meta){
		          	var data = row.serviceCode;
		            return data;
		          }
		         }, 
	            { "data": "serviceType" },
	            { "data": "fromDate" },
	            { "data": "fromDate" },
	            { "data": "productId",
		          "render": function(data, type, row, meta){
		          	var data = row.chargeCode;
		            return data;
		          }
		         }, 
	            
	            { "data": "description" },
	            
	            { "data": "transactionProductAssocId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="transAttrChargeCodeUpdate?transactionProductAssocId='+row.transactionProductAssocId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
});	
	
</script>