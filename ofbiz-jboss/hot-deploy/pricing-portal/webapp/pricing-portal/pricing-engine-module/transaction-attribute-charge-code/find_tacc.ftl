<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.transChargeMapping}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findTransChargeMappingForm" class="form-horizontal" name="findTransChargeMappingForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="serviceCode"
				placeholder=uiLabelMap.serviceCode
				value=filterProdHierMain.serviceCode
				required=false
				maxlength=60
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="serviceType"
				options=serviceTypeList
				required=false
				value=filterProdHierMain.serviceType
				allowEmpty=true
				emptyText = uiLabelMap.serviceType
				/>	
         </div>
         
         <@fromSimpleAction id="find-transChargeMapping-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
