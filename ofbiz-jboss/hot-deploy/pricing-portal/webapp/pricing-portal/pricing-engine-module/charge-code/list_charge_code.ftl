<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.chargeCode}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_charge_code">
	<thead>
	<tr>
		<th>${uiLabelMap.chargeCode!}</th>
		<th>${uiLabelMap.description!}</th>
		<th>${uiLabelMap.chargeType!}</th>
		<th>${uiLabelMap.serviceCode!}</th>
		<th>${uiLabelMap.printZero!}</th>
		<th>${uiLabelMap.pricingFrequency!}</th>
		<th>${uiLabelMap.variationCurrency!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	/*
	$('#list-custom-field-group').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	*/

$('#find-chargeCode-button').on('click', function(){
	findChargeCodes();
});
			
findChargeCodes();
function findChargeCodes() {
	
	var searchCondition = $("#searchCondition").val();
	var searchConditionOpertor = $("#searchConditionOpertor").val();
	var searchValue = $("#searchValue").val();
	   	
   	var url = "searchChargeCodes?searchCondition="+searchCondition+"&searchConditionOpertor="+searchConditionOpertor+"&searchValue="+searchValue;
   
	$('#list_charge_code').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				},
				{
					"targets": 2,
					"orderable": false
				},
				{
					"targets": 3,
					"orderable": false
				},
				{
					"targets": 4,
					"orderable": false
				},
				{
					"targets": 5,
					"orderable": false
				},
				{
					"targets": 6,
					"orderable": false
				},
				{
					"targets": 7,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "chargeCode" },
	            { "data": "description" },
	            { "data": "chargeType" },
	            { "data": "serviceCode" },
	            { "data": "printZero" },
	            { "data": "pricingFrequency" },
	            { "data": "bundleCurrency" },
	            
	            { "data": "productId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="chargeCodeUpdate?productId='+row.productId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
});	
	
</script>