<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.chargeCodeCreate}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>chargeCodeCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="chargeCode"
				label=uiLabelMap.chargeCode
				placeholder=uiLabelMap.chargeCode
				value=chargeCode.chargeCode
				required=true
				maxlength=60
				/>	
				
			<@dropdownInput 
				id="chargeType"
				label=uiLabelMap.chargeType
				options=chargeTypeList
				required=true
				value=chargeCode.chargeType
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="debitSystemType"
				label=uiLabelMap.debitSystemType
				options=debitSystemTypeList
				required=false
				value=chargeCode.debitSystemType
				allowEmpty=true
				/>																																								
																																	
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
				
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=chargeCode.description
				required=true
				maxlength=100
				/>
				
			<@dropdownInput 
				id="serviceCode"
				label=uiLabelMap.serviceCode
				options=serviceCodeList
				required=true
				value=chargeCode.serviceCode
				allowEmpty=true
				/>																						
																															
			</div>
							
		</div>
						
	</div>
	<#-- 
	<div class="col-sm-12">
	  	<div class="text-right">
	  		<button type="reset" class="btn btn-xs btn-success mt-1"> <i class="fa fa-plus" aria-hidden="true" title="Add"></i></button>
	  	</div>
  	</div>
	 -->
	 		
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
