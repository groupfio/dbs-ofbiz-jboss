<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.chargeCodes}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findChargeCodeForm" class="form-horizontal" name="findChargeCodeForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchCondition"
				options=searchConditionList
				required=false
				value=filterChargeCode.searchCondition
				allowEmpty=true
				emptyText = "---Select---"
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchConditionOpertor"
				options=searchConditionOpertorList
				required=false
				value=filterChargeCode.searchConditionOpertor
				allowEmpty=false
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="searchValue"
				placeholder=uiLabelMap.searchValue
				value=filterChargeCode.searchValue
				required=false
				/>
         </div>
         
         <@fromSimpleAction id="find-chargeCode-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
