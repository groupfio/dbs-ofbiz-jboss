<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>
<#include "component://pricing-portal/webapp/pricing-portal/pricing-engine-module/modal.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.chargeCodeUpdate} - ${chargeCode.productId!}</h1>
</div>

<form id="chargeCodeUpdateForm" role="form" class="form-horizontal" action="<@ofbizUrl>chargeCodeUpdateAction</@ofbizUrl>" method="post" data-toggle="validator">

<input type="hidden" name="productId" value="${chargeCode.productId!}">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="chargeCode"
				label=uiLabelMap.chargeCode
				placeholder=uiLabelMap.chargeCode
				value=chargeCode.chargeCode
				required=true
				maxlength=60
				/>	
				
			<@dropdownInput 
				id="chargeType"
				label=uiLabelMap.chargeType
				options=chargeTypeList
				required=true
				value=chargeCode.chargeType
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="debitSystemType"
				label=uiLabelMap.debitSystemType
				options=debitSystemTypeList
				required=false
				value=chargeCode.debitSystemType
				allowEmpty=true
				/>																																																												
																																																																																							
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
				
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=chargeCode.description
				required=true
				maxlength=100
				/>
				
			<@dropdownInput 
				id="serviceCode"
				label=uiLabelMap.serviceCode
				options=serviceCodeList
				required=true
				value=chargeCode.serviceCode
				allowEmpty=true
				/>																																													
																															
			</div>
							
		</div>
						
	</div>
			
</div>

<div class="text-right">
	<button id="ccd-modal-btn" type="button" class="btn btn-xs btn-secondary" data-toggle="modal" data-target="#chargeCodeDetailModalView"> <i class="fa fa-file-text-o" aria-hidden="true"></i></button> 
	<button id="gl-config-modal-btn" type="button" data-toggle="modal" data-target="#glConfigModalView" class="btn btn-xs btn-secondary ">GL Configuration</button>
</div>	
			
</div>

<input type="hidden" name="invoiceRequired" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="priceFreq" value="">
<input type="hidden" name="pricingDay" value="">
<input type="hidden" name="billAddressLevel" value="">
<input type="hidden" name="invFreq" value="">
<input type="hidden" name="invDay" value="">
<input type="hidden" name="currencyType" value="">
<input type="hidden" name="bundleCurrency" value="">

<input type="hidden" name="glAccountId" value="">
<input type="hidden" name="glCurrency" value="">
<input type="hidden" name="pcCode" value="">

<@fromCommonAction showCancelBtn=false showClearBtn=false/>

</form>

<script>

$("#ccd-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#chargeCodeDetailModalView').attr("data-parentForm", "chargeCodeUpdateForm");    
        
    $('#chargeCodeDetailModalView').modal("show");
		
});

$("#gl-config-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#glConfigModalView').attr("data-parentForm", "chargeCodeUpdateForm");    
        
    $('#glConfigModalView').modal("show");
		
});

</script>
