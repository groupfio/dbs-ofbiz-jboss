<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.prodHierMain}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_prod_hier_main">
	<thead>
	<tr>
		<th>${uiLabelMap.serviceCode!}</th>
		<th>${uiLabelMap.serviceType!}</th>
		<th>${uiLabelMap.currencyType!}</th>
		<th>${uiLabelMap.bundleCurrency!}</th>
		<th>${uiLabelMap.description!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	
$('#find-prodHierMain-button').on('click', function(){
	findProductHierarchyMaintenances();
});
	
});	

findProductHierarchyMaintenances();
function findProductHierarchyMaintenances() {
	
	var serviceCode = $('#findProdHierMainForm input[name="serviceCode"]').val();
	var serviceType = $("#serviceType").val();
	var currencyType = $("#currencyType").val();
	var bundleCurrency = $("#bundleCurrency").val();
	   	
   	var url = "searchProductHierarchyMaintenances?serviceCode="+serviceCode+"&serviceType="+serviceType+"&currencyType="+currencyType+"&bundleCurrency="+bundleCurrency;
   
	$('#list_prod_hier_main').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				},
				{
					"targets": 2,
					"orderable": false
				},
				{
					"targets": 3,
					"orderable": false
				},
				{
					"targets": 4,
					"orderable": false
				},
				{
					"targets": 5,
					"orderable": false
				} 
			],
					      
	        "columns": [
					        	
	            { "data": "serviceCode" },
	            { "data": "serviceType" },
	            { "data": "currencyType" },
	            { "data": "bundleCurrency" },
	            
	            { "data": "description" },
	            
	            { "data": "productId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="prodHierMainUpdate?productId='+row.productId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
</script>