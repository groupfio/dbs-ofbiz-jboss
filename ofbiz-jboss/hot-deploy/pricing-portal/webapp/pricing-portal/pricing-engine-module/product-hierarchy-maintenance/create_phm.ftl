<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.prodHierMainCreate}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>prodHierMainCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="serviceCode"
				label=uiLabelMap.serviceCode
				placeholder=uiLabelMap.serviceCode
				value=prodHierMain.serviceCode
				required=true
				maxlength=60
				/>	
				
			<@dropdownInput 
				id="serviceType"
				label=uiLabelMap.serviceType
				options=serviceTypeList
				required=true
				value=prodHierMain.serviceType
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="currencyType"
				label=uiLabelMap.currencyType
				options=currencyTypeList
				required=false
				value=prodHierMain.currencyType
				allowEmpty=true
				/>																																								
																																	
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
				
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=prodHierMain.description
				required=true
				maxlength=100
				/>
				
			<@dropdownInput 
				id="bundleCurrency"
				label=uiLabelMap.bundleCurrency
				options=bundleCurrencyTypeList
				required=false
				value=prodHierMain.bundleCurrency
				allowEmpty=true
				/>																						
																															
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
