<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.custAcctPricelist}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_custAcctPricelist">
	<thead>
	<tr>
		<th>${uiLabelMap.personalised!}</th>
		<th>${uiLabelMap.customer!}</th>
		<th>${uiLabelMap.account!}</th>
		<th>${uiLabelMap.priceListId!}</th>
		<th>${uiLabelMap.chargeCode!}</th>
		<th>${uiLabelMap.description!}</th>
		<th>${uiLabelMap.tierFrom!}</th>
		<th>${uiLabelMap.tierTo!}</th>
		<th>${uiLabelMap.rate!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<div id="pricelistValueModalView" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Pricelist Maintenance</h4>
				<button type="reset" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				<div class="col-12 bg-light border-bottom mt-2 mb-2">
					<strong>Rates</strong>
				</div>
				<div class="clearfix"></div>
				
				<form id="pricelistValueModalForm" role="form" method="post" data-toggle="validator">
				
				<input type="hidden" name="productPriceListId" value="">
				<input type="hidden" name="custPartyId" value="">
				<input type="hidden" name="accountId" value="">
				
				<div class="table-responsive">
					
					<table class="table table-striped">
						<thead>
							<tr>
								<th>${uiLabelMap.tierFrom!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.tierTo!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.minimumCharge!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.maximumCharge!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.rate!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.appliedOn!}</th>
								<th>${uiLabelMap.discountRateType!}</th>
								<th>${uiLabelMap.discountRate!}</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="pricelist-rates-content">
							
						</tbody>
					</table>
					
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="rate-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	
$('#find-custAcctPricelist-button').on('click', function(){
	findCustAcctPricelist();
});

$('#pricelistValueModalForm').validator().on('submit', function (e) {
		
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
  		
  		$('#pricelistValueModalForm input[name="productPriceListId"]').val( productPriceListId );
  		$('#pricelistValueModalForm input[name="custPartyId"]').val( custPartyId );
  		$('#pricelistValueModalForm input[name="accountId"]').val( accountId );
  		
  		$.post('applyCustAccountPricelistRateConfig', $('#pricelistValueModalForm').serialize(), function(returnedData) {
	
			if (returnedData.code == 200) {
				showAlert ("success", returnedData.message)
				findCustAcctPricelist();
			} else {
				showAlert ("error", returnedData.message)
			}
				
			$('#pricelistValueModalView').modal("hide");	
											
		});
		
    	// everything looks good!
  	}
});	

$('#pricelistValueModalView').on('shown.bs.modal', function (e) {
	
	loadPricelistRates(productPriceListId, accountId);
	
});
			
});	

var pricingMethodId;
var productPriceListId;
var accountId;
var custPartyId;

findCustAcctPricelist();
function findCustAcctPricelist() {
	
	var searchCondition = $("#searchCondition").val();
	var searchConditionOpertor = $("#searchConditionOpertor").val();
	var searchValue = $("#searchValue").val();
	   	
   	var url = "searchCustAcctPricelist?searchCondition="+searchCondition+"&searchConditionOpertor="+searchConditionOpertor+"&searchValue="+searchValue;
   
	$('#list_custAcctPricelist').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "priceRuleListTypeId",
		          "render": function(data, type, row, meta){
		          	var data = row.priceRuleListTypeDesc;
		            return data;
		          }
		         },
		         { "data": "custPartyId",
		          "render": function(data, type, row, meta){
		          	var data = row.customerNameDesc;
		            return data;
		          }
		         },
		         { "data": "accountId",
		          "render": function(data, type, row, meta){
		          	var data = row.accountNameDesc;
		            return data;
		          }
		         },
		         { "data": "productPriceListId",
		          "render": function(data, type, row, meta){
		          	var data = row.productPriceListId;
		            return data;
		          }
		         },
		         { "data": "chargeCode",
		          "render": function(data, type, row, meta){
		          	var data = row.chargeCode;
		            return data;
		          }
		         },
		         { "data": "description",
		          "render": function(data, type, row, meta){
		          	var data = row.description;
		            return data;
		          }
		         },
		         { "data": "tierFrom",
		          "render": function(data, type, row, meta){
		          	var data = row.tierFrom;
		            return data;
		          }
		         },
		         { "data": "tierTo",
		          "render": function(data, type, row, meta){
		          	var data = row.tierTo;
		            return data;
		          }
		         },
		         { "data": "tierRate",
		          "render": function(data, type, row, meta){
		          	var data = row.tierRate;
		            return data;
		          }
		         },
	            
	            { "data": "productPriceListId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<button data-custPartyId="'+row.custPartyId+'" data-accountId="'+row.accountId+'" data-productPriceListId="'+row.productPriceListId+'" class="btn btn-xs btn-secondary tooltips configure-pricelist-value" data-original-title="Detail" ><i class="fa fa-pencil-square-o"></i></button>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },   
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	      		resetValueConfigEvents()
	    	}
		});
}	

function resetValueConfigEvents() {
   
    $('.configure-pricelist-value').unbind( "click" );
    $('.configure-pricelist-value').bind( "click", function( event ) {
    
    	pricingMethodId = $(this).attr("data-pricingMethodId");
    	productPriceListId = $(this).attr("data-productPriceListId");
    	accountId = $(this).attr("data-accountId");
    	custPartyId = $(this).attr("data-custPartyId");
		
		$('#pricelistValueModalView').modal("show");
    
    });
    
}

function loadPricelistRates(productPriceListId, accountId) {
	var pricelistRateContent = '';
	var pricelistRateList;
	$.ajax({
			      
		type: "POST",
     	url: "getPricelistRates",
        data:  {"productPriceListId": productPriceListId, "accountId": accountId, "custPartyId": custPartyId},
        async: false,
        success: function (data) {   
            methodAttrHtml = '';
            if (data.code == 200) {
            	pricelistRateList = data.pricelistRateList;
            	if (data.pricelistRateList.length > 0) {
            		for (var i = 0; i < data.pricelistRateList.length; i++) {
	            		var pricelistRate = data.pricelistRateList[i];
	            		pricelistRateContent += prepareRateContent(pricelistRate, i);
						$("#pricelist-rates-content").html( pricelistRateContent );
	            	}
            	} else {
            		pricelistRateContent = prepareRateContent([], 0);
            		$("#pricelist-rates-content").html( pricelistRateContent );
            	}
            }
        }
	}); 
	
	if (pricelistRateList) {
		if (pricelistRateList.length > 0) {
    		for (var i = 0; i < pricelistRateList.length; i++) {
        		var pricelistRate = pricelistRateList[i];
        		$('#rateAppliedOn'+i+'').val(pricelistRate.appliedOn);
				$('#discountRateType'+i+'').val(pricelistRate.discountType);
        	}
        }
	}	
	
	$('.ui.dropdown').dropdown({
		useLabels: false
	});
	$('#pricelistValueModalForm').validator('update');	
}

function prepareRateContent(pricelistRate, index) {
	
	var actionContent = '<td><span class="input-group-addon">';
	if (index == 0) {
		actionContent += '<a onclick="addRateRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle text-dark"></i></a>';
	} else {
		actionContent += '<a class="plus-icon01 rd ml-1" onclick="removeRateRepeateContent(this)"><i class="fa fa-minus-circle text-dark" aria-hidden="true"></i></a><a onclick="addRateRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>';
	}
	actionContent += '</span></td>';
	
	var content = '<tr class="rate-content">'+
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierFrom+'" id="tierFrom'+index+'" name="tierFrom" placeholder="Tier From" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierFrom_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierTo+'" id="tierTo'+index+'" name="tierTo" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierTo_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.minimumCharge+'" id="minimumCharge'+index+'" name="minimumCharge" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="minimumCharge_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.maximumCharge+'" id="maximumCharge'+index+'" name="maximumCharge" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="maximumCharge_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierRate+'" id="tierRate'+index+'" name="tierRate" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierRate_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group  mr  rateAppliedOn ">'+
					  	'<select class="ui dropdown search form-control " id="rateAppliedOn'+index+'" name="rateAppliedOn" onchange="">'+
					  	'<option value="">Please Select</option>'+
					  	
					  	<#if rateAppliedOnList?has_content>
					  	<#list rateAppliedOnList.entrySet() as entry>  
					  	'<option value="${entry.key}">${entry.value!}</option>'+
					  	</#list>
					  	</#if>
					  	
					  	'</select>'+
					  	'<div class="form-control-focus">'+
					  	'</div>'+
					  	'<span class="help-block with-errors"></span>'+
					'</div>'+
					'</td>'+	
					
					'<td>'+
					'<div class="form-group  mr  discountRateType ">'+
					  	'<select class="ui dropdown search form-control " id="discountRateType'+index+'" name="discountRateType" onchange="">'+
					  	'<option value="">Please Select</option>'+
					  	
					  	<#if discountRateTypeList?has_content>
					  	<#list discountRateTypeList.entrySet() as entry>  
					  	'<option value="${entry.key}">${entry.value!}</option>'+
					  	</#list>
					  	</#if>
					  	
					  	'</select>'+
					  	'<div class="form-control-focus">'+
					  	'</div>'+
					  	'<span class="help-block with-errors"></span>'+
					'</div>'+
					'</td>'+	
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.discountRate+'" id="discountRate'+index+'" name="discountRate" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="discountRate_error"></div>'+
					'</div>'+
					'</td>'+	
					
					actionContent+																																																																																																																
					
				'</tr>'
				;
	
	return content;
}
	
</script>