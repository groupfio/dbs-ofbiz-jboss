<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.custAcctPricelistView}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findCustAcctPricelistForm" class="form-horizontal" name="findCustAcctPricelistForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchCondition"
				options=searchConditionList
				required=false
				value=filterCustAcctPricelist.searchCondition
				allowEmpty=true
				emptyText = "---Select---"
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="searchConditionOpertor"
				options=searchConditionOpertorList
				required=false
				value=filterCustAcctPricelist.searchConditionOpertor
				allowEmpty=true
				emptyText = "---Select---"
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="searchValue"
				placeholder=uiLabelMap.searchValue
				value=filterCustAcctPricelist.searchValue
				required=false
				/>
         </div>
         
         <@fromSimpleAction id="find-custAcctPricelist-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
