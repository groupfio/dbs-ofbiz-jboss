<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.pricelistMaintenance!}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>pricelistMaintenanceCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<#-- <th></th> -->
					<th>${uiLabelMap.chargeCode!} <span class="text-danger">*</span> <i
						class="fa fa-building" aria-hidden="true" data-toggle="modal"
						data-target="#CC"></i></th>
					<#-- <th>${uiLabelMap.description!} <span class="text-danger">*</span></th> -->
					<th>${uiLabelMap.variationCode!} <i class="fa fa-building" aria-hidden="true"
						data-toggle="modal" data-target="#VC"></i></th>
					<th>${uiLabelMap.bundle!} <i class="fa fa-building" aria-hidden="true"
						data-toggle="modal" data-target="#BU"></i></th>
					<th>${uiLabelMap.pricingTemplate!} <i class="fa fa-building"
						aria-hidden="true" data-toggle="modal" data-target="#PM1"></i></th>
					<th>${uiLabelMap.variationCurrency!} <i class="fa fa-building"
						aria-hidden="true" data-toggle="modal" data-target="#CR"></i></th>
					<th>${uiLabelMap.fromDate!} <span class="text-danger">*</span></th>
					<th>${uiLabelMap.thruDate!}</th>
					<#-- <th></th> -->
				</tr>
			</thead>
			<tbody>
				<tr>
					<#-- <td>
						<div class="checkbox ">
							<label><input type="checkbox" value=""></label>
						</div>
					</td> -->
					<td>
						<@simpleDropdownInput 
						id="chargeCode"
						options=chargeCodeList
						required=true
						value=pricelistMain.chargeCode
						allowEmpty=true
						/>
					</td>
					<#-- <td>
						<@simpleInput 
						id="description"
						placeholder=uiLabelMap.description
						value=pricelistMain.description
						required=true
						/>
					</td> -->
					<td>
						<@simpleDropdownInput 
						id="variationCode"
						options=variationCodeList
						required=false
						value=pricelistMain.variationCode
						allowEmpty=true
						/>	
					</td>
					<td>
						<@simpleDropdownInput 
						id="bundleId"
						options=bundleList
						required=false
						value=pricelistMain.bundle
						allowEmpty=true
						/>	
					</td>
					<td>
						<@simpleDropdownInput 
						id="pricingMethodId"
						options=pricingTemplateList
						required=false
						value=pricelistMain.pricingTemplate
						allowEmpty=true
						/>	
					</td>
					<td>
						<@simpleDropdownInput 
						id="variationCurrency"
						options=bundleCurrencyTypeList
						required=false
						value=pricelistMain.bundleCurrencyType
						allowEmpty=true
						/>	
					</td>
					<td>
						<@simpleDateInput 
						id="fromDate"
						value=pricelistMain.fromDate
						disablePastDate=false
						required=true
						/>
					</td>
					<td>
						<@simpleDateInput 
						id="thruDate"
						value=pricelistMain.thruDate
						disablePastDate=true
						required=false
						/>
					</td>
					<#-- <td>
						<button type="button" class="btn btn-xs btn-secondary"
							data-toggle="modal" data-target="#PM1">
							<i class="fa fa-file-text-o" aria-hidden="true" title="Details"></i>
						</button>
					</td> -->
				</tr>
				
			</tbody>
		</table>
	</div>
			
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
