<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.pricelist}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_pricelist">
	<thead>
	<tr>
		<th>${uiLabelMap.chargeCode!}</th>
		<#-- <th>${uiLabelMap.description!}</th> -->
		<th>${uiLabelMap.variationCode!}</th>
		<th>${uiLabelMap.bundle!}</th>
		<th>${uiLabelMap.pricingTemplate!}</th>
		<th>${uiLabelMap.variationCurrency!}</th>
		<th>${uiLabelMap.fromDate!}</th>
		<th>${uiLabelMap.thruDate!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<div id="pricelistValueModalView" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Pricelist Maintenance</h4>
				<button type="reset" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="col-12 bg-light border-bottom mt-2 mb-2">
					<strong>Product Definition</strong>
				</div>
				<div class="row padding-r">
					<div class="col-md-6 col-sm-6 form-horizontal">
						<div class="form-group row">
							<label class="col-sm-5 col-form-label">Status </label>
							<div class="col-sm-7">
								<label class="col-form-label input-sm fw">A</label>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 form-horizontal">
						<div class="form-group row">
							<label class="col-sm-5 col-form-label">Description <span
								class="text-danger">*</span></label>
							<div class="col-sm-7">
								<label class="col-form-label input-sm fw"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-12 bg-light border-bottom mt-2 mb-2">
					<strong>Pricing Methodology</strong>
				</div>
				
				<div class="row padding-r">
					<div id="pricing-method-left-columns" class="col-md-6 col-sm-6 form-horizontal">
						
					</div>
					<div id="pricing-method-right-columns" class="col-md-6 col-sm-6 form-horizontal">
						
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-12 bg-light border-bottom mt-2 mb-2">
					<strong>Rates</strong>
				</div>
				<div class="clearfix"></div>
				
				<form id="pricelistValueModalForm" role="form" method="post" data-toggle="validator">
				
				<input type="hidden" name="productPriceListId" value="">
				
				<div class="table-responsive">
					
					<table class="table table-striped">
						<thead>
							<tr>
								<th>${uiLabelMap.tierFrom!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.tierTo!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.minimumCharge!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.maximumCharge!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.rate!} <span class="text-danger">*</span></th>
								<th>${uiLabelMap.appliedOn!}</th>
								<th>${uiLabelMap.discountRateType!}</th>
								<th>${uiLabelMap.discountRate!}</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="pricelist-rates-content">
							
							<#-- 
							<tr class="rate-content">
								<td>
									<@simpleInput 
									id="tierFrom"
									placeholder=uiLabelMap.tierFrom
									required=true
									inputType="number"
									step=".0001"
									/>
								</td>
								<td>
									<@simpleInput 
									id="tierTo"
									placeholder=uiLabelMap.tierTo
									required=true
									inputType="number"
									step=".0001"
									/>
								</td>
								<td>
									<@simpleInput 
									id="minimumCharge"
									placeholder=uiLabelMap.minimumCharge
									required=true
									inputType="number"
									step=".0001"
									/>
								</td>
								<td>
									<@simpleInput 
									id="maximumCharge"
									placeholder=uiLabelMap.maximumCharge
									required=true
									inputType="number"
									step=".0001"
									/>
								</td>
								<td>
									<@simpleInput 
									id="tierRate"
									placeholder=uiLabelMap.rate
									required=true
									inputType="number"
									step=".0001"
									/>
								</td>
				
								<td>
									<@simpleDropdownInput 
									id="rateAppliedOn"
									options=rateAppliedOnList
									required=false
									allowEmpty=true
									/>
								</td>
								<td>
									<@simpleDropdownInput 
									id="discountRateType"
									options=discountRateTypeList
									required=false
									allowEmpty=true
									/>
								</td>
								<td>
									<@simpleInput 
									id="discountRate"
									placeholder=uiLabelMap.discountRate
									required=false
									inputType="number"
									step=".0001"
									/>
								</td>
								<td>
									<span class="input-group-addon"> 
										<a onclick="addRateRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>
									</span>
								</td>
							</tr>
							 -->
							 
						</tbody>
					</table>
					<#-- 
					<button onclick="addRateRepeateContent(this)" type="button" class="btn btn-xs btn-secondary mt">Add</button>
					<button type="button" class="btn btn-xs btn-secondary mt">Delete</button>
					 -->
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="rate-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

var pricingMethodId;
var productPriceListId;

jQuery(document).ready(function() {	

$('#pricelistValueModalForm').validator().on('submit', function (e) {
		
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
  		
  		$('#pricelistValueModalForm input[name="productPriceListId"]').val( productPriceListId );
  		
  		$.post('applyPricelistRateConfig', $('#pricelistValueModalForm').serialize(), function(returnedData) {
	
			if (returnedData.code == 200) {
				showAlert ("success", returnedData.message)
			} else {
				showAlert ("error", returnedData.message)
			}
				
			$('#pricelistValueModalView').modal("hide");									
		});
		
    	// everything looks good!
  	}
});	

$('#find-pricelist-button').on('click', function(){
	findPricelistMaintenances();
});

$('#pricelistValueModalView').on('shown.bs.modal', function (e) {
	
	var methodAttrHtml = '';
	$.ajax({
			      
		type: "POST",
     	url: "getPricingMethodAttributes",
        data:  {"pricingMethodId": pricingMethodId, "binaryFlag": "1"},
        async: false,
        success: function (data) {   
            methodAttrHtml = '';
            if (data.code == 200) {
            	
            	for (var i = 0; i < data.methodAttrList.length; i++) {
            		var methodAttr = data.methodAttrList[i];
            		methodAttrHtml += '<div class="form-group row">'+
							'<label class="col-sm-5 col-form-label">'+methodAttr.description+' '+
								//'<span class="text-danger">*</span>'+
							'</label>'+
							'<div class="col-sm-7">'+
								'<label class="col-form-label input-sm fw">'+methodAttr.attrValueDesc+'</label>'+
							'</div>'+
						'</div>';
            	}
            	
            }
			    	
        }
        
	}); 
	$("#pricing-method-left-columns").html( methodAttrHtml );
	
	$.ajax({
			      
		type: "POST",
     	url: "getPricingMethodAttributes",
        data:  {"pricingMethodId": pricingMethodId, "binaryFlag": "2"},
        async: false,
        success: function (data) {   
            methodAttrHtml = '';
            if (data.code == 200) {
            	
            	for (var i = 0; i < data.methodAttrList.length; i++) {
            		var methodAttr = data.methodAttrList[i];
            		methodAttrHtml += '<div class="form-group row">'+
							'<label class="col-sm-5 col-form-label">'+methodAttr.description+' '+
								//'<span class="text-danger">*</span>'+
							'</label>'+
							'<div class="col-sm-7">'+
								'<label class="col-form-label input-sm fw">'+methodAttr.attrValueDesc+'</label>'+
							'</div>'+
						'</div>';
            	}
            	
            }
			    	
        }
        
	}); 
	$("#pricing-method-right-columns").html( methodAttrHtml );
	
	loadPricelistRates(productPriceListId);
	
});
	
});

function addRateRepeateContent (actionButton) {

	var cloneHtml = $(actionButton).closest( ".rate-content" ).clone();
	
	if ( $(actionButton).parent().children().find('[class=\"fa fa-minus-circle text-dark\"]').length == 0 ) {
        cloneHtml.children().find('[class=\"fa fa-plus-circle text-dark\"]').parent().before('<a class="plus-icon01 rd ml-1" onclick="removeRateRepeateContent(this)"><i class="fa fa-minus-circle text-dark" aria-hidden="true"></i></a>');
    }
	cloneHtml.children().find('.form-control').val("");
	$(actionButton).closest( ".rate-content" ).after(cloneHtml);
	
	$('.ui.dropdown').dropdown({
		useLabels: false
	});
	
	$('#pricelistValueModalForm').validator('update');
	
}	

function removeRateRepeateContent (actionButton) {
	$(actionButton).closest( ".rate-content" ).remove();
}

function loadPricelistRates(productPriceListId) {
	var pricelistRateContent = '';
	var pricelistRateList;
	$.ajax({
			      
		type: "POST",
     	url: "getPricelistRates",
        data:  {"productPriceListId": productPriceListId},
        async: false,
        success: function (data) {   
            methodAttrHtml = '';
            if (data.code == 200) {
            	pricelistRateList = data.pricelistRateList;
            	if (data.pricelistRateList.length > 0) {
            		for (var i = 0; i < data.pricelistRateList.length; i++) {
	            		var pricelistRate = data.pricelistRateList[i];
	            		pricelistRateContent += prepareRateContent(pricelistRate, i);
						$("#pricelist-rates-content").html( pricelistRateContent );
	            	}
            	} else {
            		pricelistRateContent = prepareRateContent([], 0);
            		$("#pricelist-rates-content").html( pricelistRateContent );
            	}
            }
        }
	}); 
	
	if (pricelistRateList) {
		if (pricelistRateList.length > 0) {
    		for (var i = 0; i < pricelistRateList.length; i++) {
        		var pricelistRate = pricelistRateList[i];
        		$('#rateAppliedOn'+i+'').val(pricelistRate.appliedOn);
				$('#discountRateType'+i+'').val(pricelistRate.discountType);
        	}
        }
	}	
	
	$('.ui.dropdown').dropdown({
		useLabels: false
	});
	$('#pricelistValueModalForm').validator('update');	
}

function prepareRateContent(pricelistRate, index) {
	
	var actionContent = '<td><span class="input-group-addon">';
	if (index == 0) {
		actionContent += '<a onclick="addRateRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle text-dark"></i></a>';
	} else {
		actionContent += '<a class="plus-icon01 rd ml-1" onclick="removeRateRepeateContent(this)"><i class="fa fa-minus-circle text-dark" aria-hidden="true"></i></a><a onclick="addRateRepeateContent(this)" class="gd ml-1"><i class="fa fa-plus-circle"></i></a>';
	}
	actionContent += '</span></td>';
	
	var content = '<tr class="rate-content">'+
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierFrom+'" id="tierFrom'+index+'" name="tierFrom" placeholder="Tier From" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierFrom_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierTo+'" id="tierTo'+index+'" name="tierTo" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierTo_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.minimumCharge+'" id="minimumCharge'+index+'" name="minimumCharge" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="minimumCharge_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.maximumCharge+'" id="maximumCharge'+index+'" name="maximumCharge" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="maximumCharge_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.tierRate+'" id="tierRate'+index+'" name="tierRate" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="tierRate_error"></div>'+
					'</div>'+
					'</td>'+
					
					'<td>'+
					'<div class="form-group  mr  rateAppliedOn ">'+
					  	'<select class="ui dropdown search form-control " id="rateAppliedOn'+index+'" name="rateAppliedOn" onchange="">'+
					  	'<option value="">Please Select</option>'+
					  	
					  	<#if rateAppliedOnList?has_content>
					  	<#list rateAppliedOnList.entrySet() as entry>  
					  	'<option value="${entry.key}">${entry.value!}</option>'+
					  	</#list>
					  	</#if>
					  	
					  	'</select>'+
					  	'<div class="form-control-focus">'+
					  	'</div>'+
					  	'<span class="help-block with-errors"></span>'+
					'</div>'+
					'</td>'+	
					
					'<td>'+
					'<div class="form-group  mr  discountRateType ">'+
					  	'<select class="ui dropdown search form-control " id="discountRateType'+index+'" name="discountRateType" onchange="">'+
					  	'<option value="">Please Select</option>'+
					  	
					  	<#if discountRateTypeList?has_content>
					  	<#list discountRateTypeList.entrySet() as entry>  
					  	'<option value="${entry.key}">${entry.value!}</option>'+
					  	</#list>
					  	</#if>
					  	
					  	'</select>'+
					  	'<div class="form-control-focus">'+
					  	'</div>'+
					  	'<span class="help-block with-errors"></span>'+
					'</div>'+
					'</td>'+	
					
					'<td>'+
					'<div class="form-group mr">'+
					 	'<input type="number" class="form-control input-sm  " autocomplete="off" value="'+pricelistRate.discountRate+'" id="discountRate'+index+'" name="discountRate" placeholder="Tier To" required="" maxlength="" step=".0001">'+
					 	'<div class="help-block with-errors" id="discountRate_error"></div>'+
					'</div>'+
					'</td>'+	
					
					actionContent+																																																																																																																
					
				'</tr>'
				;
	
	return content;
}

function resetValueConfigEvents() {
   
    $('.configure-pricelist-value').unbind( "click" );
    $('.configure-pricelist-value').bind( "click", function( event ) {
    
    	pricingMethodId = $(this).attr("data-pricingMethodId");
    	productPriceListId = $(this).attr("data-productPriceListId");
		
		$('#pricelistValueModalView').modal("show");
    
    });
    
}

findPricelistMaintenances();
function findPricelistMaintenances() {
	
	//var serviceType = $("#serviceType").val();
	   	
   	var url = "searchPricelistMaintenances";
   
	$('#list_pricelist').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "chargeCode",
		          "render": function(data, type, row, meta){
		          	var data = row.chargeCode;
		            return data;
		          }
		         },
		         
		         /*{ "data": "description",
		          "render": function(data, type, row, meta){
		          	var data = row.description;
		            return data;
		          }
		         },*/
		         
		         { "data": "variationCodeId",
		          "render": function(data, type, row, meta){
		          	var data = row.variationCodeDesc;
		            return data;
		          }
		         },
		         { "data": "bundleId",
		          "render": function(data, type, row, meta){
		          	var data = row.bundleId;
		            return data;
		          }
		         },
		         { "data": "pricingMethodId",
		          "render": function(data, type, row, meta){
		          	var data = row.pricingMethodDesc;
		            return data;
		          }
		         },
		         { "data": "variationCurrency",
		          "render": function(data, type, row, meta){
		          	var data = row.variationCurrencyDesc;
		            return data;
		          }
		         },
	            
	            { "data": "fromDate" },
	            { "data": "thruDate" },
	            
	            { "data": "productPriceListId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<button data-pricingMethodId="'+row.pricingMethodId+'" data-productPriceListId="'+row.productPriceListId+'" class="btn btn-xs btn-secondary tooltips configure-pricelist-value" data-original-title="Detail" ><i class="fa fa-pencil-square-o"></i></button>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	      		resetValueConfigEvents();
	    	}
		});
}
	
</script>