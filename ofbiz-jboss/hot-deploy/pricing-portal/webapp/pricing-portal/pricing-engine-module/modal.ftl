
<div id="prodAssocValueModalView" class="modal fade" role="dialog" data-parentForm="">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">${uiLabelMap.transChargeMappingCreate!}</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				<div class="clearfix"></div>
				
				<form id="prodAssocValueModalForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
				
				<div class="row padding-r">
					<div class="col-md-6 col-sm-6 form-horizontal">
						
						<#if transAttrLeftList?has_content>
		
						<#list transAttrLeftList as transAttr>
						
							<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
								<#assign isRequired = true>
							<#else>
								<#assign isRequired = false>
							</#if>
							<#assign inputId = "TA_" + transAttr.attrName>
							
							<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
								<@dateInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								disablePastDate=false
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
								<@dropdownInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								options=appStatusList
								required=isRequired
								allowEmpty=true
								/>	
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								inputType="number"
								/>
							<#else>
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								/>
							</#if>
							
						</#list>
					
						</#if>		
				         
					</div>
					
					<div class="col-md-6 col-sm-6 form-horizontal">
					
						<#if transAttrRightList?has_content>
		
						<#list transAttrRightList as transAttr>
						
							<#if transAttr.isRequired?has_content && transAttr.isRequired=="Y">
								<#assign isRequired = true>
							<#else>
								<#assign isRequired = false>
							</#if>
							<#assign inputId = "TA_" + transAttr.attrName>
							
							<#if transAttr.inputType?has_content && transAttr.inputType=="TEXT">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="DATE">
								<@dateInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								disablePastDate=false
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="DROPDOWN">
								<@dropdownInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								options=appStatusList
								required=isRequired
								allowEmpty=true
								/>	
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="PICKER">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								/>
							<#elseif transAttr.inputType?has_content && transAttr.inputType=="NUMBER">
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								inputType="number"
								/>
							<#else>
								<@generalInput 
								id="${inputId}"
								label="${uiLabelMap.get(transAttr.description)}"
								placeholder="${uiLabelMap.get(transAttr.description)}"
								value="${transChargeMapping.get(transAttr.attrName)!}"
								required=isRequired
								/>
							</#if>
						
						</#list>
					
						</#if>	
					
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="billingAddressModal-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary"
						data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<#if chargeCode?has_content>
<div id="chargeCodeDetailModalView" class="modal fade" role="dialog" data-parentForm="">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">${uiLabelMap.chargeCodeDetails!}</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				<div class="clearfix"></div>
				
				<form id="chargeCodeDetailModalForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
				
				<div class="row padding-r">
					<div class="col-md-12 col-sm-12">
						
			        <@radioInput 
					name="invoiceRequired"
					label=uiLabelMap.invoice
					value=chargeCode.invoiceRequired
					options=yesNoOptions
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
			        
			        <@dropdownInput 
					id="printFlag"
					label=uiLabelMap.printFlag
					options=printFlagList
					required=false
					value=chargeCode.printFlag
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>
					
					<@dropdownInput 
					id="priceFreq"
					label=uiLabelMap.priceFreq
					options=priceFreqList
					required=false
					value=chargeCode.priceFreq
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
					
					<@dateInput 
					id="pricingDay"
					label=uiLabelMap.pricingDay
					value=chargeCode.pricingDay
					disablePastDate=false
					required=false
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
					
					<@dropdownInput 
					id="billAddressLevel"
					label=uiLabelMap.billAddressLevel
					options=billAddressLevelList
					required=false
					value=chargeCode.billAddressLevel
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>
					
					<@dropdownInput 
					id="invFreq"
					label=uiLabelMap.invFreq
					options=invFreqList
					required=false
					value=chargeCode.invFreq
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>
					
					<div class="form-group row">
			        <label class="col-sm-3 col-form-label">Invoice Date</label>
			        <div class="col-sm-3">
			        <@simpleDropdownInput 
					id="invDay"
					options=dayList
					required=false
					value=chargeCode.invDay
					allowEmpty=true
					/>
			        </div>
			        <div class="col-sm-6">
					 <label class="col-form-label"><small>Month &amp; Year will taken automatically.</small> </label>
			          <!--input type='text' class="form-control input-sm" placeholder=""  Value="Month"/!-->
			        </div>
			        <!--div class="col-sm-3">
			        <input type='text' class="form-control input-sm" placeholder=""  Value="Year"/>
			        </div!-->
			        </div>
					<#-- 
					<@dropdownInput 
					id="currencyType"
					label=uiLabelMap.accountCurrency
					options=currencyTypeList
					required=false
					value=chargeCode.currencyType
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
					-->
					<@dropdownInput 
					id="bundleCurrency"
					label=uiLabelMap.variationCurrency
					options=currencyList
					required=false
					value=chargeCode.bundleCurrency
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>																																																																																																																																																																																																																																																																																																																																																																																																														
				         
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="chargeCodeDetailModal-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary"
						data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="glConfigModalView" class="modal fade" role="dialog" data-parentForm="">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">${uiLabelMap.glConfig!}</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				<div class="clearfix"></div>
				
				<form id="glConfigModalForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
				
				<div class="row padding-r">
					<div class="col-md-12 col-sm-12">
						
			        <@generalInput 
					id="glAccountId"
					label=uiLabelMap.glAccountId
					placeholder=uiLabelMap.glAccountId
					value=chargeCode.glAccountId
					required=true
					maxlength=60
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>		
			        
			        <@dropdownInput 
					id="glCurrency"
					label=uiLabelMap.glCurrency
					options=bundleCurrencyTypeList
					required=false
					value=chargeCode.glCurrency
					allowEmpty=true
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
					
					<@generalInput 
					id="pcCode"
					label=uiLabelMap.pcCode
					placeholder=uiLabelMap.pcCode
					value=chargeCode.pcCode
					required=false
					maxlength=60
					labelColSize="col-sm-3" 
					inputColSize="col-sm-9"
					/>	
					
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="chargeCodeDetailModal-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary"
						data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</#if>

<script>

jQuery(document).ready(function() {

$('#invDay').dropdown({
useLabels: false
});

$("#priceFreq").change(function() {
	alert( $("#priceFreq").val() );
});	

/*
$("#billingAddressModal-apply-btn").click(function() {
	
	
	
});
*/

$('#prodAssocValueModalForm').validator().on('submit', function (e) {
	var parentForm = $('#prodAssocValueModalView').attr("data-parentForm");
	
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
		
		<#if transAttrLeftList?has_content>
		<#list transAttrLeftList as transAttr>
		
		<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
		<#assign tempInputId = "TA_" + transAttr.attrName>
		$('#'+parentForm+' input[name="${inputId}"]').val( $('#${tempInputId}').val() );
		
		</#list>
		</#if>	
		
		<#if transAttrRightList?has_content>
		<#list transAttrRightList as transAttr>
		
		<#assign inputId = "TRANS_ATTR_" + transAttr.attrName>
		<#assign tempInputId = "TA_" + transAttr.attrName>
		$('#'+parentForm+' input[name="${inputId}"]').val( $('#${tempInputId}').val() );
		
		</#list>
		</#if>  
		      
		$('#prodAssocValueModalView').modal("hide");
	
    	// everything looks good!
  	}
});

$('#chargeCodeDetailModalForm').validator().on('submit', function (e) {
	var parentForm = $('#chargeCodeDetailModalView').attr("data-parentForm");
	
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
		 
		$('#'+parentForm+' input[name="invoiceRequired"]').val( $('#chargeCodeDetailModalForm input[name="invoiceRequired"]:checked').val() );   
  		$('#'+parentForm+' input[name="printFlag"]').val( $('#printFlag').val() );
		$('#'+parentForm+' input[name="priceFreq"]').val( $('#priceFreq').val() );
		$('#'+parentForm+' input[name="pricingDay"]').val( $('#pricingDay').val() );
		$('#'+parentForm+' input[name="billAddressLevel"]').val( $('#billAddressLevel').val() );
		$('#'+parentForm+' input[name="invFreq"]').val( $('#invFreq').val() );
		$('#'+parentForm+' input[name="invDay"]').val( $('#invDay').val() );
		$('#'+parentForm+' input[name="currencyType"]').val( $('#currencyType').val() );
		$('#'+parentForm+' input[name="bundleCurrency"]').val( $('#bundleCurrency').val() );
	
		$('#chargeCodeDetailModalView').modal("hide");
	
    	// everything looks good!
  	}
});

$('#glConfigModalForm').validator().on('submit', function (e) {
	var parentForm = $('#glConfigModalView').attr("data-parentForm");
	
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
		 
		$('#'+parentForm+' input[name="glAccountId"]').val( $('#glAccountId').val() );
  		$('#'+parentForm+' input[name="glCurrency"]').val( $('#glCurrency').val() );
		$('#'+parentForm+' input[name="pcCode"]').val( $('#pcCode').val() );
	
		$('#glConfigModalView').modal("hide");
	
    	// everything looks good!
  	}
});

});

</script>