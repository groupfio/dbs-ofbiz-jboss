<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.systemConfiguration}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="createProfileConfigForm" class="form-horizontal" name="createProfileConfigForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="profileConfigurationId"
				placeholder=uiLabelMap.uniqueId
				value=filterProfileConfig.uniqueId
				required=false
				maxlength=60
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="profileType"
				options=profileTypeList
				required=false
				value=filterProfileConfig.profileType
				allowEmpty=true
				emptyText = uiLabelMap.profileType
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="profileCode"
				placeholder=uiLabelMap.profileCode
				value=filterProfileConfig.profileCode
				required=false
				maxlength=60
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="profileDescription"
				placeholder=uiLabelMap.profileDescription
				value=filterProfileConfig.profileDescription
				required=false
				maxlength=60
				/>
         </div>
         
         <@fromSimpleAction id="add-profileConfig-button" showCancelBtn=false isSubmitAction=false submitLabel="Add"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<script>

jQuery(document).ready(function() {	

$('#add-profileConfig-button').on('click', function(){
	
	$.post('createProfileConfig', $('#createProfileConfigForm').serialize(), function(returnedData) {
	
			if (returnedData.code == 200) {
				
				$('#createProfileConfigForm')[0].reset();
				findProfileConfigurations();
				
				showAlert ("success", returnedData.message)
				
			} else {
				showAlert ("error", returnedData.message)
			}
				
		});
	
});

});

</script>
