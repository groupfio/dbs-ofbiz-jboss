<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.systemConfiguration}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_profile_config_main">
	<thead>
	<tr>
		<th>${uiLabelMap.uniqueId!}</th>
		<th>${uiLabelMap.profileType!}</th>
		<th>${uiLabelMap.profileCode!}</th>
		<th>${uiLabelMap.profileDescription!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	/*
	$('#list-custom-field-group').DataTable({
  		"order": [],
  		"fnDrawCallback": function( oSettings ) {
      		resetDefaultEvents();
    	}
	});
	*/

$('#find-profileConfig-button').on('click', function(){
	findProfileConfigurations();
});
			
});	

findProfileConfigurations();
function findProfileConfigurations() {
	
	var profileConfigurationId = $("#profileConfigurationId").val();
	var profileType = $("#profileType").val();
	var profileCode = $("#profileCode").val();
	var profileDescription = $("#profileDescription").val();
	   	
   	//var url = "searchSystemConfigurations?profileConfigurationId="+profileConfigurationId+"&profileType="+profileType+"&profileCode="+profileCode+"&profileDescription="+profileDescription;
   	var url = "searchSystemConfigurations";
   
	$('#list_profile_config_main').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": true,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "profileConfigurationId" },
	            
	            { "data": "profileTypeId",
		          "render": function(data, type, row, meta){
		          	var data = row.profileTypeDesc;
		            return data;
		          }
		         },  
	            { "data": "profileCode" },
	            
	            { "data": "profileDescription" },
	            
		         { "data": "profileConfigurationId",
		          "render": function(data, type, row, meta){
		            if(type === 'display'){
		                data = '<div class="text-center"><a class="btn btn-xs btn-secondary tooltips confirm-message" onclick="javascript: removeProfileConfig(\''+row.profileConfigurationId+'\')" data-original-title="Remove" data-config-id="'+row.profileConfigurationId+'"><i class="fa fa-trash"></i></a></div>';
		            }
		            return data;
		         }
		      	},    
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	

function removeProfileConfig (profileConfigurationId) {
	
	$.ajax({
			      
		type: "POST",
     	url: "removeProfileConfig",
        data:  {"profileConfigurationId": profileConfigurationId},
        success: function (data) {   
            
            findProfileConfigurations();
			    	
        }
        
	});    
	
}
	
</script>