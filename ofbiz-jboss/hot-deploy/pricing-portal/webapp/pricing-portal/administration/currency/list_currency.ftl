<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.currency}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_currency">
	<thead>
	<tr>
		<th>${uiLabelMap.currencyCode!}</th>
		<th>${uiLabelMap.abbreviation!}</th>
		<th>${uiLabelMap.description!}</th>
		<th>${uiLabelMap.isEnabled!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	
$('#find-currency-button').on('click', function(){
	findCurrencys();
});
			
findCurrencys();
function findCurrencys() {
	
	var currencyCode = $("#currencyCode").val();
	var abbreviation = $("#abbreviation").val();
	var description = $("#description").val();
	var isEnabled = $("#isEnabled").val();
	   	
   	var url = "searchCurrencys?currencyCode="+currencyCode+"&abbreviation="+abbreviation+"&description="+description+"&isEnabled="+isEnabled;
   
	$('#list_currency').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "uomId" },
	            { "data": "abbreviation" },
	            { "data": "description" },
	            { "data": "isEnabled" },
	            
	            { "data": "uomId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                data += '<a class="btn btn-xs btn-secondary tooltips" href="currencyUpdate?currencyCode='+row.uomId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	    	}
		});
}	
	
});	
	
</script>