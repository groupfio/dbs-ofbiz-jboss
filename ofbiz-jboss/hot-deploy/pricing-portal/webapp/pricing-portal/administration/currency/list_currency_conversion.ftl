<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.currencyConversion}</h2>
</div>
			
<div class="table-responsive">
	<table class="table table-hover" id="list_currency_conversion">
	<thead>
	<tr>
		<th>${uiLabelMap.fromCurrency!}</th>
		<th>${uiLabelMap.toCurrency!}</th>
		<th>${uiLabelMap.conversionFactor!}</th>
		<th>${uiLabelMap.conversionMethod!}</th>
		<th>${uiLabelMap.fromDate!}</th>
		<th>${uiLabelMap.thruDate!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {		
	
$('#find-currencyConversion-button').on('click', function(){
	findCurrencyConversions();
});
	
});	

findCurrencyConversions();
function findCurrencyConversions() {
	
	//var currencyCode = $("#currencyCode").val();
	 	
   	var url = "searchCurrencyConversions";
   
	$('#list_currency_conversion').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "searching": false,
		    "destroy": true,
		    "ajax": {
	            "url": url,
	            "type": "POST"
	        },
	        "pageLength": 20,
	        "stateSave": false,
	        
	        "columnDefs": [ 
	        	{
					"targets": 1,
					"orderable": false
				}
			],
					      
	        "columns": [
					        	
	            { "data": "uomId" },
	            { "data": "uomIdTo" },
	            { "data": "conversionFactor" },
	            { "data": "conversionMethod",
		          "render": function(data, type, row, meta){
		          	var data = row.conversionMethodDesc;
		            return data;
		          }
		         },
	            { "data": "fromDate" },
	            { "data": "thruDate" },
	            
	            { "data": "uomId",
		          "render": function(data, type, row, meta){
		          	var data = '<div class="text-center ml-1" >';
		          	
		            if(type === 'display'){
		                //data += '<a class="btn btn-xs btn-secondary tooltips" href="currencyUpdate?currencyCode='+row.uomId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
		                data += '<button class="btn btn-xs btn-secondary tooltips remove-currency-conversion" data-original-title="Remove" data-uomId="'+row.uomId+'" data-uomIdTo="'+row.uomIdTo+'" data-fromDate="'+row.fromDate+'"><i class="fa fa-trash"></i></button>';
		            }
		            
		            data += "</div>";
		            return data;
		          }
		         },     
	        ],
	        "fnDrawCallback": function( oSettings ) {
	      		resetDefaultEvents();
	      		resetCurrencyConversionEvents();
	    	}
		});
}	

function resetCurrencyConversionEvents() {
   
    $('.remove-currency-conversion').unbind( "click" );
    $('.remove-currency-conversion').bind( "click", function( event ) {
    
    	uomId = $(this).attr("data-uomId");
    	uomIdTo = $(this).attr("data-uomIdTo");
		fromDate = $(this).attr("data-fromDate");
		
		$.ajax({
			      
			type: "POST",
	     	url: "removeCurrencyConversion",
	        data:  {"uomId": uomId, "uomIdTo": uomIdTo, "fromDate": fromDate},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
					showAlert ("success", data.message);
					findCurrencyConversions();
				} else {
					showAlert ("error", returnedData.message);
				}
				
	        }
		}); 
    
    });
    
}
	
</script>