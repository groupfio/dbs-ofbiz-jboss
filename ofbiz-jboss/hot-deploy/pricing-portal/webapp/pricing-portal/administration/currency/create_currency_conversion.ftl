<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.currencyConversion}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>currencyConversionCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@dropdownInput 
				id="uomId"
				label=uiLabelMap.fromCurrency
				options=currencyList
				required=true
				value=currency.uomId
				allowEmpty=true
				/>
				
			<@dropdownInput 
				id="uomIdTo"
				label=uiLabelMap.toCurrency
				options=currencyList
				required=true
				value=currency.uomIdTo
				allowEmpty=true
				/>
				
			<@generalInput 
				id="conversionFactor"
				label=uiLabelMap.conversionFactor
				placeholder=uiLabelMap.conversionFactor
				value=currency.conversionFactor
				required=true
				inputType="number"
				step=".0001"
				/>	
				
			<@dropdownInput 
				id="conversionMethod"
				label=uiLabelMap.conversionMethod
				options=conversionMethodList
				required=true
				value=currency.conversionMethod
				allowEmpty=true
				/>	
				
			<@dateInput 
				id="fromDate"
				label=uiLabelMap.fromDate
				disablePastDate=false
				required=true
				dateFormat="YYYY-MM-DD"
				/>	
				
			<@dateInput 
				id="thruDate"
				label=uiLabelMap.thruDate
				disablePastDate=false
				required=false
				dateFormat="YYYY-MM-DD"
				/>								
																													
			</div>
							
		</div>
						
	</div>
		
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
