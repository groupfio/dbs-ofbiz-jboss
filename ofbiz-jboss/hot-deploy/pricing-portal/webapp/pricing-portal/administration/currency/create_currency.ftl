<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.currencyCreate}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>currencyCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<input type="hidden" name="uomTypeId" value="CURRENCY_MEASURE">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="currencyCode"
				label=uiLabelMap.currencyCode
				placeholder=uiLabelMap.currencyCode
				value=currency.currencyCode
				required=true
				maxlength=60
				/>
				
			<@generalInput 
				id="abbreviation"
				label=uiLabelMap.abbreviation
				placeholder=uiLabelMap.abbreviation
				value=currency.abbreviation
				required=true
				maxlength=60
				/>
				
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=currency.description
				required=true
				maxlength=255
				/>							
																															
			</div>
							
		</div>
						
	</div>
		
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>
