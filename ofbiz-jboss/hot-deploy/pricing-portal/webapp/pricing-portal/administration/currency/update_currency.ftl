<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.currencyUpdate} - ${currency.uomId!}</h1>
</div>

<form role="form" class="form-horizontal" action="<@ofbizUrl>currencyUpdateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<input type="hidden" name="uomTypeId" value="CURRENCY_MEASURE">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@readonlyInput 
				id="currencyCode"
				label=uiLabelMap.currencyCode
				value=currency.uomId
				isHiddenInput=true
				/>
				
			<@generalInput 
				id="abbreviation"
				label=uiLabelMap.abbreviation
				placeholder=uiLabelMap.abbreviation
				value=currency.abbreviation
				required=true
				maxlength=60
				/>
				
			<@generalInput 
				id="description"
				label=uiLabelMap.description
				placeholder=uiLabelMap.description
				value=currency.description
				required=true
				maxlength=255
				/>
				
			<@dropdownInput 
				id="isEnabled"
				label=uiLabelMap.isEnabled
				options=yesNoOptions
				required=false
				value=currency.isEnabled
				allowEmpty=true
				/>																
																															
			</div>
							
		</div>
						
	</div>
		
</div>
	
</div>

<@fromCommonAction showCancelBtn=false showClearBtn=false/>

</form>
