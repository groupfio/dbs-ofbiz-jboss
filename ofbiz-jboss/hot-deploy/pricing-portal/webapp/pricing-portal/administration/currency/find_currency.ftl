<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.currencyFind}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findCurrencyForm" class="form-horizontal" name="findCurrencyForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="currencyCode"
				placeholder=uiLabelMap.currencyCode
				value=filterCurrency.currencyCode
				required=false
				maxlength=60
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="abbreviation"
				placeholder=uiLabelMap.abbreviation
				value=filterCurrency.abbreviation
				required=false
				maxlength=60
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleInput 
				id="description"
				placeholder=uiLabelMap.description
				value=filterCurrency.description
				required=false
				maxlength=255
				/>
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDropdownInput 
				id="isEnabled"
				options=yesNoOptions
				required=false
				value=filterCurrency.isEnabled
				allowEmpty=true
				emptyText = uiLabelMap.isEnabled
				/>	
         </div>
         
         <@fromSimpleAction id="find-currency-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>
