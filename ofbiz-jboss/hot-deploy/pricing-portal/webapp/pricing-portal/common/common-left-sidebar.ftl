<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<#if userLogin?has_content>

<#assign getMenuList = dispatcher.runSync("getComponentMenuList", Static["org.ofbiz.base.util.UtilMisc"].toMap("activeApp", activeApp, "userLogin", userLogin))/>

<nav class="bg-light sidebar sidenav" id="mySidenav">
	<div class="sidebar-sticky">
		<ul class="nav flex-column list-unstyled mt-3">
			
			<li class="nav-item"><a class="nav-link active" href="#">Dashboard</a></li>
			
			<#if getMenuList?has_content>
		  	<#list getMenuList.menuList as getMenuGV>
		    <#assign tab = getMenuGV.get("tab")>
		    <#assign shortcut = getMenuGV.get("shortcut")>
					    		    		    
		    <li class="<#if sectionName?exists && sectionName?has_content && sectionName == "${tab.pageId?if_exists}">active</#if>"><a href="#${tab.tabId!}" data-toggle="collapse"
				aria-expanded="false" class="dropdown-toggle nav-link">${uiLabelMap.get(tab.uiLabels)}</a>
				<ul class="collapse list-unstyled" id="${tab.tabId!}">
					
					<#list shortcut as short>
						<#if !short.parentShortcutId?has_content>
						
						<@menuItemLink getMenuGV short />
						
						</#if>
                    </#list>
					
				</ul>
			</li>
		    
		  	</#list>
			</#if>
							
		</ul>
		
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	</div>
</nav>

<div
	class="badge rounded-circle border bg-light display-3 position-absolute"
	style="font-size: 15px; cursor: pointer; z-index: 1;"
	onclick="openNav()">&#9776;</div>

</#if>	
