<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>
<#include "component://pricing-portal/webapp/pricing-portal/customer-account/modal.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.accountCreate}</h1>
	<div class="float-right">  
		<button id="nominated-account-modal-btn" style="display: none;" type="button" class="btn btn-xs btn-primary"> Nominated Account</button>  
		<button id="billing-address-modal-btn" type="button" class="btn btn-xs btn-primary"> Billing Address</button> 
	</div>
</div>

<form id="accountCreateForm" role="form" class="form-horizontal" action="<@ofbizUrl>accountCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@dropdownInput 
				id="customerId"
				label=uiLabelMap.customer
				options=customerList
				required=true
				value=account.customer
				allowEmpty=false
				/>
			
			<@generalInput 
				id="accountId"
				label=uiLabelMap.accountId
				placeholder=uiLabelMap.accountId
				value=account.accountId
				required=true
				maxlength=60
				pattern="[a-zA-Z0-9]+"
				/>	
			
			<@dateInput 
				id="openDate"
				label=uiLabelMap.openDate
				disablePastDate=false
				required=false
				/>	
			
			<@generalInput 
				id="bundle"
				label=uiLabelMap.bundle
				placeholder=uiLabelMap.bundle
				value=account.bundle
				required=false
				maxlength=60
				/>	
				
			<@dateInput 
				id="bundleFromDate"
				label=uiLabelMap.bundleFromDate
				disablePastDate=false
				required=false
				/>	
			
			<@dropdownInput 
				id="poolingPoint"
				label=uiLabelMap.poolingPoint
				options=yesNoOptions
				required=false
				value=account.poolingPoint
				allowEmpty=true
				/>		
				
			<@dropdownInput 
				id="balanceConsolidated"
				label=uiLabelMap.balanceConsolidated
				options=yesNoOptions
				required=false
				value=account.balanceConsolidated
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="taxPrintIndicator"
				label=uiLabelMap.taxPrintIndicator
				options=yesNoOptions
				required=false
				value=account.taxPrintIndicator
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="advicePrintIndicator"
				label=uiLabelMap.advicePrintIndicator
				options=yesNoOptions
				required=false
				value=account.advicePrintIndicator
				allowEmpty=true
				/>	
			<#-- 	
			<@dropdownInput 
				id="defaultAccount"
				label=uiLabelMap.defaultAccount
				options=yesNoOptions
				required=false
				value=account.defaultAccount
				allowEmpty=true
				/>						
			 -->
																															
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="accountName"
				label=uiLabelMap.accountName
				placeholder=uiLabelMap.accountName
				value=account.accountName
				required=true
				maxlength=60
				pattern="^[ A-Za-z0-9'@.!&:*()+-]*$"
				dataError="Please enter valid name"
				/>	
			
			<@dropdownInput 
				id="accountType"
				label=uiLabelMap.accountType
				options=accountTypeList
				required=true
				value=account.accountType
				allowEmpty=true
				/>	
			
			<@dateInput 
				id="applicationDate"
				label=uiLabelMap.applicationDate
				disablePastDate=false
				required=true
				/>
				
			<@dropdownInput 
				id="accountCurrency"
				label=uiLabelMap.accountCurrency
				options=bundleCurrencyTypeList
				required=true
				value=account.accountCurrency
				allowEmpty=false
				/>	
			
			<@dateInput 
				id="bundleToDate"
				label=uiLabelMap.bundleToDate
				disablePastDate=false
				required=false
				/>
			
			<@dropdownInput 
				id="feeFloatBased"
				label=uiLabelMap.feeFloatBased
				options=feeFloatBasedList
				required=false
				value=account.feeFloatBased
				allowEmpty=true
				/>	
																													
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<input type="hidden" name="generalAttnName" value="">
<input type="hidden" name="addressLine1" value="">
<input type="hidden" name="addressLine2" value="">
<input type="hidden" name="addressLine3" value="">
<input type="hidden" name="addressLine4" value="">
<input type="hidden" name="addressLine5" value="">
<input type="hidden" name="countryGeoId" value="">
<input type="hidden" name="stateProvinceGeoId" value="">
<input type="hidden" name="city" value="">
<input type="hidden" name="postalCode" value="">

<input type="hidden" name="nominatedAccountId" value="">
<input type="hidden" name="nominatedAccountFromDate" value="">
<input type="hidden" name="nominatedAccountThruDate" value="">

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>

<script>

jQuery(document).ready(function() {

$("#billing-address-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#billingAddressModalView').attr("data-parentForm", "accountCreateForm");    
        
    $('#billingAddressModalView').modal("show");
		
});

$("#nominated-account-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#nominatedAccountModalView').attr("data-parentForm", "accountCreateForm");    
        
    $('#nominatedAccountModalView').modal("show");
		
});

$('#nominatedAccountModalView').on('shown.bs.modal', function (e) {
	loadCustomerAccountList();	   
});

$("#poolingPoint").change(function(e) {
	
	if ($("#poolingPoint").val() == "Y") {
		$("#nominated-account-modal-btn").hide();
	} else {
		$("#nominated-account-modal-btn").show();
	}
		
});

});

function loadCustomerAccountList() {
	
	var ddOptions = '';
	
	if ( $("#customerId").val() ) {
		$('#nominatedAccountId').dropdown('clear');
		$.ajax({
			      
			type: "POST",
	     	url: "getCustomerAccounts",
	        data:  {"partyId": $("#customerId").val()},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.accountList.length; i++) {
	            		var account = data.accountList[i];
	            		ddOptions += '<option value="'+account.accountId+'">('+account.accountId+') '+account.accountName+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
		$("#nominatedAccountId").html( ddOptions );
		
		$("#nominatedAccountId").dropdown('refresh');
		
	}
		
}

</script>

