<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>

<div class="page-header border-b">
	<h1>${uiLabelMap.customerView}</h1>
</div>

<div class="card-header mt-2 mb-3">
   <form method="post" action="#" id="findCustomerForm" class="form-horizontal" name="findCustomerForm" novalidate="novalidate" data-toggle="validator">
      <div class="row">
      	
         <div class="col-md-2 col-sm-2">
         	<@simpleDateInput 
				id="fromDate"
				disablePastDate=false
				required=true
				/>	
         </div>
         <div class="col-md-2 col-sm-2">
         	<@simpleDateInput 
				id="thruDate"
				disablePastDate=false
				required=false
				/>	
         </div>
         
         <@fromSimpleAction id="find-customer-button" showCancelBtn=false isSubmitAction=false submitLabel="Search"/>
        	
      </div>
   </form>
   <div class="clearfix"> </div>
</div>

<script>

jQuery(document).ready(function() {

$('#find-customer-button').on('click', function(){
	findCustomers();
});

});

</script>
