<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros_dbs.ftl"/>
<#include "component://pricing-portal/webapp/pricing-portal/customer-account/modal.ftl"/>

<div class="page-header border-b">
	<h1 class="float-left">${uiLabelMap.customerCreate}</h1>
	<div class="float-right">  
		<#-- <button type="reset" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#DD"> Direct Debit</button> -->  
		<button id="billing-address-modal-btn" type="reset" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#billingAddressModalView"> Billing Address</button> 
	</div>
</div>

<form id="customerCreateForm" role="form" class="form-horizontal" action="<@ofbizUrl>customerCreateAction</@ofbizUrl>" encType="multipart/form-data" method="post" data-toggle="validator">

<div class="card-header">
<div class="row padding-r">
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			
			<@generalInput 
				id="customerId"
				label=uiLabelMap.customerId
				placeholder=uiLabelMap.customerId
				value=customer.customerId
				required=true
				maxlength=60
				pattern="[a-zA-Z0-9]+"
				/>	
				
			<@generalInput 
				id="companyName"
				label=uiLabelMap.companyName
				placeholder=uiLabelMap.companyName
				value=customer.companyName
				required=true
				maxlength=255
				pattern="^[ A-Za-z0-9'@.!&:*()+-]*$"
				dataError="Please enter valid name"
				/>	
				
			<@generalInput 
				id="relationshipManager"
				label=uiLabelMap.relationshipManager
				placeholder=uiLabelMap.relationshipManager
				value=customer.relationshipManager
				required=false
				maxlength=60
				/>		
				
			<@dateInput 
				id="openDate"
				label=uiLabelMap.openDate
				disablePastDate=false
				required=isRequired
				/>	
			<#-- 	
			<@generalInput 
				id="modeOfPayment"
				label=uiLabelMap.modeOfPayment
				placeholder=uiLabelMap.modeOfPayment
				value=customer.modeOfPayment
				required=false
				maxlength=60
				/>	
			 -->	
			<@dropdownInput 
				id="currencyType"
				label=uiLabelMap.currencyType
				options=bundleCurrencyTypeList
				required=true
				value=customer.currencyType
				allowEmpty=false
				/>																																																																																																																																																																																																																																																																																																																																													
																																	
			</div>
							
		</div>
						
	</div>
	
	<div class="col-md-6 col-sm-6">
				
		<div class="portlet-body form">
							
			<div class="form-body">
			<#-- 	
			<@generalInput 
				id="parentId"
				label=uiLabelMap.parentId
				placeholder=uiLabelMap.parentId
				value=customer.parentId
				required=false
				maxlength=100
				/>
			 -->
			<@generalInput 
				id="emailAddress"
				label=uiLabelMap.emailId
				placeholder=uiLabelMap.emailId
				value=customer.emailAddress
				required=false
				maxlength=255
				inputType="email"
				dataError="Please enter valid email address"
				/>	
						
			<@dropdownInput 
				id="billingFrequency"
				label=uiLabelMap.billingFrequency
				options=billingFrequencyList
				required=false
				value=customer.billingFrequency
				allowEmpty=true
				/>	
				
			<@dropdownInput 
				id="giroConsolidationFrequency"
				label=uiLabelMap.giroConsolidationFrequency
				options=giroConsFreqList
				required=false
				value=customer.giroConsolidationFrequency
				allowEmpty=true
				/>
				
			<@dropdownInput 
				id="giroDebitingFrequency"
				label=uiLabelMap.giroDebitingFrequency
				options=giroDbtFreqList
				required=false
				value=customer.giroDebitingFrequency
				allowEmpty=true
				/>
				
			<@dropdownInput 
				id="giroDebiting"
				label=uiLabelMap.giroDebiting
				options=yesNoOptions
				required=false
				value=customer.giroDebiting
				allowEmpty=true
				/>																																																																																																																																																																																																								
																															
			</div>
							
		</div>
						
	</div>
			
</div>
	
</div>

<input type="hidden" name="generalAttnName" value="">
<input type="hidden" name="addressLine1" value="">
<input type="hidden" name="addressLine2" value="">
<input type="hidden" name="addressLine3" value="">
<input type="hidden" name="addressLine4" value="">
<input type="hidden" name="addressLine5" value="">
<input type="hidden" name="countryGeoId" value="">
<input type="hidden" name="stateProvinceGeoId" value="">
<input type="hidden" name="city" value="">
<input type="hidden" name="postalCode" value="">

<@fromCommonAction showCancelBtn=false showClearBtn=true/>

</form>

<script>

$("#billing-address-modal-btn").click(function(e) {
	
	e.preventDefault(); 
        
    $('#billingAddressModalView').attr("data-parentForm", "customerCreateForm");
    
    if (!$('#generalAttnName').val()) {
    	$('#generalAttnName').val( $('#companyName').val() );    
    }
	        
    $('#billingAddressModalView').modal("show");
		
});

</script>

