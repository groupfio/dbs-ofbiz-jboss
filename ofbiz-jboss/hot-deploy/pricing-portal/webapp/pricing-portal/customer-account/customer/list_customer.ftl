<#include "component://homeapps/webapp/homeapps/lib/ofbizFormMacros.ftl"/>

<div class="page-header">
	<h2 class="float-left">${uiLabelMap.ListOf} ${uiLabelMap.customer!}</h2>
	<#-- <div class="float-right">
		<div class="form-group row">
			<div class="col-sm-5">
				<input type="text" class="form-control input-sm" placeholder="">
			</div>
			<div class="col-sm-3">
				<button type="reset" class="btn btn-xs btn-primary m5">Search</button>
			</div>
			<div class="col-sm-4">
				<a href="" class="btn btn-xs btn-primary m5" alt="Report"
					title="Report">Clear All Filters</a>
			</div>
	
		</div>
	
	</div> -->
</div>

<div class="table-responsive">
	<table class="table table-striped" id="customer-list">
	<thead>
	<tr>
		<th></th>
		<th>${uiLabelMap.partyId!}</th>
		<th>${uiLabelMap.customerId!}</th>
		<th>${uiLabelMap.companyName!}</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
		
	</tbody>
	</table>
</div>

<script type="text/javascript">

var customerGrid;

jQuery(document).ready(function() {	

	findCustomers();
		
});	

function resetCustomerEvents() {
	
	$('#customer-list td.details-control').unbind( "click" );
	$('#customer-list td.details-control').bind( "click", function( event ) {
		
        var tr = $(this).closest('tr');
        var row = customerGrid.row( tr );
     
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            var customerId = row.data()['partyId'];
            var subtable_id = "subtable-"+customerId;
            row.child(prepareAccountGrid(customerId, subtable_id)).show(); /* HERE I format the new table */
            tr.addClass('shown');
            findAccounts(customerId, subtable_id); /*HERE I was expecting to load data*/
        }
    });
    
	$('.refresh-accounts').unbind( "click" );
	$('.refresh-accounts').bind( "click", function( event ) {
	
		event.preventDefault(); 
		
		customerId = $(this).attr("data-customerId");
		tableId = $(this).attr("data-tableId");
		
		findAccounts(customerId, tableId);
																										
	});
    
}

function findCustomers() {
	
	//var searchPartyId = $("#partyId").val();
	
	var fromDate = $('#findCustomerForm input[name="fromDate"]').val();
	var thruDate = $('#findCustomerForm input[name="thruDate"]').val();
   	
   	var url = "searchCustomers?fromDate="+fromDate+"&thruDate="+thruDate;
   
	customerGrid = $('#customer-list').DataTable( {
	    "processing": true,
	    "serverSide": true,
	    "destroy": true,
	    "searching": false,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "pageLength": 15,
        "stateSave": false,
        /*
        "columnDefs": [ 
        	{
				"targets": 8,
				"orderable": false,
				"className": "longtext"
			} 
		],
		*/	      
		"order": [[ 1, "desc" ]],
        "columns": [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },	
            { "data": "partyId", "visible": false },        	
            { "data": "partyIdentificationIdValue" },
            { "data": "groupName" },
            
            { "data": "partyId",
	          "render": function(data, type, row, meta){
	          	var data = '<div class="text-center ml-1" >';
	          	
	            if(type === 'display'){
	                data += '<a class="btn btn-xs btn-secondary tooltips" href="customerUpdate?partyId='+row.partyId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
	            }
	            
	            data += "</div>";
	            return data;
	          }
	         }, 
            
        ],
        "fnDrawCallback": function(settings, json) {
		    resetDefaultEvents();
		    resetCustomerEvents();
		}
	});
	
}

function findAccounts(customerId, subTableId) {
	
   	var url = "searchAccounts?customerId="+customerId;
   
	$('#'+subTableId).DataTable( {
	    "processing": true,
	    "serverSide": true,
	    "searching": false,
	    "destroy": true,
	    "searching": false,
	    "ajax": {
            "url": url,
            "type": "POST"
        },
        "pageLength": 15,
        "stateSave": false,
        /*
        "columnDefs": [ 
        	{
				"targets": 14,
				"orderable": false,
				"className": "longtext"
			} 
		],
		*/	  
		"order": [[ 0, "asc" ]],    
        "columns": [
			{ "data": "accountId" },
            { "data": "accountName" },
            { "data": "accountId",
	          "render": function(data, type, row, meta){
	          	var data = '<div class="text-center ml-1" >';
	          	
	            if(type === 'display'){
	                data += '<a class="btn btn-xs btn-secondary tooltips" href="accountUpdate?partyId='+row.custPartyId+'&accountId='+row.accountId+'" data-original-title="Edit" ><i class="fa fa-pencil-square-o"></i></a>';
	            }
	            
	            data += "</div>";
	            return data;
	          }
	         }, 
        ],
        "fnDrawCallback": function(settings, json) {
		    resetDefaultEvents();
		    resetCustomerEvents();
		}
	});
	
}

function prepareAccountGrid ( customerId, table_id ) {
    // `d` is the original data object for the row
    return '<div class="page-header ml-4 mr-4"><h2 class="float-left display-4">Accounts</h2><div class="float-right"><a href="#" class="btn btn-xs btn-primary m5 refresh-accounts tooltips" data-customerId="'+customerId+'" data-tableId="'+table_id+'" title="Refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a></div></div>' + 
    '<table id="'+table_id+'" class="table table-striped">'+
    '<thead>'+
    '<th>${uiLabelMap.accountId!}</th>'+
    '<th>${uiLabelMap.accountName!}</th>'+
    '<th class="text-center">Action</th>'+
    '</thead>'+
    '</table>';
}

</script>