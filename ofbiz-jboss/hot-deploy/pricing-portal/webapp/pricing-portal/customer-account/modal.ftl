
<div id="billingAddressModalView" class="modal fade" role="dialog" data-parentForm="">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Customer Address</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="col-12 bg-light mb-1">
					<strong>Billing Address</strong>
				</div>
				<div class="clearfix"></div>
				
				<form id="billingAddressModalForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
				
				<div class="row padding-r">
					<div class="col-md-8 col-sm-8 form-horizontal">
						
						<@generalInput 
			               id="generalAttnName"
			               name="generalAttnName"
			               label=uiLabelMap.attentionName
			               placeholder=uiLabelMap.attentionName
			               value="${postalAddress?if_exists.attnName?if_exists}"
			               required=true
			               maxlength=255
			               pattern="^([^0-9]*)$"
							dataError="Please enter valid name"
			               />
               
						<@generalInput 
				         id="addressLine1"
				         label=uiLabelMap.address1
				         placeholder=uiLabelMap.address1
				         value="${postalAddress?if_exists.address1?if_exists}"
				         required=false
				         maxlength="255"
				         />
				         
				         <@generalInput 
				         id="addressLine2"
				         label=uiLabelMap.address2
				         placeholder=uiLabelMap.address2
				         value="${postalAddress?if_exists.address2?if_exists}"
				         required=false
				         maxlength="255"
				         />
				         
				         <@generalInput 
				         id="addressLine3"
				         label=uiLabelMap.address3
				         placeholder=uiLabelMap.address3
				         value="${postalAddress?if_exists.address3?if_exists}"
				         required=false
				         maxlength="255"
				         />
				         
				         <@generalInput 
				         id="addressLine4"
				         label=uiLabelMap.address4
				         placeholder=uiLabelMap.address4
				         value="${postalAddress?if_exists.address4?if_exists}"
				         required=false
				         maxlength="255"
				         />
				         
				         <@generalInput 
				         id="addressLine5"
				         label=uiLabelMap.address5
				         placeholder=uiLabelMap.address5
				         value="${postalAddress?if_exists.address5?if_exists}"
				         required=false
				         maxlength="255"
				         />
				         
						<@dropdownInput 
				         id="countryGeoId"
				         label=uiLabelMap.country
				         options=countryMap
				         required=true
				         value="${postalAddress?if_exists.countryGeoId?if_exists}"
				         allowEmpty=true
				         dataLiveSearch=true
				         />
						<@dropdownInput 
				         id="stateProvinceGeoId"
				         label=uiLabelMap.state
				         required=true
				         value="${postalAddress?if_exists.stateProvinceGeoId?if_exists}"
				         allowEmpty=true
				         dataLiveSearch=true
				         />
				        
				        <#-- 
				        <@dropdownInput 
				         id="city"
				         label=uiLabelMap.city
				         required=true
				         value="${postalAddress?if_exists.city?if_exists}"
				         allowEmpty=true
				         dataLiveSearch=true
				         /> 
				         -->
				         <@generalInput 
				         id="city"
				         label=uiLabelMap.city
				         placeholder=uiLabelMap.city
				         value="${postalAddress?if_exists.city?if_exists}"
				         required=true
				         maxlength="255"
				         />
				           
						<@generalInput 
				         id="postalCode"
				         label=uiLabelMap.postalCode
				         placeholder=uiLabelMap.postalCode
				         value="${postalAddress?if_exists.postalCode?if_exists}"
				         required=false
				         dataError="Should accept 6 digits and numbers only"
				         pattern="^\\d{6}$"
				         />
				         
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="billingAddressModal-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary"
						data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<#if nominatedAccount?exists>
<div id="nominatedAccountModalView" class="modal fade" role="dialog" data-parentForm="">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nominated Account </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="col-12 bg-light mb-1">
					<strong>Nomination</strong>
				</div>
				<div class="clearfix"></div>
				
				<form id="nominatedAccountModalForm" role="form" class="form-horizontal" method="post" data-toggle="validator">
				
				<div class="row padding-r">
					<div class="col-md-8 col-sm-8 form-horizontal">
						
						<@dropdownInput 
				         id="nominatedAccountId"
				         label=uiLabelMap.nominatedAccount
				         options=accountList
				         required=true
				         value="${nominatedAccount?if_exists.nominatedAccountId?if_exists}"
				         allowEmpty=true
				         dataLiveSearch=true
				         />
				         
				        <@dateInput 
						id="nominatedAccountFromDate"
						label=uiLabelMap.fromDate
						value="${nominatedAccount?if_exists.fromDate?if_exists}"
						disablePastDate=false
						required=true
						/>
						
						<@dateInput 
						id="nominatedAccountThruDate"
						label=uiLabelMap.thruDate
						value="${nominatedAccount?if_exists.thruDate?if_exists}"
						disablePastDate=false
						required=false
						/>  
							
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-primary" id="nominatedAccountModal-apply-btn">Apply</button>
					<button type="button" class="btn btn-sm btn-primary"
						data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</#if>

<script>

jQuery(document).ready(function() {	

$("#countryGeoId").change(function() {
	loadStateList();
});

$('#billingAddressModalView').on('shown.bs.modal', function (e) {
	loadStateList();	   
});

$('#billingAddressModalForm').validator().on('submit', function (e) {
	var parentForm = $('#billingAddressModalView').attr("data-parentForm");
	
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
		  
  		$('#'+parentForm+' input[name="generalAttnName"]').val( $('#generalAttnName').val() );
		$('#'+parentForm+' input[name="addressLine1"]').val( $('#addressLine1').val() );
		$('#'+parentForm+' input[name="addressLine2"]').val( $('#addressLine2').val() );
		$('#'+parentForm+' input[name="addressLine3"]').val( $('#addressLine3').val() );
		$('#'+parentForm+' input[name="addressLine4"]').val( $('#addressLine4').val() );
		$('#'+parentForm+' input[name="addressLine5"]').val( $('#addressLine5').val() );
		$('#'+parentForm+' input[name="countryGeoId"]').val( $('#countryGeoId').val() );
		$('#'+parentForm+' input[name="stateProvinceGeoId"]').val( $('#stateProvinceGeoId').val() );
		$('#'+parentForm+' input[name="city"]').val( $('#city').val() );
		$('#'+parentForm+' input[name="postalCode"]').val( $('#postalCode').val() );
	
		$('#billingAddressModalView').modal("hide");
	
    	// everything looks good!
  	}
});

$('#nominatedAccountModalForm').validator().on('submit', function (e) {
	var parentForm = $('#nominatedAccountModalView').attr("data-parentForm");
	
	if (e.isDefaultPrevented()) {
  		
    	// handle the invalid form...
  	} else {
  	
  		e.preventDefault();
		  
  		$('#'+parentForm+' input[name="nominatedAccountId"]').val( $('#nominatedAccountId').val() );
		$('#'+parentForm+' input[name="nominatedAccountFromDate"]').val( $('#nominatedAccountFromDate').val() );
		$('#'+parentForm+' input[name="nominatedAccountThruDate"]').val( $('#nominatedAccountThruDate').val() );
	
		$('#nominatedAccountModalView').modal("hide");
	
    	// everything looks good!
  	}
});

});

function loadStateList() {
	
	var groupNameOptions = '';
	
	if ( $("#countryGeoId").val() ) {
		$('#stateProvinceGeoId').dropdown('clear');
		$.ajax({
			      
			type: "POST",
	     	url: "getGeoAssocList",
	        data:  {"geoId": $("#countryGeoId").val(), "geoAssocTypeId": "REGIONS"},
	        async: false,
	        success: function (data) {   
	            
	            if (data.code == 200) {
	            
	            	for (var i = 0; i < data.results.length; i++) {
	            		var result = data.results[i];
	            		groupNameOptions += '<option value="'+result.geoIdTo+'">'+result.geoName+'</option>';
	            	}
	            	
	            }
				    	
	        }
	        
		});    
		
		$("#stateProvinceGeoId").html( groupNameOptions );
		
		<#if postalAddress.stateProvinceGeoId?has_content>
		$("#stateProvinceGeoId").val( "${postalAddress.stateProvinceGeoId!}" );
		</#if>
		
		$("#stateProvinceGeoId").dropdown('refresh');
		
	}
		
}

</script>