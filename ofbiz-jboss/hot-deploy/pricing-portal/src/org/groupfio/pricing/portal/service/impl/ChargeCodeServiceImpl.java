/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class ChargeCodeServiceImpl {

	private static final String MODULE = ChargeCodeServiceImpl.class.getName();
    
	@SuppressWarnings("resource")
	public static String createTransAttrChargeCode(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String serviceCode = request.getParameter("serviceCode");
		String chargeCode = request.getParameter("chargeCode");
		String description = request.getParameter("description");
		String fromDate = request.getParameter("fromDate");
		String thruDate = request.getParameter("thruDate");
		
		String transactionProductAssocId = null;
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			String productId = null;
				if (UtilValidate.isNotEmpty(chargeCode)) {
					productId = chargeCode;
				} else {
					productId = serviceCode;
				}
    			
    			GenericValue code = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssoc", UtilMisc.toMap("productId", productId), null, false) );
    			if (UtilValidate.isNotEmpty(code)) {
					request.setAttribute("_ERROR_MESSAGE_", "Transaction Attribute - Charge Code Mapping already exists!");
		    		return "error";
				}
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					if (UtilValidate.isEmpty(code)) {
							code = delegator.makeValue("TransactionProductAssoc");
    						code.put("transactionProductAssocId", delegator.getNextSeqId("TransactionProductAssoc"));
    						
    						GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productId), null, false) );
    						
    						code.put("productId", productId);
    						code.put("productTypeId", product.getString("productTypeId"));
    						code.put("description", description);

    						if (UtilValidate.isNotEmpty(fromDate)) {
    							code.put("fromDate", UtilDateTime.stringToTimeStamp(fromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
    						}
    						if (UtilValidate.isNotEmpty(thruDate)) {
    							code.put("thruDate", UtilDateTime.stringToTimeStamp(thruDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
    						}
    						
    						code.create();
    						transactionProductAssocId = code.getString("transactionProductAssocId");
						}
    					
    					GenericValue codeValue = delegator.makeValue("TransactionProductAssocValue");
    					codeValue.put("transactionProductAssocId", code.getString("transactionProductAssocId"));
    					codeValue.put("attrName", attrName);
    					codeValue.put("attrValue", request.getParameter(key));
    					
    					codeValue.create();
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
		
    	request.setAttribute("transactionProductAssocId", transactionProductAssocId);
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully mapped Transaction Attribute and Charge Code..");
    	
		return "success";
	}
	
	@SuppressWarnings("resource")
	public static String updateTransAttrChargeCode(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String transactionProductAssocId = request.getParameter("transactionProductAssocId");
		
		String serviceCode = request.getParameter("serviceCode");
		String chargeCode = request.getParameter("chargeCode");
		String description = request.getParameter("description");
		String fromDate = request.getParameter("fromDate");
		String thruDate = request.getParameter("thruDate");
		
		request.setAttribute("transactionProductAssocId", transactionProductAssocId);
		
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			String productId = null;
				if (UtilValidate.isNotEmpty(chargeCode)) {
					productId = chargeCode;
				} else {
					productId = serviceCode;
				}
    			
    			GenericValue code = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssoc", UtilMisc.toMap("transactionProductAssocId", transactionProductAssocId), null, false) );
    			if (UtilValidate.isEmpty(code)) {
					request.setAttribute("_ERROR_MESSAGE_", "Transaction Attribute - Charge Code Mapping not exists!");
		    		return "error";
				}
    			
    			GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productId), null, false) );
				
				code.put("productId", productId);
				code.put("productTypeId", product.getString("productTypeId"));
				code.put("description", description);

				if (UtilValidate.isNotEmpty(fromDate)) {
					code.put("fromDate", UtilDateTime.stringToTimeStamp(fromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				}
				if (UtilValidate.isNotEmpty(thruDate)) {
					code.put("thruDate", UtilDateTime.stringToTimeStamp(thruDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				}
				
				code.store();
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					GenericValue codeValue = delegator.makeValue("TransactionProductAssocValue");
    					codeValue.put("transactionProductAssocId", code.getString("transactionProductAssocId"));
    					codeValue.put("attrName", attrName);
    					codeValue.put("attrValue", request.getParameter(key));
    					
    					GenericValue assocValue = EntityUtil.getFirst( delegator.findByAnd("TransactionProductAssocValue", UtilMisc.toMap("transactionProductAssocId", code.getString("transactionProductAssocId"), "attrName", attrName), null, false) );
    					if (UtilValidate.isNotEmpty(assocValue)) {
    						codeValue.store();
    					} else {
    						codeValue.create();
    					}
    					
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
    	
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully Updated Transaction Attribute and Charge Code Mapping..");
    	
		return "success";
	}
	
    public static Map deleteTransAttrChargeCode(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("SampleTable",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Custom field group not exists!"));
    			return result;
    		}
    		
    		group.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Custom field group.."));
    	
    	return result;
    	
    }
    
    public static Map createChargeCode(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String chargeCode = (String) context.get("chargeCode");
    	String chargeType = (String) context.get("chargeType");
    	String debitSystemType = (String) context.get("debitSystemType");
    	String description = (String) context.get("description");
    	String serviceCode = (String) context.get("serviceCode");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String productId = null;
    	try {
        	
    		GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productIdAlt", chargeCode), null, false) );
    		
    		if (UtilValidate.isNotEmpty(product)) {
    			result.putAll(ServiceUtil.returnError("Charge Code already exists!"));
    			return result;
    		}
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
    		
    		Map<String, Object> productCreateContext = new HashMap<String, Object>();
    		
    		productCreateContext.put("productIdAlt", chargeCode);
    		productCreateContext.put("productTypeId", "DIGITAL_GOOD");
    		
    		productCreateContext.put("internalName", description);
    		productCreateContext.put("productName", description);
    		productCreateContext.put("description", description);
    		
    		productCreateContext.put("introductionDate", UtilDateTime.nowTimestamp());
    		productCreateContext.put("releaseDate", UtilDateTime.nowTimestamp());
    		
    		productCreateContext.put("userLogin", userLogin);
    		
    		Map<String, Object> productCreateResult = dispatcher.runSync("createProduct", productCreateContext);
    		
    		if (ServiceUtil.isSuccess(productCreateResult)) {
    			productId = ParamUtil.getString(productCreateResult, "productId");
    			
    			ProductUtil.createOrUpdateProductContent(delegator, userLogin, productId, ((String) context.get("description")), "DESCRIPTION");
    			
    			if (UtilValidate.isNotEmpty(chargeType)) {
    				callCtxt = FastMap.newInstance();
    				callCtxt.put("productId", productId);
    				callCtxt.put("attrType", "CHARGE_CODE");
    				callCtxt.put("attrName", "CHARGE_TYPE");
    				callCtxt.put("attrValue", chargeType);
    				callCtxt.put("userLogin", userLogin);
    				callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    			}
				
				if (UtilValidate.isNotEmpty(debitSystemType)) {
					callCtxt = FastMap.newInstance();
					callCtxt.put("productId", productId);
					callCtxt.put("attrType", "CHARGE_CODE");
					callCtxt.put("attrName", "DEBIT_SYSTEM_TYPE");
					callCtxt.put("attrValue", debitSystemType);
					callCtxt.put("userLogin", userLogin);
					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    			}
				
				if (UtilValidate.isNotEmpty(serviceCode)) {
					GenericValue serviceProduct = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", serviceCode), null, false) );
					if (UtilValidate.isNotEmpty(serviceProduct)) {
						callCtxt = FastMap.newInstance();
						callCtxt.put("productId", productId);
						callCtxt.put("attrType", "CHARGE_CODE");
						callCtxt.put("attrName", "SERVICE_CODE");
						callCtxt.put("attrValue", serviceProduct.get("productIdAlt"));
						callCtxt.put("userLogin", userLogin);
						callResult = dispatcher.runSync("createProductAttribute", callCtxt);
						
						callCtxt = FastMap.newInstance();
						callCtxt.put("productId", productId);
						callCtxt.put("productIdTo", serviceCode);
						callCtxt.put("productAssocTypeId", "PRODUCT_COMPONENT");
						callCtxt.put("fromDate", UtilDateTime.nowTimestamp());
						callCtxt.put("userLogin", userLogin);

						callResult = dispatcher.runSync("createProductAssoc", callCtxt);
					}
					
    			}
				
    		}
    		
    		result.put("productId", productId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Charge Code.."));
    	
    	return result;
    	
    }
    
    public static Map updateChargeCode(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String productId = (String) context.get("productId");
    	
    	String chargeCode = (String) context.get("chargeCode");
    	String chargeType = (String) context.get("chargeType");
    	String debitSystemType = (String) context.get("debitSystemType");
    	String description = (String) context.get("description");
    	String serviceCode = (String) context.get("serviceCode");
    	
    	String invoiceRequired = (String) context.get("invoiceRequired");
    	String printFlag = (String) context.get("printFlag");
    	String priceFreq = (String) context.get("priceFreq");
    	String pricingDay = (String) context.get("pricingDay");
    	String billAddressLevel = (String) context.get("billAddressLevel");
    	String invFreq = (String) context.get("invFreq");
    	String invDay = (String) context.get("invDay");
    	String currencyType = (String) context.get("currencyType");
    	String bundleCurrency = (String) context.get("bundleCurrency");
    	
    	String glAccountId = (String) context.get("glAccountId");
    	String glCurrency = (String) context.get("glCurrency");
    	String pcCode = (String) context.get("pcCode");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	try {
        	
    		GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productId), null, false) );
    		
    		if (UtilValidate.isEmpty(product)) {
    			result.putAll(ServiceUtil.returnError("Charge Code not exists!"));
    			return result;
    		}
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
    		
    		Map<String, Object> productCreateContext = new HashMap<String, Object>();
    		
    		productCreateContext.put("productId", productId);
    		
    		productCreateContext.put("productIdAlt", chargeCode);
    		productCreateContext.put("productTypeId", "DIGITAL_GOOD");
    		
    		productCreateContext.put("internalName", description);
    		productCreateContext.put("productName", description);
    		productCreateContext.put("description", description);
    		
    		productCreateContext.put("introductionDate", UtilDateTime.nowTimestamp());
    		productCreateContext.put("releaseDate", UtilDateTime.nowTimestamp());
    		
    		productCreateContext.put("userLogin", userLogin);
    		
    		Map<String, Object> productCreateResult = dispatcher.runSync("updateProduct", productCreateContext);
    		
    		if (ServiceUtil.isSuccess(productCreateResult)) {
    			
    			ProductUtil.createOrUpdateProductContent(delegator, userLogin, productId, ((String) context.get("description")), "DESCRIPTION");
    			
    			ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "CHARGE_TYPE", chargeType);
				
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "DEBIT_SYSTEM_TYPE", debitSystemType);
				
				if (UtilValidate.isNotEmpty(serviceCode)) {
					GenericValue serviceProduct = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", serviceCode), null, false) );
					if (UtilValidate.isNotEmpty(serviceProduct)) {
						
	    				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "SERVICE_CODE", serviceProduct.getString("productIdAlt"));
						
	    				EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
	    						EntityCondition.makeCondition("productId", EntityOperator.EQUALS, productId),
	    						EntityCondition.makeCondition("productIdTo", EntityOperator.EQUALS, serviceCode),
	    						EntityCondition.makeCondition("productAssocTypeId", EntityOperator.EQUALS, "PRODUCT_COMPONENT"),
	    						EntityUtil.getFilterByDateExpr()
	    					);
	    				GenericValue productAssoc = EntityUtil.getFirst(delegator.findList("ProductAssoc", condition, null, null, null, false));
	    				if (UtilValidate.isEmpty(productAssoc)) {
	    					callCtxt = FastMap.newInstance();
							callCtxt.put("productId", productId);
							callCtxt.put("productIdTo", serviceCode);
							callCtxt.put("productAssocTypeId", "PRODUCT_COMPONENT");
							callCtxt.put("fromDate", UtilDateTime.nowTimestamp());
							callCtxt.put("userLogin", userLogin);

							callResult = dispatcher.runSync("createProductAssoc", callCtxt);
	    				}
						
					}
					
    			}
				
				// Charge code detail [start]
				
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "INVOICE_REQUIRED", invoiceRequired);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "PRINT_FLAG", printFlag);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "PRICE_FREQ", priceFreq);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "PRICING_DAY", pricingDay);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "BILL_ADDRESS_LEVEL", billAddressLevel);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "INV_FREQ", invFreq);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "INV_DAY", invDay);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "CURRENCY_TYPE", currencyType);
				ProductUtil.createOrUpdateProductAttribute(delegator, dispatcher, userLogin, productId, "CHARGE_CODE", "BUNDLE_CURRENCY_TYPE", bundleCurrency);
				
				// Charge code detail [end]
				
				if (UtilValidate.isNotEmpty(glAccountId)) {
					
					EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
    						EntityCondition.makeCondition("productId", EntityOperator.EQUALS, productId),
    						//EntityCondition.makeCondition("glAccountId", EntityOperator.EQUALS, glAccountId),
    						EntityUtil.getFilterByDateExpr()
    					);
					GenericValue productGlAssoc = EntityUtil.getFirst(delegator.findList("ProductGlAccountAssoc", condition, null, null, null, false));
	    			
	    			Map<String, Object> inputContext = new HashMap<String, Object>();
	    			inputContext.put("productId", productId);
	    			inputContext.put("glAccountId", glAccountId);
	    			inputContext.put("pcCode", pcCode);
	    			inputContext.put("glCurrency", glCurrency);
	    			
	        		if (UtilValidate.isEmpty(productGlAssoc)) {
	        			productGlAssoc = delegator.makeValue("ProductGlAccountAssoc");
	        			inputContext.put("fromDate", UtilDateTime.nowTimestamp());
	        			productGlAssoc.putAll(inputContext);
	        			productGlAssoc.create();
	        		} else if (!glAccountId.equals(productGlAssoc.getString("glAccountId"))) {
	        			productGlAssoc.put("thruDate", UtilDateTime.nowTimestamp());
	        			productGlAssoc.store();
	        			
	        			productGlAssoc = delegator.makeValue("ProductGlAccountAssoc");
	        			inputContext.put("fromDate", UtilDateTime.nowTimestamp());
	        			productGlAssoc.putAll(inputContext);
	        			productGlAssoc.create();
	        		} else {
	        			productGlAssoc.putAll(inputContext);
	        			productGlAssoc.store();
	        		}
	        		
	    		}
				
    		}
    		
    		result.put("productId", productId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Charge Code.."));
    	
    	return result;
    	
    }
    
}
