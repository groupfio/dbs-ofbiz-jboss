/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class ProductHierarchyMaintenanceServiceImpl {

	private static final String MODULE = ProductHierarchyMaintenanceServiceImpl.class.getName();
    
    public static Map createProductHierarchyMaintenance(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String serviceCode = (String) context.get("serviceCode");
    	String serviceType = (String) context.get("serviceType");
    	String currencyType = (String) context.get("currencyType");
    	String description = (String) context.get("description");
    	String bundleCurrency = (String) context.get("bundleCurrency");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String productId = null;
    	try {
        	
    		GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productIdAlt", serviceCode), null, false) );
    		
    		if (UtilValidate.isNotEmpty(product)) {
    			result.putAll(ServiceUtil.returnError("Product Hierarchy already exists!"));
    			return result;
    		}
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
    		
    		Map<String, Object> productCreateContext = new HashMap<String, Object>();
    		
    		productCreateContext.put("productIdAlt", serviceCode);
    		productCreateContext.put("productTypeId", "SERVICE");
    		
    		productCreateContext.put("internalName", description);
    		productCreateContext.put("productName", description);
    		productCreateContext.put("description", description);
    		
    		productCreateContext.put("introductionDate", UtilDateTime.nowTimestamp());
    		productCreateContext.put("releaseDate", UtilDateTime.nowTimestamp());
    		
    		productCreateContext.put("userLogin", userLogin);
    		
    		Map<String, Object> productCreateResult = dispatcher.runSync("createProduct", productCreateContext);
    		
    		if (ServiceUtil.isSuccess(productCreateResult)) {
    			productId = ParamUtil.getString(productCreateResult, "productId");
    			
    			ProductUtil.createOrUpdateProductContent(delegator, userLogin, productId, ((String) context.get("description")), "DESCRIPTION");
    			
    			if (UtilValidate.isNotEmpty(serviceType)) {
    				callCtxt = FastMap.newInstance();
    				callCtxt.put("productId", productId);
    				callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
    				callCtxt.put("attrName", "SERVICE_TYPE");
    				callCtxt.put("attrValue", serviceType);
    				callCtxt.put("userLogin", userLogin);
    				callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    			}
				
				if (UtilValidate.isNotEmpty(currencyType)) {
					callCtxt = FastMap.newInstance();
					callCtxt.put("productId", productId);
					callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
					callCtxt.put("attrName", "CURRENCY_TYPE");
					callCtxt.put("attrValue", currencyType);
					callCtxt.put("userLogin", userLogin);
					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    			}
				
				if (UtilValidate.isNotEmpty(bundleCurrency)) {
					callCtxt = FastMap.newInstance();
					callCtxt.put("productId", productId);
					callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
					callCtxt.put("attrName", "BUNDLE_CURRENCY_TYPE");
					callCtxt.put("attrValue", bundleCurrency);
					callCtxt.put("userLogin", userLogin);
					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    			}
				
    		}
    		
    		result.put("productId", productId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Product Hierarchy.."));
    	
    	return result;
    	
    }
    
    public static Map updateProductHierarchyMaintenance(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String productId = (String) context.get("productId");
    	
    	String serviceCode = (String) context.get("serviceCode");
    	String serviceType = (String) context.get("serviceType");
    	String currencyType = (String) context.get("currencyType");
    	String description = (String) context.get("description");
    	String bundleCurrency = (String) context.get("bundleCurrency");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue product = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productId), null, false) );
    		
    		if (UtilValidate.isEmpty(product)) {
    			result.putAll(ServiceUtil.returnError("Product Hierarchy not exists!"));
    			return result;
    		}
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
    		
    		Map<String, Object> productCreateContext = new HashMap<String, Object>();
    		
    		productCreateContext.put("productId", productId);
    		productCreateContext.put("productIdAlt", serviceCode);
    		productCreateContext.put("productTypeId", "SERVICE");
    		
    		productCreateContext.put("internalName", description);
    		productCreateContext.put("productName", description);
    		productCreateContext.put("description", description);
    		
    		productCreateContext.put("userLogin", userLogin);
    		
    		Map<String, Object> productCreateResult = dispatcher.runSync("updateProduct", productCreateContext);
    		
    		if (ServiceUtil.isSuccess(productCreateResult)) {
    			
    			ProductUtil.createOrUpdateProductContent(delegator, userLogin, productId, ((String) context.get("description")), "DESCRIPTION");
    			
    			if (UtilValidate.isNotEmpty(serviceType)) {
    				callCtxt = FastMap.newInstance();
    				callCtxt.put("productId", productId);
    				callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
    				callCtxt.put("attrName", "SERVICE_TYPE");
    				callCtxt.put("attrValue", serviceType);
    				callCtxt.put("userLogin", userLogin);
    				GenericValue attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "SERVICE_TYPE"), null, false) );
    				if (UtilValidate.isNotEmpty(attrInstance)) {
    					callResult = dispatcher.runSync("updateProductAttribute", callCtxt);
    				} else {
    					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    				}
    			}
				
				if (UtilValidate.isNotEmpty(currencyType)) {
					callCtxt = FastMap.newInstance();
					callCtxt.put("productId", productId);
					callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
					callCtxt.put("attrName", "CURRENCY_TYPE");
					callCtxt.put("attrValue", currencyType);
					callCtxt.put("userLogin", userLogin);
					GenericValue attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "CURRENCY_TYPE"), null, false) );
					if (UtilValidate.isNotEmpty(attrInstance)) {
    					callResult = dispatcher.runSync("updateProductAttribute", callCtxt);
    				} else {
    					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    				}
    			}
				
				if (UtilValidate.isNotEmpty(bundleCurrency)) {
					callCtxt = FastMap.newInstance();
					callCtxt.put("productId", productId);
					callCtxt.put("attrType", "PRODUCT_HIERARCHY_MAINTENANCE");
					callCtxt.put("attrName", "BUNDLE_CURRENCY_TYPE");
					callCtxt.put("attrValue", bundleCurrency);
					callCtxt.put("userLogin", userLogin);
					GenericValue attrInstance = EntityUtil.getFirst( delegator.findByAnd("ProductAttribute", UtilMisc.toMap("productId", product.getString("productId"), "attrType", "PRODUCT_HIERARCHY_MAINTENANCE", "attrName", "BUNDLE_CURRENCY_TYPE"), null, false) );
					if (UtilValidate.isNotEmpty(attrInstance)) {
    					callResult = dispatcher.runSync("updateProductAttribute", callCtxt);
    				} else {
    					callResult = dispatcher.runSync("createProductAttribute", callCtxt);
    				}
    			}
    		}
    		
    		result.put("productId", productId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Product Hierarchy.."));
    	
    	return result;
    	
    }
    
    public static Map deleteProductHierarchyMaintenance(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("SampleTable",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Custom field group not exists!"));
    			return result;
    		}
    		
    		group.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Custom field group.."));
    	
    	return result;
    	
    }
    
}
