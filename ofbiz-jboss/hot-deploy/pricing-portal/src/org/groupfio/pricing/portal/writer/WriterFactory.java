/**
 * 
 */
package org.groupfio.pricing.portal.writer;

/**
 * @author Sharif
 *
 */
public final class WriterFactory {

	private static final LogWriter LOG_WRITER = new LogWriter();
	
	public static LogWriter getLogWriter () {
		return LOG_WRITER;
	}
	
}
