/**
 * 
 */
package org.groupfio.pricing.portal.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.homeapps.util.ParamUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.party.party.PartyHelper;

import javolution.util.FastList;

/**
 * @author Sharif
 *
 */
public class DataHelper {
	
	private static String MODULE = DataHelper.class.getName();
	
	public static GenericValue getProductPriceList(Delegator delegator, Map<String, String> filter){
		
		GenericValue productPriceList = null;
		
		try {
			
			String parentId = (String) filter.get("parentId");
			String custPartyId = (String) filter.get("custPartyId");
			String accountId = (String) filter.get("accountId");
			String priceListLevel = (String) filter.get("priceListLevel");
			
			List conditionList = FastList.newInstance();
			
			if (UtilValidate.isNotEmpty(parentId)) {
				conditionList.add(EntityCondition.makeCondition("parentId", EntityOperator.EQUALS, parentId));
			}
			if (UtilValidate.isNotEmpty(custPartyId)) {
				conditionList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, custPartyId));
			}
			if (UtilValidate.isNotEmpty(accountId)) {
				conditionList.add(EntityCondition.makeCondition("accountId", EntityOperator.EQUALS, accountId));
			}
			if (UtilValidate.isNotEmpty(priceListLevel)) {
				conditionList.add(EntityCondition.makeCondition("priceListLevel", EntityOperator.EQUALS, priceListLevel));
			}
			
			conditionList.add(EntityUtil.getFilterByDateExpr());
			
			EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
			productPriceList = EntityUtil.getFirst( delegator.findList("ProductListAndDetailSummary", mainConditons, null, UtilMisc.toList("productPriceListId"), null, false) );
			
			int i = 0;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return productPriceList;
	}
}
