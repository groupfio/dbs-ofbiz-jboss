/*
 * Copyright (c) Open Source Strategies, Inc.
 *
 * Opentaps is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Opentaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Opentaps.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.groupfio.pricing.portal.event;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.groupfio.homeapps.ResponseCodes;
import org.groupfio.homeapps.constants.GlobalConstants;
import org.groupfio.pricing.portal.constants.RTConstants;
import org.groupfio.pricing.portal.util.DataHelper;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.LocalDispatcher;

import javolution.util.FastList;

/**
 * Utility class for making Ajax JSON responses.
 * @author Sharif Ul Islam
 */
public final class AjaxEvents {

    private AjaxEvents() { }

    private static final String MODULE = AjaxEvents.class.getName();

    public static String doJSONResponse(HttpServletResponse response, JSONObject jsonObject) {
        return doJSONResponse(response, jsonObject.toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Collection<?> collection) {
        return doJSONResponse(response, JSONArray.fromObject(collection).toString());
    }

    public static String doJSONResponse(HttpServletResponse response, Map map) {
        return doJSONResponse(response, JSONObject.fromObject(map));
    }

    public static String doJSONResponse(HttpServletResponse response, String jsonString) {
        String result = "success";

        response.setContentType("application/x-json");
        try {
            response.setContentLength(jsonString.getBytes("UTF-8").length);
        } catch (UnsupportedEncodingException e) {
            Debug.logWarning("Could not get the UTF-8 json string due to UnsupportedEncodingException: " + e.getMessage(), MODULE);
            response.setContentLength(jsonString.length());
        }

        Writer out;
        try {
            out = response.getWriter();
            out.write(jsonString);
            out.flush();
        } catch (IOException e) {
            Debug.logError(e, "Failed to get response writer", MODULE);
            result = "error";
        }
        return result;
    }
    
    public static GenericValue getUserLogin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (GenericValue) session.getAttribute("userLogin");
    }

    /*************************************************************************/
    /**                                                                     **/
    /**                      Common JSON Requests                           **/
    /**                                                                     **/
    /*************************************************************************/
    
    @SuppressWarnings("unchecked")
	public static String getGeoAssocList(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String geoId = request.getParameter("geoId");
		String geoAssocTypeId = request.getParameter("geoAssocTypeId");
		Timestamp statusDate = UtilDateTime.nowTimestamp();

		Map<String, Object> resp = new HashMap<String, Object>();

		try {
			
			if (UtilValidate.isNotEmpty(geoId) && UtilValidate.isNotEmpty(geoAssocTypeId)) {
				
				List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
				
				List<EntityCondition> conditions = new ArrayList <EntityCondition>();
				
				conditions.add( EntityCondition.makeCondition(EntityOperator.AND,
						EntityCondition.makeCondition("geoId", EntityOperator.EQUALS, geoId),
						EntityCondition.makeCondition("geoAssocTypeId", EntityOperator.EQUALS, geoAssocTypeId)
						)
						);
				
				EntityCondition mainConditons = EntityCondition.makeCondition(conditions, EntityOperator.AND);
				List<GenericValue> geoAssocList = delegator.findList("GeoAssocSummary", mainConditons, null, null, null, false);
				if (UtilValidate.isNotEmpty(geoAssocList)) {
					for (GenericValue geoAssoc : geoAssocList) {
						
						Map<String, Object> result = new HashMap<String, Object>();
						
						result.put("geoId", geoAssoc.getString("geoId"));
						result.put("geoIdTo", geoAssoc.getString("geoIdTo"));
						result.put("geoName", geoAssoc.getString("geoName"));
						result.put("geoAssocTypeId", geoAssoc.getString("geoAssocTypeId"));
						
						results.add(result);
					}
				}
				
				resp.put("results", results);

				resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
				
			} else {
				resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
	            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Provide required parameters as geoId, geoAssocTypeId..");
			}
			
		} catch (Exception e) {
			
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String removeProfileConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String profileConfigurationId = request.getParameter("profileConfigurationId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(profileConfigurationId)) {
				
				GenericValue profileConfig = EntityUtil.getFirst( delegator.findByAnd("ProfileConfiguration", UtilMisc.toMap("profileConfigurationId", profileConfigurationId), null, false) );
				
				if (UtilValidate.isNotEmpty(profileConfig)) {
					
					profileConfig.remove();
					
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Successfully removed system config..");
					
				} else {
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Not found system config..");
				}
				
			}
			
		} catch (Exception e) {
			
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String createProfileConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String isCompleteReset = request.getParameter("isCompleteReset");

		String profileConfigurationId = request.getParameter("profileConfigurationId");
		String profileType = request.getParameter("profileType");
		String profileCode = request.getParameter("profileCode");
		String profileDescription = request.getParameter("profileDescription");
		
		String sequenceNumber = request.getParameter("sequence");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(profileType) && UtilValidate.isNotEmpty(profileCode) && UtilValidate.isNotEmpty(profileDescription)) {
				
				EntityCondition mainCond = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("profileConfigurationId", EntityOperator.EQUALS, profileConfigurationId),
						EntityCondition.makeCondition("profileCode", EntityOperator.EQUALS, profileCode)
						);
				
				List<GenericValue> profileConfigList = delegator.findList("ProfileConfiguration", mainCond, null, null, null, false);
				if (UtilValidate.isNotEmpty(profileConfigList)) {
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "System Configuration exists..");
				} else {
					
					GenericValue profileConfig = delegator.makeValue("ProfileConfiguration");
					
					String configId = UtilValidate.isEmpty(profileConfigurationId) ? delegator.getNextSeqId("ProfileConfiguration") : profileConfigurationId;
		    		
					profileConfig.put("profileConfigurationId", configId);
		    		
					profileConfig.put("profileTypeId", profileType);
					profileConfig.put("profileCode", profileCode);
					
					profileConfig.put("profileDescription", profileDescription);
					
					profileConfig.put("sequence", UtilValidate.isNotEmpty(sequenceNumber) ? Long.parseLong(sequenceNumber) : new Long(1));
					
					profileConfig.create();
					
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Successfully added System Configuration..");
					
				}
				
			}
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String getPricingMethodAttributes(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String pricingMethodId = request.getParameter("pricingMethodId");
		String binaryFlag = request.getParameter("binaryFlag");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			List<Map<String, Object>> attrList = new ArrayList<Map<String, Object>>();
			Map<String, Object> pricingMethod = new HashMap();
			
			if (UtilValidate.isNotEmpty(pricingMethodId)) {
				
				List conditionList = FastList.newInstance();
				
				/*EntityCondition mainCond = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("pricingMethodId", EntityOperator.EQUALS, pricingMethodId)
						);*/
				
				conditionList.add(EntityCondition.makeCondition("hide", EntityOperator.EQUALS, "N"));
				if (UtilValidate.isNotEmpty(binaryFlag)) {
					conditionList.add(EntityCondition.makeCondition("binaryFlag", EntityOperator.EQUALS, binaryFlag));
				}
				
				EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
				List<GenericValue> methodAttrList = delegator.findList("PricingMethodAttribute", mainConditons, null, UtilMisc.toList("pricingMethodAttributeId"), null, false);
				
				if (UtilValidate.isNotEmpty(methodAttrList)) {
					for (GenericValue methodAttr : methodAttrList) {
						
						Map<String, Object> attr = methodAttr.getAllFields(); 
						String attrValue = "";
						String attrValueDesc = "";
						String attrName = methodAttr.getString("attrName");
						
						GenericValue methodValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("pricingMethodId", pricingMethodId, "attrName", attrName), null, false) );
						if (UtilValidate.isNotEmpty(methodValue)) {
							attrValue = methodValue.getString("attrValue");
							attrValueDesc = attrValue;
							if (methodAttr.getString("inputType").equals("DROPDOWN")) {
								GenericValue attributeLov = EntityUtil.getFirst( delegator.findByAnd("PricingMethodAttributeLov", UtilMisc.toMap("pricingMethodAttributeId", methodAttr.getString("pricingMethodAttributeId"), "attrLovName", attrValue), null, false) );
								if (UtilValidate.isNotEmpty(attributeLov)) {
									attrValueDesc = attributeLov.getString("attrLovValue");
								}
							}
						}
						
						attr.put("attrValue", attrValue);
						attr.put("attrValueDesc", attrValueDesc);
						attrList.add(attr);
						
					}
					
					/*for (GenericValue transAttr : methodAttrList) {
						String attrName = transAttr.getString("attrName");
						String attrValue = "";
						GenericValue codeValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("pricingMethodId", pricingMethodId, "attrName", attrName), null, false) );
						if (UtilValidate.isNotEmpty(codeValue)) {
							attrValue = codeValue.getString("attrValue");
						}
						pricingMethod.put(attrName, attrValue);				
					}*/
				}
				
			}
			
			resp.put("methodAttrList", attrList);
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String getPricelistRates(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String productPriceListId = request.getParameter("productPriceListId");
		String custPartyId = request.getParameter("custPartyId");
		String accountId = request.getParameter("accountId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			List<Map<String, Object>> attrList = new ArrayList<Map<String, Object>>();
			Map<String, Object> pricingMethod = new HashMap();
			
			if (UtilValidate.isNotEmpty(productPriceListId)) {
				
				List conditionList = FastList.newInstance();
				
				/*EntityCondition mainCond = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("pricingMethodId", EntityOperator.EQUALS, pricingMethodId)
						);*/
				
				if (UtilValidate.isNotEmpty(accountId)) {
					GenericValue accountPriceList = DataHelper.getProductPriceList(delegator, UtilMisc.toMap("parentId", productPriceListId, "custPartyId", custPartyId, "accountId", accountId, "priceListLevel", "ACCT"));
					if (UtilValidate.isNotEmpty(accountPriceList)) {
						productPriceListId = accountPriceList.getString("productPriceListId");
					}
				}
				
				conditionList.add(EntityCondition.makeCondition("productPriceListId", EntityOperator.EQUALS, productPriceListId));
				
				EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
				List<GenericValue> pricelistRateList = delegator.findList("ProductPriceListDetail", mainConditons, null, UtilMisc.toList("productPriceListId"), null, false);
				
				if (UtilValidate.isNotEmpty(pricelistRateList)) {
					for (GenericValue pricelistRate : pricelistRateList) {
						
						Map<String, Object> rate = pricelistRate.getAllFields(); 
						
						attrList.add(rate);
						
					}
					
				}
				
			}
			
			resp.put("pricelistRateList", attrList);
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
    public static String applyPricelistRateConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String productPriceListId = request.getParameter("productPriceListId");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	
        	Enumeration params = request.getParameterNames();
        	
        	String tierFromPricelist[] = request.getParameterValues("tierFrom");
        	String tierToPricelist[] = request.getParameterValues("tierTo");
        	String minimumChargePricelist[] = request.getParameterValues("minimumCharge");
        	String maximumChargePricelist[] = request.getParameterValues("maximumCharge");
        	String tierRatePricelist[] = request.getParameterValues("tierRate");
        	String rateAppliedOnPricelist[] = request.getParameterValues("rateAppliedOn");
        	String discountRateTypePricelist[] = request.getParameterValues("discountRateType");
        	String discountRatePricelist[] = request.getParameterValues("discountRate");
        	
        	if (UtilValidate.isNotEmpty(productPriceListId)) {
        		
        		GenericValue productPriceList = EntityUtil.getFirst( delegator.findByAnd("ProductPriceList", UtilMisc.toMap("productPriceListId", productPriceListId), null, false) );
        		if (UtilValidate.isEmpty(productPriceList)) {
        			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Product Price List not exists..");
		            return doJSONResponse(response, resp);
        		}
        		
        		GenericValue chargeCode = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productPriceList.getString("productId")), null, false) );
        		
        		delegator.removeByAnd("ProductPriceListDetail", UtilMisc.toMap("productPriceListId", productPriceListId));
        		
        		Long sequenceNumber = new Long(1);
        		for (int i = 0; i < tierFromPricelist.length; i++) {
        		
        			String tierFrom = tierFromPricelist[i];
        			String tierTo = tierToPricelist[i];
        			String minimumCharge = minimumChargePricelist[i];
        			String maximumCharge = maximumChargePricelist[i];
        			String tierRate = tierRatePricelist[i];
        			String rateAppliedOn = rateAppliedOnPricelist[i];
        			String discountRateType = discountRateTypePricelist[i];
        			String discountRate = discountRatePricelist[i];
        			
        			if (UtilValidate.isNotEmpty(tierFrom) && UtilValidate.isNotEmpty(tierTo) && UtilValidate.isNotEmpty(minimumCharge) && UtilValidate.isNotEmpty(maximumCharge) && UtilValidate.isNotEmpty(tierRate)) {
        				
        				GenericValue pricelistDetail = delegator.makeValue("ProductPriceListDetail");
        				
        				pricelistDetail.put("tierFrom", new BigDecimal(tierFrom));
        				pricelistDetail.put("tierTo", new BigDecimal(tierTo));
        				pricelistDetail.put("minimumCharge", new BigDecimal(minimumCharge));
        				pricelistDetail.put("maximumCharge", new BigDecimal(maximumCharge));
        				pricelistDetail.put("tierRate", new BigDecimal(tierRate));
        				
        				pricelistDetail.put("appliedOn", rateAppliedOn);
        				pricelistDetail.put("discountType", discountRateType);
        				pricelistDetail.put("discountRate", UtilValidate.isNotEmpty(discountRate) ? new BigDecimal(discountRate) : null);
        				
        				pricelistDetail.put("productPriceListId", productPriceListId);
        				pricelistDetail.put("sequence", sequenceNumber);
        				pricelistDetail.put("priceRuleListTypeId", "PRICING_STD");
        				
        				pricelistDetail.put("variationCodeId", productPriceList.getString("variationCodeId"));
        				pricelistDetail.put("chargeCodeId", chargeCode.getString("productIdAlt"));
        				pricelistDetail.put("variationCurrency", productPriceList.getString("variationCurrency"));
        				pricelistDetail.put("pricingMethodId", productPriceList.getString("pricingMethodId"));
        				
        				pricelistDetail.create();
        				
        				sequenceNumber++;
        				
        			}
        		}
        	}
        	
        	resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	resp.put(GlobalConstants.RESPONSE_MESSAGE, "Successfully Updated Pricelist Rates!!");
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(GlobalConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
    @SuppressWarnings("unchecked")
    public static String applyCustAccountPricelistRateConfig(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {
        
    	LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
    	Delegator delegator = (Delegator) request.getAttribute("delegator");
        GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
        
        Locale locale = UtilHttp.getLocale(request);
        TimeZone timeZone = UtilHttp.getTimeZone(request);
        HttpSession session = request.getSession(true);
        
        String productPriceListId = request.getParameter("productPriceListId");
        String custPartyId = request.getParameter("custPartyId");
        String accountId = request.getParameter("accountId");
        
        Map<String, Object> resp = new HashMap<String, Object>();
        try {
        	Timestamp nowTimestamp = UtilDateTime.nowTimestamp();
        	Enumeration params = request.getParameterNames();
        	
        	String tierFromPricelist[] = request.getParameterValues("tierFrom");
        	String tierToPricelist[] = request.getParameterValues("tierTo");
        	String minimumChargePricelist[] = request.getParameterValues("minimumCharge");
        	String maximumChargePricelist[] = request.getParameterValues("maximumCharge");
        	String tierRatePricelist[] = request.getParameterValues("tierRate");
        	String rateAppliedOnPricelist[] = request.getParameterValues("rateAppliedOn");
        	String discountRateTypePricelist[] = request.getParameterValues("discountRateType");
        	String discountRatePricelist[] = request.getParameterValues("discountRate");
        	
        	if (UtilValidate.isNotEmpty(productPriceListId)) {
        		
        		GenericValue productPriceList = EntityUtil.getFirst( delegator.findByAnd("ProductPriceList", UtilMisc.toMap("productPriceListId", productPriceListId), null, false) );
        		if (UtilValidate.isEmpty(productPriceList)) {
        			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Product Price List not exists..");
		            return doJSONResponse(response, resp);
        		}
        		
        		GenericValue chargeCode = EntityUtil.getFirst( delegator.findByAnd("Product", UtilMisc.toMap("productId", productPriceList.getString("productId")), null, false) );
        		
				GenericValue accountPriceList = DataHelper.getProductPriceList(delegator, UtilMisc.toMap("custPartyId", custPartyId, "accountId", accountId, "priceListLevel", "ACCT"));
				if (UtilValidate.isNotEmpty(accountPriceList)) {
					delegator.removeByAnd("ProductPriceListDetail", UtilMisc.toMap("productPriceListId", accountPriceList.getString("productPriceListId")));
				} else {
					accountPriceList = delegator.makeValue("ProductPriceList");
					accountPriceList.putAll(productPriceList.getAllFields());
					
					accountPriceList.put("productPriceListId", delegator.getNextSeqId("ProductPriceList"));
					accountPriceList.put("parentId", productPriceListId);
					
					accountPriceList.put("createdStamp", nowTimestamp);
					accountPriceList.put("lastUpdatedStamp", nowTimestamp);
					
					accountPriceList.create();
				}
        		
        		Long sequenceNumber = new Long(1);
        		for (int i = 0; i < tierFromPricelist.length; i++) {
        		
        			String tierFrom = tierFromPricelist[i];
        			String tierTo = tierToPricelist[i];
        			String minimumCharge = minimumChargePricelist[i];
        			String maximumCharge = maximumChargePricelist[i];
        			String tierRate = tierRatePricelist[i];
        			String rateAppliedOn = rateAppliedOnPricelist[i];
        			String discountRateType = discountRateTypePricelist[i];
        			String discountRate = discountRatePricelist[i];
        			
        			if (UtilValidate.isNotEmpty(tierFrom) && UtilValidate.isNotEmpty(tierTo) && UtilValidate.isNotEmpty(minimumCharge) && UtilValidate.isNotEmpty(maximumCharge) && UtilValidate.isNotEmpty(tierRate)) {
        				
        				GenericValue pricelistDetail = delegator.makeValue("ProductPriceListDetail");
        				
        				pricelistDetail.put("tierFrom", new BigDecimal(tierFrom));
        				pricelistDetail.put("tierTo", new BigDecimal(tierTo));
        				pricelistDetail.put("minimumCharge", new BigDecimal(minimumCharge));
        				pricelistDetail.put("maximumCharge", new BigDecimal(maximumCharge));
        				pricelistDetail.put("tierRate", new BigDecimal(tierRate));
        				
        				pricelistDetail.put("appliedOn", rateAppliedOn);
        				pricelistDetail.put("discountType", discountRateType);
        				pricelistDetail.put("discountRate", UtilValidate.isNotEmpty(discountRate) ? new BigDecimal(discountRate) : null);
        				
        				pricelistDetail.put("productPriceListId", accountPriceList.getString("productPriceListId"));
        				pricelistDetail.put("sequence", sequenceNumber);
        				pricelistDetail.put("priceRuleListTypeId", "PRICING_PREF");
        				
        				pricelistDetail.put("variationCodeId", productPriceList.getString("variationCodeId"));
        				pricelistDetail.put("chargeCodeId", chargeCode.getString("productIdAlt"));
        				pricelistDetail.put("variationCurrency", productPriceList.getString("variationCurrency"));
        				pricelistDetail.put("pricingMethodId", productPriceList.getString("pricingMethodId"));
        				
        				pricelistDetail.put("partyId", custPartyId);
        				pricelistDetail.put("accountId", accountId);
        				pricelistDetail.put("priceListLevel", "ACCT");
        				
        				pricelistDetail.create();
        				
        				sequenceNumber++;
        				
        			}
        		}
        	}
        	
        	resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
        	resp.put(GlobalConstants.RESPONSE_MESSAGE, "Successfully Updated Pricelist Rates!!");
        	
        } catch (Exception e) {
        	e.printStackTrace();
            Debug.logError(e.getMessage(), MODULE);
            
            resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(GlobalConstants.RESPONSE_MESSAGE, e.getMessage());
            
            return doJSONResponse(response, resp);
        }
        
        return doJSONResponse(response, resp);
    }
    
    @SuppressWarnings("unchecked")
	public static String removeCurrencyConversion(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);

		String uomId = request.getParameter("uomId");
		String uomIdTo = request.getParameter("uomIdTo");
		String fromDate = request.getParameter("fromDate");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			if (UtilValidate.isNotEmpty(uomId) && UtilValidate.isNotEmpty(uomIdTo) && UtilValidate.isNotEmpty(fromDate)) {
				
				Timestamp fd = UtilDateTime.stringToTimeStamp(fromDate, "dd/MM/yyyy", TimeZone.getDefault(), Locale.getDefault());
				
				GenericValue uomConversionDated = EntityUtil.getFirst( delegator.findByAnd("UomConversionDated", UtilMisc.toMap("uomId", uomId, "uomIdTo", uomIdTo, "fromDate", fd), null, false) );
				
				if (UtilValidate.isNotEmpty(uomConversionDated)) {
					
					uomConversionDated.remove();
					
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Successfully removed currency conversion..");
					
				} else {
					resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Not found currency conversion..");
				}
				
			}
			
		} catch (Exception e) {
			
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
            resp.put(GlobalConstants.RESPONSE_MESSAGE, "Error: "+e.getMessage());
			
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
    @SuppressWarnings("unchecked")
	public static String getCustomerAccounts(HttpServletRequest request, HttpServletResponse response) throws GenericEntityException {

		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");

		Locale locale = UtilHttp.getLocale(request);
		HttpSession session = request.getSession(true);
		
		String partyId = request.getParameter("partyId");

		Map<String, Object> resp = new HashMap<String, Object>();

		try {

			List<Map<String, Object>> accountList = new ArrayList<Map<String, Object>>();
			
			if (UtilValidate.isNotEmpty(partyId)) {
				
				List conditionList = FastList.newInstance();
				
				/*EntityCondition mainCond = EntityCondition.makeCondition(EntityOperator.OR,
						EntityCondition.makeCondition("pricingMethodId", EntityOperator.EQUALS, pricingMethodId)
						);*/
				
				conditionList.add(EntityCondition.makeCondition("custPartyId", EntityOperator.EQUALS, partyId));
				
				EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
				List<GenericValue> accounts = delegator.findList("CustomerAccountAssoc", mainConditons, null, UtilMisc.toList("accountId"), null, false);
				
				if (UtilValidate.isNotEmpty(accounts)) {
					for (GenericValue account : accounts) {
						
						accountList.add(account.getAllFields());
						
					}
					
				}
				
			}
			
			resp.put("accountList", accountList);
			resp.put(GlobalConstants.RESPONSE_CODE, ResponseCodes.SUCCESS_CODE);
			
		} catch (Exception e) {
			Debug.logError(e.getMessage(), MODULE);
		}
		
		return doJSONResponse(response, resp);
	}
    
}
