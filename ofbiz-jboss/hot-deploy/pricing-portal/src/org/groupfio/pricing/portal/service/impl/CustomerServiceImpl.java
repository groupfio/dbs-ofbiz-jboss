/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.homeapps.util.DataUtil;
import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.groupfio.homeapps.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;
import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class CustomerServiceImpl {

	private static final String MODULE = CustomerServiceImpl.class.getName();
    
    public static Map createCustomer(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String customerId = (String) context.get("customerId");
    	String companyName = (String) context.get("companyName");
    	String relationshipManager = (String) context.get("relationshipManager");
    	String openDate = (String) context.get("openDate");
    	//String modeOfPayment = (String) context.get("modeOfPayment");
    	String currencyType = (String) context.get("currencyType");
    	//String parentId = (String) context.get("parentId");
    	String emailAddress = (String) context.get("emailAddress");
    	String billingFrequency = (String) context.get("billingFrequency");
    	String giroConsolidationFrequency = (String) context.get("giroConsolidationFrequency");
    	String giroDebitingFrequency = (String) context.get("giroDebitingFrequency");
    	String giroDebiting = (String) context.get("giroDebiting");
    	
    	String attnTo = (String) context.get("generalAttnName");
    	String address1 = (String) context.get("addressLine1");
    	String address2 = (String) context.get("addressLine2");
    	String address3 = (String) context.get("addressLine3");
    	String address4 = (String) context.get("addressLine4");
    	String address5 = (String) context.get("addressLine5");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String city = (String) context.get("city");
    	String postalCode = (String) context.get("postalCode");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String partyId = null;
    	String source = "EXT_PARTY_ID";
    	try {
        	
    		GenericValue partyIdentification = EntityUtil.getFirst(delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyIdentificationTypeId", source, "idValue", customerId), null, true));
    		
    		if (UtilValidate.isNotEmpty(partyIdentification)) {
    			result.putAll(ServiceUtil.returnError("Customer already exists!"));
    			return result;
    		}
    		
    		Timestamp importTimestamp = UtilDateTime.nowTimestamp();
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
			List<GenericValue> toBeStored = FastList.newInstance();
			
			partyId = delegator.getNextSeqId("Party");
			
			toBeStored.addAll(UtilImport.makePartyWithRolesExt(partyId, "PARTY_GROUP", customerId, UtilMisc.toList("ACCOUNT"), delegator));
	        GenericValue company = delegator.makeValue("PartyGroup", UtilMisc.toMap("partyId", partyId, "groupName", companyName));
	        company.put("currencyUomId", currencyType);
	        toBeStored.add(company);
	        
	        Map<String, Object> partyRelationship = UtilMisc.toMap("partyIdTo", "admin", "roleTypeIdTo", "MANAGER", "partyIdFrom", partyId, "roleTypeIdFrom", "ACCOUNT", "partyRelationshipTypeId", "RESPONSIBLE_FOR", "fromDate", importTimestamp);
	        partyRelationship.put("securityGroupId", "ACCOUNT_OWNER");
	        toBeStored.add(delegator.makeValue("PartyRelationship", partyRelationship));
	        
	        GenericValue partySupplementalData = delegator.makeValue("PartySupplementalData", UtilMisc.toMap("partyId", partyId, "companyName", companyName));
	        
	        if (UtilValidate.isNotEmpty(attnTo)
	        		|| UtilValidate.isNotEmpty(address1)
	        		) {
	        	
	            // associate this as the GENERAL_LOCATION and BILLING_LOCATION
	            GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
	            String postalAddressContactMechId = contactMech.getString("contactMechId");
	            GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, companyName, "", "", attnTo, address1, address2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
	            mainPostalAddress.put("address3", address3);
	            mainPostalAddress.put("address4", address4);
	            mainPostalAddress.put("address5", address5);
	            toBeStored.add(contactMech);
	            toBeStored.add(mainPostalAddress);

	            toBeStored.add(UtilImport.makeContactMechPurpose("GENERAL_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("BILLING_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", postalAddressContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	          	partySupplementalData.set("primaryPostalAddressId", postalAddressContactMechId);
	        }
	        
	        if (!UtilValidate.isEmpty(emailAddress)) {
	            // make the email address
	            GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", emailAddress));
	            String emailContactMechId = emailContactMech.getString("contactMechId");
	            toBeStored.add(emailContactMech);

	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, partyId, importTimestamp, delegator));
	           	partySupplementalData.set("primaryEmailId", emailContactMechId);
	        }
	        
	        if (!UtilValidate.isEmpty(source)) {
	        	GenericValue checkSource = EntityUtil.getFirst( delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyId", partyId), null, false) );
	        	if(UtilValidate.isEmpty(checkSource)) {
	        		toBeStored.add(delegator.makeValue("PartyIdentification", UtilMisc.toMap( "partyId", partyId,"partyIdentificationTypeId", source,"idValue", customerId)));
	        		partySupplementalData.put("source", source);
	        	}
	        }
	        
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_CONS_FREQ", giroConsolidationFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_DBT_FREQ", giroDebitingFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "BILLING_FREQUENCY", billingFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_DEBITING", giroDebiting));
	        
	        prepareSupplementalData(partySupplementalData, context);
	        
	        toBeStored.add(partySupplementalData);
	        
	        delegator.storeAll(toBeStored);
			
    		result.put("partyId", partyId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Customer.."));
    	
    	return result;
    	
    }
    
    public static Map updateCustomer(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String partyId = (String) context.get("partyId");
    	
    	String customerId = (String) context.get("customerId");
    	String companyName = (String) context.get("companyName");
    	String relationshipManager = (String) context.get("relationshipManager");
    	String openDate = (String) context.get("openDate");
    	//String modeOfPayment = (String) context.get("modeOfPayment");
    	String currencyType = (String) context.get("currencyType");
    	//String parentId = (String) context.get("parentId");
    	String emailAddress = (String) context.get("emailAddress");
    	String billingFrequency = (String) context.get("billingFrequency");
    	String giroConsolidationFrequency = (String) context.get("giroConsolidationFrequency");
    	String giroDebitingFrequency = (String) context.get("giroDebitingFrequency");
    	String giroDebiting = (String) context.get("giroDebiting");
    	
    	String attnTo = (String) context.get("generalAttnName");
    	String address1 = (String) context.get("addressLine1");
    	String address2 = (String) context.get("addressLine2");
    	String address3 = (String) context.get("addressLine3");
    	String address4 = (String) context.get("addressLine4");
    	String address5 = (String) context.get("addressLine5");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String city = (String) context.get("city");
    	String postalCode = (String) context.get("postalCode");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String source = "EXT_PARTY_ID";
    	try {
    		
    		List<EntityCondition> accountConditions = new ArrayList<EntityCondition>();
    		EntityCondition accountRoleTypeCondition = EntityCondition.makeCondition(EntityCondition.makeCondition("roleTypeIdFrom", EntityOperator.EQUALS, "ACCOUNT"));
    		accountConditions.add(accountRoleTypeCondition);
    		accountConditions.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId));
    		
			EntityCondition accountPartyStatusCondition = EntityCondition.makeCondition(UtilMisc.toList(EntityCondition.makeCondition("statusId", EntityOperator.NOT_EQUAL, "PARTY_DISABLED"),
			EntityCondition.makeCondition("statusId", EntityOperator.EQUALS, null)), EntityOperator.OR);
			
			accountConditions.add(accountPartyStatusCondition);
			accountConditions.add(EntityUtil.getFilterByDateExpr());
			
			EntityCondition mainConditons = EntityCondition.makeCondition(accountConditions, EntityOperator.AND);
    		
			GenericValue customer = EntityUtil.getFirst(delegator.findList("PartyCommonView", mainConditons, UtilMisc.toSet("partyId", "groupName"), null, null, false));
			if (UtilValidate.isEmpty(customer)) {
				result.putAll(ServiceUtil.returnError("Customer not exists!"));
    			return result;
			}
			
			Timestamp importTimestamp = UtilDateTime.nowTimestamp();
			
			Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
			List<GenericValue> toBeStored = FastList.newInstance();
        	
    		GenericValue partyIdentification = EntityUtil.getFirst(delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyIdentificationTypeId", source, "partyId", partyId), null, false));
    		if (UtilValidate.isNotEmpty(partyIdentification)) {
    			partyIdentification.put("idValue", customerId);
    			toBeStored.add(partyIdentification);
    		}
    		
			GenericValue partyGroup = delegator.findOne("PartyGroup", UtilMisc.toMap("partyId", partyId), false);
			if (UtilValidate.isNotEmpty(partyGroup)) {
				partyGroup.put("groupName", companyName);
				partyGroup.put("currencyUomId", currencyType);
	        	toBeStored.add(partyGroup);
			}
			
			GenericValue partySupplementalData = delegator.findOne("PartySupplementalData", false, UtilMisc.toMap("partyId", partyId));
			
			GenericValue emailPurpose = DataUtil.getActivePartyContactMechPurpose(delegator, partyId, "PRIMARY_EMAIL", null);
         	if(UtilValidate.isNotEmpty(emailPurpose)){
         		GenericValue emailContactMech = delegator.findOne("ContactMech", false, UtilMisc.toMap("contactMechId",emailPurpose.getString("contactMechId")));
             	emailContactMech.put("infoString", emailAddress);
             	toBeStored.add(emailContactMech);
         	} else if(!UtilValidate.isEmpty(emailAddress)){
         		GenericValue emailContactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "EMAIL_ADDRESS", "infoString", emailAddress));
	            String emailContactMechId = emailContactMech.getString("contactMechId");
	            toBeStored.add(emailContactMech);

	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", emailContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_EMAIL", emailContactMech, partyId, importTimestamp, delegator));
	           	partySupplementalData.set("primaryEmailId", emailContactMechId);
         	}
         	
         	GenericValue findPostalContactMech = DataUtil.getActivePartyContactMechPurpose(delegator, partyId, "GENERAL_LOCATION", null);
        	if(UtilValidate.isNotEmpty(findPostalContactMech)){
        		String contactMechId = findPostalContactMech.getString("contactMechId");
        		GenericValue postalContactMech = delegator.findOne("PostalAddress", false, UtilMisc.toMap("contactMechId",contactMechId));
        		postalContactMech.put("toName", companyName);
        		postalContactMech.put("attnName", attnTo);
        		postalContactMech.put("address1", address1);
        		postalContactMech.put("address2", address2);
        		postalContactMech.put("address3", address3);
        		postalContactMech.put("address4", address4);
        		postalContactMech.put("address5", address5);
        		postalContactMech.put("city", city);
        		postalContactMech.put("postalCode", postalCode);
        		postalContactMech.put("countryGeoId", countryGeoId);
        		postalContactMech.put("stateProvinceGeoId", stateProvinceGeoId);
        		toBeStored.add(postalContactMech);	        		
        	} else if (UtilValidate.isNotEmpty(attnTo)
	        		|| UtilValidate.isNotEmpty(address1)) {
        		// associate this as the GENERAL_LOCATION and BILLING_LOCATION
	            GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
	            String postalAddressContactMechId = contactMech.getString("contactMechId");
	            GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, companyName, "", "", attnTo, address1, address2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
	            mainPostalAddress.put("address3", address3);
	            mainPostalAddress.put("address4", address4);
	            mainPostalAddress.put("address5", address5);
	            toBeStored.add(contactMech);
	            toBeStored.add(mainPostalAddress);

	            toBeStored.add(UtilImport.makeContactMechPurpose("GENERAL_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("BILLING_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", postalAddressContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	          	partySupplementalData.set("primaryPostalAddressId", postalAddressContactMechId);
            }
			
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_CONS_FREQ", giroConsolidationFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_DBT_FREQ", giroDebitingFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "BILLING_FREQUENCY", billingFrequency));
	        toBeStored.add(UtilImport.makePartyAttribute(delegator, partyId, "GIRO_DEBITING", giroDebiting));
	        
	        prepareSupplementalData(partySupplementalData, context);
	        
	        toBeStored.add(partySupplementalData);
	        
	        delegator.storeAll(toBeStored);
			
    		result.put("partyId", partyId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Customer.."));
    	
    	return result;
    	
    }
    
    private static void prepareSupplementalData(GenericValue partySupplementalData, Map entry) {
    	
    	partySupplementalData.put("companyName", entry.get("companyName"));
    	partySupplementalData.put("attnTo", entry.get("generalAttnName"));
    	partySupplementalData.put("address1", entry.get("addressLine1"));
        partySupplementalData.put("address2", entry.get("addressLine2"));
        partySupplementalData.put("address3", entry.get("addressLine3"));
        partySupplementalData.put("address4", entry.get("addressLine4"));
        partySupplementalData.put("address5", entry.get("addressLine5"));
        partySupplementalData.put("city", entry.get("city"));
        partySupplementalData.put("stateProvinceGeoId", entry.get("stateProvinceGeoId"));
        partySupplementalData.put("countryGeoId", entry.get("countryGeoId"));
        partySupplementalData.put("postalCode", entry.get("postalCode"));
        partySupplementalData.put("emailAddress", entry.get("emailAddress"));
        partySupplementalData.put("source", entry.get("source"));
    	
    }
    
}
