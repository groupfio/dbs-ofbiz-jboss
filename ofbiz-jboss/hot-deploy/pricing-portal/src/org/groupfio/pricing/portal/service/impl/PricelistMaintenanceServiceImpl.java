/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class PricelistMaintenanceServiceImpl {

	private static final String MODULE = PricelistMaintenanceServiceImpl.class.getName();
    
    public static Map createPricelistMaintenance(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String chargeCode = (String) context.get("chargeCode");
    	String description = (String) context.get("description");
    	String variationCode = (String) context.get("variationCode");
    	String bundleId = (String) context.get("bundleId");
    	String pricingMethodId = (String) context.get("pricingMethodId");
    	String variationCurrency = (String) context.get("variationCurrency");
    	String fromDate = (String) context.get("fromDate");
    	String thruDate = (String) context.get("thruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String productId = chargeCode;
    	try {
        	
    		EntityCondition condition = EntityCondition.makeCondition(EntityOperator.AND,
					EntityCondition.makeCondition("productId", EntityOperator.EQUALS, productId),
					EntityUtil.getFilterByDateExpr()
				);
			GenericValue productPriceList = EntityUtil.getFirst(delegator.findList("ProductPriceList", condition, null, null, null, false));
    		if (UtilValidate.isNotEmpty(productPriceList)) {
    			result.putAll(ServiceUtil.returnError("Product PriceList already exists!"));
    			return result;
    		}
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
    		
    		productPriceList = delegator.makeValue("ProductPriceList");
    		
    		productPriceList.put("productPriceListId", delegator.getNextSeqId("ProductPriceList"));
    		
    		productPriceList.put("description", description);
    		if (UtilValidate.isNotEmpty(fromDate)) {
    			productPriceList.put("fromDate", UtilDateTime.stringToTimeStamp(fromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(thruDate)) {
				productPriceList.put("thruDate", UtilDateTime.stringToTimeStamp(thruDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
    		productPriceList.put("productId", productId);
    		productPriceList.put("variationCodeId", variationCode);
    		productPriceList.put("bundleId", bundleId);
    		productPriceList.put("pricingMethodId", pricingMethodId);
    		productPriceList.put("variationCurrency", variationCurrency);
    		
    		productPriceList.create();
    		
    		result.put("productId", productId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully Created Pricelist.."));
    	
    	return result;
    	
    }
    
}
