/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class VariationCodeServiceImpl {

	private static final String MODULE = VariationCodeServiceImpl.class.getName();
    
	@SuppressWarnings("resource")
	public static String createVariationCode(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String variationCodeId = null;
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			GenericValue code = null;
    			String primaryKey = "";
    			
    			/*for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					GenericValue transAttr = EntityUtil.getFirst( delegator.findByAnd("TransactionAttribute", UtilMisc.toMap("attrName", attrName), null, false) );
    					if (UtilValidate.isNotEmpty(transAttr)) {
    						
    						if (UtilValidate.isNotEmpty(transAttr.getString("isRequired")) && transAttr.getString("isRequired").equals("Y")) {
    							primaryKey += request.getParameter(key)+" ";
    							GenericValue codeValue = EntityUtil.getFirst( delegator.findByAnd("VariationCodeValue", UtilMisc.toMap("attrName", attrName), null, false) );
    							if (UtilValidate.isNotEmpty(codeValue)) {
    								request.setAttribute("_ERROR_MESSAGE_", "Variation Code already exists!");
    					    		return "error";
    							}
    						}
    						
    					}
    					
    				}
    			}*/
    			
    			if (UtilValidate.isNotEmpty(request.getParameter("TRANS_ATTR_DESC"))) {
    				
    				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
                   		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "DESC"),
                   		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("attrValue")), EntityOperator.LIKE, request.getParameter("TRANS_ATTR_DESC").toUpperCase()+"%")
                   	), EntityOperator.AND);
    				
    				GenericValue codeValue = EntityUtil.getFirst( delegator.findList("VariationCodeValue", condition, null, null, null, false) );
    				if (UtilValidate.isNotEmpty(codeValue)) {
						request.setAttribute("_ERROR_MESSAGE_", "Variation Code already exists!");
			    		return "error";
					}
    			}
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					if (UtilValidate.isEmpty(code)) {
							code = delegator.makeValue("VariationCode");
    						code.put("variationCodeId", delegator.getNextSeqId("VariationCode"));
    						
    						//code.put("description", "Variation code for# "+primaryKey);
    						code.put("description", UtilValidate.isNotEmpty(request.getParameter("TRANS_ATTR_DESC")) ? request.getParameter("TRANS_ATTR_DESC") : "Variation code for# "+primaryKey);
    						code.put("fromDate", UtilDateTime.nowTimestamp());
    						
    						code.create();
    						variationCodeId = code.getString("variationCodeId");
						}
    					
    					GenericValue codeValue = delegator.makeValue("VariationCodeValue");
    					codeValue.put("variationCodeId", code.getString("variationCodeId"));
    					codeValue.put("attrName", attrName);
    					codeValue.put("attrValue", request.getParameter(key));
    					
    					codeValue.create();
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
		
    	request.setAttribute("variationCodeId", variationCodeId);
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully created Variation Code..");
    	
		return "success";
	}
	
	@SuppressWarnings("resource")
	public static String updateVariationCode(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String variationCodeId = request.getParameter("variationCodeId");
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			GenericValue code = EntityUtil.getFirst( delegator.findByAnd("VariationCode", UtilMisc.toMap("variationCodeId", variationCodeId), null, false) );
    			if (UtilValidate.isEmpty(code)) {
					request.setAttribute("_ERROR_MESSAGE_", "Variation Code not exists!");
		    		return "error";
				}
    			
    			if (UtilValidate.isNotEmpty(request.getParameter("TRANS_ATTR_DESC"))) {
    				code.put("description", request.getParameter("TRANS_ATTR_DESC"));
    				code.store();
    			}
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					GenericValue codeValue = EntityUtil.getFirst( delegator.findByAnd("VariationCodeValue", UtilMisc.toMap("attrName", attrName), null, false) );
						if (UtilValidate.isNotEmpty(codeValue)) {
							codeValue.put("attrValue", request.getParameter(key));
							codeValue.store();
						} else {
							codeValue = delegator.makeValue("VariationCodeValue");
	    					codeValue.put("variationCodeId", code.getString("variationCodeId"));
	    					codeValue.put("attrName", attrName);
	    					codeValue.put("attrValue", request.getParameter(key));
	    					
	    					codeValue.create();
						}
						
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
		
    	request.setAttribute("variationCodeId", variationCodeId);
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully updated Variation Code..");
    	
		return "success";
	}
	
    public static Map deleteVariationCode(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("SampleTable",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Custom field group not exists!"));
    			return result;
    		}
    		
    		group.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Custom field group.."));
    	
    	return result;
    	
    }
    
}
