/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityFieldValue;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class PricingMethodologyServiceImpl {

	private static final String MODULE = PricingMethodologyServiceImpl.class.getName();
    
	@SuppressWarnings("resource")
	public static String createPricingMethodology(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String pricingMethodId = null;
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			GenericValue method = null;
    			String primaryKey = "";
    			
    			/*for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					GenericValue transAttr = EntityUtil.getFirst( delegator.findByAnd("PricingMethodAttribute", UtilMisc.toMap("attrName", attrName), null, false) );
    					if (UtilValidate.isNotEmpty(transAttr)) {
    						
    						if (UtilValidate.isNotEmpty(transAttr.getString("isRequired")) && transAttr.getString("isRequired").equals("Y")) {
    							primaryKey += request.getParameter(key)+" ";
    							GenericValue codeValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("attrName", attrName, "attrValue", request.getParameter(key)), null, false) );
    							if (UtilValidate.isNotEmpty(codeValue)) {
    								request.setAttribute("_ERROR_MESSAGE_", "Pricing Method already exists!");
    					    		return "error";
    							}
    						}
    						
    					}
    					
    				}
    			}*/
    			
    			
    			if (UtilValidate.isNotEmpty(request.getParameter("TRANS_ATTR_DESCRIPTION"))) {
    				
    				EntityCondition condition = EntityCondition.makeCondition(UtilMisc.toList(
                   		EntityCondition.makeCondition("attrName", EntityOperator.EQUALS, "DESCRIPTION"),
                   		EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("attrValue")), EntityOperator.LIKE, request.getParameter("TRANS_ATTR_DESCRIPTION").toUpperCase()+"%")
                   	), EntityOperator.AND);
    				
    				GenericValue codeValue = EntityUtil.getFirst( delegator.findList("PricingMethodValue", condition, null, null, null, false) );
    				if (UtilValidate.isNotEmpty(codeValue)) {
						request.setAttribute("_ERROR_MESSAGE_", "Pricing Method already exists!");
			    		return "error";
					}
    			}
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					if (UtilValidate.isEmpty(method)) {
    						method = delegator.makeValue("PricingMethod");
    						method.put("pricingMethodId", delegator.getNextSeqId("PricingMethod"));
    						
    						//method.put("description", "Pricing Method for# "+primaryKey);
    						method.put("description", request.getParameter("TRANS_ATTR_DESCRIPTION"));
    						method.put("fromDate", UtilDateTime.nowTimestamp());
    						
    						method.create();
    						pricingMethodId = method.getString("pricingMethodId");
						}
    					
    					GenericValue methodValue = delegator.makeValue("PricingMethodValue");
    					methodValue.put("pricingMethodId", method.getString("pricingMethodId"));
    					methodValue.put("attrName", attrName);
    					methodValue.put("attrValue", request.getParameter(key));
    					
    					methodValue.create();
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
		
    	request.setAttribute("pricingMethodId", pricingMethodId);
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully created Pricing Method..");
    	
		return "success";
	}
	
	@SuppressWarnings("resource")
	public static String updatePricingMethodology(HttpServletRequest request,HttpServletResponse response) {
		
		Delegator delegator = (Delegator) request.getAttribute("delegator");
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		GenericValue userLogin = (GenericValue) request.getSession(true).getAttribute("userLogin");
		HttpSession session = request.getSession(true);
		
		String pricingMethodId = request.getParameter("pricingMethodId");
    	try {
    		
    		Map<String, Object> ctx = UtilHttp.getParameterMap(request);
    		
    		if (UtilValidate.isNotEmpty(ctx)) {
    			
    			GenericValue code = EntityUtil.getFirst( delegator.findByAnd("PricingMethod", UtilMisc.toMap("pricingMethodId", pricingMethodId), null, false) );
    			if (UtilValidate.isEmpty(code)) {
					request.setAttribute("_ERROR_MESSAGE_", "Pricing Method not exists!");
		    		return "error";
				}
    			
    			if (UtilValidate.isNotEmpty(request.getParameter("TRANS_ATTR_DESCRIPTION"))) {
    				code.put("description", request.getParameter("TRANS_ATTR_DESCRIPTION"));
    				code.store();
    			}
    			
    			for (String key : ctx.keySet()) {
    				if (key.startsWith("TRANS_ATTR_")) {
    					
    					String attrName = key.substring(11, key.length());
    					
    					GenericValue methodValue = EntityUtil.getFirst( delegator.findByAnd("PricingMethodValue", UtilMisc.toMap("pricingMethodId", pricingMethodId, "attrName", attrName), null, false) );
						if (UtilValidate.isNotEmpty(methodValue)) {
							methodValue.put("attrValue", request.getParameter(key));
							methodValue.store();
						} else {
							methodValue = delegator.makeValue("PricingMethodValue");
							methodValue.put("pricingMethodId", code.getString("pricingMethodId"));
							methodValue.put("attrName", attrName);
							methodValue.put("attrValue", request.getParameter(key));
	    					
							methodValue.create();
						}
						
    				}
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		request.setAttribute("_ERROR_MESSAGE_", e.getMessage());
    		return "error";
		}
		
    	request.setAttribute("pricingMethodId", pricingMethodId);
    	request.setAttribute("_EVENT_MESSAGE_", "Successfully updated Pricing Method..");
    	
		return "success";
	}
	
    public static Map deletePricingMethodology(DispatchContext dctx, Map context) {
    	
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String groupId = (String) context.get("groupId");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {
        	
    		GenericValue group = EntityUtil.getFirst( delegator.findByAnd("SampleTable",UtilMisc.toMap("groupId", groupId), null, false) );
    		
    		if (UtilValidate.isEmpty(group)) {
    			result.putAll(ServiceUtil.returnError("Custom field group not exists!"));
    			return result;
    		}
    		
    		group.remove();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully deleted Custom field group.."));
    	
    	return result;
    	
    }
    
}
