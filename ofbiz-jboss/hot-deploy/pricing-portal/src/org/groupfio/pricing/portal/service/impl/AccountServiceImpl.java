/**
 * 
 */
package org.groupfio.pricing.portal.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.groupfio.homeapps.util.ParamUtil;
import org.groupfio.homeapps.util.ProductUtil;
import org.groupfio.homeapps.util.UtilImport;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastList;
import javolution.util.FastMap;

/**
 * @author Sharif
 *
 */
public class AccountServiceImpl {

	private static final String MODULE = AccountServiceImpl.class.getName();
    
    public static Map createAccount(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String customerId = (String) context.get("customerId");
    	String accountId = (String) context.get("accountId");
    	String accountName = (String) context.get("accountName");
    	String openDate = (String) context.get("openDate");
    	String bundle = (String) context.get("bundle");
    	String bundleFromDate = (String) context.get("bundleFromDate");
    	String poolingPoint = (String) context.get("poolingPoint");
    	String balanceConsolidated = (String) context.get("balanceConsolidated");
    	String taxPrintIndicator = (String) context.get("taxPrintIndicator");
    	String advicePrintIndicator = (String) context.get("advicePrintIndicator");
    	//String defaultAccount = (String) context.get("defaultAccount");
    	String accountType = (String) context.get("accountType");
    	String applicationDate = (String) context.get("applicationDate");
    	String accountCurrency = (String) context.get("accountCurrency");
    	String bundleToDate = (String) context.get("bundleToDate");
    	String feeFloatBased = (String) context.get("feeFloatBased");
    	
    	String attnTo = (String) context.get("generalAttnName");
    	String address1 = (String) context.get("addressLine1");
    	String address2 = (String) context.get("addressLine2");
    	String address3 = (String) context.get("addressLine3");
    	String address4 = (String) context.get("addressLine4");
    	String address5 = (String) context.get("addressLine5");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String city = (String) context.get("city");
    	String postalCode = (String) context.get("postalCode");
    	
    	String nominatedAccountId = (String) context.get("nominatedAccountId");
    	String nominatedAccountFromDate = (String) context.get("nominatedAccountFromDate");
    	String nominatedAccountThruDate = (String) context.get("nominatedAccountThruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	//String partyId = null;
    	String source = "EXT_PARTY_ID";
    	try {
        	
    		GenericValue customerIdentification = EntityUtil.getFirst(delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyIdentificationTypeId", source, "partyId", customerId), null, true));
    		
    		if (UtilValidate.isEmpty(customerIdentification)) {
    			result.putAll(ServiceUtil.returnError("Invalid Customer!"));
    			return result;
    		}
    		
    		String custPartyId = customerIdentification.getString("partyId");
    		
    		GenericValue account = EntityUtil.getFirst(delegator.findByAnd("CustomerAccountAssoc", UtilMisc.toMap("custPartyId", custPartyId, "accountId", accountId, "accountType", accountType), null, true));
    		
    		if (UtilValidate.isNotEmpty(account)) {
    			result.putAll(ServiceUtil.returnError("Account already exists!"));
    			return result;
    		}
    		
    		Timestamp importTimestamp = UtilDateTime.nowTimestamp();
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
			List<GenericValue> toBeStored = FastList.newInstance();
			
			account = delegator.makeValue("CustomerAccountAssoc", UtilMisc.toMap("custPartyId", custPartyId, "accountId", accountId, "accountType", accountType));
			
			if (UtilValidate.isNotEmpty(openDate)) {
				account.put("openDate", UtilDateTime.stringToTimeStamp(openDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(applicationDate)) {
				account.put("applicationDate", UtilDateTime.stringToTimeStamp(applicationDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(bundleFromDate)) {
				account.put("bundleFromDate", UtilDateTime.stringToTimeStamp(bundleFromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(bundleToDate)) {
				account.put("bundleToDate", UtilDateTime.stringToTimeStamp(bundleToDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			
			account.put("bundle", bundle);
			account.put("accountCurrency", accountCurrency);
			account.put("poolingPoint", poolingPoint);
			account.put("balanceConsolidated", balanceConsolidated);
			account.put("feeFloatBased", feeFloatBased);
			account.put("taxPrintIndicator", taxPrintIndicator);
			account.put("advicePrintIndicator", advicePrintIndicator);
			//account.put("defaultAccount", defaultAccount);
			account.put("accountName", accountName);
			account.put("accountCurrency", accountCurrency);
			
			if (UtilValidate.isNotEmpty(attnTo)
	        		|| UtilValidate.isNotEmpty(address1)
	        		) {
	        	
	            // associate this as the GENERAL_LOCATION and BILLING_LOCATION
	            GenericValue contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
	            String postalAddressContactMechId = contactMech.getString("contactMechId");
	            GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, accountName, "", "", attnTo, address1, address2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
	            mainPostalAddress.put("address3", address3);
	            mainPostalAddress.put("address4", address4);
	            mainPostalAddress.put("address5", address5);
	            toBeStored.add(contactMech);
	            toBeStored.add(mainPostalAddress);
	            /*
	            toBeStored.add(UtilImport.makeContactMechPurpose("GENERAL_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("BILLING_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", postalAddressContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	            */
	            account.set("billingContactMechId", postalAddressContactMechId);
	        }
			
			// Nominated account [start]
			
			List conditionList = FastList.newInstance();
			
			conditionList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, custPartyId));
			conditionList.add(EntityCondition.makeCondition("accountId", EntityOperator.EQUALS, accountId));
			
			EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
			GenericValue nominatedAccount = EntityUtil.getFirst( delegator.findList("NominatedAccountAssoc", mainConditons, null, null, null, false) );

			if (UtilValidate.isNotEmpty(nominatedAccount) 
					&& ( (UtilValidate.isNotEmpty(poolingPoint) && (poolingPoint.equals("Y"))) || UtilValidate.isEmpty(poolingPoint)) 
					) {
				nominatedAccount.remove();
				nominatedAccount = null;
			}
			
			if (UtilValidate.isNotEmpty(nominatedAccountId)
					&& (UtilValidate.isNotEmpty(poolingPoint) && (poolingPoint.equals("N")))
					) {
				
				if (UtilValidate.isEmpty(nominatedAccount)) {
					nominatedAccount = delegator.makeValue("NominatedAccountAssoc");
					nominatedAccount.put("partyId", custPartyId);
					nominatedAccount.put("accountId", accountId);
				}
				
				nominatedAccount.put("nominatedAccountId", nominatedAccountId);
				
				if (UtilValidate.isNotEmpty(nominatedAccountFromDate)) {
					nominatedAccount.put("fromDate", UtilDateTime.stringToTimeStamp(nominatedAccountFromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				} else {
					nominatedAccount.put("fromDate", null);
				}
				
				if (UtilValidate.isNotEmpty(nominatedAccountThruDate)) {
					nominatedAccount.put("thruDate", UtilDateTime.stringToTimeStamp(nominatedAccountThruDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				} else {
					nominatedAccount.put("thruDate", null);
				}
				
				toBeStored.add(nominatedAccount);
				
			}
			
			// Nominated account [end]
	        
	        toBeStored.add(account);
	        
	        delegator.storeAll(toBeStored);
			
    		result.put("accountId", accountId);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully created Account.."));
    	
    	return result;
    	
    }
    
    public static Map updateAccount(DispatchContext dctx, Map context) {
    	
    	LocalDispatcher dispatcher = dctx.getDispatcher();
    	Delegator delegator = (Delegator) dctx.getDelegator();
    	GenericValue userLogin = (GenericValue) context.get("userLogin");
    	
    	String partyId = (String) context.get("partyId");
    	
    	String customerId = (String) context.get("customerId");
    	String accountId = (String) context.get("accountId");
    	String accountName = (String) context.get("accountName");
    	String openDate = (String) context.get("openDate");
    	String bundle = (String) context.get("bundle");
    	String bundleFromDate = (String) context.get("bundleFromDate");
    	String poolingPoint = (String) context.get("poolingPoint");
    	String balanceConsolidated = (String) context.get("balanceConsolidated");
    	String taxPrintIndicator = (String) context.get("taxPrintIndicator");
    	String advicePrintIndicator = (String) context.get("advicePrintIndicator");
    	//String defaultAccount = (String) context.get("defaultAccount");
    	String accountType = (String) context.get("accountType");
    	String applicationDate = (String) context.get("applicationDate");
    	String accountCurrency = (String) context.get("accountCurrency");
    	String bundleToDate = (String) context.get("bundleToDate");
    	String feeFloatBased = (String) context.get("feeFloatBased");
    	
    	String attnTo = (String) context.get("generalAttnName");
    	String address1 = (String) context.get("addressLine1");
    	String address2 = (String) context.get("addressLine2");
    	String address3 = (String) context.get("addressLine3");
    	String address4 = (String) context.get("addressLine4");
    	String address5 = (String) context.get("addressLine5");
    	String countryGeoId = (String) context.get("countryGeoId");
    	String stateProvinceGeoId = (String) context.get("stateProvinceGeoId");
    	String city = (String) context.get("city");
    	String postalCode = (String) context.get("postalCode");
    	
    	String nominatedAccountId = (String) context.get("nominatedAccountId");
    	String nominatedAccountFromDate = (String) context.get("nominatedAccountFromDate");
    	String nominatedAccountThruDate = (String) context.get("nominatedAccountThruDate");
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	//String partyId = null;
    	String source = "EXT_PARTY_ID";
    	
    	result.put("partyId", partyId);
    	result.put("accountId", accountId);
    	
    	try {
        	
    		GenericValue customerIdentification = EntityUtil.getFirst(delegator.findByAnd("PartyIdentification", UtilMisc.toMap("partyIdentificationTypeId", source, "partyId", partyId), null, false));
    		if (UtilValidate.isEmpty(customerIdentification)) {
    			result.putAll(ServiceUtil.returnError("Invalid Customer!"));
    			return result;
    		}
    		
    		GenericValue account = EntityUtil.getFirst(delegator.findByAnd("CustomerAccountAssoc", UtilMisc.toMap("custPartyId", partyId, "accountId", accountId), null, false));
    		if (UtilValidate.isEmpty(account)) {
    			result.putAll(ServiceUtil.returnError("Invalid Customer and Account association!"));
    			return result;
    		}
    		
    		Timestamp importTimestamp = UtilDateTime.nowTimestamp();
    		
    		Map<String, Object> callCtxt = FastMap.newInstance();
			Map<String, Object> callResult = FastMap.newInstance();
			List<GenericValue> toBeStored = FastList.newInstance();
			
			if (UtilValidate.isNotEmpty(openDate)) {
				account.put("openDate", UtilDateTime.stringToTimeStamp(openDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(applicationDate)) {
				account.put("applicationDate", UtilDateTime.stringToTimeStamp(applicationDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(bundleFromDate)) {
				account.put("bundleFromDate", UtilDateTime.stringToTimeStamp(bundleFromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			if (UtilValidate.isNotEmpty(bundleToDate)) {
				account.put("bundleToDate", UtilDateTime.stringToTimeStamp(bundleToDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
			}
			
			account.put("bundle", bundle);
			account.put("accountCurrency", accountCurrency);
			account.put("poolingPoint", poolingPoint);
			account.put("balanceConsolidated", balanceConsolidated);
			account.put("feeFloatBased", feeFloatBased);
			account.put("taxPrintIndicator", taxPrintIndicator);
			account.put("advicePrintIndicator", advicePrintIndicator);
			//account.put("defaultAccount", defaultAccount);
			account.put("accountName", accountName);
			account.put("accountCurrency", accountCurrency);
			
			if (UtilValidate.isNotEmpty(attnTo)
	        		|| UtilValidate.isNotEmpty(address1)) {
	        	
	        	GenericValue contactMech = null;
	        	String postalAddressContactMechId = null;
	        	
	        	if (UtilValidate.isNotEmpty(account.get("billingContactMechId"))) {
	        		contactMech = EntityUtil.getFirst(delegator.findByAnd("ContactMech", UtilMisc.toMap("contactMechId", account.get("billingContactMechId")), null, false));
	        		postalAddressContactMechId = contactMech.getString("contactMechId");
	        	} else {
	        		// associate this as the GENERAL_LOCATION and BILLING_LOCATION
		            contactMech = delegator.makeValue("ContactMech", UtilMisc.toMap("contactMechId", delegator.getNextSeqId("ContactMech"), "contactMechTypeId", "POSTAL_ADDRESS"));
		            postalAddressContactMechId = contactMech.getString("contactMechId");
		            account.set("billingContactMechId", postalAddressContactMechId);
	        	}
	        	
	            GenericValue mainPostalAddress = UtilImport.makePostalAddress(contactMech, accountName, "", "", attnTo, address1, address2, city, stateProvinceGeoId, postalCode, null, countryGeoId, delegator);
	            mainPostalAddress.put("address3", address3);
	            mainPostalAddress.put("address4", address4);
	            mainPostalAddress.put("address5", address5);
	            toBeStored.add(contactMech);
	            toBeStored.add(mainPostalAddress);
	            /*
	            toBeStored.add(UtilImport.makeContactMechPurpose("GENERAL_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("BILLING_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(UtilImport.makeContactMechPurpose("PRIMARY_LOCATION", mainPostalAddress, partyId, importTimestamp, delegator));
	            toBeStored.add(delegator.makeValue("PartyContactMech", UtilMisc.toMap("contactMechId", postalAddressContactMechId, "partyId", partyId, "fromDate", importTimestamp,"allowSolicitation","Y")));
	            */
	            
	        }
			
			// Nominated account [start]
			
			List conditionList = FastList.newInstance();
			
			conditionList.add(EntityCondition.makeCondition("partyId", EntityOperator.EQUALS, partyId));
			conditionList.add(EntityCondition.makeCondition("accountId", EntityOperator.EQUALS, accountId));
			
			EntityCondition mainConditons = EntityCondition.makeCondition(conditionList, EntityOperator.AND);
			GenericValue nominatedAccount = EntityUtil.getFirst( delegator.findList("NominatedAccountAssoc", mainConditons, null, null, null, false) );

			if (UtilValidate.isNotEmpty(nominatedAccount) 
					&& ( (UtilValidate.isNotEmpty(poolingPoint) && (poolingPoint.equals("Y"))) || UtilValidate.isEmpty(poolingPoint)) 
					) {
				nominatedAccount.remove();
				nominatedAccount = null;
			}
			
			if (UtilValidate.isNotEmpty(nominatedAccountId)
					&& (UtilValidate.isNotEmpty(poolingPoint) && (poolingPoint.equals("N")))
					) {
				
				if (UtilValidate.isEmpty(nominatedAccount)) {
					nominatedAccount = delegator.makeValue("NominatedAccountAssoc");
					nominatedAccount.put("partyId", partyId);
					nominatedAccount.put("accountId", accountId);
				}
				
				nominatedAccount.put("nominatedAccountId", nominatedAccountId);
				
				if (UtilValidate.isNotEmpty(nominatedAccountFromDate)) {
					nominatedAccount.put("fromDate", UtilDateTime.stringToTimeStamp(nominatedAccountFromDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				} else {
					nominatedAccount.put("fromDate", null);
				}
				
				if (UtilValidate.isNotEmpty(nominatedAccountThruDate)) {
					nominatedAccount.put("thruDate", UtilDateTime.stringToTimeStamp(nominatedAccountThruDate, "dd-MM-yyyy", TimeZone.getDefault(), Locale.getDefault()));
				} else {
					nominatedAccount.put("thruDate", null);
				}
				
				toBeStored.add(nominatedAccount);
				
			}
	        
	        toBeStored.add(account);
	        
	        delegator.storeAll(toBeStored);
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		Debug.logError(e.getMessage(), MODULE);
    		result.putAll(ServiceUtil.returnError(e.getMessage()));
			return result;
		}
    	
    	result.putAll(ServiceUtil.returnSuccess("Successfully updated Account.."));
    	
    	return result;
    	
    }
    
}
