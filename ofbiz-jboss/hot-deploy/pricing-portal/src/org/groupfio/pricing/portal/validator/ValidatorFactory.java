/**
 * 
 */
package org.groupfio.pricing.portal.validator;

/**
 * @author Sharif
 *
 */
public final class ValidatorFactory {

	private static final AccessTokenValidator ACCESS_TOKEN_VALIDATOR = new AccessTokenValidator();
	
	private static final UpdatePartyDataValidator UPDATE_PARTY_VALIDATOR = new UpdatePartyDataValidator();
	
	public static AccessTokenValidator getAccessTokenValidator () {
		return ACCESS_TOKEN_VALIDATOR;
	}
	
	public static UpdatePartyDataValidator getUpdatePartyDataValidator () {
		return UPDATE_PARTY_VALIDATOR;
	}
	
}
