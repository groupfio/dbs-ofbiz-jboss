<#if userLogin?has_content>
		
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top">      	 	 	 
		<a class="navbar-brand navbar-brand-tit mr-auto" href="#"> <img src="/bootstrap-dbs/images/dbs-dark.png" class="img-responsive" alt="" /></a>
	 	<div>
	       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainnav" aria-controls="mainnav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	      </button>
	      <div class="collapse navbar-collapse" id="mainnav">
	        <ul class="navbar-nav">
	          <li class="nav-item dropdown"> <a class="nav-link" href="<@ofbizUrl>main</@ofbizUrl>">Home</a></li>
	          <li class="nav-item dropdown"> <a class="nav-link" href="#">Pending</a></li>
			    <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="#" id="crmdd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Change Office [Singapore]</a>
				<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item" href="">India</a>
			    <a class="dropdown-item" href="">Thailand</a>
			    <a class="dropdown-item" href="">Hong Kong</a>
		        <a class="dropdown-item" href="">Singapore</a>
	            </div>
	          </li>
	  
	        </ul>
	         <ul class="navbar-nav">
	            <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog">
	              </span></a>
	             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown05">
	              <a class="dropdown-item" href="#">Help</a>
	              <a class="dropdown-item" href="<@ofbizUrl>logout</@ofbizUrl>">Logout</a>
	            </div>
	            </li>
	          </ul>	
		  </div>
	 	</div>
	</nav>  
	
 </#if>
