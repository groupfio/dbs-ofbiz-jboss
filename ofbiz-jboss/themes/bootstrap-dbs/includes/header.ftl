<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="${pageDescriptionLabel!}" />
  <meta name="keywords" content="${keywordsLabel!}" />
  <meta name="author" content="">
  
  <#if layoutSettings.VT_SHORTCUT_ICON?has_content>
    	<#assign shortcutIcon = layoutSettings.VT_SHORTCUT_ICON.get(0)/>
  	<#elseif layoutSettings.shortcutIcon?has_content>
    	<#assign shortcutIcon = layoutSettings.shortcutIcon/>
  	</#if>
  	<#if shortcutIcon?has_content>
    	<link rel="shortcut icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />	
		<link rel="icon" type="image/x-icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />
  	</#if>
  <!-- <link rel="shortcut icon" href="/bootstrap-dbs/images/favicon.ico"> -->
  <title>${layoutSettings.companyName?if_exists} | <#if (page.titleProperty)?has_content>${uiLabelMap[page.titleProperty]}<#else>${(page.title)!}</#if></title>
  <!-- Bootstrap core CSS -->
  <link href="/bootstrap-dbs/css/blue.css" rel="stylesheet">
  <link href="/bootstrap-dbs/css/bootstrap.min.css" rel="stylesheet">
  <link href="/bootstrap-dbs/css/font-awesome.min.css" rel="stylesheet">
  <link href="/bootstrap-dbs/css/bootstrap-select.min.css" rel="stylesheet" >
  <link href="/bootstrap-dbs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="/bootstrap-dbs/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <script type="text/javascript" src="/bootstrap-dbs/js/include.js"> </script>
  <script type="text/javascript" src="/bootstrap-dbs/js/bootstrap-select.min.js" defer></script>
  <script type="text/javascript"  src="/bootstrap-dbs/js/popper.min.js"></script>
  <script type="text/javascript" src="/bootstrap-dbs/js/jquery.min.js"> </script>
  <script type="text/javascript" src="/bootstrap-dbs/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/bootstrap-dbs/js/dataTables.bootstrap.min.js"></script>  
  <script src="/bootstrap-dbs/js/bootstrap.min.js"></script>    
  <link href="/bootstrap-dbs/css/summernote.css" rel="stylesheet">
  <script src="/bootstrap-dbs/js/summernote.js"></script>
  <script>window.jQuery || document.write('<script src="/bootstrap-dbs/js/jquery.min.js"><\/script>')</script>
  <script type="text/javascript" src="/bootstrap-dbs/js/moment.js"></script>
  
  <script type="text/javascript" src="/bootstrap-dbs/js/validator.min.js"></script>
  <script type="text/javascript" src="/bootstrap-dbs/js/bootstrap-datetimepicker.min.js"></script>
  <script src="/bootstrap-dbs/js/bootstrap-notify.min.js"></script>
  <script src="/bootstrap-dbs/js/bootstrap-notify.js"></script>
  <script src="/bootstrap-dbs/js/bootstrap-confirmation.js"></script>
  <script src="/bootstrap-dbs/js/jquery.pulsate.min.js"></script>
  <script src="/bootstrap-dbs/js/bootbox/bootbox.min.js"></script>

  <#-- <script type="text/javascript" src="/bootstrap-dbs/js/bootstrap-validate.js"></script> -->
	  
  <link href="/bootstrap-dbs/css/animate.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript" >
     $(document).ready(function() {
         $('#dtable').DataTable();
         $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="hidden"], [type="submit"], [type="reset"], button):enabled:visible';
     } );
  </script>

      
<link href="/bootstrap-dbs/css/dropdown.css" rel="stylesheet">
<link href="/bootstrap-dbs/css/transition.min.css" rel="stylesheet">
<script src="/bootstrap-dbs/js/dropdown.js"></script>
<script src="/bootstrap-dbs/js/transition.min.js"></script>

<script src="/bootstrap-dbs/js/custom.js"></script>

<#--  
<link href="/bootstrap-dbs/css/semantic.min.css" rel="stylesheet">
<script src="/bootstrap-dbs/js/semantic.min.js"></script> 
 -->
 
<link href="/bootstrap-dbs/css/fio-custom.css" rel="stylesheet">

</head>
 