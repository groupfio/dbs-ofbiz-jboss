(function () {
      	jQuery(document).ready(function () {
      		jQuery('#navbox-trigger').click(function () {
      			return jQuery('#navigation-bar').toggleClass('navbox-open');
      		});
      		return jQuery(document).on('click', function (e) {
      			var target;
      			target = jQuery(e.target);
      			if (!target.closest('.navbox').length && !target.closest('#navbox-trigger').length) {
      				return jQuery('#navigation-bar').removeClass('navbox-open');
      			}
      		});
      	});
      }.call(this));

$(function () {
          $('#datetimepicker8').datetimepicker();
          $('#datetimepicker9').datetimepicker();
          $('#datetimepicker10').datetimepicker();
          $('#datetimepicker11').datetimepicker();
          $('#datetimepicker12').datetimepicker();
          $('#datetimepicker6').datetimepicker();
          $('#datetimepicker7').datetimepicker({
              useCurrent: false //Important! See issue #1075
          });
          $("#datetimepicker6").on("dp.change", function (e) {
              $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
          });
          $("#datetimepicker7").on("dp.change", function (e) {
              $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
          });
      }); 

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  if(inp != null && inp != "") {
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  }
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      });
}

/*An array containing all the country names in the world:*/
var countries = ["AAA Prod [10000]","BBB Prod [10000]","CCC Prod [10000]","DDD Prod [10000]","EE Prod [10000]","FFF Prod [10000]","Manufact Prod [10000]","PS Peacock Pen [10000]","Fitbit Charge HR [10000]","Raw mat1 [10001]"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);

String.prototype.replaceAll = function(search, replace) {
    if (replace === undefined) {
        return this.toString();
    }
    return this.split(search).join(replace);
};
function showAlert (type, message) {
	var notifyType = "info";
	
	if(type == "error") {
		notifyType = "danger";
	} else if(type == "warning") {
		notifyType = "warning";
	} else if(type == "success") {
		notifyType = "success";
	} else if(type == "info") {
		notifyType = "info";
	}
	
	$.notify({
		// options
		message: message
	},{
		// settings
		type: notifyType,
		delay: 1
	});
	
}

$('.pulsate-regular').pulsate({
    color: "#bf1c56"
});

$('.pulsate-once').pulsate({
    color: "#399bc3",
    repeat: false
});

$('.pulsate-crazy').pulsate({
    color: "#fdbe41",
    reach: 50,
    repeat: 10,
    speed: 100,
    glow: true
});
	
resetDefaultEvents();
function resetDefaultEvents () {
	
	$(".tooltip").tooltip("hide");
	
	$('.tooltips').tooltip();
	
	$('.confirm-message').unbind( "click" );
	
	$('.confirm-message').bind( "click", function( event ) {
		event.preventDefault(); 
		
		var href = $(this).attr('href');
		var message = $(this).data('message');
		if (!$.trim(message)) {
			message = "Are you sure?";
		}
		
		bootbox.confirm(message, function(result) {
			if (result) {
				window.location.href = href;
			}
	    });
	});
	$('[data-toggle="confirmation"]').confirmation();
	$('.selectpicker').selectpicker('refresh');
}

jQuery(document).ready(function() {  
	/* 
	$(".form_datetime").datetimepicker({
	        //autoclose: true,
	        //isRTL: BootStrapInit.isRTL(),
	        //format: "dd MM yyyy - hh:ii",
	        //pickerPosition: (BootStrapInit.isRTL() ? "bottom-right" : "bottom-left")
	  });
	 */
	 $('.ui.dropdown.search').dropdown();
	 
});