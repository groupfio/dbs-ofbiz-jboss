<footer class="footer">
  <div class="container-fluid">
  <div class="row">	  
    <div class="col-md-6 col-sm-6">
      <p class="text-muted">		 Time Zone : Eastern Daylight Time</p>
    </div>
    <div class="col-md-6 col-sm-6">
      <p class="text-muted float-right">		2018 © <img alt="Group FiO" src="/bootstrap/images/logo.png" height="20" width="44" border="0" > 
        Professional Edition of FiO RMS Build 2018.2. All Rights Reserved
      </p>
    </div>
    </div>
  </div>
</footer>

      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script>window.jQuery || document.write('<script src="/bootstrap/js/jquery.min.js"><\/script>')</script>
      <script type="text/javascript" src="/bootstrap/js/moment.js"></script>
      <script type="text/javascript" src="/bootstrap/js/validator.min.js"></script>
      <script type="text/javascript" src="/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
      
      <script type="text/javascript" src="/bootstrap/js/custom.js"></script>
      <script type="text/javascript" src="/bootstrap/js/index.js"></script>
<script type="text/javascript" src="/bootstrap/js/jquery.easing.min.js"></script>
      
<script>

$(".form_datetime").datetimepicker({
    //autoclose: true,
    //isRTL: BootStrapInit.isRTL(),
    //format: "dd MM yyyy - hh:ii",
    //pickerPosition: (BootStrapInit.isRTL() ? "bottom-right" : "bottom-left")
     useCurrent: false,
     maxDate: moment()
});

$('.ui.dropdown').dropdown({
	//clearable: true
});

$('.reset-btn').on('click', function(){
	$('.ui.dropdown').dropdown("clear");
});

</script>            