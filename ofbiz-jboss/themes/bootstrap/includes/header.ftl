<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="${pageDescriptionLabel!}" />
  <meta name="keywords" content="${keywordsLabel!}" />
  <meta name="author" content="">
  
  <#if layoutSettings.VT_SHORTCUT_ICON?has_content>
    	<#assign shortcutIcon = layoutSettings.VT_SHORTCUT_ICON.get(0)/>
  	<#elseif layoutSettings.shortcutIcon?has_content>
    	<#assign shortcutIcon = layoutSettings.shortcutIcon/>
  	</#if>
  	<#if shortcutIcon?has_content>
    	<link rel="shortcut icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />	
		<link rel="icon" type="image/x-icon" href="<@ofbizContentUrl>${StringUtil.wrapString(shortcutIcon)}</@ofbizContentUrl>" />
  	</#if>
  <!-- <link rel="shortcut icon" href="/bootstrap/images/favicon.ico"> -->
  <title>${layoutSettings.companyName?if_exists} | <#if (page.titleProperty)?has_content>${uiLabelMap[page.titleProperty]}<#else>${(page.title)!}</#if></title>
  <!-- Bootstrap core CSS -->
  <link href="/bootstrap/css/blue.css" rel="stylesheet">
  <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/bootstrap/css/font-awesome.min.css" rel="stylesheet">
  <link href="/bootstrap/css/bootstrap-select.min.css" rel="stylesheet" >
  <link href="/bootstrap/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <script type="text/javascript" src="/bootstrap/js/include.js"> </script>
  <script type="text/javascript" src="/bootstrap/js/bootstrap-select.min.js" defer></script>
  <script type="text/javascript"  src="/bootstrap/js/popper.min.js"></script>
  <script type="text/javascript" src="/bootstrap/js/jquery.min.js"> </script>
  <script type="text/javascript" src="/bootstrap/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/bootstrap/js/dataTables.bootstrap.min.js"></script>  
  <script src="/bootstrap/js/bootstrap.min.js"></script>    
  <link href="/bootstrap/css/summernote.css" rel="stylesheet">
  <script src="/bootstrap/js/summernote.js"></script>
  <script>window.jQuery || document.write('<script src="/bootstrap/js/jquery.min.js"><\/script>')</script>
  <script type="text/javascript" src="/bootstrap/js/moment.js"></script>
  
  <script type="text/javascript" src="/bootstrap/js/validator.min.js"></script>
  <script type="text/javascript" src="/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
  <script src="/bootstrap/js/bootstrap-notify.min.js"></script>
  <script src="/bootstrap/js/bootstrap-notify.js"></script>
  <script src="/bootstrap/js/bootstrap-confirmation.js"></script>
  <script src="/bootstrap/js/jquery.pulsate.min.js"></script>
  <script src="/bootstrap/js/bootbox/bootbox.min.js"></script>

  <#-- <script type="text/javascript" src="/bootstrap/js/bootstrap-validate.js"></script> -->
	  
  <link href="/bootstrap/css/animate.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript" >
     $(document).ready(function() {
         $('#dtable').DataTable();
         $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="hidden"], [type="submit"], [type="reset"], button):enabled:visible';
     } );
  </script>

      
<link href="/bootstrap/css/dropdown.css" rel="stylesheet">
<link href="/bootstrap/css/transition.min.css" rel="stylesheet">
<script src="/bootstrap/js/dropdown.js"></script>
<script src="/bootstrap/js/transition.min.js"></script>

<#--  
<link href="/bootstrap/css/semantic.min.css" rel="stylesheet">
<script src="/bootstrap/js/semantic.min.js"></script> 
 -->
 
<link href="/bootstrap/css/fio-custom.css" rel="stylesheet">

</head>
 