<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#if userLogin?has_content>
	<#assign getMenuList = dispatcher.runSync("getComponentMenuList", Static["org.ofbiz.base.util.UtilMisc"].toMap("activeApp", activeApp, "userLogin", userLogin))/>
	<#--<#assign appModelMenu = Static["org.fio.menu.model.ComponentMenuList"].getComponentMenuList(applicationMenuLocation)?if_exists />-->
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
	 
	   <div id="navigation-bar" class="navigation-bar">
	      <div class="bar">
	        <button id="navbox-trigger" class="navbox-trigger">
	        <span class="glyphicon glyphicon-th"></span>
	        </button>
	      </div>
	      <div class="navbox">
	         <div class="navbox-tiles">
	         <#--  <a href="/crm/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/crm.png" alt="CRM, Customer Service, Order Entry, Marketing" title="CRM, Customer Service, Order Entry, Marketing">
	          <span class="tiletxt"> CRM </span>
	          </a>              
			  <#--<a href="/cms/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/content-management.png" alt="Content management System" title="Content management System">
	          <span class="tiletxt">Content Management </span>
	          </a> 
	         <a href="/campaign/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/campaign-manager.png" alt="Retail Marketing Solutions" title="Retail Marketing Solutions">
	          <span class="tiletxt"> Campaign Manager </span>
	          </a>
			  <a href="/custom-field/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/customer-segmentation.png" alt="Customer Segmentation" title="Customer Segmentation">
	          <span class="tiletxt"> Segmentation </span>
	          </a>			 
			  <a href="/partymgr/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/user-management.png" alt="Parties and Users" title="Parties and Users">
	          <span class="tiletxt"> User Management </span>
	          </a>
	          <a href="/data-importer/control/main?${StringUtil.wrapString(externalKeyParam)}" class="tile">
	          <img class="" src="/bootstrap/images/data-import.png" alt="Data Importer" title="Data Importer">
	          <span class="tiletxt"> Data Importer </span>
	          </a>	-->
	           <#if componentAccess?exists && componentAccess?has_content>
          <#list componentAccess as components>
            
            <a href="${components.requestURI!}?${StringUtil.wrapString(externalKeyParam)}" class="tile">
              <img class="" src="${components.imageURL!}" alt="${components.uiLabels!}" title="${components.uiLabels!}">
               <span class="tiletxt"> ${components.uiLabels!} </span>
            </a>
           </#list>
         </#if>		  
	        </div>
	      </div>
	    </div>	  
	   <a class="navbar-brand navbar-brand-tit" href="<@ofbizUrl>main</@ofbizUrl>">${layoutSettings.companyName?if_exists}</a>
	   <div class="d-sm-block d-md-block d-lg-none d-xl-none"> <a class="nav-link btn-primary" href="/crm/control/createLeadShortForm" title="Create Lead Short Form"><span class="fa fa-user-plus">	
			</span></a>
		</div>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainnav" aria-controls="mainnav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="mainnav">
	    <ul class="navbar-nav mr-auto">
	    <#--<#if appModelMenu?has_content>
	    <#list appModelMenu as menu>
			<#assign menuName = menu.getTitle(context)>
			<#assign tabName=menu.name/>
			<#assign menuItemList = menu.menuItemList>
			<#if menuItemList?has_content>
				<li class="nav-item dropdown <#if sectionName?exists && sectionName?has_content && sectionName == tabName>active</#if>">
				   <a class="nav-link dropdown-toggle" href="#" id="crmdd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${menuName?if_exists}</a>
					<div class="dropdown-menu">
					<#list menuItemList as item>
						<#assign name = item.name>
						<#assign title = item.getTitle(context)>
						<#assign target = item.getLink().getTarget(context)>			     
					   <a class="dropdown-item" href="${target}">${title}</a>
				     </#list>
					</div>
				  </li>
			</#if>
		</#list>
		</#if>-->
		<#if getMenuList?has_content>
		  <#list getMenuList.menuList as getMenuGV>
		    <#assign tab = getMenuGV.tab>
		    <#assign shortcut = getMenuGV.shortcut>
		    <li class="nav-item dropdown <#if sectionName?exists && sectionName?has_content && sectionName == "${tab.pageId?if_exists}">active</#if>" >
                <a class="nav-link dropdown-toggle" href="#" id="crmddff" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${tab.uiLabels?if_exists}</a>
                  <div class="dropdown-menu">
                     <#list shortcut as short>
                        <a class="dropdown-item" href="${short.requestUri?if_exists}">${short.uiLabels?if_exists}</a>
                     </#list>
                  </div>
              </li>
		  </#list>
		</#if>
		</ul>
        <a class="nav-link btn-primary" href="/crm/control/createLeadShortForm" id="dropdown05" title="Create Lead Short Form">
            <span class="fa fa-user-plus"> </span>
        </a>
		<h1 class="float-right mt-2 ml-2">${userName?if_exists}</h1>
		<ul class="navbar-nav ">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog">
				</span></a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown05">
				<!--<a class="dropdown-item" href="#">Tenant Info</a> -->
				<a class="dropdown-item" href="#">Profile</a>
				<a class="dropdown-item" href="#">Help</a>
				<a class="dropdown-item" href="<@ofbizUrl>logout</@ofbizUrl>">${uiLabelMap.CommonLogout}</a>
				</div>
			</li>
		</ul>	
	 </nav>	 
 </#if>
