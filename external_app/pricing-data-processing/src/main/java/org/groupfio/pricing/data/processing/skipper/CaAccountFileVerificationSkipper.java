/**
 * 
 */
package org.groupfio.pricing.data.processing.skipper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.groupfio.pricing.data.processing.BatchConstants.ErrorType;
import org.groupfio.pricing.data.processing.entity.BatchStepErrorLog;
import org.groupfio.pricing.data.processing.listener.step.CaAccountExecutionListener;
import org.groupfio.pricing.data.processing.util.LogWriter;
import org.groupfio.pricing.data.processing.util.UtilDateTime;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Sharif
 *
 */
public class CaAccountFileVerificationSkipper implements SkipPolicy {
	
	@Autowired
    private EntityManagerFactory entityManagerFactory;
	
	private CaAccountExecutionListener stepExecutionListener;
	
	public CaAccountFileVerificationSkipper(CaAccountExecutionListener stepExecutionListener) {
		this.stepExecutionListener = stepExecutionListener;
	}
	
	@Override
	public boolean shouldSkip(Throwable exception, int skippCount) throws SkipLimitExceededException {
		
		if (UtilValidate.isNotEmpty(stepExecutionListener)) {
			
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			LogWriter.write(entityManager, stepExecutionListener.getStepExecution().getJobExecutionId(), stepExecutionListener.getStepExecution().getId(), exception.getMessage(), ErrorType.PARSING);
			
			entityManager.close();
			
		}
		
		return true;
		
	}

}
