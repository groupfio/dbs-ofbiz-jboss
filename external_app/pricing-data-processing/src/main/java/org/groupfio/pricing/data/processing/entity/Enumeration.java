/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "Enumeration.findAll", query = "select p from Enumeration p"),
	@NamedQuery(name = "Enumeration.findOne", query = "select p from Enumeration p where p.enumId=:enumId")
})
@Table(name = "enumeration")
@Entity
public class Enumeration implements Serializable {
	
	@Id
	private String enumId;
	private String enumTypeId;
	private String enumCode;
	private String sequenceId;
	private String description;
	
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;

	public Enumeration() {
		
	}
	
	public String getEnumId() {
		return enumId;
	}

	public void setEnumId(String enumId) {
		this.enumId = enumId;
	}

	public String getEnumTypeId() {
		return enumTypeId;
	}

	public void setEnumTypeId(String enumTypeId) {
		this.enumTypeId = enumTypeId;
	}

	public String getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}

	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}

	public Timestamp getCreatedStamp() {
		return createdStamp;
	}

	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}

	public String getPrimaryIdentifier() {
		return "enumId: "+enumId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " enum_Id='"+enumId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
