package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ 
	@NamedQuery(name = "CustomFieldPartyClassification.findAll", query = "select p from CustomFieldPartyClassification p"),
	@NamedQuery(name = "CustomFieldPartyClassification.findOne", query = "select p from CustomFieldPartyClassification p where p.groupId=:groupId and p.customFieldId=:customFieldId and p.partyId=:partyId")
})
@Table(name = "custom_field_party_classification")
@Entity
public class CustomFieldPartyClassification implements Serializable {
	@Id
	private String groupId;
	@Id
	private String customFieldId;
	@Id
	private String partyId;
	private String groupActualValue;
	
	private String isRemoved;
	
	private Timestamp inceptionDate;
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getCustomFieldId() {
		return customFieldId;
	}
	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getGroupActualValue() {
		return groupActualValue;
	}
	public void setGroupActualValue(String groupActualValue) {
		this.groupActualValue = groupActualValue;
	}
	public Timestamp getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(Timestamp inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}
	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}
	public Timestamp getCreatedStamp() {
		return createdStamp;
	}
	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}
	public String getIsRemoved() {
		return isRemoved;
	}
	public void setIsRemoved(String isRemoved) {
		this.isRemoved = isRemoved;
	}
	
	
}
