/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "DataImportCustomer.findAll", query = "select p from DataImportCustomer p"),
	@NamedQuery(name = "DataImportCustomer.findOne", query = "select p from DataImportCustomer p where p.id.customerId=:customerId")
})
@Table(name = "data_import_customer")
@Entity
public class DataImportCustomer implements Serializable {

	@EmbeddedId
    private DataImportCustomerId id;
	
	/*@Id
	private String batchId;
	@Id
	private String customerId;*/
	
	private String companyName;
	private String firstName;
	private String lastName;
	private String attnName;
	private String address1;
	private String address2;
	private String city;
	private String stateProvinceGeoId;
	private String postalCode;
	private String postalCodeExt;
	private String countryGeoId;
	private String primaryPhoneCountryCode;
	private String primaryPhoneAreaCode;
	private String primaryPhoneNumber;
	private String primaryPhoneExtension;
	private String secondaryPhoneCountryCode;
	private String secondaryPhoneAreaCode;
	private String secondaryPhoneNumber;
	private String secondaryPhoneExtension;
	private String faxCountryCode;
	private String faxAreaCode;
	private String faxNumber;
	private String didCountryCode;
	private String didAreaCode;
	private String didNumber;
	private String didExtension;
	private String emailAddress;
	private String webAddress;
	private BigDecimal discount;
	private String partyClassificationTypeId;
	private String creditCardNumber;
	private String creditCardExpDate;
	private BigDecimal outstandingBalance;
	private BigDecimal creditLimit;
	private String currencyUomId;
	private String disableShipping;
	private Long netPaymentDays;
	private String shipToCompanyName;
	private String shipToFirstName;
	private String shipToLastName;
	private String shipToAttnName;
	private String shipToAddress1;
	private String shipToAddress2;
	private String shipToCity;
	private String shipToStateProvinceGeoId;
	private String shipToPostalCode;
	private String shipToPostalCodeExt;
	private String shipToStateProvGeoName;
	private String shipToCountryGeoId;
	private String note;
	private String primaryPartyId;
	private String companyPartyId;
	private String personPartyId;
	private String source;
	
	private String isImportAsContact;
	
	private String importStatusId;
	private String importError;
	private Timestamp processedTimestamp;
	
	public DataImportCustomer() {
		
	}

	
	
	/*public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}*/

	public DataImportCustomerId getId() {
		return id;
	}
	
	public void setId(DataImportCustomerId id) {
		this.id = id;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAttnName() {
		return attnName;
	}

	public void setAttnName(String attnName) {
		this.attnName = attnName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvinceGeoId() {
		return stateProvinceGeoId;
	}

	public void setStateProvinceGeoId(String stateProvinceGeoId) {
		this.stateProvinceGeoId = stateProvinceGeoId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPostalCodeExt() {
		return postalCodeExt;
	}

	public void setPostalCodeExt(String postalCodeExt) {
		this.postalCodeExt = postalCodeExt;
	}

	public String getCountryGeoId() {
		return countryGeoId;
	}

	public void setCountryGeoId(String countryGeoId) {
		this.countryGeoId = countryGeoId;
	}

	public String getPrimaryPhoneCountryCode() {
		return primaryPhoneCountryCode;
	}

	public void setPrimaryPhoneCountryCode(String primaryPhoneCountryCode) {
		this.primaryPhoneCountryCode = primaryPhoneCountryCode;
	}

	public String getPrimaryPhoneAreaCode() {
		return primaryPhoneAreaCode;
	}

	public void setPrimaryPhoneAreaCode(String primaryPhoneAreaCode) {
		this.primaryPhoneAreaCode = primaryPhoneAreaCode;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getPrimaryPhoneExtension() {
		return primaryPhoneExtension;
	}

	public void setPrimaryPhoneExtension(String primaryPhoneExtension) {
		this.primaryPhoneExtension = primaryPhoneExtension;
	}

	public String getSecondaryPhoneCountryCode() {
		return secondaryPhoneCountryCode;
	}

	public void setSecondaryPhoneCountryCode(String secondaryPhoneCountryCode) {
		this.secondaryPhoneCountryCode = secondaryPhoneCountryCode;
	}

	public String getSecondaryPhoneAreaCode() {
		return secondaryPhoneAreaCode;
	}

	public void setSecondaryPhoneAreaCode(String secondaryPhoneAreaCode) {
		this.secondaryPhoneAreaCode = secondaryPhoneAreaCode;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String getSecondaryPhoneExtension() {
		return secondaryPhoneExtension;
	}

	public void setSecondaryPhoneExtension(String secondaryPhoneExtension) {
		this.secondaryPhoneExtension = secondaryPhoneExtension;
	}

	public String getFaxCountryCode() {
		return faxCountryCode;
	}

	public void setFaxCountryCode(String faxCountryCode) {
		this.faxCountryCode = faxCountryCode;
	}

	public String getFaxAreaCode() {
		return faxAreaCode;
	}

	public void setFaxAreaCode(String faxAreaCode) {
		this.faxAreaCode = faxAreaCode;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getDidCountryCode() {
		return didCountryCode;
	}

	public void setDidCountryCode(String didCountryCode) {
		this.didCountryCode = didCountryCode;
	}

	public String getDidAreaCode() {
		return didAreaCode;
	}

	public void setDidAreaCode(String didAreaCode) {
		this.didAreaCode = didAreaCode;
	}

	public String getDidNumber() {
		return didNumber;
	}

	public void setDidNumber(String didNumber) {
		this.didNumber = didNumber;
	}

	public String getDidExtension() {
		return didExtension;
	}

	public void setDidExtension(String didExtension) {
		this.didExtension = didExtension;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getPartyClassificationTypeId() {
		return partyClassificationTypeId;
	}

	public void setPartyClassificationTypeId(String partyClassificationTypeId) {
		this.partyClassificationTypeId = partyClassificationTypeId;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpDate() {
		return creditCardExpDate;
	}

	public void setCreditCardExpDate(String creditCardExpDate) {
		this.creditCardExpDate = creditCardExpDate;
	}

	public BigDecimal getOutstandingBalance() {
		return outstandingBalance;
	}

	public void setOutstandingBalance(BigDecimal outstandingBalance) {
		this.outstandingBalance = outstandingBalance;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCurrencyUomId() {
		return currencyUomId;
	}

	public void setCurrencyUomId(String currencyUomId) {
		this.currencyUomId = currencyUomId;
	}

	public String getDisableShipping() {
		return disableShipping;
	}

	public void setDisableShipping(String disableShipping) {
		this.disableShipping = disableShipping;
	}

	public Long getNetPaymentDays() {
		return netPaymentDays;
	}

	public void setNetPaymentDays(Long netPaymentDays) {
		this.netPaymentDays = netPaymentDays;
	}

	public String getShipToCompanyName() {
		return shipToCompanyName;
	}

	public void setShipToCompanyName(String shipToCompanyName) {
		this.shipToCompanyName = shipToCompanyName;
	}

	public String getShipToFirstName() {
		return shipToFirstName;
	}

	public void setShipToFirstName(String shipToFirstName) {
		this.shipToFirstName = shipToFirstName;
	}

	public String getShipToLastName() {
		return shipToLastName;
	}

	public void setShipToLastName(String shipToLastName) {
		this.shipToLastName = shipToLastName;
	}

	public String getShipToAttnName() {
		return shipToAttnName;
	}

	public void setShipToAttnName(String shipToAttnName) {
		this.shipToAttnName = shipToAttnName;
	}

	public String getShipToAddress1() {
		return shipToAddress1;
	}

	public void setShipToAddress1(String shipToAddress1) {
		this.shipToAddress1 = shipToAddress1;
	}

	public String getShipToAddress2() {
		return shipToAddress2;
	}

	public void setShipToAddress2(String shipToAddress2) {
		this.shipToAddress2 = shipToAddress2;
	}

	public String getShipToCity() {
		return shipToCity;
	}

	public void setShipToCity(String shipToCity) {
		this.shipToCity = shipToCity;
	}

	public String getShipToStateProvinceGeoId() {
		return shipToStateProvinceGeoId;
	}

	public void setShipToStateProvinceGeoId(String shipToStateProvinceGeoId) {
		this.shipToStateProvinceGeoId = shipToStateProvinceGeoId;
	}

	public String getShipToPostalCode() {
		return shipToPostalCode;
	}

	public void setShipToPostalCode(String shipToPostalCode) {
		this.shipToPostalCode = shipToPostalCode;
	}

	public String getShipToPostalCodeExt() {
		return shipToPostalCodeExt;
	}

	public void setShipToPostalCodeExt(String shipToPostalCodeExt) {
		this.shipToPostalCodeExt = shipToPostalCodeExt;
	}

	public String getShipToStateProvGeoName() {
		return shipToStateProvGeoName;
	}

	public void setShipToStateProvGeoName(String shipToStateProvGeoName) {
		this.shipToStateProvGeoName = shipToStateProvGeoName;
	}

	public String getShipToCountryGeoId() {
		return shipToCountryGeoId;
	}

	public void setShipToCountryGeoId(String shipToCountryGeoId) {
		this.shipToCountryGeoId = shipToCountryGeoId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPrimaryPartyId() {
		return primaryPartyId;
	}

	public void setPrimaryPartyId(String primaryPartyId) {
		this.primaryPartyId = primaryPartyId;
	}

	public String getCompanyPartyId() {
		return companyPartyId;
	}

	public void setCompanyPartyId(String companyPartyId) {
		this.companyPartyId = companyPartyId;
	}

	public String getPersonPartyId() {
		return personPartyId;
	}

	public void setPersonPartyId(String personPartyId) {
		this.personPartyId = personPartyId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getImportStatusId() {
		return importStatusId;
	}

	public void setImportStatusId(String importStatusId) {
		this.importStatusId = importStatusId;
	}

	public String getImportError() {
		return importError;
	}

	public void setImportError(String importError) {
		this.importError = importError;
	}

	public Timestamp getProcessedTimestamp() {
		return processedTimestamp;
	}

	public void setProcessedTimestamp(Timestamp processedTimestamp) {
		this.processedTimestamp = processedTimestamp;
	}

	public String getIsImportAsContact() {
		return isImportAsContact;
	}

	public void setIsImportAsContact(String isImportAsContact) {
		this.isImportAsContact = isImportAsContact;
	}
	
}
