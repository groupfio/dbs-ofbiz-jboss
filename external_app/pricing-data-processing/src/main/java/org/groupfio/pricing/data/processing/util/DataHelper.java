/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.groupfio.pricing.data.processing.BatchConstants;

/**
 * @author Sharif
 *
 */
public class DataHelper {

	public static boolean isActiveStep(EntityManager entityManager, Long parentJobExecutionId, String stepName) {
    	if (UtilValidate.isNotEmpty(parentJobExecutionId)) {
			Query query = entityManager.createNativeQuery("select write_count from batch_step_execution where step_name=:stepName and job_execution_id=:jobExecutionId");
			query.setParameter("stepName", stepName);
			query.setParameter("jobExecutionId", parentJobExecutionId);
			
			List results = query.getResultList();
			if (UtilValidate.isNotEmpty(results)) {
				Integer writeCount = Integer.parseInt( results.get(0).toString() );
				if (UtilValidate.isNotEmpty(writeCount) && writeCount.intValue() == 0) {
					return false;
				}
				return true;
			}
		}
    	return false;
    }
	
	public static boolean isActiveStep(EntityManager entityManager, String stepName) {
		String tableName = BatchConstants.TABLE_NAME_BY_STEP_NAME.get(stepName);
    	if (UtilValidate.isNotEmpty(tableName)) {
			Query query = entityManager.createNativeQuery("select count(1) from "+tableName);
			//query.setParameter("tableName", tableName);

			BigInteger writeCount = (BigInteger) query.getSingleResult();
			if (UtilValidate.isNotEmpty(writeCount) && writeCount.intValue() == 0) {
				return false;
			}
			return true;
			
		}
    	return false;
    }
	
	public static String sqlPropToJavaProp(String prop) {
		if (UtilValidate.isNotEmpty(prop)) {
			//String prop = "hp_due_wthin_1_yr_amt";
			prop = prop.toLowerCase();
			prop = prop.replace("_1_", "1");
			String convertedString = "";
			for (int i = 0; i < prop.length(); i++) {
				if (prop.charAt(i) == '_') {
					convertedString += (""+prop.charAt(++i)).toUpperCase();
				} else {
					convertedString += prop.charAt(i);
				}
			}
			return convertedString;
		}
		return prop;
	}
	
	public static String javaPropToSqlProp(String prop) {
		if (UtilValidate.isNotEmpty(prop)) {
			for (int i = 0; i < prop.length(); i++) {
				if (Character.isUpperCase(prop.charAt(i))) {
					String additionalChar = (i == 0) ? "" : "_";
					prop = prop.replace("" + prop.charAt(i), additionalChar + ("" + prop.charAt(i)).toLowerCase());
				}
	
			}
		}
		return prop;
	}
	
}
