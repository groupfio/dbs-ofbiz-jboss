package org.groupfio.pricing.data.processing.listener.job;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.groupfio.pricing.data.processing.BatchConstants.BatchJobType;
import org.groupfio.pricing.data.processing.util.BatchUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class JobCompletionCommonNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionCommonNotificationListener.class);

	@Autowired
    private EntityManagerFactory entityManagerFactory;

	@Override
	public void beforeJob(JobExecution jobExecution) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		BatchUtil.trackJobType(entityManager, jobExecution.getId(), BatchJobType.HADOOP);
		entityManager.close();
	}
	
}
