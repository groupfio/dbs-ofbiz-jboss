package org.groupfio.pricing.data.processing.listener.job;

import java.io.File;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.StoredProcedureQuery;

import org.groupfio.pricing.data.processing.BatchConstants.BatchJobType;
import org.groupfio.pricing.data.processing.util.BatchUtil;
import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.StaticResourceLoader;
import org.groupfio.pricing.data.processing.util.UtilDateTime;
import org.groupfio.pricing.data.processing.util.UtilMisc;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    private EntityManagerFactory entityManagerFactory;

	@Autowired
	public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Bean
	public TaskExecutor taskExecutorContactProcedure() {
		SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
		return taskExecutor;
	}
	
	@Override
	public void beforeJob(JobExecution jobExecution) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		BatchUtil.trackJobType(entityManager, jobExecution.getId(), BatchJobType.HADOOP);
		entityManager.close();
	}
	
	@Override
	public void afterJob(JobExecution jobExecution) {
		try {
			if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
				
				log.info("Hadoop import process completed successfully......");
				
				taskExecutorContactProcedure().execute(new Runnable() {

		            @Override

		            public void run() {

		            	EntityManager entityManager = entityManagerFactory.createEntityManager();
		            	
		            	String procedureContactName = PropertiesCache.getInstance().getProperty("procedure.contact.name");
						if (UtilValidate.isNotEmpty(procedureContactName)) {
							StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery(procedureContactName);
							// set parameters
							//storedProcedure.registerStoredProcedureParameter("subtotal", Double.class, ParameterMode.IN);
							//storedProcedure.registerStoredProcedureParameter("tax", Double.class, ParameterMode.OUT);
							//storedProcedure.setParameter("subtotal", 1f);
							// execute SP
							log.info("start execute procedure: "+procedureContactName+", time: "+UtilDateTime.nowTimestamp());
							storedProcedure.execute();
							log.info("end execute procedure: "+procedureContactName+", time: "+UtilDateTime.nowTimestamp());
						}
						
						entityManager.close();
		            }

		        });
				
				/*String localImportLocation = PropertiesCache.getInstance().getProperty("local.import.location");
				
				String localDumpLocation = PropertiesCache.getInstance().getProperty("local.dump.location");
				File targetDumpLocation = new File(localDumpLocation);
				if (!targetDumpLocation.exists()) {
					targetDumpLocation.mkdir();
				}
				
				Resource baseResource = new FileSystemResource(localImportLocation);

				StaticResourceLoader resourceLoader = new StaticResourceLoader();
				resourceLoader.setBaseResource(baseResource);

				Resource[] resources = null;

				ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(resourceLoader);
				resources = patternResolver.getResources("*.csv");
				
				if (UtilValidate.isNotEmpty(resources)) {
					
					for (Resource resource : resources) {
						
						File importFile = resource.getFile();
						UtilMisc.copyFile(importFile, new File(targetDumpLocation.getAbsolutePath() + File.separator + importFile.getName()));
						importFile.delete();
						
					}
					
				}*/
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
