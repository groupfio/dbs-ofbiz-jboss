package org.groupfio.pricing.data.processing.listener.step;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.StaticResourceLoader;
import org.groupfio.pricing.data.processing.util.UtilMisc;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class CaAccountExecutionListener extends StepExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(CaAccountExecutionListener.class);
	
	private StepExecution stepExecution;
	
	@Autowired
    private EntityManagerFactory entityManagerFactory;
	
	@Autowired
    private JobRepository jobRepository;
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		super.beforeStep(stepExecution);
		this.stepExecution = stepExecution;
		
		stepExecution.getExecutionContext().put("hdpCaAccountMultiResourceItemReader.resourceIndex", -1);
        jobRepository.updateExecutionContext(stepExecution);
	}
	
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		try {
			if(stepExecution.getStatus() == BatchStatus.COMPLETED) {
				
				String localImportLocation = PropertiesCache.getInstance().getProperty("local.import.location");
				
				String localDumpLocation = PropertiesCache.getInstance().getProperty("local.dump.location");
				File targetDumpLocation = new File(localDumpLocation);
				if (!targetDumpLocation.exists()) {
					targetDumpLocation.mkdir();
				}
				
				Resource baseResource = new FileSystemResource(localImportLocation);

				StaticResourceLoader resourceLoader = new StaticResourceLoader();
				resourceLoader.setBaseResource(baseResource);

				Resource[] resources = null;

				ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(resourceLoader);
				resources = patternResolver.getResources("*_ca_account*.csv");
				
				if (UtilValidate.isNotEmpty(resources) && resources.length > 0) {
					
					for (Resource resource : resources) {
						
						File importFile = resource.getFile();
						UtilMisc.copyFile(importFile, new File(targetDumpLocation.getAbsolutePath() + File.separator + importFile.getName()));
						importFile.delete();
						
					}
					
					// adjust step statistics [start]
					
					EntityManager entityManager = entityManagerFactory.createEntityManager();
					
					entityManager.getTransaction().begin();
					
					BigInteger actualTotalWriteCount = (BigInteger) entityManager.createNativeQuery("select count(1) from hdp_ca_account").getSingleResult();
					if (UtilValidate.isNotEmpty(actualTotalWriteCount)) {
						Query stepQuery = entityManager.createNativeQuery("update batch_step_execution set actual_write_count=:writeCount where step_execution_id=:stepExecutionId");
						stepQuery.setParameter("stepExecutionId", stepExecution.getId());
						stepQuery.setParameter("writeCount", actualTotalWriteCount);
						stepQuery.executeUpdate();
					}
					
					entityManager.getTransaction().commit();
					
					entityManager.close();
					
					// adjust step statistics [end]
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public StepExecution getStepExecution() {
		return stepExecution;
	}

	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}
	
}
