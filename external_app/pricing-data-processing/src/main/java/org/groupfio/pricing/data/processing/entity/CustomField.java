package org.groupfio.pricing.data.processing.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ @NamedQuery(name = "CustomField.findAll", query = "select p from CustomField p") })
@Table(name = "custom_field")
@Entity
public class CustomField {

	@Id
	private String customFieldId;
	
	private String groupId;
	private String groupType;
	private String groupName;
	private String customFieldFormat;
	private String customFieldName;
	private String customFieldExtName;
	private String locale;
	private String customFieldType;
	private String hide;
	private String isEnabled;
	private String subClassGroupId;
	
	private Long sequenceNumber;
	private Long customFieldLength;
	private Long groupCount;
	
	public CustomField() {
	}
	
	public CustomField(String customFieldId, String customFieldName) {
		this.customFieldId = customFieldId;
		this.customFieldName = customFieldName;
	}

	public String getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getCustomFieldFormat() {
		return customFieldFormat;
	}

	public void setCustomFieldFormat(String customFieldFormat) {
		this.customFieldFormat = customFieldFormat;
	}

	public String getCustomFieldName() {
		return customFieldName;
	}

	public void setCustomFieldName(String customFieldName) {
		this.customFieldName = customFieldName;
	}

	public String getCustomFieldExtName() {
		return customFieldExtName;
	}

	public void setCustomFieldExtName(String customFieldExtName) {
		this.customFieldExtName = customFieldExtName;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCustomFieldType() {
		return customFieldType;
	}

	public void setCustomFieldType(String customFieldType) {
		this.customFieldType = customFieldType;
	}

	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getSubClassGroupId() {
		return subClassGroupId;
	}

	public void setSubClassGroupId(String subClassGroupId) {
		this.subClassGroupId = subClassGroupId;
	}

	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Long getCustomFieldLength() {
		return customFieldLength;
	}

	public void setCustomFieldLength(Long customFieldLength) {
		this.customFieldLength = customFieldLength;
	}

	public Long getGroupCount() {
		return groupCount;
	}

	public void setGroupCount(Long groupCount) {
		this.groupCount = groupCount;
	}

	@Override
	public String toString() {
		return "CustomField [customFieldId=" + customFieldId + ", groupId=" + groupId + ", groupType=" + groupType
				+ ", groupName=" + groupName + ", customFieldFormat=" + customFieldFormat + ", customFieldName="
				+ customFieldName + ", customFieldExtName=" + customFieldExtName + ", locale=" + locale
				+ ", customFieldType=" + customFieldType + ", hide=" + hide + ", isEnabled=" + isEnabled
				+ ", subClassGroupId=" + subClassGroupId + ", sequenceNumber=" + sequenceNumber + ", customFieldLength="
				+ customFieldLength + ", groupCount=" + groupCount + "]";
	}
	
}
