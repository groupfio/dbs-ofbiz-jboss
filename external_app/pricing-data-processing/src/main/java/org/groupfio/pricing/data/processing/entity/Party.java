/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "Party.findAll", query = "select p from Party p"),
	@NamedQuery(name = "Party.findOne", query = "select p from Party p where p.partyId=:partyId")
})
@Table(name = "party")
@Entity
public class Party implements Serializable {
	
	@Id
	private String partyId;
	
	public Party() {
		
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPrimaryIdentifier() {
		return "partyId: "+partyId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " party_Id='"+partyId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
