package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ @NamedQuery(name = "CustomFieldValueConfig.findAll", query = "select p from CustomFieldValueConfig p"),
		@NamedQuery(name = "CustomFieldValueConfig.findOne", query = "select p from CustomFieldValueConfig p where p.groupId=:groupId and p.customFieldId=:customFieldId and p.valueCapture=:valueCapture and p.valueSeqNum=:valueSeqNum") })
@Table(name = "custom_field_value_config")
@Entity
public class CustomFieldValueConfig implements Serializable {
	
	@Id
	private String groupId;
	@Id
	private String customFieldId;
	@Id
	private String valueCapture;
	@Id
	private String valueSeqNum;
	private String valueMin;
	private String valueMax;
	private String valueData;
	private String description;
	private String hide;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}

	public String getValueCapture() {
		return valueCapture;
	}

	public void setValueCapture(String valueCapture) {
		this.valueCapture = valueCapture;
	}

	public String getValueSeqNum() {
		return valueSeqNum;
	}

	public void setValueSeqNum(String valueSeqNum) {
		this.valueSeqNum = valueSeqNum;
	}

	public String getValueMin() {
		return valueMin;
	}

	public void setValueMin(String valueMin) {
		this.valueMin = valueMin;
	}

	public String getValueMax() {
		return valueMax;
	}

	public void setValueMax(String valueMax) {
		this.valueMax = valueMax;
	}

	public String getValueData() {
		return valueData;
	}

	public void setValueData(String valueData) {
		this.valueData = valueData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

}
