package org.groupfio.pricing.data.processing.processor;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.groupfio.pricing.data.processing.entity.HdpCaAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class HdpCaAccountItemProcessor implements ItemProcessor<HdpCaAccount, HdpCaAccount> {

    private static final Logger log = LoggerFactory.getLogger(HdpCaAccountItemProcessor.class);

    @PersistenceContext
	public EntityManager entityManager;
    
    private String batchId;
    
    @Override
    public HdpCaAccount process(final HdpCaAccount entity) throws Exception {
    	
    	entity.setBatchId(batchId);
    	entity.setImportStatusId("DATAIMP_INITIATE");
    	entity.setImportError(null);
    	entity.setProcessedTimestamp(null);
    	
        String lcin = entity.getLcin();
        String acctNum = entity.getAcctNum();
        String cntryCde = entity.getCntryCde();
        String businessDt = entity.getBusinessDt();
        
        log.info("processing HdpCaAccount, lcin: "+lcin+", acctNum: "+acctNum+", cntryCde:"+cntryCde+", businessDt:"+businessDt);

        return entity;
    }
    
	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	
}
