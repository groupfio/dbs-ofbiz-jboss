package org.groupfio.pricing.data.processing.util;

import java.util.Collection;
import java.util.Map;

/**
 * @author sharif
 *
 */
public class ObjectType {

	public static boolean isEmpty(Object value) {
        if (value == null) return true;

        if (value instanceof String) {
            if (((String) value).length() == 0) {
                return true;
            }
        } else if (value instanceof Collection) {
            if (((Collection) value).size() == 0) {
                return true;
            }
        } else if (value instanceof Map) {
            if (((Map) value).size() == 0) {
                return true;
            }
        }
        return false;
    }
	
}
