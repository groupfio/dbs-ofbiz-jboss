package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ @NamedQuery(name = "PartyMetricIndicator.findAll", query = "select p from PartyMetricIndicator p"),
		@NamedQuery(name = "PartyMetricIndicator.findOne", query = "select p from PartyMetricIndicator p where p.groupId=:groupId and p.customFieldId=:customFieldId and p.partyId=:partyId") })
@Table(name = "party_metric_indicator")
@Entity
public class PartyMetricIndicator implements Serializable {
	
	@Id
	private String groupId;
	@Id
	private String customFieldId;
	@Id
	private String partyId;
	private String groupingCode;
	private String propertyName;
	private String propertyValue;
	private Long sequenceNumber;
	
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getGroupingCode() {
		return groupingCode;
	}

	public void setGroupingCode(String groupingCode) {
		this.groupingCode = groupingCode;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}

	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}

	public Timestamp getCreatedStamp() {
		return createdStamp;
	}

	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}
	
	public String getPrimaryIdentifier() {
		return "groupId: "+groupId+", customFieldId:"+customFieldId+", partyId:"+partyId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " group_Id='"+groupId+"' AND custom_Field_Id='"+customFieldId+"' party_Id='"+partyId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
