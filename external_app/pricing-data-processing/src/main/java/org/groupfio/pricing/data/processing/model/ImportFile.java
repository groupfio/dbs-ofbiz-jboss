/**
 * 
 */
package org.groupfio.pricing.data.processing.model;

import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class ImportFile {

	private String localImportLocation;
	private String fileName;
	
	public ImportFile() {}

	public String getLocalImportLocation() {
		return localImportLocation;
	}

	public void setLocalImportLocation(String localImportLocation) {
		this.localImportLocation = localImportLocation;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getAbsoulateFileLocation() {
		
		if (UtilValidate.isNotEmpty(localImportLocation) && UtilValidate.isNotEmpty(fileName)) {
			return localImportLocation + fileName;
		}
		
		return null;
	}
	
}
