package org.groupfio.pricing.data.processing.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ @NamedQuery(name = "SftpConfiguration.findAll", query = "select p from SftpConfiguration p") })
@Table(name = "sftp_configuration")
@Entity
public class SftpConfiguration {

	@Id
	private String seqId;
	
	private String userName;
	private String password;
	private String port;
	private String host;
	private String location;
	private String enable;
	private String modelName;

	public SftpConfiguration() {
	}

	public String getSeqId() {
		return seqId;
	}

	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	@Override
	public String toString() {
		return "SftpConfiguration [seqId=" + seqId + ", userName=" + userName + ", password=" + password + ", port="
				+ port + ", host=" + host + ", location=" + location + ", enable=" + enable + ", modelName=" + modelName
				+ "]";
	}
	
}
