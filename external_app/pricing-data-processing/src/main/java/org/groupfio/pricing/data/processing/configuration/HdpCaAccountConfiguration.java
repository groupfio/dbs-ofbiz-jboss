package org.groupfio.pricing.data.processing.configuration;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.groupfio.pricing.data.processing.BatchConstants;
import org.groupfio.pricing.data.processing.entity.HdpCaAccount;
import org.groupfio.pricing.data.processing.listener.step.CaAccountExecutionListener;
import org.groupfio.pricing.data.processing.processor.HdpCaAccountItemProcessor;
import org.groupfio.pricing.data.processing.skipper.CaAccountFileVerificationSkipper;
import org.groupfio.pricing.data.processing.util.ParamUtil;
import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.StaticResourceLoader;
import org.groupfio.pricing.data.processing.util.UtilMisc;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.groupfio.pricing.data.processing.writer.HdpCaAccountItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

/**
 * @author Sharif
 *
 */
@Configuration
@EnableBatchProcessing
public class HdpCaAccountConfiguration {
	
	private static final Logger log = LoggerFactory.getLogger(HdpCaAccountConfiguration.class);
	
	@Autowired
	private StepBuilderFactory stepBuilder;
	
	@Autowired
    private EntityManagerFactory entityManagerFactory;
    
    @Autowired
    private HdpCaAccountItemProcessor processor;
    
    @Autowired
    private CaAccountExecutionListener stepExecutionListener;
    
    @Value("${max-threads}")
	private int maxThreads;
    
    @Bean
    public SkipPolicy caAccountFileVerificationSkipper() {
    	return new CaAccountFileVerificationSkipper(stepExecutionListener);
    }
    
    @Bean
	public TaskExecutor taskExecutorCaAccount() {
		SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
		taskExecutor.setConcurrencyLimit(maxThreads);
		return taskExecutor;
	}
    
    public Step importStep() {
    	String executeCompleteChunkCount = PropertiesCache.getInstance().getProperty("execute.complete.chunk.count");
    	int chunkCount = BatchConstants.DEFAULT_CHUNK_COUNT;
		if (UtilValidate.isNotEmpty(executeCompleteChunkCount)) {
			chunkCount = Integer.parseInt(executeCompleteChunkCount);
		}
    	processor.setBatchId(UtilMisc.generateBatchId());
        return stepBuilder.get("HdpCaAccountImportStep")
            .<HdpCaAccount, HdpCaAccount>chunk(chunkCount)
            .reader(multiResourceItemReader()).faultTolerant().skipPolicy(caAccountFileVerificationSkipper())
            .processor(processor)
            .writer(writerCaAccount())
            .taskExecutor(taskExecutorCaAccount())
            .throttleLimit(maxThreads)
            .allowStartIfComplete(true)
            .listener(stepExecutionListener)
            .build();
    }
    
    @Bean
	public HdpCaAccountItemWriter writerCaAccount() {
		return new HdpCaAccountItemWriter();
	}
    
	public MultiResourceItemReader<HdpCaAccount> multiResourceItemReader() {
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		MultiResourceItemReader<HdpCaAccount> resourceItemReader = new MultiResourceItemReader<HdpCaAccount>();
		resourceItemReader.setName("hdpCaAccountMultiResourceItemReader");

		String localImportLocation = PropertiesCache.getInstance().getProperty("local.import.location");
		Resource baseResource = new FileSystemResource(localImportLocation);

		StaticResourceLoader resourceLoader = new StaticResourceLoader();
		resourceLoader.setBaseResource(baseResource);

		Resource[] resources = null;

		ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(resourceLoader);
		try {
			resources = patternResolver.getResources("*_ca_account*.csv");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (UtilValidate.isNotEmpty(resources) && resources.length > 0) {
			entityManager.getTransaction().begin();
			Query removeExisting = entityManager.createNativeQuery("DELETE FROM hdp_ca_account");
			int removeCount = removeExisting.executeUpdate();
			log.info("removeCount>> "+removeCount);
			entityManager.getTransaction().commit();
		}
		
		entityManager.close();

		resourceItemReader.setResources(resources);
		resourceItemReader.setDelegate(reader());
		resourceItemReader.setSaveState(false);
		return resourceItemReader;
	}

	public FlatFileItemReader<HdpCaAccount> reader() {
		FlatFileItemReader<HdpCaAccount> reader = new FlatFileItemReader<HdpCaAccount>();
		
		reader.setName("HdpCaAccountItemReader");
    	reader.setEncoding("UTF-8");
    	reader.setLinesToSkip(1);
		
		reader.setLineMapper(new DefaultLineMapper<HdpCaAccount>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer("|") {
					{
						setNames(new String[] {
								"lcin", "srcSysCde", "acctNum", "acctCcyCde", "acctCcyAmt", "acctCcyAvgAmt", "glAccountAlt", "prinGlNbr", "glProduct", "hypProduct", "acctStatus", "opDt", "valDt", "branchCde", "schmTyp", "schmCde", "acctClDt", "legalEntity", "localCcyCde", "acctName", "schmDesc", "rm", "intBasis", "baseCcyCde", "bsType", "intPeriodicity", "payDayConvention", "dealRelType", "pcCode", "bookType", "earmarkAmt", "earmarkCode", "salCrdDbtInd", "stdInstrInd", "clrWthDbsInd", "cashMngtInd", "cashMngtFee", "acctOwnership", "priAcctInd", "numAcctHolders", "staffFlag", "freezeCode", "modeOfOpertaion", "holdFundDate", "securedAgainstColletaral", "chequeFacility", "calcDayConvention", "glBu", "lglEntLvl4", "drvFundUser", "procSrcSysCde", "contractRefAddt1", "contractRefAddt2", "customerRef1", "customerRef2", "customerRef3", "giroInd", "finCif", "dpsExclusionInd", "programCode", "branchDeptCode", "accountDesc", "productDesc", "loadDate", "businessDt", "cntryCde"
								
						});
					}
				});
				
				setFieldSetMapper(fields -> {
					
					HdpCaAccount entity = new HdpCaAccount();
					
					entity.setLcin(ParamUtil.getString(fields, "lcin"));
					entity.setSrcSysCde(ParamUtil.getString(fields, "srcSysCde"));
					entity.setAcctNum(ParamUtil.getString(fields, "acctNum"));
					entity.setAcctCcyCde(ParamUtil.getString(fields, "acctCcyCde"));
					entity.setAcctCcyAmt(ParamUtil.getBigDecimal(fields, "acctCcyAmt"));
					entity.setAcctCcyAvgAmt(ParamUtil.getBigDecimal(fields, "acctCcyAvgAmt"));
					entity.setGlAccountAlt(ParamUtil.getString(fields, "glAccountAlt"));
					entity.setPrinGlNbr(ParamUtil.getString(fields, "prinGlNbr"));
					entity.setGlProduct(ParamUtil.getString(fields, "glProduct"));
					entity.setHypProduct(ParamUtil.getString(fields, "hypProduct"));
					entity.setAcctStatus(ParamUtil.getString(fields, "acctStatus"));
					entity.setOpDt(ParamUtil.getString(fields, "opDt"));
					entity.setValDt(ParamUtil.getString(fields, "valDt"));
					entity.setBranchCde(ParamUtil.getLong(fields, "branchCde"));
					entity.setSchmTyp(ParamUtil.getLong(fields, "schmTyp"));
					entity.setSchmCde(ParamUtil.getLong(fields, "schmCde"));
					entity.setAcctClDt(ParamUtil.getString(fields, "acctClDt"));
					entity.setLegalEntity(ParamUtil.getLong(fields, "legalEntity"));
					entity.setLocalCcyCde(ParamUtil.getString(fields, "localCcyCde"));
					entity.setAcctName(ParamUtil.getString(fields, "acctName"));
					entity.setSchmDesc(ParamUtil.getString(fields, "schmDesc"));
					entity.setRm(ParamUtil.getString(fields, "rm"));
					entity.setIntBasis(ParamUtil.getString(fields, "intBasis"));
					entity.setBaseCcyCde(ParamUtil.getString(fields, "baseCcyCde"));
					entity.setBsType(ParamUtil.getString(fields, "bsType"));
					entity.setIntPeriodicity(ParamUtil.getString(fields, "intPeriodicity"));
					entity.setPayDayConvention(ParamUtil.getLong(fields, "payDayConvention"));
					entity.setDealRelType(ParamUtil.getString(fields, "dealRelType"));
					entity.setPcCode(ParamUtil.getLong(fields, "pcCode"));
					entity.setBookType(ParamUtil.getString(fields, "bookType"));
					entity.setEarmarkAmt(ParamUtil.getBigDecimal(fields, "earmarkAmt"));
					entity.setEarmarkCode(ParamUtil.getString(fields, "earmarkCode"));
					entity.setSalCrdDbtInd(ParamUtil.getString(fields, "salCrdDbtInd"));
					entity.setStdInstrInd(ParamUtil.getString(fields, "stdInstrInd"));
					entity.setClrWthDbsInd(ParamUtil.getString(fields, "clrWthDbsInd"));
					entity.setCashMngtInd(ParamUtil.getString(fields, "cashMngtInd"));
					entity.setCashMngtFee(ParamUtil.getString(fields, "cashMngtFee"));
					entity.setAcctOwnership(ParamUtil.getString(fields, "acctOwnership"));
					entity.setPriAcctInd(ParamUtil.getString(fields, "priAcctInd"));
					entity.setNumAcctHolders(ParamUtil.getLong(fields, "numAcctHolders"));
					entity.setStaffFlag(ParamUtil.getString(fields, "staffFlag"));
					entity.setFreezeCode(ParamUtil.getLong(fields, "freezeCode"));
					entity.setModeOfOpertaion(ParamUtil.getLong(fields, "modeOfOpertaion"));
					entity.setHoldFundDate(ParamUtil.getString(fields, "holdFundDate"));
					entity.setSecuredAgainstColletaral(ParamUtil.getString(fields, "securedAgainstColletaral"));
					entity.setChequeFacility(ParamUtil.getString(fields, "chequeFacility"));
					entity.setCalcDayConvention(ParamUtil.getLong(fields, "calcDayConvention"));
					entity.setGlBu(ParamUtil.getString(fields, "glBu"));
					entity.setLglEntLvl4(ParamUtil.getString(fields, "lglEntLvl4"));
					entity.setDrvFundUser(ParamUtil.getString(fields, "drvFundUser"));
					entity.setProcSrcSysCde(ParamUtil.getString(fields, "procSrcSysCde"));
					entity.setContractRefAddt1(ParamUtil.getString(fields, "contractRefAddt1"));
					entity.setContractRefAddt2(ParamUtil.getString(fields, "contractRefAddt2"));
					entity.setCustomerRef1(ParamUtil.getString(fields, "customerRef1"));
					entity.setCustomerRef2(ParamUtil.getString(fields, "customerRef2"));
					entity.setCustomerRef3(ParamUtil.getString(fields, "customerRef3"));
					entity.setGiroInd(ParamUtil.getString(fields, "giroInd"));
					entity.setFinCif(ParamUtil.getString(fields, "finCif"));
					entity.setDpsExclusionInd(ParamUtil.getString(fields, "dpsExclusionInd"));
					entity.setProgramCode(ParamUtil.getString(fields, "programCode"));
					entity.setBranchDeptCode(ParamUtil.getString(fields, "branchDeptCode"));
					entity.setLoadDate(ParamUtil.getString(fields, "loadDate"));
					entity.setBusinessDt(ParamUtil.getString(fields, "businessDt"));
					entity.setCntryCde(ParamUtil.getString(fields, "cntryCde"));
					
					entity.setAccountDesc(ParamUtil.getString(fields, "accountDesc"));
					entity.setProductDesc(ParamUtil.getString(fields, "productDesc"));
					
					return entity;
					
				});
				
				/*
				setFieldSetMapper(new BeanWrapperFieldSetMapper<HdpCaAccount>() {
					{
						setTargetType(HdpCaAccount.class);
					}
				});
				*/
			}
		});
		
		return reader;
	}
    
}
