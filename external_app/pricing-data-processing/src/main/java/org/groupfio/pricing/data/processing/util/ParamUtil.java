package org.groupfio.pricing.data.processing.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.groupfio.pricing.data.processing.BatchConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.boot.ApplicationArguments;

/**
 * @author Sharif
 *
 */
public class ParamUtil {
	
	private static final Logger log = LoggerFactory.getLogger(ParamUtil.class);

	public static long getLong (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return Long.valueOf(""+context.get(key));
		}
		return 0;
	}
	public static Long getLong (FieldSet fields, String key) {
		try {
			if (UtilValidate.isNotEmpty(fields) && UtilValidate.isNotEmpty(fields.getProperties().containsKey(key))) {
				return fields.readLong(key);
			}
		} catch (Exception e) {
			//log.error(e.getMessage());
		}
		return null;
	}
	
	public static BigDecimal getBigDecimal (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return BigDecimal.valueOf(Double.valueOf(""+context.get(key)));
		}
		return null;
	}
	public static BigDecimal getBigDecimal (FieldSet fields, String key) {
		try {
			if (UtilValidate.isNotEmpty(fields) && UtilValidate.isNotEmpty(fields.getProperties().containsKey(key))) {
				return fields.readBigDecimal(key);
			}
		} catch (Exception e) {
			//log.error(e.getMessage());
		}
		return null;
	}
	
	public static boolean getBoolean (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return Boolean.valueOf(""+context.get(key));
		}
		return false;
	}
	
	public static String getString (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return String.valueOf(""+context.get(key));
		}
		return "";
	}
	public static String getString (FieldSet fields, String key) {
		try {
			if (UtilValidate.isNotEmpty(fields) && UtilValidate.isNotEmpty(fields.getProperties().containsKey(key))) {
				return fields.readString(key);
			}
		} catch (Exception e) {
			//log.error(e.getMessage());
		}
		return null;
	}
	
	public static int getInteger (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return Integer.valueOf(""+context.get(key));
		}
		return 0;
	}
	
	public static Double getDouble (Map<String, Object> context, String key) {
		if (UtilValidate.isNotEmpty(context) && UtilValidate.isNotEmpty(context.get(key))) {
			return Double.valueOf(""+context.get(key));
		}
		return 0.0;
	}
	
	public static Map<String, Object> prepareObjectParams (Map<String, Object> context) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		for (String key : context.keySet()) {
			if (UtilValidate.isNotEmpty(context.get(key))) {
				params.put(key, context.get(key));
			}
		}
		return params;
	} 
	
	public static Map<String, String> prepareStringParams (Map<String, String> context) {
		Map<String, String> params = new HashMap<String, String>();
		
		for (String key : context.keySet()) {
			if (UtilValidate.isNotEmpty(context.get(key))) {
				params.put(key, context.get(key));
			}
		}
		return params;
	} 
	
	
	public static String getRestString (String value) {
		if(UtilValidate.isEmpty(value)) {
			return "'"+"'";
		}
		return value;
	} 
	
	public static String getServiceStatus (String activeServiceName, ApplicationArguments appArgs) {
		
		if (UtilValidate.isNotEmpty(activeServiceName)) {
			boolean containsOption = appArgs.containsOption("service.name");
			
			String activeServiceStatus = PropertiesCache.getInstance().getProperty(activeServiceName);
			if (UtilValidate.isNotEmpty(activeServiceStatus) && activeServiceStatus.equals("Y")) {
				return "Y";
			} else if (containsOption) {
				List<String> argOptions =  appArgs.getOptionValues("service.name");
				if (UtilValidate.isNotEmpty(argOptions)) {
					for (String argOption : argOptions) {
						String configServiceName = BatchConstants.SERVICE_NAME_BY_CONFIG.get(activeServiceName);
						if (UtilValidate.isNotEmpty(argOption) && UtilValidate.isNotEmpty(configServiceName) && argOption.equals(configServiceName)) {
							return "Y";
						}
					}
				}
			}
			
		}
		
		return "N";
	}
	
}
