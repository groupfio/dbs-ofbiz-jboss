package org.groupfio.pricing.data.processing.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.groupfio.pricing.data.processing.decider.AssocJobFlowDecision;
import org.groupfio.pricing.data.processing.listener.job.JobCompletionCommonNotificationListener;
import org.groupfio.pricing.data.processing.listener.job.JobCompletionNotificationListener;
import org.groupfio.pricing.data.processing.task.FileDownloadImmediateTask;
import org.groupfio.pricing.data.processing.task.FileDownloadTask;
import org.groupfio.pricing.data.processing.util.DataHelper;
import org.groupfio.pricing.data.processing.util.ParamUtil;
import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 * @author Sharif
 *
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
	
	private static final Logger log = LoggerFactory.getLogger(BatchConfiguration.class);

    @Autowired
    private JobBuilderFactory jobBuilder;

    @Autowired
    private StepBuilderFactory stepBuilder;
    
    @Autowired
    private JobCompletionNotificationListener completeNotification;
    @Autowired
    private JobCompletionCommonNotificationListener commonNotification;
    
    @Autowired
    private FileDownloadTask fileDownloadTask;
    @Autowired
    private FileDownloadImmediateTask fileDownloadImmediateTask;
    
    @Autowired
    private HdpCaAccountConfiguration hdpCaAccountConfiguration;
    
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private ApplicationArguments appArgs;
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    
    @Bean
    public boolean executeJobs() {
    	
    	log.info("executeJobs....................");
    	
    	try {
    		
    		String executeCompleteImport = ParamUtil.getServiceStatus("execute.complete.import", appArgs);
    		
    		if (UtilValidate.isNotEmpty(executeCompleteImport) && executeCompleteImport.equals("Y")) {
    			JobExecution jobExecution = jobLauncher.run(importHadoopJob(), new JobParameters());
    			
    		} 
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return true;
    }
    
    public Job importHadoopJob() {
    	
    	String isSftpEnabled = PropertiesCache.getInstance().getProperty("is.sftp.enabled");
    	if (UtilValidate.isNotEmpty(isSftpEnabled) && isSftpEnabled.equals("Y")) {
    		fileDownloadImmediateTask.execute();
    	}
    	
    	Flow masterFlow = new FlowBuilder<Flow>("masterFlow").start(hdpCaAccountConfiguration.importStep()).build();
    	
    	List<Flow> flows = new ArrayList<Flow>();
    	
    	flows.add(masterFlow);
    	
    	Flow slaveFlow = new FlowBuilder<Flow>("slaveFlow").split(new SimpleAsyncTaskExecutor()).add(flows.toArray(new Flow[flows.size()])).build();
    	
    	return (jobBuilder.get("importHadoopJob")
                .incrementer(new RunIdIncrementer())
                .listener(completeNotification)
                .start(slaveFlow)
                .build()).build();
    	
    	/*return (jobBuilder.get("importHadoopJob")
	                .incrementer(new RunIdIncrementer())
	                .start(masterFlow)
	                .next(slaveFlow)
	                .build()).build();*/
    	
    	/*return jobBuilder.get("importHadoopJob")
        		.incrementer(new RunIdIncrementer())
        	.listener(completeNotification)
            //.start(customFieldConfiguration.importStep())
            .start(hdpCustomerSmeConfiguration.importStep())
            //.next(hdpCustomerConfiguration.importStep())
            .next(hdpCaAccountConfiguration.importStep())
            .next(hdpFacilitiesConfiguration.importStep())
            .next(hdpFdAccountConfiguration.importStep())
            .next(hdpLoanAccountConfiguration.importStep())
            .next(hdpTradeAccountConfiguration.importStep())
            .next(hdpContactConfiguration.importStep())
            .next(hdpGcinGpinLinkageConfiguration.importStep())
            .build();*/
    	
    }
    
    public Step fileDownloadStep() {
        return stepBuilder.get("fileDownloadStep")
            .tasklet(fileDownloadTask)
            .allowStartIfComplete(false)
            .build();
    }
    
}
