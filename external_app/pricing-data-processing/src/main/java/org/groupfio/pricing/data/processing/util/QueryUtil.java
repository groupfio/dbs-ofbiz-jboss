/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import java.util.Collection;

import javax.persistence.Query;

/**
 * @author Sharif
 *
 */
public class QueryUtil {

	public static Object getSingleResult(Query query) {
		Object entity = null;
		try {
			//entity = (Object) query.getSingleResult();
			Collection<?> entityList = query.getResultList();
			if (UtilValidate.isNotEmpty(entityList)) {
				entity = entityList.toArray()[0];
			}
		} catch (Exception e) {
		}
		return entity;
	}
	
}
