/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Id;

/**
 * @author Sharif
 *
 */
@Embeddable
public class DataImportCustomerId implements Serializable {

	private String batchId;
	private String customerId;
	
	public DataImportCustomerId() {
		
	}
	
	public DataImportCustomerId(String batchId, String customerId) {
        this.batchId = batchId;
        this.customerId = customerId;
    }

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
}
