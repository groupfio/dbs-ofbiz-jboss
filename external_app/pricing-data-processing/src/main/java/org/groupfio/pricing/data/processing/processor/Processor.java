/**
 * 
 */
package org.groupfio.pricing.data.processing.processor;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Sharif
 *
 */
public abstract class Processor {

	private static final Logger log = LoggerFactory.getLogger(Processor.class);
	
	private Map<String, Object> context;

	protected abstract Map<String, Object> doProcess(Map<String, Object> context) throws Exception;

	public Map<String, Object> process(Map<String, Object> context){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = doProcess(context);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}
	
}
