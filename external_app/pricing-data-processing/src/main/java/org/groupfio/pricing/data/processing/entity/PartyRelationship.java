/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "PartyRelationship.findAll", query = "select p from PartyRelationship p"),
	@NamedQuery(name = "PartyRelationship.findOne", query = "select p from PartyRelationship p where p.partyIdFrom=:partyIdFrom AND p.partyIdTo=:partyIdTo AND p.roleTypeIdFrom=:roleTypeIdFrom AND p.roleTypeIdTo=:roleTypeIdTo AND p.fromDate=:fromDate")
})
@Table(name = "party_relationship")
@Entity
public class PartyRelationship implements Serializable {
	
	@Id
	private String partyIdFrom;
	@Id
	private String partyIdTo;
	@Id
	private String roleTypeIdFrom;
	@Id
	private String roleTypeIdTo;
	private String partyRelationshipTypeId;
	private String securityGroupId;
	private String createdByUserLoginId;
	
	@Id
	private Timestamp fromDate;
	private Timestamp thruDate;
	
	public PartyRelationship() {
		
	}
	
	public String getPartyIdFrom() {
		return partyIdFrom;
	}

	public void setPartyIdFrom(String partyIdFrom) {
		this.partyIdFrom = partyIdFrom;
	}

	public String getPartyIdTo() {
		return partyIdTo;
	}

	public void setPartyIdTo(String partyIdTo) {
		this.partyIdTo = partyIdTo;
	}

	public String getRoleTypeIdFrom() {
		return roleTypeIdFrom;
	}

	public void setRoleTypeIdFrom(String roleTypeIdFrom) {
		this.roleTypeIdFrom = roleTypeIdFrom;
	}

	public String getRoleTypeIdTo() {
		return roleTypeIdTo;
	}

	public void setRoleTypeIdTo(String roleTypeIdTo) {
		this.roleTypeIdTo = roleTypeIdTo;
	}

	public String getPartyRelationshipTypeId() {
		return partyRelationshipTypeId;
	}

	public void setPartyRelationshipTypeId(String partyRelationshipTypeId) {
		this.partyRelationshipTypeId = partyRelationshipTypeId;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getCreatedByUserLoginId() {
		return createdByUserLoginId;
	}

	public void setCreatedByUserLoginId(String createdByUserLoginId) {
		this.createdByUserLoginId = createdByUserLoginId;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}
	
	public Timestamp getThruDate() {
		return thruDate;
	}

	public void setThruDate(Timestamp thruDate) {
		this.thruDate = thruDate;
	}

	public String getPrimaryIdentifier() {
		return "partyIdFrom: "+partyIdFrom+", partyIdTo: "+partyIdTo+", roleTypeIdFrom: "+roleTypeIdFrom+", roleTypeIdTo: "+roleTypeIdTo+", fromDate: "+fromDate;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return null;
		//return " partyIdFrom='"+partyIdFrom+"'"+" partyIdTo='"+partyIdTo+"'"+" roleTypeIdFrom='"+roleTypeIdFrom+"'"+" partyIdFrom='"+partyIdFrom+"'"+" partyIdFrom='"+partyIdFrom+"'"+" partyIdFrom='"+partyIdFrom+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
