/**
 * 
 */
package org.groupfio.pricing.data.processing.decider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component("AssocJobFlowDecision")
public class AssocJobFlowDecision implements JobExecutionDecider {
	
	
	
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        
		
                
        return FlowExecutionStatus.COMPLETED;           
        //return FlowExecutionStatus.FAILED;
    }

}
