/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "CustomFieldGroup.findAll", query = "select p from CustomFieldGroup p"),
	@NamedQuery(name = "CustomFieldGroup.findOne", query = "select p from CustomFieldGroup p where p.groupId=:groupId")
})
@Table(name = "custom_field_group")
@Entity
public class CustomFieldGroup implements Serializable {
	
	@Id
	private String groupId;
	
	private String groupName;
	private Long sequence;
	private String groupType;
	private String parentId;
	private String levelNum;
	private String groupingCode;
	private String serviceName;
	private String serviceTypeId;
	private String serviceConfigId;
	private String historicalCapture;
	private String valueCapture;
	private String isCampaignUse;
	private String classType;
	
	private String type;
	private String isActive;
	private String isUseDynamicEntity;
	private String hide;

	public CustomFieldGroup() {
		
	}
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getLevelNum() {
		return levelNum;
	}

	public void setLevelNum(String levelNum) {
		this.levelNum = levelNum;
	}

	public String getGroupingCode() {
		return groupingCode;
	}

	public void setGroupingCode(String groupingCode) {
		this.groupingCode = groupingCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getServiceConfigId() {
		return serviceConfigId;
	}

	public void setServiceConfigId(String serviceConfigId) {
		this.serviceConfigId = serviceConfigId;
	}

	public String getHistoricalCapture() {
		return historicalCapture;
	}

	public void setHistoricalCapture(String historicalCapture) {
		this.historicalCapture = historicalCapture;
	}

	public String getValueCapture() {
		return valueCapture;
	}

	public void setValueCapture(String valueCapture) {
		this.valueCapture = valueCapture;
	}

	public String getIsCampaignUse() {
		return isCampaignUse;
	}

	public void setIsCampaignUse(String isCampaignUse) {
		this.isCampaignUse = isCampaignUse;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsUseDynamicEntity() {
		return isUseDynamicEntity;
	}

	public void setIsUseDynamicEntity(String isUseDynamicEntity) {
		this.isUseDynamicEntity = isUseDynamicEntity;
	}
	
	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	public String getPrimaryIdentifier() {
		return "groupId: "+groupId;
	}
	
}
