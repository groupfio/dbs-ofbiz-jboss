/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * @author Sharif
 *
 */
public class BatchUtil {

	public static boolean trackJobType(EntityManager entityManager, Long jobExecutionId, String batchJobType) {
		
		try {
			if (UtilValidate.isNotEmpty(batchJobType)) {
				entityManager.getTransaction().begin();
				
				Query jobQuery = entityManager.createNativeQuery("update batch_job_execution set batch_job_type=:batchJobType where job_execution_id=:jobExecutionId");
				jobQuery.setParameter("jobExecutionId", jobExecutionId);
				jobQuery.setParameter("batchJobType", batchJobType);
				jobQuery.executeUpdate();
				
				entityManager.getTransaction().commit();
			}
		} catch (Exception e) {
			return false;
		}
		
		return true;
		
	}
	
}
