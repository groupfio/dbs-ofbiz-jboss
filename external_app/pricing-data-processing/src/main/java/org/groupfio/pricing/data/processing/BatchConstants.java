/**
 * 
 */
package org.groupfio.pricing.data.processing;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sharif
 *
 */
public class BatchConstants {

	public static final int DEFAULT_CHUNK_COUNT = 1000;
	
	public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_MESSAGE = "message";
	
	public static final Map<String, String> SERVICE_NAME_BY_CONFIG = 
	   		 Collections.unmodifiableMap(new HashMap<String, String>() {{ 
	    	        put("execute.complete.import", "completeImport");
	   		 }});
	
	public static final class ErrorType {
        private ErrorType() { }
        public static final String DUPLICATE = "DUPLICATE";
        public static final String IGNORE = "IGNORE";
        public static final String PARSING = "PARSING";
        public static final String UNKNOWN = "UNKNOWN";
    }
	
	public static final class GroupType {
        private GroupType() { }
        public static final String CUSTOM_FIELD = "CUSTOM_FIELD";
        public static final String SEGMENTATION = "SEGMENTATION";
        public static final String ATTRIBUTE = "ATTRIBUTE";
        public static final String ECONOMIC_METRIC = "ECONOMIC_METRIC";
    }
	
	public static final class BatchJobType {
        private BatchJobType() { }
        public static final String HADOOP = "HADOOP";
        public static final String MICRO_SERVICE = "MICRO_SERVICE";
        public static final String OFBIZ_DATA = "OFBIZ_DATA";
        public static final String PRICING_ENGINE = "PRICING_ENGINE";
    }
	
	public static final Map<String, String> TABLE_NAME_BY_STEP_NAME = 
	   		 Collections.unmodifiableMap(new HashMap<String, String>() {{ 
	    	        put("HdpCaAccountImportStep", "hdp_ca_account");
	   		 }});
	
	public static final Map<String, String> ENTITY_NAME_BY_FILE_TYPE = 
	   		 Collections.unmodifiableMap(new HashMap<String, String>() {{ 
	    	        put("caAccount", "HdpCaAccount");
	   		 }});
	
}
