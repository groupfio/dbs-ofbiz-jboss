/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sharif
 *
 */
public class UtilMisc {

	public static void copyFile(File sourceLocation , File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            throw new IOException("File is a directory, not a file, cannot copy") ;
        } else {

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }
	
	public static String generateBatchId () {
		
		Date date = new Date();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHMMSS");
	    String batchId = dateFormat.format(date);
		
	    return batchId;
	}
	
}
