/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "CustomFieldMultiValue.findAll", query = "select p from CustomFieldMultiValue p"),
	@NamedQuery(name = "CustomFieldMultiValue.findOne", query = "select p from CustomFieldMultiValue p where p.customFieldId=:customFieldId and p.multiValueId=:multiValueId")
})
@Table(name = "custom_field_multi_value")
@Entity
public class CustomFieldMultiValue implements Serializable {
	
	@Id
	private String customFieldId;
	@Id
	private String multiValueId;
	private String fieldValue;
	private String description;
	private String hide;
	private Long sequenceNumber;
	
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;

	public CustomFieldMultiValue() {
		
	}
	
	public String getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}

	public String getMultiValueId() {
		return multiValueId;
	}

	public void setMultiValueId(String multiValueId) {
		this.multiValueId = multiValueId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}

	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}

	public Timestamp getCreatedStamp() {
		return createdStamp;
	}

	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}

	public String getPrimaryIdentifier() {
		return "customFieldId: "+customFieldId+", multiValueId:"+multiValueId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " custom_Field_Id='"+customFieldId+"' AND multi_Value_Id='"+multiValueId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
