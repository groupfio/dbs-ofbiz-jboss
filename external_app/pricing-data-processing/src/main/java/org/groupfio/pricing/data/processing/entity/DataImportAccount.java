/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "DataImportAccount.findAll", query = "select p from DataImportAccount p"),
	@NamedQuery(name = "DataImportAccount.findOne", query = "select p from DataImportAccount p where p.accountId=:accountId")
})
@Table(name = "data_import_account")
@Entity
public class DataImportAccount implements Serializable {
	
	@Id
	private String batchId;
	@Id
	private String accountId;
	
	private String accountName;
	private String attnName;
	private String address1;
	private String address2;
	private String city;
	private String stateProvinceGeoId;
	private String postalCode;
	private String postalCodeExt;
	private String stateProvinceGeoName;
	private String countryGeoId;
	private String primaryPhoneCountryCode;
	private String primaryPhoneAreaCode;
	private String primaryPhoneNumber;
	private String primaryPhoneExtension;
	private String secondaryPhoneCountryCode;
	private String secondaryPhoneAreaCode;
	private String secondaryPhoneNumber;
	private String secondaryPhoneExtension;
	private String faxCountryCode;
	private String faxAreaCode;
	private String faxNumber;
	private String didCountryCode;
	private String didAreaCode;
	private String didNumber;
	private String didExtension;
	private String emailAddress;
	private String webAddress;
	private String note;
	private String primaryPartyId;
	private String source;
	
	private String importStatusId;
	private String importError;
	private Timestamp processedTimestamp;
	
	public DataImportAccount() {}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAttnName() {
		return attnName;
	}

	public void setAttnName(String attnName) {
		this.attnName = attnName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvinceGeoId() {
		return stateProvinceGeoId;
	}

	public void setStateProvinceGeoId(String stateProvinceGeoId) {
		this.stateProvinceGeoId = stateProvinceGeoId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPostalCodeExt() {
		return postalCodeExt;
	}

	public void setPostalCodeExt(String postalCodeExt) {
		this.postalCodeExt = postalCodeExt;
	}

	public String getStateProvinceGeoName() {
		return stateProvinceGeoName;
	}

	public void setStateProvinceGeoName(String stateProvinceGeoName) {
		this.stateProvinceGeoName = stateProvinceGeoName;
	}

	public String getCountryGeoId() {
		return countryGeoId;
	}

	public void setCountryGeoId(String countryGeoId) {
		this.countryGeoId = countryGeoId;
	}

	public String getPrimaryPhoneCountryCode() {
		return primaryPhoneCountryCode;
	}

	public void setPrimaryPhoneCountryCode(String primaryPhoneCountryCode) {
		this.primaryPhoneCountryCode = primaryPhoneCountryCode;
	}

	public String getPrimaryPhoneAreaCode() {
		return primaryPhoneAreaCode;
	}

	public void setPrimaryPhoneAreaCode(String primaryPhoneAreaCode) {
		this.primaryPhoneAreaCode = primaryPhoneAreaCode;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getPrimaryPhoneExtension() {
		return primaryPhoneExtension;
	}

	public void setPrimaryPhoneExtension(String primaryPhoneExtension) {
		this.primaryPhoneExtension = primaryPhoneExtension;
	}

	public String getSecondaryPhoneCountryCode() {
		return secondaryPhoneCountryCode;
	}

	public void setSecondaryPhoneCountryCode(String secondaryPhoneCountryCode) {
		this.secondaryPhoneCountryCode = secondaryPhoneCountryCode;
	}

	public String getSecondaryPhoneAreaCode() {
		return secondaryPhoneAreaCode;
	}

	public void setSecondaryPhoneAreaCode(String secondaryPhoneAreaCode) {
		this.secondaryPhoneAreaCode = secondaryPhoneAreaCode;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String getSecondaryPhoneExtension() {
		return secondaryPhoneExtension;
	}

	public void setSecondaryPhoneExtension(String secondaryPhoneExtension) {
		this.secondaryPhoneExtension = secondaryPhoneExtension;
	}

	public String getFaxCountryCode() {
		return faxCountryCode;
	}

	public void setFaxCountryCode(String faxCountryCode) {
		this.faxCountryCode = faxCountryCode;
	}

	public String getFaxAreaCode() {
		return faxAreaCode;
	}

	public void setFaxAreaCode(String faxAreaCode) {
		this.faxAreaCode = faxAreaCode;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getDidCountryCode() {
		return didCountryCode;
	}

	public void setDidCountryCode(String didCountryCode) {
		this.didCountryCode = didCountryCode;
	}

	public String getDidAreaCode() {
		return didAreaCode;
	}

	public void setDidAreaCode(String didAreaCode) {
		this.didAreaCode = didAreaCode;
	}

	public String getDidNumber() {
		return didNumber;
	}

	public void setDidNumber(String didNumber) {
		this.didNumber = didNumber;
	}

	public String getDidExtension() {
		return didExtension;
	}

	public void setDidExtension(String didExtension) {
		this.didExtension = didExtension;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getImportStatusId() {
		return importStatusId;
	}

	public void setImportStatusId(String importStatusId) {
		this.importStatusId = importStatusId;
	}

	public String getImportError() {
		return importError;
	}

	public void setImportError(String importError) {
		this.importError = importError;
	}

	public String getPrimaryPartyId() {
		return primaryPartyId;
	}

	public void setPrimaryPartyId(String primaryPartyId) {
		this.primaryPartyId = primaryPartyId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Timestamp getProcessedTimestamp() {
		return processedTimestamp;
	}

	public void setProcessedTimestamp(Timestamp processedTimestamp) {
		this.processedTimestamp = processedTimestamp;
	}
	
}
