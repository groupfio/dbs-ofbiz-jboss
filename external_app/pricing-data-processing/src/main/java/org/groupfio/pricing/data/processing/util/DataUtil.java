/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Sharif
 *
 */
public class DataUtil {
	
	private static final Logger log = LoggerFactory.getLogger(DataUtil.class);

	public static String createRecordIdentifier(String entityName, Object data) {
		if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpGcinGpinLinkage")) {
			return " AND gcin=:gcin";
		} else if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpContact") && data.getClass().getSimpleName().equals("HdpContact")) {
			return " AND lcin=:lcin AND rpIdentityno=:rpIdentityno AND rpDesignation=:rpDesignation";
		}
		return " AND lcin=:lcin";
	}
	
	public static void prepareQueryParams(String entityName, Object data, Query query) {
		if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpGcinGpinLinkage")) {
			query.setParameter("gcin", DataUtil.getFieldValue(data, "gcin"));
		} else if (UtilValidate.isNotEmpty(entityName) && entityName.equals("HdpContact") && data.getClass().getSimpleName().equals("HdpContact")) {
			query.setParameter("lcin", DataUtil.getFieldValue(data, "lcin"));
			query.setParameter("rpIdentityno", DataUtil.getFieldValue(data, "rpIdentityno"));
			query.setParameter("rpDesignation", DataUtil.getFieldValue(data, "rpDesignation"));
		} else {
			query.setParameter("lcin", DataUtil.getFieldValue(data, "lcin"));
		}
	}
	
	public static Object getFieldValue(Object data, String fieldName) {
		
		try {
			
			Object value = new PropertyDescriptor(fieldName, data.getClass()).getReadMethod().invoke(data);
			
			return value;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
