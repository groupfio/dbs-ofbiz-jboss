/**
 * 
 */
package org.groupfio.pricing.data.processing.task;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.groupfio.pricing.data.processing.entity.SftpConfiguration;
import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.SftpUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class FileDownloadTask implements Tasklet, StepExecutionListener {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private static final Logger log = LoggerFactory.getLogger(FileDownloadTask.class);

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		String localImportLocation = PropertiesCache.getInstance().getProperty("local.import.location");

		log.info("Start time to download file:::::::::::::::::::: "+ new Date());
    	log.info("\n\n\n");
    	
    	File targetLocalLocation = new File(localImportLocation);
		if (!targetLocalLocation.exists()) {
			targetLocalLocation.mkdir();
		}
    	
    	SftpConfiguration sftpConfiguration = entityManager.find(SftpConfiguration.class, "attribute-default");
    	log.info("sftpConfiguration>> "+sftpConfiguration);
    	
    	SftpUtility sftp = new SftpUtility(sftpConfiguration.getHost(), Integer.parseInt(sftpConfiguration.getPort()), sftpConfiguration.getUserName(), sftpConfiguration.getPassword(), "/");
        sftp.setCreateDir(true);
        
        List processedFiles = sftp.readFileFromDir(sftpConfiguration.getLocation(), targetLocalLocation.getAbsolutePath());
        
        log.info("processedFiles>> "+processedFiles);
        
        log.info("End time to download file:::::::::::::::::::: "+ new Date());
        log.info("\n\n\n");
		
		return RepeatStatus.FINISHED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		
		
		return ExitStatus.COMPLETED;
	}

}
