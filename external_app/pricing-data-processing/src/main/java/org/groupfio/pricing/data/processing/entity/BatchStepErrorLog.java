/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "BatchStepErrorLog.findAll", query = "select p from BatchStepErrorLog p"),
	@NamedQuery(name = "BatchStepErrorLog.findOne", query = "select p from BatchStepErrorLog p where p.batchStepErrorLogId=:batchStepErrorLogId")
})
@Table(name = "batch_step_error_log")
@Entity
public class BatchStepErrorLog implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long batchStepErrorLogId;
	
	private Long jobExecutionId;
	private Long stepExecutionId;
	
	private String errorType;
	private String errorMessage;
	
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;
	
	public BatchStepErrorLog() {
	}

	public long getBatchStepErrorLogId() {
		return batchStepErrorLogId;
	}

	public void setBatchStepErrorLogId(long batchStepErrorLogId) {
		this.batchStepErrorLogId = batchStepErrorLogId;
	}
	
	public Long getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public Long getStepExecutionId() {
		return stepExecutionId;
	}

	public void setStepExecutionId(Long stepExecutionId) {
		this.stepExecutionId = stepExecutionId;
	}
	
	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}

	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}

	public Timestamp getCreatedStamp() {
		return createdStamp;
	}

	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}
	
}
