/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "PartySupplementalData.findAll", query = "select p from PartySupplementalData p"),
	@NamedQuery(name = "PartySupplementalData.findOne", query = "select p from PartySupplementalData p where p.partyId=:partyId")
})
@Table(name = "party_supplemental_data")
@Entity
public class PartySupplementalData implements Serializable {
	
	@Id
	private String partyId;
	
	private String industryEnumId;
	private String ownershipEnumId;
	
	public PartySupplementalData() {
		
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	
	public String getIndustryEnumId() {
		return industryEnumId;
	}

	public void setIndustryEnumId(String industryEnumId) {
		this.industryEnumId = industryEnumId;
	}

	public String getOwnershipEnumId() {
		return ownershipEnumId;
	}

	public void setOwnershipEnumId(String ownershipEnumId) {
		this.ownershipEnumId = ownershipEnumId;
	}

	public String getPrimaryIdentifier() {
		return "partyId: "+partyId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " party_Id='"+partyId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
