/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "CustomFieldValue.findAll", query = "select p from CustomFieldValue p"),
	@NamedQuery(name = "CustomFieldValue.findOne", query = "select p from CustomFieldValue p where p.customFieldId=:customFieldId and p.partyId=:partyId")
})
@Table(name = "custom_field_value")
@Entity
public class CustomFieldValue implements Serializable {
	
	@Id
	private String customFieldId;
	@Id
	private String partyId;
	private String fieldValue;
	private Timestamp fromDate;
	private Timestamp thruDate;
	private Timestamp lastUpdatedStamp;
	private Timestamp createdStamp;

	public CustomFieldValue() {
		
	}
	
	public String getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(String customFieldId) {
		this.customFieldId = customFieldId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getThruDate() {
		return thruDate;
	}

	public void setThruDate(Timestamp thruDate) {
		this.thruDate = thruDate;
	}

	public Timestamp getLastUpdatedStamp() {
		return lastUpdatedStamp;
	}

	public void setLastUpdatedStamp(Timestamp lastUpdatedStamp) {
		this.lastUpdatedStamp = lastUpdatedStamp;
	}

	public Timestamp getCreatedStamp() {
		return createdStamp;
	}

	public void setCreatedStamp(Timestamp createdStamp) {
		this.createdStamp = createdStamp;
	}

	public String getPrimaryIdentifier() {
		return "customFieldId: "+customFieldId+", partyId:"+partyId;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " custom_Field_Id='"+customFieldId+"' AND party_Id='"+partyId+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
