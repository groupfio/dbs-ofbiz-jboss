/**
 * 
 */
package org.groupfio.pricing.data.processing.reader;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.groupfio.pricing.data.processing.entity.CustomField;
import org.groupfio.pricing.data.processing.entity.SftpConfiguration;
import org.groupfio.pricing.data.processing.util.PropertiesCache;
import org.groupfio.pricing.data.processing.util.SftpUtility;
import org.groupfio.pricing.data.processing.util.StaticResourceLoader;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

/**
 * @author Sharif
 *
 */
@Component
public class CustomFieldItemReader implements ItemReader<CustomField> {

	//private ItemReader<CustomField> delegate;
	//private MultiResourceItemReader<CustomField> delegate;
	
	@Override
	public CustomField read() throws Exception {
		
		String localImportLocation = PropertiesCache.getInstance().getProperty("local.import.location");
		Resource baseResource = new FileSystemResource(localImportLocation);
		
		StaticResourceLoader resourceLoader = new StaticResourceLoader();
        resourceLoader.setBaseResource(baseResource);
		
		Resource[] resources = null;
		
	    ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(resourceLoader);   
	    try {
	        resources = patternResolver.getResources("attribute-*.csv");
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
		
	    //MultiResourceItemReader<CustomField> reader = new MultiResourceItemReader<CustomField>();
		
		//if (delegate == null) {
	    MultiResourceItemReader<CustomField> delegate = new MultiResourceItemReader<CustomField>();
        //}
		
		delegate.setResources(resources);
		delegate.setDelegate(reader());
		
        return delegate.read();
		
	}
	
	public FlatFileItemReader<CustomField> reader() {
    	
    	DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    	lineTokenizer.setNames(new String[] {"customFieldId", "customFieldName", "customFieldFormat"});
    	
    	BeanWrapperFieldSetMapper<CustomField> fieldMapper = new BeanWrapperFieldSetMapper<CustomField>();
        fieldMapper.setTargetType(CustomField.class);
    	
    	DefaultLineMapper<CustomField> lineMapper = new DefaultLineMapper<CustomField>();
    	lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldMapper);
    	
    	FlatFileItemReader<CustomField> reader = new FlatFileItemReader<CustomField>();
    	reader.setName("customFieldItemReader");
    	reader.setEncoding("UTF-8");
    	reader.setLinesToSkip(0);
    	
    	reader.setLineMapper(lineMapper);
    	
    	return reader;
        
    }

	
	
}
