/**
 * 
 */
package org.groupfio.pricing.data.processing.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Sharif
 *
 */
@NamedQueries({ 
	@NamedQuery(name = "HdpCaAccount.findAll", query = "select p from HdpCaAccount p"),
	@NamedQuery(name = "HdpCaAccount.findOne", query = "select p from HdpCaAccount p where p.lcin=:lcin and p.acctNum=:acctNum and p.cntryCde=:cntryCde and p.businessDt=:businessDt")
})
@Table(name = "hdp_ca_account")
@Entity
public class HdpCaAccount implements Serializable {

	@Id
	private String lcin;
	
	private String srcSysCde;
	@Id
	private String acctNum;
	private String acctCcyCde;
	private BigDecimal acctCcyAmt;
	private BigDecimal acctCcyAvgAmt;
	private String glAccountAlt;
	private String prinGlNbr;
	private String glProduct;
	private String hypProduct;
	private String acctStatus;
	private String opDt;
	private String valDt;
	private Long branchCde;
	private Long schmTyp;
	private Long schmCde;
	private String acctClDt;
	private Long legalEntity;
	private String localCcyCde;
	private String acctName;
	private String schmDesc;
	private String rm;
	private String intBasis;
	private String baseCcyCde;
	private String bsType;
	private String intPeriodicity;
	private Long payDayConvention;
	private String dealRelType;
	private Long pcCode;
	private String bookType;
	private BigDecimal earmarkAmt;
	private String earmarkCode;
	private String salCrdDbtInd;
	private String stdInstrInd;
	private String clrWthDbsInd;
	private String cashMngtInd;
	private String cashMngtFee;
	private String acctOwnership;
	private String priAcctInd;
	private Long numAcctHolders;
	private String staffFlag;
	private Long freezeCode;
	private Long modeOfOpertaion;
	private String holdFundDate;
	private String securedAgainstColletaral;
	private String chequeFacility;
	private Long calcDayConvention;
	private String glBu;
	private String lglEntLvl4;
	private String drvFundUser;
	private String procSrcSysCde;
	@Column(name="CONTRACT_REF_ADDT1")
	private String contractRefAddt1;
	@Column(name="CONTRACT_REF_ADDT2")
	private String contractRefAddt2;
	@Column(name="CUSTOMER_REF1")
	private String customerRef1;
	@Column(name="CUSTOMER_REF2")
	private String customerRef2;
	@Column(name="CUSTOMER_REF3")
	private String customerRef3;
	private String giroInd;
	private String finCif;
	private String dpsExclusionInd;
	private String programCode;
	private String branchDeptCode;
	private String loadDate;
	@Id
	private String businessDt;
	@Id
	private String cntryCde;
	
	private String accountDesc;
	private String productDesc;
	
	private String batchId;
	private String importStatusId;
	private String importError;
	private Timestamp processedTimestamp;
	
	public HdpCaAccount() {
	}

	public String getLcin() {
		return lcin;
	}

	public void setLcin(String lcin) {
		this.lcin = lcin;
	}

	public String getSrcSysCde() {
		return srcSysCde;
	}

	public void setSrcSysCde(String srcSysCde) {
		this.srcSysCde = srcSysCde;
	}

	public String getAcctNum() {
		return acctNum;
	}

	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}

	public String getAcctCcyCde() {
		return acctCcyCde;
	}

	public void setAcctCcyCde(String acctCcyCde) {
		this.acctCcyCde = acctCcyCde;
	}

	public BigDecimal getAcctCcyAmt() {
		return acctCcyAmt;
	}

	public void setAcctCcyAmt(BigDecimal acctCcyAmt) {
		this.acctCcyAmt = acctCcyAmt;
	}

	public BigDecimal getAcctCcyAvgAmt() {
		return acctCcyAvgAmt;
	}

	public void setAcctCcyAvgAmt(BigDecimal acctCcyAvgAmt) {
		this.acctCcyAvgAmt = acctCcyAvgAmt;
	}

	public String getGlAccountAlt() {
		return glAccountAlt;
	}

	public void setGlAccountAlt(String glAccountAlt) {
		this.glAccountAlt = glAccountAlt;
	}

	public String getPrinGlNbr() {
		return prinGlNbr;
	}

	public void setPrinGlNbr(String prinGlNbr) {
		this.prinGlNbr = prinGlNbr;
	}

	public String getGlProduct() {
		return glProduct;
	}

	public void setGlProduct(String glProduct) {
		this.glProduct = glProduct;
	}

	public String getHypProduct() {
		return hypProduct;
	}

	public void setHypProduct(String hypProduct) {
		this.hypProduct = hypProduct;
	}

	public String getAcctStatus() {
		return acctStatus;
	}

	public void setAcctStatus(String acctStatus) {
		this.acctStatus = acctStatus;
	}

	public String getOpDt() {
		return opDt;
	}

	public void setOpDt(String opDt) {
		this.opDt = opDt;
	}

	public String getValDt() {
		return valDt;
	}

	public void setValDt(String valDt) {
		this.valDt = valDt;
	}

	public Long getBranchCde() {
		return branchCde;
	}

	public void setBranchCde(Long branchCde) {
		this.branchCde = branchCde;
	}

	public Long getSchmTyp() {
		return schmTyp;
	}

	public void setSchmTyp(Long schmTyp) {
		this.schmTyp = schmTyp;
	}

	public Long getSchmCde() {
		return schmCde;
	}

	public void setSchmCde(Long schmCde) {
		this.schmCde = schmCde;
	}

	public String getAcctClDt() {
		return acctClDt;
	}

	public void setAcctClDt(String acctClDt) {
		this.acctClDt = acctClDt;
	}

	public Long getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(Long legalEntity) {
		this.legalEntity = legalEntity;
	}

	public String getLocalCcyCde() {
		return localCcyCde;
	}

	public void setLocalCcyCde(String localCcyCde) {
		this.localCcyCde = localCcyCde;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public String getSchmDesc() {
		return schmDesc;
	}

	public void setSchmDesc(String schmDesc) {
		this.schmDesc = schmDesc;
	}

	public String getRm() {
		return rm;
	}

	public void setRm(String rm) {
		this.rm = rm;
	}

	public String getIntBasis() {
		return intBasis;
	}

	public void setIntBasis(String intBasis) {
		this.intBasis = intBasis;
	}

	public String getBaseCcyCde() {
		return baseCcyCde;
	}

	public void setBaseCcyCde(String baseCcyCde) {
		this.baseCcyCde = baseCcyCde;
	}

	public String getBsType() {
		return bsType;
	}

	public void setBsType(String bsType) {
		this.bsType = bsType;
	}

	public String getIntPeriodicity() {
		return intPeriodicity;
	}

	public void setIntPeriodicity(String intPeriodicity) {
		this.intPeriodicity = intPeriodicity;
	}

	public Long getPayDayConvention() {
		return payDayConvention;
	}

	public void setPayDayConvention(Long payDayConvention) {
		this.payDayConvention = payDayConvention;
	}

	public String getDealRelType() {
		return dealRelType;
	}

	public void setDealRelType(String dealRelType) {
		this.dealRelType = dealRelType;
	}

	public Long getPcCode() {
		return pcCode;
	}

	public void setPcCode(Long pcCode) {
		this.pcCode = pcCode;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public BigDecimal getEarmarkAmt() {
		return earmarkAmt;
	}

	public void setEarmarkAmt(BigDecimal earmarkAmt) {
		this.earmarkAmt = earmarkAmt;
	}

	public String getEarmarkCode() {
		return earmarkCode;
	}

	public void setEarmarkCode(String earmarkCode) {
		this.earmarkCode = earmarkCode;
	}

	public String getSalCrdDbtInd() {
		return salCrdDbtInd;
	}

	public void setSalCrdDbtInd(String salCrdDbtInd) {
		this.salCrdDbtInd = salCrdDbtInd;
	}

	public String getStdInstrInd() {
		return stdInstrInd;
	}

	public void setStdInstrInd(String stdInstrInd) {
		this.stdInstrInd = stdInstrInd;
	}

	public String getClrWthDbsInd() {
		return clrWthDbsInd;
	}

	public void setClrWthDbsInd(String clrWthDbsInd) {
		this.clrWthDbsInd = clrWthDbsInd;
	}

	public String getCashMngtInd() {
		return cashMngtInd;
	}

	public void setCashMngtInd(String cashMngtInd) {
		this.cashMngtInd = cashMngtInd;
	}

	public String getCashMngtFee() {
		return cashMngtFee;
	}

	public void setCashMngtFee(String cashMngtFee) {
		this.cashMngtFee = cashMngtFee;
	}

	public String getAcctOwnership() {
		return acctOwnership;
	}

	public void setAcctOwnership(String acctOwnership) {
		this.acctOwnership = acctOwnership;
	}

	public String getPriAcctInd() {
		return priAcctInd;
	}

	public void setPriAcctInd(String priAcctInd) {
		this.priAcctInd = priAcctInd;
	}

	public Long getNumAcctHolders() {
		return numAcctHolders;
	}

	public void setNumAcctHolders(Long numAcctHolders) {
		this.numAcctHolders = numAcctHolders;
	}

	public String getStaffFlag() {
		return staffFlag;
	}

	public void setStaffFlag(String staffFlag) {
		this.staffFlag = staffFlag;
	}

	public Long getFreezeCode() {
		return freezeCode;
	}

	public void setFreezeCode(Long freezeCode) {
		this.freezeCode = freezeCode;
	}

	public Long getModeOfOpertaion() {
		return modeOfOpertaion;
	}

	public void setModeOfOpertaion(Long modeOfOpertaion) {
		this.modeOfOpertaion = modeOfOpertaion;
	}

	public String getHoldFundDate() {
		return holdFundDate;
	}

	public void setHoldFundDate(String holdFundDate) {
		this.holdFundDate = holdFundDate;
	}

	public String getSecuredAgainstColletaral() {
		return securedAgainstColletaral;
	}

	public void setSecuredAgainstColletaral(String securedAgainstColletaral) {
		this.securedAgainstColletaral = securedAgainstColletaral;
	}

	public String getChequeFacility() {
		return chequeFacility;
	}

	public void setChequeFacility(String chequeFacility) {
		this.chequeFacility = chequeFacility;
	}

	public Long getCalcDayConvention() {
		return calcDayConvention;
	}

	public void setCalcDayConvention(Long calcDayConvention) {
		this.calcDayConvention = calcDayConvention;
	}

	public String getGlBu() {
		return glBu;
	}

	public void setGlBu(String glBu) {
		this.glBu = glBu;
	}

	public String getLglEntLvl4() {
		return lglEntLvl4;
	}

	public void setLglEntLvl4(String lglEntLvl4) {
		this.lglEntLvl4 = lglEntLvl4;
	}

	public String getDrvFundUser() {
		return drvFundUser;
	}

	public void setDrvFundUser(String drvFundUser) {
		this.drvFundUser = drvFundUser;
	}

	public String getProcSrcSysCde() {
		return procSrcSysCde;
	}

	public void setProcSrcSysCde(String procSrcSysCde) {
		this.procSrcSysCde = procSrcSysCde;
	}

	public String getContractRefAddt1() {
		return contractRefAddt1;
	}

	public void setContractRefAddt1(String contractRefAddt1) {
		this.contractRefAddt1 = contractRefAddt1;
	}

	public String getContractRefAddt2() {
		return contractRefAddt2;
	}

	public void setContractRefAddt2(String contractRefAddt2) {
		this.contractRefAddt2 = contractRefAddt2;
	}

	public String getCustomerRef1() {
		return customerRef1;
	}

	public void setCustomerRef1(String customerRef1) {
		this.customerRef1 = customerRef1;
	}

	public String getCustomerRef2() {
		return customerRef2;
	}

	public void setCustomerRef2(String customerRef2) {
		this.customerRef2 = customerRef2;
	}

	public String getCustomerRef3() {
		return customerRef3;
	}

	public void setCustomerRef3(String customerRef3) {
		this.customerRef3 = customerRef3;
	}

	public String getGiroInd() {
		return giroInd;
	}

	public void setGiroInd(String giroInd) {
		this.giroInd = giroInd;
	}

	public String getFinCif() {
		return finCif;
	}

	public void setFinCif(String finCif) {
		this.finCif = finCif;
	}

	public String getDpsExclusionInd() {
		return dpsExclusionInd;
	}

	public void setDpsExclusionInd(String dpsExclusionInd) {
		this.dpsExclusionInd = dpsExclusionInd;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getBranchDeptCode() {
		return branchDeptCode;
	}

	public void setBranchDeptCode(String branchDeptCode) {
		this.branchDeptCode = branchDeptCode;
	}

	public String getLoadDate() {
		return loadDate;
	}

	public void setLoadDate(String loadDate) {
		this.loadDate = loadDate;
	}

	public String getBusinessDt() {
		return businessDt;
	}

	public void setBusinessDt(String businessDt) {
		this.businessDt = businessDt;
	}

	public String getCntryCde() {
		return cntryCde;
	}

	public void setCntryCde(String cntryCde) {
		this.cntryCde = cntryCde;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getImportStatusId() {
		return importStatusId;
	}

	public void setImportStatusId(String importStatusId) {
		this.importStatusId = importStatusId;
	}

	public String getImportError() {
		return importError;
	}

	public void setImportError(String importError) {
		this.importError = importError;
	}

	public Timestamp getProcessedTimestamp() {
		return processedTimestamp;
	}

	public void setProcessedTimestamp(Timestamp processedTimestamp) {
		this.processedTimestamp = processedTimestamp;
	}

	public String getAccountDesc() {
		return accountDesc;
	}

	public void setAccountDesc(String accountDesc) {
		this.accountDesc = accountDesc;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	
	public String getPrimaryIdentifier() {
		return "lcin: "+lcin+", acctNum: "+acctNum+", cntryCde:"+cntryCde+", businessDt:"+businessDt;
	}
	public void setPrimaryIdentifier(String primaryIdentifier) {
		
	}
	
	public String getPrimaryCondition() {
		return " lcin='"+lcin+"' AND acct_Num='"+acctNum+"' AND cntry_Cde='"+cntryCde+"'"+" AND business_Dt='"+businessDt+"'";
	}
	public void setPrimaryCondition(String primaryCondition) {
		
	}
}
