/**
 * 
 */
package org.groupfio.pricing.data.processing.util;

import javax.persistence.EntityManager;

import org.groupfio.pricing.data.processing.entity.BatchStepErrorLog;

/**
 * @author Sharif
 *
 */
public class LogWriter {

	public static boolean write(EntityManager entityManager, Long jobExecutionId, Long stepExecutionId, String errorMessage, String errorType) {
		
		try {
			entityManager.getTransaction().begin();
			
			BatchStepErrorLog errorLog = new BatchStepErrorLog();
			
			errorLog.setJobExecutionId(jobExecutionId);
			errorLog.setStepExecutionId(stepExecutionId);
			
			errorLog.setErrorMessage(errorMessage);
			errorLog.setErrorType(errorType);
			
			errorLog.setCreatedStamp(UtilDateTime.nowTimestamp());
			errorLog.setLastUpdatedStamp(UtilDateTime.nowTimestamp());
			
			entityManager.persist(errorLog);
			
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			return false;
		}
		
		return true;
		
	}
	
}
