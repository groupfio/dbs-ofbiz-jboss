/**
 * 
 */
package org.groupfio.pricing.data.processing.writer;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.axiom.om.OMElement;
import org.groupfio.pricing.data.processing.BatchConstants.ErrorType;
import org.groupfio.pricing.data.processing.entity.BatchStepErrorLog;
import org.groupfio.pricing.data.processing.entity.HdpCaAccount;
import org.groupfio.pricing.data.processing.listener.step.CaAccountExecutionListener;
import org.groupfio.pricing.data.processing.util.LogWriter;
import org.groupfio.pricing.data.processing.util.UtilDateTime;
import org.groupfio.pricing.data.processing.util.UtilValidate;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Sharif
 *
 */
public class HdpCaAccountItemWriter implements ItemWriter<HdpCaAccount> {
	
	private static final Logger log = LoggerFactory.getLogger(HdpCaAccountItemWriter.class);

	@Autowired
    private EntityManagerFactory entityManagerFactory;
	
	@Autowired
    private CaAccountExecutionListener stepExecutionListener;

	@Override
	public void write(List<? extends HdpCaAccount> items) throws Exception {
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		Set<String> accountIds = new HashSet<String>();
		
		for (HdpCaAccount item : items) {
			
			try {
				log.info("importing HdpCaAccount: "+item.getPrimaryIdentifier());
				entityManager.getTransaction().begin();
				
				entityManager.persist(item);
				
				entityManager.getTransaction().commit();
			} catch (Exception e) {
				entityManager.getTransaction().rollback();
				
				if (e.getCause().getCause() instanceof ConstraintViolationException) {
					LogWriter.write(entityManager, stepExecutionListener.getStepExecution().getJobExecutionId(), stepExecutionListener.getStepExecution().getId(), "Duplicate HdpCaAccount, " + item.getPrimaryIdentifier(), ErrorType.DUPLICATE);
				} else if (e.getCause().getCause() instanceof DataException) {
					LogWriter.write(entityManager, stepExecutionListener.getStepExecution().getJobExecutionId(), stepExecutionListener.getStepExecution().getId(), "Parsing error HdpCaAccount, " + item.getPrimaryIdentifier() + ", error: "+e.getCause().getCause().getCause().getMessage(), ErrorType.PARSING);
				} else {
					LogWriter.write(entityManager, stepExecutionListener.getStepExecution().getJobExecutionId(), stepExecutionListener.getStepExecution().getId(), "Unknown error HdpCaAccount, " + item.getPrimaryIdentifier()+ ", error: "+e.getCause().getCause().getCause().getMessage(), ErrorType.UNKNOWN);
				}
				
			}
			
		}
		
		entityManager.close();
		
	}
	
}
